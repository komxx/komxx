# Copyright (C) 1996  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import string
import log
import re
import SimpleHTTPServer
import base64

import komhtml
import komexceptions

from language import lingo

class myhttpbase(SimpleHTTPServer.SimpleHTTPRequestHandler):

    def send_response(self, code, message=None):
	"As myhttpbase.send_response, but adds some field."
	SimpleHTTPServer.SimpleHTTPRequestHandler.send_response(
	   self, code, message)
	if not hasattr(self, "cookies"):
	    self.cookies = []
	if hasattr(self, "_cookie_name"):
	    if not hasattr(self, "_cookie_number"):
		log.err("Internal err: _cookie_number unset, but _cooke_name="
			+ `self._cookie_name`)
		raise komexceptions.internal_error("inconsistent cookies")
	    if self._cookie_name == None:
		name = ""
	    else:
		name = re.sub("=", "#", string.strip(base64.encodestring(
		   self._cookie_name)))
	    if self._cookie_number == None:
		number = ""
	    else:
		number = `self._cookie_number`
	    self.cookies.append(("LysKOM_www_cookie", name + ":" + number))
	for (key, val) in self.cookies:
	    self.send_header("Set-cookie", key + "=" + val + "; path=/")

    def respond(self, body):
	self.send_response(200)
	self.send_header("Content-type", "text/html")
	self.send_header("Content-length", len(body))
	self.end_headers()
	self.wfile.write(body)

    def log_message(self, format, *args):
	"""Log an arbitrary message.

	This is used by all other logging functions.  Override
	it if you have specific logging wishes.

	The first argument, FORMAT, is a format string for the
	message to be logged.  If the format string contains
	any % escapes requiring parameters, they should be
	specified as subsequent arguments (it's just like
	printf!).

	The client host and current date/time are prefixed to
	every message.

	"""

	log.info("%s %s" % (self.address_string(), format%args))

    def bad_auth(self):
	self.send_response(401)
	self.send_header("WWW-Authenticate", 'Basic realm="LysKOM"')
	self.end_headers()

    def redirect(self, newsession = None):
	if newsession:
	    newurl = komhtml.decodeurl(self.url.as_string_sans_hostpart())
	    newurl.pers_no = newsession
	    urlstr = newurl.as_string_with_session()
	else:
	    urlstr = self.url.as_string()
	self.send_redirect(urlstr)

    def send_redirect(self, urlstr):
	body = lingo.session_redirect(urlstr)
	self.send_response(301)
	self.send_header("Location", urlstr)
	self.send_header("Content-type", "text/html")
	self.send_header("Content-length", len(body))
	self.end_headers()
	self.wfile.write(body)

    # Overridden since the default method didn't specify
    # the content-type of the message.
    def send_error(self, code, message=None):
	"""Send and log an error reply.

	Arguments are the error code, and a detailed message.
	The detailed message defaults to the short entry matching the
	response code.

	This sends an error response (so it must be called before any
	output has been generated), logs the error, and finally sends
	a piece of HTML explaining the error to the user.

	"""

	try:
	    short, long = self.responses[code]
	except KeyError:
	    short, long = '???', '???'
	if not message:
	    message = short
	explain = long
	self.log_error("code %d, message %s", code, message)
	self.send_response(code, message)
	self.send_header("Content-type", "text/html")
	self.end_headers()
	self.wfile.write(self.error_message_format %
			 {'code': code,
			  'message': message,
			  'explain': explain})

    def cookie_fetch(self):
	if not hasattr(self, "_cookie_name"):
	    self._cookie_name = None
	    self._cookie_number = None
	    self._raw_readorder = None
	    self.__cookie_is_read = None
	    self.supports_cookies = 0
	    self.cookies = []
	    cookieblob = self.headers.getheader('Cookie')
	    if cookieblob != None:
		cookieblob = re.sub(" \t", "", cookieblob)
		cookies = string.splitfields(cookieblob, ";")
		for cookie in cookies:
		    keyval = string.splitfields(cookie, "=")
		    if (len(keyval) == 2 and
			string.strip(keyval[0]) == "LysKOM_www_cookie"):

			log.debug("Got cookie: " + keyval[1])
			self.supports_cookies = 1
			fields = string.splitfields(string.strip(keyval[1]),
						    ":")
			if len(fields) != 2:
			    log.notice("Malformed cookie '" + cookie
				       + "' received")
			else:
			    try:
				self._cookie_number = string.atoi(fields[1])
			    except:
				pass
			    self._cookie_name = base64.decodestring(
			       re.sub("#", "=", fields[0]))
			    if (self._cookie_name == "" \
				or not self._cookie_number):

				self._cookie_number = None
				self._cookie_name = None
		    if (len(keyval) == 2 \
			and string.strip(keyval[0])== "LysKOM_www_readorder"):

			self.supports_cookies = 1
			log.debug("Got readorder: " + keyval[1])
			self._raw_readorder = string.strip(keyval[1])
		    if (len(keyval) == 2 \
			and string.strip(keyval[0])== "LysKOM_auth" \
			and string.strip(keyval[1])== "bad"):

			self.supports_cookies = 1
			log.debug("Got cookie LysKOM_auth=bad")
			self.bad_authorization_cookie = 1
		    if (len(keyval) == 2 \
			and string.strip(keyval[0])== "LysKOM_is_read"):

			self.supports_cookies = 1
			log.debug("Got cookie is_read")
			try:
			    self.__cookie_is_read = string.atoi(keyval[1])
			except:
			    pass

    def cookie_pers_no(self):
	self.cookie_fetch()
	return self._cookie_number

    def cookie_name(self):
	self.cookie_fetch()
	return self._cookie_name

    def cookie_read_order(self):
	self.cookie_fetch()
	return self._raw_readorder

    def cookie_is_read(self):
	self.cookie_fetch()
	return self.__cookie_is_read

    def set_cookie(self, name, number):
	self._cookie_name = name
	self._cookie_number = number

    def clear_cookie(self):
	self._cookie_number = None
	self._cookie_name = None

    def retrieve_authorization(self):
	"""Return a list [name, password], or raise komhtml.bad_auth."""
	auth = self.headers.getheader('Authorization')
	if auth == None:
	    raise komhtml.bad_auth

	[scheme, auth_cookie] = string.split(auth)
	if (string.lower(scheme) != "basic"):
	    self.log_error("Unknown authorization scheme used: " + auth)
	    raise komhtml.bad_auth

	return string.split(base64.decodestring(auth_cookie), ":")
