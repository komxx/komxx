# Copyright (C) 1996  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import string

import komexceptions
import komhtml
from language import lingo

class toolbar:
    """Generalized toolbar: handle the command icons.

    There are currently two supported page styles:  
    - The traditional,
          where there is a line of icons at the top of the page 
	  (which may also be repeated at the bottom), and 
    - the July 1997 style 
          with a number of icons at fixed positions in a table 
	  column to the left of the screen.  This class may handle 
	  even more layouts in the future.

    All toolbars have active tools for "home", "selconf" and "exit",
    unless they are inhibited with the methods, which must be called
    before the first call to at_top(), at_bottom() or table_element()
    to take effect:

      toolbar.inhibit_home()
      toolbar.inhibit_selconf()
      toolbar.inhibit_exit()

    New tools can be activated with any of the following methods:

      toolbar.add_text_unformatted(tno)
      toolbar.add_text_formatted(tno)
      toolbar.add_config()
      toolbar.add_comment(tno)
      toolbar.add_current_conf(cno)
      toolbar.add_write(cno)
      toolbar.add_review(cno)
      toolbar.add_help(url_trail)

    New tools can also be activated by these methods, but only one of them
    may be added since they are mutually exclusive:
    
      toolbar.add_default_conf(cno, cname)
      toolbar.add_default_text(tno)
      toolbar.add_default_comment(tno)
      toolbar.add_default_footnote(tno)
      toolbar.add_default_time()

    To control if the toolbar should appear at the top and/or the
    bottom, the following methods may be called.  The default is that
    the toolbar appears at the top only.  The July 1997 layout is not
    affected by these methods.

      toolbar.enable_bottom()
      toolbar.disable_top()

    These methods returns the toolbar as an HTML string with the
    traditional layout; the string will be empty if no toolbar is
    wanted at the specified point.

      toolbar.at_top()
      toolbar.at_bottom()

    This method returns the toolbar with the July 1997 layout as a
    string.

      toolbar.table_element()

    The ordering of the icons in the old-style layout is determined by
    the __ordering member.  The placement in the July 1997 layout is
    determined by the __placement and __textops* members.  Both all
    these members may have to be updated as new icons are added.

    """

    # Keep this in sync with the icons installed in html/Makefile.am!
    __ordering = [

	# Default commands
	"next-conf",
	"next-text",
	"next-comment",
	"next-footnote",
	"time",

	# Page-specific commands
	"comment",
	"answer",
	"write",
	#"review",
	"unformatted",
	"formatted",

	# More or less universally available commands
	"board",
	"home",
	"selconf",
	"config",
	"create-conf",
	"admin-conf",
	"create-user",

	# Exit and help
	"exit",
	"help"]

    # Keep this in sync with the icons installed in html/Makefile.am!
    __placement = [
	[
	    # Top level operations
	    "home",
	    "selconf",
	    "next-conf",
	], [
	    # Operations within a conference
	    "board",
	    #"review",
	    "write",
	], [
	    # Text operations
	    "formatted",	# Show text formatted
	    "unformatted",	# Show text unformatted
	], [
	    "create-conf",
	    "admin-conf",
	    "create-user",
	], [
	    # General operations
	    #"time",		# Get the current time
	    "config",		# Change personal settings
	    #"help", FIXME: Doesn't work right now
	    "exit"		# Exit the LysKOM system
	]]

    __textops_1 = [[\
	"comment",
	]]

    __textops_2 = [[\
	"answer",
	]]

    def __init__(self):
	self.__top_wanted = 1
	self.__bottom_wanted = 0
	self.__textops_wanted = 0
	self.__toolbar = []
	self.__toolmap = {}
	self.__default = 0
	self.__sorted = 1
	self.__inhibit_home = 0
	self.__inhibit_selconf = 0
	self.__inhibit_exit = 0
	self.__default_added = 0
	self.__default_july_1997 = None

    def enable_bottom(self):
	self.__bottom_wanted = 1

    def disable_top(self):
	self.__top_wanted = 0

    def enable_textops(self):
	self.__textops_wanted = 1

    def __add_tool(self, image, alt, url, explanation):
	order = self.__ordering.index(image)
	icon = komhtml.icon(image, alt, url, explanation)
	self.__toolbar.append((order, icon))
	self.__toolmap[image] = komhtml.icon(image, alt, url,
					     explanation, "July 1997-sv")
	self.__sorted = 0

    def add_text_unformatted(self, tno):
	self.__add_tool("unformatted", lingo.alt_unformatted(),
			komhtml.komurl("text?text_no=" + `tno` + "&style=pre"),
			lingo.explain_unformatted(tno))
	
    def add_text_formatted(self, tno):
	self.__add_tool("formatted", lingo.alt_formatted(),
			komhtml.komurl("text?text_no=" + `tno`
				       + "&style=heuristic"),
			lingo.explain_formatted(tno))

    def add_config(self):
	self.__add_tool("config", lingo.alt_config(),
			komhtml.komurl("configuration"),
			lingo.explain_config())

    def add_create_conf(self):
	self.__add_tool("create-conf", lingo.alt_create_conf(),
			komhtml.komurl("create_conf_form"),
			lingo.explain_create_conf())

    def add_admin_conf(self, confno):
	self.__add_tool("admin-conf", lingo.alt_admin_conf(),
			komhtml.komurl("admin_conf?conf_no=" + `confno`),
			lingo.explain_admin_conf())

    def add_create_user(self):
	self.__add_tool("create-user", lingo.alt_create_user(),
			komhtml.komurl("create_user_form"),
			lingo.explain_create_user())

    def __add_dflt(self, image, alt, url, explanation):
	if self.__default != 0:
	    raise komexceptions.system_error("default set twice")
	self.__default = 1
	self.__add_tool(image, alt, url, explanation)
	self.__default_july_1997 = "%s<br><font size=-1>%s</font>" % (
	    komhtml.icon("arrow", alt, url, explanation, "July 1997-sv"),
	    alt)

    #
    # ----------------------------------------------------------------
    #

    def add_comment(self, textno):
	self.__current_text = textno
	self.__add_tool("comment", lingo.alt_comment(),
			komhtml.komurl("comment?parent=" + `textno`),
			lingo.explain_comment(textno))

    def add_answer(self, textno):
	self.__current_text = textno
	self.__add_tool("answer", lingo.alt_answer(),
			komhtml.komurl("answer?parent=" + `textno`),
			lingo.explain_answer(textno))

    #
    # ----------------------------------------------------------------
    #

    def add_current_conf(self, confno):
	self.__add_tool("board", lingo.alt_conf(),
			komhtml.komurl("conf?conf_no=" + `confno`),
			lingo.explain_conf())

    def add_write(self, confno):
	self.__add_tool("write", lingo.alt_write_text(),
			komhtml.komurl("write_text?conf_no=" + `confno`),
			lingo.explain_write_text())

    def add_review(self, confno):
	return
	self.__add_tool("review", lingo.alt_review_text(),
			komhtml.komurl("review_texts?conf_no=" + `confno`),
			lingo.explain_review_text())

    def add_help(self, url_trail):
	self.__add_tool("help", lingo.alt_help(),
			komhtml.komurl_help(url_trail),
			lingo.explain_help())	

    def add_default_conf(self, cno, cname):
	self.__add_dflt("next-conf", lingo.alt_next_conf(cname),
			komhtml.komurl("conf?conf_no=" + `cno`),
			lingo.explain_next_conf(cname))

    def add_default_text(self, tno):
	self.__add_dflt("next-text", lingo.alt_next_text(tno),
			komhtml.komurl("text?text_no=" + `tno`),
			lingo.explain_next_text(tno))

    def add_default_comment(self, tno):
	self.__add_dflt("next-comment", lingo.alt_next_comment(tno),
			komhtml.komurl("text?text_no=" + `tno`),
			lingo.explain_next_comment(tno))

    def add_default_footnote(self, tno):
	self.__add_dflt("next-footnote", lingo.alt_next_footnote(tno),
			komhtml.komurl("text?text_no=" + `tno`),
			lingo.explain_next_footnote(tno))

    def add_default_time(self):
	self.__add_dflt("time", lingo.alt_see_time(),
			komhtml.komurl("time"),
			lingo.explain_see_time())

    def inhibit_home(self):
	self.__inhibit_home = 1

    def inhibit_selconf(self):
	self.__inhibit_selconf = 1

    def inhibit_exit(self):
	self.__inhibit_exit = 1

    def __add_default(self):
	if not self.__default_added:
	    if not self.__inhibit_home:
		self.__add_tool("home", lingo.alt_home(),
				komhtml.komurl("home"),
				lingo.explain_home())
	    if not self.__inhibit_selconf:
		self.__add_tool("selconf", lingo.alt_select_conf(),
				komhtml.komurl("select_conf_form"),
				lingo.explain_select_conf())
	    if not self.__inhibit_exit:
		self.__add_tool("exit", lingo.alt_kill(),
				komhtml.komurl("kill"),
				lingo.explain_kill())
	    self.__default_added = 1

    def __as_string(self):
	self.__add_default()
	if not self.__sorted:
	    self.__toolbar.sort()
	    self.__toolbar_sorted = map(lambda x : x[1], self.__toolbar)
	    self.__sorted = 1

	return string.join(["", "<!-- toolbar start -->"]
			   + self.__toolbar_sorted
			   + ["<!-- toolbar end --><br>"], "\n")


    def at_top(self):
	if self.__top_wanted:
	    return self.__as_string()
	else:
	    return ""

    def at_bottom(self):
	if self.__bottom_wanted:
	    return self.__as_string()
	else:
	    return ""

    def table_element(self):
	self.__add_default()
	res = ["<!-- toolbar July 1997 start -->\n",
	       "<td %svalign=top>\n" % "", # FIXME: width=166
	       ]
	self.table(res, self.__placement)
	res.append("</td><!-- toolbar July 1997 end -->\n")
	return string.joinfields(res, "")

    def text_ops(self):
	if self.__textops_wanted:
	    res = ["<!-- textops July 1997 start -->\n",
		   "<table width=\"100%\" cellpadding=3 cellspacing=0 "\
		   "border=0><tr bgcolor=\"#ccffff\"><td>"]
	    self.table(res, self.__textops_1)
	    res.append("</td><td>")
	    self.table(res, self.__textops_2)
	    res.append("</td><td width=\"100%\">&nbsp;</td><td>")

	    res.append(komhtml.anchor(komhtml.komurl("move_text") +
				      "?text_no=" + `self.__current_text`,
				      lingo.link_move_text()))
	    res.append("</td></tr></table>\n"\
		       "<!-- textops July 1997 end -->\n")
	    return string.joinfields(res, "")
	else:
	    return ""

    #
    # Add a table with groups of buttons to RES.
    #
    # GROUPLIST is a list of lists where each list contains a number 
    # of keys to the buttons. Each list of buttons will be contained 
    # in a black frame with a yellow background.
    #
    def table(self, res, grouplist):
	res.append("<table cellpadding=0 cellspacing=0 border=0>\n<tr><td>")
	res.append(komhtml.iconref("menu-top", "", "img"))
	res.append("</td></tr>\n")
	need_middle = 0
	for group in grouplist:
	    if need_middle:
		res.append("<tr><td>")
		res.append(komhtml.iconref("menu-middle-2", "", "img"))
		res.append("</td></tr>")
	    need_middle = 1
	    for image in group:
		res.append("<tr><td>")
		if self.__toolmap.has_key(image):
		    res.append(self.__toolmap[image])
		else:
		    res.append(komhtml.iconref(image, "", "July 1997-sv", 0))
		res.append("</td></tr>\n")
	res.append("<tr><td>")
	res.append(komhtml.iconref("menu-bottom", "", "img"))
	res.append("</td></tr>\n</table>\n")

    def default_element(self):
	if self.__default_july_1997 == None:
	    return ""
	else:
	    return self.__default_july_1997
