# Copyright (C) 1996  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import cgi
import pickle
import komconfig
import re
import re
import string
import sys
import log

bad_auth='bad_auth'
not_a_komurl='not_a_komurl'

def setlooknfeel(feel):
    global iconmode
    # Bad values for iconmode are ignored, so there is no need to
    # check the value here.
    log.debug("looknfeel set to " + feel)
    iconmode = feel

def init():
    global iconmode
    global hostpart

    iconmode="traditional"
    hostpart="http://" + komconfig.wwwhost
    if komconfig.wwwport != 80:
	hostpart = hostpart + ":" + komconfig.wwwport

text_to_mark = None

def anchor(url, text, explanation = None):
    if explanation == None:
	return '<a href="' + url + '">' + text + '</a>'
    else:
	return ('<a href="' + url + '" onMouseOver="window.status='
		+ "'" + explanation + "'" + '; return true;">'
		+ text + '</a>')

def form_anchor(cookie, text):
    """Create a single-button form.

    The COOKIE argument specifies which URL that should be invoked
    when the user activates the form.  It should be a tuple, whose first
    element is the form action (a string) and second element is a list
    of tuples (key, value) which specifies the arguments to send to
    the URL.

    The second argument TEXT specifies the text that should be used on
    the submit button.
    """

    res = ['<form action="',
	   cookie[0],
	   '" method="get">']
    for (k, v) in cookie[1]:
	res.append('<input type="hidden" name="')
	res.append(k)
	res.append('" value="')
	res.append(htmlquote(v))
	res.append('">')
    res.append('<input type="submit" value="')
    res.append(htmlquote(text))
    res.append('"></form>')
    return string.joinfields(res, '')


def komurl(path, session = None):
    # Decide if session should be string or number, and make sure it always
    # is.
    if session:
	session = `session`
	base = hostpart + "/wwwkom/(" + session + ")/" + path
    else:
	base = hostpart + "/wwwkom/" + path

    if text_to_mark == None:
	return base

    if string.find(path, "?") == -1:
	return base + "?is_read=" + `text_to_mark`
    else:
	return base + "&is_read=" + `text_to_mark`


def komurl_help(path):
    return hostpart + "/help/" + path

#
# Return a string which is a link to the conference CONF_NO using NAME
# as the text in the link.  
#
def conf_as_link(conf_no, name):
    return anchor(komurl('conf_stat?conf_no=' + `conf_no`), htmlquote(name))


def conf_as_direct_link(conf_no, name):
    return anchor(komurl('conf?conf_no=' + `conf_no`), htmlquote(name))


def text_as_link(text_no, link_text = None):
    if link_text == None:
	link_text = `text_no`
    return anchor(komurl('text?text_no=' + `text_no`), link_text)


def htmlquote(s):
    return cgi.escape(s)

def detect_url(s):
    s = re.sub('(https?://[^ "\n!,<>]*)', '<a href="\\1">\\1</a>', s)
    s = re.sub('(ftp://[^ "\n!,<>]*)', '<a href="\\1">\\1</a>', s)
    s = re.sub('(gopher://[^ "\n!,<>]*)', '<a href="\\1">\\1</a>', s)
    s = re.sub('(file://[^ "\n!,<>]*)', '<a href="\\1">\\1</a>', s)
    s = re.sub('(wais://[^ "\n!,<>]*)', '<a href="\\1">\\1</a>', s)
    # The URL below will not work, but management told me to put it here.
    s = re.sub('(kom://[^ "\n!,<>]*)', '<a href="\\1">\\1</a>', s)
    if text_to_mark:
	markpart="&is_read=" + `text_to_mark`
    else:
	markpart=""
    # FIXME: The lines below have several problems:
    # 1. They fail to detect "1984 7897 879" -- only every other
    #    number is turned into a link.
    # 2. They make links of numbers with no corresponding text.
    # Heuristic link detection should probably only be available if
    # the user wants it.
    # s = re.sub('(^\|[\n (])([0-9][0-9]*)([\n .]|$)',
    #	     '\\1<a href="' + hostpart
    #	     + '/wwwkom/text?text_no=\\2' + markpart + '">\\2</a>\\3',
    #	     s)
    return s


class decodeurl:
    """Decoded WWW-kom URL.

    Usage:  x = decodeurl("/some/url")
    Throws not_a_komurl, or returns an object with the following members:
	pers_no:  a number
	cmd:      the command -- if the url /wwwkom/foo?bar is
		  entered, cmd will be set to foo.
        arglist:  a dict-like object (often a cgi.SvFormContentDict)
		  containing all the arguments in the URL.  Don't
		  access this!  Use one of the following methods
		  instead:

		  getnum(key)	Return the value as an integer.  Returs
		  		None if the key was not found, or the
		  		value was not an integer, or the key
		  		was present more than once.

		  getnumlist(key) Return a (possibly empty) list of
		  		all values that has the corresponding
		  		key.  Raises an exception if any value
		  		isn't numeric.

		  getstr_empty(key) Return the value as a string, or
		  		the empty string if the key isn't
		  		present.  Unknown behaviour if the key
		  		is present more than once.

		  getstr(key)	Return the value as a string, or None
		  		if the key isn't present.  Unknown
		  		behaviour if the key is present more
		  		than once.

                  getstrlist(key) Return a (possibly empty) list of
		                all values that has the corresponding
                                key.
    
    There are a number of methods that can be called to access
    arguments.
    """

    def __init__(self, url):
	# Assertion: url is on the format "/wwwkom/(4)/cmd?args"
	# or "/wwwkom/cmd?args" where the "?args" part is optional.
	u = string.split(url, '/')
	if len(u) < 3 or len(u[2]) < 1 or u[0] != '' or u[1] != 'wwwkom':
	    raise not_a_komurl, "Too short or bad: " + `u`
	u[0:2] = []

	# Check for a person number.
	if u[0][0] == '(' and u[0][-1] == ')':
	    if len(u[0]) < 3:
		raise not_a_komurl, "Empty session: " + `u[0]`
	    try:
		self.pers_no = string.atoi(u[0][1:-1])
	    except ValueError:
		raise not_a_komurl, "Bad session: " + `u[0]`
	    u[0:1] = []
	else:
	    self.pers_no = None

	if len(u) != 1:
	    raise not_a_komurl, "Expected a single command, got " + `u`

	# Take care of arguments, storing them on arglist.
	arglist=string.split(u[0], "?")
	if len(arglist) == 1:
	    self.cmd = arglist[0]
	    self.rawarglist = ""
	    self.arglist = {}
	elif len(arglist) == 2:
	    self.cmd = arglist[0]
	    self.rawarglist = arglist[1]
	    self.arglist = form_data(arglist[1])
	else:
	    raise not_a_komurl, "Multiple question marks in arglist: " + u[0]

    def as_string(self, want_session = 0):
	return hostpart + self.as_string_sans_hostpart(want_session)

    def as_string_with_session(self):
	return self.as_string(1)

    def as_string_sans_hostpart(self, want_session = 0):
	res = "/wwwkom/"
	if self.pers_no and want_session:
	    res = res + "(" + `self.pers_no` + ")/"
	res = res + self.cmd
	if self.rawarglist:
	    res = res + "?" + self.rawarglist
	return res

    def as_string_sans_hostpart_with_session(self):
	return self.as_string_sans_hostpart(1)

    def getnum(self, key):
	try:
	    x = string.atoi(self.arglist[key])
	    log.debug("getnum(" + key + ") returns " + `x`)
	    return x
	except:
	    return None

    def getnumlist(self, key):
	# FIXME: what should be done if string.atoi fails?
	if self.arglist.has_key(key):
	    return map(string.atoi, self.arglist.getlist(key))
	else:
	    return []

    def getstr_empty(self, key):
	try:
	    return self.arglist[key]
	except KeyError:
	    return ""

    def getstr(self, key):
	try:
	    return self.arglist[key]
	except KeyError:
	    return None

    def getstrlist(self, key):
	if self.arglist.has_key(key):
	    return self.arglist.getlist(key)
	else:
	    return []

def body_as_html_pre(s):
    return "<pre>" + htmlquote(s) + "</pre>"

def body_as_html_heuristic(s):
    s = string.expandtabs(s, 8)
    # A list of paragraphs.  Each entry is a tuple witht the fields
    # (blank, [lines], [lens], [indent]).  Blank is the number of
    # blank lines that follows the paragraph (normally 1).
    paragraphs = []
    # Lines in the current paragraph.  Any indent is removed before it
    # is stored here.
    lines = []
    lens = []
    indents = []
    blank = 0
    for line in string.split(s, '\n'):
	if re.match(" *$", line) is None:
	    # Non-empty line.  Store it in lines for later processing.
	    if blank > 0 and len(lines) > 0:
		# This is the first line of a paragraph.  Store the
		# previous paragraph.
		paragraphs.append((blank, lines, lens, indents))
		lines = []
		lens = []
		indents = []
		blank = 0
	    sans_indent = line.lstrip()
	    lines.append(sans_indent)
	    lens.append(len(line))
	    indents.append(len(line) - len(sans_indent))
	else:	    
	    # An empty line terminates the paragraph; watch out for
	    # multiple blank lines
	    blank = blank + 1
    # Don't forget the last paragraph
    if len(lines) > 0:
	paragraphs.append((0, lines, lens, indents))

    # Which indent levels are used?
    usedindents = {}
    for (blank, lines, lens, indents) in paragraphs:
	for i in indents:
	    usedindents[i] = 1
    usedindents = usedindents.keys()
    usedindents.sort()
    log.debug("Used indents: " + `usedindents`)

    # Format each paragraph separately, storing the result in res.
    res = []
    for (blank, lines, lens, indents) in paragraphs:

	# Find out how to format this paragraph.

	mini = min(indents)
	maxi = max(indents)
	pre = 0
	br = 0
	if len(lens) > 1:
	    minll = min(lens[:-1])
	    maxll = max(lens[:-1])
	else:
	    minll = None
	    maxll = None
	# Use preformatted text if the indent level differs within the
	# paragraph.  This is good for C-code and ASCII graphics.
	# Actually, br-style formatting might be better for C-code,
	# but this is definitely the right choice for ASCII graphics.
	# FIXME: if this selects pre or br should be configurable.
	if maxi != mini > 0:
	    pre = 1
	# Use preformatted if this is a "tegelsten".
	if len(lines) > 4 and minll == maxll:
	    pre = 1
	# Use br if any line (except the last one) is shorter than 50
	# characters.
	if len(lens) > 1 and minll < 50:
	    br = 1
	# Use pre if any line contains three spaces in a row (not
	# counting the indent).
	if not pre:
	    for l in lines:
		if re.search("   ", l) is not None:
		    pre = 1
		    break

	# Use br if the lines start with the same character (which
	# probably means that the text is included).
	if not br and not pre and len(lines) > 1:
	    startchar = lines[0][0]
	    for l in lines:
		if l[0] != startchar:
		    startchar = None
		    break
	    if startchar != None:
		br = 1

	log.debug("Indent levels: " + `indents`)
	# Actually format this paragraph.
	if pre or br:
	    # The plan was initially to use <pre> for this type of
	    # formatting, but since the </pre> tag adds an extra blank
	    # line we use <tt></tt> and lots of &nbsp; instead.  Now,
	    # pre and br is actually almost the same thing.  The only
	    # difference is that <tt> is not emitted in br-mode.
	    current_indent = 0
	    in_tt = 0
	    need_br = 0
	    for (l, indent) in map(None, lines, indents):
		log.debug("Emitting line with indent " + `indent`)
		indent = usedindents.index(indent)
		log.debug("(translates to dl-indent " + `indent` + ")")
		if indent != current_indent:
		    if in_tt:
			res.append("</tt>\n")
			in_tt = 0
		    if indent < current_indent:
			res.append("</dl>" * (current_indent - indent))
		    else:
			res.append("<dl><dd>" * (indent - current_indent))
		    current_indent = indent
		    need_br = 0
		if pre and not in_tt:
		    res.append("<tt>")
		    in_tt = 1
		if need_br:
		    res.append("<br>\n")
		if pre:
		    res.append(detect_url(re.sub(' ', '&nbsp;', htmlquote(l))))
		else:
		    res.append(detect_url(htmlquote(l)))
		need_br = 1
	    if in_tt:
		res.append("</tt>\n")
	    if current_indent > 0:
		res.append("</dl>" * current_indent)
	else:
	    res.append("<dl><dd>" * usedindents.index(mini))
	    for l in lines:
		res.append(detect_url(htmlquote(l)))
		res.append("\n")
	    res.append("</dl>" * usedindents.index(mini))
	res.append("<p>\n" * blank)
    return string.joinfields(res, "")

class form_data(cgi.SvFormContentDict):
    def __init__(self, arg):
	self.data = self.dict = cgi.parse_qs(arg)

size_map_cache = None
def size_map(img):
    global size_map_cache

    if size_map_cache == None:
	f = open("../imgsize.pickle", "r")
	size_map_cache = pickle.load(f)
	f.close()

    try:
	return size_map_cache[img]
    except KeyError:
	return size_map_cache[img + ".gif"]

def iconref(iconname, alt, mode=None, enabled=1):
    if mode == None:
	mode = iconmode

    if enabled == 0:
	iconname = iconname + "-d"

    if mode == "traditional":
	file = iconname + "-sv.gif"
    elif mode == "sillyicons" or mode == "img":
	file = iconname + ".gif"
    elif mode == "textonly":
	return alt
    elif mode == "July 1997-sv":
	file = iconname + "-sv-j.gif"
    else:
	# Default fallback
	file = iconname + "-sv.gif"
    (x, y) = size_map(file)
    return ('<img src="/' + file
	    + ('" border=0 width=%d height=%d alt="' % (x, y))
	    + alt + '">')

def icon(file, alt, link, explanation = None, mode = None):
    a = anchor(link, iconref(file, alt, mode), explanation)
    if iconmode == "textonly":
	return " [ " + a + " ] "
    else:
	return a
