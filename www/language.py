# Copyright (C) 1996  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import string
import time
import komhtml
import log

# Set by sc and lm.  This module uses toolbar.at_top(),
# toolbar.at_bottom() and similar methods to extract the tool bar.
toolbar = None

TEXT_FORMAT = """%(creation_date)s (text %(text_no)d) %(author)s<br>
%(recipients)s%(uplinks)s%(marks)s
�rende: <b>%(subject)s</b>
<hr noshade>
%(body)s
<hr noshade>
%(footnotes)s%(comments)s
"""

TEXT_FORMAT_JULY_1997 = """\
<table cellpadding=3 cellspacing=0 border=0 width="100%%">
<tr bgcolor="#ccffff">
 <td valign=top>F�rfattare: %(author)s</td>
 <td align=right>(%(creation_date)s)</td>
</tr>
<tr bgcolor="#ccffff">
 <td>�rende: <b>%(subject)s</b></td>
 <td align=right>(Text: %(text_no)d)</td>
</tr>
<tr bgcolor="#ccffff">
 <td colspan=2>%(uplinks)s%(recipients)s%(marks)s</td>
</tr>
<tr bgcolor="#ffffff">
 <td colspan=2><hr></td>
</tr>
<tr bgcolor="#FFFFFF">
 <td colspan=2>%(body)s</td>
</tr>
<tr bgcolor="#ffffff">
 <td colspan=2><hr></td>
</tr>
<tr bgcolor="#ccffff">
 <td colspan=2>%(footnotes)s%(comments)s</td>
</tr>
</table>
"""

CONVERSION_FORMAT = """Konvertering av text:<br>
<input type="radio" name="conversion"
    value="swascii" %(select_swascii)s>fr�n swascii<br>
<input type="radio" name="conversion"
    value="none" %(select_none)s>ingen<br>"""

WRITE_COMMENT_FORMAT = """Kommentera text %(parent_link)s %(parent_author)s
%(form)s
"""

WRITE_PERSONAL_ANSWER_FORMAT = """Skicka personligt svar till
%(parent_link)s %(parent_author)s
%(form)s
"""

WRITE_TEXT_FORMAT = """Skriva ett nytt inl�gg i %(conf-name)s.
%(form)s
"""

WRITE_PRESENTATION_FORMAT = """Skriva en ny presentation av %(conf-name)s.
%(form)s
"""

WRITE_TEXT_FORM = """

<p><form action="submit_text" method="get">
<input type="hidden" name="known_comments" value="%(known_comments)s">
%(conversion)s
%(presentations)s
%(recipients)s
%(comments)s

<p><input type="submit" value="%(submit_text)s">

<p><input type="text" name="subject" value="%(subject)s" size=70>
<textarea name="body" rows="15" cols="70" wrap="hard">%(body)s</textarea>

<p><input type="submit" value="%(submit_text)s">
</form>
%(comments_inline)s
"""

COMMENTING_FORM = """

<p>%(lead)s<br>
<form><textarea name="body" rows="10" cols="70" wrap="hard"
>%(body)s</textarea>
"""

REVIEW_TEXTS_FORM = """\

<p>
<form action="review_texts2" method="get">
�terse texter i m�tet %(confname)s.
<p>
Antal inl�gg:
<input type="text" name="num_texts" size=8><br>
<p>
<input type="submit" value="�ndra antal ol�sta">
<input type="hidden" name="conf_no" value="%(conf_no)d">
</form>
"""


MOVE_TEXT_FORM="""
Flytta text nummer %(text-no)s.

%(error)s

<p>
<form action="move_text2" method="get">
<b>Kryssa i de mottagare du vill ta bort:<b><br>
<table border=1>
%(recipients)s
</table>


<p>
<b>Vilket m�te skall texten flyttas till?</b><br>
<input type="text" name="new_recipient" size=50 value=""><br>
<input type="hidden" name="text_no" value="%(text-no)s"><br>
<input type="submit" value="Flytta texten">
</form>
"""


SET_UNREAD_FORM = """
<form action="set_unread" method="get">
H�r kan du �ndra antalet ol�sta texter i %(name_nolink)s.  Du kan
v�lja att bara l�sa inl�gg som skrivits de senaste dagarna, eller att
bara l�sa de senaste inl�ggen.
<p>
Antal inl�gg: <input type="text" name="unread_texts" size=8><br>
Antal dagar: <input type="text" name="unread_days" size=8>
<input type="submit" value="�ndra antal ol�sta">
<input type="hidden" name="conf_no" value="%(conf_no)d">
</form>
"""

CONFIGURATION_FORM = """

P� den h�r sidan kan du st�lla in hur du vill att WWW-kom ska fungera.
Med tiden kommer fler och fler saker att g� att p�verka h�r.  De
inst�llningar du g�r sparas mellan sessionerna.

<form action="submit_configuration" method="get">

<h3>Bakgrundsf�rg</h3>

Vill du att WWW-kom ska s�tta bakgrundsf�rg p� sidorna?<br>

<input type="radio" name="color" value="default" %(color-default)s>
Ja, anv�nd bakgrundsf�rg f�r att �ka tydligheten.<br>

<input type="radio" name="color" value="none" %(color-none)s>
Nej, l�t min browser best�mma f�rgen.<br>

<h3>Konvertering swascii -> ISO 8859-1</h3>

De svenska tecknen "�", "�" och "�" kan vara sv�ra att mata in fr�n
viss utrustning.  Om du har l�ttare att mata in "}", "{" och "|" s�
kan WWW-kom konvertera den text du skriver �t dig n�r du skapar
inl�gg.  T�nk p� att om du sl�r p� den h�r konverteringen s� blir det
om�jligt f�r dig att skriva f�ljande tecken: "}{|][\\".

<p>Vill du att WWW-kom ska konvertera texter du skriver �t dig?<br>

<input type="radio" name="entry-conversion" value="off" %(ec-off)s>
Nej, konvertera inte.<br>

<input type="radio" name="entry-conversion" value="ask-off" %(ec-ask-off)s>
Konvertera inte, men ge m�jlighet att sl� p� konvertering varje g�ng
en text skrivs.<br>

<input type="radio" name="entry-conversion" value="ask-on" %(ec-ask-on)s>
Konvertera, men ge m�jlighet att sl� av konvertering varje g�ng
en text skrivs.<br>

<input type="radio" name="entry-conversion" value="on" %(ec-on)s>
Ja, konvertera alltid.

<h3>Utseende</h3>

Det finns n�gra olika utseenden du kan v�lja mellan.
Defaultalternativet kr�ver att din browser kan hantera tabeller.  De
andra alternativen �r inte lika kr�vande.

<p>WWW-kom kan ha fyra olika utseenden.  Vilket tycker du b�st om?<br>

<input type="radio" name="looknfeel" value="default" %(lnf-default-checked)s>
Navigeringsverktyg till v�nster, rubrik �verst, och defaultknapp
l�ngst till h�ger.<br>

<input type="radio" name="looknfeel" value="traditional"
%(lnf-traditional-checked)s>Navigeringslist �verst p� sidan med enkla
knappar med text p�, som ser ut s� h�r: %(lnf-default-demo)s<br>

<input type="radio" name="looknfeel" value="textonly" %(lnf-text-checked)s>
Navigeringslist med enbart text.  Motsvarande knapp ser ut s� h�r:
%(lnf-text-demo)s<br>

<input type="radio" name="looknfeel" value="sillyicons" %(lnf-silly-checked)s>
Navigeringslist med f�rgglada, men ack s� sv�rbegripliga, ikoner:
%(lnf-silly-demo)s<br>

<h3>Begr�nsa antal m�ten</h3>

N�r du v�ljer %(home-demo)s fr�n navigerinslisten visas normalt en
�versiktssida med alla m�ten d�r du har ol�sta texter.  Om det �r
m�nga m�ten kan det ta tid att f� fram hela sidan.  H�r kan du
s�tta en maxstorlek p� hur stor listan ska f� vara.  Om det egentligen
finns fler m�ten med nyheter visas en text l�ngst ner som ber�ttar
det.  Allt eftersom du l�ser inl�ggen kommer nya m�ten att visas i
listningen, tills du har f�rre ol�sta �n den gr�ns du s�tter h�r.

<p>Om f�ltet �r blankt eller "0" kommer det inte att finnas n�gon
begr�nsning.<br>

Gr�ns: <input type="text" name="home-max-confs"
	size="10" value=\"%(home-max-confs)s">

<h3>Klickbara f�rfattare</h3>

N�r du v�ljer %(conf-demo)s fr�n navigerinslisten visas en
�versiktssida med de ol�sta texter som finns i m�tet.  Om man klickar
p� textnumret eller �renderaden f�r en viss text f�r man se den
texten.  N�r "klickbara f�rfattare" �r p� slaget kan man dessutom
klicka p� f�rfattarna p� den sidan.  Om man g�r det f�r man upp en
statussida f�r f�rfattaren.<br>

<input type="radio" name="conf-authors-linked" value="off" %(cal-off-checked)s>
Ej klickbara f�rfattare.
<input type="radio" name="conf-authors-linked" value="on" %(cal-on-checked)s>
Klickbara f�rfattare.

<h3>Nedflyttade �renderader</h3>

Ofta har en kommentar till ett inl�gg samma �rende som inl�gget den
kommenterar.  P� text�versiktssidan skrivs normalt bara �rendet f�r
den f�rsta texten med samma �rende ut, och resten ers�tts med "--" som
ett sorts nedflyttningstecken.<br>

<input type="radio" name="conf-compact-duplicates" value="on" %(ccd-on-checked)s>
Nedflyttningstecken.
<input type="radio" name="conf-compact-duplicates" value="off" %(ccd-off-checked)s>
�terupprepa �renderaden.

<p><input type="submit" value="Spara inst�llningarna">
</form>
"""

WRITE_TEXT_NEW_COMMENTS = """\
Sedan du b�rjade f�rfatta din kommentar har det skapats minst en
ny kommentar.  �gna igenom de nya texterna nedan.  Om du fortfarande
tycker att din kommentar �r relevant kan du klicka p�
"Skapa kommentaren"-knappen (l�ngst ner p� sidan) f�r att skicka in
texten igen.  Om du m�rker att n�gon annan redan har sagt det du hade
t�nkt s�ga �r det kanske ingen st�rre vits med att du upprepar samma
sak en g�ng till.  I s� fall kan du g� tillbaks och forts�tta l�sa.
<p>

<hr noshade><blockquote>
%(new_texts)s
</blockquote><hr>
%(form)s
"""

KILLED = """\
Din sessionshanterare �r nu avslutad.  Du �r utloggad ur LysKOM.
Observera dock att din WWW-klient troligtvis fortfarande inneh�ller
ditt LysKOM-anv�ndarnamn och l�senord.  F�r att s�kerst�lla att
ingen obeh�rigt anv�nder LysKOM i ditt namn b�r du starta om din
WWW-klient innan du l�mnar datorn.

<p>Du kan starta en ny session genom att v�lja n�got av alternativen ovan.
"""

SELECT_CONF_FORM = """\
<form action="submit_select_conf" method=get>
<b>Namn:</b> <input type="text" name="conf_name" size=50>
<input type="submit" value="S�k">
<p>
<b>Vad vill du s�ka efter?</b><br>
<input type="radio" name="domain" value="conf" checked>Enbart m�ten<br>
<input type="radio" name="domain" value="pers">Enbart personer<br>
<input type="radio" name="domain" value="both">B�de personer och m�ten<br>
</form>

<p>I rutan efter "Namn" kan du fylla i ett m�tes- eller personnamn.
Du kan f�rkorta namnet genom att utel�mna ord p� slutet, och genom att
utel�mna bokst�ver p� slutet i ord.  Om mer �n ett m�te matchar det
f�rkortade m�tesnamnet du angivit kommer du att f� upp en lista med
dem, d�r du kan v�lja r�tt.

<p>Om du l�mnar rutan tom kommer alla m�ten att returneras.  Det kan
ta ett bra tag.

<p>Du kan v�lja om du vill s�ka bland m�ten, personer eller b�de och.
"""


CREATE_CONF_FORM = """\

Du h�ller p� att skapa ett nytt m�te.  Du b�r ge m�tet ett s�
beskrivande namn som m�jligt.  Delar av namnet som skrivs inom
parentes kommer inte att matchas n�r man sl�r upp m�tesnamnet, s� det
kan vara l�mpligt att s�tta ord som "f�r" och "att" inom parentes.
Det f�rsta ordet b�r vara beskrivande f�r m�tet.

<form action="submit_create_conf" method="get">
<b>Namn:</b> <input type="text" name="conf_name" size=50><br>

<b>Ska m�tet vara �ppet?</b><br>
<input type="radio" name="prot" value="0" checked
>Ja, vem som helst f�r bli medlem.<br>
<input type="radio" name="prot" value="1"
>Nej, organisat�ren sl�pper in nya medlemmar.<p>

<b>Ska man f� kommentera inl�gg?</b><br>
<input type="radio" name="orig" value="0" checked
>Ja.<br>
<input type="radio" name="orig" value="1"
>Nej.  Kommentarer skickas till �verm�tet.<p>

<b>Ska m�tet vara hemligt?</b><br>
<input type="radio" name="secr" value="1" 
>Ja.  Bara medlemmar f�r se m�tet.<br>
<input type="radio" name="secr" value="0" checked
>Nej.<p>

<b>Ska m�tet till�ta anonyma inl�gg?</b><br>
<input type="radio" name="allow_anon" value="1" 
checked>Ja.  Anonyma inl�gg �r till�tna.<br>
<input type="radio" name="allow_anon" value="0"
>Nej.<p>

<b>F�r man vara hemlig medlem i m�tet?</b><br>
<input type="radio" name="forbid_secret" value="0" 
checked>Ja.  Hemliga medlemmar �r till�tna.<br>
<input type="radio" name="forbid_secret" value="1"
>Nej.<p>

<b>Begr�nsning av mailimport</b><br>
<input type="checkbox" name="refuse_import" value="all">
F�rbjud all import.<br>
<input type="checkbox" name="refuse_import" value="spam">
Importera inte mail som klassats som spam.<br>
<input type="checkbox" name="refuse_import" value="html">
Importera inte mail som �r i HTML-format.<br><p>

<input type="submit" value="Skapa m�tet">
</form>

"""

ADMIN_CONF_FORM = """\

Administration av m�te %(conf-no)s: %(conf-name)s

<form action="submit_admin_conf" method="get">
<b>Nytt namn:</b><br>
<input type="text" name="conf_name" size=50 value="%(conf-name)s"><br>
<input type="hidden" name="conf_no" value="%(conf-no)s"><br>

<b>Ska m�tet vara �ppet eller slutet?</b><br>
<input type="radio" name="prot" value="0" %(unprotected-checked)s>
  �ppet - vem som helst f�r bli medlem.<br>
<input type="radio" name="prot" value="1" %(protected-checked)s>
  Slutet - organisat�ren sl�pper in nya medlemmar.<p>

<b>Ska man f� kommentera inl�gg?</b><br>
<input type="radio" name="orig" value="0" %(non-orig-checked)s>
  Ja.<br>
<input type="radio" name="orig" value="1" %(orig-checked)s>
  Nej.  Kommentarer skickas till �verm�tet.<p>

<b>Ska m�tet vara hemligt?</b><br>
<input type="radio" name="secr" value="1" %(secret-checked)s>
  Ja.  Bara medlemmar f�r se m�tet.<br>
<input type="radio" name="secr" value="0" %(nonsecret-checked)s>
  Nej, vem som helst kan se m�tet.<p>

<input type="submit" value="�ndra m�tet">
</form>

<hr>
<form action="submit_admin_members" method="get">

<input type="hidden" name="conf_no" value="%(conf-no)s"><br>
<input type="submit" value="�ndra medlemmar">

<p>
<b>L�gg till f�ljande person som medlem i m�tet:</b><br>
<input type="text" name="new_member" size=50"><br>
<p>

<b>Uteslut f�ljande medlemmar i m�tet:</b><br>
<table border=1>
%(members)s
</table>
<p>

<input type="submit" value="�ndra medlemmar">
</form>

"""

SUBMIT_ADMIN_MEMBERS_FORM = """\

Administration av medlemmar i m�te %(conf-no)s: (%(conf-name)s)

<p>
<b>F�ljande person �r nu medlem i m�tet:</b><br>
%(new-member)s
<p>

<b>f�ljande medlemmar �r nu uteslutna ur m�tet:</b><br>
%(del-members)s
<p>


"""

CONF = """\
%(set-unread-form)s
%(unread)s
%(truncated)s
<p>
%(estim-unread)s

"""


CONF_STAT = """\
%(become-member)s
%(goto-conference)s
%(set-unread-form)s
%(estimated-unread)s
%(motd)s
%(presentation)s
%(change-presentation)s
%(disjoin-conference)s
%(info-fields)s
"""

CREATE_USER_FORM = """\

Du h�ller p� att skapa en ny anv�ndaridentitet.  F�r att skapa maximal
klarhet, b�r du l�ta namnet I din organisation ing�; du kan till
exempel ange <emph>Eskil Block, FOA</emph>.  Om du vill s� kan du
senare byta namn p� din anv�ndaridentitet om du t ex skulle byta
organisation.

<p>
<hrule>
<form action="submit_create_user" method=get>
Anv�ndarens namn: <input type="text" name="username" size=50><br>
Ange �nskat l�senord: <input type="password" name="password" size=10><br>
L�sen en g�ng till: <input type="password" name="passcheck" size=10>
(f�r kontroll)
<p>
<input type="submit" value="Skapa anv�ndaren">
<input type="reset" value="T�m formul�r">
</form>


"""


JULY_1997_PAGE = """\
<html><head><title>%(title)s</title></head>
%(body_tag)s
<table cellpadding=0 cellspacing=0 border=0>
 <tr>
  %(toolbar)s
 <td width=12><img src="/fill.gif" alt=""></td>
 <td valign=top width="100%%">
  <table width="100%%" cellpadding=3 cellspacing=0 border=0>
   <tr%(headcolor)s>
    <td width="100%%"><font size=+2>%(position)s</font></td>
    <td valign=top rowspan=2 align=right>%(default)s</td>
   </tr>
   <tr%(headcolor)s>
    <td>%(subtitle)s</td>
   </tr>
   <tr><td colspan=2><br></td></tr>
   <tr%(white)s>
    <td colspan=2>
     %(header)s%(body)s%(textops)s
    </td>
   </tr>
  </table>
 </td>
 </tr>
</table>
<hr noshade>
<table>
 <tr>
  <td>%(siglogo)s%(wwwlogo)s</td>
  <td><font size=-1>Sidan genererad %(time)s av WWW-kom.</font>
      <br>WWW-kom. Copyright � 1996-1997 Signum Support AB.</td>
 </tr>
</table>
</body></html>"""

class pages:
    def __init__(self):
	self.use_color = 1
	self.july_1997_layout = 1

    def page(self, title, header, body, position=None, subtitle="&nbsp;"):
	global toolbar

	if self.july_1997_layout:
	    if self.use_color:
		body_tag = '<body bgcolor="#fafad7" text="#000000" ' \
			   'link="#0000ee" vlink="551a8b" alink="ff0000">'
		headcolor=' bgcolor="#CCCCFF"'
		white    =' bgcolor="#ffffff"'
	    else:
		body_tag = "<body>"
		headcolor = ""
		white = ""

            if toolbar is None:
                left_toolbar = ''
                right_toolbar = ''
                textops = ''
            else:
                left_toolbar = toolbar.table_element()
                right_toolbar = toolbar.default_element()
                textops = toolbar.text_ops()

	    # "position" is the text to print in the head as a navigation help.
	    # "title" appears in the title.
	    # "header" should appear on the page, but not if it is
	    # equal to the position.
	    if position == None:
		position = title
	    if header == position:
		header = ""
	    elif header != "":
		header = "<h2>" + header + "</h2>"

	    return JULY_1997_PAGE % {
		"title": self.title(title),
		"position": position,
		"header": header,
		"subtitle": subtitle,
		"toolbar": left_toolbar,
		"default": right_toolbar,
		"textops": textops,
		"body_tag": body_tag,
		"white":white,
		"body": body,
		"headcolor": headcolor,
		"time":self.format_date(time.localtime(time.time())),
		"siglogo":komhtml.icon("signum", "Signum Support AB",
				       "http://www.signum.se/"),
		"wwwlogo":komhtml.icon("wwwkom", "WWW-koms hemsida", "/"),
		}
	    
	if toolbar:
	    top_tool = toolbar.at_top()
	    bot_tool = toolbar.at_bottom()
	    toolbar = None
	else:
	    top_tool = ""
	    bot_tool = ""
	body_tag = "<body>\n"
	if self.use_color:
	    body_tag = ('<body bgcolor="#ffffff" text="#000000"'
			+ ' link="#0000ee" vlink="551a8b" alink="ff0000">\n')
	if header != "":
	    header = "<h2>" + header + "</h2>\n\n"
	return ("<html><head><title>" + self.title(title)
		+ "</title></head>\n"
		+ body_tag
		+ top_tool + "\n"
		+ header + body + bot_tool + "\n\n"
		+ self.stdfoot() + "</body></html>")

    def color_mode(self, mode):
	if mode == "none":
	    self.use_color = 0
	elif mode == "default":
	    self.use_color = 1
	# Silently ignore bad modes.

    def layout_mode(self, mode):
	if mode == "July 1997":
	    self.july_1997_layout = 1
	elif mode == "traditional":
	    self.july_1997_layout = 0
	# Silently ignore bad modes.

    def help(self, key):
	(a, b, c) = self.helpmap[key]
	demo = {
	    "home-demo":komhtml.iconref("home", self.alt_home()),
	    "help-demo":komhtml.iconref("help", self.alt_help()),
	    "conf-demo":komhtml.iconref("selconf", self.alt_conf()),
	    "next-conf-demo":komhtml.iconref("next-conf",
					     self.alt_next_conf("")),
	    "next-text-demo":komhtml.iconref("next-text",
					     self.alt_next_text(0)),
	    "next-comment-demo":komhtml.iconref("next-comment",
						self.alt_next_comment(0)),
	    "next-footnote-demo":komhtml.iconref("next-footnote",
						 self.alt_next_footnote(0)),
	    "time-demo":komhtml.iconref("time", self.alt_see_time()),
	    "answer-demo":komhtml.iconref("answer", self.alt_answer()),
	    "comment-demo":komhtml.iconref("comment", self.alt_comment()),
	    "write-demo":komhtml.iconref("write", self.alt_write_text()),
	    "unformatted-demo":komhtml.iconref("unformatted",
					       self.alt_unformatted()),
	    "formatted-demo":komhtml.iconref("formatted",
					     self.alt_formatted()),
	    "board-demo":komhtml.iconref("board", self.alt_conf()),
	    "home-demo":komhtml.iconref("home", self.alt_home()),
	    "selconf-demo":komhtml.iconref("selconf", self.alt_select_conf()),
	    "create-conf-demo":komhtml.iconref("create-conf",
					       self.alt_create_conf()),
	    "config-demo":komhtml.iconref("config", self.alt_config()),
	    "exit-demo":komhtml.iconref("exit", self.alt_kill())
	    }
		
	return self.page(a, b, c % demo)



SV_HELPMAP = {
    "home":

    ("Hj�lp f�r m�tes�versikt", "M�tes�versikt", """

    Den h�r �versiktssidan �r normalt den f�rsta sidan man kommer till
    n�r man loggar in.  Man kan n�r som helst �terv�nda till den genom
    att klicka p� %(home-demo)s i <a
    href="toolbar">navigeringslisten</a>.

    <p>Listan visar en uppskattning av antalet ol�sta texter och
    namnet p� m�tet.  Det kan h�nda att antalet texter inte st�mmer
    exakt.  Texter som har mer �n en mottagare r�knas dessutom i alla
    m�ten de har som mottagare, s� det kan h�nda att totalsumman �r
    betydligt st�rre �n det verkliga antalet ol�sta inl�gg.

    <p>F�r tillf�llet visas m�tena i stort sett i slumpm�ssig ordning.
    P� sikt finns planer p� att fixa s� att m�tena alltid listas i en
    ordning som du sj�lv kan st�lla in.

    <p>Se �ven:
    <ul>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
    """),

    "login":

    ("Hj�lp f�r inloggningssidan", "Inloggning", """

    N�r du loggar in kommer du i princip till samma sida som <a
    href="home">m�tes�versikten</a>.  L�s om dess hj�lptext i st�llet.

    """),

    "toolbar":

    ("Hj�lp f�r navigeringslisten", "Navigeringslisten", """

    �verst p� varje sida finns en navigeringslist.  Exakt vad den
    inneh�ller varierar fr�n sida till sida eftersom man bara kan g�ra
    vissa saker i vissa l�gen.  En del kommandon som visserligen �r
    anv�ndbara j�mnt, men som man s�llan anv�nder, kan bara kommas �t
    fr�n �versiktssidan.
	      
    <p>H�r �r en kort f�rklaring av alla de knappar som f�rekommer i
    navigeringslisten.  Ikonerna �r l�nkade till hj�lpsidan f�r den
    sida du normalt kommer till n�r du anv�nder ikonen.

    <dl>

    <dt><a href="conf">%(next-conf-demo)s</a>

    <dd>G� till n�sta m�te.  Den h�r ikonen visas n�r du inte har
    n�gra ol�sta texter kvar i ditt nuvarande m�te, eller om du precis
    har loggat in och inte �r n�rvarande i n�got m�te.

    <dt><a href="text">%(next-text-demo)s</a>

    <dd>Visa n�sta ol�sta text i aktuellt m�te.

    <dt><a href="text">%(next-comment-demo)s</a>

    <dd>Visa n�sta kommentar.  N�r du l�ser en text kommer WWW-kom
    ih�g alla kommentarer och fotnoter till texten, och ser till att
    du f�r l�sa dem innan du g�r vidare med texter som h�r till andra
    kommentarskedjor.

    <dt><a href="text">%(next-footnote-demo)s</a>

    <dd>Visa n�sta fotnot.  Fotnoter till inl�gg visas f�re
    kommentarer, �ven om fotnoten �r skriven efter kommentarerna.

    <dt><a href="time">%(time-demo)s</a>

    <dd>Ber�tta vad klockan �r.  Det h�r alternativet dyker upp n�r du
    har l�st slut p� alla texter.  Egentligen �r det kanske inte s�
    meningsfullt, men n�got default-kommando ska det ju finnas, och
    traditionellt har KOM-system ofta visat tiden n�r det inte funnits
    n�got annat att g�ra.

    <dt><a href="comment">%(comment-demo)s</a>

    <dd>Kommentera inl�gget.  Du f�r upp ett formul�r d�r du kan
    skriva in din text.  Den text du skapar kommer att vara en
    kommentar till texten som just nu visas.

    <dt><a href="answer">%(answer-demo)s</a>

    <dd>Svara personligt p� et tinl�gg.  Du f�r upp ett formul�r d�r
    du kan skriva in din text.  Den text du skapar kommer att vara en
    kommentar till texten som just nu visas, och bara du och
    f�rfattaren till texten som visas kommer att kunna se den.

    <dt><a href="write_text">%(write-demo)s</a>

    <dd>Skriva ett inl�gg i m�tet.  Du f�r upp ett formul�r d�r du kan
    skriva in din text.

    <dt><a href="text">%(unformatted-demo)s</a>

    <dd>LysKOM �r i grund och botten textbaserat.  Inl�ggen
    formatteras av f�rfattaren n�r han skriver dem.  De flesta andra
    klienter till LysKOM visar inl�gg med en fixbreddsfont.  WWW-kom
    f�rs�ker d�remot formattera om texten s� att den ska passa i en
    Web-browser.  Ibland gissar den fel p� vad f�rfattaren har menat.
    Genom att klicka p� den h�r ikonen formatteras texen om s� att den
    visas med en fixbreddsfont p� samma s�tt som LysKOM-anv�ndare som
    anv�nder andra klienter ser den.

    <dt><a href="text">%(formatted-demo)s</a>

    <dd>Anv�nd heuristiken som det talas om ovan f�r att visa inl�gget
    p� ett snyggare s�tt.

    <dt><a href="conf">%(board-demo)s</a>

    <dd>Visa en �versikt �ver de ol�sta texter som finns i nuvarande
    m�te.

    <dt><a href="home">%(home-demo)s</a>

    <dd>Visa en �versikt �ver de m�ten d�r du har ol�sta inl�gg.

    <dt><a href="select_conf_form">%(selconf-demo)s</a>

    <dd>Ta fram ett formul�r d�r man kan leta reda p� ett m�te genom
    att s�ka p� m�tesnamn.

    <dt><a href="create_conf_form">%(create-conf-demo)s</a>

    <dd>Ta fram ett formul�r d�r man kan skapa ett m�te.

    <dt><a href="configuration">%(config-demo)s</a>

    <dd>G�r personliga inst�llningar av WWW-koms utseende och
    beteende.

    <dt><a href="kill">%(exit-demo)s</a>

    <dd>Avsluta sessionen.

    <dt>%(help-demo)s

    <dd>F� upp en hj�lpsida som beskriver den sida som visas.  Fr�n
    hj�lpsidan kan man �ven f� upp en inneh�llsf�rteckning �ver all
    hj�lp som finns att f�.
    
    </dl>
     """),

    "text":

    ("Hj�lp f�r inl�ggsvisning", "Inl�gg", """

    Den h�r sidan visar ett inl�gg.  Den f�rsta raden inneh�ller
    tidpunkten n�r inl�gget skrevs (alla tidpunkten anges i
    LysKOM-serverns tidszon), inl�ggets textnummer, och namnet p�
    f�rfattaren.  Om du klickar p� f�rfattarens namn kommer du till en
    <a href="conf">statussida</a> som bland annat visar hans
    presentation.  Du kan �ven skicka brev till honom fr�n den sidan.

    <p>Efter den f�rsta raden f�ljer en lista med alla mottagare till
    inl�gget.  Ett inl�gg kan ha b�de m�ten och personer som
    mottagare.  Alla som �r medlem i n�got av m�tena kommer att f�
    l�sa inl�gget automatiskt.  Om n�got av m�tena �r ett �ppet m�te
    kan dessutom vem som helst l�sa texten om de lyckas f�r reda p�
    dess textnummer.  Om man klickar p� en mottagares namn kommer man
    till samma <a href="conf">statussida</a> som n�r man klickar p�
    f�rfattarens namn.  Efter f�rfattarens namn st�r inom klamrar
    (&lt;&gt;) texten ordningsnummer i m�tet.

    <p>Mottagarna kan vara antingen <i>mottagare</i> eller <i>extra
    kopiemottagare</i>.  Den enda skillnaden mellan de tv� begreppen
    �r hur systemet beter sig n�r man kommenterar ett inl�gg.  N�got
    f�renklat kan man s�ga att kopian kommer att skickas till alla
    mottagare, men inte till extra kopiemottagarna.  <a
    href="comment_recip_default">De exakta reglerna</a> �r lite
    komplexare.
    
    <p>Om mottagaren �r en person som har l�st inl�gget kommer en
    extra rad, som kan se ut som "Mottaget: 1996-08-24 05:16:09", att
    skjutas in.  Den visar n�r personen l�ste inl�gget.

    <p>Man kan l�gga till en mottagare i till ett inl�gg i efterhand.
    <!-- FIXME: not implemented --> F�r tillf�llet m�ste man dock
    anv�nda en annan klient �n WWW-kom f�r att g�ra det.  Mottagare
    som lagts till i efterhand markeras genom att de f�r en extra rad
    som ser ut som "S�nt: 1996-09-18 10:42:00".  Om det var n�gon
    annan �n f�rfattaren till inl�gget som s�nde det till m�tet skrivs
    dessutom raden "S�nt av: <i>personnamn</i>" ut.

    <p>Efter mottagarna st�r det vilket inl�gg detta inl�gg �r en
    kommentar eller fotnot till, om inl�gget �r en kommentar eller en
    fotnot.  Man kan klicka p� textnumret f�r att se den kommenterade
    texten.

    <p>Inl�ggets �rende visas sist i huvudet.  Sedan f�ljer sj�lva
    texten, och till sist visas om texten har n�gra kommentarer eller
    fotnoter.

    <p>Se �ven:
    <ul>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
     """),

    "conf":

    ("Hj�lp f�r text�versikt", "Text�versikt", """

    Den h�r sidan visar vilka ol�sta texter du har i ett visst m�te.
    Texterna visas i den ordning du kommer att f� l�sa dem om du
    anv�nder dig av default-valet.  (Om du anv�nder navigeringslist �r
    det det val som finns l�ngst till v�nster.  Om du anv�nder den
    nyare layout som inf�rdes juli 1997 n�r du defaultvalet genom
    pilen l�ngst upp till h�ger).  �verst visas den �ldsta
    texten.  Under den visas, indenterat, alla texter som �r kommentar
    till den, med kommentarer till sig indenterade ytterligare ett
    steg, och s� vidare.  Sedan kommer den �ldsta texten som �nnu inte
    skrivits ut.

    <p>P� <a href="configuration">inst�llningssidan</a> kan du st�lla
    in s� att f�rfattarnamnen blir l�nkar till <a href="conf"
    >statussidan</a> f�r respektive f�rfattare.  Den finessen �r
    normalt avslagen eftersom sidan l�tt blir f�r plottrig om l�nkarna
    �r p�.

    <p>Om det finns fler �n 40 ol�sta inl�gg i m�tet kommer enbart de
    f�rsta 40 inl�ggen att visas.  En textrad om att det finns fler
    ol�sta inl�gg dyker i s� fall upp l�ngst ner p� sidan.  Gr�nsen
    kommer i framtiden att g� att st�lla in via inst�llningssidan.
    <!-- FIXME -->

    <p>Om det finns fler �n 20 ol�sta inl�gg i m�tet kommer sidan att
    inneh�lla ett formul�r d�r du kan best�mma hur m�nga inl�gg du
    vill l�sa.  Med hj�lp av det kan du allts� enkelt hoppa �ver
    inl�gg om det finns fler inl�gg �n du har tid att l�sa.
    Gr�nsen kommer i framtiden att g� att st�lla in via
    inst�llningssidan.  <!-- FIXME -->

    <p>Se �ven:
    <ul>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
     """),

    "conf_stat":

    ("Hj�lp f�r statussidan", "Statussidan", """

    P� statussidan kan du se statusen f�r ett visst m�te eller en viss
    person.  Om du �r organisat�r f�r m�tet eller personen kan du
    dessutom p�verka m�tet/personen.  Du kan dessutom g� med i m�ten,
    skicka brev till m�ten, och en del annat.

    <p>Statussidan kommer inom kort att genomg� en v�lbeh�vlig
    ansiktslyftning.  Mer funktioner kommer att inf�ras.  N�r det �r
    klart kommer den h�r hj�lpsidan att ber�tta mer i detalj vad man
    kan g�ra och vad det inneb�r.  <!-- FIXME-->

    <p>Se �ven:
    <ul>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
    """),

    "write_text":

    ("Hj�lp f�r inl�ggsskrivning", "Skriva inl�gg i m�te", """

    Den h�r sidan hamnar du p� d� du skriver inl�gg i ett visst m�te.

    <p><!-- FIXME -->Den h�r hj�lpsidan kommer att kompletteras
    senare.

    <p>Se �ven:
    <ul>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
    """),
    
    "comment":
    
    ("Hj�lp f�r kommentarsskrivning", "Att kommentera ett inl�gg", """

    Den h�r sidan hamnar du p� d� du kommenterar ett inl�gg.

    <p><!-- FIXME -->Den h�r hj�lpsidan kommer att kompletteras
    senare.

    <p>Se �ven:
    <ul>
    <li><a href="comment_recip_default">Vart skickas en kommentar?</a>
    <li><a href="answer">Personliga svar</a>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
    """),
    
    "answer":
    
    ("Hj�lp f�r svarsskrivning", "Att svara p� ett inl�gg", """

    Den h�r sidan hamnar du p� d� du svarar p� ett inl�gg.  Svaret
    kommer att hamna f�rfattarens och din brevl�da.

    <p><!-- FIXME -->Den h�r hj�lpsidan kommer att kompletteras
    senare.

    <p>Se �ven:
    <ul>
    <li><a href="comment">Att kommentera ett inl�gg</a>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
    """),
    
    "comment_recip_default":

    ("Vart skickas en kommentar?", "Vart skickas en kommentar?", """

    N�r du kommenterar en text �r grundregeln att kommentarer hamnar i
    de m�ten och personer som �r mottagare till inl�gget du
    kommenterar.  M�ten och personer som bara �r extra kopiemottagare
    kommer inte att bli mottagare till det nya inl�gget.

    <p>F�r varje mottagare kontrolleras dessutom om det �r till�tet
    att skriva kommentarer i det m�tet.  Om det inte �r det s�nds
    inl�gget i st�llet till m�tets <i>superm�te</i>, om m�tet har ett
    s�dant.  Om m�tet inte har n�got superm�te kommer den mottagaren
    att ignoreras. (P� <a href="conf">statussidan</a> kan man se om
    ett m�te har ett superm�te).  Det �r till�tet att skriva
    kommentarer i de flesta m�ten, s� f�r det mesta p�verkar det h�r
    stycket inte vart texten skickas.

    <p>Sedan kontrolleras att du har r�ttighet att skriva i m�tet.
    F�r att ha det m�ste du vara medlem i det m�te som p� statussidan
    kallas <i>till�tna f�rfattare</i>.  Om till�tna f�rfattare �r satt
    till 0 inneb�r det att vem som helst f�r skriva inl�gg.  Om du
    inte har r�tt att skriva i m�tet skickas kommentaren till
    superm�tet.  Den h�r kontrollen upprepas tills man n�r ett m�te
    som du har r�tt att skriva i, eller tills systemet tr�ttnar p� att
    leta upp nya superm�ten.  I de flesta m�ten f�r vem som helst
    skriva inl�gg, s� f�r det mesta p�verkar det h�r stycket inte vart
    texten skickas.
    
    <p>Till sist f�rs�ker systemet se till att b�de du och f�rfattaren
    till det inl�gg du kommenterar kommer att f� l�sa det inl�gg du
    skriver och eventuella framtida kommentarer till det.  Det g�rs
    genom att ni b�gge blir mottagare till inl�gget om ni inte �r
    medlem i n�got av de m�ten som redan �r mottagare.

    <p>Alla dessa steg g�rs innan du f�r upp formul�ret d�r du skriver
    din kommentar.  P� den sidan kan du sedan v�lja bort mottagare.
    <!-- FIXME: rewrite when below is implemented --> I framtida
    versioner av WWW-kom kommer du �ven att kunna l�gga till nya
    mottagare d�r.

    <p>Se �ven:
    <ul>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="comment">Kommentera ett inl�gg</a>
    </ul>
    """),

    "set_presentation":

    ("Hj�lp f�r presentations�ndring", "�ndra presentation", """

    <!-- FIXME-->Hj�lptexten f�r den h�r sidan �r inte klar �n.

    <p>Se �ven:
    <ul>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
    """),

    "kill":

    ("Hj�lp f�r avsluta", "Avsluta", """

    <!--FIXME-->Hj�lptexten f�r den h�r sidan �r inte klar �n.

    <p>Se �ven:
    <ul>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
    """),

    "who_is_on":

    ("Hj�lp f�r vilkalistan", "Vilkalistan", """

    H�r kan du se vilka som �r inne i LysKOM just nu.  F�r varje
    person anges dessutom vilket m�te han �r n�rvarande i just nu.

    <p>Siffran l�ngst till v�nster �r ett <i>sessionsnummer</i>.
    Varje ny uppkoppling till servern f�r ett nytt l�pnummer.

    <p>Den h�r sidan kan �ven visa enbart de som �r n�rvarande i ett
    visst m�te.  I s� fall st�r det "N�rvarande i m�te" i st�llet f�r
    "F�ljande personer �r inloggade f�r n�rvarande".

    <p>Se �ven:
    <ul>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
    """),

    "select_conf_form":

    ("Hj�lp f�r m�tesuppslagning", "Leta m�te", """

    Den h�r sidan kan du anv�nda n�r du vill g�ra n�got med ett m�te
    eller en person som inte syns p� <a
    href="home">m�tes�versikten</a>.  Den �r anv�ndbar till exempel
    n�r du vill skicka ett brev eller g� med i ett m�te.

    <p>Du f�r fylla i namnet p� m�tet eller personen du vill g�ra
    n�got med.  Om s�kningen p� namnet ger exakt en tr�ff kommer du
    att hamna p� <a href="conf">statussidan</a> f�r det du s�kte.
    Annars f�r du upp en sida som listar alla m�ten/personer som
    matchar s�km�nstret.

    <p>Matchningen g�rs ord f�r ord.  F�rst tas allt som st�r inom
    parenteser i m�tesnamnen bort.  Sedan delas m�tesnamnen upp i
    ord.  D�refter j�mf�rs varje ord med de ord du matat in.  Du kan
    utel�mna bokst�ver i slutet p� varje ord, och du kan utel�mna ord
    i slutet p� m�tesnamnet.  Matchningen betraktar stora och sm�
    bokst�ver som samma bokstav.

    <p>Ett exempel kanske f�rtydligar matchningen.  S�kstr�ngen "k e"
    matchar till exempel "(Alla) KOM(-system) (-) Erfarenhetsutbyte",
    "Kampsport erfarenhetsutbyte" och "Kent Engstr�m", men inte
    "Lysators KOM-system erfarenhetsutbyte", "Alla KOM-system
    Erfarenhetsutbyte", eller "Kampsport - erfarenhetsutbyte".

    <p>Du kan st�lla in om du vill s�ka efter m�ten, personer eller
    b�de och.

    <p>Se �ven:
    <ul>
    <li><a href="conf">Statussidan</a>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
    """),

    "configuration":

    ("Hj�lp f�r inst�llningar", "Inst�llningar", """

    Du kan sj�lv p�verka hur WWW-kom fungerar.  Bland annat kan du
    v�lja mellan olika utseenden p� ikonerna, om du vill ha
    bakgrundsf�rg eller inte, och hur m�nga m�ten som ska visas p� <a
    href="home">m�tes�versikten</a>.  Sidan inneh�ller utf�rliga
    f�rklaringar som f�rklarar vad varje parameter inneb�r.

    <p>Se �ven:
    <ul>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
    """),

    "create_conf_form":

    ("Hj�lp f�r m�tesskapning", "Skapa m�te", """

    Den h�r sidan anv�nds f�r att skapa m�ten.  Du blir organisat�r
    f�r m�ten som du skapar.  N�r du har skapat m�tet hamnar du p� en
    statussida f�r m�tet.  D�rifr�n kan du skapa en presentation f�r
    m�tet, och du kan �ven g� med i m�tet.

    <!-- FIXME: add more text here -->

    <p>Se �ven:
    <ul>
    <li><a href="index">Inneh�llsf�rteckning</a>
    <li><a href="toolbar">Navigeringslisten</a>
    </ul>
    """),

    "time":

    ("Hj�lp f�r se tiden", "Tiden", """

    Den h�r sidan visar vad klockan �r.  Den visar dels vad
    LysKOM-servern tycker klockan �r, dels vad WWW-kom tycker klockan
    �r.

    """),

    "index":

    # Keep this list in sync with testsuite/lm.3/README.
    ("Inneh�llsf�rteckning", "Inneh�ll", """

    Hj�lp finns om f�ljande �mnen:
    <ul>
    <li><a href="toolbar">Navigeringslisten</a>
    <li><a href="login">Inloggningsbilden</a>
    <li><a href="home">M�tes�versikt -- vilka m�ten finns ol�sta texter i?</a>
    <li><a href="conf">Text�versikt -- vad �r ol�st inom ett m�te?</a>
    <li><a href="text">Inl�ggsvisning</a>
    <li><a href="select_conf_form">Leta m�te</a>
    <li><a href="conf_stat">Statussida f�r m�ten/personer</a>
    <li><a href="write_text">Skriva inl�gg</a>
    <li><a href="comment">Kommentera inl�gg</a>
    <li><a href="answer">Svara personligt p� inl�gg</a>
    <li><a href="comment_recip_default">Vart hamnar kommentarer?</a>
    <li><a href="set_presentation">�ndra presentation</a>
    <li><a href="create_conf_form">Skapa m�te</a>
    <li><a href="kill">Avsluta</a>
    <li><a href="who_is_on">Vem �r inloggad?</a>
    <li><a href="configuration">Inst�llningar</a>
    </ul>
    """)}



class sv_pages(pages):

    helpmap = SV_HELPMAP

    def stdfoot(self):
	return ("<hr noshade><table><tr><td>"
		+ komhtml.icon("wwwkom", "WWW-koms hemsida", "/")
		+ komhtml.icon("signum", "Signum Support AB",
			       "http://www.signum.se/")
		+ '</td><td><font size=-1>Sidan genererad '
		+ self.format_date(time.localtime(time.time()))
		+ ' av WWW-kom.</font><br>\n'
		+ 'WWW-kom. Copyright � 1996 Signum Support AB.</td></tr></table>\n')

    def title(self, t):
	return "LysKOM: " + t

    def ambiguous_login(self, given, matches):
	res=["<ul>"]
	for m in matches:
	    res.append('<li>'
		       + komhtml.anchor(komhtml.komurl("login", m.conf_no()),
					komhtml.htmlquote(m.name())))
	res.append("</ul>\n")
	return self.page("Vem �r du?", "Vem �r du?",
			 "Det namn du angav, \"" + komhtml.htmlquote(given)
			 + "\", kan vara n�gon av f�ljande personer:\n"
			 + string.joinfields(res, "\n"))

    def bad_password_for_session(self, given_user):
	return self.page("Fel l�sen", "Felaktigt l�senord",
			 "Det l�senord du angav st�mmer inte med "
			 + "l�senordet f�r "
			 + given_user + '.  '
			 + komhtml.anchor(komhtml.komurl("login"),
					  "F�rs�k igen."))

    def nonexisting_person_url(self, given_number):
	return self.page("Felaktig URL", "Felaktig URL",
			 "Du har f�ljt en l�nk som inneh�ller en referens till"
			 + " person nummer " + `given_number`
			 + ". Det finns f�r tillf�llet ingen s�dan anv�ndare. "
			 + komhtml.anchor(komhtml.komurl("login"),
					  "F�rs�k igen."))

    def noconnect(self):
	return self.page("Servern svarar inte", "LysKOM-servern svarar inte",
			 "LysKOM-servern kan inte n�s.  Det kan bero p� att "
			 + "servern �r nere eller n�got n�tverksproblem.  "
			 + "F�rs�k igen om en halvtimme.")

    def session_redirect(self, newurl):
	return self.page("Omstyrning", "Omstyrning",
			 "Din WWW-klient tycks inte f�rst� felkod 301. Det "
			 + "inneb�r troligtvis att du inte kan anv�nda den "
			 + "f�r att l�sa LysKOM med WWW-kom, men om du har "
			 + "tur kanske du kan anv�nda WWW-kom genom att "
			 + komhtml.anchor(newurl, "f�lja denna l�nk"))

    def who_is_on(self, wholist):
	header = "<table border=0 cellpadding=3>"
	footer = "</table><p><h3>Sammanlagt " + `len(wholist)` \
		 + " personer</h3>"

	wholiststring = string.join(wholist, "\n")
	return self.page("Inloggade i lyskom",
			 "F�ljande personer �r inloggade f�r n�rvarande",
			 header + wholiststring + footer)

    def who_is_on_conf(self, conf, wholist):
	header = ("F�ljande personer �r just nu n�rvarande i "
		  + conf + "\n\n<table border=0 cellpadding=3>")
	footer = "</table><p><h3>Sammanlagt " + `len(wholist)` \
		 + " personer</h3>"

	wholiststring = string.join(wholist, "\n")
	return self.page("N�rvarande i m�te",
			 "N�rvarande i m�te",
			 header + wholiststring + footer)

    def not_present(self):
	return "Ej n�rvarande i n�got m�te"

    def format_footnote(self, footnote, footnote_author):
	return ("Fotnot i text " + footnote + " " + footnote_author
		+ "<br>\n")

    def format_comment(self, comment, comment_author):
	return ("Kommentar i text " + comment + " " + comment_author
		+ "<br>\n")

    def format_date(self, date):
	return "%04d-%02d-%02d %02d:%02d:%02d" % date[0:6]

    def format_date_sans_time(self, date):
	return "%04d-%02d-%02d" % date[0:3]

    def format_uplink(self, is_footnote, parent_text, parent_author_name,
		      sent_at, sender_name):
	if is_footnote:
	    res = "Fotnot till text "
	else:
	    res = "Kommentar till text "
	res = res + parent_text + " " + parent_author_name + "<br>\n"
	if sent_at:
	    res = (res + "&nbsp;&nbsp;S�nt: " + self.format_date(sent_at)
		   + "<br>\n")
	if sender_name:
	    res = res + "&nbsp;&nbsp;S�nt av: " + sender_name + "<br>\n"
	return res

    def undef_conf(self, conf_no):
	return "Person/m�te nummer " + `conf_no` + " (existerar ej/hemlig)"

    def nonexisting_text(self):
	return " (ej l�sbar av dig)"

    def nonexisting_text_long(self):
	return "Texten finns inte, eller ocks� �r den hemlig."

    def anonymous_author(self):
	return " (anonym f�rfattare)"

    def nonexistent_author(self, pers_no):
	return " (utpl�nad/hemlig f�rfattare nummer " + `pers_no` + ")"

    def by_author(self, author):
	return "av " + author

    def format_recipient(self, is_carbon_copy, name, local_no,
			 rec_time, sent_time, sender_name):
	if is_carbon_copy:
	    res = "Extra kopiemottagare: "
	else:
	    res = "Mottagare: "
	res = res + name + " &lt;" + `local_no` + "&gt;<br>\n"
	if rec_time:
	    res = (res + "&nbsp;&nbsp;Mottaget: " + self.format_date(rec_time)
		   + "<br>\n")
	if sent_time:
	    res = (res + "&nbsp;&nbsp;S�nt: " + self.format_date(sent_time)
		   + "<br>\n")
	if sender_name:
	    res = res + "&nbsp;&nbsp;S�nt av: " + sender_name + "<br>\n"
	return res

    def text_rewrite_map(self, map):
	if map["lines"] == 1:
	    map["lines"] = "1 rad"
	else:
	    map["lines"] = `map["lines"]` + " rader"
	marks = map["num_marks"]
	if marks == 0:
	    map["marks"] = ""
	elif marks == 1:
	    map["marks"] = "Markerad av en person<br>\n"
	else:
	    map["marks"] = "Markerad av " + `marks` + " personer<br>\n"

	# FIXME
	map["default_url"] = "<i>Default-URL</i>"


    def alt_conf(self):
	return "Ol�sta i m�tet"

    def explain_conf(self):
	return "Lista ol�sta inl�gg i m�tet"

    def alt_comment(self):
	return "Kommentera"

    def explain_comment(self, textno):
	return "Skriv en kommentar till text " + `textno`

    def alt_answer(self):
	return "Besvara"

    def explain_answer(self, textno):
	return "Skriv en personlig kommentar till text " + `textno`

    def alt_write_text(self):
	return "Skriv inl�gg"

    def explain_write_text(self):
	return "Skriv inl�gg"

    def alt_review_text(self):
	return "�terse inl�gg"

    def explain_review_text(self):
	return "�terse inl�gg"

    def alt_help(self):
	return "Hj�lp"

    def explain_help(self):
	return "Hj�lp om denna sida"

    def alt_next_conf(self, cname):
	return "N�sta m�te"

    def explain_next_conf(self, cname):
	return "G� till n�sta m�te (" + cname + ")"

    def alt_see_time(self):
	return "Se tiden"

    def explain_see_time(self):
	return "Se tiden (enligt servern)"

    def alt_home(self):
	return "Hem"

    def explain_home(self):
	return "Lista m�ten med ol�sta texter"

    def alt_kill(self):
	return "Logga ut"

    def explain_kill(self):
	return "Avsluta WWW-kom"

    def alt_next_text(self, tno):
	return "N�sta text (" + `tno` + ")"

    def explain_next_text(self, tno):
	return "L�sa n�sta text (" + `tno` + ")"

    def alt_unformatted(self):
	return "L�sa oformatterad"

    def explain_unformatted(self, tno):
	return "L�sa text " + `tno` + " igen, oformatterad"

    def alt_formatted(self):
	return "L�sa formatterad"

    def explain_formatted(self, tno):
	return "L�sa text " + `tno` + " igen, heuristiskt formatterad"

    def alt_config(self):
	return "�ndra inst�llningar"

    def explain_config(self):
	return "Formul�r f�r att �ndra personliga inst�llningar"

    def alt_create_conf(self):
	return "Skapa m�te"

    def explain_create_conf(self):
	return "Formul�r f�r att skapa nytt m�te"

    def alt_next_comment(self, tno):
	return "N�sta kommentar (" + `tno` + ")"

    def explain_next_comment(self, tno):
	return "L�sa n�sta kommentar (" + `tno` + ")"

    def alt_next_footnote(self, tno):
	return "N�sta fotnot (" + `tno` + ")"

    def explain_next_footnote(self, tno):
	return "L�sa n�sta fotnot (" + `tno` + ")"

    def alt_select_conf(self):
	return "Lista m�ten"

    def explain_select_conf(self):
	return "S�k efter m�te och visa info"

    def text_format(self):
	if self.july_1997_layout:
	    return TEXT_FORMAT_JULY_1997
	else:
	    return TEXT_FORMAT
	    
    def format_text(self, map):
	self.text_rewrite_map(map)
	return self.text_format() % map

    def format_presentation(self, map, name_nolink):
	self.text_rewrite_map(map)
	return ("<p><b>Presentation f�r m�tet:</b>\n<p>"
		+ self.text_format() % map)

    def format_motd(self, map, name_nolink):
	self.text_rewrite_map(map)
	return ("<p>Lapp p� d�rren f�r " + name_nolink + ":<dl><dd>"
		+ self.text_format() % map + "</dl>")

    def info_unread_conf(self, conf, unread):
	if unread == 1:
	    return "1 ol�st i " + conf
	else:
	    return `unread` + " ol�sta i " + conf

    def there_are_more_unread_confs(self):
	return ("\n<p>\nDu har ol�sta inl�gg i �nnu fler m�ten. "
		+ "De kommer att visas n�r du har l�st ovanst�ende m�ten.")

    def login(self, unreadlist, trunc_msg, tot_unread, is_login):
	if trunc_msg == "" and len(unreadlist) > 0:
	    if tot_unread == 1:
		trunc_msg = "\n\n<p>Du har totalt 1 ol�st text."
	    else:
		trunc_msg = ("\n\n<p>Du har totalt " + `tot_unread`
			     + " ol�sta texter.")

	if len(unreadlist) > 0:
	    msg = "Du har ol�sta inl�gg i f�ljande m�ten:\n<p>\n" + unreadlist
	else:
	    msg = "Du har f�r n�rvarande inga ol�sta texter."

	if is_login:
	    return self.page("V�lkommen", "V�lkommen till LysKOM!",
			     "Du �r nu inloggad. " + msg + trunc_msg)
	else:
	    return self.page("Ol�sta m�ten", "Ol�sta m�ten",
			     msg + trunc_msg)

    def text_link(self, text, creation_time, subject, prev_subject, author):
	if subject == prev_subject:
	    subject = "--"
	elif subject == "":
	    subject = "(�rende saknas)"
	else:
	    subject = "<b>" + subject + "</b>"

	linktext = "%s (text %d)" % (subject, text)
	return "%s %s %s" % (self.format_date_sans_time(creation_time),
			     komhtml.text_as_link(text, linktext),
			     author)

    def different_passwords(self):
	return self.page("WWW-kom", "Kunde inte skapa person",
			 "Du angav tv� olika l�senord.  F�rs�k igen.")

    def person_creation_failed(self, name):
	return self.page("WWW-kom", "Kunde inte skapa person",
			 "En person med namnet " +name+ " kunde inte skapas.\n"
			 + "Kanske finns det redan n�gon som heter s�?\n\n"
			 + "<p>F�rs�k igen med ett annat namn.\n"
			 + "Du kan till exempel l�gga till en initial "
			 + "eller namnet p� din organisation.")

    def creation_done(self, name, pno):
	return self.page("WWW-kom", "V�lkommen",
			 "V�lkommen till LysKOM, " + komhtml.htmlquote(name)
			 + "!\nHoppas du ska trivas h�r.\n"
			 + "Nu m�ste du "
			 + komhtml.anchor(komhtml.komurl("login"), "logga in")
			 + ". "
			 + "Din Web-klient kommer att be dig mata in det namn "
			 + "och l�senord som du just angav.\n"
			 + "(Du tycker kanske att det �r dumt att du ska "
			 + "beh�va mata in de uppgifterna igen.  Tyv�rr "
			 + "tvingar HTTP fram den h�r l�sningen).\n\n"
			 + "<p>Om du �r den siffer�lskande typen vill du "
			 + "kanske veta att ditt anv�ndarnummer �r " + `pno`
			 + ".  Annars kan du strunta i det -- du kan alltid "
			 + "anv�nda ditt namn och beh�ver inte komma ih�g det "
			 + "nummer som anv�nds internt i LysKOM-systemet.")

    def creation_done_sc(self, name, pno):
	return self.page("WWW-kom", "V�lkommen",
			 "Du har nu skapat anv�ndaren "
			 + komhtml.htmlquote(name) + ".")

    def button_submit_text(self, presentations, footnotes, comments):
	if presentations > 0:
	    return "Skapa presentationen"
	elif footnotes > 0:
	    return "Skapa fotnoten"
	elif comments > 0:
	    return "Skapa kommentaren"
	else:
	    return "Skapa texten"

    def write_text_form(self, map):
	return WRITE_TEXT_FORM % map

    def write_comment(self, form, parent, parent_author):
	return self.page("Kommentera text " + parent,
			 "Kommentera text " + parent,
			 WRITE_COMMENT_FORMAT % {"parent_link": parent,
						 "parent_author":parent_author,
						 "form": form})
    def cannot_answer_anonymous(self):
	return self.page("Personligt svar", "Kan ej skapa personligt svar",
			 """Den text du kommenterar �r anonym.  Det
			 finns inget s�tt att f� reda p� vem som
			 skrivit den.  Det g�r att du tyv�rr inte kan
			 svara p� den med ett personligt svar.
			 """)

    def cannot_answer_dead(self):
	return self.page("Personligt svar", "Kan ej skapa personligt svar",
			 """Den text du kommenterar �r skriven av en
			 f�rfattare som inte l�ngre existerar.
			 Det g�r att du tyv�rr inte kan svara p� den
			 med ett personligt svar.
			 """)

    def write_personal_answer(self, form, parent, parent_author):
	return self.page("Svara p� text " + parent,
			 "Svara p� text " + parent,
			 WRITE_PERSONAL_ANSWER_FORMAT %
			 {"parent_link": parent,
			  "parent_author": parent_author,
			  "form": form})

    def review_texts(self):
	return self.page("�terse l�sta texter", 
			 "�terse l�sta texter", REVIEW_TEXTS_FORM)

    def write_text(self, form, conference):
	return self.page("Skriva inl�gg",
			 "Skriva inl�gg i " + conference,
			 WRITE_TEXT_FORMAT % {"conf-name": conference,
					      "form": form})

    def write_presentation(self, form, conference):
	return self.page("Presentera " + conference,
			 "Skriva presentation f�r " + conference,
			 WRITE_PRESENTATION_FORMAT % {"conf-name": conference,
						      "form": form})

    def conversion_format(self, map):
	return CONVERSION_FORMAT % map

    def write_text_format_recipient(self, conf, conf_as_link):
	return ('Mottagare: <input type="checkbox" name="rcpt'
		+ '" value="' + `conf` + '" checked> '
		+ conf_as_link + '<br>\n')

    def write_text_format_cc_recipient(self, conf, conf_as_link):
	return ('Extra kopie-mottagare: <input type="checkbox" name="cc_rcpt'
		+ '" value="' + `conf` + '" checked> '
		+ conf_as_link + '<br>\n')

    def write_text_format_recipient_part(self, recips):
	if len(recips) == 0:
	    return ""
	elif len(recips) == 1:
	    return recips[0]
	else:
	    return ("Texten du skriver har flera mottagare. "
		    + "Du kan ta bort mottagare genom att v�lja bort dem."
		    + "<br>\n"
		    + string.joinfields(recips, ""))

    def write_text_format_comment(self, text, author):
	return ('<input type="checkbox" name="parent" value="'
		+ `text` + '" checked> Kommentar till text ' + `text`
		+ ' ' + author + '<br>\n')

    def write_text_format_footnote(self, text, author):
	return ('<input type="checkbox" name="parent_footnote" value="'
		+ `text` + '" checked> Fotnot till text ' + `text`
		+ ' ' + author + '<br>\n')

    def write_text_comment_ref_form(self, body):
	return COMMENTING_FORM % {"lead": "Du kommenterar denna text:",
				  "body": komhtml.htmlquote(body)}

    def write_text_footnote_ref_form(self, body):
	return COMMENTING_FORM % {"lead": "Du skriver en fotnot till:",
				  "body": komhtml.htmlquote(body)}

    def write_text_format_comments_part(self, comments):
	if len(comments) == 0:
	    return ""
	elif len(comments) == 1:
	    return comments[0]
	else:
	    return ("Texten du skriver �r kommentar till flera texter. "
		    + "Du kan v�lja bort en eller flera av dem.<br>"
		    + string.joinfields(comments, ""))

    def write_text_format_presentation(self, cno, cname):
	return ('<input type="checkbox" name="presentation_for" value="'
		+ `cno` + '" checked> Presentation f�r ' + cname + '<br>\n')

    def write_text_format_presentations_part(self, presents):
	if len(presents) == 0:
	    return ""
	elif len(presents) == 1:
	    return presents[0]
	else:
	    return ("Texten du skriver �r presentation till flera m�ten. "
		    + "Det �r nog ovanligt att det �r vettigt att ha samma "
		    + "text som presentation till flera m�ten -- du har v�l "
		    + "koll p� vad du g�r nu? "
		    + "Du kan v�lja bort en eller flera av dem.<br>"
		    + string.joinfields(presents, ""))
	

    def new_texts_exists(self, new_texts, form):
	new_texts = string.join(new_texts, "\n<hr>\n")
	return self.page("Nya kommentarer", "Nya kommentarer funna",
			 WRITE_TEXT_NEW_COMMENTS % {"new_texts": new_texts,
						    "form": form})

    #
    # ----------------------------------------------------------------
    #

    def link_move_text(self):
	return "Flytta"

    def move_text(self, map):
	return self.page("Flytta text", "", MOVE_TEXT_FORM % map)

    def move_text_completed(self, map):
	return self.page("Flytta text klart", "", 
			 """
Text nummer %(text-no)s �r nu flyttad.

%(next-action)s

""" % map)

    #
    # ----------------------------------------------------------------
    #

    def killed(self):
	return self.page("Omstart", "Omstart", KILLED)

    def select_conf_form(self):
	return self.page("Lista m�ten", "Lista m�ten", SELECT_CONF_FORM)

    def list_conf(self, matches):
	res="<ul>\n"
	for item in matches:
	    res = (res + '<li>'
		   + komhtml.conf_as_link(item.conf_no(), item.name())
		   + '\n')
	res = res + "</ul>\n\n"
	return self.page("M�ten / Personer", "M�ten / Personer",
			 "F�ljande m�ten/personer st�mmer in p� det du s�kte: <p>" 
			 + res)

    def conf(self, session, cno, name_link, name_nolink, unread_list, truncated, unread, estim):

	map = {}

	if len(unread_list) == 0:
	    if cno == 6 and name_nolink == "Inl�gg }t mig":
		map["unread"] = "Du har inga ol�sta texter i M�tet."
	    else:
		map["unread"] = "Du har inga ol�sta texter i m�tet."
	    map["estim-unread"] = "" 
	else:
	    map["unread"] = "Dessa texter �r ol�sta:" + string.joinfields(
		unread_list, "\n")
	    if estim == 1:
		map["estim-unread"] = "Du har totalt 1 ol�st text.\n<p>"
	    else:
		map["estim-unread"] = ("Du har totalt " + `estim`
				       + " ol�sta.\n<p>")

	if unread:
	    map["set-unread-form"] = SET_UNREAD_FORM % \
				     {"name_nolink":name_nolink,
				     "conf_no":cno}
	else:
	    map["set-unread-form"] = ""

	if truncated:
	    map["truncated"] = "Du har �nnu fler ol�sta texter."
	else:
	    map["truncated"] = ""

	return lingo.page("Ol�sta i " + name_nolink,
			  "Ol�sta i " + name_link,
			  CONF % map)

    def conf_stat(self, in_map, currently_member, is_supervisor, conf):
	csmap = {}
	if not currently_member:
	    if in_map["join_allowed"]:
		csmap["become-member"] = komhtml.form_anchor(
		    in_map["join"], "G� med i m�tet") + "<br>"
	    else:
		# FIXME: send I-wanna-be-a-member-of-your-cool-conf
		# to the organizer.
		if conf.type().is_letterbox():
		    csmap["become-member"] = ""
		else:
		    csmap["become-member"] = """

		    Du f�r inte g� med i m�tet.  Skicka ett brev till
		    organisat�ren f�r medlemsskap.
		    """
	    csmap["disjoin-conference"] = ""
	    csmap["goto-conference"] = ""
	    csmap["set-unread-form"] = ""
	    csmap["estimated-unread"] = ""
	else:
	    csmap["become-member"] = ""
	    csmap["disjoin-conference"] = komhtml.form_anchor(
		in_map["disjoin"], "Uttr�da ur m�tet") + "<br>"
	    csmap["goto-conference"] = (komhtml.anchor(in_map["goto"],
						       "G� till m�tet")
					+ "<br>")
	    csmap["set-unread-form"] = SET_UNREAD_FORM % \
				       {"name_nolink":in_map["name_nolink"],
					"conf_no":in_map["conf_no"]}

	    if in_map["estimated"] == 0:
		csmap["estimated-unread"] = "Du har inga ol�sta i m�tet."
	    elif in_map["estimated"] == 1:
		csmap["estimated-unread"] = "Du har 1 ol�st inl�gg i m�tet."
	    else:
		csmap["estimated-unread"] = ("Du har "
					     + `in_map["estimated"]`
					     + " ol�sta inl�gg i m�tet.")

	csmap["motd"] = in_map["motd"]
	if in_map["presentation"] == None:
	    if conf.type().is_letterbox():
		csmap["presentation"] = "Personen saknar presentation."
	    else:
		csmap["presentation"] = "M�tet saknar presentation."
	else:
	    csmap["presentation"] = in_map["presentation"]
	
	if is_supervisor:
	    csmap["change-presentation"] = in_map["change-presentation"]
	else:
	    csmap["change-presentation"] = ""

	info = ["<b>�vrig information:</b><br><table>"]

	if conf.type().is_letterbox():
	    crea = "Personen skapad"
	    secr = "Personen �r hemlig"
	    prot = None
	    unprot = ("Personen �r �ppen"
		      +" (vem som helst f�r bli medlem i brevl�dan)")
	else:
	    crea = "M�tet skapat"
	    secr = "M�tet �r hemligt"
	    prot = "M�tet �r skyddat (vem som helst f�r ej bli medlem)"
	    unprot = None

	if in_map["creator"] != None:
	    info.append("<tr><td valign=top>" + crea + " av:</td><td valign=top>" + in_map["creator"] + "</td></tr>")
	info.append("<tr><td valign=top>" + crea + ":</td><td valign=top>" + self.format_date(conf.creation_time()) + "</td></tr>")
	# FIXME: Forts�tt h�r
	if conf.type().is_secret():
	    info.append("<tr><td colspan=2" + secr + "</td></tr>")
	if conf.type().is_protected():
	    if prot:
		info.append("<tr><td colspan=2" + prot + "</td></tr>")
	else:
	    if unprot:
		info.append("<tr><td colspan=2" + unprot + "</td></tr>")
	if conf.type().is_original():
	    info.append("<tr><td colspan=2" + "Kommentarer styrs om till superm�tet" + "</td></tr>")

	if in_map["supervisor"] == None:
	    info.append("<tr><td colspan=2" + "Organisat�r saknas" + "</td></tr>")
	else:
	    info.append("<tr><td valign=top>Organisat�r:</td><td valign=top>" + in_map["supervisor"] + "</td></tr>")

	if in_map["permitted_submitters"] == None:
	    info.append("<tr><td valign=top>Till�tna f�rfattare:</td><td valign=top>alla</td></tr>")
	else:
	    info.append("<tr><td valign=top>Till�tna f�rfattare:</td><td valign=top>"
			+ in_map["permitted_submitters"] + "</td></tr>")

	if in_map["super-conf"] == None:
	    info.append("<tr><td colspan=2" + "Superm�te saknas" + "</td></tr>")
	else:
	    info.append("<tr><td valign=top>Superm�te:</td><td valign=top>"
			+ in_map["super-conf"] + "</td></tr>")

	info.append("<tr><td valign=top>Antal medlemmar:</td><td valign=top>"
		    + `conf.no_of_members()` + "</td></tr>")
	info.append("<tr><td valign=top>Livsl�ngd p� inl�gg:</td><td valign=top>"
		    + `conf.nice()` + " dagar" + "</td></tr>")
	info.append("<tr><td valign=top>Senaste inl�gg:</td><td valign=top>"
		    + self.format_date(conf.last_written()) + "</td></tr>")

	# Don't include first_local_no or no_of_texts for now.

	csmap["info-fields"] = string.joinfields(info, "\n") + "\n</table>"

	return self.page("Status f�r " + in_map["name_nolink"],
			 in_map["name_nolink"],
			 CONF_STAT % csmap)

    def create_presentation(self, url):
	return komhtml.anchor(url, "Skapa presentation")

    def change_presentation(self, url):
	return komhtml.anchor(url, "�ndra presentationen")

    #
    # ----------------------------------------------------------------
    #                    Create conference
    # ----------------------------------------------------------------
    #

    def create_conf(self):
	return self.page("Skapa m�te", "Skapa nytt m�te", CREATE_CONF_FORM)

    def create_conf_empty_name(self):
	return self.page("Skapa m�te misslyckades",
			 "Skapa m�te misslyckades",
			 """M�tet kunde inte skapas, eftersom du inte
			 angav n�got namn p� m�tet.  Fyll i
			 namnf�ltet och f�rs�k igen. 
			 """)

    def create_conf_long_name(self):
	return self.page("Skapa m�te misslyckades",
			 "Skapa m�te misslyckades",
			 """M�tet kunde inte skapas, eftersom det namn
			 du gett m�tet inte godk�ndes.  Det �r f�r l�ngt.
			 """)

    def create_conf_bad_name(self):
	return self.page("Skapa m�te misslyckades",
			 "Skapa m�te misslyckades",
			 """M�tet kunde inte skapas, eftersom det namn
			 du gett m�tet inte godk�ndes.  Det inneh�ller
			 otill�tna tecken.
			 """)

    def create_conf_conf_exists(self):
	return self.page("Skapa m�te misslyckades",
			 "Skapa m�te misslyckades",
			 """M�tet kunde inte skapas, eftersom det
			 redan finns ett m�te med samma namn.
			 """)

    def create_conf_secret_public(self):
	return self.page("Skapa m�te misslyckades",
			 "Skapa m�te misslyckades",
			 """M�tet kunde inte skapas, eftersom du
			 f�rs�kt g�ra det b�de hemligt och �ppet.  Den
			 kombinationen accepteras inte av systemet.
			 """)

    def create_conf_illegal_aux_item(self):
	return self.page("Skapa m�te misslyckades",
			 "Skapa m�te misslyckades",
			 """M�tet kunde inte skapas, eftersom ett
			 felaktigt aux-item anv�ndes.
			 """)

    def create_conf_aux_item_permission(self):
	return self.page("Skapa m�te misslyckades",
			 "Skapa m�te misslyckades",
			 """M�tet kunde inte skapas, eftersom ett
			 du saknar r�ttigheter att s�tta ett aux-item.
			 """)

    #
    # ----------------------------------------------------------------
    #                    Administrate conference
    # ----------------------------------------------------------------
    #

    def alt_admin_conf(self):
	return "Administrera m�te"

    def explain_admin_conf(self):
	return "Formul�r f�r att administrera ett m�te"

    def admin_conf(self, map):
	return self.page("Administrera m�te", "", ADMIN_CONF_FORM % map)

    def submit_admin_members(self, map):
	return self.page("Administrera medlemmar", "", 
			 SUBMIT_ADMIN_MEMBERS_FORM % map)

    #
    # ----------------------------------------------------------------
    #                    Create a new user
    # ----------------------------------------------------------------
    #

    def alt_create_user(self):
	return "L�gg till anv�ndare"

    def explain_create_user(self):
	return "Formul�r f�r att l�gga till en ny anv�ndare"

    def create_user(self):
	return self.page("L�gga till ny anv�ndare", 
			 "L�gga till ny anv�ndare", CREATE_USER_FORM)

    #
    # ----------------------------------------------------------------
    #

    def time(self, server_time, local_time):
	return self.page("Se tiden", "Tiden",
			 "Klockan �r just nu:\n<ul>\n<li>"
			 + self.format_date(server_time)
			 + " (enligt LysKOM-servern)\n<li>"
			 + self.format_date(local_time)
			 + " (enligt WWW-kom-servern)\n</ul>\n")

    def configuration(self, map):
	return self.page("�ndra inst�llningar", "�ndra inst�llningar",
			 CONFIGURATION_FORM % map)

lingo=sv_pages()
