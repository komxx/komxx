#! @PYKOM@
# session_cache for WWW-kom.
# This is the hidden web server that handles a specific session.

import sys
import os

sys.path.insert(1, '@PYKOMLIB@')

os.chdir('@HTMLDIR@')

# Enable core dumps, but don't fail if the resource module is unavailable.
try:
    import resource
    (soft, hard) = resource.getrlimit(resource.RLIMIT_CORE)
    resource.setrlimit(resource.RLIMIT_CORE, (hard, hard))
except ImportError:
    pass

import sc

sc.start_sc(0, "@SERVER@", "@PORT@")
