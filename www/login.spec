After pondering different solutions, it is decided to use a single
cookie.  It will contain a value formed of two parts: name and number.
The name is encoded using base 64 encoding (which uses "A-Za-z0-9+/",
and equal signs for padding).  The number is printed in decimal
notation, using ASCII digits (0-9).  A cookie may not contain
semicolon, comma or white space.  The name and number are separated by
a separator; it seems reasonable to use colon (":").

The equal sign that base 64 uses for padding is changed to a #.

Both the name and number parts may be empty.

---------

Probably wanted behaviour of LM

A: The cookie name is set.
B: The authorization name is equal to the cookie LysKOM_name.
C: The cookie number is set.
D: The URL contains a session id.
E: cookie.number and URL.session_id is equal.
F: The authorization name is
	0: non-matching
	1: unique
	2: ambiguous.

ABCDEF
0o00o0 401	# First attempt (10, 12)
0o00o1 use the session from auth.name (15)	# First attempt
0o00o2 multiple choice from auth.name (13)  # First attempt
0o01o0 401 (17)
0o01o1 use the session from auth.name (18, 19)
0o01o2 multiple choice from auth.name (20)
0o10o- use session from LysKOM_number (assume client discarded name) (21,22)
0o110- redirect to session-less URL (23)
	(LM uses session from URL; SC redirects)
0o111- redirect to session-less URL (24)
	(LM uses session from URL; SC redirects)
10---0 401 (25)
10---1 use the session from auth.name (26, 27)
10---2 multiple choice from auth.name (28)
1100o0 401 (29)
1100o1 use the session from auth.name (30)
1100o2 multiple choice from auth.name (31)
1101o- use the session from the URL	# Select from multiple choice (32)
1110o- use the session from LysKOM_number	# Normal read (33)
11110- use the session from URL (back, byte av session)
11111- use the session from LysKOM_number	# Normal read (34, 35)

M�L:

Inga URL:er med sessioner i, utom fr�n valsidan vid inloggning.
F�ljdsats: om en sessionen finns i URL:en ska den anv�ndas.  Punkt.
Om man misslyckas ska man inte f� en 401, utan en "det gick fel"-sida
med l�nkar till URL:less session, re-enter l�senord, och kanske mer.

REGLOR f�r LM:

- auth saknas -> 401.  Alltid.  Ev. cookie beh�lls.
- auth.name och cookie.name �r olika (b�gge finns) -> bortse fr�n
  cookie.  Nolla cookie.  (I den vidare behandlingen s�tts kanske
  cookie igen).
- url.session finns -> F�rs�k anv�nda den, utan att sl� upp namnet.
- - url.session misslyckas pga ok�nd anv�ndare: -> felsida (cookie nollas)
- - url.session misslyckas pga bad password: -> annan felsida (cookie nollas)
- om cookie finns och cookie.name == auth.name:
- - anv�nd cookie.session
- - - om inloggning p� nystartad SC misslyckas: -> 401 (cookie beh�lls)
- sl� upp auth.name
- - unikt: koppla till SC
- - - om inloggning p� nystartad SC misslyckas: -> 401 (ingen cookie)
- - ingen match: -> 401
- - unambiguous: valsida

Observation: det tycks som om LM aldrig s�tter en cookie.  Cookies
s�tts bara vid lyckad inloggning, och d� �r det enklast om SC s�tter
den.  Man skulle till och med kunna g� s� l�ngt att LM aldrig p�verkar
cookies utom n�r den nollar den.  Men f�r enkelhetens skull s�tter LM
alltid cookien.

REGLOR f�r SC (unsorted, so far):

- url.session finns -> redirect till URL utan session. (set cookie)
  (Detta g�rs innan l�senordskoll f�r att f�rhindra o�ndliga loopar).
- om auth.password ej st�mmer: -> 401 (cookie resettas)
- bryr sig ej om auth.name (f�rutom att cookie.name s�tts fr�n auth.name)
- bryr sig ej om cookies (f�rutom att de s�tts eller resettas)

ANNAT:

- access av "/relogin" ger:

	om cookie satt: 401 med nollad cookie
	annars: redirect till /login
