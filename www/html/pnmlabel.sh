#!/bin/sh

# Create a label for the July 1997-style navigation toolbar.
#
# Usage: $0 srcdir extension left_template template xoffset

if [ $# != 5 ]
then
    echo $0: usage: $0 srcdir extension left_template template xoffset >&2
    exit 1
fi

srcdir=$1
extension=$2
left_template=$3
template=$4
xoffset=$5

files=""
rm -f crop.tmp
while read file text
do
    files="$files $file"
    echo Scanning $file... >&2
    [ -f $srcdir/$file-j.ppm ] || {
	echo $0: $srcdir/$file-j.ppm: no such file >&2
	exit 1
    }
    rm -f $file.gif $file.ppm.tmp $file.ppm.tmp2 $file.ppm.tmp3
    # We have to convert from ISO 8859-1 to swascii, since pbmtext
    # cannot handle 8-bit data.
    pbmtext -font $srcdir/font.pbm \
	"`echo "$text" | tr '������' '}{|]\133\134'`" \
    | pnmdepth 255 \
    | pgmtoppm "#000030"-"#e3e3d4" \
    | tee $file.ppm.tmp \
    | pnmcrop >> crop.tmp 2>&1 >/dev/null
done

topcrop=`grep top crop.tmp | awk '{ print $3 }' | sort -n | head -1`
botcrop=`grep bot crop.tmp | awk '{ print $3 }' | sort -n | head -1`
leftcrop=`grep left crop.tmp | awk '{ print $3 }' | sort -n | head -1`

echo cropping $topcrop rows off the top and $botcrop off the bottom >&2

templatey=`pnmfile $template | awk ' { print $6 } '`

for file in $files
do
    echo Generating $file-j.gif... >&2
    eval `pnmfile $file.ppm.tmp \
	  | awk '{ printf("pnmcut %d %d %d %d '$file'.ppm.tmp",
		          '$leftcrop',
		          '$topcrop', 
			  $4 - '$leftcrop',
			  $6 - '$botcrop' - '$topcrop') }'` \
    > $file.ppm.tmp2

    y=`pnmfile $file.ppm.tmp2 | awk '{ print ('$templatey' - $6)/2 }'`

    pnmpaste $file.ppm.tmp2 $xoffset $y $template \
    | pnmcat -lr $left_template $srcdir/$file-j.ppm - \
    | tee $file.ppm.tmp3 \
    | ppmtogif > $srcdir/$file$extension-j.gif || exit 1
    
    eval `pnmfile $file.ppm.tmp3 \
	  | awk '{ printf("width=%d;height=%d;", $4, $6)'}`
		   
    lmarg=6
    bmarg=2
    tmarg=2
    rmarg=10
    pnmcut $lmarg $bmarg \
	    `expr $width - $lmarg - $rmarg` \
	    `expr $height - $bmarg - $tmarg` \
	    $file.ppm.tmp3 \
    | ppmdim 0.6 \
    | pnmpaste - $lmarg $bmarg $file.ppm.tmp3 \
    | ppmtogif > $srcdir/$file-d$extension-j.gif || exit 1

    rm $file.ppm.tmp $file.ppm.tmp2 $file.ppm.tmp3
done
rm crop.tmp

exit 0
