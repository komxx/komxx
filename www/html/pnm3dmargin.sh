#!/bin/sh

tlcolor=$1
shift
brcolor=$1
shift
size=$1
shift

tmp1=/tmp/pnm3m1$$
tmp2=/tmp/pnm3m2$$
tmp3=/tmp/pnm3m3$$
tmp4=/tmp/pnm3m4$$
rm -f $tmp1 $tmp2 $tmp3 $tmp4

cat $@ > $tmp1
ppmmake $tlcolor 1 1 > $tmp2
ppmmake $brcolor 1 1 > $tmp3

pnmcat -lr $tmp2 $tmp1 $tmp3 > $tmp4
if test $size = 1
then
    pnmcat -tb $tmp2 $tmp4 $tmp3
else
    pnmcat -tb $tmp2 $tmp4 $tmp3 | $0 $tlcolor $brcolor `expr $size - 1`
fi

rm -f $tmp1 $tmp2 $tmp3 $tmp4
