import os
import sys
import pickle
import re
import string

size_re = re.compile("reading ([1-9][0-9]*) by ([1-9][0-9]*) GIF image")

def update(dbfile, images):
    err = 0
    try:
	f = open(dbfile, "r")
	m = pickle.load(f)
	f.close()
    except (IOError, EOFError):
	m = {}

    for img in images:
	f = os.popen("giftopnm -verbose %s 2>&1 >/dev/null" % (img),
		     "r")
        good = 0
        for line in f.readlines():
            match = size_re.search(line)
            if match is not None:
                m[os.path.basename(img)] = tuple(map(string.atoi,
                                                     match.groups()))
                good = 1
                break
        if good == 0:
	    sys.stderr.write(img + ": not a gif file\n")
	    sys.stderr.write(line)
	    err = 1

    if err == 0:
	f = open(dbfile, "w")
	pickle.dump(m, f)
	f.close()

    return err

if __name__ == "__main__":
    if len(sys.argv) < 3:
	sys.stderr.write("usage: %s dbfile image ...\n")
	sys.exit(1)
    
    sys.exit(update(sys.argv[1], sys.argv[2:]))
