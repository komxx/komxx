#!/bin/sh

# Warning: the bytes in fgcolor can only take on the values 00 or ff due
# to a bug in pgmtoppm, and fgcolor cannot be #ffffff since that would
# interfere with this script.

if [ $# != 2 ]
then
    echo $0: usage: $0 srcdir extension >&2
    exit 1
fi

srcdir=$1
extension=$2

# First attempt: blue text on gray background.  The gray looks good on
# some screens, but not on others.
fgcolor='#0000ff'
bgcolor='#bdbdbd'
topcolor='#e4e4e4'
botcolor='#202020'

# Second attempt: blue text on white background.  This is readable
# even on black-n-white screens.
fgcolor='#0000ff'
bgcolor='#ffffff'
topcolor='#bdbdbd'
botcolor='#404040'

files=""
rm -f crop.tmp
while read file text
do
    files="$files $file"
    echo Scanning $file... >&2
    rm -f $file.gif $file.ppm.tmp
    # We have to convert from ISO 8859-1 to swascii, since pbmtext
    # cannot handle 8-bit data.
    pbmtext -font $srcdir/font.pbm \
	"`echo "$text" | tr '������' '}{|]\133\134'`" \
    | pnmdepth 255 \
    | pgmtoppm "$fgcolor"-"$bgcolor" \
    | tee $file.ppm.tmp \
    | pnmcrop >> crop.tmp 2>&1 >/dev/null
done

topcrop=`grep top crop.tmp | awk '{ print $3 }' | sort -n | head -1`
botcrop=`grep bot crop.tmp | awk '{ print $3 }' | sort -n | head -1`

echo cropping $topcrop rows off the top and $botcrop off the bottom >&2

for file in $files
do
    echo Generating $file.gif... >&2
    eval `pnmfile $file.ppm.tmp \
	  | awk '{ printf("pnmcut 0 %d %d %d '$file'.ppm.tmp",
		          '$topcrop', $4, $6 - '$botcrop' - '$topcrop') }'` \
    | ./pnm3dmargin "$bgcolor" "$bgcolor" 2 \
    | ./pnm3dmargin "$topcolor" "$botcolor" 3 \
    | ppmtogif > $srcdir/$file.gif
    rm $file.ppm.tmp
done
rm crop.tmp
exit 0
