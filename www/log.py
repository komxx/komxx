# Copyright (C) 1996  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import os
import time
import language

try:
    import syslog

    facility=syslog.LOG_LOCAL5

    def init(name, debugmode=0):
	global facility
	if debugmode:
	    facility=syslog.LOG_LOCAL4
	syslog.openlog(name,
		       syslog.LOG_PID | syslog.LOG_CONS | syslog.LOG_NDELAY,
		       facility)

    def close():
	syslog.closelog()

    def emerg(string):
	syslog.syslog(syslog.LOG_EMERG, string[0:512])

    def alert(string):
	syslog.syslog(syslog.LOG_ALERT, string[0:512])

    def crit(string):
	syslog.syslog(syslog.LOG_CRIT, string[0:512])

    def err(string):
	syslog.syslog(syslog.LOG_ERR, string[0:512])

    def warning(string):
	syslog.syslog(syslog.LOG_WARNING, string[0:512])

    def notice(string):
	syslog.syslog(syslog.LOG_NOTICE, string[0:512])

    def info(string):
	syslog.syslog(syslog.LOG_INFO, string[0:512])

    def debug(string):
	syslog.syslog(syslog.LOG_DEBUG, string[0:512])

except ImportError:
    def init(name, debugmode=0):
	global filename
	if debugmode:
	    name = name + ".dbg"
	filename="/tmp/" + name + "."

    def close():
	pass

    def dolog(facility, str):
	syslogfile = open(filename + `os.getpid()`, "a+", 0)
	syslogfile.write(language.lingo.format_date(
	    time.localtime(time.time()))
			 + " " + facility + ": " + str + "\n")
	syslogfile.close()

    def emerg(str):
	dolog("emerg", str)

    def alert(str):
	dolog("alert", str)

    def crit(str):
	dolog("crit", str)

    def err(str):
	dolog("err", str)

    def warning(str):
	dolog("warning", str)

    def notice(str):
	dolog("notice", str)

    def info(str):
	dolog("info", str)

    def debug(str):
	dolog("debug", str)

if __name__ == '__main__':
    init("cedertest")
    # emerg("This is an emergency message")
    alert("This is an alert")
    crit("This is a critical message")
    err("This is a fake error message")
    warning("This is a warning message")
    notice("This is notice")
    info("This code was written 1996-05-06 13:58:56")
    debug("The last line is now reached")
