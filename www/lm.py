# Copyright (C) 1996  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Login manager
# This is a login manager for WWW-kom, a special-purpose HTTP server.

# Python 1.4beta2 and newer insists on adding the directory containing
# this script first on the path, which is not what I want, since the
# test suite tries to override the komconfig.py found in this
# directory with its own version.  At least Python 1.4beta3 adds a
# trailing slash to the last component of the path it adds, so remove
# it here.

import sys
if sys.path[0][-1] == "/":
    del sys.path[0]

import socket
import os
import base64
import string
import re
import traceback

import myhttpbase
import komxx
import myserver
import komconfig
import komexceptions
import siteconfig
import komhtml
import komtext
import log
import estring
import toolbar
import language
from language import lingo

# Global variables:
session=None		# The LysKOM session used to e.g. match names.
session_map={}
reverse_session_map={}
startport=None

# Constants used internally in this module:
true=1
false=0

def compare_z_info(a, b):
    if a.name() < b.name():
	return -1
    elif a.name() == b.name():
	return 0
    else:
	return 1

class lm(myhttpbase.myhttpbase):
    def connect(self, host, port):
	"""Connect to a remot port.

	Returns a tuple (status, readfile, writefile) where status
	is true if the connection succeeded, false if the connection
	was refused.  If the call succeeded, readfile will be a file
	opened for reading and writefile a file opened for writing."""

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	try:
	    s.connect((host, port))
	except socket.error, x:
	    if x[1] == "Connection refused":
		return (false, false, false)
	    else:
		raise socket.error, x
	r = s.makefile("r")
	w = s.makefile("w")
	s.close()
	return (true, r, w)

    def start_server(self, person, password):
	global session_map
	global reverse_session_map
	log.debug("Attempting to start a server for person " + `person`)
	# FIXME: configurable.  Try alternate hosts for load balancing.
	host = komconfig.wwwhost
	(success, r, w) = self.connect(host, startport)
	#log.debug("start_server: success = " + `success`)
	if success:
	    # FIXME: check errors
	    log.debug("...reading Port:")
	    port = string.atoi(estring.read_word(r, "Port:"))
	    log.debug("...Got " + `port`)
	    # base64.encodestring normally appends a newline,
	    # but not if it decodes the empty string!
	    if password == "":
		pwd = "\n"
	    else:
		pwd = base64.encodestring(password)
	    w.write("Person: " + `person` + "\nPassword: " + pwd)
	    w.flush()
	    log.debug("...reading response")
	    response = string.strip(r.readline())
	    log.debug("...Got " + `response`)
	    r.close()
	    w.close()
	    log.debug("Created a server at " + host + " " + `port`)
	    if reverse_session_map.has_key((host, port)):
		log.debug("The port was previously used by"
			  + `reverse_session_map[(host, port)]`)
		del session_map[reverse_session_map[(host, port)]]
		del reverse_session_map[(host, port)]
	    if response == "Login OK":
		log.debug("Login on the port was successful.")
		session_map[person] = (host, port)
		reverse_session_map[(host, port)] = person
		self.connect_server(person)
	    elif response == "Bad person":
		log.debug("The person doesn't exist, so the login failed.")
		raise komexceptions.nonexisting_person(person)
	    elif response == "Bad password":
		log.debug("The password was incorrect, so the login failed.")
		raise komhtml.bad_auth
	    elif response == "Connection refused":
		log.notice("SC-starter said that the connection was refused")
		raise komexceptions.connection_refused
	    else:
		log.err("Got unknown response '" + response
			+ "' from SC starter")
		self.log_error("Got unknown response '" + response
			       + "' from SC starter")
	else:
	    self.log_error("Cannot contact WWW-kom; connection refused")
	    raise komexceptions.internal_error(
                "connection refused while contacting SC")


    def connect_server(self, person):
	global session_map
	log.debug("Attempting to connect to SC server")
	if session_map.has_key(person):
	    (host, port) = session_map[person]
	    log.debug("Connecting to " + `host` + " " + `port`)
	    (success, r, w) = self.connect(host, port)
	    if success:
		self.session_server_r = r
		self.session_server_w = w
		log.debug("Successful connection performed")
	    else:
		log.debug("Server at" + `host` + " " + `port`
			  + "has died.  Removing records.")
		del session_map[person]
		del reverse_session_map[(host, port)]
	    return success
	else:
	    log.debug("Don't know about a SC server for this person")
	    return false

    def forward_query(self):
	# FIXME: it appears that execution sometimes can arrive here
	# with self.session_server_w unset.  Find out how.
	self.session_server_w.write(
	   string.join([self.command, self.path, self.request_version]))
	self.session_server_w.write("\r\n")
	self.session_server_w.writelines(self.headers.headers)
	self.session_server_w.write("\r\n")
	# FIXME: Forward the body, if any
	self.session_server_w.flush()

    def server_side_redirect(self, person, password):
	log.debug("server_side_redirect(" + `person` + ") entered")
	if not self.connect_server(person):
	    self.start_server(person, password)
	self.forward_query()
	while 1:
	    line = self.session_server_r.readline()
	    log.debug("REPLY: " + line)
	    if line:
		try:
		    self.wfile.write(line)
		except IOError:
		    pass
	    else:
		break
	log.debug("END REPLY")
	self.session_server_r.close()
	self.session_server_w.close()

    def do_POST(self):
	# All POST methods are currently implemented in LM.
	# There is not yet any support for forwarding data to SC.
	# No authentication check is done.
	length = self.headers.getheader('Content-length')
	if (self.headers.getheader('Content-type')
	    != "application/x-www-form-urlencoded"):
	    self.log_error("Bad content-type"
			   + self.headers.getheader('Content-type'))
	    self.send_error(400)
	    return
	body = self.rfile.read(string.atoi(length))
	args = komhtml.form_data(body)
	u = string.split(self.path, '/')
	if len(u) != 2 or u[0] != '':
	    self.send_error(404)
	    return
	mname = "do_POST_" + u[1]
	method = getattr(self, mname)
	method(args)

    def do_POST_create(self, args):
	if not (args.has_key('username')
		and args.has_key('password')
		and args.has_key('passcheck')):
	    self.log_error("Missing argument to create")
	    self.error(400)
	    return
	if args['password'] != args['passcheck']:
	    self.respond(lingo.different_passwords())
	else:
	    name = args['username']
	    pno = session.create_person(name, args['password'], [], false)
	    if (pno == komxx.st_error):
		self.respond(lingo.person_creation_failed(name))
	    else:
		info = session.get_server_info()
		lookup={"mailbox":pno,
			"confs":info[1],
			"pers":info[2],
			"motd":info[3],
			"news":info[4]}
		place=0
		for (conf, prio) in komconfig.default_confs:
		    
		    if lookup.has_key(conf):
			conf = lookup[conf]
		    
		    session.join_conference(conf, prio, place,
					    komxx.membership_type())
		    place = place + 1

		session.logout()
		self.respond(lingo.creation_done(name, pno))

    def do_PING(self):
	self.send_response(200)
	self.send_header("Content-type", "application/x-ping-response")
	self.send_header("Content-length", 5)
	self.end_headers()
	self.wfile.write("PONG\n")

    def do_GET(self):
	try:
	    if estring.is_prefix(self.path, "/help/"):
		self.do_GET_help()
	    else:
		self.do_GET_wwwkom()
	except:
	    self.send_error(500)
	    self.wfile.flush()
	    if sys.exc_type:
		log.err("Caught exception (pos 1): "
			+ `sys.exc_type` + " " + `sys.exc_value`)
                tb = sys.exc_traceback
                try:
                    log.err(str(sys.exc_value.args))
                except:
                    pass
		for block in traceback.format_tb(tb):
                    for line in string.split(block, "\n"):
                        if string.strip(line):
                            log.err(line)

    def do_GET_help(self):
	# Remove the "/help/" part of the path.
	command = self.path[6:]
	self.toolbar = toolbar.toolbar()
	language.toolbar = self.toolbar
	self.respond(lingo.help(command))

    def do_GET_wwwkom(self):
	try:
	    # komhtml.decodeurl throws komhtml.not_a_komurl if
	    # the URL isn't an URL that should be forwarded to SC.
	    # Such URL:s are catched below and passed to the base class.
	    # One might argue that it is bad practise to throw an
	    # exception for such a normal event.  However, it makes sense
	    # to throw an exception in SC, and LM and SC shares the
	    # komhtml module.  Don't touch the cookie if an error occurs
	    # this early.
	    self.url = komhtml.decodeurl(self.path)

	    # FIXME: should not log all of the parameters, for
	    # instance the contents of created text.
	    log.info("GET " + self.url.as_string_sans_hostpart_with_session())

	    self.cookie_fetch()

	    if hasattr(self, "bad_authorization_cookie"):
		log.debug("Bad auth cookie detected; will ask for new auth")
		self.cookies.append(("LysKOM_auth", "good"))
		raise komhtml.bad_auth

	    # This looks like something that should be forwarded to SC.
	    # Check that we have some authorization.
	    # The next line might raise komhtml.bad_auth.
	    [auth_name, auth_password] = self.retrieve_authorization()

	    if auth_name != self.cookie_name():
		self.clear_cookie()

	    if self.url.pers_no != None:

		try:
		    self.server_side_redirect(self.url.pers_no, auth_password)
		except komhtml.bad_auth:
		    # FIXME: I don't think these are necessary.
		    # log.debug("Setting auth bad")
		    # self.cookies.append(("LysKOM_auth", "bad"))
		    self.respond(lingo.bad_password_for_session(
		       komtext.safe_name_nolink(session, self.url.pers_no)))
		except komexceptions.nonexisting_person:
		    self.respond(lingo.nonexisting_person_url(
			self.url.pers_no))

	    elif auth_name == self.cookie_name() and self.cookie_pers_no():

		self.server_side_redirect(self.cookie_pers_no(), auth_password)

	    else:

		m = re.match("p ([1-9][0-9]*)$", auth_name)
		if  m is not None:
		    id = int(m.group(1))
		    log.debug("Got numeric ID " + `id`)
		    self.server_side_redirect(id, auth_password)
		else:
		    log.debug("Asking lyskomd for '" + auth_name + "'")
		    matches = session.lookup_z_name(auth_name, true, false)
		    if len(matches) == 0:
			raise komhtml.bad_auth
		    elif len(matches) == 1:
			self.server_side_redirect(matches[0].conf_no(), 
						  auth_password)
		    else:
			matches.sort(compare_z_info)
			self.respond(lingo.ambiguous_login(auth_name,
							   matches))

	except komhtml.bad_auth:
	    self.bad_auth()
	except komhtml.not_a_komurl:
	    if self.path[-1] == "/":
		self.path = self.path + "index.html"
	    myhttpbase.myhttpbase.do_GET(self)
	except komexceptions.connection_refused:
	    self.respond(lingo.noconnect())


def detach():
    if os.getppid() != 1:
	try:
	    signal.signal(signal.SIGTTOU, signal.SIG_IGN)
	except:
	    pass
	try:
	    signal.signal(signal.SIGTTIN, signal.SIG_IGN)
	except:
	    pass
	try:
	    signal.signal(signal.SIGTSTP, signal.SIG_IGN)
	except:
	    pass

	child = os.fork()
	if child < 0:
	    log.err("fork failed")
	    sys.exit(1)
	elif child > 0:
	    os._exit(0)	# parent
	
	# child

	try:
	    os.setsid()
	except:
	    pass

    for i in range(63):
	try:
	    os.close(i)
	except:
	    pass

    log.close()
    f=open("/tmp/wwwkomlm." + `os.getpid()` + ".out", "w")
    os.dup2(f.fileno(), 0)
    os.dup2(f.fileno(), 1)
    os.dup2(f.fileno(), 2)


def do_start_lm(debug, lyskom_host, lyskom_port):
    """
    debug: true if lm should not background itself.
    lyskom_host: hostname of LysKOM server.
    lyskom_port: port number of LysKOM server (as a string).
    """
    global session

    if not debug:
	detach()
	log.init("lm")
	log.notice("LM starting")
    else:
	log.init("lm", 1)
	log.notice("LM debug run starting")
    print "Connecting to LysKOM host", lyskom_host + ", port",
    print lyskom_port + ". Session started via port", startport
    sys.stdout.flush()
    try:
	session = komxx.new_session(lyskom_host, lyskom_port,
				    "WWW-kom dispatcher", "0.7.post.7")
    except komxx.no_connection:
	sys.stderr.write("Unable to connect to " + lyskom_host + ":"
			 + lyskom_port + "\n")
	sys.stderr.flush()
	sys.exit(1)
    print "Connected to LysKOM server.  Starting HTTP server"
    sys.stdout.flush()
    log.debug("Connected to LysKOM server.")
    try:
	httpd = myserver.myserver((komconfig.wwwhost, 
				   string.atoi(komconfig.wwwport)), lm)
	print "HTTP server started."
	sys.stdout.flush()
	log.debug("HTTP server started.")
	httpd.serve_forever()
    except komxx.no_connection:
	sys.stderr.write("Connection to " + lyskom_host + ":" + lyskom_port
			 + "lost\n")
	log.crit("Connection to LysKOM server lost")
	sys.exit(2)

def start_lm(debug, lyskom_host, lyskom_port, sc_port):
    """
    debug: true if lm should not background itself.
    lyskom_host: hostname of LysKOM server.
    lyskom_port: port number of LysKOM server (as a string).
    sc_port: port to connect to when a new sc is needed.
    """
    global startport

    startport = sc_port

    try:
	try:
	    siteconfig.siteconfig()
	    komhtml.init()
	    do_start_lm(debug, lyskom_host, lyskom_port)
	except SystemExit:
	    raise sys.exc_type, sys.exc_value, sys.exc_traceback
	except:
	    if sys.exc_type:
		log.err("Caught exception: "
			+ `sys.exc_type` + " " + `sys.exc_value`)
		for line in traceback.format_tb(sys.exc_traceback):
		    log.err(line)
    finally:
	log.notice("LM exiting")
