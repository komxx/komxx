# Copyright (C) 1996  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import errno
import socket
import SocketServer

class myserver(SocketServer.TCPServer):
    def server_bind(self):
	self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	self.socket.bind(self.server_address)

    def get_request(self):
	"""Like TCPServer.get_request, but ignore EPROTO errors.
	"""
	while 1:
	    try:
		return self.socket.accept()
	    except socket.error, value:
		if value[0] != errno.EPROTO:
		    raise sys.exc_type, sys.exc_value, sys.exc_traceback
