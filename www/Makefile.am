# Makefile template for WWW-kom
# Copyright (C) 2000  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

PYFILES = \
	estring.py \
	komexceptions.py \
	komhtml.py \
	komtext.py \
	language.py \
	lm.py \
	log.py \
	myhttpbase.py \
	myserver.py \
	sc.py \
	toolbar.py

SUBDIRS = html

wwwdatadir = $(pkgdatadir)/wwwkom
wwwkompydir = $(wwwdatadir)/py
htmldir = $(wwwdatadir)/html



if ENABLE_WWW_KOM

sbin_SCRIPTS = login_manager
libexec_SCRIPTS = session_cache
wwwkompy_SCRIPTS = $(PYFILES) komconfig.py

endif


EXTRA_DIST = $(PYFILES) genconfig.sh buildvers.py genconfig \
	login_manager.py session_cache.py urls.txt www.texi siteconfig.py

MOSTLYCLEANFILES = login_manager session_cache komconfig.py genconfig

login_manager: login_manager.py Makefile
	$(RM) $@ $@.tmp
	sed < $(srcdir)/login_manager.py > $@.tmp \
	    -e 's%@PYKOM@%$(bindir)/pykom%g' \
	    -e 's%@PYKOMLIB@%$(wwwkompydir)%g' \
	    -e 's%@HTMLDIR@%$(htmldir)%g' \
	    -e 's%@SERVER@%$(default_server)%g' \
	    -e 's%@PORT@%$(default_port)%g'
	chmod +x $@.tmp
	mv $@.tmp $@

session_cache: session_cache.py Makefile
	$(RM) $@ $@.tmp
	sed < $(srcdir)/session_cache.py > $@.tmp \
	    -e 's%@PYKOM@%$(bindir)/pykom%g' \
	    -e 's%@PYKOMLIB@%$(wwwkompydir)%g' \
	    -e 's%@HTMLDIR@%$(htmldir)%g' \
	    -e 's%@SERVER@%$(default_server)%g' \
	    -e 's%@PORT@%$(default_port)%g'
	chmod +x $@.tmp
	mv $@.tmp $@

genconfig: $(srcdir)/genconfig.sh Makefile
	$(RM) genconfig.tmp
	cat $(srcdir)/genconfig.sh > genconfig.tmp
	chmod +x genconfig.tmp
	mv -f genconfig.tmp genconfig

komconfig.py: genconfig
	$(RM) komconfig.py komconfig.py.tmp
	./genconfig > komconfig.py.tmp
	mv -f komconfig.py.tmp komconfig.py

install-data-hook:
if ENABLE_WWW_KOM
	if [ -f $(wwwkompydir)/siteconfig.py ]; then : ; \
	else $(INSTALL_DATA) $(srcdir)/siteconfig.py $(wwwkompydir) || exit 1;\
	fi
	$(bindir)/pykom -c    "import compileall;\
			       compileall.compile_dir('$(pkgdatadir)')"
	$(bindir)/pykom -O -c "import compileall;\
			       compileall.compile_dir('$(pkgdatadir)')"
	if [ -f $(wwwkompydir)/buildvers.py ]; \
	then \
	    ( sed 's/[---version ="]//g' $(wwwkompydir)/buildvers.py \
	    | awk '{ printf "version = \"-%d\"", $$1 + 1 }'\
	    > $(wwwkompydir)/buildvers.py.new ) ; \
	    mv -f $(wwwkompydir)/buildvers.py.new \
		$(wwwkompydir)/buildvers.py ; \
	else \
	    echo 'version = "-1"' > $(wwwkompydir)/buildvers.py; \
	fi
endif
