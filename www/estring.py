"""Extra string utilities."""

import string

import log
import komexceptions

def read_word(source, header):
    """Read a key-value line from SOURCE and return the value.

    Check that the first word is equal to HEADER; raise internal_error
    otherwise."""

    # FIXME: check errors
    response = source.readline()

    words = string.splitfields(response, " ")
    if len(words) != 2 or words[0] != header:
	log.err("Protocol error detected in read_word:")
	log.err("response: " + repr(response))
	log.err("len(words): %d" % len(words))
	for w in words:
	    log.err("Next word: %s" % repr(w))
	raise komexceptions.internal_error(
            "read_word error: got %s" % repr(response))
    res = string.strip(words[1])
    log.debug("read_word(%s) -> %s" % (header, repr(res)))
    return res

def xlisttoints(xlist):
    res = []
    for elem in string.splitfields(xlist, "x")[:-1]:
	try:
	    res.append(string.atoi(elem))
	except ValueError:
	    # Ignore the trail once an error has been detected
	    return res
    return res

def intstoxlist(ints):
    res = []
    for i in ints:
	res.append(repr(i) + "x")
    return string.joinfields(res, "")

def is_prefix(str, pfx):
    return str[:len(pfx)] == pfx
