#!/bin/sh

# Attempt to get a FQDN in $hostname.  Using domainname is bogus,
# since that returns a NIS domain name, but it is often correct.  And
# if hostname returns a FQDN this script will work correctly.  If you
# find that this script fails, please do not edit the resulting file,
# but edit siteconfig.py instead.

hostname=`hostname`
case $hostname in
    *.*) ;;
    *) if [ "`dnsdomainname`" != "" ]
       then
           hostname=$hostname.`dnsdomainname`
       elif [ "`domainname`" != "(none)" ]
       then
           hostname=$hostname.`domainname`
       fi;;
esac

cat <<EOF

# The host name where this instance of WWW-kom is running.
wwwhost='$hostname'

def set_wwwhost(host):
    global wwwhost
    wwwhost = host

# The port number where this instance of WWW-kom is running.
wwwport='8494'

def set_wwwport(port):
    global wwwport
    wwwport = port

# default_confs is a list of pairs (conference, priority).
# Newly created persons will be members of the following conferences.
# The first element of each pair can be a conference number or
# one of the following strings:
#
#  'mailbox'	The persons own mailbox.
#  'confs'	conf-pres-conf from 36=get-info.
#  'pers'	pers-pres-conf from 36=get-info.
#  'motd'	motd-conf from 36=get-info.
#  'news'	kom-news-conf from 36=get-info.

default_confs=[('mailbox', 255),
	       ('news', 200),
	       ('confs', 200)]

EOF
