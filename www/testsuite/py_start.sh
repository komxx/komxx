#!/bin/sh
#
# Start a Python interpreter
# Arguments:
# 1: full path to the python interpreter
# 2: directory to cd to
# 3: pythonpath arg

export PYTHONPATH
PYTHONPATH=`pwd`/$3:`pwd`:..:PYTHONLIB
cd $2
exec $1
