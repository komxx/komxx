# This script was used to calculate the times found in config/unix.exp.
import rand
import time
t = time.time() - 100000000L
for i in range(1, 100):
    tm = time.localtime(t)
    print '# %s' % (time.asctime(tm))
    print 'set t%02d "%d %d %d %d %d %d %d %d %d"' % (
	i,
	tm[5], tm[4], tm[3], tm[2], tm[1]-1, tm[0]-1900,
	(tm[6]+1)%7, tm[7]-1, tm[8])
    print
    t = t + 100 * rand.rand() + rand.rand()
