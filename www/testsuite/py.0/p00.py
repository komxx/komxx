import komxx
import time
import re

def start(port):
    global s

    s = komxx.new_session("localhost", port, "test suite", "0.0")
    s.login(5, "secret", 1)

def set_last_read(lno):
    global s
    s.set_last_read(3, lno)

def list_unread():
    global s
    s.goto_conf(3)
    print "List start"
    for (t, l) in s.list_unread_texts_in_current(100):
	print "Text", t, "Level", l
    print "List end"

def last_local_before(timestamp):
    v = eval("[" + re.sub(" ", ",", timestamp) + "]")

    t = (v[5] + 1900, v[4] + 1, v[3], v[2], v[1], v[0],
	 (v[6]+6)%7, v[7] + 1, v[8])

    print "Found local no", s.last_local_before(3, t)

    
