import komxx
def start(port):
    global s

    s = komxx.new_session("localhost", port, "test suite", "1.0")
    s.login(5, "secret", 1)
    return s

def list_unread():
    global s
    print "List start"
    try:
	for (t, l) in s.list_unread_texts_in_current(100):
	    print "Text", t, "Level", l
    except komxx.no_conf_selected_error:
	print "No conf selected"
    print "List end"

def pending():
    global s
    print "Footnotes:",
    for f in s.get_pending_footnotes():
	print f,
    print
    print "Comments:",
    for c in s.get_pending_comments():
	print c,
    print
    print "Next text:", s.next_text()
