import socket
import select
import sys
import string
import re

s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
port=string.atoi(sys.argv[1])
pfx = ""
metapfx = ""
if len(sys.argv) > 2:
   pfx = sys.argv[2] + ": "
   metapfx = sys.argv[2] + "meta: "
try:
   s.bind(('', port))
except socket.error, x:
   print metapfx + "bind failed:", x[1]
   sys.exit(1)
s.listen(1)
print metapfx + "Listening on", port
(p,addr)=s.accept()
print metapfx + "Got a connection"
needpfx = 1
while 1:
   (i, o, e) = select.select([p, sys.stdin], [], [])
   if p in i:
      d = p.recv(512)
      if d == "":
	 if needpfx == 0:
	    print
	 print metapfx + "EOF on socket"
	 sys.exit(0)
      line = ""
      if needpfx:
	 line = pfx
	 needpfx = 0
      if d[-1] == '\n':
	 d = d[:-1]
	 needpfx = 1
      line = line + re.sub("\n", "\n" + pfx, d)
      if needpfx == 1:
	 line = line + "\n"
      sys.stdout.write(line)
      sys.stdout.flush()
   if sys.stdin in i:
      d = sys.stdin.readline()
      if d == "":
	 print metapfx + "EOF on stdin"
	 sys.exit(1)
      p.send(d)
