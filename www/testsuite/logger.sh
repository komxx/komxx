#!/bin/sh
#
# Log a line to the a log.
# Arguments:
# 1: full path to the python interpreter
# 2: htmldir
# 3: string to log
# 4: system to log as (e.g. "lm" or "sc")

export PYTHONPATH
PYTHONPATH=`pwd`:..:PYTHONLIB
cd $2
exec $1 ../testsuite/logger.py "$3" $4
