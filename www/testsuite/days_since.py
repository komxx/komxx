import time
import sys
import re

v = eval("[" + re.sub(" ", ",", sys.argv[1]) + "]")

t = (v[5] + 1900, v[4] + 1, v[3], v[2], v[1], v[0],
     (v[6]+6)%7, v[7] + 1, v[8])

print int((time.time() - time.mktime(t)) / 86400)
