#!/bin/sh
#
# Start sc for a test run.
# Arguments:
# 1: full path to the python interpreter
# 2: htmldir (the source directory of the html files)
# 3: the lyskomd host to connect to (normally localhost)
# 4: lyskomd_port (the port lm should contact to get to the LysKOM
#    server -- however, listening to that port will be a process
#    controlled by the DejaGnu test script, but please don't tell that
#    to sc!)

export PYTHONPATH
PYTHONPATH=`pwd`:..:PYTHONLIB
cd $2
exec $1 ../sc.py $3 $4 tell_pid 2>&1
