# Makefile template for WWW-kom testsuite
# Copyright (C) 1996  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Process this file with python to generate Makefile.in.

from pmg import *

m = komxxmakefile()

m.add_target(generated_source(".gdbinit"), "all")

m.mostlyclean('*.log')
m.mostlyclean('*.sum')
m.emit()

print """
check: py_check sc_check lm_check

py_check: site.exp py_start .gdbinit logger
	$(RUNTEST) $(RUNTESTFLAGS) --tool py --srcdir $(srcdir)

sc_check: site.exp sc_start .gdbinit logger
	$(RUNTEST) $(RUNTESTFLAGS) --tool sc --srcdir $(srcdir)

lm_check: site.exp lm_start .gdbinit logger
	$(RUNTEST) $(RUNTESTFLAGS) --tool lm --srcdir $(srcdir)

site.exp: Makefile
	$(RM) $@.tmp
	echo "# Automatically generated -- do not edit" > $@.tmp
	echo >> $@.tmp
	echo "set python "`pwd`"/../../python/python" >> $@.tmp
	echo "# unpurepython should be a unpurified python binary"
	echo "set unpurepython "`pwd`"/../../python/python" >> $@.tmp
	echo >> $@.tmp
	echo "set srcdir $(srcdir)" >> $@.tmp
	echo "set htmldir $(srcdir)/../html" >> $@.tmp
	echo "set pydir $(srcdir)/.." >> $@.tmp
	echo "set env(PYTHONPATH) `cd $(top_srcdir)/python/Lib;pwd`" >> $@.tmp
	chmod 444 $@.tmp
	mv -f $@.tmp $@

.gdbinit:
	$(RM) $@ $@.tmp
	echo 'dir $(top_srcdir)/py-clients' >> $@.tmp
	echo 'dir ../../genclasses' >> $@.tmp
	echo 'dir $(top_srcdir)/libconnection' >> $@.tmp
	echo 'dir ../../libkom++' >> $@.tmp
	echo 'dir $(top_srcdir)/libkom++' >> $@.tmp
	echo 'dir $(top_srcdir)/libisc/src' >> $@.tmp
	echo 'dir $(top_srcdir)/libcompat' >> $@.tmp
	mv $@.tmp $@

lm_start: $(srcdir)/lm_start.sh Makefile
	$(RM) lm_start.tmp
	sed "s@PYTHONLIB@`cd $(top_srcdir)/python/Lib;pwd`@" \
		$(srcdir)/lm_start.sh > lm_start.tmp
	chmod +x lm_start.tmp
	chmod 555 lm_start.tmp
	mv -f lm_start.tmp lm_start

sc_start: $(srcdir)/sc_start.sh Makefile
	$(RM) sc_start.tmp
	sed "s@PYTHONLIB@`cd $(top_srcdir)/python/Lib;pwd`@" \
		$(srcdir)/sc_start.sh > sc_start.tmp
	chmod +x sc_start.tmp
	chmod 555 sc_start.tmp
	mv -f sc_start.tmp sc_start

py_start: $(srcdir)/py_start.sh Makefile
	$(RM) py_start.tmp
	sed "s@PYTHONLIB@`cd $(top_srcdir)/python/Lib;pwd`@" \
		$(srcdir)/py_start.sh > py_start.tmp
	chmod +x py_start.tmp
	chmod 555 py_start.tmp
	mv -f py_start.tmp py_start

logger: $(srcdir)/logger.sh Makefile
	$(RM) logger.tmp
	sed "s@PYTHONLIB@`cd $(top_srcdir)/python/Lib;pwd`@" \
		$(srcdir)/logger.sh > logger.tmp
	chmod +x logger.tmp
	chmod 555 logger.tmp
	mv -f logger.tmp logger
"""
