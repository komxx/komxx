#! @PYKOM@
# login_manager for WWW-kom.
# This is the web server that users talks to.  It multiplexes the
# request to the proper session_cache process.

import sys
import os

sys.path.insert(1, '@PYKOMLIB@')

os.chdir('@HTMLDIR@')

# Enable core dumps, but don't fail if the resource module is unavailable.
try:
    import resource
    (soft, hard) = resource.getrlimit(resource.RLIMIT_CORE)
    resource.setrlimit(resource.RLIMIT_CORE, (hard, hard))
except ImportError:
    pass

import lm

if len(sys.argv) > 1:
    debug = 1
else:
    debug = 0
    
lm.start_lm(debug, "@SERVER@", "@PORT@", 8439)
