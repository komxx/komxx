# Copyright (C) 1996  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Python 1.4beta2 and newer insists on adding the directory containing
# this script first on the path, which is not what I want, since the
# test suite tries to override the komconfig.py found in this
# directory with its own version.  At least Python 1.4beta3 adds a
# trailing slash to the last component of the path it adds, so remove
# it here.

import sys
if len(sys.path[0]) > 0 and sys.path[0][-1] == "/":
    del sys.path[0]

import signal
import string
import base64
import toolbar
import socket
import traceback
import os
import time

import komhtml
import komtext
import komexceptions
import myserver
import myhttpbase
import komxx
import komconfig
import siteconfig
import language
import log
import estring
import buildvers

from language import lingo

session = None
this_person = 0
true=1
false=0

class State:
    def __init__(self):
	self.password = None		# The correct password

state = State()

class sh(myhttpbase.myhttpbase):

    def send_response(self, code, message=None):
	"As myhttpbase.send_response, but adds an expired Expires field."
	myhttpbase.myhttpbase.send_response(self, code, message)
	self.send_header("Expires", self.date_time_string())
	self.reply_written = 1

    def prepare_request(self):
	global state
	self.reply_written = 0

	self.cookie_fetch()
	# FIXME: Handle async calls when idle, somehow.
	# Requires a new top-loop.
	while session.handle_async(20) == komxx.st_pending:
	    log.debug("Handling even more async...")
	self.url = komhtml.decodeurl(self.path)

	log.info("GET " + self.url.as_string_sans_hostpart_with_session())
	[user, password] = self.retrieve_authorization()
	self.set_cookie(user, session.my_login())

	if self.url.pers_no:
	    self.redirect()

	elif password != state.password:

	    log.info("Attempt to log in to " + `session.my_login()`
		     + " with bad password")

	    self.clear_cookie()
	    raise komhtml.bad_auth

	# The user is now authenticated.
	if session.my_login() == 0:
	    raise komexceptions.internal_error(
                "my_login() is 0 after successful login")

	# Read some info from the user-area.
	self.process_user_area()

	# Initialize toolbar
	self.toolbar = toolbar.toolbar()

	# Transfer the toolbar into the language package.
	language.toolbar = self.toolbar
	komhtml.text_to_mark = None

	# Set up the read-order if it is set in a cookie.
	self.fix_readorder()

	# Perform embedded commands
	for read_text in [self.url.getnum("is_read"), self.cookie_is_read()]:
	    if read_text:
		log.debug("Text " + `read_text` + " is read")
		session.remove_text(read_text)
		text = session.text(read_text)
		try:
		    text.mark_as_read()
		except komxx.err_no_such_text:
		    pass

    def process_user_area(self):
	tmp = user_area_get("color")
	if tmp != None:
	    lingo.color_mode(tmp)

	tmp = user_area_get("looknfeel")
	if tmp != None:
	    if tmp == "traditional" or \
	       tmp == "textonly" or \
	       tmp == "sillyicons":

		komhtml.setlooknfeel(tmp)
		lingo.layout_mode("traditional")
	    else:
		komhtml.setlooknfeel("traditional")
		lingo.layout_mode("July 1997")

    def fix_readorder(self):
	self.user_next_text = None
	self.user_current_conf = None
	self.user_pending_footnotes = []
	self.user_pending_comments = []
	data = self.cookie_read_order()
	log.debug("fix_readorder: got " + `data`)
	if data == None:
	    return
	fields = string.splitfields(data, ":")
	if len(fields) < 3:
	    return
	try:
	    self.user_current_conf = string.atoi(fields[0])
	    if len(fields[1]) != 0:
		self.user_next_text = string.atoi(fields[1])
	except ValueError:
	    log.notice("Malformed cookie '" + data + "' received")
	    return
	self.user_pending_footnotes = estring.xlisttoints(fields[2])
	if len(fields) == 4:
	    self.user_pending_comments = estring.xlisttoints(fields[3])

	if self.user_next_text != None:
	    if session.goto_conf(self.user_current_conf) == komxx.st_ok:
		log.debug(" Set current conf: " + `session.current_conf()`
			  + " user: " + `self.user_current_conf`)
		log.debug(" Unread texts: " + `session.list_unread_texts_in_current(20)`)
		session.set_pending_footnotes(self.user_pending_footnotes)
		session.set_pending_comments(self.user_pending_comments)
		log.debug(" Unread texts: " + `session.list_unread_texts_in_current(20)`)

    def do_GET(self):
	self.reply_written = 0
	signal.alarm(1800)
	try:
	    try:
		self.prepare_request()
		mname = "kom_GET_" + self.url.cmd
		if not hasattr(self, mname):
		    self.send_error(501)
		    return
		method = getattr(self, mname)
		self.toolbar.add_help(self.url.cmd)
		method(self.url)
	    except komhtml.bad_auth:
		self.bad_auth()
	    except komhtml.not_a_komurl, emsg:
		if emsg != None:
		    self.send_error(500, "not-a-komurl:" + emsg)
		else:
		    self.send_error(500, "not-a-komurl.")
	    except komexceptions.internal_error, e:
		self.send_error(500, "internal_error 1: %s" % e)
	    # FIXME: a nicer error page, thanks.
	    except komxx.errno_error, err:
		self.send_error(500, "During GET: unhandled LysKOM error "
				+ `err.errno` + " occured: " + err.errmsg)
		if sys.exc_type:
		    log.err("Caught exception: "
			    + `sys.exc_type` + " " + `sys.exc_value`)
		    for line in traceback.format_tb(sys.exc_traceback):
			log.err(line)
	except:
	    if self.reply_written == 0:
		log.err("No reply sent to query (exception)")
		self.send_error(500, "Internal error 2: unexpected exception")
		self.wfile.flush()
		if sys.exc_type:
		    log.err("Caught exception: "
			    + `sys.exc_type` + " " + `sys.exc_value`)
		    for line in traceback.format_tb(sys.exc_traceback):
			log.err(line)
                    log.err("Exception value: " + `sys.exc_value.args`)

	# Not that it matters much if we call user_active first or
	# last, but do it after all processing is finished so that it
	# doesn't degrade the performance the user sees in even the
	# slightest way.
	session.user_active()

	if self.reply_written == 0:
	    log.err("No reply sent to query (normal execution)")
	    self.send_error(500, "Internal error 3")
	    self.wfile.flush()

    def do_POST(self):
	# Note: do_POST is never used and probably contains errors.
	raise komhtml.not_a_komurl, "do_POST entered"
	signal.alarm(600)
	try:
	    self.prepare_request()
	    length = self.headers.getheader('Content-length')
	    if (self.headers.getheader('Content-type')
		!= "application/x-www-form-urlencoded"):
		self.log_error("Bad content-type"
			       + self.headers.getheader('Content-type'))
		self.send_error(400)
		return
	    body = self.rfile.read(string.atoi(length))
	    args = komhtml.form_data(body)
	    mname = "kom_POST_" + self.url.cmd
	    if not hasattr(self, mname):
		self.send_error(501)
		return
	    method = getattr(self, mname)
	    method(args)
	except komhtml.bad_auth:
	    self.bad_auth()
	except komhtml.not_a_komurl, emsg:
	    self.send_error(500, "not-a-komurl 2" + emsg)
	except komexceptions.internal_error, e:
	    self.send_error(500, "internal_error 3: %s" % e)
	except komxx.errno_error, err:
	    self.send_error(500, "During POST: unhandled LysKOM error "
			    + `err.errno` + " occured: " + err.errmsg)

    #
    # ----------------------------------------------------------------
    #                      GET_login and GET_home
    # ----------------------------------------------------------------
    #

    def kom_GET_login(self, args, is_login=1):
	texts_in_conf=50
	confs = user_area_get_num("home-max-confs", 0)

	if confs == 0:
	    confs = 10000
	    opt_limit = 0
	else:
	    opt_limit = confs + 1
	while session.prefetch(0, 0, 0,
			       0, texts_in_conf, confs+1,
			       40) == komxx.st_pending:
	    session.block(1000)

	unread_list=session.confs_with_estimated_unread(opt_limit)
	trunc=""
	if len(unread_list) > confs:
	    unread_list[-1:]=[]
	    trunc=lingo.there_are_more_unread_confs()

	conflist=[]
	sum = 0
	if 0:
	    # Pure text
	    for (unread_cno, num_unread) in unread_list:
		conf = komtext.safe_name_direct_conf(session, unread_cno)
		conflist.append(lingo.info_unread_conf(conf, num_unread))
		sum = sum + num_unread
	    conflist = string.join(conflist, "<br>\n")
	else:
	    # Using a table
	    conflist.append("<table>")
	    for (unread_cno, num_unread) in unread_list:
		conf = komtext.safe_name_direct_conf(session, unread_cno)
		conflist.append("<tr><td valign=top>" + `num_unread` + "</td><td>" + conf + "</td></tr>")
		sum = sum + num_unread
	    if len(conflist) == 1:
		conflist = ""
	    else:
		conflist.append("</table>")
		conflist = string.join(conflist, "\n")

	if len(unread_list) > 0:
	    # FIXME: this default should probably be according to
	    # the current contents of the readorder cookie.  This is bogus.
	    self.toolbar.add_default_conf(unread_list[0][0],
					  komtext.safe_name_nolink(
		session, unread_list[0][0]))
	else:
	    self.toolbar.add_default_time()

	self.toolbar.add_config()
	self.toolbar.add_create_conf()
	#self.toolbar.add_admin_conf()
	self.toolbar.add_create_user()

	if is_login:
	    if len(unread_list) > 0:
		self.cookies.append(("LysKOM_www_readorder",
				     `unread_list[0][0]` + ":::"))
	    else:
		self.cookies.append(("LysKOM_www_readorder", ""))

	self.respond(lingo.login(conflist, trunc, sum, is_login))

    def kom_GET_home(self, args):
	self.kom_GET_login(args, 0)


    #
    # ----------------------------------------------------------------
    #                            GET_text
    # ----------------------------------------------------------------
    #

    def kom_GET_text(self, args):
	tno = args.getnum("text_no")
	if tno == None:
	    raise komhtml.not_a_komurl, "text_no argument missing to /text"
	self.kom_get_text(tno, args.getstr_empty("style"))

    #
    # 
    #
    def kom_get_text(self, tno, style):
	text=session.text(tno)
	try:
	    if not self.supports_cookies:
		komhtml.text_to_mark = tno
	    komtext.prefetch_for_display(session, text)
	    tmp_map = komtext.map_for_display(session, text, style)
	    the_page = lingo.format_text(tmp_map)
	    self.toolbar.enable_bottom()
	except komxx.err_no_such_text:
	    the_page = lingo.nonexisting_text_long()
	self.toolbar.add_comment(tno)
	self.toolbar.add_answer(tno)
	subtitle = ""
	if session.current_conf() != 0:
	    self.toolbar.add_write(session.current_conf())
	    self.toolbar.add_review(session.current_conf())
	    self.toolbar.add_current_conf(session.current_conf())
	    session.enqueue_children(tno)
	    # FIXME: Swedish! Be gone!
	    subtitle = (subtitle + "%d ol�sta texter. " %
			session.estimated_unread_texts_in_current())
	    position = komtext.safe_name(session, session.current_conf())
        else:
            position = ""
	subtitle = subtitle + "%d ol�sta m�ten." % len(
	    session.confs_with_unread())
	session.remove_text(tno)
	self.fix_default(1)
	if style == "pre":
	    self.toolbar.add_text_formatted(tno)
	else:
	    self.toolbar.add_text_unformatted(tno)
	self.cookies.append(("LysKOM_is_read", `tno`))
	self.toolbar.enable_textops()
	# FIXME: Swedish!  Be gone!
	self.respond(lingo.page("Text " + `tno`, "", the_page,
				position, subtitle))

    def fix_default(self, do_set_cookie = 0):
	if do_set_cookie == 0:
	    # Form the default action depending on what the user said
	    # was the default action, but within certain limits.
	    if self.user_next_text != None:
		if len(self.user_pending_footnotes):
		    self.toolbar.add_default_footnote(self.user_next_text)
		elif len(self.user_pending_comments):
		    self.toolbar.add_default_comment(self.user_next_text)
		else:
		    self.toolbar.add_default_text(self.user_next_text)
		return
	    elif self.user_current_conf != None and self.user_current_conf !=0:
		self.toolbar.add_default_conf(self.user_current_conf,
					      komtext.safe_name_nolink(
						  session,
						  self.user_current_conf))
		return

	    # No cookie given, so try to figure out a good default
	    # action and set a cookie.
	    log.debug("No cookie detected by fix_default; will set one")
		
	prompt = session.next_prompt()
	cookie = None
	while prompt == 6:
	    prompt = session.next_prompt()
	log.debug("Next prompt: " + `prompt`
		  + " Current conf: " + `session.current_conf()`
		  + " user: " + `self.user_current_conf`)
	if prompt == 1:
	    # Next conf
	    cno = session.query_next_conf()
	    if cno != 0:
		self.toolbar.add_default_conf(
		    cno,
		    komtext.safe_name_nolink(session, cno))
		cookie = `cno` + ":::"
	    else:
		log.err("Internal error detected in set_unread_cookie")
	elif prompt == 5:
	    # Nothing to do
	    cookie = `session.current_conf()` + ":::"
	    self.toolbar.add_default_time()
	else:
	    # Something to do.
	    tno = session.next_text()
	    if prompt == 2:
		self.toolbar.add_default_text(tno)
	    elif prompt == 3:
		self.toolbar.add_default_comment(tno)
	    elif prompt == 4:
		self.toolbar.add_default_footnote(tno)
	    else:
		log.err("Read-order error " + `prompt`
			+ "detected in set_unread_cookie.")
	    footnotes = estring.intstoxlist(session.get_pending_footnotes())
	    comments = estring.intstoxlist(session.get_pending_comments())
	    cookie = `session.current_conf()` + ":" + `tno` + ":" \
		     + footnotes + ":" + comments
	if cookie == None:
	    log.err("Cookie not set by fix_default")
	else:
	    self.cookies.append(("LysKOM_www_readorder", cookie))

    #
    # ----------------------------------------------------------------
    #                            GET_move_text
    # ----------------------------------------------------------------
    #

    def kom_GET_move_text(self, args):
	tno = args.getnum("text_no")
	if tno == None:
	    raise komhtml.not_a_komurl, "no text_no to /move_text"

	return self.do_move_text(tno)

    def do_move_text(self, tno, errortxt = None):
	# Prepare the map for the form.
	map = {}
	map["text-no"] = `tno`
	if not errortxt:
	    map["error"] = ""
	else:
	    map["error"] = '<p><font color="red">' + errortxt + "</font>"

	reciplist = []
	text = session.text(tno)
	for rec in text.recipients():
	    recip_confno = rec.recipient()
	    try:
		recip_conf_name = session.conf(recip_confno).name()
	    except:
		recip_conf_name = lingo.undef_conf(recip_confno)

	    reciplist.append('<tr><td><input type="checkbox" name="delrecip" value="' +
				`recip_confno` + '"></td><td>' + 
				komhtml.htmlquote(recip_conf_name) +
				'</td></tr>')

	map["recipients"] = string.join(reciplist, "\n")

	self.respond(lingo.move_text(map))


    def kom_GET_move_text2(self, args):
	tno = args.getnum("text_no")
	if tno == None:
	    raise komhtml.not_a_komurl, "no text_no to /move_text"
	text = session.text(tno)

	# New recipient
	new_recip_name = args.getstr("new_recipient")
	if new_recip_name == None:
	    return self.do_move_text(tno, "Du m�ste ange en ny mottagare")
	    # FIXME: Det kanske man inte m�ste...

	matches = session.lookup_z_name(new_recip_name, 1, 1)
	if matches == None or len(matches) == 0:
	    return self.do_move_text(tno,
				     'Det fanns inget m�te med namnet "' + 
				     new_recip_name + '".')
	elif len(matches) == 1:

	    # We have ONE conference.  Now try to add this as a recipient
	    conf = session.conf(matches[0].conf_no())

	    try:
		text.add_recipient(conf.conf_no())
	    except:
		# FIXME: Dela upp felen p� olika kategorier.
		return self.do_move_text(tno,
					 'Det gick inte att flytta texten till det m�tet')

	else:
	    l = []
	    l.append("F�ljande m�ten eller personer matchade namnet:<br>")
	    l.append("<ul>")
	    for m in matches:
		conf = session.conf(m.conf_no())
		name = conf.name()
		l.append("<li>" + name + "<br>")
	    l.append("</ul>")
	    l.append("Var god v�lj en av dem.<br>")
	    return self.do_move_text(tno, string.join(l, "\n") + "\n")

	# Deletion of recipients
	delrecip = args.getnumlist("delrecip")
	notdeleted = []
	for recip in delrecip:
	    try:
		text.sub_recipient(recip)
	    except:
		notdeleted.append(recip)
	if len(notdeleted) > 0:
	    return self.do_move_text(tno,
				     "Det gick inte att ta bort alla valda mottagare")
	map = {}
	map["text-no"] = `tno`
	map["current-conf"] = `session.conf(session.current_conf()).name()`
	prompt = session.next_prompt()
	if 2 <= prompt and prompt <= 4:
	    next_tno = session.next_text()
	    map["next-action"] = komhtml.komurl("text?text_no=" + `next_tno`,
						"G� till n�sta text")
	else:
	    map["next-action"] = komhtml.anchor(komhtml.komurl("home"),
						"G� tillbaka")
	    
	self.respond(lingo.move_text_completed(map))


    #
    # ----------------------------------------------------------------
    #

    def kom_GET_conf(self, args):
	cno = args.getnum("conf_no")
	if cno == None:
	    raise komhtml.not_a_komurl, "no conf_no to /conf"
	self.kom_get_conf(cno)

    def kom_get_conf(self, cno):
	texts_to_display = user_area_get_num("conf-max-texts", 40)
	set_unread_threshold = user_area_get_num("conf-unread-threshold", 20)

	texts_to_prefetch = texts_to_display + 1
	conf=session.conf(cno)
	if session.goto_conf(cno) != komxx.st_ok:
	    # FIXME: better error message
	    raise komhtml.not_a_komurl, "Couldn't go to the conf"

	# Prefetch information for the list.
	# FIXME: I want to exit the loop as soon as all prefetch
	# questions have been issued, even if some have not yet
	# returned.
	while session.prefetch(0, texts_to_prefetch, 1, \
			       0, texts_to_prefetch, 1, \
			       40) == komxx.st_pending:
	    session.block(1000)

	unread=session.list_unread_texts_in_current(texts_to_display + 1)
	estim = session.estimated_unread_texts_in_current()

	# Should the unread button be present?
	if len(unread) >= set_unread_threshold:
	    include_unread = 1
	elif set_unread_threshold <= texts_to_display:
	    include_unread = 0
	else:
	    include_unread = estim >= set_unread_threshold


	# Are we truncating the list?
	if len(unread) > texts_to_display:
	    unread = unread[:texts_to_display]
	    truncated = 1
	else:
	    truncated = 0

	# Keep on prefetching...
	unprefetched_authors=[]
	for (text, level) in unread:
	    komtext.prefetch_author(session, session.text(text),
				    unprefetched_authors)
	for t in unprefetched_authors:
	    if t.exists():
		session.conf(t.author()).prefetch_uconf()

	# Format the list of unread texts.
	prev_level = -1
	prev_subject = None
	unread_list = []
	use_link = user_area_get("conf-authors-linked") == "on"
	set_subject = user_area_get("conf-compact-duplicates") != "off"
	for (text, level) in unread:
	    while prev_level < level:
		prev_level = prev_level + 1
		unread_list.append("<ul>")
	    while prev_level > level:
		prev_level = prev_level - 1
		unread_list.append("</ul>")
	    t = session.text(text)
	    subject = t.subject()
	    unread_list.append("<li>" + lingo.text_link(
		text, t.creation_time(),
		subject, prev_subject,
		komtext.safe_author(session, text, "by", use_link)))
	    if set_subject:
		prev_subject = subject

	while prev_level > -1:
	    prev_level = prev_level - 1
	    unread_list.append("</ul>")

	# 
	self.fix_default(1)
	self.toolbar.add_write(cno)
	self.toolbar.add_admin_conf(cno)
	self.toolbar.add_review(cno)
	self.toolbar.enable_bottom()
	name_link = komtext.safe_name(session, cno)
	name_nolink = komtext.safe_name_nolink(session, cno)
	self.respond(lingo.conf(session, cno, name_link, name_nolink,
				unread_list, truncated, include_unread,
				estim))

    def kom_GET_comment(self, args):
	tno = args.getnum("parent")
	if tno == None:
	    raise komhtml.not_a_komurl, "missing argument parent to /comment"
	text=session.text(tno)
	text.prefetch()
	#
	# Prefetch everything needed to comment text TEXT
	#
	author=session.conf(text.author())
	author.prefetch_uconf()
	for rcpt in text.recipients():
	    if not rcpt.is_carbon_copy():
		session.conf(rcpt.recipient()).prefetch()
	#
	# Prepare the page
	#
	form = komtext.text_creation_form(session)
	form.setup_comment_to(tno)
	if session.current_conf() != 0:
	    self.toolbar.add_current_conf(session.current_conf())
	    self.toolbar.add_write(session.current_conf())
	    self.toolbar.add_review(session.current_conf())
	self.respond(lingo.write_comment(form.as_html_form(),
					 komhtml.text_as_link(tno),
					 komtext.safe_author(session, tno,
							     "by")))

    def kom_GET_answer(self, args):
	tno = args.getnum("parent")
	if tno == None:
	    raise komhtml.not_a_komurl, "missing argument parent to /answer"
	text=session.text(tno)
	text.prefetch()

	if text.author() == 0:
	    self.respond(lingo.cannot_answer_anonymous())
	    return
	#
	# Prefetch everything needed to answer text TEXT
	#
	author=session.conf(text.author())
	author.prefetch_uconf()
	try:
	    author.name()
	except (komxx.err_undef_conf, komxx.err_undef_pers, \
		komxx.err_access, komxx.err_perm):
	    
	    self.respond(lingo.cannot_answer_dead())
	    return
	#
	# Prepare the page
	#
	form = komtext.text_creation_form(session)
	form.setup_personal_answer_to(tno)
	if session.current_conf() != 0:
	    self.toolbar.add_current_conf(session.current_conf())
	    self.toolbar.add_write(session.current_conf())
	    self.toolbar.add_review(session.current_conf())
	self.respond(lingo.write_personal_answer(
	    form.as_html_form(), komhtml.text_as_link(tno),
	    komtext.safe_author(session, tno, "by")))

    def kom_GET_write_text(self, args):
	cno = args.getnum("conf_no")
	if cno == None:
	    raise komhtml.not_a_komurl, "missing arg conf_no to /write_text"
	if cno == 0:
	    raise komhtml.not_a_komurl, "bad arg conf_no=0 to /write_text"
	conf=session.conf(cno)
	conf.prefetch()
	#
	# Prepare the page
	#
	form = komtext.text_creation_form(session)
	form.add_recipient(cno)
	form.maybe_add_authors_as_recipients()
	self.toolbar.add_default_conf(cno,
				      komtext.safe_name_nolink(session, cno))
	self.respond(lingo.write_text(form.as_html_form(),
				      komtext.safe_name(session, cno)))

    def kom_GET_submit_text(self, args):
	form = komtext.text_creation_form(session)
	form.init_from_form(args)
	new_comments = form.new_comments()
	if len(new_comments) > 0:
	    for c in new_comments:
		komtext.prefetch_for_display(session, session.text(c))
	    new_texts=[]
	    for c in new_comments:
		tmp_map = komtext.map_for_display(session, session.text(c), "")
		new_texts.append(lingo.format_text(tmp_map))
	    self.fix_default()
	    form.setup_known_comments()
	    self.respond(lingo.new_texts_exists(new_texts,
						form.as_html_form()))
	else:
	    tno = form.create_text()
	    self.send_redirect(komhtml.komurl("text?text_no=" + `tno`))
	
    # ----------------------------------------------------------------

    def kom_GET_review_texts(self, args):
	cno = args.getnum("conf_no")
	if cno == None:
	    raise komhtml.not_a_komurl, "missing arg conf_no to /review_texts"
	if cno == 0:
	    raise komhtml.not_a_komurl, "bad arg conf_no=0 to /review_texts"
	conf=session.conf(cno)
	conf.prefetch()
	#
	# Prepare the page
	#
	# FIXME: See GET_write_text for a way to create a form...
	self.respond(lingo.review_texts())

    # ----------------------------------------------------------------

    def kom_GET_kill(self, args):
	self.respond(lingo.killed())
	sys.stdout.flush()
	sys.stderr.flush()
	self.wfile.flush()
	signal.alarm(1)

    def kom_GET_time(self, args):
	self.fix_default()
	self.respond(lingo.time(session.get_time(),
				time.localtime(time.time())))

    def kom_GET_select_conf_form(self, args):
	self.fix_default()
	self.respond(lingo.select_conf_form())

    def kom_GET_submit_select_conf(self, args):
	domain = args.getstr_empty("domain")
	search_conf = 0
	search_pers = 0
	if domain == "conf":
	    search_conf = 1
	elif domain == "pers":
	    search_pers = 1
	elif domain == "both":
	    search_conf = 1
	    search_pers = 1
	else:
	    search_conf = 1
	matches = session.lookup_z_name(args.getstr_empty("conf_name"),
					search_pers, search_conf)
	if len(matches) == 1:
	    return self.kom_get_conf_stat(matches[0].conf_no())
	else:
	    self.respond(lingo.list_conf(matches))

    #
    # ----------------------------------------------------------------
    #                            GET_conf_stat
    # ----------------------------------------------------------------
    #

    def kom_GET_conf_stat(self, args):
	cno = args.getnum("conf_no")
	if cno == None:
	    raise komhtml.not_a_komurl, "missing conf_no to /conf_stat"
	if cno == 0:
	    raise komhtml.not_a_komurl, "bad conf_no=0 to /conf_stat"
	self.kom_get_conf_stat(cno)


    def kom_get_conf_stat(self, cno):
	session.prefetch_is_member(session.my_login(), cno)
	conf = session.conf(cno)
	name_nolink = komtext.safe_name_nolink(session, cno)
	tmp_map = {}
	tmp_map["name"] = komtext.safe_name(session, cno)
	tmp_map["name_nolink"] = name_nolink
	tmp_map["conf_no"] = cno
	tmp_map["creator"] = komtext.safe_name(session, conf.creator())
	tmp_map["supervisor"] = komtext.safe_name(session, conf.supervisor())
	tmp_map["super-conf"] = komtext.safe_name(session, conf.super_conf())

	tmp_map["permitted_submitters"] = komtext.safe_name(
	    session, conf.permitted_submitters())

	# Presentation link.
	if conf.presentation() == 0:
	    tmp_map["presentation"] = None
	else:
	    tmp_map["presentation"] = lingo.format_presentation(
		komtext.map_for_display(session,
					session.text(conf.presentation()),
					""),
		name_nolink)

	if session.is_supervisor(session.my_login(), cno):
	    k = komhtml.komurl("set_presentation?conf_no=" + `cno`)
	    if conf.presentation() == 0:
		tmp_map["change-presentation"] = lingo.create_presentation(k)
	    else:
		tmp_map["change-presentation"] = lingo.change_presentation(k)
	else:
	    tmp_map["change-presentation"] = ""

	# Motd
	if conf.msg_of_day() != 0:
	    tmp_map["motd"] = lingo.format_motd(
		komtext.map_for_display(session,
					session.text(conf.msg_of_day()),
					""),
		name_nolink)
	    # FIXME: implemente /remove_motd
	    # if session.is_supervisor(session.my_login(), cno):
	    # tmp_map["remove-motd"] = komhtml.komurl(
	    # "remove_motd?conf_no=" + `cno`)
	else:
	    tmp_map["motd"] = ""

	# FIXME: set motd

	tmp_map["join"] = ("join", [("conf_no", `cno`)])
	tmp_map["disjoin"] = ("disjoin", [("conf_no", `cno`)])
	tmp_map["goto"] = komhtml.komurl("conf?conf_no=" + `cno`)
	if session.is_member(session.my_login(), cno):
	    currently_member = 1
	    tmp_map["estimated"] = session.estimated_unread_texts_in_conf(cno)
	else:
	    currently_member = 0
	    # FIXME: Need to check if the user has enabled his powers.
	    if (session.my_login() == cno \
		or session.is_supervisor(session.my_login(), cno) \
		or session.is_member(session.my_login(), cno) \
		or not conf.type().is_protected()):

		tmp_map["join_allowed"] = 1
	    else:
		tmp_map["join_allowed"] = 0

	self.toolbar.add_current_conf(cno)
	self.toolbar.add_write(cno)
	self.toolbar.add_admin_conf(cno)
	self.toolbar.add_review(cno)
	self.fix_default()
	self.respond(lingo.conf_stat(
	    tmp_map, currently_member,
	    session.is_supervisor(session.my_login(), cno),
	    conf))

    #
    # ----------------------------------------------------------------
    #                            GET_disjoin
    # ----------------------------------------------------------------
    #

    def kom_GET_disjoin(self, args):
	cno = args.getnum("conf_no")
	if cno == None:
	    raise komhtml.not_a_komurl, "missing conf_no arg to /disjoin"
	if cno == 0:
	    raise komhtml.not_a_komurl, "bad conf_no=0 to /disjoin"
	# FIXME: improved error handling.
	session.disjoin_conference(cno)

	# Clear the cookie so that no attempt to go to the conference
	# again is made.
	if self.user_current_conf == cno:
	    self.cookies.append(("LysKOM_www_readorder", ""))

	self.send_redirect(komhtml.komurl("conf_stat?conf_no=" + `cno`))

    def kom_GET_join(self, args):
	cno = args.getnum("conf_no")
	if cno == None:
	    raise komhtml.not_a_komurl, "missing conf_no arg to /join"
	# FIXME: correct level and placement?
	session.join_conference(cno, 100, 1, komxx.membership_type())
	self.send_redirect(komhtml.komurl("conf_stat?conf_no=" + `cno`))

    def kom_GET_set_presentation(self, args):
	cno = args.getnum("conf_no")
	if cno == None:
	    raise komhtml.not_a_komurl,\
		  "missing argument conf_no to /set_presentation"

	form = komtext.text_creation_form(session)
	form.setup_set_presentation(cno)
	if session.current_conf() != 0:
	    self.toolbar.add_current_conf(session.current_conf())
	    self.toolbar.add_write(session.current_conf())
	    self.toolbar.add_review(session.current_conf())
	self.respond(lingo.write_presentation(form.as_html_form(),
					      session.conf(cno).name()))
	
    def kom_GET_who_is_on(self, args):
	wholist = session.who_is_on()

	cno = args.getnum("conf_no")

	if cno == 0:
	    cno = None

	# Prefetch the name for the person and his working conference
	for w in wholist:
	    if cno == None or w[1] == cno:
		session.conf(w[0]).prefetch_uconf()
		session.conf(w[1]).prefetch_uconf()

	wholist2 = []
	for w in wholist:
	    if cno == None or w[1] == cno:
		perslink = komtext.safe_name(session, w[0])
		if w[1] == 0:
		    confname = lingo.not_present()
		else:
		    confname = komtext.safe_name(session, w[1])

		if w[2] == session.my_session():
		    item = ("<tr valign=top>" 
			    + "<td><b>" + `w[2]` 
			    + "</b><td><b>" + perslink
			    + "</b><td><b>" + confname
			    + "</b></tr>")
		else:
		    item = ("<tr valign=top>" 
			    + "<td>" + `w[2]` 
			    + "<td>" + perslink
			    + "<td>" + confname
			    + "</tr>")
		wholist2.append(item)

	self.fix_default()
	if cno == None:
	    self.respond(lingo.who_is_on(wholist2))
	else:
	    self.respond(lingo.who_is_on_conf(komtext.safe_name(session,cno),
					      wholist2))

    def kom_GET_set_unread(self, args):
	cno = args.getnum("conf_no")
	days = args.getnum("unread_days")
	txts = args.getnum("unread_texts")
	if cno == None:
	    raise komhtml.not_a_komurl, "Missing arg conf_no to /set_unread"
	if (days == None and txts == None) or (days != None and txts != None):
	    self.respond(lingo.set_unread_need_one())

	if days != None:
	    timeval = time.localtime(time.time() - 86400 * days)
	    timeval = (timeval[0], timeval[1], timeval[2], 0, 0, 0,
		       timeval[6], timeval[7], timeval[8])
	    lno = session.last_local_before(cno, timeval)
	else:
	    conf = session.conf(cno)
	    lno = conf.first_local_no() + conf.no_of_texts() - 1 - txts;
	session.set_last_read(cno, lno)
	self.kom_get_conf(cno)

    def kom_GET_configuration(self, args):
	tmp_map={"ec-off":"",
	     "ec-ask-off":"",
	     "ec-ask-on":"",
	     "ec-on":"",
	     "lnf-default-checked":"",
	     "lnf-traditional-checked":"",
	     "lnf-text-checked":"",
	     "lnf-silly-checked":"",
	     "lnf-default-demo":komhtml.iconref("comment",
						lingo.alt_comment(),
						"traditional"),
	     "lnf-text-demo":komhtml.iconref("comment",
					     lingo.alt_comment(), "textonly"),
	     "lnf-silly-demo":komhtml.iconref("comment",
					      lingo.alt_comment(),
					      "sillyicons"),
	     "home-demo":komhtml.iconref("home", lingo.alt_home()),
	     "conf-demo":komhtml.iconref("board", lingo.alt_conf()),
	     "cal-on-checked":"",
	     "cal-off-checked":"",
	     "ccd-on-checked":"",
	     "ccd-off-checked":"",
	     "color-default":"",
	     "color-none":""}

	# Number of conferences with unread to show on the login/home page.

	tmp = user_area_get("home-max-confs")
	if tmp == "0" or tmp == None:
	    tmp = ""
	tmp_map["home-max-confs"] = tmp

	# Conversion of text from swascii to iso 8859-1 on entry?

	tmp = user_area_get("entry-conversion")
	if tmp == "off":
	    tmp_map["ec-off"] = "checked"
	elif tmp == "ask-off":
	    tmp_map["ec-ask-off"] = "checked"
	elif tmp == "ask-on":
	    tmp_map["ec-ask-on"] = "checked"
	elif tmp == "on":
	    tmp_map["ec-on"] = "checked"
	else:
	    # This is the default
	    tmp_map["ec-off"] = "checked"

	# Should authors on the /conf page be links?

	tmp = user_area_get("conf-authors-linked")
	if tmp == "on":
	    tmp_map["cal-on-checked"] = "checked"
	elif tmp == "off":
	    tmp_map["cal-off-checked"] = "checked"
	else:
	    # This is the default
	    tmp_map["cal-off-checked"] = "checked"

	# Should repeated subjects on the /conf page be compressed?

	tmp = user_area_get("conf-compact-duplicates")
	if tmp == "on":
	    tmp_map["ccd-on-checked"] = "checked"
	elif tmp == "off":
	    tmp_map["ccd-off-checked"] = "checked"
	else:
	    # This is the default
	    tmp_map["ccd-on-checked"] = "checked"

	# Display color?

	tmp = user_area_get("color")
	if tmp == "none":
	    tmp_map["color-none"] = "checked"
	elif tmp == "default":
	    tmp_map["color-default"] = "checked"
	else:
	    # This is the default
	    tmp_map["color-default"] = "checked"

	# Toolbar style?

	tmp = user_area_get("looknfeel")
	if tmp == "default":
	    tmp_map["lnf-default-checked"] = "checked"
	elif tmp == "traditional":
	    tmp_map["lnf-traditional-checked"] = "checked"
	elif tmp == "textonly":
	    tmp_map["lnf-text-checked"] = "checked"
	elif tmp == "sillyicons":
	    tmp_map["lnf-silly-checked"] = "checked"
	else:
	    tmp_map["lnf-default-checked"] = "checked"
	self.respond(lingo.configuration(tmp_map))

    def kom_GET_submit_configuration(self, args):
	for config in ["color", "entry-conversion", "looknfeel",
		       "home-max-confs", "conf-authors-linked",
		       "conf-compact-duplicates"]:
	    val = args.getstr(config)
	    if val != None:
		session.user_area_set("WWW-kom", config, val)
	    else:
		if config == "home-max-confs":
		    session.user_area_set("WWW-kom", config, "0")

	session.user_area_store()
	self.process_user_area()
	self.send_redirect(komhtml.komurl("home"))

    #
    # ----------------------------------------------------------------
    #                   Create a new conference
    # ----------------------------------------------------------------
    #

    def kom_GET_create_conf_form(self, args):
	self.respond(lingo.create_conf())

    def kom_GET_submit_create_conf(self, args):
	name = args.getstr("conf_name")
	if name == None:
	    self.respond(lingo.create_conf_empty_name())
	    return
	prot = args.getnum("prot")
	orig = args.getnum("orig")
	secr = args.getnum("secr")
	allow_anon = args.getnum("allow_anon")
	forbid_secret = args.getnum("forbid_secret")
	if prot == None or orig == None or secr == None:
	    raise komhtml.not_a_komurl, "missing prot or orig or secr"
	if allow_anon is None or forbid_secret is None:
	    raise komhtml.not_a_komurl("missing allow_anon or forbid_secret")
	aux = []
	for s in args.getstrlist("refuse_import"):
	    if s in ["all", "spam", "html"]:
		aux.append(komxx.aux_item(35, s))
	    else:
		raise komhtml.not_a_komurl("bad refuse_import value")
	try:
	    cno = session.create_conference(name, prot, orig, secr,
					    aux, allow_anon, forbid_secret)
	    self.send_redirect(komhtml.komurl("conf_stat?conf_no=" + `cno`))
	except komxx.err_long_str:
	    self.respond(lingo.create_conf_long_name())
	except komxx.err_bad_name:
	    self.respond(lingo.create_conf_bad_name())
	except komxx.err_conf_exists:
	    self.respond(lingo.create_conf_conf_exists())
	except komxx.err_secret_public:
	    self.respond(lingo.create_conf_secret_public())
	except komxx.err_illegal_aux_item:
	    self.respond(lingo.create_conf_illegal_aux_item())
	except komxx.err_aux_item_permission:
	    self.respond(lingo.create_conf_aux_item_permission())
	    

    #
    # ----------------------------------------------------------------
    #                   Administer a new conference
    # ----------------------------------------------------------------
    #

    def kom_GET_admin_conf(self, args):
	# Get the conference
	cno = args.getnum("conf_no")
	if cno == None:
	    raise komhtml.not_a_komurl, "missing arg conf_no to /admin_conf"
	if cno == 0:
	    raise komhtml.not_a_komurl, "bad arg conf_no=0 to /admin_conf"

	return self.do_admin_conf(cno)


    def do_admin_conf(self, cno):

	conf=session.conf(cno)
	#if not session.is_supervisor(session.my_login, cno):
	#    pass

	# Prepare the map for the form.
	map = {}
	map["conf-no"] = `cno`
	map["conf-name"] = conf.name()

	conftype=conf.type()
	if conftype.is_protected():
	    map["protected-checked"]   = "checked"
	    map["unprotected-checked"] = ""
	else:
	    map["protected-checked"]   = ""
	    map["unprotected-checked"] = "checked"
	if conftype.is_original():
	    map["orig-checked"]     = "checked"
	    map["non-orig-checked"] = ""
	else:
	    map["orig-checked"]     = ""
	    map["non-orig-checked"] = "checked"
	if conftype.is_secret():
	    map["secret-checked"]    = "checked"
	    map["nonsecret-checked"] = ""
	else:
	    map["secret-checked"]    = ""
	    map["nonsecret-checked"] = "checked"
	    
	members = session.get_members(cno, 0, 16383)
	memblist = []
	for memb in members:
	    session.conf(memb).prefetch()
	for memb in members:
	    conf = session.conf(memb)
	    if conf.exists():
		memblist.append('<tr><td><input type="checkbox" name="delmember" value="' +
				`memb` + '"></td><td>' + 
				komhtml.htmlquote(conf.name()) +
				'</td></tr>')
	map["members"] = string.join(memblist, "\n")
		
	self.respond(lingo.admin_conf(map))


    def kom_GET_submit_admin_conf(self, args):
	# Get the conference
	cno = args.getnum("conf_no")
	if cno == None:
	    raise komhtml.not_a_komurl, "missing arg conf_no to /submit_admin_conf"
	if cno == 0:
	    raise komhtml.not_a_komurl, "bad arg conf_no=0 to /submit_admin_conf"
	conf = session.conf(cno)
	if not conf.exists():
	    raise komhtml.not_a_komurl, "conference " + `cno` + " doesn't exist"

	# Get rest of data
	try:
	    newname = args.getstr("conf_name")
	    newprot = int(args.getnum("prot"))
	    neworig = int(args.getnum("orig"))
	    newsecr = int(args.getnum("secr"))
	    if (newname == None or newprot == None or neworig == None or newsecr == None):
		raise komhtml.not_a_komurl, "missing name or prot or orig or secr"
	except:
	    raise komhtml.not_a_komurl, "Wrong type in prot or orig or secr"

	# Check if name has changed
	if newname != None and len(newname) > 0 and newname != conf.name():
	    conf.change_name(newname)

	# Check if type has changed
	conftype=conf.type()
	prot = 0
	if conftype.is_protected():
	    prot = 1
	orig = 0
	if conftype.is_original():
	    orig = 1
	secr = 0
	if conftype.is_secret():
	    secr = 1

	if prot != newprot or orig != neworig or secr != newsecr:
	    conf.set_type((newprot, neworig, newsecr, conftype.is_letterbox()))

	self.do_admin_conf(cno)


    def kom_GET_submit_admin_members(self, args):
	# Get the conference
	cno = args.getnum("conf_no")
	if cno == None:
	    raise komhtml.not_a_komurl, "missing arg conf_no to /submit_admin_conf"
	if cno == 0:
	    raise komhtml.not_a_komurl, "bad arg conf_no=0 to /submit_admin_conf"
	conf = session.conf(cno)
	if not conf.exists():
	    raise komhtml.not_a_komurl, "conference " + `cno` + " doesn't exist"

	map = {}
	map["conf-no"]     = `cno`
	map["conf-name"]   = conf.name()

	# Deletion of members
	del_members = args.getnumlist("delmember")
	deleted = []
	not_deleted = []
	for memb in del_members:
	    conf = session.conf(memb)
	    if conf.exists():
		name = conf.name()
	    else:
		name = "Person " + `memb`
	    try:
		session.sub_member(cno, memb)
		deleted.append(name)
	    except:
		not_deleted.append(name)
	if len(deleted) == 0:
	    map["del-members"] = "[Ingen person borttagen]<br>"
	    if len(not_deleted) > 0:
		map["del-members"] = map["del-members"] + "[N�gra personer kunde inte tas bort, troligen pga bristande r�ttigheter.]<br>"
	else:
	    map["del-members"] = string.join(deleted, "<br>\n") + "<br>\n"
	
	# New member
	new_member = args.getstr("new_member")
	if new_member == None:
	    map["new-member"] = "[Ingen person tillagd]"
	else:
	    new_members = session.lookup_z_name(new_member, 1, 0)
	    if new_members == None or len(new_members) == 0:
		map["new-member"] = "[Ingen person med namnet " + new_member + " funnen.]"
	    elif len(new_members) == 1:
		try:
		    session.add_member(cno, new_members[0].conf_no(), 100, 1,
				       komxx.membership_type())
		    map["new-member"] = new_members[0].name() + "<br>\n"
		except:
		    map["new-member"] = "Du kunde inte l�gga till personen " + new_members[0].name() + " till m�tet, troligen pga bristande r�ttigheter."
	    else:
		l = []
		l.append("[F�ljande personer matchade namnet:<br>")
		l.append("<ul>")
		for nm in new_members:
		    l.append("<li>" + nm.name() + "<br>")
		l.append("</ul>")
		l.append("Var god v�lj en av dem.]<br>")
		map["new-member"] = string.join(l, "\n") + "\n"

	self.respond(lingo.submit_admin_members(map))


    #
    # ----------------------------------------------------------------
    #

    def kom_GET_create_user_form(self, args):
	self.respond(lingo.create_user())

    def kom_GET_submit_create_user(self, args):
	username  = args.getstr("username")
	password  = args.getstr("password")
	passcheck = args.getstr("passcheck")

	if username == None or password == None or passcheck == None:
	    self.respond(lingo.create_conf_empty_name())
	    return

	if password != passcheck:
	    self.respond(lingo.different_passwords())
	    return

	pno = session.create_person(username, password, [], false)
	if (pno == komxx.st_error):
	    self.respond(lingo.person_creation_failed(name))
	else:
	    self.respond(lingo.creation_done_sc(username, pno))
	return

    #
    # ----------------------------------------------------------------
    #

def detach():
    global syncw

    if os.getppid() != 1:
	try:
	    signal.signal(signal.SIGTTOU, signal.SIG_IGN)
	except:
	    pass
	try:
	    signal.signal(signal.SIGTTIN, signal.SIG_IGN)
	except:
	    pass
	try:
	    signal.signal(signal.SIGTSTP, signal.SIG_IGN)
	except:
	    pass

	(syncr, syncw) = os.pipe()

	child = os.fork()
	if child < 0:
	    log.err("fork failed")
	    sys.exit(1)
	elif child > 0:
	    os.close(syncw)
	    os.read(syncr, 1)
	    os._exit(0)	# parent
	
	# child
	os.close(syncr)

	try:
	    os.setsid()
	except:
	    pass

def start_httpd(lyskom_host, lyskom_port):
    global session
    global state

    # Allocate a port for this session.
    for port in range(8010, 8500):
	try:
	    httpd = myserver.myserver((komconfig.wwwhost, port), sh)
            break
	except socket.error:
	    # Try next port in the range
            sys.stdout.flush()
	    pass
    else:
        # FIXME: error handling
        pass
    print "Port:", port
    sys.stdout.flush()

    # Read person and password for this session from the login_manager.
    log.debug("...reading Person:")
    pers_no = string.atoi(estring.read_word(sys.stdin, "Person:"))
    log.debug("...got " + `pers_no`)
    log.debug("...reading Password:")
    state.password = base64.decodestring(estring.read_word(
            sys.stdin, "Password:"))
    log.debug("...got something")

    # Establish a connection to the LysKOM server.
    try:
        session = komxx.new_session(lyskom_host, lyskom_port,
                                    "WWW-kom",
                                    "0.7.post.7" + buildvers.version)
    except komxx.no_connection:
        print "Connection refused"
        sys.stdout.flush()
        log.notice("Failed to connect to LysKOM server at "
                   + lyskom_host + ":" + lyskom_port)
        os.write(syncw, "X")
        sys.exit(1)

    if session.login(pers_no, state.password) == komxx.st_ok:
        print "Login OK"
        sys.stdout.flush()
    else:
        # FIXME: need a way to get the error code!
        # This should return different error messages
        # depending on what went wrong.
        print "Bad password"
        sys.stdout.flush()
        log.notice("Bad start-up password or user; exiting")
        os.write(syncw, "X")
        sys.exit(1)

    log.debug("closing everything")
    log.close()
    for i in range(3):
        try:
            os.close(i)
        except:
            pass

    # FIXME: nothing should write to these files, but several
    # things does.  Remove these files when nothing writes to them.

    f=open("/tmp/wwwkomserver." + `os.getpid()` + ".out", "w")
    os.dup2(f.fileno(), 0)
    os.dup2(f.fileno(), 1)
    os.dup2(f.fileno(), 2)
    log.init("sc")
    log.debug("everything is closed")

    return httpd

def do_start_sc(debug, lyskom_host, lyskom_port):
    global syncw

    try:
	siteconfig.siteconfig()
	komhtml.init()
        if debug:
	    log.init("sc", 1)
	else:
	    log.init("sc")
	log.notice("SC starting")

	detach()
        if debug:
	    print "Pid:", os.getpid()
	    sys.stdout.flush()

	httpd = start_httpd(lyskom_host, lyskom_port)
	os.write(syncw, "X")
	httpd.serve_forever()
    except SystemExit:
	raise sys.exc_type, sys.exc_value, sys.exc_traceback
    except:
	if sys.exc_type:
	    log.err("Caught exception: "
		    + `sys.exc_type` + " " + `sys.exc_value`)
	    for line in traceback.format_tb(sys.exc_traceback):
		log.err(line)


def user_area_get(name):
    try:
	return session.user_area_get("WWW-kom", name)
    except komxx.err_key_not_found:
	return None
    except komxx.err_no_user_area:
	return None

def user_area_get_num(name, default):
    """Look up a number in the user area.

    Look up NAME in the user area under the "WWW-kom" heading, convert
    it to a number, and return it.  If any of these steps results in
    an error, return DEFAULT instead.

    """

    try:
	return string.atoi(user_area_get(name))
    except TypeError:
	return default
    except ValueError:
	return default

def start_sc(debug, lyskom_host, lyskom_port):
    try:
	do_start_sc(debug, lyskom_host, lyskom_port)
    finally:
	log.notice("SC exiting")
