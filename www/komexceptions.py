class internal_error(Exception):
    """Raised when an internal error is detected.
    """
    pass

class nonexisting_person(Exception):
    pass

class connection_refused(Exception):
    pass

class system_error(Exception):
    pass

