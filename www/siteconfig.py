import komconfig

# This file is only installed the first time WWW-kom is installed.
# You can add local configuration here, to override the guesses made
# in komconfig.py.  Changes you make in this file (once it is
# installed) will never be overwritten.

def siteconfig():
    # If the guess in komconfig.py about which host and port you want to
    # run WWW-kom on are wrong you can change them here by uncommenting
    # and modifying the following lines.

    # komconfig.set_wwwhost('host.domain')
    # komconfig.set_wwwport('80')

    # Uncommenting the following line will cause newly created persons to
    # automatically be added to conference number 6 with priority 3.

    # komconfig.default_confs.append((6, 3))

    # A function must always contain at least one statement,
    # so insert a pass statement here in case the local site
    # needs no configuration.
    pass
