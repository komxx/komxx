# Copyright (C) 1996  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import komhtml
import komxx
import log
import estring
import string
import re

from language import lingo

def prefetch_author(session, text, unprefetched_authors):
    if text.prefetch() == komxx.st_ok:
	session.conf(text.author()).prefetch_uconf()
    else:
	unprefetched_authors.append(text)

def safe_name(session, conf_no):
    try:
	return komhtml.conf_as_link(conf_no, session.conf(conf_no).name())
    except komxx.err_conf_zero:
	return None
    except (komxx.err_undef_conf, komxx.err_undef_pers,
	    komxx.err_access, komxx.err_perm):
	return lingo.undef_conf(conf_no)

def safe_name_direct_conf(session, conf_no):
    try:
	return komhtml.conf_as_direct_link(conf_no, 
					   session.conf(conf_no).name())
    except komxx.err_conf_zero:
	return None
    except (komxx.err_undef_conf, komxx.err_undef_pers,
	    komxx.err_access, komxx.err_perm):
	return lingo.undef_conf(conf_no)

def safe_name_nolink(session, conf_no):
    try:
	return session.conf(conf_no).name()
    except komxx.err_conf_zero:
	return None
    except (komxx.err_undef_conf, komxx.err_undef_pers,
	    komxx.err_access, komxx.err_perm):
	return lingo.undef_conf(conf_no)

def safe_author(session, text_no, style, use_link=1):
    """Return the name of the author of TEXT_NO as HTML.

    STYLE is a string that can have one of the following values:

    "bold":	use bold text.
    "by":	use lingo.by_author to reformat the author name.  If
    		Swedish is used this will simply add "av " before the
		name.

    The author name will be a link to the conf-stat page for the
    author unless the optional USE_LINK argument is false, in which
    case the name will not be a link at all.
    """
    try:
	cno = session.text(text_no).author()
	link = session.conf(cno).name()
	if use_link:
	    link = komhtml.conf_as_link(cno, link)
	if style == "bold":
	    return "<b>" + link + "</b>"
	elif style == "by":
	    return lingo.by_author(link)
	else:
	    log.err("safe_author: unknown formatting style " + style)
	    return link
	
    except (komxx.err_no_such_text):
	return lingo.nonexisting_text()
    except komxx.err_conf_zero:
	return lingo.anonymous_author()
    except (komxx.err_undef_conf, komxx.err_undef_pers):
	return lingo.nonexistent_author(session.text(text_no).author())

def prefetch_for_display(session, text):
    author=session.conf(text.author())
    author.prefetch_uconf()
    for rcpt in text.recipients():
	session.conf(rcpt.recipient()).prefetch_uconf()
	if rcpt.sender() != 0:
	    session.conf(rcpt.sender()).prefetch_uconf()
    unprefetched_authors=[]
    for uplink in text.uplinks():
	prefetch_author(session, session.text(uplink.parent()),
			unprefetched_authors)
	if uplink.sender() != 0:
	    session.conf(uplink.sender()).prefetch_uconf()
    for footnote in text.footnotes_in():
	prefetch_author(session, session.text(footnote), unprefetched_authors)
    for comment in text.comments_in():
	prefetch_author(session, session.text(comment), unprefetched_authors)
    for t in unprefetched_authors:
	if t.exists():
	    session.conf(t.author()).prefetch_uconf()

def map_for_display(session, text, style):
    map={}
    map["text_no"]       = text.text_no()
    map["creation_date"] = lingo.format_date(text.creation_time())
    map["lines"]         = text.num_lines()
    map["author"]        = safe_author(session, text.text_no(), "bold")
    tmp=""
    for rcpt in text.recipients():
	tmp = tmp + lingo.format_recipient(
	   rcpt.is_carbon_copy(),
	   safe_name(session, rcpt.recipient()),
	   rcpt.local_no(),
	   rcpt.has_rec_time() and rcpt.rec_time(),
	   rcpt.has_sent_at() and rcpt.sent_at(),
	   safe_name(session, rcpt.sender()))
    map["recipients"] = tmp
    tmp = ""
    for uplink in text.uplinks():
	tmp = tmp + lingo.format_uplink(
	   uplink.is_footnote(),
	   komhtml.text_as_link(uplink.parent()),
	   safe_author(session, uplink.parent(), "by"),
	   uplink.sent_later() and uplink.sent_at(),
	   safe_name(session, uplink.sender()))
    map["uplinks"] = tmp
    map["num_marks"] = text.num_marks()
    map["subject"] = komhtml.detect_url(komhtml.htmlquote(text.subject()))
    if style == "pre":
	map["body"] = komhtml.body_as_html_pre(text.body())
    elif style == "heuristic" or style == "":
	map["body"] = komhtml.body_as_html_heuristic(text.body())
    else:
	log.info("Unknown formatting style " + `style` + " requested")
	map["body"] = komhtml.body_as_html_heuristic(text.body())
    tmp=[]
    for footnote in text.footnotes_in():
	tmp.append(lingo.format_footnote(komhtml.text_as_link(footnote),
					 safe_author(session, footnote, "by")))
    map["footnotes"] = string.joinfields(tmp, "")
    tmp = []
    for comment in text.comments_in():
	tmp.append(lingo.format_comment(komhtml.text_as_link(comment),
					safe_author(session, comment, "by")))
    map["comments"] = string.joinfields(tmp, "")
    return map


def target_conference(session, conf_no):
    conf = session.conf(conf_no)
    if conf.type().is_original():
	conf = session.conf(conf.super_conf())
    i=0
    while (i < 20 and conf.exists()
	   and conf.permitted_submitters() != 0
	   and not session.is_member(session.my_login(),
				     conf.permitted_submitters())):
	conf = session.conf(conf.super_conf())
	i=i+1
    return conf.conf_no()


class text_creation_form:
    """This class is used to create and interpret a text-creation form.

    Object creation:

    init_from_form(args):	Initialize from a form_data content.

    Modification:

    add_recipient(cno)
    add_cc_recipient(cno)
    maybe_add_authors_as_recipients()  Check that all authors recieve the text.
    inherit_recipients(tno)	Inherit the recipients from the specified text.

    Data extraction:

    as_html_form()		Returns string which begins with <form>
                                and ends with </form>.
    """

    # This maps from the values in the userarea to values suitable
    # to assign to 
    conversion_map = {"on":      (0, "swascii"),
		      "ask-on":  (1, "swascii"),
		      "ask-off": (1, "none"),
		      "off":     (0, "none")}

    def __init__(self, session):
	self.__session = session
	self.__comment_to = []
	self.__footnote_to = []
	self.__recipients = []
	self.__cc_recipients = []
	self.__subject = ""
	self.__body = ""
	self.__known_comments = []
	# A list of conferences this text should be presentation for.
	self.__presents = []

	# Valid values for __conversion:
	# "none": no conversion wanted.
	# "swascii": swascii to latin1 conversion wanted
	self.__conversion = "none"

	# Valid values for __ask_conversion:
	# 0: don't ask the user
	# 1: ask the user
	self.__ask_conversion = 0

	# The user-area settings may override the conversion defaults.
	try:
	    uaconv = session.user_area_get("WWW-kom", "entry-conversion")
	    (self.__ask_conversion, self.__conversion) = \
				    self.conversion_map[uaconv]
	except komxx.err_key_not_found:
	    pass
	except komxx.err_no_user_area:
	    pass
	except KeyError:
	    # Ignore bad data from the user-area.
	    pass

    def init_from_form(self, args):
	self.__recipients = args.getnumlist("rcpt")
	self.__comment_to = args.getnumlist("parent")
	self.__footnote_to = args.getnumlist("parent_footnote")
	self.__body = re.sub('\r', ' ', args.getstr_empty("body"))
	self.__body = re.sub('[ \t]*\n', '\n', self.__body)
	self.__subject = args.getstr_empty("subject")
	self.__presents = args.getnumlist("presentation_for")
	conv = args.getstr("conversion")
	if conv != None:
	    self.__conversion = conv
	self.__known_comments = estring.xlisttoints(
	    args.getstr_empty("known_comments"))

	# Remove trailing whitespace from the body.
	for i in xrange(len(self.__body) -1, -1, -1):
	    if self.__body[i] not in " \t\n":
		self.__body = self.__body[:i+1]
		break
	else:
	    # Nothing printable was found.
	    self.__body = ""

    def add_recipient(self, cno):
	if cno not in self.__recipients:
	    self.__recipients.append(cno)

    def add_cc_recipient(self, cno):
	if cno not in self.__recipients:
	    self.__cc_recipients.append(cno)

    def add_comment_to(self, tno):
	self.__comment_to.append(tno)

    def as_html_form(self):
	form_map={}

	# Prefetch stage 1:
	for c in self.__presents:
	    self.__session.conf(c).prefetch_uconf()
	for recip in self.__recipients + self.__cc_recipients:
	    self.__session.conf(recip).prefetch()
	for uplink in self.__comment_to + self.__footnote_to:
	    t = self.__session.text(uplink)
	    t.prefetch_text()
	    if (t.prefetch() == komxx.st_ok):
		self.__session.conf(t.author()).prefetch_uconf()
	    
	# Prefetch stage 2:
	for uplink in self.__comment_to + self.__footnote_to:
	    t = self.__session.text(uplink)
	    self.__session.conf(t.author()).prefetch_uconf()

	# Create the map:

	presents = []
	for c in self.__presents:
	    presents.append(lingo.write_text_format_presentation(
		c, safe_name(self.__session, c)))
	form_map["presentations"] = lingo.write_text_format_presentations_part(
	    presents)

	recipients = []
	for recip in self.__recipients:
	    if recip != 0:
		recipients.append(lingo.write_text_format_recipient(
		    recip, safe_name(self.__session, recip)))
	for recip in self.__cc_recipients:
	    if recip != 0:
		recipients.append(lingo.write_text_format_cc_recipient(
		    recip, safe_name(self.__session, recip)))
	form_map["recipients"] = lingo.write_text_format_recipient_part(
	    recipients)

	comments = []
	comments_included = []
	for txt in self.__comment_to:
	    comments.append(lingo.write_text_format_comment(
		txt, safe_author(self.__session, txt, "by")))
	    comments_included.append(lingo.write_text_comment_ref_form(
		self.__session.text(txt).body()))
	for txt in self.__footnote_to:
	    comments.append(lingo.write_text_format_footnote(
		txt, safe_author(self.__session, txt, "by")))
	    comments_included.append(lingo.write_text_footnote_ref_form(
		self.__session.text(txt).body()))

	form_map["comments"] = lingo.write_text_format_comments_part(comments)
	
	if self.__ask_conversion:
	    conv_map = {"select_swascii": "",
			"select_none": ""}
	    if self.__conversion == "none":
		conv_map["select_none"] = "checked"
	    elif self.__conversion == "swascii":
		conv_map["select_swascii"] = "checked"

	    form_map["conversion"] = lingo.conversion_format(conv_map)
	else:
	    form_map["conversion"] = ""

	form_map["subject"] = komhtml.htmlquote(self.__subject)
	form_map["body"] = komhtml.htmlquote(self.__body)
	form_map["known_comments"] = estring.intstoxlist(self.__known_comments)
	form_map["submit_text"] = lingo.button_submit_text(
	    len(self.__presents),
	    len(self.__footnote_to), len(self.__comment_to))
	form_map["comments_inline"] = string.joinfields(comments_included, "")
	return lingo.write_text_form(form_map)

    def maybe_add_authors_as_recipients(self):
	# No need to check __footnote_to -- only the author can write
	# a footnote, so we are by definition the author.
	for t in self.__comment_to:
	    self.__session.text(t).prefetch()
	authors = [self.__session.my_login()]
	for t in self.__comment_to:
	    a = self.__session.text(t).author()
	    if a not in authors:
		authors.append(a)
	for rcpt in self.__recipients + self.__cc_recipients:
	    for a in authors:
		self.__session.prefetch_is_member(a, rcpt)
	    # Note the [:] on the next line, which causes a copy of
	    # the list to be made.  This is necessary since the list
	    # is modified inside the loop.
	    for a in authors[:]:
		if (self.__session.is_member(a, rcpt)):
		    authors.remove(a)
	for a in authors:
	    # FIXME: It might be better to deal with these recipients
	    # differently.  Should maybe force the user to specify
	    # what to do with them: add, cc-add, ignore.
	    self.__recipients.append(a)

    def inherit_subject(self, text_no):
	self.__subject = self.__session.text(text_no).subject()

    def inherit_body(self, text_no):
	self.__body = self.__session.text(text_no).body()

    def inherit_recipients(self, parent, keep_conf = None):
	# keep_conf is used for presentations.
	text = self.__session.text(parent)
	for rcpt in text.recipients():
	    if not rcpt.is_carbon_copy():
		if keep_conf == rcpt.recipient():
		    r = rcpt.recipient()
		else:
		    r = target_conference(self.__session, rcpt.recipient())
		conf = self.__session.conf(r)
		if r != 0 and conf.exists():
		    if r not in self.__recipients:
			self.__recipients.append(r)

    def setup_comment_to(self, text_no):
	if not self.__session.text(text_no).exists():
	    raise komxx.err_no_such_text
	self.add_comment_to(text_no)
	self.inherit_recipients(text_no)
	self.inherit_subject(text_no)
	self.maybe_add_authors_as_recipients()
	self.setup_known_comments()

    def setup_personal_answer_to(self, text_no):
	t = self.__session.text(text_no)
	if not t.exists():
	    raise komxx.err_no_such_text
	self.add_comment_to(text_no)
	self.add_recipient(self.__session.my_login())
	self.add_recipient(t.author())
	self.inherit_subject(text_no)
	self.setup_known_comments()

    def setup_set_presentation(self, conf_no):
	self.__presents.append(conf_no)
	c = self.__session.conf(conf_no)
	old_presentation = c.presentation()
	info = self.__session.get_server_info()
	if c.type().is_letterbox():
	    pres_conf = info[2]
	else:
	    pres_conf = info[1]
	try:
	    self.inherit_body(old_presentation)
	    self.inherit_recipients(old_presentation, pres_conf)
	except komxx.err_no_such_text:
	    pass
	except komxx.err_text_zero:
	    pass
	if len(self.__recipients) == 0:
	    self.__recipients = [pres_conf]
	self.__subject = c.name()

    def __find_comments(self):
	parents = map(self.__session.text,
		      self.__comment_to + self.__footnote_to)
	for t in parents:
	    t.prefetch()
	current_comments = []
	for t in parents:
	    current_comments[len(current_comments):] = t.comments_in()
	    current_comments[len(current_comments):] = t.footnotes_in()
	return current_comments

    def setup_known_comments(self):
	self.__known_comments = self.__find_comments()

    def new_comments(self):
	new_comments = []
	for c in self.__find_comments():
	    if c not in self.__known_comments and c not in new_comments:
		new_comments.append(c)
	new_comments.sort()
	return new_comments

    def create_text(self):
	body = self.__do_convert(self.__body)
	subject = self.__do_convert(self.__subject)
	tno = self.__session.create_text(
	    subject, body,
	    self.__recipients, self.__cc_recipients,
	    self.__comment_to, self.__footnote_to)
	for pres in self.__presents:
	    self.__session.set_presentation(pres, tno)
	return tno

    def __do_convert(self, str):
	if self.__conversion == "swascii":
	    str = re.sub('}', '�', str)
	    str = re.sub('{', '�', str)
	    str = re.sub('\\|', '�', str)
	    str = re.sub(']', '�', str)
	    str = re.sub('\[', '�', str)
	    str = re.sub('\\\\', '�', str)
	return str

