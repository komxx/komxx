// The Sparsemap template class.
//
// Copyright (C) 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef SPARSEMAP_H
#define SPARSEMAP_H

// A Sparsemap is a map, in many ways similar to the STL map type, but
// with several special properties:
//
// 1. The value for a specific key can be either unknown, known or
//    nonexisting (that is, known not to exist).
// 2. The value "0" is common, and long sequences of "0":s (for
//    consecutive keys) are common.
// 3. Sequences of nonexisting and "0" values are stored efficiently.
// 4. Sequences of known non-zero values are stored efficiently.
// 5. All nonexisting values belong to one of two key intervals: one
//    stretching from -infinity, the other stretching to +infinity.
//    The known and unknown values belong to the interval in the
//    middle.  The border of the middle interval may change during the
//    lifetime of the Sparsemap.  It may be unknown.
// 6. Once a value is known, it cannot be unknown again (unless it
//    becomes nonexisting first).
// 7. Both known and unknown values can become nonexisting.

#include <map>

#include "memcheck.h"

#undef USE_TEMPLATE

#ifdef USE_TEMPLATE
#  define TMPL(x, y) x, y
#else
#  define TMPL(x, y)
#  include "kom-types.h"
#  ifndef K
#    define K Local_text_no
#  endif
#  ifndef D
#    define D Text_no
#  endif
#endif

class Sparsemap_base MEMCHECK {
public:
    enum State {
	KNOWN = 1,
	UNKNOWN,
	NONEXISTING
    };
};

#  define Interval_map map<K, Interval TMPL(<K, D>) *, less<K> > 
#  define Mixed_map map<K, D, less<K> >

#ifdef USE_TEMPLATE
template <class K, class D>
#endif
class Interval MEMCHECK {
public:
    virtual ~Interval() { };
    virtual K begin_key() const =0;
    virtual K end_key() const =0;
    virtual void set_begin_key(const K &) =0;
    virtual void set_end_key(const K &) =0;
    virtual pair<D, Sparsemap_base::State> get(const K &) =0;
    virtual void set(const K &, const D &) =0;
};

#ifdef USE_TEMPLATE
template <class K, class D>
#endif
class Mixed_interval : public Interval TMPL(<K, D>) {
public:
    Mixed_interval(const K &interior_key);
    K begin_key() const;
    K end_key() const;
    void set_begin_key(const K &);
    void set_end_key(const K &);
    pair<D, Sparsemap_base::State> get(const K &);
    void set(const K &, const D &);
private:
    // typedef map<K, D, less<K> > Mixed_map;
    Mixed_map mixed_map;
    K inter_begin;
    K inter_end;
};

#ifdef USE_TEMPLATE
template <class K, class D>
#endif
class Sparsemap_internal MEMCHECK {
public:
    Sparsemap_internal();

    int refcount;

    Interval_map interval_map;

    bool key_min_known;
    bool key_max_known;
    K key_min;
    K key_max;
#if 0
private:
    // Not available:
    Sparsemap_internal(const Sparsemap_internal &);
    const Sparsemap_internal &operator =(const Sparsemap_internal &);
#endif
};


#ifdef USE_TEMPLATE
template <class K, class D>
#endif
class Sparsemap : Sparsemap_base {
  public:
    Sparsemap();
    Sparsemap(const Sparsemap &);
    virtual ~Sparsemap();
    Sparsemap &operator =(const Sparsemap &);

    pair<D, State> get(const K &);
    void set(const K &, const D &);

    // Set an interval to all-zero.
    void set_to_zero(const K &zero_begin, const K &zero_end);

    // Everything outside the interval "known_begin <= i < known_end" 
    // is considered to be nonexisting.
    void set_begin(const K &known_begin);
    void set_end(const K &known_end);
  private:
    // typedef map<K, Interval<K, D> *, less<K> > Interval_map;

#ifdef USE_TEMPLATE
    Sparsemap_internal<K, D> *sparse_map;
#else 
    Sparsemap_internal *sparse_map;
#endif
};

#ifndef OUTLINE
#include "sparsemap.icc"
#endif

#endif
