// Set of Local_text_no.
//
// Copyright (C) 1999  Per Cederqvist
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXXX_LOCAL_TEXT_NO_SET_H
#define KOMXXX_LOCAL_TEXT_NO_SET_H

#include <map>

#include "kom-types.h"

class Local_text_no_set {
  public:
    Local_text_no_set() : included() {}
    Local_text_no_set(const Local_text_no_set &s) : included(s.included) {}

    Local_text_no_set &operator=(const Local_text_no_set&);

    void add(Local_text_no first, Local_text_no last);
    void add(Local_text_no lno);
    bool present(Local_text_no lno) const;
    bool empty() const { return included.empty(); }
    Local_text_no front() const;
    void erase(Local_text_no);
    void erase_above(Local_text_no);
    void clear();

    // Return the range of consecutive items surrounding key.  All of
    // the Local_text_no numbers in the range first to last
    // (inclusive) are included in the set.  If key is not present in
    // the set, the next higher included range is returned.  If key is
    // larger than any included Local_text_no, first and last will not
    // be modified, and get_range() returns false.
    bool get_range(Local_text_no key,
		   Local_text_no &first, 
		   Local_text_no &last);

  private:
    typedef std::map<Local_text_no, Local_text_no> Map;

    // The set is represented as a number of ranges.  Each range is
    // stored in this dictionary: the lower bound is the key, and the
    // upper bound the value.  Example: the range 5-8 (inclusive) is
    // stored as included[5] = 8.  The map is always kept normalized,
    // with as few ranges as possible, so if included[5] == 8 neither
    // of the keys 6, 7, 8 or 9 is included.
    Map included;

    Map::iterator find_containing(Local_text_no);
    Map::const_iterator find_containing(Local_text_no) const;
};

#endif
