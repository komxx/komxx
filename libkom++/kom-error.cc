// Map an error number to a human-readable string.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdio>
#include "kom-error.h"
#include "error-codes.h"

char *
kom_errmsg(int errno)
{
    static char buf[20];
    std::sprintf(buf, "Err %d", errno);
    return buf;
}

Kom_err
int_to_kom_err(int arg)
{
    if (arg < 0 || arg >= KOM_max_errno)
	return KOM_UNKNOWN_ERROR;

    switch (arg)
    {
    case 1:
    case 40:
    case 41:
	return KOM_UNKNOWN_ERROR;
    }

    if (arg >= KOM_last_server_error && arg < KOM_ANCIENT_SERVER)
	return KOM_UNKNOWN_ERROR;

    return (Kom_err)arg;
}
