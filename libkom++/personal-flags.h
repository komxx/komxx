// Personal flags.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_PERSONAL_FLAGS_H
#define KOMXX_PERSONAL_FLAGS_H

class LyStr;

/*
 * Flags in the Person struct.
 */
class Personal_flags {
  public:
    Personal_flags();
    explicit Personal_flags(bool unread_is_secret);
    Personal_flags(const Personal_flags&);

    const Personal_flags &operator =(const Personal_flags&);

    friend Personal_flags parse_personal_flags(LyStr &prot_a_str);

    bool unread_is_secret() const;
    bool flg2() const;
    bool flg3() const;
    bool flg4() const;
    bool flg5() const;
    bool flg6() const;
    bool flg7() const;
    bool flg8() const;
  private:
   /* FALSE if everyone is allowed to ask how 
    many unread texts you have.	*/
    unsigned int  pf_unread_is_secret : 1;
    unsigned int  pf_flg2	: 1;
    unsigned int  pf_flg3	: 1;
    unsigned int  pf_flg4	: 1;
    unsigned int  pf_flg5	: 1;
    unsigned int  pf_flg6	: 1;
    unsigned int  pf_flg7	: 1;
    unsigned int  pf_flg8	: 1;
};

Personal_flags parse_personal_flags(LyStr &prot_a_str);
#endif
