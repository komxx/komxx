// Handle information about conferences with unread texts.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_UNREAD_CONFS_H
#define KOMXX_UNREAD_CONFS_H

#include <vector>
#include <list>

#include "session.h"
#include "read-order.h"

class get_unread_confs_question;

struct Unread_info {
    Conf_no   conf;
    long      min_unread;	// At least this many unread texts.
    long      max_unread;	// At most this many unread texts.
				// -1 if no upper limit can be computed yet.
    Unread_info();
    Unread_info &operator =(const Unread_info &);
};

struct Unread_info_estimated {
    Conf_no   conf;
    long      unread;		// Approximately this many texts are unread.

    Unread_info_estimated();
    Unread_info_estimated &operator =(const Unread_info_estimated &);
};


// Which conferences do we have unread texts in?

class Unread_confs {
  public:
    Unread_confs(Session *sess, Pers_no the_reader);
    Unread_confs(const Unread_confs &);
    ~Unread_confs();

    // This method can be used to require a certain amount of
    // knowledge about what is unread. Prefetching is done according
    // to the the criteria in the Work_quota argument.
    // Returns st_ok if so much already was prefetched that nothing
    // new needed to be done.
    Status prefetch(Work_quota *work);

    // goto_next_conf() will perform a change_conference() if it
    // changed the conference.  It returns the current conference
    // (which may be 0 if there is nothing to read -- but if a
    // conference is current when this method is called it will remain
    // current conference if there are no texts in another
    // conference).
    Conf_no              goto_next_conf();

    // Ask which conference a call to goto_next_conf() would change to.
    Conf_no              query_next_conf();

    // Go to a specified conference.  Will perform a
    // change_conference() if necessary.
    Status		 goto_conf(Conf_no);

    // Returns PROMPT_ERROR if there is a fatal error.
    Prompt               next_prompt();

    // Returns 0 if there is a fatal error, or if there are no more
    // texts to be read in the current conference.  This will never
    // change the current working conference.  If remove is true the
    // text will be removed and any footnotes and comments to it will
    // be enqueued (as if enqueue_children had been called).
    Text_no              next_text(bool remove=false);

    // Enqueue all unread footnotes and comments to the specified text
    // that have a recipient in the current conference so that they
    // will be read.  Automatically done by next_text(true).
    void enqueue_children(Text_no);

    // Remove the text from all lists of unread texts.  The text will
    // not be marked as read in the server.
    void                 remove_text(Text_no);

    // Return the conference number of the current working conference,
    // or 0 if no conference has been selected.
    Conf_no              current_conf();

    // Return information about how much texts the user has unread.
    std::vector<Unread_info>  confs_with_unread();

    // Like confs_with_unread, but do a quick estimate (which on rare
    // occasions may be very wrong) on the number of unread texts
    // instead of returning an interval.  This call can be slower than
    // confs_with_unread during startup.
    std::vector<Unread_info_estimated> confs_with_estimated_unread(int max_limit=0);

    // Return information about how much the users has unread in the
    // current conference.  A conference must already be selected.
    long                 min_unread_texts_in_current();
    long                 max_unread_texts_in_current();
    long                 estimated_unread_texts_in_current();

    // Return information about how much the user has unread in a
    // specified conference.  Returns an error if the user isn't a
    // member of the conference.
    Kom_res<long>        estimated_unread_texts_in_conf(Conf_no cno);

    // Return STL-like iterators that iterate in comment-order over
    // the unread texts in the current working conference (which must
    // already have been selected).
    Read_order_iterator current_conf_begin(int wanted_texts =100);
    Read_order_iterator current_conf_end();

    // Retrieve state so that the exact same read-order configuration
    // can later be restored.  The first text to show is returned as
    // the lowest index.  A working conference must already have been
    // selected.
    std::vector<Text_no> query_pending_footnotes() const;
    std::vector<Text_no> query_pending_comments() const;

    // Restore state saved with query_pending_footnotes or
    // query_pending_comments.  Any texts in the arguments that have
    // already been read are silently ignored.  A working conference
    // must already have been selected.
    void set_pending_footnotes(const std::vector<Text_no> &);
    void set_pending_comments(const std::vector<Text_no> &);

    // Join a conference, or change priority/placement of an already
    // joined conference.  This will use Session::add_member to
    // actually add the member to the conference.
    Status               join_conference(Conf_no, unsigned char priority,
					 unsigned short placement,
					 Membership_type type);

    // Permanently leave a conference.  This will use
    // Session::sub_member to actually disjoin the conference.
    Kom_err  disjoin_conference(Conf_no);

    // Set the number of unread texts in a conference so that
    // first_unread is the first unread texts.  This uses
    // Session::set_last_read to actually update the number of unread
    // texts the user has if (and only if) the session is logged in as
    // the reader.  The user must already be a member of the
    // conference.
    Kom_err set_last_read(Conf_no cno, Local_text_no last_text_read);

    // The cache automatically registers this callback method with the
    // Async_dispatcher.  This method should not be used in any other
    // way.
    void        new_text(const Async_text_created &msg);

    // Retrieve the session object.
    Session  *session() const;

  private:
    const Unread_confs & operator = (const Unread_confs &); // N/A

    // Add a new conference. If LAST is true, append it to the list
    // of unread conferences, otherwise prepend it.  The caller must
    // make sure that the user actually is a member of the conference
    // before calling this method, or confusion will arise.
    Read_order *add_conference(Conf_no, bool last);

    // Utility functions that prefetch one known text/the list of
    // conferences that may have unread texts.
    Status need_one_mapped();
    Status need_unread_confs();

    Session              * the_session;
    Conferences          & conferences;
    Persons		 & persons;
    Maps<Text_mapping>   & maps;
    Texts		 & texts;

    std::list<Read_order *>   unread_confs;
    // Since the Read_order objects contains state that is useful even
    // when we have read all texts, we save them here.  FIXME: The
    // state should be removed from them. The state that is useful is
    // the membership, and we now have a cache for them.  The
    // Read_orders on read_confs by definition never contain any
    // unread texts.  One tiny little piece of information remains
    // useful: current_read_order->conf_no() is the number of the
    // current working conference.
    std::vector<Read_order *> read_confs;
    Pers_no		      reader;
    Read_order		     *current_read_order;

    get_unread_confs_question *ucq;
    Async_handler_tag<Async_text_created> cr_tag;
    bool unread_confs_has_been_fetched;
};
#endif
