// Access to fields in a user-area.
//
// Copyright (C) 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <map>

#include "create_text_old_q.h"
#include "set_user_area_q.h"
#include "user-area.h"
#include "text.h"
#include "container.h"
#include "prot-a-LyStr.h"

typedef std::map<LyStr, std::map<LyStr, LyStr>* > Parsed_map;

class User_area_internal MEMCHECK {
  public:
    User_area_internal(Person p, Text_stat_cache *, Text_mass_cache *);
    ~User_area_internal();

    // Try to fetch the userarea.  Only block if the parameter is
    // true.
    void fetch(bool block = true);

    // The first index is the name of the area, and the second index
    // is the key.
    Parsed_map parsed_map;

    // The parts of the map that don't follow the standard format set
    // out by the elisp-client are stored in this map.  The name of
    // the area is the key.
    std::map<LyStr, LyStr> unknown_map;

    // The last known text number of the user area.
    Text_no used_user_area;

    // Error code if the user area couldn't be fetched.
    Kom_err frozen_error_code;

    // True once the user area has been fetched (or once it is clear
    // that the user area cannot be fetched).
    bool fetch_complete;

    // True if unsaved changes has been made.
    bool unclean;

    // The person whose user-area this object handles.
    Person person;

    Text_stat_cache *tsc;
    Text_mass_cache *tmc;
};

User_area_internal::User_area_internal(Person p,
				       Text_stat_cache *tscache,
				       Text_mass_cache *tmcache)
  : parsed_map(),
    unknown_map(),
    used_user_area(0),
    frozen_error_code(KOM_NO_ERROR),
    fetch_complete(false),
    unclean(false),
    person(p),
    tsc(tscache),
    tmc(tmcache)
{
    fetch(false);
}

User_area_internal::~User_area_internal()
{
}

void
User_area_internal::fetch(bool block)
{
    if (fetch_complete)
	return;

    if (!block && person.prefetch_pers_only() == st_pending)
	return;

    if (!person.pers_exists())
    {
	frozen_error_code = person.kom_errno();
	fetch_complete = true;
	used_user_area = 0;
	return;
    }

    used_user_area = person.user_area();
    if (used_user_area == 0)
    {
	frozen_error_code = KOM_NO_USER_AREA;
	fetch_complete = true;
	return;
    }

    Text t = Text(used_user_area, tsc, tmc);

    if (!block && t.prefetch_text() == st_pending)
	return;

    LyStr_errno raw_text = t.text_mass();
    if (raw_text.kom_errno != KOM_NO_ERROR)
    {
	frozen_error_code = raw_text.kom_errno;
	fetch_complete = true;
	used_user_area = 0;
	return;
    }

    prot_a_LyStr userarea = raw_text.str;
    LyStr_errno block_names = userarea.stos();
    if (block_names.kom_errno != KOM_NO_ERROR)
    {
	frozen_error_code = KOM_BAD_USER_AREA;
	fetch_complete = true;
	return;
    }

    LyStr_errno block_name;
    prot_a_LyStr block_names_a = block_names.str;
    LyStr_errno datablock_err;
    prot_a_LyStr datablock;
    LyStr_errno key;
    LyStr_errno val;
    while ((block_name = block_names_a.stos()).kom_errno == KOM_NO_ERROR)
    {
	datablock_err  = userarea.stos();
	if (datablock_err.kom_errno != KOM_NO_ERROR)
	{
	    frozen_error_code = datablock_err.kom_errno;
	    fetch_complete = true;
	    return;
	}

	datablock = datablock_err.str;
	std::map<LyStr, LyStr> *current_map = new std::map<LyStr, LyStr>();
	while ((key = datablock.stos()).kom_errno == KOM_NO_ERROR
	       && (val = datablock.stos()).kom_errno == KOM_NO_ERROR)
	{
	    (*current_map)[key.str] = val.str;
	    if (datablock.prefix("\n"))
		datablock.remove_first();
	}
	datablock.skipspace();
	if (key.kom_errno != KOM_BAD_HOLLERITH || datablock.strlen() != 0)
	{
	    // We failed to parse this block.
	    delete current_map;
	    unknown_map[block_name.str] = datablock_err.str;
	}
	else
	{
	    // This block was properly parsed.
	    parsed_map[block_name.str] = current_map;
	}
	// FIXME: there is no check for duplicate blocks with the same name.
    }

    block_names_a.skipspace();
    userarea.skipspace();

    if (block_names_a.strlen() > 0 || userarea.strlen() > 0)
    {
	frozen_error_code = KOM_BAD_USER_AREA;
	fetch_complete = true;
	return;
    }

    fetch_complete = true;
    unclean = false;
}

User_area::User_area(Person p, Text_stat_cache *tsc, Text_mass_cache *tmc)
  : uai(new User_area_internal(p, tsc, tmc))
{
}

User_area::~User_area()
{
    delete_contents_map(uai->parsed_map);
    delete uai;
}

Kom_res<LyStr>
User_area::lookup(const LyStr &area,
		  const LyStr &key)
{
    uai->fetch();
    if (uai->frozen_error_code != KOM_NO_ERROR)
	return uai->frozen_error_code;

    Parsed_map::iterator area_iter = uai->parsed_map.find(area);
    if (area_iter == uai->parsed_map.end())
	return KOM_KEY_NOT_FOUND;
    
    std::map<LyStr, LyStr>::iterator value_iter = 
	(*area_iter).second->find(key);

    if (value_iter == (*area_iter).second->end())
	return KOM_KEY_NOT_FOUND;

    return (*value_iter).second;
}

Kom_err
User_area::set(const LyStr &area, const LyStr &key, const LyStr &value)
{
    uai->fetch();
    if (uai->frozen_error_code == KOM_NO_USER_AREA)
	uai->frozen_error_code = KOM_NO_ERROR;
	
    if (uai->frozen_error_code != KOM_NO_ERROR)
	return uai->frozen_error_code;

    if (uai->unknown_map.find(area) != uai->unknown_map.end())
	// The area exists, but we cannot parse it.
	return KOM_BAD_USER_AREA;

    Parsed_map::iterator area_iter = uai->parsed_map.find(area);
    std::map<LyStr, LyStr> *areamap;
    if (area_iter != uai->parsed_map.end())
	areamap = (*area_iter).second;
    else
    {
	areamap = new std::map<LyStr, LyStr>();
	uai->parsed_map[area] = areamap;
    }

    std::map<LyStr, LyStr>::iterator key_iter = areamap->find(key);
    if (key_iter == areamap->end())
    {
	(*areamap)[key] = value;
	uai->unclean = true;
    }
    else if ((*key_iter).second != value)
    {
	(*key_iter).second = value;
	uai->unclean = true;
    }

    return KOM_NO_ERROR;
}

Kom_err
User_area::store_in_server()
{
    uai->fetch();
    
    if (!uai->unclean)
	return KOM_NO_ERROR;

    if (uai->frozen_error_code != KOM_NO_ERROR)
	return uai->frozen_error_code;

    prot_a_LyStr areas;
    prot_a_LyStr datablocks;

    for (Parsed_map::iterator pmiter = uai->parsed_map.begin();
	 pmiter != uai->parsed_map.end(); ++pmiter)
    {
	areas << ' ';
	areas.append_hollerith((*pmiter).first);
	
	prot_a_LyStr block;
	std::map<LyStr, LyStr> *blockmap = (*pmiter).second;
	for (std::map<LyStr, LyStr>::iterator block_iter = blockmap->begin();
	     block_iter != blockmap->end(); ++block_iter)
	{
	    block << ' ';
	    block.append_hollerith((*block_iter).first);
	    block << ' ';
	    block.append_hollerith((*block_iter).second);
	    block << '\n';
	}

	datablocks.append_hollerith(block);
    }

    for (std::map<LyStr, LyStr>::iterator unknown_iter = 
	     uai->unknown_map.begin(); 
	 unknown_iter != uai->unknown_map.end(); ++unknown_iter)
    {
	areas.append_hollerith((*unknown_iter).first);
	datablocks.append_hollerith((*unknown_iter).second);
    }
    
    prot_a_LyStr total;
    total.append_hollerith(areas);
    total << ' ' << datablocks;

    create_text_old_question q(uai->person.connection(), total,
			       std::vector<Recipient>(),
			       std::vector<Text_no>(),
			       std::vector<Text_no>());
    if (q.receive() != st_ok)
	return q.error();

    set_user_area_question suq(uai->person.connection(), uai->person.conf_no(),
			       q.result());
    if (suq.receive() != st_ok)
	return suq.error();

    uai->unclean = false;

    // FIXME: update user_area and create_texts in person.

    uai->used_user_area = q.result();
    return KOM_NO_ERROR;
}
