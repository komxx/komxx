// A aux-item class.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <cassert>

#include "time-utils.h"
#include "aux-item.h"
#include "prot-a-LyStr.h"

Aux_item_flags 
parse_aux_item_flags(prot_a_LyStr &prot_a_str)
{
    Aux_item_flags res;
    
    prot_a_str.skipspace();
    prot_a_str.parse_bit(res.aif_deleted);
    prot_a_str.parse_bit(res.aif_inherit);
    prot_a_str.parse_bit(res.aif_secret);
    prot_a_str.parse_bit(res.aif_hide_creator);
    prot_a_str.parse_bit(res.aif_dont_garb);
    prot_a_str.parse_bit(res.aif_reserved2);
    prot_a_str.parse_bit(res.aif_reserved3);
    prot_a_str.parse_bit(res.aif_reserved4);

    return res;
}


Aux_item_flags::Aux_item_flags()
  : aif_deleted(false),
    aif_inherit(false),
    aif_secret(false),
    aif_hide_creator(false),
    aif_dont_garb(false),
    aif_reserved2(false),
    aif_reserved3(false),
    aif_reserved4(false)
{
}

Aux_item_flags::Aux_item_flags(bool deleted,
			       bool inherit,
			       bool secret,
			       bool hide_creator,
			       bool dont_garb,
			       bool reserved2,
			       bool reserved3,
			       bool reserved4)
  : aif_deleted(deleted),
    aif_inherit(inherit),
    aif_secret(secret),
    aif_hide_creator(hide_creator),
    aif_dont_garb(dont_garb),
    aif_reserved2(reserved2),
    aif_reserved3(reserved3),
    aif_reserved4(reserved4)
{
}


bool 
Aux_item_flags::deleted() const
{
    return aif_deleted;
}

bool 
Aux_item_flags::inherit() const
{
    return aif_inherit;
}

bool 
Aux_item_flags::secret() const
{
    return aif_secret;
}

bool 
Aux_item_flags::hide_creator() const
{
    return aif_hide_creator;
}

bool 
Aux_item_flags::dont_garb() const
{
    return aif_dont_garb;
}

bool 
Aux_item_flags::reserved2() const
{
    return aif_reserved2;
}

bool 
Aux_item_flags::reserved3() const
{
    return aif_reserved3;
}

bool 
Aux_item_flags::reserved4() const
{
    return aif_reserved4;
}

void 
Aux_item_flags::set_deleted(bool x)
{
    aif_deleted = x;
}

void 
Aux_item_flags::set_inherit(bool x)
{
    aif_inherit = x;
}

void 
Aux_item_flags::set_secret(bool x)
{
    aif_secret = x;
}

void 
Aux_item_flags::set_hide_creator(bool x)
{
    aif_hide_creator = x;
}

void 
Aux_item_flags::set_dont_garb(bool x)
{
    aif_dont_garb = x;
}

void 
Aux_item_flags::set_reserved2(bool x)
{
    aif_reserved2 = x;
}

void 
Aux_item_flags::set_reserved3(bool x)
{
    aif_reserved3 = x;
}

void 
Aux_item_flags::set_reserved4(bool x)
{
    aif_reserved4 = x;
}

Aux_item 
parse_aux_item(prot_a_LyStr &prot_a_str)
{
    Aux_item res;
    
    res.ax_aux_no = prot_a_str.stol();
    res.ax_tag = prot_a_str.stol();
    res.ax_creator = prot_a_str.stol();
    res.ax_created_at = parse_time(prot_a_str);
    res.ax_flags = parse_aux_item_flags(prot_a_str);
    res.ax_inherit_limit = prot_a_str.stol();
    LyStr_errno tmp = prot_a_str.stos();
    assert(tmp.kom_errno == KOM_NO_ERROR);
    res.ax_data = tmp.str;

    return res;
}


Aux_item::Aux_item()
  : ax_aux_no(),
    ax_tag(),
    ax_creator(),
    ax_created_at(),
    ax_flags(),
    ax_inherit_limit(),
    ax_data()
{
}


Aux_item::Aux_item(const Aux_item&o)
  : ax_aux_no(o.ax_aux_no),
    ax_tag(o.ax_tag),
    ax_creator(o.ax_creator),
    ax_created_at(o.ax_created_at),
    ax_flags(o.ax_flags),
    ax_inherit_limit(o.ax_inherit_limit),
    ax_data(o.ax_data)
{
}


Aux_item::Aux_item(Aux_tag tag,
		   Aux_item_flags flags,
		   uint32_t inherit_limit,
		   LyStr data)
  : ax_aux_no(),
    ax_tag(tag),
    ax_creator(),
    ax_created_at(),
    ax_flags(flags),
    ax_inherit_limit(inherit_limit),
    ax_data(data)
{
}


Aux_item &
Aux_item::operator =(const Aux_item &o)
{
    if (this != &o)
    {
	ax_aux_no = o.ax_aux_no;
	ax_tag = o.ax_tag;
	ax_creator = o.ax_creator;
	ax_created_at = o.ax_created_at;
	ax_flags = o.ax_flags;
	ax_inherit_limit = o.ax_inherit_limit;
	ax_data = o.ax_data;
    }

    return *this;
}

Aux_no 
Aux_item::aux_no() const
{
    return ax_aux_no;
}

Aux_tag 
Aux_item::tag() const
{
    return ax_tag;
}

Pers_no 
Aux_item::creator() const
{
    return ax_creator;
}

struct std::tm
Aux_item::created_at() const
{
    return ax_created_at;
}

Aux_item_flags 
Aux_item::flags() const
{
    return ax_flags;
}

uint32_t 
Aux_item::inherit_limit() const
{
    return ax_inherit_limit;
}

LyStr 
Aux_item::data() const
{
    return ax_data;
}

void 
Aux_item::set_tag(Aux_tag t)
{
    ax_tag = t;
}

void 
Aux_item::set_flags(const Aux_item_flags &f)
{
    ax_flags = f;
}

void 
Aux_item::set_inherit_limit(uint32_t lim)
{
    ax_inherit_limit = lim;
}

void 
Aux_item::set_data(const LyStr &d)
{
    ax_data = d;
}
