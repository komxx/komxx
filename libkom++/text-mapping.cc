// Fetch-on-demand map from local to global text numbers.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <map>
#include <algorithm>
#include <cassert>

#include "question.h"
#include "local-to-global-map.h"
#include "text-mapping.h"
#include "local-text-no-set.h"

template <class Q>
class Text_mapping_basic<Q>::Pending {
  public:
    Pending(Local_text_no f, Q *qp)
      : first(f), q(qp) {}
	
    Local_text_no first;
    Q *q;
};

template <class Q>
struct Text_mapping_basic<Q>::TMI {
  public:
    TMI(Conf_no cno, Connection *cn, int chunk_sz);
    ~TMI();

    Conf_no conf_no;
    Connection *connection;
    const int chunk_size;
    
    void add_ref();
    int sub_ref();		// Returns the new reference count.

    typedef std::map<Local_text_no, Text_no> L2g;

    L2g l2g;
    Local_text_no_set known;
    std::vector<Pending> pending;

    // These start out to cover the entire possible interval.  The
    // interval is decreased as new information becomes available.
    Local_text_no first_existing;
    Local_text_no first_non_existing;

  private:
    TMI(const TMI&);		// N/A
    TMI& operator=(const TMI&); // N/A

    int refcount;
};


template <class Q>
Text_mapping_basic<Q>::Text_mapping_basic()
    : tmi(NULL)
{
}

template <class Q>
Text_mapping_basic<Q>::Text_mapping_basic(Conf_no conf_no, Connection *conn, int chunk_sz)
  : tmi(new TMI(conf_no, conn, chunk_sz))
{
    tmi->add_ref();
}

template <class Q>
Text_mapping_basic<Q>::Text_mapping_basic(const Text_mapping_basic&m)
  : tmi(m.tmi)
{
    if (tmi)
	tmi->add_ref();
}

template <class Q>
Text_mapping_basic<Q>::~Text_mapping_basic()
{
    if (tmi && tmi->sub_ref() == 0)
	delete tmi;
}

template <class Q>
Text_mapping_basic<Q>& 
Text_mapping_basic<Q>::operator=(const Text_mapping_basic&m)
{
    if (this != &m)
    {
	if (tmi && tmi->sub_ref() == 0)
	    delete tmi;
	tmi = m.tmi;
	if (tmi)
	    tmi->add_ref();
    }
    return *this;
}

template <class Q>
bool
Text_mapping_basic<Q>::valid() const
{
    return tmi != NULL;
}

template <class Q>
Text_no
Text_mapping_basic<Q>::operator [](Local_text_no lno)
{
    if (tmi==NULL)		// FIXME-throw
	return 0;
    
    if (lno < tmi->first_existing || tmi->first_non_existing <= lno)
	return 0;

    poll_pending();

    {
	typename TMI::L2g::iterator d = tmi->l2g.find(lno);
	if (d != tmi->l2g.end())
	    return (*d).second;
    }
    
    if (tmi->known.present(lno))
	// We know about this number, but it wasn't present.  It must 
	// be a deleted text.
	return 0;

    {
	typename std::vector<Pending>::iterator i = get_prefetch(lno);
	(*i).q->receive();
    }

    poll_pending();

    {
	typename TMI::L2g::iterator d = tmi->l2g.find(lno);
	if (d != tmi->l2g.end())
	    return (*d).second;
    }

    assert(tmi->known.present(lno));
    return 0;
}


template <class Q>
Local_text_no
Text_mapping_basic<Q>::next_existing(Local_text_no first)
{
    if (tmi==NULL)		// FIXME-throw
	return 0;

    // Handle overflow explicitly.  There are, by definition, no more
    // existing texts after LOCAL_TEXT_NO_MAX...
    if (first == MAX_LOCAL_TEXT_NO)
	return 0;
    
    if (tmi->first_non_existing <= first + 1)
	return 0;

    poll_pending();

    {
	typename TMI::L2g::iterator d = tmi->l2g.upper_bound(first);
	if (d != tmi->l2g.end())
	{
	    Local_text_no known_first;
	    Local_text_no known_last;

	    if (tmi->known.get_range(first + 1, known_first, known_last)
		&& first + 1 >= known_first
		&& d->first <= known_last)
	    {
		return d->first;
	    }
	}
    }

    {
	typename std::vector<Pending>::iterator i = get_prefetch(
	    first + 1);
	i->q->receive();
    }

    poll_pending();

    {
	typename TMI::L2g::iterator d = tmi->l2g.upper_bound(first);
	if (d != tmi->l2g.end())
	    return d->first;
    }

    return 0;
}

template <class Q>
Status
Text_mapping_basic<Q>::prefetch(Local_text_no lno)
{
    if (tmi == NULL)		// FIXME-throw
	return st_error;

    if (lno < tmi->first_existing || tmi->first_non_existing <= lno)
	return st_ok;

    poll_pending();
    if (tmi->known.present(lno))
	return st_ok;

    typename std::vector<Pending>::iterator i = get_prefetch(lno);
    return (*i).q->status();
}

template <class Q>
void
Text_mapping_basic<Q>::add(Local_text_no key, Text_no text)
{
    assert(tmi != NULL);

    if (text)
	tmi->l2g[key] = text;
    tmi->known.add(key);
}

template <class Q>
void
Text_mapping_basic<Q>::add_unknown()
{
    assert(tmi != NULL);

    if (tmi->l2g.empty())
    {
	tmi->known.clear();
	tmi->first_non_existing = 0;
	tmi->first_non_existing = MAX_LOCAL_TEXT_NO;
    }
    else
    {
	typename TMI::L2g::iterator iter = tmi->l2g.end();
	--iter;
	Local_text_no last_known = iter->first;
	tmi->first_non_existing = MAX_LOCAL_TEXT_NO;
	tmi->known.erase_above(last_known);
    }
}


template <class Q>
void
Text_mapping_basic<Q>::add(const local_to_global_map& l2g)
{
    assert(tmi != NULL);
    for (local_to_global_map::const_iterator iter = l2g.begin();
	 iter != l2g.end();
	 ++iter)
    {
	tmi->l2g[iter->first] = iter->second;
    }
    if (l2g.more_texts_exists())
	tmi->known.add(l2g.range_begin(), l2g.range_end()-1);
    else
    {
	tmi->known.add(l2g.range_begin(), MAX_LOCAL_TEXT_NO);
	tmi->first_non_existing = l2g.range_end();
    }
}

template <class Q>
void
Text_mapping_basic<Q>::add_zero_range(Local_text_no begin,
				      Local_text_no end)
{
    assert(tmi != NULL);
    tmi->known.add(begin, end-1);
}

template <class Q>
typename std::vector<typename Text_mapping_basic<Q>::Pending>::iterator 
Text_mapping_basic<Q>::get_prefetch(Local_text_no lno)
{
    // This function assumes that we only get dense results.  If we
    // get a sparse result, this might issue several needless
    // requests.  There really isn't a whole lot we can do about
    // that...

    assert(tmi != NULL);

    for (typename std::vector<Pending>::iterator res = tmi->pending.begin();
	 res != tmi->pending.end();
	 ++res)
    {
	if ((*res).first <= lno && lno < (*res).first + tmi->chunk_size)
	    return res;
    }
    
    tmi->pending.push_back(Pending(lno, new Q(
	tmi->connection, tmi->conf_no, lno, tmi->chunk_size)));
    return tmi->pending.end() - 1;
}

template <class Q>
struct Text_mapping_basic<Q>::got_reply
    : public std::unary_function<typename Text_mapping_basic<Q>::Pending &, 
				 bool> 
{
  got_reply(Text_mapping_basic<Q> *t) : tm(t) {}

  bool operator () (Text_mapping_basic<Q>::Pending &pend) 
  { 
    if (pend.q->status() != st_pending)
    {
	if (pend.q->status() == st_ok)
	    tm->add(pend.q->result());
	else
	{
	    if (pend.q->error() == KOM_NO_SUCH_LOCAL_TEXT)
	    {
		tm->tmi->first_non_existing = std::min(
		    pend.first,
		    tm->tmi->first_non_existing);
		tm->add_zero_range(pend.first, MAX_LOCAL_TEXT_NO);
	    }
	    else
	    {
		/// FIXME: unexpected error: raise exception.
	    }
	}
	delete pend.q;
	pend.q = NULL;
	return true;
    }
    else
	return false;
  }
private:
  Text_mapping_basic<Q> *tm;
};

template <class Q>
void
Text_mapping_basic<Q>::poll_pending()
{
    typename std::vector<Pending>::iterator iter = std::remove_if(
	tmi->pending.begin(), tmi->pending.end(), got_reply(this));
    
    tmi->pending.erase(iter, tmi->pending.end());
}

template <class Q>
int
Text_mapping_basic<Q>::chunk_size() const 
{
    return tmi->chunk_size;
}

template <class Q>
Text_mapping_basic<Q>::TMI::TMI(Conf_no cno, Connection *conn, int chunk_sz)
  : conf_no(cno), 
    connection(conn),
    chunk_size(chunk_sz),
    l2g(),
    known(),
    pending(),
    first_existing(0), 
    first_non_existing(MAX_LOCAL_TEXT_NO),
    refcount(0)
{
}

template <class Q>
Text_mapping_basic<Q>::TMI::~TMI()
{
    // FIXME: can this leak pending[x].q?
    // FIXME: can this leak anything else?
}

template <class Q>
void
Text_mapping_basic<Q>::TMI::add_ref()
{
    ++refcount;
}

template <class Q>
int
Text_mapping_basic<Q>::TMI::sub_ref()
{
    return --refcount;
}
