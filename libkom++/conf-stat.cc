// A Conf-status class.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>
#include <cassert>

#include "error-codes.h"
#include "time-utils.h"
#include "kom-types.h"
#include "conf-stat.h"
#include "prot-a-LyStr.h"
#include "time-utils.h"


// ================================================================
//                    Conf_stat_internal methods


Conf_stat::Conf_stat_internal::Conf_stat_internal()
    : this_conf_no(0),
      kom_errno(KOM_NO_ERROR),
      creator(0),
//      creation_time(),
      presentation(0),
      supervisor(0),
      permitted_submitters(0),
      super_conf(0),
      type(),
//      last_written(),
      msg_of_day(0),
      nice(77),
      keep_commented(77),
      conf_name(),
      no_of_members(0),
      first_local_no(1),
      no_of_texts(0),
      expire(0),
      aux()
{
}


Conf_stat::Conf_stat_internal::Conf_stat_internal(Conf_no conf_no,
						  bool old,
						  prot_a_LyStr &prot_a_str)
  : this_conf_no(conf_no), kom_errno(KOM_NO_ERROR), aux()
{
    LyStr_errno cname = prot_a_str.stos();
    kom_errno = cname.kom_errno;
    conf_name = cname.str;
    type = parse_conf_type(prot_a_str);
    creation_time = parse_time(prot_a_str);
    last_written = parse_time(prot_a_str);
    creator = prot_a_str.stol();
    presentation = prot_a_str.stol();
    supervisor = prot_a_str.stol();
    permitted_submitters = prot_a_str.stol();
    super_conf = prot_a_str.stol();
    msg_of_day = prot_a_str.stol();
    nice = prot_a_str.stol();
    if (old)
	keep_commented = 77;
    else
	keep_commented = prot_a_str.stol();
    no_of_members = prot_a_str.stol();
    first_local_no = prot_a_str.stol();
    no_of_texts = prot_a_str.stol();
    if (old)
    {
	expire = 0;
    }
    else
    {
	expire = prot_a_str.stol();
	prot_a_str.parse_aux_list(aux);
    }
}


Conf_stat::Conf_stat_internal::Conf_stat_internal(const Conf_stat_internal &c)
    : this_conf_no(c.this_conf_no)
    , kom_errno(c.kom_errno)
    , creator(c.creator)
    , creation_time(c.creation_time)
    , presentation(c.presentation)
    , supervisor(c.supervisor)
    , permitted_submitters(c.permitted_submitters)
    , super_conf(c.super_conf)
    , type(c.type)
    , last_written(c.last_written)
    , msg_of_day(c.msg_of_day)
    , nice(c.nice)
    , keep_commented(c.keep_commented)
    , conf_name(c.conf_name)
    , no_of_members(c.no_of_members)
    , first_local_no(c.first_local_no)
    , no_of_texts(c.no_of_texts)
    , expire(c.expire)
    , aux(c.aux)
{
}


const Conf_stat::Conf_stat_internal &
Conf_stat::Conf_stat_internal::operator =(const Conf_stat::Conf_stat_internal
					  &c)
{
    refcount = c.refcount;
    kom_errno = c.kom_errno;
    this_conf_no = c.this_conf_no;
    creator = c.creator;
    creation_time = c.creation_time;
    presentation = c.presentation;
    supervisor = c.supervisor;
    permitted_submitters = c.permitted_submitters;
    super_conf = c.super_conf;
    type = c.type;
    last_written = c.last_written;
    msg_of_day = c.msg_of_day;
    nice = c.nice;
    keep_commented = c.keep_commented;
    conf_name = c.conf_name;
    no_of_members = c.no_of_members;
    first_local_no = c.first_local_no;
    no_of_texts = c.no_of_texts;
    expire = c.expire;
    aux = c.aux;

    return *this;
}


// ================================================================
//                         Conf_stat methods

Conf_stat::Conf_stat()
{
    csi = NULL;
}


Conf_stat::Conf_stat(const Conf_stat &conf_stat)
{
    if (conf_stat.csi != NULL)
    {
	conf_stat.csi->refcount++;
    }
    csi = conf_stat.csi;
}


Conf_stat::Conf_stat(Conf_no conf_no, bool old, prot_a_LyStr &prot_a_str)
{
    csi = new Conf_stat_internal(conf_no, old, prot_a_str);
    csi->refcount = 1;
}

Conf_stat::Conf_stat(Kom_err errno)
{
    csi = new Conf_stat_internal;
    csi->refcount = 1;
    csi->kom_errno = errno;
}

Conf_stat::~Conf_stat()
{
    if (csi != NULL) 
    {
	csi->refcount--;
	if (csi->refcount == 0) 
	{
	    delete csi;
	}
    }
}


Conf_stat &
Conf_stat::operator=(const Conf_stat &conf_stat)
{
    if (&conf_stat != this) 
    {
	// Delete the old Conf_stat_internal if this was the last reference.
	if (csi != NULL)
	{
	    csi->refcount--;
	    if (csi->refcount == 0) 
	    {
		delete csi;
	    }
	}
	
	csi = conf_stat.csi;
	if (csi != NULL)
	    csi->refcount++;
    }

    return *this;
}


bool
Conf_stat::valid() const
{
    return (bool) (csi != NULL && csi->kom_errno == 0);
}

Kom_err
Conf_stat::kom_errno() const
{
    if (csi == NULL)
	return KOM_UNINITIALIZED;
    else
	return csi->kom_errno;
}



Pers_no 
Conf_stat::creator() const
{
    assert(valid());
    return csi->creator;
}

struct tm		
Conf_stat::creation_time() const
{
    assert(valid());
    return csi->creation_time;
}

Text_no	        
Conf_stat::presentation() const
{
    assert(valid());
    return csi->presentation;
}

Conf_no	        
Conf_stat::supervisor() const
{
    assert(valid());
    return csi->supervisor;
}

Conf_no	        
Conf_stat::permitted_submitters() const
{
    assert(valid());
    return csi->permitted_submitters;
}

Conf_no	        
Conf_stat::super_conf() const
{
    assert(valid());
    return csi->super_conf;
}

Conf_type	 	
Conf_stat::type() const
{
    assert(valid());
    return csi->type;
}

struct tm	        
Conf_stat::last_written() const
{
    assert(valid());
    return csi->last_written;
}

Text_no	        
Conf_stat::msg_of_day() const
{
    assert(valid());
    return csi->msg_of_day;
}

Garb_nice	        
Conf_stat::nice() const
{
    assert(valid());
    return csi->nice;
}

Garb_nice	        
Conf_stat::keep_commented() const
{
    assert(valid());
    return csi->keep_commented;
}

LyStr
Conf_stat::name() const
{
    assert(valid());
    return csi->conf_name;
}

unsigned short	
Conf_stat::no_of_members() const
{
    assert(valid());
    return csi->no_of_members;
}

Local_text_no	
Conf_stat::first_local_no() const
{
    assert(valid());
    return csi->first_local_no;
}

unsigned long	
Conf_stat::no_of_texts() const
{
    assert(valid());
    return csi->no_of_texts;
}

Garb_nice	        
Conf_stat::expire() const
{
    assert(valid());
    return csi->expire;
}

int
Conf_stat::num_aux() const
{
    assert(valid());
    return csi->aux.size();
}

Aux_item
Conf_stat::aux(int n) const
{
    assert(valid());
    return csi->aux[n];
}

void
Conf_stat::set_type(Conf_type conf_type)
{
    assert(valid());
    csi->type = conf_type;
}


void
Conf_stat::change_name(const LyStr &new_name)
{
    assert(valid());
    csi->conf_name = new_name;
}


void
Conf_stat::set_permitted_submitters(Conf_no permsub)
{
    assert(valid());
    csi->permitted_submitters = permsub;
}


void
Conf_stat::inform_text_exists(Local_text_no lno,
			      const struct tm &creation_time)
{
    // FIXME: it should be possible to cache the "known" range of
    // lno:s even if the conf-stat is not yet fetched.  That may
    // not be very useful, though, and probably rather complicated.
    assert(valid());
    if (lno < csi->first_local_no)
    {
	csi->no_of_texts += (csi->first_local_no - lno);
	csi->first_local_no = lno;
    }
    if (csi->first_local_no + csi->no_of_texts <= lno)
	csi->no_of_texts = lno - csi->first_local_no + 1;
    if (struct_tm_newer(creation_time, csi->last_written))
	csi->last_written = creation_time;
}


void
Conf_stat::set_expire(Garb_nice expire)
{
    assert(valid());
    csi->expire = expire;
}


void
Conf_stat::set_garb_nice(Garb_nice garb_nice)
{
    assert(valid());
    csi->nice = garb_nice;
}


void
Conf_stat::set_keep_commented(Garb_nice keep_commented)
{
    assert(valid());
    csi->keep_commented = keep_commented;
}
