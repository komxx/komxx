// Template class for simple error management.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_ERROR_AGGREGATE_H_
#define KOMXX_ERROR_AGGREGATE_H_

#include "error-codes.h"

template <class T>
class Kom_res {
  public:
    // The default constructor sets error to KOM_NO_ERROR
    // and data to T().
    Kom_res();

    // Set error to KOM_NO_ERROR and data to result.
    Kom_res(const T &result);

    // Set error to ecode and data to T().
    Kom_res(Kom_err ecode);

    Kom_res(const Kom_res &);
    Kom_res &operator =(const Kom_res &);

    Kom_err error;
    T data;
};

#ifndef OUTLINE
#  include "error-aggregate.icc"
#endif

#endif
// error-aggregate.h ends here.
