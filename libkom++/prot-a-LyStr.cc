// String class with knowledge on protocol A.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cctype>

#include "prot-a-LyStr.h"
#include "aux-item.h"
#include "member.h"

prot_a_LyStr::prot_a_LyStr() 
{ 
}

prot_a_LyStr::prot_a_LyStr(const char *c_str)
    : LyStr(c_str)
{
}

prot_a_LyStr::prot_a_LyStr(void *block, int len)
    : LyStr(block, len)
{
}

prot_a_LyStr::prot_a_LyStr(const prot_a_LyStr &l)
    : LyStr(l)
{
}

prot_a_LyStr::prot_a_LyStr(const LyStr &l)
    : LyStr(l)
{
}


// ================================================================


long
prot_a_LyStr::stol()
{
    // +++ Better error handling needed here.
    // FIXME: code in Membership::Membership_internal::Membership_internal
    // in membership.cc relies on the current behavior.
    long res = 0;
    int i = 0;

    while (i < used && operator[](i) == ' ')
	i++;

    while(i < used && isdigit(operator[](i)))
    {
	res *= 10;
	res += operator[](i++) - '0';
    }

    remove_first(i);
    return res;
}

LyStr_errno
prot_a_LyStr::stos()
{
    long prevlen = strlen();
    long len = stol();
    long currentlen = strlen();

    if (prevlen == currentlen || len + 1 > currentlen || operator[](0) != 'H')
	return KOM_BAD_HOLLERITH;

    remove_first(1);
    LyStr res = substr(len);
    remove_first(len);
    return res;
}

void
prot_a_LyStr::append_hollerith(const LyStr &str)
{
    *this << str.strlen() << 'H' << str;
}

void
prot_a_LyStr::append_aux_item(const Aux_item &aux)
{
    *this << aux.tag() << ' ';
    const Aux_item_flags &f(aux.flags());
    append_bool(f.deleted());
    append_bool(f.inherit());
    append_bool(f.secret());
    append_bool(f.hide_creator());
    append_bool(f.dont_garb());
    append_bool(f.reserved2());
    append_bool(f.reserved3());
    append_bool(f.reserved4());
    *this << ' ' << aux.inherit_limit() << ' ';
    append_hollerith(aux.data());
}


void
prot_a_LyStr::append_aux_no_list(const std::vector<Aux_no> &aux_nos)
{
    typedef std::vector<Aux_no>::const_iterator AI;

    append_array_start(aux_nos.size());
    for (AI iter = aux_nos.begin(); iter != aux_nos.end(); ++iter)
	*this << ' ' << *iter;
    append_array_end();
}
    

void
prot_a_LyStr::append_membership_type(const Membership_type &type)
{
    append_bool(type.invitation());
    append_bool(type.passive());
    append_bool(type.secret());
    append_bool(type.passive_message_invert());
    append_bool(type.reserved2());
    append_bool(type.reserved3());
    append_bool(type.reserved4());
    append_bool(type.reserved5());
}


void
prot_a_LyStr::append_aux_list(const std::vector<Aux_item> &aux_items)
{
    typedef std::vector<Aux_item>::const_iterator AI;

    append_array_start(aux_items.size());
    for (AI iter = aux_items.begin(); iter != aux_items.end(); ++iter)
	append_aux_item(*iter);
    append_array_end();
}


void
prot_a_LyStr::append_bool(bool flag)
{
    *this << (flag ? '1' : '0');
}


bool
prot_a_LyStr::parse_bit(bool &flag)
{
    if (strlen() > 0)
    {
	switch (operator[](0))
	{
	case '0':
	    flag = false;
	    remove_first();
	    return true;
	case '1':
	    flag = true;
	    remove_first();
	    return true;
	default:
	    return false;
	}
    }

    return false;
}

void
prot_a_LyStr::skip_bits()
{
    char c;
    while (strlen() > 0 && ((c = operator[](0)) == '0' || c == '1'))
	remove_first();
}


int
prot_a_LyStr::parse_array_start()
{
    int   size;

    size = stol();
    if (size > 0)
    {
	skipspace();
	// FIXME: we need a way to explicitly handle things such as "2
	// *".  They work reasonably now due to lack of proper error
	// handling.
	if (strlen() > 0 && operator[](0) == '{')
	    remove_first(1);
    }
    
    return size;
}


void
prot_a_LyStr::parse_array_end(int arrsize)
{
    skipspace();
    if (arrsize < 0)
	return;
    else if (arrsize == 0) 
    {
	if (strlen() > 0) 
	{
	    if (operator[](0) == '*')
		remove_first(1);
	}
    } 
    else
    {
	if (strlen() > 0) 
	{
	    if (operator[](0) == '}')
		remove_first(1);
	}
    }
}


void
prot_a_LyStr::append_array_start(int arrsize)
{
    *this << arrsize << " { ";
}


void
prot_a_LyStr::append_array_end()
{
    *this <<  "}";
}


void
prot_a_LyStr::parse_aux_list(std::vector<Aux_item> &result)
{
    int sz = parse_array_start();
    
    result.resize(sz);

    for (int i = 0; i < sz; i++)
	result[i] = parse_aux_item(*this);
    
    parse_array_end(sz);
}


void
parse_kom_type(prot_a_LyStr &str, uint32_t &res)
{
    res = str.stol();
}

void
parse_kom_type(prot_a_LyStr &str, unsigned short &res)
{
    res = str.stol();
}

void
parse_kom_type(prot_a_LyStr &str, Member &res)
{
    res = Member(str);
}
