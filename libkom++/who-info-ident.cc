// Who-info-ident object.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>
#include <cstdlib>

#include "who-info-ident.h"
#include "prot-a-LyStr.h"


Who_info_ident::Who_info_ident()
    : person(0),
      working_conference(0),
      session_no(0),
      what_am_i_doing(),
      user_name(),
      host_name(),
      ident_user()
{
}

Who_info_ident::Who_info_ident(const Who_info_ident &w)
    : person(w.person),
      working_conference(w.working_conference),
      session_no(w.session_no),
      what_am_i_doing(w.what_am_i_doing),
      user_name(w.user_name),
      host_name(w.host_name),
      ident_user(w.ident_user)
{
}

Who_info_ident::Who_info_ident(prot_a_LyStr &prot_a_string)
{
    parse(prot_a_string);
}


Who_info_ident::~Who_info_ident()
{
}



const Who_info_ident &
Who_info_ident::operator =(const Who_info_ident &w)
{
    person = w.person;
    working_conference = w.working_conference;
    session_no = w.session_no;
    what_am_i_doing = w.what_am_i_doing;
    user_name = w.user_name;
    host_name = w.host_name;
    ident_user = w.ident_user;
    return *this;
}


void
Who_info_ident::parse(prot_a_LyStr &prot_a_string)
{
    person = prot_a_string.stol();
    working_conference = prot_a_string.stol();
    session_no = prot_a_string.stol();
    LyStr_errno tmp = prot_a_string.stos();
    assert(tmp.kom_errno == KOM_NO_ERROR);
    what_am_i_doing = tmp.str;
    tmp = prot_a_string.stos();
    assert(tmp.kom_errno == KOM_NO_ERROR);
    user_name = tmp.str;
    tmp = prot_a_string.stos();
    assert(tmp.kom_errno == KOM_NO_ERROR);
    host_name = tmp.str;
    tmp = prot_a_string.stos();
    assert(tmp.kom_errno == KOM_NO_ERROR);
    ident_user = tmp.str;
}


Pers_no
Who_info_ident::pers_no() const
{
    return person;
}


Conf_no
Who_info_ident::working_conf() const
{
    return working_conference;
}


Session_no
Who_info_ident::session() const
{
    return session_no;
}


LyStr
Who_info_ident::doing() const
{
    return what_am_i_doing;
}


LyStr
Who_info_ident::username() const
{
    return user_name;
}

LyStr
Who_info_ident::hostname() const
{
    return host_name;
}

LyStr
Who_info_ident::identuser() const
{
    return ident_user;
}
