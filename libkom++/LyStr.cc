// A string class.  Why reuse code when you can roll your own?
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstring>
#include <cassert>
#include <cctype>
#include <cstdlib>
#include <algorithm>

#include <sstream>

#include "LyStr.h"

LyStr::LyStr()
{
    data = NULL;
    size = 0;
    used = 0;
    origin = 0;
}

LyStr::LyStr(const char *c_str)
{
    used = std::strlen (c_str);
    size = 2*used;		// Heuristic...
    data = new char [size];
    std::memcpy(data, c_str, used);
    origin = 0;
}

LyStr::LyStr(void *block,
	     int len)
{
    size = 2 * len;
    data = new char [ size ];
    used = len;
    origin = 0;
    std::memcpy(data, block, len);
}

LyStr::LyStr(const LyStr &s)
{
    data = new char [ s.size ];
    size = s.size;
    used = s.used;
    origin = 0;
    std::memcpy(data, s.data+s.origin, used);
}

LyStr::~LyStr()
{
    delete[] data;
}

LyStr &
LyStr::operator << (const LyStr &s)
{
    append(s.data+s.origin, s.used);
    return *this;
}

LyStr &
LyStr::operator << (char c)
{
    append(&c, 1);
    return *this;
}

LyStr &
LyStr::operator << (unsigned char c)
{
    append((const char *)&c, 1);
    return *this;
}

LyStr &
LyStr::operator << (signed char c)
{
    append((const char *)&c, 1);
    return *this;
}

LyStr &
LyStr::operator << (short num)
{
    std::ostringstream o;
    o << num;
    append(o.str());
    return *this;
}

LyStr &
LyStr::operator << (unsigned short num)
{
    std::ostringstream o;
    o << num;
    append(o.str());
    return *this;
}

LyStr &
LyStr::operator << (int num)
{
    std::ostringstream o;
    o << num;
    append(o.str());
    return *this;
}

LyStr &
LyStr::operator << (unsigned int num)
{
    std::ostringstream o;
    o << num;
    append(o.str());
    return *this;
}

LyStr &
LyStr::operator << (long num)
{
    std::ostringstream o;
    o << num;
    append(o.str());
    return *this;
}

LyStr &
LyStr::operator << (unsigned long num)
{
    std::ostringstream o;
    o << num;
    append(o.str());
    return *this;
}

void
LyStr::need_space(long length)
{
    if (origin+used+length <= size)
    {
	// NOP
    }
    else if (used+length <= size)
    {
	for (int i=0; i < used; i++)
	    data[i] = data[origin+i];
	origin = 0;
    }
    else
    {
	int newsize = 2 * (used+length);
	char *newdata = new char [ newsize ];
	if (used)
	    memcpy(newdata, data+origin, used);
	origin = 0;
	if (data)
	    delete[] data;
	data = newdata;
	size = newsize;
    }
}

void
LyStr::append(const char *buffer,
	      long length)
{
    if (length)
    {
	need_space(length);
	std::memcpy(data+origin+used, buffer, length);
	used += length;
    }
}

void
LyStr::append(const std::string &buffer)
{
    long length = buffer.size();
    if (length)
    {
	need_space(length);
	std::memcpy(data+origin+used, buffer.data(), length);
	used += length;
    }
}

char &
LyStr::operator [] (long index)
{
    assert(index >= 0 && index < used);
    return data[origin+index];
}

const char &
LyStr::operator [] (long index) const
{
    assert(index >= 0 && index < used);
    return data[origin+index];
}

char & 
LyStr::first()
{
    assert(used > 0);
    return data[origin];
}

const char &
LyStr::first() const
{
    assert(used > 0);
    return data[origin];
}

LyStr & 
LyStr::operator = (const LyStr &s)
{
    if (this != &s)
    {
	if (size < s.used)
	{
	    if (data)
		delete [] data;
	    size = 2 * s.used;
	    data = new char [ size ];
	}
	if (s.used)
	    std::memcpy(data, s.data+s.origin, s.used);
	origin = 0;
	used = s.used;
    }
    return *this;
}

long
LyStr::strlen() const
{
    return used;
}

bool
LyStr::empty() const
{
    return used == 0 ? true : false;
}

bool
LyStr::nonempty() const
{
    return used != 0 ? true : false;
}


void
LyStr::remove_first(long no)
{
    assert (no <= used);
    origin += no;
    used -= no;
}

void
LyStr::remove_last(long no)
{
    assert (no <= used);
    used -= no;
}

void
LyStr::clear()
{
    origin = 0;
    used = 0;
}

void
LyStr::skipspace()
{
    while (strlen() > 0 && first() == ' ')
	remove_first();
}

LyStr
LyStr::extract_word()
{
    skipspace();
    int wend = strchr(' ');
    if (wend == -1)
    {
	LyStr tmp = *this;
	clear();
	return tmp;
    }
    else
    {
	assert(wend > 0);
	LyStr tmp = substr(wend);
	remove_first(wend);
	return tmp;
    }
}

long
LyStr::extract_number(bool &err)
{
    bool found = false;
    bool negative = false;
    long res = 0;
  
    skipspace();

    if (nonempty() && first() == '-')
    {
	negative = true;
	remove_first();
    }
	
    while (nonempty() && isdigit(first()))
      {
	res *= 10;
	res += first() - '0';
	remove_first();
	found = true;
      }

    if (!found)
	err = true;
    
    if (negative)
	return -res;
    else
	return res;
}


long
LyStr::strchr(char c) const
{
    for (long ix = 0; ix < used; ix++)
	if (data[origin+ix] == c)
	    return ix;

    return -1;
}

bool
LyStr::present(char c) const
{
    if (strchr(c) == -1)
	return false;
    else
	return true;
}

bool 
LyStr::prefix(const LyStr &p)
{
  long len = p.strlen();

  if (len > strlen())
    return false;
  
  for (long ix=0 ; ix < len; ix++)
    if (p[ix] != operator[](ix))
      return false;
  
  return true;
}

bool 
LyStr::prefix(const char *s)
{
  long ix = 0;
  
  while (1)
    {
      if (*s == '\0')
	return true;
      if (ix >= strlen())
	return false;
      if (operator[](ix) != *s)
	return false;
      ix++;
      s++;
    }
}

	
char *
LyStr::malloc_copy(long start, long wanted_size) const
{
    assert(start >= 0);
    if (wanted_size == -1 || wanted_size > used - start)
      wanted_size = used - start;
    assert(wanted_size >= 0);

    char *copy = (char*) std::malloc(wanted_size+1);
    if (copy != NULL)
    {
	std::memcpy(copy, data+origin+start, wanted_size);
	copy[wanted_size] = '\0';
    }
    return copy;
}

const char *
LyStr::view_block() const
{
    // Make sure that the memory block is NUL-terminated.
    // (It is normally not).
    const_cast<LyStr*>(this)->need_space(1);
    const_cast<LyStr*>(this)->data[origin+used] = '\0';

    return data + origin;
}

void
LyStr::view_done(const char *) const
{
}

LyStr
LyStr::substr(long sub) const
{
    if (sub > used)
	return *this;

    LyStr ret(data+origin, sub);
    return ret;
}

LyStr
LyStr::substr(long begin, long end) const
{
    assert(begin >= 0);

    if (begin > used)
	return "";

    if (end > used && begin == 0)
	return *this;

    if (end > used)
	end = used;

    LyStr ret(data + origin + begin, end - begin);
    return ret;
}

std::ostream &
operator << (std::ostream &os, const LyStr &s)
{
    int pad = os.width() - s.strlen();
    char padchar = os.fill();

    if (pad > 0 && (os.flags() & std::ios::right))
	while (pad-- > 0)
	    os << padchar;

    for (int i=s.origin; i < s.used+s.origin; i++)
	os << s.data[i];

    while (pad-- > 0)
	os << padchar;

    os.width(0);
    os.fill(' ');

    return os;
}

std::istream &
operator >> (std::istream &is, LyStr &s)
{
    int c;

    while ((c = is.get()) != std::istream::traits_type::eof())
	if (c == '\n')
	{
	    is.putback(c);
	    return is;
	}
	else
	{
	    s << (char)c;
	}

    return is;
}

bool
operator ==(const LyStr &a, const LyStr &b)
{
    if (a.used != b.used)
	return false;
    
    if (a.used == 0)
	return true;
    
    for (int ix = 0; ix < a.used; ix++)
	if (a[ix] != b[ix])
	    return false;
    
    return true;
}  

bool
operator !=(const LyStr &a, const LyStr &b)
{
    if (a == b)
	return false;
    else
	return true;
}

bool
operator <(const LyStr &a, const LyStr &b)
{
    return std::lexicographical_compare(
	a.data + a.origin, a.data + a.origin + a.used,
	b.data + b.origin, b.data + b.origin + b.used);
}

size_t
LyStr::hash() const
{
    const char *str = view_block();
    size_t res = 0;
    for (; *str != '\0'; str++)
    {
	res ^= (*str << 6);
	res += *str;
    }
    view_done(str);
    return res;
}
