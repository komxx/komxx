// A recipient of a text.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_RECIPIENT_H
#define KOMXX_RECIPIENT_H

#include <ctime>

#include "kom-types.h"

class prot_a_LyStr;

class Recipient {
  public:
    Recipient();
    Recipient(const Recipient&);
    Recipient(Conf_no recpt, bool carbon_copy);
    const Recipient &operator =(const Recipient &);

    int parse(bool cc_recpt, prot_a_LyStr &prot_a_str);

    Conf_no        recipient()      const;
    bool           is_carbon_copy() const;
    Local_text_no  local_no()       const;
    bool           has_rec_time()   const;
    struct std::tm rec_time()       const;
    Pers_no        sender()         const;
    bool           has_sent_at()    const;
    struct std::tm sent_at()        const;
  private:
    Conf_no	   r_recipient;
    enum { RECIPIENT, CC_RECIPIENT } r_recipient_type;
    Local_text_no  r_local_no;
    bool           r_rec_time_valid;
    struct std::tm r_rec_time;
    Pers_no	   r_sender;		// 0 if not sent.
    bool           r_sent_at_valid;
    struct std::tm r_sent_at;
};
#endif
