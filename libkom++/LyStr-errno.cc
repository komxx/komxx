// LyStr and errno aggregate
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "LyStr-errno.h"

LyStr_errno::LyStr_errno(const LyStr &str_init,
			 Kom_err err_init)
  : str(str_init),
    kom_errno(err_init)
{
}

LyStr_errno::LyStr_errno(Kom_err err_init)
  : str(),
    kom_errno(err_init)
{
}

LyStr_errno::LyStr_errno(const LyStr &str_init)
  : str(str_init),
    kom_errno(KOM_NO_ERROR)
{
}
    
LyStr_errno::LyStr_errno(const LyStr_errno &e)
  : str(e.str),
    kom_errno(e.kom_errno)
{
}


LyStr_errno::LyStr_errno()
  : str(),
    kom_errno(KOM_NO_ERROR)
{
}

LyStr_errno::~LyStr_errno()
{
}

LyStr_errno &
LyStr_errno::operator=(const LyStr_errno &e)
{
    str = e.str;
    kom_errno = e.kom_errno;
    return *this;
}

