// Create-on-demand map of mappings for conferences.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "conference.h"
#include "maps.h"
#include "connection.h"
#include "map-cache.h"

template <class M>
Maps<M>::Maps(Connection *conn, Async_dispatcher *async_disp)
  : cache(new Map_cache<M>(conn, async_disp))
{
}

template <class M>
Maps<M>::~Maps()
{
    delete cache;
}

template <class M>
Connection *
Maps<M>::connection() const
{
    return cache->connection();
}

    
template <class M>
M
Maps<M>::operator[](Conf_no c) const
{
    return cache->get(c);
}

template <class M>
Map_cache<M> *
Maps<M>::read_map_cache() const
{
    return cache;
}
