// Store a Version-Info Protocol A result.
//
// Copyright (C) 2005  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "version-info.h"

Version_info::Version_info()
  : protocol(),
    server(),
    software()
{
}

Version_info::Version_info(long p, const LyStr &srv, const LyStr &sw)
  : protocol(p),
    server(srv),
    software(sw)
{
}


Version_info::Version_info(const Version_info &v)
  : protocol(v.protocol),
    server(v.server),
    software(v.software)
{
}

const Version_info &
Version_info::operator =(const Version_info &o)
{
    if (&o != this)
    {
	protocol = o.protocol;
	server = o.server;
	software = o.software;
    }
    return *this;
}

long        
Version_info::protocol_version() const
{
    return protocol;
}

LyStr       
Version_info::server_software() const
{
    return server;
}

LyStr       
Version_info::software_version() const
{
    return software;
}
