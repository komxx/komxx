// Information about a session.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_SESSION_INFO_H
#define KOMXX_SESSION_INFO_H

#include <time.h>

#include "kom-types.h"
#include "LyStr.h"

class Session_info {
  public:
    Session_info();
    Session_info(const Session_info&);
    const Session_info &operator =(const Session_info &);
  private:
    Pers_no		person;
    LyStr		what_am_i_doing;
    LyStr		username;            /* Userid and hostname. */
    Conf_no		working_conference;
    Session_no	session;             /* Serial number of connection. */
    struct tm	connection_time;     /* Not logintime. */
    unsigned long		idle_time; 	     /* Seconds. */
};
#endif
