// Store a mapping from local to global text-numbers.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>
#include <cstdlib>

#include "Local2Global.h"
#include "prot-a-LyStr.h"

Local2Global::Local2Global()
  : t(NULL)
{
}

Local2Global::Local2Global(prot_a_LyStr& prot_a_str)
{
    f = prot_a_str.stol();
    s = prot_a_str.stol();
    t = new Text_no[s];
    prot_a_str.skipspace();
    if (prot_a_str[0] == '*')
    {
	prot_a_str.remove_first(1);
	assert(s == 0);
    }
    else
    {
	assert(prot_a_str[0] == '{');
	prot_a_str.remove_first(1);
	for (int i=0; i < s; i++)
	    t[i] = prot_a_str.stol();
	prot_a_str.skipspace();
	assert(prot_a_str[0] == '}');
	prot_a_str.remove_first(1);
    }
}

Local2Global::Local2Global(const Local2Global&l)
  : f(l.f), s(l.s), t((l.t != NULL) ? new Text_no[l.s] : (Text_no*)NULL)
{
    if (t != NULL)
	for (int i=0; i < s; i++)
	    t[i] = l.t[i];
}

Local2Global & 
Local2Global::operator=(const Local2Global&l)
{
    if (this != &l)
    {
	if (l.t != NULL)
	{
	    if (s < l.s && l.t != NULL)
	    {
		delete[] t;
		t = new Text_no[l.s];
	    }
	    s = l.s;
	    f = l.f;
	    for (int i=0; i < s; i++)
		t[i] = l.t[i];
	}
	else if (t != NULL)
	{
	    delete[] t;
	    t = NULL;
	}
    }
    return *this;
}

Local2Global::~Local2Global()
{
    if (t != NULL)
	delete[] t;
}

Text_no &
Local2Global::operator[] (Local_text_no lno) const
{
    assert(t);
    assert(lno >= f);
    assert(lno < f + s);
    return t[lno - f];
}

Local_text_no
Local2Global::first() const
{
    return f;
}

int
Local2Global::size() const
{
    return s;
}
