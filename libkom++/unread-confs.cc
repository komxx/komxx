// Handle information about conferences with unread texts.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>

#include "unread-confs.h"
#include "connection.h"
#include "get_unread_confs_q.h"
#include "change_conference_q.h"
#include "async_text_created.h"
#include "container.h"

Unread_info::Unread_info()
    :conf(0), min_unread(-1), max_unread(-2)
{
}

Unread_info &
Unread_info::operator =(const Unread_info &u)
{
  conf = u.conf;
  min_unread = u.min_unread;
  max_unread = u.max_unread;
  return *this;
}

Unread_info_estimated::Unread_info_estimated()
    :conf(0), unread(-2)
{
}

Unread_info_estimated &
Unread_info_estimated::operator =(const Unread_info_estimated &u)
{
  conf = u.conf;
  unread = u.unread;
  return *this;
}

// ================================================================ 
//                    Constructors and Destructors                  


Unread_confs::Unread_confs(Session *sess, Pers_no the_reader)
    : the_session(sess),
      conferences(sess->conferences()), 
      persons(sess->persons()),
      maps(sess->maps()),
      texts(sess->texts()),
      unread_confs(), 
      read_confs(),
      reader(the_reader),
      current_read_order(NULL),
      ucq(NULL),
      unread_confs_has_been_fetched(false)
{
    Kom_auto_ptr<Async_callback<Async_text_created> > ptr;
    ptr.reset(async_callback_method(this, &Unread_confs::new_text).release());
    cr_tag = sess->async_dispatcher().register_text_created_handler(ptr);
}
 
// FIXME: Make Unread_confs into a reference counting class.
// This class owns the Read_orders pointed to by read_confs and unread_confs,
// so we cannot have two Unread_confs objects pointing to the same Read_orders.
//
// Unread_confs::Unread_confs(const Unread_confs &cl)
//     : session(cl.session), conferences(cl.conferences), persons(cl.persons),
//       maps(cl.maps), texts(cl.texts), unread_confs(cl.unread_confs), 
//       reader(cl.reader), read_confs(cl.read_confs),
//       current_read_order(cl.current_read_order), ucq(cl.ucq),
//       unread_confs_has_been_fetched(cl.unread_confs_has_been_fetched)
// {
// }


Unread_confs::~Unread_confs()
{
    delete_contents(unread_confs);
    delete_contents(read_confs);

    the_session->async_dispatcher().unregister_text_created_handler(cr_tag);

    delete ucq;
}


// ================================================================ 
//

Status
Unread_confs::prefetch(Work_quota *work)
{
    int q = work->questions_left();
    // FIXME: there may be cases where there is nothing to prefetch.
    // This method should return st_ok in those cases.
    if (q == 0)
	return st_pending;

    if (!unread_confs_has_been_fetched)
    {
	ucq = new get_unread_confs_question(conferences.connection(), reader);
	unread_confs_has_been_fetched = true;
    }

    if (ucq != NULL && ucq->receive() == st_pending)
    {
	work->bump_question();
	return st_pending;
    }

    if (ucq != NULL)
    {
	if (ucq->receive() == st_error)
	   return st_error;

	assert(unread_confs.empty() && read_confs.empty());

	std::vector<Conf_no> new_unread_confs = ucq->result();
	delete ucq;
	ucq = NULL;

	for (std::vector<Conf_no>::iterator iter = new_unread_confs.begin();
	     iter != new_unread_confs.end(); ++iter)
	{
	    add_conference(*iter, true);
	}
    }

    std::list<Read_order *>::iterator src;
    std::list<Read_order *>::iterator dst;
    for (src = dst = unread_confs.begin();
	 src != unread_confs.end() && !work->done();
	 ++src)
    {
	work->change_conf();
	if (work->done())
	    break;

	Status st = (*src)->prefetch(work);
	int nt = (*src)->min_num_unread();

	if (st == st_ok && nt == 0)
	{     
	    // There is nothing to read in this conference. Remove it
	    // from unread_confs.
	    read_confs.push_back(*src);
	}
	else
	{
	    *dst++ = *src;
	}
    }
    // FIXME-now that unread_confs is a std::list, it may be possible to
    // erase the nodes one at a time in the above loop instead.
    unread_confs.erase(dst, src);

    return (q == work->questions_left()) ? st_ok : st_pending;
}

Conf_no
Unread_confs::goto_next_conf()
{
    if (goto_conf(query_next_conf()) != st_ok)
	return 0;
    return current_conf();
}


Conf_no
Unread_confs::query_next_conf()
{
    if (need_one_mapped() == st_error)
	return 0;

    if (unread_confs.empty())
    {
	if (current_read_order == NULL)
	    return 0;
	else
	    return current_read_order->confno();
    }

    // Unread_confs::prefetch should remove any empty Read_order.
    assert (unread_confs.front()->empty() == false);

    // Store the current Read_order in current if it contains unread texts.
    Read_order *current = NULL;
    if (current_read_order == unread_confs.front())
    {
	current = unread_confs.front();
	unread_confs.pop_front();
    }

    // Make sure that at least one text exists in the new current
    // working conference.
    need_one_mapped();

    Conf_no result = 0;
    if (!unread_confs.empty())
	result = unread_confs.front()->confno();

    if (current != NULL)
    {
	unread_confs.push_front(current);
	if (result == 0)
	    result = current->confno();
    }

    return result;
}

class confno_eq : public std::unary_function<const Read_order*, bool> {
    Conf_no c;
  public:
    confno_eq(Conf_no cno) : c(cno) {}
    bool operator() (const Read_order *ro) const { return c == ro->confno(); }
};

Status
Unread_confs::goto_conf(Conf_no c)
{
    if (current_read_order == NULL && c == 0)
	return st_ok;

    if (current_read_order != NULL && current_read_order->confno() == c)
	// Return immediately if we are already standing in the conference.
	return st_ok;

    Read_order *ro = NULL;

    if (c != 0)
    {
	if (need_unread_confs() != st_ok)
	    return st_error;

	if (ro == NULL)
	{
	    std::list<Read_order*>::iterator iter = find_if(
		unread_confs.begin(), unread_confs.end(),
		confno_eq(c));
	    if (iter != unread_confs.end())
	    {
		ro = *iter;
		unread_confs.erase(iter);
	    }
	}

	if (ro == NULL)
	{
	    std::vector<Read_order*>::iterator iter = find_if(
		read_confs.begin(), read_confs.end(),
		confno_eq(c));
	    if (iter != read_confs.end())
	    {
		ro = *iter;
		read_confs.erase(iter);
	    }
	}

	if (ro == NULL)
	    ro = new Read_order(the_session, c, reader);
    }

    change_conference_question q(conferences.connection(), c);
    // +++ We could relatively safely ignore this question, except
    // when we had to create a new Read_order.
    if (q.receive() == st_ok)
    {
	if (ro != NULL)
	    unread_confs.push_front(ro);
	current_read_order = ro;
    }
    else
    {
	// The user was not a member in this conference (or something
	// else happened).  ro may be NULL here if we are going to
	// conference 0.
	assert(current_read_order != ro);
	delete ro;
    }
    return q.receive();
}

Prompt
Unread_confs::next_prompt()
{
    Prompt   prompt;

    if (need_one_mapped() == st_error)
	return PROMPT_ERROR;

    if (unread_confs.empty())
	return NO_MORE_TEXT;

    if (current_read_order != unread_confs.front())
	return NEXT_CONF;

    prompt = unread_confs.front()->next_prompt();
    assert(prompt != NO_MORE_TEXT && prompt != NEXT_CONF);
    return prompt;
}


Text_no
Unread_confs::next_text(bool remove)
{
    assert(current_read_order != NULL);

    if (need_one_mapped() == st_error)
	return 0;

    assert(current_read_order != NULL);

    if (current_read_order->empty())
	return 0;

    Text_no  next = current_read_order->next_text(remove);
    if (next != 0 && remove)
	remove_text(next);

    return next;
}


void
Unread_confs::enqueue_children(Text_no tno)
{
    assert(current_read_order != NULL);
    current_read_order->enqueue_children(tno);
}


void
Unread_confs::remove_text(Text_no tno)
{
    if (need_unread_confs() != st_ok)
	return;

#if 1				// FIXME: Create test-case for this.
    // Silently ignore any attempts to remove a nonexisting text.
    if (!texts[tno].exists())
      return;
#endif

    // Loop through the recipients of the text.
    for (int i = 0; i < texts[tno].num_recipients(); ++i) {
	Conf_no  cno = texts[tno].recipient(i)->recipient();
	Local_text_no lno = texts[tno].recipient(i)->local_no();

        // Find the recipient and remove the text from it.
	std::list<Read_order*>::iterator iter = find_if(
	    unread_confs.begin(), unread_confs.end(), confno_eq(cno));
	if (iter != unread_confs.end())
	    (*iter)->remove_text(tno, lno);
    }
}


Conf_no
Unread_confs::current_conf()
{
    if (current_read_order == NULL)
	return 0;
    else
	return current_read_order->confno();
}

std::vector<Unread_info>
Unread_confs::confs_with_unread()
{
    if (need_unread_confs() != st_ok)
	return std::vector<Unread_info>();

    std::vector<Unread_info>  arr(unread_confs.size());
    int                  index = 0;

    for (std::list<Read_order*>::iterator iter = unread_confs.begin(); 
	 iter != unread_confs.end(); ++iter) 
    {
	arr[index].conf = (*iter)->confno();
	arr[index].min_unread = (*iter)->min_num_unread();
	arr[index].max_unread = (*iter)->max_num_unread();
	index++;
    }

    return arr;
}

std::vector<Unread_info_estimated>
Unread_confs::confs_with_estimated_unread(int max_limit)
{
    if (need_unread_confs() != st_ok)
	return std::vector<Unread_info_estimated>();

    int m = unread_confs.size();
    if (max_limit != 0 && m > max_limit)
	m = max_limit;
    std::vector<Unread_info_estimated>   arr(m);
    int index = 0;

    // FIXME: it is probably possible to boost performance with
    // creative prefetching here.

    for (std::list<Read_order*>::iterator iter = unread_confs.begin(); 
	 iter != unread_confs.end() && index < m;
	 ++iter) 
    {
	arr[index].conf = (*iter)->confno();
	arr[index].unread = (*iter)->estimated_num_unread();
	index++;
    }

    assert(index == m);

    return arr;
}

long
Unread_confs::min_unread_texts_in_current()
{
    assert(current_read_order != NULL);
    return current_read_order->min_num_unread();
}

long
Unread_confs::max_unread_texts_in_current()
{
    assert(current_read_order != NULL);
    return current_read_order->max_num_unread();
}

long
Unread_confs::estimated_unread_texts_in_current()
{
    assert(current_read_order != NULL);
    return current_read_order->estimated_num_unread();
}


Kom_res<long>
Unread_confs::estimated_unread_texts_in_conf(Conf_no cno)
{

    if (need_unread_confs() != st_ok)
	return KOM_INDETERMINATE;

    Read_order *ro = NULL;
    if (ro == NULL)
    {
	std::list<Read_order*>::iterator iter = find_if(
	    unread_confs.begin(), unread_confs.end(), confno_eq(cno));
	if (iter != unread_confs.end())
	    ro = *iter;
    }
    if (ro == NULL)
    {
	std::vector<Read_order*>::iterator iter = find_if(
	    read_confs.begin(), read_confs.end(), confno_eq(cno));
	if (iter != read_confs.end())
	    ro = *iter;
    }

    if (ro == NULL)
    {
	// Not present in the caches, but the user may still be a member.
	if (the_session->is_member(reader, cno))
	    ro = add_conference(cno, true);
	else
	    return KOM_NOT_MEMBER;
    }

    if (ro == NULL)
	return KOM_INDETERMINATE;

    return ro->estimated_num_unread();
}

    

Read_order_iterator
Unread_confs::current_conf_begin(int wanted_texts)
{
    assert(current_read_order != NULL);
    return current_read_order->begin(wanted_texts);
}

Read_order_iterator
Unread_confs::current_conf_end()
{
    assert(current_read_order != NULL);
    return current_read_order->end();
}

std::vector<Text_no>
Unread_confs::query_pending_footnotes() const
{
    assert(current_read_order != NULL);
    return current_read_order->query_pending_footnotes();
}

std::vector<Text_no>
Unread_confs::query_pending_comments() const
{
    assert(current_read_order != NULL);
    return current_read_order->query_pending_comments();
}

void
Unread_confs::set_pending_footnotes(const std::vector<Text_no> &arr)
{
    assert(current_read_order != NULL);
    current_read_order->set_pending_footnotes(arr);
}

void
Unread_confs::set_pending_comments(const std::vector<Text_no> &arr)
{
    assert(current_read_order != NULL);
    current_read_order->set_pending_comments(arr);
}


// The caller must make sure that the user actually is a member of the
// conference before calling this method, or confusion will arise.
Read_order *
Unread_confs::add_conference(Conf_no cno, bool last)
{
    if (need_unread_confs() != st_ok)
	return NULL;

    Read_order  * read_order;

    read_order = new Read_order(the_session, cno, reader);
    if (last)
	unread_confs.push_back(read_order);
    else
	unread_confs.push_front(read_order);

    return read_order;
    // Note: If there were no texts in this conference, it will be
    // moved to read_confs as soon as we start to prefetch from it.
}

Status
Unread_confs::join_conference(Conf_no cno, 
			      unsigned char priority,
			      unsigned short placement,
			      Membership_type type)
{
    // FIXME: There should be a way to insert the new conference in the
    // correct place. We do not know the order of the conferences. Big
    // lose.+++

    if (need_unread_confs() != st_ok)
	return st_error;

    bool present = (find_if(unread_confs.begin(), unread_confs.end(),
			    confno_eq(cno)) != unread_confs.end()
		    || find_if(read_confs.begin(), read_confs.end(),
			       confno_eq(cno)) != read_confs.end());

    Status ret = the_session->add_member(cno, reader, priority, placement,
					 type);

    if (ret != st_ok)
    {
	// FIXME: Errorcode
	return ret;
    }

    if (!present)
	add_conference(cno, true);

    return ret;
}

Kom_err
Unread_confs::disjoin_conference(Conf_no cno)			
{
    // FIXME: need_unread_confs should return error code
    if (need_unread_confs() != st_ok)
	return KOM_UNKNOWN_ERROR;

    Read_order *ro = NULL;
    std::list<Read_order*>::iterator ix = find_if(
	unread_confs.begin(), unread_confs.end(), confno_eq(cno));
    std::vector<Read_order*>::iterator iy;
    if (ix != unread_confs.end())
    {
	ro = *ix;
	iy = read_confs.end();
    }
    else
    {
	iy = find_if(read_confs.begin(), read_confs.end(), confno_eq(cno));
	if (iy != read_confs.end())
	    ro = *iy;
    }

    // FIXME: why do the work above if an error occurs?  This function 
    // would be simpler if the above code was moved below.
    Kom_err ret = the_session->sub_member(cno, reader);

    if (ret == KOM_NO_ERROR)
    {
	if (current_read_order == ro)
	    current_read_order = NULL;
	if (ix != unread_confs.end())
	{
	    assert(*ix == ro);
	    unread_confs.erase(ix);
	}
	else if (iy != read_confs.end())
	{
	    assert(*iy == ro);
	    read_confs.erase(iy);
	}
	delete ro;
    }

    return ret;
}

Kom_err
Unread_confs::set_last_read(Conf_no cno, 
			   Local_text_no last_text_read)
{
    // FIXME: There should be a way to insert the conference in the
    // correct place (in those cases where it hasn't already been
    // fetched.  We do not know the order of the conferences.  Big
    // lose.+++

    if (need_unread_confs() != st_ok)
	return KOM_INDETERMINATE;

    Read_order *ro = NULL;
    if (ro == NULL)
    {
	std::list<Read_order*>::iterator iter = find_if(
	    unread_confs.begin(), unread_confs.end(), confno_eq(cno));
	if (iter != unread_confs.end())
	    ro = *iter;
    }
    if (ro == NULL)
    {
	std::vector<Read_order*>::iterator iter = find_if(
	    read_confs.begin(), read_confs.end(), confno_eq(cno));
	if (iter != read_confs.end())
	    ro = *iter;
    }
    
    if (ro == NULL)
    {
	if (the_session->is_member(reader, cno))
	    ro = add_conference(cno, true);
	else
	    return KOM_NOT_MEMBER;
    }

    if (ro == NULL)
	return KOM_INDETERMINATE;

    return ro->set_last_read(last_text_read);
}

void
Unread_confs::new_text(const Async_text_created &msg)
{
    if (need_unread_confs() != st_ok)
	return;

    std::vector<Conf_no> not_present;

    Text_stat t = msg.text_stat();
    for (int ir = 0; ir < t.num_recipients(); ir++)
    {
	const Recipient *rcpt = t.recipient(ir);
	Conf_no confno = rcpt->recipient();
	bool present = find_if(unread_confs.begin(), unread_confs.end(),
			       confno_eq(confno)) != unread_confs.end();
	if (!present)
	{
	    std::vector<Read_order*>::iterator iter = find_if(
		read_confs.begin(), read_confs.end(), confno_eq(confno));
	    if (iter != read_confs.end())
	    {
		unread_confs.push_back(*iter);
		read_confs.erase(iter);
		present = true;
	    }
	}

	if (!present)
	{
	    Conf_no cno = rcpt->recipient();
	    not_present.push_back(cno);
	    the_session->prefetch_is_member(reader, cno);
	}
    }

    for (std::vector<Conf_no>::iterator iter = not_present.begin(); 
	 iter < not_present.end(); ++iter)
	if (the_session->is_member(reader, *iter))
	    add_conference(*iter, true);
}

Status
Unread_confs::need_one_mapped()
{
    Work_quota w;
    while (prefetch(((w = Work_quota(1,0,10)), &w)) == st_pending)
	if (the_session->connection()->drain(100) == st_error)
	    return st_error;
    return st_ok;
}

Status
Unread_confs::need_unread_confs()
{
    Work_quota w;
    while (!unread_confs_has_been_fetched
	   && prefetch(((w = Work_quota(1,0,10)), &w)) == st_pending)
    {
	if (the_session->connection()->drain(100) == st_error)
	    return st_error;
    }

    return unread_confs_has_been_fetched ? st_ok : st_error;
}

Session *
Unread_confs::session() const
{
    return the_session;
}

// eof
