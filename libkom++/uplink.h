// "Footn_to" and "comment_to" link objects.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_UPLINK_H
#define KOMXX_UPLINK_H

#include <ctime>

#include "kom-types.h"

class prot_a_LyStr;

class Uplink {
  public:
    Uplink();
    Uplink(const Uplink&);
    const Uplink &operator =(const Uplink &);

    int parse(bool is_footnote, prot_a_LyStr &prot_a_str);

    bool      is_footnote()const;
    Text_no   parent()     const;
    Pers_no   sender()     const;
    bool      sent_later() const;
    struct std::tm sent_at() const;
  private:
    enum { FOOTNOTE, COMMENT } upl_link_type;
    Text_no	upl_parent_text_no;
    Pers_no	upl_sender;	// 0 if not sent. Always 0 for FOOTNOTEs.
    bool	upl_sent_later;
    struct std::tm upl_sent_at;	// Only valid if sent_later is true.
};
#endif
