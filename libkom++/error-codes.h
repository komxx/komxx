// Error codes used by the LysKOM server.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_ERROR_CODES_H
#define KOMXX_ERROR_CODES_H

// Be careful when editing this enum.  You may have to change
// int_to_kom_err as well.
typedef enum 
{
    KOM_NO_ERROR = 0,		/* No error has occured */
    KOM_NOT_IMPL = 2,		/* Not implemented yet */
    KOM_OBSOLETE = 3,		/* No longer implemented */
    KOM_PWD = 4,			/* Wrong/illegal password */
    KOM_LONG_STR = 5,		/* String too long */
    KOM_LOGIN = 6,			/* Not logged in. */
    KOM_LOGIN_DISALLOWED = 7,	/* System is in 'singel-user mode' */
    KOM_CONF_ZERO = 8,		/* Attempt to use conference number 0. */
    KOM_UNDEF_CONF = 9,		/* Undefined or secret conference */
    KOM_UNDEF_PERS = 10,		/* Undefined or secret person */
    KOM_ACCESS = 11,		 	/* No 'read/write permission' */
    KOM_PERM = 12,			/* No permission */
    KOM_NOT_MEMBER = 13,             /* Not member in conf */
    KOM_NO_SUCH_TEXT = 14,		/* No such global text_no, or no access */
    KOM_TEXT_ZERO = 15,		/* Can't use text no 0 */
    KOM_NO_SUCH_LOCAL_TEXT = 16,	/* No such local text_no */
    KOM_LOCAL_TEXT_ZERO = 17,	/* Can't use local text no 0 */
    KOM_BAD_NAME = 18,		/* Too short/long or contains illegal chars */
    KOM_INDEX_OUT_OF_RANGE = 19,	/*  */
    KOM_CONF_EXISTS = 20,		/* Already exists */
    KOM_PERS_EXISTS = 21,		/* Already exists (no longer used) */
    KOM_SECRET_PUBLIC = 22,		/* Cannot be secret and !rd_prot */
    KOM_LETTER_BOX = 23,		/* Cannot change letter_box flag */
    KOM_LDB_ERR = 24,		/* Database is corrupted. */
    KOM_ILL_MISC = 25,		/* Illegal misc field.
				   err_stat holds field no */

    KOM_ILLEGAL_INFO_TYPE = 26,	/* Info_type parameter was illegal. This
				   means that there is a bug in the client. */
    KOM_ALREADY_RECIPIENT = 27,	/* Already recipient to this text. */
    KOM_ALREADY_COMMENT = 28,	/* Already comment to this text. */
    KOM_ALREADY_FOOTNOTE = 29,	/* Already footnote to this text. */
    KOM_NOT_RECIPIENT = 30,		/* Not recipient */
    KOM_NOT_COMMENT = 31,		/* Not comment to this text. */
    KOM_NOT_FOOTNOTE = 32,		/* Not footnote to this text. */
    KOM_RECIPIENT_LIMIT = 33,	/* Too many recipients */
    KOM_COMM_LIMIT = 34,		/* Too many comments */
    KOM_FOOT_LIMIT = 35,		/* Too many footnotes */
    KOM_MARK_LIMIT = 36,		/* Too many marks. */
    KOM_NOT_AUTHOR = 37,		/* Only the author may add footnotes or
				   delete texts. */


    KOM_NO_CONNECT = 38,		/* Can't connect to specified server */
    KOM_OUT_OF_MEMORY = 39,		/* Couldn't get memory for result */
    KOM_UNDEF_SESSION = 42,	/* No such session exists. */
    KOM_REGEX_ERROR = 43,	/* Regexp compilation failed. */
    KOM_NOT_MARKED = 44,	/* Attempt to unmark an unmarked text. */
    KOM_TEMPFAIL = 45,		/* Try again later. */
    KOM_LONG_ARRAY = 46,	/* Too long array supplied. */
    KOM_ANON_REJECTED = 47,	/* Anonymous text not allowed in conference. */
    KOM_ILLEGAL_AUX_ITEM = 48,  /* Malformed aux item. */
    KOM_AUX_ITEM_PERMISSION = 49, /* No permission. */
    KOM_UNKNOWN_ASYNC = 50,     /* Unknown async seen. */
    KOM_INTERNAL_ERROR = 51,    /* Internal server error detected. */
    KOM_FEATURE_DISABLED = 52,  /* Feature disabled. */
    KOM_MESSAGE_NOT_SENT = 53,  /* Async message not sent. */
    KOM_INVALID_MEMBERSHIP_TYPE = 54,  /* Bad membership type. */
    KOM_INVALID_RANGE = 55,     /* Lower limit higher than upper limit. */
    KOM_INVALID_RANGE_LIST = 56, /* Ranges not sorted. */
    KOM_UNDEFINED_MEASUREMENT = 57, /* That measurement is not made. */
    KOM_PRIORITY_DENIED = 58,   /* You lack privileges to do that. */
    KOM_WEIGHT_DENIED = 59,     /* You lack privileges to do that. */
    KOM_WEIGHT_ZERO = 60,       /* Weight must be non-zero. */
    KOM_BAD_BOOL = 61,          /* A bool must be sent as 0 or 1. */
    KOM_last_server_error,
    KOM_ANCIENT_SERVER = 201,	// The server does not understand thequestion.
    KOM_UNINITIALIZED = 202,	// The object is uninitialized.
    KOM_INDETERMINATE = 203,	// The exact cause of the error is lost.
    KOM_INVALIDATED = 204,	// Returned by invalidated objects.
    KOM_NO_USER_AREA = 205,	// The person has no user area.
    KOM_KEY_NOT_FOUND = 206,	// The specified key does not exist.
    KOM_UNKNOWN_ERROR = 207,	// The server returned an error code
				// that isn't known to kom++.
    KOM_BAD_HOLLERITH = 208,	// Protocol error.
    KOM_BAD_USER_AREA = 209,	// kom++ cannot parse the user area.
    KOM_max_errno
} Kom_err;

// Convert the integer to an error code.  This always returns one of
// the values in Kom_err (except KOM_last_server_error and
// KOM_max_errno) -- if the supplied integer is out of range
// KOM_UNKNOWN_ERROR is returned.
Kom_err int_to_kom_err(int);

#endif
