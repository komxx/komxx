// Fetch-on-demand cache for texts.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_TEXT_H
#define KOMXX_TEXT_H

#include <ctime>

#include "text-cache.h"
#include "LyStr.h"
#include "LyStr-errno.h"
#include "fetchable.h"

class Connection;
class Text_mass_cache;


class Text : public Fetchable {
  public:
    Text();
    Text(Text_no, Text_stat_cache *, Text_mass_cache *);
    Text(const Text &);
    ~Text();

    const Text & operator = (const Text &);

    Status   	prefetch() 	 const;
    Status      prefetch_text()  const;
    bool        prefetch_pending() const;
    bool	present() 	 const;
    bool	exists() 	 const;
    Kom_err	kom_errno()	 const;
    Text_no     text_no()        const;

    // Accessors to the various parts of the data.
    struct std::tm creation_time() const;
    Pers_no	author() 	 const;
    long        num_lines()      const;
    long        num_chars()      const;
    int         num_marks()      const;
    int         num_recipients() const;
    const Recipient* recipient(int) const;
    int		num_comment_in() const;
    Text_no	comment_in(int)	 const;
    int		num_footnote_in()const;
    Text_no	footnote_in(int) const;
    int		num_uplinks()	 const;
    const Uplink* uplink(int)	 const;
    int		num_aux()	 const;
    Aux_item    aux(int)	 const;

    // FIXME: do something clever about the end argument.
    LyStr_errno	text_mass(long begin=0, long end=0x7fffffff) const;
    // subject() and body() can be used to retriev only the subject
    // and body of the text.  The arguments to body() are relative to
    // the body, so they are NOT the same numbers as in text_mass().
    LyStr_errno subject() const;
    // FIXME: do something clever about the end argument.
    LyStr_errno body(long begin=0, long end=0x7fffffff) const;

    // Returns the time when this text was entered in the specified
    // conference with the specified local number.  This is most of
    // the time equal to creation_time(), but it may differ if this
    // text was added to the conference at a later time.
    struct std::tm entry_time(Conf_no, Local_text_no);

    // Utility functions. These might be quite costly, so
    // take care when using them.
    Status      mark_as_read(Conf_no confno = 0);
    Status      mark_as_unread(Conf_no confno = 0);
    Kom_err     mark(unsigned char mark_type = 255);
    Kom_err     unmark();

    // If CONF_NO is a recipient of this text, return the
    // Local_text_no it has. Otherwise, or if an error occurs, return
    // 0.
    Local_text_no  local_no_recipient(Conf_no conf_no);

    Kom_err     add_recipient(Conf_no conf_no);
    Kom_err     sub_recipient(Conf_no conf_no);

    Kom_err     modify_info(const std::vector<Aux_no> &del,
			    const std::vector<Aux_item> &add);

  private:
    Text_no tno;
    Text_stat_cache *ts_cache;
    Text_mass_cache *tm_cache;
};



class Texts {
  public:
    Texts(Connection *c, Async_dispatcher *async_disp);
    ~Texts();

    Text  operator[](Text_no);

    Text_stat_cache * read_text_stat_cache() const;
    Text_mass_cache * read_text_mass_cache() const;

  private:
    Texts(const Texts &);
    const Texts & operator = (const Texts &);

    Connection      * connection;
    Text_stat_cache * ts_cache;
    Text_mass_cache * tm_cache;
};


#endif
