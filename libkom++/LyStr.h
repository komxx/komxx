// A string class.  Why reuse code when you can roll your own?
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_LYSTR_H
#define KOMXX_LYSTR_H

#include <iosfwd>
#include <cstddef>
#include <string>

#include "memcheck.h"

// A simple string package.
class LyStr MEMCHECK {
  public:
    LyStr();
    LyStr(const char *c_str);
    LyStr(void *block, int len);
    LyStr(const LyStr &);
    ~LyStr();
    LyStr & operator = (const LyStr &);

    // operator << (...)  appends to the string
    LyStr &operator << (const LyStr &str);
    LyStr &operator << (char c);
    LyStr &operator << (unsigned char c);
    LyStr &operator << (signed char c);
    LyStr &operator << (short num);           // Append decimal number
    LyStr &operator << (unsigned short num);  // Append decimal number
    LyStr &operator << (int num);           // Append decimal number
    LyStr &operator << (unsigned int num);  // Append decimal number
    LyStr &operator << (long num);          // Append decimal number
    LyStr &operator << (unsigned long num); // Append decimal number
    void append(const char *buffer, long length);
    void append(const std::string &s);
    char & operator [] (long index);
    const char & operator [] (long index) const;
    char & first();		// Get the first character
    const char &first() const;	// Look at the first character
    long strlen() const;
    bool empty() const;
    bool nonempty() const;
    void clear();
    void remove_first(long no=1);// Remove first NO chars from the string.
    void remove_last(long no=1); // Remove last NO chars from the string.
    void skipspace();		 // Remove leading whitespace

    // Return index to first occurance of ``c'', or -1 if ``c'' is not
    // found in the string.
    long strchr(char c) const;

    // Return true if ``c'' is present in the string.
    bool present(char c) const;

    // Check if the beginning of a string is equal to the given string.
    bool prefix(const LyStr &);
    bool prefix(const char *);
    
    // Create a malloc'd copy of the string. If start and length are
    // given, a substring starting at start and length characters long
    // is returned. The malloced copy is always nul-terminated. The
    // nul is not counted in length.
    // The default for length is to include the entire string.
    char *malloc_copy(long start=0, long length= -1) const;

    // Obtain a read-only pointer to the contents of the string.
    // When you are done with the contents, you must call view_done.
    // These methods are typically used in code such as this example:
    //      LyStr foo = bar();
    //      const char *buf = foo.view_block();
    //      int status = write(fd, buf, foo.strlen());
    //      foo.view_done(buf);
    // In the current implementation of LyStr, view_block simply returns
    // a pointer into the internal storage and view_done is a noop; in
    // a future implementation view_block may have to allocate the
    // block and view_done will then free it.
    //
    // A trailing NUL is added past the end of the block returned by
    // view_block().
    const char *view_block() const;
    void view_done(const char *) const;

    // Create a new string, consisting of the first SIZE characters
    // from the string.
    LyStr substr(long size) const;

    // Create a new string, consisting of the characters in the
    // range [begin, end).
    LyStr substr(long begin, long end) const;

    // Extract (and remove) the first whitespace-separated word.
    // Any leading whitespace is skipped.
    // All whitespace after the word is left intact.
    LyStr extract_word();

    // Extract (and remove) the first number.  Any leading whitespace
    // is removed.  The parsing stops as soon as a non-digit is found.
    // ERROR is set to TRUE if no digit was found.  It is never
    // cleared. Overflow is not handled.
    long extract_number(bool &error);

    friend std::ostream &operator <<(std::ostream &, const LyStr &);

    // Compare strings for equality/inequality.
    friend bool operator ==(const LyStr &, const LyStr &);
    friend bool operator !=(const LyStr &, const LyStr &);
    friend bool operator <(const LyStr &, const LyStr &);

    // Return a hash value.  Nothing is guarranteed about this value
    // except that the same string always returns the same value.
    // This hash is not cryptographically secure.
    size_t hash() const;
  protected:
    // Make sure that at least ``length'' more characters can be added
    // to the string without moving/reallocating the string.
    void need_space(long length);

    char *data;			// size chars big
    long size;			// The current capacity.
    long used;			// Number of chars used.
    long origin;		// First char used.
};

// Read until the next newline, and append to the string. 
// The newline is pushed back, and the string is not initially cleared.
std::istream &operator >>(std::istream &, LyStr &);

#endif
