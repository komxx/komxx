// Fetch-on-demand cache of persons.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_PERSON_H
#define KOMXX_PERSON_H

#include "conference.h"
#include "priv-bits.h"
#include "personal-flags.h"

class Pers_cache;

class Person : public Conference {
  public:
    Person(const Person &);
    Person(Pers_no, Pers_cache *, Conf_cache *, UConf_cache *);
    const Person & operator = (const Person &);
    ~Person();

    Status   prefetch() 	  const;
    Status   prefetch_conf_only() const;
    Status   prefetch_uconf_only() const;
    Status   prefetch_pers_only() const;
    bool     exists()    	  const;
    bool     pers_exists()    	  const;
    bool     present()    	  const;
    bool     conf_present()    	  const;
    bool     pers_present()    	  const;
    Kom_err  kom_errno()	  const;
    Pers_no  pers_no()  	  const;

    Text_no	    user_area() 	  const;
    Priv_bits	    privileges() 	  const;
    Personal_flags  flags() 		  const;
    struct std::tm  last_login() 	  const;
    unsigned long   total_time_present()  const;
    unsigned long   sessions() 		  const;
    unsigned long   created_lines() 	  const;
    unsigned long   created_bytes() 	  const;
    unsigned long   read_texts() 	  const;
    unsigned long   no_of_text_fetches()  const;
    unsigned short  created_persons() 	  const;
    unsigned short  created_confs() 	  const;
    LyStr	    username() 		  const;
    Local_text_no   first_created_text()  const;
    unsigned long   no_of_created_texts() const;
    unsigned short  no_of_marks() 	  const;
    unsigned short  no_of_confs() 	  const;

  private:
    Pers_cache * cache;
};



class Persons {
  public:
    Persons();
    Persons(const Conferences &);
    Persons(const Persons &);
    ~Persons();

    const Persons & operator = (const Persons &);
    void  set_conferences(const Conferences &);

    Person  operator[](Pers_no);

    Pers_cache * read_pers_cache() const;

  private:
    Connection  * connection;
    Pers_cache  * pers_cache;
    Conf_cache  * conf_cache;
    UConf_cache * uconf_cache;
};


#endif
