// Store a Version-Info Protocol A result.
//
// Copyright (C) 2005  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_VERSION_INFO_H
#define KOMXX_VERSION_INFO_H

#include "prot-a-LyStr.h"


class Version_info {
  public:
    Version_info();
    Version_info(long, const LyStr &, const LyStr &);
    Version_info(const Version_info&);
    const Version_info &operator =(const Version_info &);
    
    long        protocol_version() const;
    LyStr       server_software() const;
    LyStr       software_version() const;
  private:
    long        protocol;
    LyStr       server;
    LyStr       software;
};
#endif
