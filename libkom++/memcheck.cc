// Debugging object allocation and destruction
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>

#include "memcheck.h"

long Memcheck::num_memchecks = 0;

#if ENABLE_MEMCHECK

Memcheck::Memcheck()
    : status(INITIALIZED), allocated_status(new memstat)
{
    *allocated_status = INITIALIZED;
    num_memchecks++;
}

Memcheck::~Memcheck()
{
    assert(status != DESTRUCTED);
    assert(status == INITIALIZED);
    assert(allocated_status != NULL);
    assert(*allocated_status == INITIALIZED);

    *allocated_status = DESTRUCTED;
    delete allocated_status;
    allocated_status = NULL;
    status = DESTRUCTED;
    num_memchecks--;
}

Memcheck::Memcheck(const Memcheck&m)
    : status(INITIALIZED), allocated_status(new memstat)
{
    assert(m.status == INITIALIZED);
    assert(m.allocated_status != NULL);
    assert(*m.allocated_status == INITIALIZED);
    *allocated_status = INITIALIZED;
    num_memchecks++;
}

Memcheck &
Memcheck::operator =(const Memcheck &m)
{
    assert(m.status == INITIALIZED);
    assert(m.allocated_status != NULL);
    assert(*m.allocated_status == INITIALIZED);
    assert(status == INITIALIZED);
    assert(allocated_status != NULL);
    assert(*allocated_status == INITIALIZED);
    return *this;
}

#endif

long
Memcheck::allocated_memchecks()
{
    return num_memchecks;
}

// eof
