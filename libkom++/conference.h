// Fetch-on-demand conference object.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_CONFERENCE_H
#define KOMXX_CONFERENCE_H

#include <ctime>
#include <vector>

#include "misc.h"
#include "fetchable.h"
#include "LyStr.h"
#include "kom-types.h"

class Aux_item;
class Conf_cache;
class UConf_cache;
class Conf_type;
class Connection;
class Async_dispatcher;

class Conference : public Fetchable {
  public:
    Conference(Conf_no, Conf_cache *, UConf_cache *);
    Conference(const Conference &);
    ~Conference();

    const Conference & operator = (const Conference &);

    Status      prefetch()   const;
    bool        prefetch_pending() const;
    Kom_err	kom_errno()  const;
    bool	present()    const;
    bool	exists()     const;

    // Variants of the above methods that work using
    // get_uconf_question.  These are cheaper, and should be used when
    // you only need the uconf part.  If the entire status is already
    // cached, these functions will return information about that
    // status instead.
    
    virtual Status      prefetch_uconf()   const;
    virtual bool        prefetch_uconf_pending() const;
    virtual Kom_err     uconf_kom_errno()  const;
    virtual bool	uconf_present()    const;
    virtual bool	uconf_exists()     const;

    Conf_no     conf_no()    const;
    Connection* connection() const;

    // get_uconf_question methods.  Note: if the entire conference
    // status is fetched for any reason, these will get the
    // information from the conference status instead.
    Conf_type		type()			const;
    Garb_nice		nice()			const;
    Garb_nice		keep_commented()        const;
    LyStr		name()			const;
    Local_text_no	highest_local_no()	const;

    // get_conf_question methods.
    Pers_no		creator()		const;
    struct std::tm	creation_time()		const;
    Text_no		presentation()		const;
    Conf_no		supervisor()		const;
    Conf_no		permitted_submitters()	const;
    Conf_no		super_conf()		const;
    struct std::tm	last_written()		const;
    Text_no		msg_of_day()		const;
    unsigned short	no_of_members()		const;
    Local_text_no	first_local_no()	const;
    unsigned long	no_of_texts()		const;
    Garb_nice		expire()                const;
    int 		num_aux()               const;
    Aux_item 		aux(int)                const;

    // Methods that change information on the server, and also update
    // the cached information.
    Kom_err             set_type(Conf_type conf_type);
    Kom_err             change_name(const LyStr &new_name);
    Kom_err             set_permitted_submitters(Conf_no new_perm_sub);
    Kom_err             modify_info(const std::vector<Aux_no> &del,
				    const std::vector<Aux_item> &add);
    Kom_err	        set_expire(Garb_nice);
    Kom_err	        set_garb_nice(Garb_nice);
    Kom_err	        set_keep_commented(Garb_nice);

  private:
    Conf_no      cno;
    Conf_cache * cache;
    UConf_cache * ucache;

    bool bypass_uconf() const;
};



class Conferences {
  public:
    Conferences(Connection *, Async_dispatcher *async_disp);
    ~Conferences();

    Connection *connection() const;

    Conference  operator[](Conf_no) const;

    Conf_cache * read_conf_cache() const;
    UConf_cache * read_uconf_cache() const;

  private:
    Conferences(const Conferences &); // N/A
    const Conferences & operator = (const Conferences &); // N/A

    Connection  * conn;
    Conf_cache  * conf_cache;
    UConf_cache * uconf_cache;
};


#endif
