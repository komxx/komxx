// Create-on-demand map of mappings for conferences.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_MAP_CACHE_H
#define KOMXX_MAP_CACHE_H

#include <map>

#include "async.h"
#include "kom-types.h"
#include "text-mapping.h"

class Async_text_created;
class Async_dispatcher;
class Conferences;

template <class M>
class Map_cache {
  public:
    Map_cache(Connection *conn, Async_dispatcher *async_disp);
    Map_cache(const Map_cache&);
    Map_cache& operator=(const Map_cache&);
    ~Map_cache();

    // Methods working on the entire cache:
    void        clear();
    Connection *connection() const;

    // Methods working on individual conf-stats.
    M get(Conf_no cno);

    // The cache automatically registers this callback method with the
    // Async_dispatcher.  This method should not be used in any other
    // way.
    void        new_text(const Async_text_created &msg);

  private:
    typedef std::map<Conf_no, M> Mtmap;

    Connection  *m_conn;
    Async_dispatcher *async_disp;
    Async_handler_tag<Async_text_created> cr_tag;

    Mtmap        mapmap;
};

#endif
