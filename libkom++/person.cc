// Fetch-on-demand cache of persons.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>
#include <cstdlib>

#include "person.h"
#include "pers-cache.h"


// ================================================================
// ================================================================
//                    Class Person

// ================================================================ 
//                    Constructors and Destructors                  


Person::Person(Pers_no pno, 
	       Pers_cache *pcache,
	       Conf_cache *ccache,
	       UConf_cache *uccache)
  : Conference(pno, ccache, uccache),
    cache(pcache)
{
}
 
Person::Person(const Person &pi)
    : Conference(pi), cache(pi.cache)
{
}


Person::~Person()
{
}


// ================================================================ 
//

const Person & 
Person::operator = (const Person &p)
{
    Conference::operator=(p);
    cache = p.cache;
    return *this;
}

Status
Person::prefetch() const
{
    Status p = cache->prefetch(conf_no());
    if (p == st_error)
	return p;

    Status c = Conference::prefetch();
    if (c == st_error || c == st_pending)
	return c;
    return p;
}

Status
Person::prefetch_conf_only() const
{
    return Conference::prefetch();
}

Status
Person::prefetch_uconf_only() const
{
    return Conference::prefetch_uconf();
}

Status
Person::prefetch_pers_only() const
{
    return cache->prefetch(conf_no());
}

// Return true only if both the person and the conference are present
// in the cache.  Otherwise problems may arise if the person is
// deleted while this client is trying to fetch it.
bool
Person::exists() const
{
    switch (prefetch())
    {
    case st_ok:
	return true;
    case st_error:
	return false;
    case st_pending:
	return (bool)(kom_errno() == KOM_NO_ERROR);
    }
    std::abort();
}

bool
Person::pers_exists() const
{
    return cache->get(conf_no()).kom_errno() == KOM_NO_ERROR;
}

// Returns KOM_NO_ERROR only if both the person and conference
// statuses have been fetched.  If fetching any of the statuses fails,
// this will return one of the error codes.  No promise is given
// regarding which error code is returned if both the person and
// conference fails.
Kom_err
Person::kom_errno() const
{
    prefetch();
    Kom_err e = cache->get(conf_no()).kom_errno();
    if (e != KOM_NO_ERROR)
	return e;
    else
	return Conference::kom_errno();
}

// Returns true only if both the person and conferenct statuses are
// present.
bool
Person::present() const
{
    return Conference::present() && cache->present(conf_no());
}

bool
Person::conf_present() const
{
    return Conference::present();
}

bool
Person::pers_present() const
{
    return cache->present(conf_no());
}

Pers_no
Person::pers_no() const
{
    return conf_no();
}

Text_no
Person::user_area() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.user_area();
}

Priv_bits
Person::privileges() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.privileges();
}

Personal_flags
Person::flags() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.flags();
}

struct tm
Person::last_login() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.last_login();
}

unsigned long
Person::total_time_present() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.total_time_present();
}

unsigned long
Person::sessions() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.sessions();
}

unsigned long
Person::created_lines() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.created_lines();
}

unsigned long
Person::created_bytes() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.created_bytes();
}

unsigned long
Person::read_texts() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.read_texts();
}

unsigned long
Person::no_of_text_fetches() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.no_of_text_fetches();
}

unsigned short
Person::created_persons() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.created_persons();
}

unsigned short
Person::created_confs() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.created_confs();
}

LyStr
Person::username() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.username();
}

Local_text_no
Person::first_created_text() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.first_created_text();
}

unsigned long
Person::no_of_created_texts() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.no_of_created_texts();
}

unsigned short
Person::no_of_marks() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.no_of_marks();
}

unsigned short
Person::no_of_confs() const
{
    Pers_stat p=cache->get(conf_no());
    assert(p.kom_errno() == KOM_NO_ERROR);
    return p.no_of_confs();
}


// ================================================================
// ================================================================
//                            Class Persons


// ================================================================ 
//                    Constructors and Destructors                  


Persons::Persons()
    : connection(NULL),
      pers_cache(NULL),
      conf_cache(NULL)
{
}
 
Persons::Persons(const Conferences& confs)
    : connection(confs.connection()),
      pers_cache(new Pers_cache(confs.connection())),
      conf_cache(confs.read_conf_cache()),
      uconf_cache(confs.read_uconf_cache())
{
}

Persons::~Persons()
{
    if (pers_cache)
	delete pers_cache;
}


// ================================================================ 
//

void
Persons::set_conferences(const Conferences &confs)
{
    assert(connection == NULL);
    connection = confs.connection();
    conf_cache = confs.read_conf_cache();
    uconf_cache = confs.read_uconf_cache();
    pers_cache = new Pers_cache(confs.connection());
}

Person
Persons::operator [](Pers_no persno)
{
    assert(pers_cache != NULL);
    return Person(persno, pers_cache, conf_cache, uconf_cache);
}

Pers_cache *
Persons::read_pers_cache() const
{
    return pers_cache;
}
