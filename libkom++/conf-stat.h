// A Conf-status class.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_CONF_STAT_H
#define KOMXX_CONF_STAT_H

#include <ctime>
#include <vector>

#include "conf-type.h"
#include "kom-types.h"
#include "LyStr.h"
#include "memcheck.h"
#include "error-codes.h"
#include "aux-item.h"

class prot_a_LyStr;

class Conf_stat MEMCHECK {
  public:
    Conf_stat();
    Conf_stat(const Conf_stat &conf_stat);
    Conf_stat(Conf_no conf_no, bool old, prot_a_LyStr &prot_a_str);
    Conf_stat(Kom_err kom_errno);
    ~Conf_stat();
    Conf_stat &operator=(const Conf_stat &);

    Kom_err   kom_errno() const;

    Pers_no	        creator() 	       const;
    struct std::tm	creation_time()        const;
    Text_no	        presentation() 	       const;
    Conf_no	        supervisor() 	       const;
    Conf_no	        permitted_submitters() const;
    Conf_no	        super_conf() 	       const;
    Conf_type	 	type() 		       const;
    struct std::tm      last_written() 	       const;
    Text_no	        msg_of_day() 	       const;
    Garb_nice	        nice() 		       const;
    Garb_nice	        keep_commented()       const;
    LyStr		name() 		       const;
    unsigned short	no_of_members()        const;
    Local_text_no	first_local_no()       const;
    unsigned long	no_of_texts() 	       const;
    Garb_nice	        expire()               const;
    int                 num_aux()              const;
    Aux_item            aux(int)               const;

    void set_type(Conf_type conf_type);
    void change_name(const LyStr &new_name);
    void set_permitted_submitters(Conf_no permsub);
    void inform_text_exists(Local_text_no lno,
			    const struct tm &creation_time);
    void set_expire(Garb_nice expire);
    void set_garb_nice(Garb_nice garb_nice);
    void set_keep_commented(Garb_nice keep_commented);
    
  private:
    struct Conf_stat_internal {
      public:
	Conf_stat_internal();
	Conf_stat_internal(Conf_no conf_no, 
			   bool old, 
			   prot_a_LyStr &prot_a_str);
	Conf_stat_internal(const Conf_stat_internal &);
	const Conf_stat_internal &operator =(const Conf_stat_internal &);

	int     refcount;
	Conf_no	this_conf_no;
	Kom_err kom_errno;

	Pers_no	        creator;
	struct std::tm	creation_time;
	Text_no	        presentation;
	Conf_no	        supervisor;
	Conf_no	        permitted_submitters;
	Conf_no	        super_conf;
	Conf_type	type;
	struct std::tm	last_written;
	Text_no	        msg_of_day;
	Garb_nice	nice;
	Garb_nice	keep_commented;
	LyStr		conf_name;
	unsigned short	no_of_members;
	Local_text_no	first_local_no;
	unsigned long	no_of_texts;
	Garb_nice	expire;
	std::vector<Aux_item> aux;
    };

    bool valid() const;

    Conf_stat_internal  * csi;
};

#endif
