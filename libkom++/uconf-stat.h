// A UConf-status class.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_UCONF_STAT_H
#define KOMXX_UCONF_STAT_H

#include <ctime>

#include "conf-type.h"
#include "kom-types.h"
#include "LyStr.h"
#include "memcheck.h"
#include "error-codes.h"

class prot_a_LyStr;

class UConf_stat MEMCHECK {
  public:
    UConf_stat();
    UConf_stat(const UConf_stat &conf_stat);
    UConf_stat(Conf_no conf_no, prot_a_LyStr &prot_a_str);
    UConf_stat(Kom_err kom_errno);
    ~UConf_stat();
    UConf_stat &operator=(const UConf_stat &);

    Kom_err   kom_errno() const;

    Conf_type	 	type() 		       const;
    Garb_nice	        nice() 		       const;
    LyStr		name() 		       const;
    Local_text_no	highest_local_no()     const;

    void set_type(Conf_type conf_type);
    void change_name(const LyStr &new_name);
    void inform_text_exists(Local_text_no lno);
    
  private:
    struct UConf_stat_internal {
      public:
	UConf_stat_internal();
	UConf_stat_internal(Conf_no conf_no, prot_a_LyStr &prot_a_str);
	UConf_stat_internal(const UConf_stat_internal &);
	const UConf_stat_internal &operator =(const UConf_stat_internal &);

	int     refcount;
	Conf_no	this_conf_no;
	Kom_err kom_errno;

	Conf_type	type;
	Garb_nice	nice;
	LyStr		conf_name;
	Local_text_no	highest_local_no;
    };

    bool valid() const;

    UConf_stat_internal  * csi;
};

#endif
