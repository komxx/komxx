// Fetch-on-demand map from local to global text numbers.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_TEXT_MAPPING_H
#define KOMXX_TEXT_MAPPING_H

#include <vector>

#include "kom-types.h"
#include "misc.h"

class Connection;
class local_to_global_map;
class local_to_global_question;
class map_created_texts_question;

// A fetch-on-demand mapping from something to global text numbers.
// The Q class determines what mapping is stored:
//
//     Q=local_to_global_question: map from the local text numbers of
//     a conference.
//
//     Q=map_created_texts_question: map from sequence number of the
//     texts created by a certain author.
//
template <class Q>
class Text_mapping_basic {
  public:
    Text_mapping_basic();	// Creates an invalid object.  Use
				// valid() to test validity.

    Text_mapping_basic(Conf_no cno, Connection *conn, int chunk_size=50);
    Text_mapping_basic(const Text_mapping_basic&);
    ~Text_mapping_basic();

    Text_mapping_basic& operator=(const Text_mapping_basic&);

    bool valid() const;

    Text_no operator [](Local_text_no lno);

    // Returns 0 if no higher-numbered text exists (yet).
    Local_text_no next_existing(Local_text_no first);
    
    // Prefetch FIRST.
    Status prefetch(Local_text_no first);

    void add(Local_text_no key, Text_no text);
    void add_unknown();

  private:
    void add(const local_to_global_map& l2g);
    void add_zero_range(Local_text_no begin, Local_text_no end);
    int chunk_size() const;

    struct TMI;
    TMI *tmi;
    
    class Pending;
    struct got_reply;

    // Find the prefetch entry for lno, creating one if needed.  The
    // iterator is only valid until anything modifies tmi->pending.
    typename std::vector<Pending>::iterator get_prefetch(Local_text_no lno);

    void poll_pending();	// Deal with outstanding questions.
};

typedef Text_mapping_basic<local_to_global_question> Text_mapping;
typedef Text_mapping_basic<map_created_texts_question> Created_mapping;

#endif
