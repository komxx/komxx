// Cache of conf-statuses.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>

#include "conf-cache.h"
#include "question.h"
#include "misc.h"
#include "error-codes.h"
#include "text-stat.h"
#include "recipient.h"
#include "async_text_created.h"
#include "get_conf_stat_q.h"

static Conf_stat bad_conf_stat;
static Conf_stat zero_conf_stat(KOM_CONF_ZERO);

Conf_cache::Conf_cache(Connection *c, Async_dispatcher *disp)
    : conn(c),
      async_disp(disp)
{
    Kom_auto_ptr<Async_callback<Async_text_created> > ptr;
    ptr.reset(async_callback_method(this, &Conf_cache::new_text).release());
    cr_tag = disp->register_text_created_handler(ptr);
}

Conf_cache::~Conf_cache()
{
    async_disp->unregister_text_created_handler(cr_tag);
}

Connection *
Conf_cache::connection() const
{
    return conn;
}

bool
Conf_cache::present(Conf_no cno) const
{
    return statusmap.find(cno) != statusmap.end();
}

Status
Conf_cache::prefetch(Conf_no cno)
{
    if (cno <= 0)
	return st_error;

    Ccmap::iterator stat_iter = statusmap.find(cno);
    if (stat_iter != statusmap.end())
    {
	if ((*stat_iter).second.kom_errno() == KOM_NO_ERROR)
	    return st_ok;
	else
	    return st_error;
    }
    else
    {
	Ccreqmap::iterator req_iter = pendingmap.find(cno);
	if (req_iter == pendingmap.end())
	{
	    pendingmap[cno] = new get_conf_stat_question(conn, cno);
	    return st_pending;
	}
	else
	{
	    switch ((*req_iter).second->status())
	    {
	    case st_pending:
		return st_pending;
	    case st_ok:
		statusmap[cno] = (*req_iter).second->result();
		delete (*req_iter).second;
		pendingmap.erase(req_iter);
		return st_ok;
	    case st_error:
		statusmap[cno] = Conf_stat((*req_iter).second->error());
		delete (*req_iter).second;
		pendingmap.erase(req_iter);
		return st_error;
	    }
	}
    }
    std::abort();
}


bool
Conf_cache::prefetch_pending(Conf_no cno)
{
    if (cno <= 0)
	return false;

    Ccreqmap::iterator iter = pendingmap.find(cno);

    if (iter == pendingmap.end())
	return false;

    return prefetch(cno) == st_pending;
}


Conf_stat
Conf_cache::get(Conf_no cno)
{
    if (cno <= 0)
	return zero_conf_stat;

    Ccmap::iterator status_iter = statusmap.find(cno);
    if (status_iter != statusmap.end())
	return (*status_iter).second;
    else 
    {
	Ccreqmap::iterator req_iter = pendingmap.find(cno);
	if (req_iter != pendingmap.end())
	{
	    Conf_stat res;
	    get_conf_stat_question *gtr = (*req_iter).second;
	    if (gtr->receive() == st_ok)
		res = gtr->result();
	    else
		res = Conf_stat(gtr->error());
	    statusmap[cno] = res;
	    delete gtr;
	    pendingmap.erase(req_iter);
	    return res;
	}
	else		
	{
	    // Not present in cache, and no outstanding question.
	    Conf_stat res;
	    get_conf_stat_question gt(conn, cno);
	    if (gt.receive() == st_ok)
		res = gt.result();
	    else
		res = Conf_stat(gt.error());
	    statusmap[cno] = res;
	    return res;
	}
    }
    std::abort();
}

void
Conf_cache::new_text(const Async_text_created &msg)
{
    Text_stat t = msg.text_stat();
    for (int ix = 0; ix < t.num_recipients(); ix++)
    {
	const Recipient *recpt = t.recipient(ix);
	Ccmap::iterator iter = statusmap.find(recpt->recipient());
	if (iter != statusmap.end())
	{
	    // FIXME: no need for the temporary object cs once this compiles.
	    Conf_stat cs = (*iter).second;
	    cs.inform_text_exists(recpt->local_no(), t.creation_time());
	}
    }
}
