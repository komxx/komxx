// Person status objects.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>
#include <cassert>

#include "error-codes.h"
#include "pers-stat.h"
#include "time-utils.h"

// ================================================================
//                    Pers_stat_internal methods

Pers_stat::Pers_stat_internal::Pers_stat_internal()
  : kom_errno(KOM_NO_ERROR),
    ps_user_area(0),
    ps_total_time_present(0),
    ps_sessions(0),
    ps_created_lines(0),
    ps_created_bytes(0),
    ps_read_texts(0),
    ps_no_of_text_fetches(0),
    ps_created_persons(0),
    ps_created_confs(0),	
    ps_username(""),
    ps_first_created_text(0),
    ps_no_of_created_texts(0),
    ps_no_of_marks(0),
    ps_no_of_confs(0)
{
}

Pers_stat::Pers_stat_internal::Pers_stat_internal(const Pers_stat_internal& p)
  : kom_errno(p.kom_errno),
    ps_user_area(p.ps_user_area),
    ps_privileges(p.ps_privileges),
    ps_flags(p.ps_flags),
    ps_last_login(p.ps_last_login),
    ps_total_time_present(p.ps_total_time_present),
    ps_sessions(p.ps_sessions),
    ps_created_lines(p.ps_created_lines),
    ps_created_bytes(p.ps_created_bytes),
    ps_read_texts(p.ps_read_texts),
    ps_no_of_text_fetches(p.ps_no_of_text_fetches),
    ps_created_persons(p.ps_created_persons),
    ps_created_confs(p.ps_created_confs),	
    ps_username(p.ps_username),
    ps_first_created_text(p.ps_first_created_text),
    ps_no_of_created_texts(p.ps_no_of_created_texts),
    ps_no_of_marks(p.ps_no_of_marks),
    ps_no_of_confs(p.ps_no_of_confs)
{
}

Pers_stat::Pers_stat_internal::Pers_stat_internal(Pers_no pno, 
		     prot_a_LyStr &prot_a_string)
    : ps_pers_no(pno), kom_errno(KOM_NO_ERROR)
{
    LyStr_errno tmp = prot_a_string.stos();
    assert(tmp.kom_errno == KOM_NO_ERROR);
    ps_username = tmp.str;
    ps_privileges = parse_priv_bits(prot_a_string);
    ps_flags = parse_personal_flags(prot_a_string);
    ps_last_login = parse_time(prot_a_string);
    ps_user_area = prot_a_string.stol();
    ps_total_time_present = prot_a_string.stol();
    ps_sessions = prot_a_string.stol();
    ps_created_lines = prot_a_string.stol();
    ps_created_bytes = prot_a_string.stol();
    ps_read_texts = prot_a_string.stol();
    ps_no_of_text_fetches = prot_a_string.stol();
    ps_created_persons = prot_a_string.stol();
    ps_created_confs = prot_a_string.stol();
    ps_first_created_text = prot_a_string.stol();
    ps_no_of_created_texts = prot_a_string.stol();
    ps_no_of_marks = prot_a_string.stol();
    ps_no_of_confs = prot_a_string.stol();
}

// const Pers_stat::Pers_stat_internal &
// Pers_stat::Pers_stat_internal::operator =(const Pers_stat_internal &)
// {
//     abort();
// }

// ================================================================
//                         Pers_stat methods

Pers_stat::Pers_stat()
{
    psi = NULL;
}


Pers_stat::Pers_stat(const Pers_stat &pers_stat)
{
    if (pers_stat.psi != NULL)
    {
	pers_stat.psi->refcount++;
    }
    psi = pers_stat.psi;
}


Pers_stat::Pers_stat(Pers_no pers_no, prot_a_LyStr &prot_a_str)
{
    psi = new Pers_stat_internal(pers_no, prot_a_str);
    psi->refcount = 1;
}

Pers_stat::Pers_stat(Kom_err errno)
{
    psi = new Pers_stat_internal;
    psi->refcount = 1;
    psi->kom_errno = errno;
}


Pers_stat::~Pers_stat()
{
    if (psi != NULL) 
    {
	psi->refcount--;
	if (psi->refcount == 0) 
	{
	    delete psi;
	}
    }
}


Pers_stat &
Pers_stat::operator=(const Pers_stat &pers_stat)
{
    if (&pers_stat != this) 
    {
	// Delete the old Pers_stat_internal if this was the last reference.
	if (psi != NULL)
	{
	    psi->refcount--;
	    if (psi->refcount == 0) 
	    {
		delete psi;
	    }
	}
	
	psi = pers_stat.psi;
	if (psi != NULL)
	    psi->refcount++;
    }

    return *this;
}


bool
Pers_stat::valid() const
{
    return psi != NULL && psi->kom_errno == 0;
}

Kom_err
Pers_stat::kom_errno() const
{
    if (psi == NULL)
	return KOM_UNINITIALIZED;
    else
	return psi->kom_errno;
}

Text_no         
Pers_stat::user_area() const
{
    assert(valid());
    return psi->ps_user_area;
}

Priv_bits       
Pers_stat::privileges() const
{
    assert(valid());
    return psi->ps_privileges;
}

Personal_flags  
Pers_stat::flags() const
{
    assert(valid());
    return psi->ps_flags;
}

struct tm       
Pers_stat::last_login() const
{
    assert(valid());
    return psi->ps_last_login;
}

unsigned long   
Pers_stat::total_time_present() const
{
    assert(valid());
    return psi->ps_total_time_present;
}

unsigned long   
Pers_stat::sessions() const
{
    assert(valid());
    return psi->ps_sessions;
}

unsigned long   
Pers_stat::created_lines() const
{
    assert(valid());
    return psi->ps_created_lines;
}

unsigned long   
Pers_stat::created_bytes() const
{
    assert(valid());
    return psi->ps_created_bytes;
}

unsigned long   
Pers_stat::read_texts() const
{
    assert(valid());
    return psi->ps_read_texts;
}

unsigned long   
Pers_stat::no_of_text_fetches() const
{
    assert(valid());
    return psi->ps_no_of_text_fetches;
}

unsigned short  
Pers_stat::created_persons() const
{
    assert(valid());
    return psi->ps_created_persons;
}

unsigned short  
Pers_stat::created_confs() const
{
    assert(valid());
    return psi->ps_created_confs;
}

LyStr           
Pers_stat::username() const
{
    assert(valid());
    return psi->ps_username;
}

Local_text_no   
Pers_stat::first_created_text() const
{
    assert(valid());
    return psi->ps_first_created_text;
}

unsigned long   
Pers_stat::no_of_created_texts() const
{
    assert(valid());
    return psi->ps_no_of_created_texts;
}

unsigned short  
Pers_stat::no_of_marks() const
{
    assert(valid());
    return psi->ps_no_of_marks;
}

unsigned short  
Pers_stat::no_of_confs() const
{
    assert(valid());
    return psi->ps_no_of_confs;
}

Pers_no
Pers_stat::pers_no() const
{
    assert(valid());
    return psi->ps_pers_no;
}
