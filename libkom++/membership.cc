// Handle a membership in a conference.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>
#include <algorithm>

#include "error-codes.h"
#include "prot-a-LyStr.h"
#include "membership.h"
#include "time-utils.h"


// ================================================================
//                 Membership_type methods

Membership_type::Membership_type()
  : mt_invitation(false),
    mt_passive(false),
    mt_secret(false),
    mt_passive_message_invert(false),
    mt_reserved2(false),
    mt_reserved3(false),
    mt_reserved4(false),
    mt_reserved5(false)
{
}

Membership_type::Membership_type(bool invitation,
				 bool passive,
				 bool secret,
				 bool passive_message_invert,
				 bool reserved2,
				 bool reserved3,
				 bool reserved4,
				 bool reserved5)
  : mt_invitation(invitation),
    mt_passive(passive),
    mt_secret(secret),
    mt_passive_message_invert(passive_message_invert),
    mt_reserved2(reserved2),
    mt_reserved3(reserved3),
    mt_reserved4(reserved4),
    mt_reserved5(reserved5)
{
}

Membership_type::Membership_type(prot_a_LyStr &str)
{
    str.parse_bit(mt_invitation);
    str.parse_bit(mt_passive);
    str.parse_bit(mt_secret);
    str.parse_bit(mt_passive_message_invert);
    str.parse_bit(mt_reserved2);
    str.parse_bit(mt_reserved3);
    str.parse_bit(mt_reserved4);
    str.parse_bit(mt_reserved5);
}


Membership_type::Membership_type(const Membership_type &o)
  : mt_invitation(o.mt_invitation),
    mt_passive(o.mt_passive),
    mt_secret(o.mt_secret),
    mt_passive_message_invert(o.mt_passive_message_invert),
    mt_reserved2(o.mt_reserved2),
    mt_reserved3(o.mt_reserved3),
    mt_reserved4(o.mt_reserved4),
    mt_reserved5(o.mt_reserved5)
{
}

Membership_type &
Membership_type::operator =(const Membership_type &o)
{
    if (this != &o)
    {
	mt_invitation = o.mt_invitation;
	mt_passive = o.mt_passive;
	mt_secret = o.mt_secret;
	mt_passive_message_invert = o.mt_passive_message_invert;
	mt_reserved2 = o.mt_reserved2;
	mt_reserved3 = o.mt_reserved3;
	mt_reserved4 = o.mt_reserved4;
	mt_reserved5 = o.mt_reserved5;
    }
    return *this;
}


Membership_type::~Membership_type()
{
}


bool 
Membership_type::invitation() const
{
    return mt_invitation;
}

bool 
Membership_type::passive() const
{
    return mt_passive;
}

bool 
Membership_type::secret() const
{
    return mt_secret;
}

bool 
Membership_type::passive_message_invert() const
{
    return mt_passive_message_invert;
}

bool 
Membership_type::reserved2() const
{
    return mt_reserved2;
}

bool 
Membership_type::reserved3() const
{
    return mt_reserved3;
}

bool 
Membership_type::reserved4() const
{
    return mt_reserved4;
}

bool 
Membership_type::reserved5() const
{
    return mt_reserved5;
}

void 
Membership_type::set_invitation(bool flg)
{
    mt_invitation = flg;
}

void 
Membership_type::set_passive(bool flg)
{
    mt_passive = flg;
}

void 
Membership_type::set_secret(bool flg)
{
    mt_secret = flg;
}

void 
Membership_type::set_passive_message_invert(bool flg)
{
    mt_passive_message_invert = flg;
}

void 
Membership_type::set_reserved2(bool flg)
{
    mt_reserved2 = flg;
}

void 
Membership_type::set_reserved3(bool flg)
{
    mt_reserved3 = flg;
}

void 
Membership_type::set_reserved4(bool flg)
{
    mt_reserved4 = flg;
}

void 
Membership_type::set_reserved5(bool flg)
{
    mt_reserved5 = flg;
}

// ================================================================
//                 Membership_internal methods


Membership::Membership_internal::Membership_internal()
    : this_pers_no(0),
      kom_errno(KOM_NO_ERROR),
      m_conf_no(0),
      m_priority(0),
//      m_last_time_read(),
      m_last_text_read(0),
      read_texts()
{
}


Membership::Membership_internal::Membership_internal(Pers_no       pers_no,
						     bool old,
						     prot_a_LyStr &str)
    : this_pers_no(pers_no),
      kom_errno(KOM_NO_ERROR),
      m_type()
{
    int   arrsize;
    int   i;

    m_last_time_read = parse_time(str);
    m_conf_no  = str.stol();
    m_priority = str.stol();
    m_last_text_read = str.stol();

    arrsize = str.parse_array_start();
    for (i = 0; i < arrsize; ++i) 
    {
	// FIXME: if this Membership is constructed due to a call such as
	// "0 46 5 0 99 0" (that is, get-membership-old with
	// want-read-texts set to 0), this loop should be avoided.
	read_texts.push_back(str.stol());
    }
    str.parse_array_end(arrsize);

    if (!old)
	m_type = Membership_type(str);
}


Membership::Membership_internal::Membership_internal(const 
						     Membership_internal &m)
    : refcount(m.refcount)
    , this_pers_no(m.this_pers_no)
    , kom_errno(m.kom_errno)
    , m_conf_no(m.m_conf_no)
    , m_priority(m.m_priority)
    , m_last_time_read(m.m_last_time_read)
    , m_last_text_read(m.m_last_text_read)
    , read_texts(m.read_texts)
{
}


const Membership::Membership_internal &
Membership::Membership_internal::operator =(const 
					  Membership::Membership_internal &m)
{
    // Note: this does not set the reference count.  A comment in
    // Membership::set explains why.
    this_pers_no = m.this_pers_no;
    kom_errno = m.kom_errno;
    m_conf_no = m.m_conf_no;
    m_priority = m.m_priority;
    m_last_time_read = m.m_last_time_read;
    m_last_text_read = m.m_last_text_read;
    read_texts = m.read_texts;

    return *this;
}

// ================================================================
//                     Membership methods


Membership::Membership()
{
    memb_int = NULL;
}


Membership::Membership(const Membership &m)
{
    if (m.memb_int != NULL)
    {
	m.memb_int->refcount++;
    }
    memb_int = m.memb_int;
}


Membership::Membership(Pers_no pno, bool old, prot_a_LyStr &prot_a_str)
{
    memb_int = new Membership_internal(pno, old, prot_a_str);
    memb_int->refcount = 1;
}


Membership::Membership(Kom_err errno)
{
    memb_int = new Membership_internal;
    memb_int->refcount = 1;
    memb_int->kom_errno = errno;
}


Membership::~Membership()
{
    if (memb_int != NULL)
    {    
	memb_int->refcount--;
	if (memb_int->refcount == 0)
	    delete memb_int;
    }
}


const Membership &
Membership::operator =(const Membership &m)
{
    if (&m != this)
    {
	if (memb_int != NULL)
	{
	    memb_int->refcount--;
	    if (memb_int->refcount == 0)
		delete memb_int;
	}

	memb_int = m.memb_int;
	if (memb_int != NULL)
	    memb_int->refcount++;
    }

    return *this;
}


// ================================================================


bool
Membership::initialized() const
{
    return (bool) (memb_int != NULL);
}


Kom_err
Membership::kom_errno() const
{
    assert(memb_int != NULL);
    return memb_int->kom_errno;
}


// ================================================================
//                            Access methods

Conf_no
Membership::conf_no() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return memb_int->m_conf_no;
}


unsigned char
Membership::priority() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return memb_int->m_priority;
}


struct tm
Membership::last_time_read() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return memb_int->m_last_time_read;
}


bool
Membership::is_read(Local_text_no lto) const
{
    assert(kom_errno() == KOM_NO_ERROR);
    if (lto <= memb_int->m_last_text_read)
	return true;
    
    std::vector<Local_text_no>::iterator end = memb_int->read_texts.end();

    return find(memb_int->read_texts.begin(), end, lto) != end;
}


Local_text_no
Membership::last_text_read() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return memb_int->m_last_text_read; 
}

long	   
Membership::num_read_after_last_text_read() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return memb_int->read_texts.size();
}

#if 0
void
Membership::mark_as_read(Local_text_no lno)
{
    FIXME - not implemented yet
}
#endif

void
Membership::set_last_text_read(Local_text_no lno)
{
    assert(kom_errno() == KOM_NO_ERROR);
    memb_int->read_texts.clear();
    memb_int->m_last_text_read = lno;
}

void
Membership::set_membership_type(const Membership_type &t)
{
    assert(kom_errno() == KOM_NO_ERROR);
    memb_int->m_type = t;
}

void
Membership::invalidate()
{
    assert(initialized());
    memb_int->kom_errno = KOM_INVALIDATED;
}

void
Membership::set(Membership from)
{
    assert(initialized());
    assert(from.initialized());
    // The assignment operator does not modify the reference count,
    // so it will remain intact.
    *memb_int = *from.memb_int;
}
