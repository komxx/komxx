// Create-on-demand map of mappings for conferences.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "kom-types.h"
#include "misc.h"
#include "map-cache.h"
#include "async_dispatcher.h"
#include "async_text_created.h"
#include "text-stat.h"
#include "conference.h"

template <class M>
Map_cache<M>::Map_cache(Connection *conn, Async_dispatcher *disp)
 : m_conn(conn),
   async_disp(disp),
   mapmap()
{
    Kom_auto_ptr<Async_callback<Async_text_created> > ptr;
    ptr.reset(async_callback_method(this, &Map_cache::new_text).release());
    cr_tag = disp->register_text_created_handler(ptr);
}


template <class M>
Map_cache<M>::~Map_cache()
{
    async_disp->unregister_text_created_handler(cr_tag);
}


template <class M>
Connection*
Map_cache<M>::connection() const
{
    return m_conn;
}

 
template <class M>
M
Map_cache<M>::get(Conf_no cno)
{
    M &r = mapmap[cno];
    if (!r.valid())
	r = M(cno, m_conn);
    return r;
}
