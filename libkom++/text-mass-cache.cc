// Fetch-on-demand cache for texts.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <map>
#include <algorithm>
#include <cassert>

#include "text-mass-cache.h"
#include "memcheck.h"
#include "get_text_q.h"
#include "kom_memory.h"

class Text_mass_cache_internal MEMCHECK {
  public:
    class Text_cache_entry MEMCHECK {
      public:
	Text_cache_entry();
	explicit Text_cache_entry(const LyStr &);
	explicit Text_cache_entry(Kom_err eno);
	Text_cache_entry(const Text_cache_entry &);
	~Text_cache_entry();
	Text_cache_entry &operator=(const Text_cache_entry&);

	Kom_err kom_errno;

	Text_no prev;		// Points towards most recently used.
	Text_no next;		// Points towards least recently used.

	LyStr str;		// Only valid if kom_errno == KOM_NO_ERROR.
    };

    typedef std::map<Text_no, Text_cache_entry> map_type;
    typedef std::map<Text_no, get_text_question*> pending_map_type;

    map_type items;
    pending_map_type pending;
    // FIXME: pending_map_type::value_type should need two more bits:
    // "bool discard;" and "bool retry;", to be used by clear() and
    // clear_errors().
};

typedef Text_mass_cache_internal::Text_cache_entry           Entry;
typedef Text_mass_cache_internal::map_type::iterator         Iter;
typedef Text_mass_cache_internal::map_type::value_type       Value;
typedef Text_mass_cache_internal::pending_map_type::iterator Pend_iter;

Text_mass_cache_internal::Text_cache_entry::Text_cache_entry()
  : kom_errno(KOM_NO_ERROR),
    prev(0),
    next(0),
    str("")
{
}

Text_mass_cache_internal::Text_cache_entry::Text_cache_entry(const LyStr &s)
  : kom_errno(KOM_NO_ERROR),
    prev(0),
    next(0),
    str(s)
{
}

Text_mass_cache_internal::Text_cache_entry::Text_cache_entry(Kom_err eno)
  : kom_errno(eno),
    prev(0),
    next(0),
    str("")
{
}

Text_mass_cache_internal::Text_cache_entry::Text_cache_entry(
    const Text_cache_entry &e)
  : kom_errno(e.kom_errno),
    prev(e.prev),
    next(e.next),
    str(e.str)
{
}

Text_mass_cache_internal::Text_cache_entry::~Text_cache_entry()
{
}

Text_mass_cache_internal::Text_cache_entry &
Text_mass_cache_internal::Text_cache_entry::operator=(
    const Text_cache_entry &e)
{
    if (this != &e)
    {
	kom_errno = e.kom_errno;
	prev= e.prev;
	next= e.next;
	str = e.str;
    }
    return *this;
}


// ================================================================ 
//                    Constructors and Destructors                  


Text_mass_cache::Text_mass_cache(Connection *c, int sz)
    : stl_containers(new Text_mass_cache_internal),
      lru(0),
      mru(0),
      conn(c),
      max_size(sz),
      prefetch_miss(0),
      prefetch_pending_cnt(0),
      prefetch_hits_ok(0),
      prefetch_hits_err(0),
      miss_ok(0),
      miss_err(0),
      block_ok(0),
      block_err(0),
      hits_ok(0),
      hits_err(0)
{
    if (max_size < 1)
	max_size = 1;

    assert_ok();
}
 

Text_mass_cache::~Text_mass_cache()
{
    assert_ok();
    delete stl_containers;
}


// ================================================================ 
//


void
Text_mass_cache::set_size(int sz)
{
    if (sz < 1)
	sz = 1;

    max_size = sz;
    limit_size();
    assert_ok();
}

void
Text_mass_cache::limit_size()
{
    assert_ok();
    while (stl_containers->items.size()
	   >= static_cast<Text_mass_cache_internal::map_type::size_type>(
	       max_size))
    {
	lru_remove();
    }
    assert_ok();
    assert(stl_containers->items.size()
	   < static_cast<Text_mass_cache_internal::map_type::size_type>(
	       max_size));
}


bool
Text_mass_cache::present(Text_no tno)
{
    return stl_containers->items.find(tno) != stl_containers->items.end();
}


void
Text_mass_cache::clear()
{
    // FIXME: there may still be pending questions that sooner or
    // later show up.

    stl_containers->items.clear();
    assert_ok();
}

// Used by clear_errors (see below)
struct has_error : public std::unary_function<Value, bool>
{
    bool operator() (const Value &v)
    {
	return v.second.kom_errno != KOM_NO_ERROR;
    }
};

void
Text_mass_cache::clear_errors()
{
    // This is not very efficient if there are many cached errors.
    // FIXME: This is O(n*n).  Should we make this in two passes
    // instead, extracting the keys to remove, and then removing them?
    // FIXME: is it possible to use remove_if?
    Iter iter;
    while ((iter = find_if(stl_containers->items.begin(),
			   stl_containers->items.end(),
			   has_error())) != stl_containers->items.end())
    {
	stl_containers->items.erase(iter);
	assert_ok();
    }
}

Connection *
Text_mass_cache::connection() const
{
    return conn;
}

Status
Text_mass_cache::prefetch(Text_no tno)
{
    assert_ok();
    if (tno <= 0)
	return st_error;	// Not counted in statistics.

    Iter iter = stl_containers->items.find(tno);

    if (iter != stl_containers->items.end())
    {
	mru_update(tno);
	if ((*iter).second.kom_errno == KOM_NO_ERROR)
	{
	    ++prefetch_hits_ok;
	    return st_ok;
	}
	else
	{
	    ++prefetch_hits_err;
	    return st_error;
	}
    }
    else
    {
	Pend_iter pending_iter = stl_containers->pending.find(tno);
	if (pending_iter == stl_containers->pending.end())
	{
	    stl_containers->pending[tno] = new get_text_question(conn, tno);
	    ++prefetch_miss;
	    return st_pending;
	}
	else
	{
	    get_text_question *q = (*pending_iter).second;
	    switch (q->status())
	    {
	    case st_pending:
		++prefetch_pending_cnt;
		return st_pending;

	    case st_ok:
		assert_ok();
		stl_containers->items[tno] = Entry(q->result());
		set_mru_initial(tno);
		stl_containers->pending.erase(pending_iter);
		delete q;
		++prefetch_hits_ok;
		return st_ok;

	    case st_error:
		stl_containers->items[tno] = Entry(q->error());
		set_mru_initial(tno);
		stl_containers->pending.erase(pending_iter);
		delete q;
		++prefetch_hits_err;
		return st_error;
	    }
	    std::abort();
	}
    }
}

bool
Text_mass_cache::prefetch_pending(Text_no tno)
{
    assert_ok();
    if (tno <= 0)
	return false;

    Pend_iter pending_iter = stl_containers->pending.find(tno);
    if (pending_iter == stl_containers->pending.end())
	return false;

    return prefetch(tno) == st_pending;
}

LyStr_errno
Text_mass_cache::get(Text_no tno)
{
    if (tno == 0)
	return KOM_TEXT_ZERO;

    assert_ok();
    long *ok_cnt = NULL;
    long *err_cnt = NULL;
    Iter iter = stl_containers->items.find(tno);

    if (iter == stl_containers->items.end()) {

	Kom_auto_ptr<get_text_question> q;

	// Are we currently waiting for this text mass?
	Pend_iter pending_iter = stl_containers->pending.find(tno);
	if (pending_iter != stl_containers->pending.end())
	{
	    // Yes.
	    Kom_auto_ptr<get_text_question> q2((*pending_iter).second);
	    q = q2;
	    if (q->status() == st_pending)
	    {
		ok_cnt = &block_ok;
		err_cnt = &block_err;
	    }
	    else
	    {
		ok_cnt = &hits_ok;
		err_cnt = &hits_err;
	    }
	    stl_containers->pending.erase(pending_iter);
	}
	else
	{
	    // No; issue a new question.
	    ok_cnt = &miss_ok;
	    err_cnt = &miss_err;
	    Kom_auto_ptr<get_text_question> p(new get_text_question(conn,tno));
	    q = p;
	}
	if (q->receive() == st_ok) 
	{
	  std::pair<Iter, bool> insert_result = stl_containers->items.insert(
		Value(tno, Entry(q->result())));
	  assert(insert_result.second);
	  set_mru_initial(tno);
	  iter = insert_result.first;
	}
	else
	{
	    std::pair<Iter, bool> insert_result = stl_containers->items.insert(
		Value(tno, Entry(q->error())));
	    assert(insert_result.second);
	    set_mru_initial(tno);
	    iter = insert_result.first;
	}
    }
    else
    {
	mru_update(tno);
	ok_cnt = &hits_ok;
	err_cnt = &hits_err;
    }
    assert(iter != stl_containers->items.end());
    LyStr_errno result;
    result.kom_errno = (*iter).second.kom_errno;
    if (result.kom_errno == KOM_NO_ERROR)
    {
	++*ok_cnt;
	result.str = (*iter).second.str;
    }
    else
    {
	++*err_cnt;
	result.str = "";
    }

    limit_size();
    return result;
}

LyStr
Text_mass_cache::stat_report() const
{
    assert_ok();
    LyStr result;
    result << "text-mass-cache status:\n"
	"\n  prefetch_miss: " << prefetch_miss
	   << "\n  prefetch_pending_cnt: " << prefetch_pending_cnt
	   << "\n  prefetch_hits_ok: " << prefetch_hits_ok
	   << "\n  prefetch_hits_err: " << prefetch_hits_err
	   << "\n  miss_ok: " << miss_ok
	   << "\n  miss_err: " << miss_err
	   << "\n  block_ok: " << block_ok
	   << "\n  block_err: " << block_err
	   << "\n  hits_ok: " << hits_ok
	   << "\n  hits_err: " << hits_err
	   << "\n";
    return result;
}
    
// ================================================================
//                    Private member functions


void
Text_mass_cache::lru_remove()
{
    assert_ok();
    assert(stl_containers->items.size() > 0);
    Iter iter = stl_containers->items.find(lru);
    assert(iter != stl_containers->items.end());
    lru = (*iter).second.prev;
    if (lru != 0)
    {
	assert(stl_containers->items.find(lru) != stl_containers->items.end());
	stl_containers->items[lru].next = 0;
    }
    else
    {
	mru = 0;
    }
    stl_containers->items.erase(iter);
    assert_ok();
}

void
Text_mass_cache::mru_update(Text_no which)
{
    assert_ok();
    assert(stl_containers->items.size() > 0);

    if (mru == which)
	return;			// Nothing needs to be done.

    assert(stl_containers->items.size() > 1);
    assert(mru != lru);
    assert(mru != 0);
    assert(lru != 0);

    Iter node = stl_containers->items.find(which);
    assert(node != stl_containers->items.end());
    Entry &entry = (*node).second;
    if (entry.next != 0)
	stl_containers->items[entry.next].prev = entry.prev;
    else
	lru = entry.prev;

    assert(entry.prev != 0);
    stl_containers->items[entry.prev].next = entry.next;

    entry.prev = 0;
    entry.next = mru;
    stl_containers->items[mru].prev = which;
    mru = which;
    assert_ok();
}

void
Text_mass_cache::set_mru_initial(Text_no which)
{
    // Note: the assertions checked by assert_ok doesn't hold at this
    // point.  The text WHICH is included in stl_containers->items,
    // but it is not present on the lru link.
    Iter node = stl_containers->items.find(which);
    assert(node != stl_containers->items.end());
    Entry &entry = (*node).second;
    assert(entry.next == 0);
    assert(entry.prev == 0);
    if (mru != 0)
    {
	assert(mru != which);
	stl_containers->items[mru].prev = which;
    }
    else
	lru = which;

    entry.next = mru;
    mru = which;
    // The assertion should hold now.
    assert_ok();
}

void
Text_mass_cache::assert_ok() const
{
    assert(stl_containers != NULL);
    if (stl_containers->items.size() != 0)
    {
	assert(lru != 0);
	assert(mru != 0);
	assert(stl_containers->items.find(lru) != stl_containers->items.end());
	assert(stl_containers->items.find(mru) != stl_containers->items.end());
	assert(stl_containers->items[lru].next == 0);
	assert(stl_containers->items[mru].prev == 0);

	if (stl_containers->items.size() == 1) 
	{
	    assert(lru == mru);
	}
	else
	{
	    Text_no l;
	    Text_no m;
	    Text_mass_cache_internal::map_type::size_type i;

	    m = mru;
	    l = stl_containers->items[m].next;
	    assert(l != m);
	    assert(l != mru);
	    for (i = 0; i < stl_containers->items.size() - 1; ++i)
	    {
		assert(m != lru);
		assert(stl_containers->items.find(l)
		       != stl_containers->items.end());
		assert(stl_containers->items[l].prev == m);
		m = l;
		l = stl_containers->items[m].next;
		assert(l != m);
		assert(l != mru);
	    }
	    assert(m == lru);
	    assert(l == 0);
	}
    }
    else
    {
	assert(lru == 0);
	assert(mru == 0);
    }
}
