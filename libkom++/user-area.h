// Access to fields in a user-area.
//
// Copyright (C) 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_USER_AREA_H
#define KOMXX_USER_AREA_H

#include "memcheck.h"
#include "person.h"
#include "error-aggregate.h"

class User_area_internal;
class Text_stat_cache;
class Text_mass_cache;

class User_area MEMCHECK {
  public:
    User_area(Person p, Text_stat_cache *, Text_mass_cache *);
    ~User_area();

    // Look up a value.  May return the error codes KOM_NO_USER_AREA or
    // KOM_KEY_NOT_FOUND as well as any error code the underlying
    // systems may return.
    Kom_res<LyStr> lookup(const LyStr &area, const LyStr &key);

    // Set a user area variable.  This only updates the local cache
    // kept by the User_area class.  To make the change permanent you
    // must call store_in_server().
    Kom_err set(const LyStr &area, const LyStr &key, const LyStr &value);

    // Create a new text and set the user-area of the person to the
    // new text.  If no values have been changed this method returns
    // KOM_NO_ERROR without doing anything.
    Kom_err store_in_server();

    // FIXME: need to design a way to fix a broken userarea.
  private:
    User_area(const User_area &); // N/A
    User_area &operator=(const User_area &); // N/A

    User_area_internal *uai;
};
    
#endif

