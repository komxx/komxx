// Dynamic session information.
//
// Copyright (C) 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "dynamic-session-info.h"

Dynamic_session_info::Dynamic_session_info()
  : session_number(0),
    person(0),
    working_conference(0),
    idle(0),
    flgs(),
    what_am_i_doing()
{
}


Dynamic_session_info::Dynamic_session_info(const Dynamic_session_info &info)
  : session_number(info.session_number),
    person(info.person),
    working_conference(info.working_conference),
    idle(info.idle),
    flgs(info.flgs),
    what_am_i_doing(info.what_am_i_doing)
{
}


Dynamic_session_info::Dynamic_session_info(prot_a_LyStr &prot_a_string)
{
    // FIXME: the Kom_err result from parse is silently ignored, so any
    // parse errors are ignored.
    parse(prot_a_string);
}


Dynamic_session_info::~Dynamic_session_info()
{
}


const Dynamic_session_info &
Dynamic_session_info::operator =(const Dynamic_session_info &info)
{
    if (&info != this)
    {
	session_number = info.session_number;
	person = info.person;
	working_conference = info.working_conference;
	idle = info.idle;
	flgs = info.flgs;
	what_am_i_doing = info.what_am_i_doing;
    }

    return *this;
}


Kom_err
Dynamic_session_info::parse(prot_a_LyStr &prot_a_string)
{
    session_number = prot_a_string.stol();
    person = prot_a_string.stol();
    working_conference = prot_a_string.stol();
    idle = prot_a_string.stol();
    flgs = parse_session_flags(prot_a_string);
    LyStr_errno tmp = prot_a_string.stos();
    what_am_i_doing = tmp.str;
    return tmp.kom_errno;
}


Pers_no      
Dynamic_session_info::pers_no() const
{
    return person;
}


Conf_no      
Dynamic_session_info::working_conf() const
{
    return working_conference;
}


long
Dynamic_session_info::idle_time() const
{
    return idle;
}


Session_no   
Dynamic_session_info::session() const
{
    return session_number;
}


Session_flags
Dynamic_session_info::flags() const
{
    return flgs;
}


LyStr
Dynamic_session_info::doing() const
{
    return what_am_i_doing;
}
