// Read texts in comment-order.
//
// Copyright (C) 1994-1995  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_READ_ORDER_H
#define KOMXX_READ_ORDER_H

#include <iterator>
#include <set>
#ifdef NEED_STRUCT_ITERATOR
#  include "struct-iterator.h"
#endif

#include "memcheck.h"
#include "misc.h"
#include "kom-types.h"
#include "text-mapping.h"
#include "membership.h"
#include "local-text-no-set.h"

#include "session.h"

class Conferences;
class Persons;
class Texts;
class get_membership_question;
class Read_order;

// An enum Prompt is returned by Read_order::prompt() and
// Unread_confs::prompt().  Read_order::prompt() never returns NEXT_CONF.
// WARNING: These values are used in e.g. www/sc.py, so don't change them.
enum Prompt {
    NEXT_CONF=1,	// Never returned by Read_order::prompt().
    NEXT_TEXT=2,
    NEXT_COMMENT=3, 
    NEXT_FOOTNOTE=4,
    NO_MORE_TEXT=5,
    TIMEOUT_PROMPT=6,		// It took too long time to find out
				// the next prompt.
    PROMPT_ERROR=7		// Can not find out the next prompt;
				// the connection might be lost.
};

class Comment_stack : private std::vector<Text_no> {
  public:
    void remove(Text_no);
    Text_no pop() { Text_no ret(back()); pop_back(); return ret; }
    void push(Text_no t) { push_back(t); }
    using std::vector<Text_no>::iterator;
    using std::vector<Text_no>::begin;
    using std::vector<Text_no>::end;
    using std::vector<Text_no>::empty;
    using std::vector<Text_no>::front;
    using std::vector<Text_no>::clear;
};

// The Work_quota class is used in each prefetch request.
// Its purpose is to do all the book-keeping required to
// determine when enough prefetching has been done.  The
// user creates a new Work_quota object and sends it to
// the prefetch() method (of Read_order or Unread_confs).
// The state of the Work_quota will be updated as the
// prefetching operation continues.
class Work_quota MEMCHECK {
  public:
    Work_quota(
	// Fetch everything there is to know about the first
	// REQUIRED_TEXTS texts in the current reading order.
	int required_texts,
	// Fetch everything there is to know about the first
	// REQUIRED_TEXTS_IN_CONF texts in each conference, but
	// only in the first REQUIRED_CONFS conferences that have
	// anything unread.
	int required_texts_in_conf,
	int required_confs,
	// Fetch information so that the global text number of the
	// first REQUIRED_MAPS texts are known.  Note: this may fetch
	// the numbers in linear order instead of in comment-order.
	int required_maps,
	// Figure out the global text number of the first
	// REQUIRED_MAPS_IN_CONF texts in each of the first
	// REQUIRED_MAPS_CONF conferences that contain anyhting
	// unread.
	int required_maps_in_conf,
	int required_maps_conf,

	// The prefetch algorithm stops when all of the above
	// conditions are met, or when there is MAX_QUESTIONS
	// outstanding questions, whichever occurs first.
	int max_questions);

    Work_quota(int mapped_texts,
	       int known_texts,
	       int max_questions);

    Work_quota();
    ~Work_quota();

    // One more question is issued.
    void bump_question(int q =1);

    // Returns true if all required conditions are met, or if there
    // are no more questions.
    bool done() const;

    // Returns true if required conditions are met in this conference,
    // or if we've run out of questions.
    bool conf_done() const;

    // Returns true if enough global text numbers are known in this
    // conference.
    bool conf_mapped() const;

    // Everything is known about yet another text.  This kinda implies
    // that the global text number is known, but bump_known_mappings()
    // may also be called for this text.
    void bump_known_texts(int texts =1);

    // The global text number of this many more texts are known.
    void bump_known_mappings(int texts =1);

    // Signal that the prefetching is now done in a new conference.
    void change_conf();

    // Ask how many more questions we may issue.
    int questions_left() const;
  private:
    int text_quota;
    int texts_in_conf_quota;
    int texts_in_this_conf_quota;
    int confs_quota;

    int map_quota;
    int maps_in_conf_quota;
    int maps_in_this_conf_quota;
    int map_confs_quota;

    int question_quota;
};

struct Read_item {
    Read_item(Text_no t, int n) : text_no(t), nesting_level(n) { }
    Read_item() : text_no(0), nesting_level(0) { }

    Text_no text_no;
    int nesting_level;
};

class Extended_comment_stack : private std::vector<Read_item> {
  public:
    void remove(Text_no);
    Read_item pop() { Read_item ret(back()); pop_back(); return ret; }
    void push(const Read_item &r) { push_back(r); }
    using std::vector<Read_item>::empty;
};

class Read_order_iterator
  : public std::iterator<std::forward_iterator_tag, Read_item, Text_no,
                    Read_item*, Read_item&>
// FIXME: is it proper to have Text_no as the difference_type?
{
public:
    Read_order_iterator();
    ~Read_order_iterator();
    Read_order_iterator(const Read_order_iterator &);
    bool operator==(const Read_order_iterator&) const;
    bool operator!=(const Read_order_iterator&) const;
    Read_order_iterator &operator=(const Read_order_iterator&);
    Read_item operator*();
    Read_item *operator->();
    Read_order_iterator &operator++();
    Read_order_iterator operator++(int);
public:
    // Only intended to be called by the iterator-creating
    // methods of class Read_order.
    Read_order_iterator(Read_order *ro, int wanted_texts);
private:
    std::set<Text_no> unread_texts;
    Extended_comment_stack pending_footnotes;
    Extended_comment_stack pending_comments;
    Local_text_no_set removed;
    std::set<Text_no>::size_type texts_left; // Including the text in current.
    Read_item current;	// text_no is 0 if past-the-end or under construction.
    Read_order *ro;
};
    
// 
// This object determines what order the texts should be read
// within a conference.  It is only used internally by class
// Unread_confs.
//

class Read_order MEMCHECK {
  public:
    Read_order(Session *sess, Conf_no cno, Pers_no reader);
    ~Read_order();

    // Prefetch everything that is normally needed to display the
    // TEXTS first texts. "What is normally needed" is defined as
    // "what is needed by nilkom".
    Status    prefetch(Work_quota *work);

    Prompt    next_prompt();
    Text_no   next_text(bool remove=false);

    // Enqueue all children of the specified text in so that they will
    // be read next.  Note: this is done automatically by next_text
    // when it is called with remove=true.
    void      enqueue_children(Text);
    void      enqueue_children(Text_no);

    void      remove_text(Text_no, Local_text_no);
    bool      empty();
    long      min_num_unread();
    long      max_num_unread();
    long      estimated_num_unread();
    Conf_no   confno() const;

    // Set the number of unread texts in so that first_unread is the
    // first unread texts.  This uses Session::set_last_read to
    // actually update the number of unread texts the user has if (and
    // only if) the session is logged in as the reader.  The
    // user must already be a member of the conference.
    Kom_err set_last_read(Local_text_no last_text_read);

    Read_order_iterator begin(int wanted_texts =100);
    Read_order_iterator end();

    // Retrieve state so that the exact same read-order configuration
    // can later be restored.  The first text to show is returned as
    // the lowest index.
    std::vector<Text_no> query_pending_footnotes() const;
    std::vector<Text_no> query_pending_comments() const;

    // Restore state saved with query_pending_footnotes or
    // query_pending_comments.  Any texts in the arguments that have
    // already been read are silently ignored.
    void set_pending_footnotes(const std::vector<Text_no> &);
    void set_pending_comments(const std::vector<Text_no> &);

    friend class Read_order_iterator;
  private:
    Read_order(const Read_order &); // N/A
    const Read_order & operator = (const Read_order &);	// N/A

    // Know about this many texts. This only prefetches the map.
    // Returns true if something was received.
    bool prefetch_map(Work_quota *work);

    void prefetch_recurse(const Text_no &node,
			  std::set<Text_no> *stoplist,
			  Work_quota *work);

    Status need_one_mapped();

    // Ask for the membership if it isn't already done.
    // Receive the membership if it is provided.
    // If BLOCK is true, this method will not return until the
    // membership has been retrieved.
    Status	   want_membership(bool block);

    void set_pending(const std::vector<Text_no> &arr, 
		     Comment_stack *const stk);

    Session      * session;
    Persons	 & persons;
    Conferences  & conferences;
    Texts	 & texts;
    Text_mapping   map;
    Conf_no        conf_no;
    Pers_no        pers_no;

    Membership     membership;

    Local_text_no  first_unchecked;
    // Note: removed must be an ordered set type.
    Local_text_no_set removed;

    // unread_texts contains all texts before first_unchecked, even
    // those that are also present in pending_footnotes and/or
    // pending_comments.  It can also contain any number of texts
    // beyond first_unchecked which have been prefetched because of
    // the comment-order-driven prefetching.
    std::set<Text_no>     unread_texts;
    Comment_stack    pending_footnotes;
    Comment_stack    pending_comments;
};

#endif
