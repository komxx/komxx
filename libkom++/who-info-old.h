// Old-style who-info object.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_WHO_INFO_OLD_H
#define KOMXX_WHO_INFO_OLD_H

#include "kom-types.h"
#include "LyStr.h"

class Who_info_old {
  public:
    Who_info_old();
    Who_info_old(const Who_info_old&);
    const Who_info_old &operator =(const Who_info_old &);
  private:
    Pers_no		person;
    LyStr		what_am_i_doing;
    Conf_no		working_conference;
};
#endif
