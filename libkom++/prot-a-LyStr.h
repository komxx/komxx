// String class with knowledge on protocol A.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_PROT_A_LYSTR_H
#define KOMXX_PROT_A_LYSTR_H

#include <vector>

#include "LyStr.h"
#include "LyStr-errno.h"
#include "kom-types.h"

class Aux_item;
class Member;
class Membership_type;

class prot_a_LyStr : public LyStr {
  public:
    prot_a_LyStr();
    prot_a_LyStr(const char *c_str);
    prot_a_LyStr(void *block, int len);
    prot_a_LyStr(const prot_a_LyStr &);
    prot_a_LyStr(const LyStr &);

    long  stol();		// Extract a number, ignoring leading spaces

    // Extract a hollerith string.  Sets errno to KOM_BAD_HOLLERITH
    // if the beginning of the string doesn't look like a hollerith
    // string or if the hollerith string length is longer than the
    // supplied string.  If errno is set to KOM_BAD_HOLLERITH an
    // indeterminate amount of the string may have been consumed --
    // don't trust it.
    LyStr_errno stos();

    void append_hollerith(const LyStr &str); // Append as hollerith

    void append_aux_item(const Aux_item &aux);
    void append_aux_list(const std::vector<Aux_item> &aux_items);
    void append_bool(bool);
    void append_aux_no_list(const std::vector<Aux_no> &aux_nos);
    void append_membership_type(const Membership_type &type);

    // Attempt to parse one bit of a bit-field.
    // If the first character in the string is '0' or '1', it is
    // removed, the flag is set to the corresponding value, and this
    // method returns true.  Otherwise this method returns false
    // without consuming anything from the string.
    bool parse_bit(bool &flag);

    // Consume all leading '0' and '1' digits.
    void skip_bits();

    int    parse_array_start();
    void   parse_array_end(int arrsize);
    void   append_array_start(int arrsize);
    void   append_array_end();

    void   parse_aux_list(std::vector<Aux_item> &result);

    template <class T>
    void
    parse_array(std::vector<T> &result);
};

void parse_kom_type(prot_a_LyStr &str, uint32_t &res);
void parse_kom_type(prot_a_LyStr &str, unsigned short &res);
void parse_kom_type(prot_a_LyStr &str, Member &res);

template <class T>
void
prot_a_LyStr::parse_array(std::vector<T> &result)
{
    int   arrsize;
    int   i;

    arrsize = parse_array_start();
    result.resize(arrsize);

    for (i = 0; i < arrsize; ++i)
	parse_kom_type(*this, result[i]);
    parse_array_end(arrsize);
}

#endif
