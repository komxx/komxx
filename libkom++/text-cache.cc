// Fetch-on-demand cache for texts.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>
#include <cassert>

#include "error-codes.h"
#include "text-cache.h"
#include "question.h"
#include "misc.h"
#include "async_text_created.h"
#include "uplink.h"
#include "get_text_stat_q.h"

static Text_stat bad_text_stat;
static Text_stat zero_text_stat(KOM_TEXT_ZERO);

Text_stat_cache::Text_stat_cache(Connection *c,
				 Async_dispatcher *disp)
    : conn(c),
      async_disp(disp)
{
    Kom_auto_ptr<Async_callback<Async_text_created> > ptr;
    ptr.reset(async_callback_method(this, &Text_stat_cache::new_text).release());
    cr_tag = disp->register_text_created_handler(ptr);
}

Text_stat_cache::~Text_stat_cache()
{
    async_disp->unregister_text_created_handler(cr_tag);
}

bool
Text_stat_cache::present(Text_no cno) const
{
    return statusmap.find(cno) != statusmap.end();
}

Connection *
Text_stat_cache::connection() const
{
    return conn;
}


Status
Text_stat_cache::prefetch(Text_no tno)
{
    if (tno <= 0)
	return st_error;

    Tstmap::iterator stat_iter = statusmap.find(tno);
    if (stat_iter != statusmap.end())
    {
	if ((*stat_iter).second.kom_errno() == KOM_NO_ERROR)
	    return st_ok;
	else
	    return st_error;
    }
    else
    {
	Tstreqmap::iterator req_iter = pendingmap.find(tno);
	if (req_iter == pendingmap.end())
	{
	    pendingmap[tno] = new get_text_stat_question(conn, tno);
	    return st_pending;
	}
	else
	{
	    switch ((*req_iter).second->status())
	    {
	    case st_pending:
		return st_pending;
	    case st_ok:
		statusmap[tno] = (*req_iter).second->result();
		delete (*req_iter).second;
		pendingmap.erase(req_iter);
		return st_ok;
	    case st_error:
		statusmap[tno] = Text_stat((*req_iter).second->error());
		delete (*req_iter).second;
		pendingmap.erase(req_iter);
		return st_error;
	    }
	}
    }
    std::abort();
}

bool
Text_stat_cache::prefetch_pending(Text_no tno)
{
    if (tno <= 0)
	return false;

    if (pendingmap.find(tno) == pendingmap.end())
	return false;

    return prefetch(tno) == st_pending;
}


Text_stat
Text_stat_cache::get(Text_no tno)
{
    if (tno <= 0)
	return zero_text_stat;

    Tstmap::iterator stat_iter = statusmap.find(tno);
    if (stat_iter != statusmap.end())
	return (*stat_iter).second;
    else
    {
	Tstreqmap::iterator req_iter = pendingmap.find(tno);
	if (req_iter != pendingmap.end())
	{
	    Text_stat res;
	    get_text_stat_question *gtr = (*req_iter).second;

	    if (gtr->receive() == st_ok)
		res = gtr->result();
	    else
		res = Text_stat(gtr->error());
	    statusmap[tno] = res;
	    delete gtr;
	    pendingmap.erase(tno);
	    return res;
	}
	else		
	{
	    // Not present in cache, and no outstanding question.
	    Text_stat res;
	    get_text_stat_question gt(conn, tno);
	    if (gt.receive() == st_ok)
		res = gt.result();
	    else
	    {
		res = Text_stat(gt.error());
	    }
	    statusmap[tno] = res;
	    return res;
	}
    }
    std::abort();
}

void
Text_stat_cache::new_text(const Async_text_created &msg)
{
    Text_stat t = msg.text_stat();
    statusmap[t.text_no()] = t;

    // New texts cannot have comments/footnotes.
    assert(t.num_comment_in() == 0);
    assert(t.num_footnote_in() == 0);

    // Update all texts that this one refers to.
    for (int ix = 0; ix < t.num_uplinks(); ix++)
    {
	Text_no parent = t.uplink(ix)->parent();
	Tstmap::iterator parent_iter = statusmap.find(parent);
	if (parent_iter != statusmap.end())
	{
	    Text_stat ts = (*parent_iter).second;
	    if (t.uplink(ix)->is_footnote())
		// FIXME: race condition: we may have fetched the text
		// status before this message is processed, so this
		// may add a duplicate link.
		ts.add_footnote_in(t.text_no());
	    else
		// FIXME: race condition: we may have fetched the text
		// status before this message is processed, so this
		// may add a duplicate link.
		ts.add_comment_in(t.text_no());
	}
    }
}
