// Information about a server.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "server-info.h"
#include "aux-item.h"

// ================================================================ 
//                    Constructors and Destructors                  


Server_info_old::Server_info_old()
  : si_version(0), 
    si_conf_pres_conf(0), 
    si_pers_pres_conf(0), 
    si_motd_conf(0),
    si_kom_news_conf(0), 
    si_motd_of_lyskom(0)
{
}
 
Server_info_old::Server_info_old(const Server_info_old &si)
  : si_version(si.si_version),
    si_conf_pres_conf(si.si_conf_pres_conf),
    si_pers_pres_conf(si.si_pers_pres_conf),
    si_motd_conf(si.si_motd_conf),
    si_kom_news_conf(si.si_kom_news_conf),
    si_motd_of_lyskom(si.si_motd_of_lyskom)
{
}


Server_info_old::Server_info_old(prot_a_LyStr &str)
    : si_version(str.stol()),  
      si_conf_pres_conf(str.stol()),
      si_pers_pres_conf(str.stol()),
      si_motd_conf(str.stol()),
      si_kom_news_conf(str.stol()), 
      si_motd_of_lyskom(str.stol())
{    
}


Server_info_old::~Server_info_old()
{
}


// ================================================================ 
//

const Server_info_old & 
Server_info_old::operator = (const Server_info_old &si)
{
    if (&si != this)
    {
	si_version= si.si_version;
	si_conf_pres_conf = si.si_conf_pres_conf;
	si_pers_pres_conf = si.si_pers_pres_conf;
	si_motd_conf = si.si_motd_conf;
	si_kom_news_conf = si.si_kom_news_conf;
	si_motd_of_lyskom = si.si_motd_of_lyskom;
    }

    return *this;
}


long
Server_info_old::version() const
{
    return si_version;
}

Conf_no
Server_info_old::conf_pres_conf() const
{
    return si_conf_pres_conf;
}

Conf_no
Server_info_old::pers_pres_conf() const
{
    return si_pers_pres_conf;
}

Conf_no
Server_info_old::motd_conf() const
{
    return si_motd_conf;
}

Conf_no
Server_info_old::kom_news_conf() const
{
    return si_kom_news_conf;
}

Text_no
Server_info_old::motd_of_lyskom() const
{
    return si_motd_of_lyskom;
}

// ================================================================ 
//

Server_info::Server_info()
  : Server_info_old(),
    si_aux()
{
}

Server_info::Server_info(const Server_info &si)
  : Server_info_old(si),
    si_aux(si.si_aux)
{
}


Server_info::Server_info(prot_a_LyStr &str)
  : Server_info_old(str),
    si_aux()
{
    str.parse_aux_list(si_aux);
}


Server_info::~Server_info()
{
}


const Server_info &
Server_info::operator =(const Server_info &si)
{
    if (&si != this)
    {
	Server_info_old::operator=(si);
	si_aux = si.si_aux;
    }

    return *this;
}


int
Server_info::num_aux() const
{
    return si_aux.size();
}


Aux_item
Server_info::aux(int nr) const
{
    return si_aux[nr];
}
