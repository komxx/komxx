// Create-on-demand map of mappings for conferences.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_MAPS_H
#define KOMXX_MAPS_H

#include "kom-types.h"
#include "text-mapping.h"
#include "map-cache.h"

class Async_dispatcher;
class Connection;

template <class M>
class Maps {
  public:
    Maps(Connection *conn, Async_dispatcher *async_disp);
    ~Maps();

    Connection *connection() const;
    
    M operator[](Conf_no) const;

    Map_cache<M> * read_map_cache() const;

  private:
    Map_cache<M> *cache;

    Maps & operator = (const Maps &); // N/A
    Maps(const Maps &);		      // N/A
};
#endif
