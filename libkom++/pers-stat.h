// Person status objects.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_PERS_STAT_H
#define KOMXX_PERS_STAT_H

#include <ctime>

#include "error-codes.h"
#include "priv-bits.h"
#include "personal-flags.h"
#include "prot-a-LyStr.h"
#include "kom-types.h"
#include "memcheck.h"

/*  Struct for persons  */
class Pers_stat MEMCHECK {
  public:    
    Pers_stat();
    Pers_stat(const Pers_stat&);
    Pers_stat(Pers_no pno, prot_a_LyStr &prot_a_string);
    Pers_stat(Kom_err kom_errno);
    ~Pers_stat();
    Pers_stat &operator =(const Pers_stat &);

    Kom_err 	    kom_errno()		  const;

    Text_no	    user_area() 	  const;
    Priv_bits	    privileges() 	  const;
    Personal_flags  flags() 		  const;
    struct std::tm  last_login() 	  const;
    unsigned long   total_time_present()  const;
    unsigned long   sessions() 		  const;
    unsigned long   created_lines() 	  const;
    unsigned long   created_bytes() 	  const;
    unsigned long   read_texts() 	  const;
    unsigned long   no_of_text_fetches()  const;
    unsigned short  created_persons() 	  const;
    unsigned short  created_confs() 	  const;
    LyStr	    username() 		  const;
    Local_text_no   first_created_text()  const;
    unsigned long   no_of_created_texts() const;
    unsigned short  no_of_marks() 	  const;
    unsigned short  no_of_confs() 	  const;
    Pers_no	    pers_no()		  const;

  private:
    struct Pers_stat_internal {
      public:
	Pers_stat_internal();
	Pers_stat_internal(Pers_no pers_no, prot_a_LyStr &prot_a_str);
	Pers_stat_internal(const Pers_stat_internal &);
	const Pers_stat_internal &operator =(const Pers_stat_internal &);

	Pers_no		ps_pers_no;
	int     	refcount;
	Kom_err		kom_errno;
	Text_no	    	ps_user_area;
	Priv_bits	ps_privileges;
	Personal_flags  ps_flags;
	struct std::tm	ps_last_login;
	unsigned long   ps_total_time_present;
	unsigned long   ps_sessions;	   
	unsigned long   ps_created_lines;	
	unsigned long   ps_created_bytes;	
	unsigned long   ps_read_texts;	   
	unsigned long   ps_no_of_text_fetches;
	unsigned short  ps_created_persons;   
	unsigned short  ps_created_confs;	
	LyStr	    	ps_username;		   
	Local_text_no   ps_first_created_text;	   
	unsigned long   ps_no_of_created_texts;
	unsigned short  ps_no_of_marks;
	unsigned short  ps_no_of_confs;
    };

    bool valid() const;

    Pers_stat_internal* psi;
};
#endif
