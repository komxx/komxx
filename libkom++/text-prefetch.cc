// Text prefetching.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "text-prefetch.h"
#include "session.h"
#include "uplink.h"

int
prefetch_text_for_display(Text text, int max_questions, Session *sess)
{
    // Note: if this function becomes a "hot spot" it can be made more
    // efficient by introducing a "bool prefetch_for_display_done;"
    // flag which is set when this function return 0 (that is, when
    // everything is prefetched).  That bit could then be tested here:
    // if (prefetch_for_display_done) return 0;
    int q = 0;

    if (q >= max_questions)
	return q;

    // Step 1: Prefetch the text itself.
    switch (text.prefetch())
    {
    case st_ok:
	break;
    case st_error:
	return 0;
    case st_pending:
	return 1;
    }
    if (q >= max_questions)
	return q;

    // Prefetch the author's name
    if (sess->conferences()[text.author()].prefetch() == st_pending)
	q++;
    if (q >= max_questions)
	return q;

    // Prefetch the conferences to which this text is a recipient
    for (int rcpt = 0; rcpt < text.num_recipients(); rcpt++)
    {
	Conference c = sess->conferences()[text.recipient(rcpt)->recipient()];
	if (c.prefetch() == st_pending)
	    q++;
	if (q >= max_questions)
	    return q;
    }

    // Prefetch the text-stats for the texts to which this text 
    // is a comment or a footnote
    for (int upl = 0; upl < text.num_uplinks(); upl++)
    {
	Text t = sess->texts()[text.uplink(upl)->parent()];
	switch (t.prefetch())
	{
	case st_ok:
	    if (sess->conferences()[t.author()].prefetch() == st_pending)
		q++;
	    break;
	case st_error:
	    break;
	case st_pending:
	    q++;
	}
	if (q >= max_questions)
	    return q;
    }

    // Prefetch the text-stats for the texts which are footnotes to this text
    for (int footn_in = 0; footn_in < text.num_footnote_in(); footn_in++)
    {
	Text t = sess->texts()[text.footnote_in(footn_in)];
	switch (t.prefetch())
	{
	case st_ok:
	    if (sess->conferences()[t.author()].prefetch() == st_pending)
		q++;
	    break;
	case st_error:
	    break;
	case st_pending:
	    q++;
	}
	if (q >= max_questions)
	    return q;
    }

    // Prefetch the text-stats for the texts which are comments to this text
    for (int comm_in = 0; comm_in < text.num_comment_in(); comm_in++)
    {
	Text t = sess->texts()[text.comment_in(comm_in)];
	switch (t.prefetch())
	{
	case st_ok:
	    if (sess->conferences()[t.author()].prefetch() == st_pending)
		q++;
	    break;
	case st_error:
	    break;
	case st_pending:
	    q++;
	}
	if (q >= max_questions)
	    return q;
    }

    if (q >= max_questions)
	return q;

    if (text.prefetch_text() == st_pending)
	++q;

    return q;
}
