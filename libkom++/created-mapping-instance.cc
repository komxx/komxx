#include "map_created_texts_q.h"
#include "text-mapping.cc"
#include "map-cache.cc"
#include "maps.cc"
template class Text_mapping_basic<map_created_texts_question>;
template class Maps<Created_mapping>;
template class Map_cache<Created_mapping>;

template <>
void
Map_cache<Created_mapping>::new_text(const Async_text_created &msg)
{
    Text_stat t = msg.text_stat();
    if (t.author() != 0)
    {
	Created_mapping map = get(t.author());
	map.add_unknown();
    }
}
