// Text status objects.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>
#include <cassert>
#include <vector>
#include <algorithm>

#include "error-codes.h"
#include "recipient.h"
#include "kom-types.h"
#include "misc.h"
#include "prot-a-LyStr.h"
#include "text-stat.h"
#include "uplink.h"
#include "container.h"
#include "aux-item.h"
#include "time-utils.h"

// ================================================================
//                    Text_stat_internal methods

struct Text_stat::Text_stat_internal {
  public:
    Text_stat_internal();
    Text_stat_internal(Text_no text_no, bool old, prot_a_LyStr &prot_a_str);
    Text_stat_internal(const Text_stat_internal &);
    ~Text_stat_internal();
    const Text_stat_internal &operator =(const Text_stat_internal &);

    int refcount;
    Kom_err kom_errno;

    Text_no               number;
    struct std::tm	  ctime;
    Pers_no		  auth;	
    unsigned short	  no_of_lines;
    unsigned long  	  no_of_chars;
    unsigned short	  no_of_marks;	
    std::vector<Recipient*> recipients; // ...and cc_recipients
    std::vector<Text_no>  comments_in;
    std::vector<Text_no>  footnotes_in;
    std::vector<Uplink*>  uplinks; // footn_to, comm_to
    std::vector<Aux_item> aux;
};

Text_stat::Text_stat_internal::Text_stat_internal()
    :kom_errno(KOM_NO_ERROR),
     number(0), auth(0), no_of_lines(0), no_of_chars(0), no_of_marks(0),
     recipients(), comments_in(), footnotes_in(), uplinks(), aux()
{
}

Text_stat::Text_stat_internal::Text_stat_internal(Text_no tno,
						  bool old,
						  prot_a_LyStr &prot_a_str)
    : kom_errno(KOM_NO_ERROR),
      number(tno),
      recipients(),
      comments_in(), 
      footnotes_in(), 
      uplinks(),
      aux()
{
    ctime = parse_time(prot_a_str);
    auth = prot_a_str.stol();
    no_of_lines = prot_a_str.stol();
    no_of_chars = prot_a_str.stol();
    no_of_marks = prot_a_str.stol();
    
    int miscs = prot_a_str.stol();
    prot_a_str.skipspace();
    if (miscs == 0)
    {
	assert(prot_a_str[0] == '*');
	prot_a_str.remove_first(1);
    }
    else
    {
	assert(prot_a_str[0] == '{');
	prot_a_str.remove_first(1);

	while (miscs > 0)
	{
	    int misctype = prot_a_str.stol();
	    switch(misctype)
	    {
	    case 0:		// recpt
	    case 1:		// cc_recpt
		{
		    Recipient *r = new Recipient;
		    miscs -= r->parse(misctype == 1 ? true : false,
				      prot_a_str);
		    recipients.push_back(r);
		}
		break;
		
	    case 2:		// comm_to
	    case 4:		// footn_to
		{
		    Uplink *u = new Uplink;
		    miscs -= u->parse(misctype == 4 ? true : false,
				      prot_a_str);
		    uplinks.push_back(u);
		}
		break;
		
	    case 3:		// comm_in
		comments_in.push_back(prot_a_str.stol());
		miscs--;
		break;

	    case 5:		// footn_in
		footnotes_in.push_back(prot_a_str.stol());
		miscs--;
		break;
		
	    default:
		std::abort();
	    }
	}

	prot_a_str.skipspace();
	assert(prot_a_str[0] == '}');
	prot_a_str.remove_first(1);
    }

    if (!old)
	prot_a_str.parse_aux_list(aux);
}

Text_stat::Text_stat_internal::~Text_stat_internal()
{
    delete_contents(recipients);
    delete_contents(uplinks);
}

// ================================================================
//                         Text_stat methods

Text_stat::Text_stat()
{
    tsi = NULL;
}


Text_stat::Text_stat(const Text_stat &text_stat)
{
    if (text_stat.tsi != NULL)
    {
	text_stat.tsi->refcount++;
    }
    tsi = text_stat.tsi;
}


Text_stat::Text_stat(Text_no text_no, bool old, prot_a_LyStr &prot_a_str)
{
    tsi = new Text_stat_internal(text_no, old, prot_a_str);
    tsi->refcount = 1;
}

Text_stat::Text_stat(Kom_err errno)
{
    tsi = new Text_stat_internal;
    tsi->kom_errno = errno;
    tsi->refcount = 1;
}


Text_stat::~Text_stat()
{
    if (tsi != NULL) 
    {
	tsi->refcount--;
	if (tsi->refcount == 0) 
	{
	    delete tsi;
	}
    }
}


Text_stat &
Text_stat::operator=(const Text_stat &text_stat)
{
    if (&text_stat != this) 
    {
	// Delete the old Text_stat_internal if this was the last reference.
	if (tsi != NULL)
	{
	    tsi->refcount--;
	    if (tsi->refcount == 0) 
	    {
		delete tsi;
	    }
	}
	
	tsi = text_stat.tsi;
	if (tsi != NULL)
	    tsi->refcount++;
    }

    return *this;
}


bool
Text_stat::valid() const
{
    return tsi != NULL && tsi->kom_errno == 0;
}

Kom_err
Text_stat::kom_errno() const
{
    if (tsi == NULL)
	return KOM_UNINITIALIZED;
    else
	return tsi->kom_errno;
}

Text_no
Text_stat::text_no() const
{
    assert(valid());
    return tsi->number;
}

struct std::tm
Text_stat::creation_time() const
{
    assert(valid());
    return tsi->ctime;
}

Pers_no
Text_stat::author() const
{
    assert(valid());
    return tsi->auth;
}

long
Text_stat::num_lines() const
{
    assert(valid());
    return tsi->no_of_lines;
}

long
Text_stat::num_chars() const
{
    assert(valid());
    return tsi->no_of_chars;
}

int
Text_stat::num_marks() const
{
    assert(valid());
    return tsi->no_of_marks;
}


int
Text_stat::num_recipients() const
{
    assert(valid());
    return tsi->recipients.size();
}

const Recipient *
Text_stat::recipient(int n) const
{
    assert(valid());
    return tsi->recipients[n];
}

int
Text_stat::num_comment_in() const
{
    assert(valid());
    return tsi->comments_in.size();
}

Text_no
Text_stat::comment_in(int n) const
{
    assert(valid());
    return tsi->comments_in[n];
}

int
Text_stat::num_footnote_in() const
{
    assert(valid());
    return tsi->footnotes_in.size();
}

Text_no
Text_stat::footnote_in(int n) const
{
    assert(valid());
    return tsi->footnotes_in[n];
}

int
Text_stat::num_uplinks() const
{
    assert(valid());
    return tsi->uplinks.size();
}

const Uplink *
Text_stat::uplink(int n) const
{
    assert(valid());
    return tsi->uplinks[n];
}

int
Text_stat::num_aux() const
{
    assert(valid());
    return tsi->aux.size();
}

Aux_item
Text_stat::aux(int n) const
{
    assert(valid());
    return tsi->aux[n];
}

void
Text_stat::add_footnote_in(Text_no t)
{
    assert(valid());
    tsi->footnotes_in.push_back(t);
}

void
Text_stat::add_comment_in(Text_no t)
{
    assert(valid());
    tsi->comments_in.push_back(t);
}

struct tm
Text_stat::entry_time(Conf_no cno, Local_text_no lno)
{
    assert(valid());
    for (int i = 0; i < num_recipients(); ++i)
    {
	const Recipient *r = recipient(i);
	if (r->recipient() == cno && r->local_no() == lno && r->has_sent_at())
	    return r->sent_at();
    }

    return creation_time();
}
