// Static session information.
//
// Copyright (C) 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "static-session-info.h"
#include "time-utils.h"

Static_session_info::Static_session_info()
  : uname(),
    hname(),
    idusr()
    // connect_time receives garbage
{
}


Static_session_info::Static_session_info(const Static_session_info &info)
  : uname(info.uname),
    hname(info.hname),
    idusr(info.idusr),
    connect_tm(info.connect_tm)
{
}


Static_session_info::Static_session_info(prot_a_LyStr &prot_a_string)
{
    // FIXME: the Kom_err result from parse is silently ignored, so any
    // parse errors are ignored.
    parse(prot_a_string);
}


Static_session_info::~Static_session_info()
{
}


const Static_session_info &
Static_session_info::operator =(const Static_session_info &info)
{
    if (&info != this)
    {
	uname = info.uname;
	hname = info.hname;
	idusr = info.idusr;
	connect_tm = info.connect_tm;
    }

    return *this;
}


Kom_err
Static_session_info::parse(prot_a_LyStr &prot_a_string)
{
    Kom_err ret = KOM_NO_ERROR;
    LyStr_errno tmp = prot_a_string.stos();
    if (tmp.kom_errno != KOM_NO_ERROR)
	ret = tmp.kom_errno;
    uname = tmp.str;

    tmp = prot_a_string.stos();
    if (tmp.kom_errno != KOM_NO_ERROR)
	ret = tmp.kom_errno;
    hname = tmp.str;

    tmp = prot_a_string.stos();
    if (tmp.kom_errno != KOM_NO_ERROR)
	ret = tmp.kom_errno;
    idusr = tmp.str;

    connect_tm = parse_time(prot_a_string);
    return ret;
}


LyStr  
Static_session_info::user_name() const
{
    return uname;
}

LyStr  
Static_session_info::hostname() const
{
    return hname;
}

LyStr  
Static_session_info::ident_user() const
{
    return idusr;
}

struct std::tm
Static_session_info::connection_time() const
{
    return connect_tm;
}

