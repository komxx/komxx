// Read texts in comment-order.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <algorithm>
#include <functional>
#include <cassert>
#include <cstdlib>

#include "conference.h"
#include "connection.h"
#include "kom-types.h"
#include "maps.h"
#include "membership.h"
#include "misc.h"
#include "read-order.h"
#include "text-mapping.h"
#include "text.h"
#include "text-prefetch.h"

// Helper class

void
Comment_stack::remove(Text_no tno)
{
    iterator iter = std::remove(begin(), end(), tno);
    erase(iter, end());
}

class textno_eq : public std::unary_function<Text_no, bool> {
    Text_no t;
  public:
    textno_eq(Text_no tno) : t(tno) {}
    bool operator() (const Read_item &r) const { return t == r.text_no; }
};

void
Extended_comment_stack::remove(Text_no tno)
{
    iterator iter = std::remove_if(begin(), end(), textno_eq(tno));
    erase(iter, end());
}

Work_quota::Work_quota(int required_texts,
		       int required_texts_in_conf,
		       int required_confs,
		       int required_maps,
		       int required_maps_in_conf,
		       int required_maps_conf,
		       int max_questions)
    : text_quota(required_texts),
      texts_in_conf_quota(required_texts_in_conf),
      texts_in_this_conf_quota(required_texts_in_conf),
      confs_quota(required_confs),
      map_quota(required_maps),
      maps_in_conf_quota(required_maps_in_conf),
      maps_in_this_conf_quota(required_maps_in_conf),
      map_confs_quota(required_maps_conf),
      question_quota(max_questions)
{
    if (map_quota < text_quota)
	map_quota = text_quota;
    if (maps_in_conf_quota < texts_in_conf_quota) 
    {
	maps_in_conf_quota = texts_in_conf_quota;
	maps_in_this_conf_quota = texts_in_conf_quota;
    }
}

Work_quota::Work_quota(int required_texts,
		       int required_maps,
		       int max_questions)
    : text_quota(required_texts),
      texts_in_conf_quota(0),
      texts_in_this_conf_quota(0),
      confs_quota(0),
      map_quota(required_maps),
      maps_in_conf_quota(0),
      maps_in_this_conf_quota(0),
      map_confs_quota(0),
      question_quota(max_questions)
{
    if (map_quota < text_quota)
	map_quota = text_quota;
}

Work_quota::Work_quota()
    : text_quota(20),
      texts_in_conf_quota(4),
      texts_in_this_conf_quota(4),
      confs_quota(20),
      map_quota(100),
      maps_in_conf_quota(50),
      maps_in_this_conf_quota(50),
      map_confs_quota(40),
      question_quota(15)
{
    if (map_quota < text_quota)
	map_quota = text_quota;
    if (maps_in_conf_quota < texts_in_conf_quota) 
    {
	maps_in_conf_quota = texts_in_conf_quota;
	maps_in_this_conf_quota = texts_in_conf_quota;
    }
}

Work_quota::~Work_quota()
{
}

void 
Work_quota::bump_question(int q)
{
    question_quota -= q;
    if (question_quota < 0)
	question_quota = 0;
}

bool 
Work_quota::done() const
{
    return question_quota == 0 || (text_quota == 0 && confs_quota == 0
				   && map_quota == 0 && map_confs_quota == 0);
}

bool 
Work_quota::conf_done() const
{
    return (question_quota == 0
	    || (texts_in_this_conf_quota == 0 && text_quota == 0));
}

bool 
Work_quota::conf_mapped() const
{
    return (question_quota == 0
	    || (maps_in_this_conf_quota == 0 && map_quota == 0));
}

void 
Work_quota::bump_known_texts(int texts)
{
    text_quota -= texts;
    if (text_quota < 0)
	text_quota = 0;
    texts_in_this_conf_quota -= texts;
    if (texts_in_this_conf_quota < 0)
	texts_in_this_conf_quota = 0;
}

void 
Work_quota::bump_known_mappings(int texts)
{
    map_quota -= texts;
    if (map_quota < 0)
	map_quota = 0;
    maps_in_this_conf_quota -= texts;
    if (maps_in_this_conf_quota < 0)
	maps_in_this_conf_quota = 0;
}

void 
Work_quota::change_conf()
{
    if (confs_quota > 0)
    {
	if (texts_in_this_conf_quota != texts_in_conf_quota) 
	{
	    if (--confs_quota > 0)
		texts_in_this_conf_quota = texts_in_conf_quota;
	    else
		texts_in_this_conf_quota = 0;
	}
	else
	    texts_in_this_conf_quota = texts_in_conf_quota;
    }
    else
	texts_in_this_conf_quota = 0;

    if (map_confs_quota > 0)
    {
	if (maps_in_this_conf_quota != maps_in_conf_quota) 
	{
	    if (--map_confs_quota > 0)
		maps_in_this_conf_quota = maps_in_conf_quota;
	    else
		maps_in_this_conf_quota = 0;
	}
	else
	    maps_in_this_conf_quota = maps_in_conf_quota;
    }
    else
	maps_in_this_conf_quota = 0;

    if (map_confs_quota == 0 && confs_quota > 0)
	map_confs_quota = 1;
    if (maps_in_this_conf_quota < texts_in_this_conf_quota)
	maps_in_this_conf_quota = texts_in_this_conf_quota;
    if (map_quota < text_quota)
	map_quota = text_quota;
}

int
Work_quota::questions_left() const
{
    return question_quota;
}

// ================================================================ 
//                    Constructors and Destructors                  


Read_order::Read_order(Session *sess, Conf_no cno, Pers_no reader)
    : session(sess), persons(sess->persons()), 
      conferences(sess->conferences()),
      texts(sess->texts()),
      map(sess->maps()[cno]),
      conf_no(cno), pers_no(reader),
      membership(),
      first_unchecked(0),
      unread_texts()
{
}
 

Read_order::~Read_order()
{
}


// ================================================================


// Returns true if any new mapping was received.
bool
Read_order::prefetch_map(Work_quota *work)
{
    bool got_anything = false;

    if (work->conf_mapped())
	return got_anything;
    Local_text_no lno = 0;
    Conference conf = conferences[conf_no];
    want_membership(false);

    switch (conf.prefetch())
    {
    case st_pending:
	// We can't do anything sensible unless we have the confstat.
	work->bump_question();
	if (session->prefetch_membership_pending(pers_no, conf_no))
	{
	    work->bump_question();
	}
	return got_anything;

    case st_error:
	// Now what? +++FIXME///
	// (Note: This case CAN occur if the conference is being
	// deleted during a session or, possibly, if the python
	// interface attempts to join a nonexisting conference).
	// Probably treat this the same way as not-member-of-the-conf.
	return got_anything;

    case st_ok:
        {
	    Local_text_no first_local = conf.first_local_no();
	    Local_text_no limit = first_local + conf.no_of_texts();

	    work->bump_known_mappings(unread_texts.size());
	    if (!work->conf_mapped() && first_unchecked < limit)
	    {
		if (want_membership(false) == st_pending)
		{
		    work->bump_question();
		    return got_anything;
		}

		assert(membership.initialized());
		assert(membership.kom_errno() != KOM_INVALIDATED);
		
		if (membership.kom_errno() != KOM_NO_ERROR)
		{
		    // FIXME: treat this properly
		    return false;
		}

		if (first_unchecked < first_local)
		    first_unchecked = first_local;
		
		while (!removed.empty()
		       && ((lno = removed.front()) < first_unchecked))
		    removed.erase(lno);

		for (;
		     first_unchecked < limit && !work->conf_mapped();
		     first_unchecked++)
		{
		    if (!removed.empty()
			&& removed.front() == first_unchecked)
		    {
			removed.erase(first_unchecked);
		    }
		    else if (!membership.is_read(first_unchecked))
		    {
			assert(!removed.present(first_unchecked));
			// FIXME+++: The "50" on the next line used to be
			// "texts", but that caused a very large question
			// to be issued. The map question should be split
			// into severaly small chunks, but I'm not sure
			// where the splitting should be done.
		        // Temporarily, do it this way.
			
			if (map.prefetch(first_unchecked) == st_pending)
			{
			    work->bump_question();
			    return got_anything;
			}

			Text_no tno = map[first_unchecked];
			if (tno != 0) 
			{
			    unread_texts.insert(tno);
			    work->bump_known_mappings();
			    got_anything = true;
			}
		    }
		    else
		    {
			assert(!removed.present(first_unchecked));
		    }
		}
	    }
	}
	break;

    default:
	std::abort();
    }

    return got_anything;
}

void
Read_order::prefetch_recurse(const Text_no &node,
			     std::set<Text_no> *stoplist,
			     Work_quota *work)
{
    // FIXME: This function keeps on recursing even if it reaches
    // texts that the user already has read.  That is a bug (unless
    // we want to prefetch the entire tree always, in case the user
    // wants to jump).

    if (stoplist->find(node) != stoplist->end())
	return;
    if (work->conf_done())
	return;

    Text t = texts[node];
    int q = prefetch_text_for_display(t, work->questions_left(), session);

    if (q == 0)
    {
	assert(t.present());
	stoplist->insert(node);
	work->bump_known_texts();
    }
    else 
    {
	work->bump_question(q);
    }

    if (t.present() && t.exists())
    {
	if (t.local_no_recipient(conf_no) == 0)	// Not present in this conf.
	    return;

	int ix;

	for (ix = 0; !work->conf_done() && ix < t.num_footnote_in(); ix++)
	    prefetch_recurse(t.footnote_in(ix), stoplist, work);
	
	for (ix = 0; !work->conf_done() && ix < t.num_comment_in(); ix++)
	    prefetch_recurse(t.comment_in(ix), stoplist, work);
    }
    return;
}

Status
Read_order::prefetch(Work_quota *work)
{
    // FIXME: This function keeps on recursing even if it reaches
    // texts that the user already has read.  That is a bug (unless
    // we want to prefetch the entire tree always, in case the user
    // wants to jump).
    std::set<Text_no> stoplist;
    int q = work->questions_left();

    // FIXME: This is maybe too conservative.  There are cases when
    // there is no prefetching to be done, and in those cases this
    // method ought to return st_ok.  On the other hand, it really
    // doesn't matter much, I guess.
    if (q == 0)
	return st_pending;

    for (Comment_stack::iterator iter = pending_footnotes.begin(); 
	 iter != pending_footnotes.end(); ++iter)
	prefetch_recurse(*iter, &stoplist, work);

    for (Comment_stack::iterator iter = pending_comments.begin(); 
	 iter != pending_comments.end(); ++iter)
	prefetch_recurse(*iter, &stoplist, work);

    for (std::set<Text_no>::iterator iter = unread_texts.begin();
	 iter != unread_texts.end() && !work->conf_done();
	 ++iter)
    {
	prefetch_recurse(*iter, &stoplist, work);
    }

    if (prefetch_map(work))
	return prefetch(work);

    if (q == work->questions_left())
	return st_ok;
    else
	return st_pending;
}
			
Prompt
Read_order::next_prompt()
{
    Work_quota w;
    while ((w = Work_quota(1, 0, 10)), (prefetch(&w) == st_pending))
    {	
	// FIXME: configurable timeout.
	switch (session->connection()->drain(100))
	{
	case st_pending:
	    return TIMEOUT_PROMPT;
	case st_error:
	    return PROMPT_ERROR;
	case st_ok:
	    break;
	}
    }

    if (!pending_footnotes.empty())
	return NEXT_FOOTNOTE;
    else if (!pending_comments.empty())
	return NEXT_COMMENT;
    else if (unread_texts.empty())
	return NO_MORE_TEXT;
    else
	return NEXT_TEXT;
}


//
// Return the next text to be read within this conference.
// Return 0 if there is no more unread text.
//

Text_no
Read_order::next_text(bool remove)
{
    Text_no tno = 0;
    
    if (need_one_mapped() == st_error)
	return 0;

    if (remove)
	if (!pending_footnotes.empty())
	    tno = pending_footnotes.pop();
	else if (!pending_comments.empty())
	    tno = pending_comments.pop();
	else if (!unread_texts.empty())
	{
	    tno = *unread_texts.begin();
	    unread_texts.erase(tno);
	}
        else
	    tno = 0;
    else
	if (!pending_footnotes.empty())
	    tno = pending_footnotes.front();
	else if (!pending_comments.empty())
	    tno = pending_comments.front();
	else if (!unread_texts.empty())
	    tno = *unread_texts.begin();
        else
	    tno = 0;

    if (remove && tno != 0)
    {
	// Add comments/footnotes:
	Text t = texts[tno];

	if (t.exists())
	{
	    enqueue_children(t);
	    remove_text(tno, t.local_no_recipient(conf_no));
	}
	else
	    remove_text(tno, 0);
    }
    return tno;
}


void
Read_order::enqueue_children(Text_no tno)
{
    enqueue_children(texts[tno]);
}


void
Read_order::enqueue_children(Text t)
{
    Text child;
    int i;
    Local_text_no lno;

    want_membership(false);

    if (!t.exists())
	return;

    want_membership(true);

    // ...prefetch the statuses
    for (i = t.num_footnote_in() -1; i >= 0; i--)
	texts[t.footnote_in(i)].prefetch();

    for (i = t.num_comment_in() -1; i >= 0; i--)
	texts[t.comment_in(i)].prefetch();

    // ...and receive them.
    for (i = t.num_footnote_in() -1; i >= 0; i--)
    {
	child = texts[t.footnote_in(i)];
	if (child.exists()
	    && (lno = child.local_no_recipient(conf_no)) != 0
	    && !removed.present(lno)
	    && !membership.is_read(lno))
	{
	    Text_no footnote_number = t.footnote_in(i);
	    pending_footnotes.push(footnote_number);
	    unread_texts.insert(footnote_number);
	}
    }

    for (i = t.num_comment_in() -1; i >= 0; i--)
    {
	child = texts[t.comment_in(i)];
	if (child.exists()
	    && (lno = child.local_no_recipient(conf_no)) != 0
	    && !removed.present(lno)
	    && !membership.is_read(lno))
	{
	    Text_no comment_number = t.comment_in(i);
	    pending_comments.push(comment_number);
	    unread_texts.insert(comment_number);
	}
    }
}


void
Read_order::remove_text(Text_no tno,
			Local_text_no lno)
{
    if (lno == first_unchecked + 1)
    {
	Local_text_no del_no = 0;
	first_unchecked++;

	while (!removed.empty()
	       && (del_no = removed.front()) < first_unchecked)
	    removed.erase(del_no);

	while (!removed.empty()
	       && (del_no = removed.front()) == first_unchecked)
	{
	    removed.erase(del_no);
	    first_unchecked++;
	}
    }
    else if (lno > first_unchecked)
	removed.add(lno);

    pending_comments.remove(tno);
    pending_footnotes.remove(tno);
    unread_texts.erase(tno);	// FIXME: what happens if tno isn't present?
}

bool
Read_order::empty()
{
    need_one_mapped();
    return unread_texts.empty();
}


long
Read_order::min_num_unread()
{
    return unread_texts.size();
}

 
long
Read_order::max_num_unread()
{
    want_membership(false);

    Conference conf = conferences[conf_no];
    if (!conf.present())
	return LONG_MAX;

    if (!membership.initialized() || membership.kom_errno() == KOM_INVALIDATED)
    {
	// FIXME: We could subtract any texts here that we know are
	// deleted. Such information may exist in the text-mapping.
	return conf.no_of_texts();
    }
    else if (membership.kom_errno() != KOM_NO_ERROR)
    {
	return 0;
    }

    long result = unread_texts.size();

    Local_text_no limit = conf.first_local_no() + conf.no_of_texts();

#if 0
    // This is SLOW when you have several thousand unread in I]M.
    for (Local_text_no ln = ((first_unchecked > membership->last_text_read())
			     ? first_unchecked : membership->last_text_read());
	 ln < limit; 
	 ln++)
    {
	if (!membership->is_read(ln))
	{
	    // +++ Should check if we know that this text is deleted.
	    result++;
	}
    }
#else
    if (first_unchecked > membership.last_text_read())
	result += limit - first_unchecked;
    else
	result += (limit - membership.last_text_read()
		   - membership.num_read_after_last_text_read());
#endif

    return result;    
}


long
Read_order::estimated_num_unread()
{
    want_membership(false);

    Conference conf = conferences[conf_no];
    if (conf.prefetch() == st_error)
	return 0;

    if (want_membership(true) != st_ok || !conf.exists())
	return 0;

    long approximation = (conf.first_local_no()
			  - membership.last_text_read()
			  + conf.no_of_texts()
			  - membership.num_read_after_last_text_read()
			  - 1);
    long min_unread = min_num_unread();
    long max_unread = max_num_unread();

    if (approximation < min_unread)
	approximation = min_unread;

    if (max_unread != -1 && approximation > max_unread)
	approximation = max_unread;

    return approximation;
}

Conf_no
Read_order::confno() const
{
    return conf_no;
}

Kom_err
Read_order::set_last_read(Local_text_no last_text_read)
{
    if (want_membership(false) == st_error)
	return membership.kom_errno();

    if (pers_no == session->my_login()) 
    {
	// FIXME: this is a good place to detect that the user isn't a
	// member of the conference, once we can get the error number
	// back from the session.
	session->set_last_read(conf_no, last_text_read);
    }

    // Now the server is updated.  Let's try to fix the local caches
    // as well.

    want_membership(true);
    if (membership.kom_errno() == KOM_NO_ERROR)
    {
	membership.set_last_text_read(last_text_read);
    }
    first_unchecked = 0;
    removed.clear();
    unread_texts.clear();
    pending_footnotes.clear();
    pending_comments.clear();
    return KOM_NO_ERROR;
}
    

std::vector<Text_no>
Read_order::query_pending_footnotes() const
{
    std::vector<Text_no> res;
    copy(pending_footnotes.begin(), pending_footnotes.end(), 
	 back_inserter(res));
    return res;
}

std::vector<Text_no>
Read_order::query_pending_comments() const
{
    std::vector<Text_no> res;
    copy(pending_comments.begin(), pending_comments.end(), 
	 back_inserter(res));
    return res;
}

void
Read_order::set_pending(const std::vector<Text_no> &arr, Comment_stack *const stk)
{
    bool lack_membership = want_membership(true) != st_ok;

    for (int prefetch_ix = arr.size() - 1; prefetch_ix >= 0; --prefetch_ix)
	texts[arr[prefetch_ix]].prefetch();

    stk->clear();
    for (int ix = arr.size() - 1; ix >= 0; --ix)
    {
	Text t = texts[arr[ix]];
	if (t.exists())		// Silently ignore bad texts.
	{
	    Local_text_no lno = t.local_no_recipient(conf_no);
	    if (lno != 0 && (lack_membership || !membership.is_read(lno))
		&& !removed.present(lno))
	    {
		// FIXME: is it necessary to check removed?  Is removed
		// really useful?  Shouldn't Membership::mark_as_read
		// be implemented and called instead?
		stk->push(arr[ix]);
	    }
	}
    }
}
    
void
Read_order::set_pending_footnotes(const std::vector<Text_no> &arr)
{
    set_pending(arr, &pending_footnotes);
}

void
Read_order::set_pending_comments(const std::vector<Text_no> &arr)
{
    set_pending(arr, &pending_comments);
}
     

Read_order_iterator
Read_order::begin(int wanted_texts)
{
    return Read_order_iterator(this, wanted_texts);
}

Read_order_iterator
Read_order::end()
{
    return Read_order_iterator(this, 0);
}

Status
Read_order::need_one_mapped()
{
    Work_quota w;
    while ((w = Work_quota(1,0,10)), (prefetch(&w) == st_pending))
	if (session->connection()->drain(100) == st_error)
	    return st_error;
    return st_ok;
}

Status
Read_order::want_membership(bool block)
{
    if (!membership.initialized() || membership.kom_errno() == KOM_INVALIDATED)
    {
	if (block
	    || session->prefetch_membership(pers_no, conf_no) != st_pending)
	{
	    membership = session->get_membership(pers_no, conf_no);
	}
	else
	    return st_pending;
    }

    if (membership.kom_errno() == KOM_NO_ERROR)
	return st_ok;
    else
	return st_error;
}

// Read_order_iterator

Read_order_iterator::Read_order_iterator()
    : unread_texts(),
      pending_footnotes(),
      pending_comments(),
      removed(),
      texts_left(0),
      current(Read_item(0, 0)),
      ro(NULL)
{
    // Any attempt to use this iterator will result in a crash.
}

Read_order_iterator::~Read_order_iterator()
{
    unread_texts.clear();
    removed.clear();
}

Read_order_iterator::Read_order_iterator(const Read_order_iterator &iter)
    : unread_texts(iter.unread_texts),
      pending_footnotes(iter.pending_footnotes),
      pending_comments(iter.pending_comments),
      removed(iter.removed),
      texts_left(iter.texts_left),
      current(iter.current),
      ro(iter.ro)
{
}

bool
Read_order_iterator::operator==(const Read_order_iterator &iter) const
{
    assert(ro != NULL);
    assert(iter.ro != NULL);
    assert(ro == iter.ro);
    return current.text_no == iter.current.text_no;
}

bool
Read_order_iterator::operator!=(const Read_order_iterator &iter) const
{
    assert(ro != NULL);
    assert(iter.ro != NULL);
    assert(ro == iter.ro);
    return current.text_no != iter.current.text_no;
}

Read_order_iterator &
Read_order_iterator::operator=(const Read_order_iterator &iter)
{
    unread_texts = iter.unread_texts;
    pending_footnotes = iter.pending_footnotes;
    pending_comments = iter.pending_comments;
    removed = iter.removed;
    texts_left = iter.texts_left;
    current = iter.current;
    ro = iter.ro;

    return *this;
}

Read_item
Read_order_iterator::operator*()
{
    assert(texts_left > 0);
    assert(current.nesting_level >= 0);
    assert(current.text_no != 0);

    return current;
}

Read_order_iterator &
Read_order_iterator::operator++()
{
    assert(texts_left > 0);

    if (texts_left == 1)
    {
	// This is becoming a past-the-end iterator
	current.text_no = 0;
	texts_left = 0;
	return *this;
    }

    if (current.text_no == 0)
    {
	// This iterator is currently being initialized.  It
	// points before-the-start.  This condition is never
	// exposed to the user of the class; it exists while
	// the iterator is being constructed.
	assert(pending_footnotes.empty());
	assert(pending_comments.empty());
    }

    current.text_no = 0;

    if (!pending_footnotes.empty())
	current = pending_footnotes.pop();
    else if (!pending_comments.empty())
	current = pending_comments.pop();
    else if (!unread_texts.empty())
    {
	current.text_no = *unread_texts.begin();
	current.nesting_level = 0;
	unread_texts.erase(current.text_no);
    }
    else
	std::abort();

    assert(current.text_no != 0);

    Text t = ro->texts[current.text_no];
    if (t.exists())
    {
	Text child;
	int i;
	Local_text_no lno;

	// ...prefetch the statuses
	for (i = t.num_footnote_in() -1; i >= 0; i--)
	    ro->texts[t.footnote_in(i)].prefetch();

	for (i = t.num_comment_in() -1; i >= 0; i--)
	    ro->texts[t.comment_in(i)].prefetch();

	// ...and receive them.
	for (i = t.num_footnote_in() -1; i >= 0; i--)
	{
	    child = ro->texts[t.footnote_in(i)];
	    if (child.exists()
		&& (lno = child.local_no_recipient(ro->conf_no)) != 0
		&& !removed.present(lno)
		&& !ro->membership.is_read(lno))
	    {
		Text_no footnote_number = t.footnote_in(i);
		pending_footnotes.push(Read_item(footnote_number,
						 current.nesting_level+1));
		unread_texts.insert(footnote_number);
	    }
	}

	for (i = t.num_comment_in() -1; i >= 0; i--)
	{
	    child = ro->texts[t.comment_in(i)];
	    if (child.exists()
		&& (lno = child.local_no_recipient(ro->conf_no)) != 0
		&& !removed.present(lno)
		&& !ro->membership.is_read(lno))
	    {
		Text_no comment_number = t.comment_in(i);
		pending_comments.push(Read_item(comment_number,
						current.nesting_level+1));
		unread_texts.insert(comment_number);
	    }
	}
    }

    pending_comments.remove(current.text_no);
    pending_footnotes.remove(current.text_no);
    unread_texts.erase(current.text_no);// FIXME: what if text_no isn't present?
    --texts_left;

    return *this;
}

Read_order_iterator
Read_order_iterator::operator++(int)
{
    Read_order_iterator tmp = *this;
    ++*this;
    return tmp;
}
    
Read_order_iterator::Read_order_iterator(Read_order *read_order,
					 int wanted_texts)
    : unread_texts(),
      pending_footnotes(),
      pending_comments(),
      removed(read_order->removed),
      texts_left(wanted_texts+1), // Compensate for the initial "hidden" step.
      current(Read_item(0, 0)),
      ro(read_order)
{
    if (wanted_texts == 0)	// The past-the-end value.
	texts_left = 0;

    Work_quota w;

    while (ro->prefetch(((w = Work_quota(wanted_texts, 0,0, 
					 wanted_texts, 0,0, 20)), 
			 &w))
	   == st_pending)
    {
	if (ro->session->connection()->drain(100) == st_error)
	{
	    texts_left = 0;
	    return;
	}
    }

    unread_texts = ro->unread_texts;
    if (unread_texts.size() + 1 < texts_left)
	texts_left = unread_texts.size() + 1;

    if (texts_left > 0)
	++*this;			// Step forward to the first text.
}

// read-order.cc ends here
