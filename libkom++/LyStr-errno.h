// LyStr and errno aggregate
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_LYSTR_ERRNO_H
#define KOMXX_LYSTR_ERRNO_H

#include "LyStr.h"
#include "error-codes.h"

// Instead of using a pair<LyStr, Kom_err> the code uses the following class.

class LyStr_errno {
  public:
    LyStr_errno(const LyStr &str_init,
		Kom_err err_init);
    LyStr_errno(Kom_err err_init);
    LyStr_errno(const LyStr &str_init);
    LyStr_errno(const LyStr_errno &);
    LyStr_errno();
    ~LyStr_errno();
    LyStr_errno &operator=(const LyStr_errno &);

    LyStr str;
    Kom_err kom_errno;
};

#endif
