// Handle information about a member.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include "member.h"
#include "membership.h"
#include "time-utils.h"
#include "prot-a-LyStr.h"

Member::Member()
  : m_member(0),
    m_added_by(0),
    m_added_at(),
    m_type()
{
}

Member::Member(prot_a_LyStr &str)
{
    m_member = str.stol();
    m_added_by = str.stol();
    m_added_at = parse_time(str);
    m_type = Membership_type(str);
}


Member::Member(const Member &o)
  : m_member(o.m_member),
    m_added_by(o.m_added_by),
    m_added_at(o.m_added_at),
    m_type(o.m_type)
{
}

Member &
Member::operator=(const Member &o)
{
    if (this != &o)
    {
	m_member = o.m_member;
	m_added_by = o.m_added_by;
	m_added_at = o.m_added_at;
	m_type = o.m_type;
    }

    return *this;
}


Member::~Member()
{
}

Pers_no 
Member::member() const
{
    return m_member;
}

Pers_no 
Member::added_by() const
{
    return m_added_by;
}

struct std::tm
Member::added_at() const
{
    return m_added_at;
}

Membership_type 
Member::type() const
{
    return m_type;
}
