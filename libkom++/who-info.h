// Who-info object.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_WHO_INFO_H
#define KOMXX_WHO_INFO_H

#include "kom-types.h"
#include "LyStr.h"

class prot_a_LyStr;

class Who_info {
  public:
    Who_info();
    Who_info(const Who_info &);
    Who_info(prot_a_LyStr &prot_a_string);
    ~Who_info();
    
    Who_info &operator =(const Who_info &);

    void  parse(prot_a_LyStr &prot_a_string);

    Pers_no      pers_no()      const;
    Conf_no      working_conf() const;
    Session_no   session()      const;
    LyStr        doing()        const;
    LyStr        username()     const;

  private:
    Pers_no		person;
    Conf_no		working_conference;
    Session_no 		session_no;	// Serial number of connection.
    LyStr		what_am_i_doing;
    LyStr		user_name;      // Userid and hostname.
};
#endif
