// Handle information about a member.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_MEMBER_H
#define KOMXX_MEMBER_H

#include <ctime>

#include "kom-types.h"
#include "membership.h"

class prot_a_LyStr;

class Member {
  public:
    Member();
    explicit Member(prot_a_LyStr &str);
    Member(const Member&);
    Member &operator = (const Member &);
    ~Member();
    
    Pers_no member() const;
    Pers_no added_by() const;
    struct std::tm added_at() const;
    Membership_type type() const;

  private:
    Pers_no m_member;
    Pers_no m_added_by;
    struct std::tm m_added_at;
    Membership_type m_type;
};

#endif
