// Store a protocol A Text-Mapping.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_LOCAL_TO_GLOBAL_MAP_H
#define KOMXX_LOCAL_TO_GLOBAL_MAP_H

#include <map>

#include "kom-types.h"

class prot_a_LyStr;

class local_to_global_map {
  public:
    typedef std::map<Local_text_no, Text_no> map_type;
    typedef map_type::const_iterator const_iterator;

    local_to_global_map();
    local_to_global_map(prot_a_LyStr& prot_a_str);
    local_to_global_map(const local_to_global_map&);
    local_to_global_map & operator=(const local_to_global_map&);
    ~local_to_global_map();

    Local_text_no range_begin() const;
    Local_text_no range_end() const;
    bool more_texts_exists() const;
    const_iterator begin() const;
    const_iterator end() const;

  private:
    Local_text_no lg_range_begin;
    Local_text_no lg_range_end;
    bool lg_more_texts_exists;
    map_type lg_mapping;
};

#endif
