// Fetch-on-demand conference object.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>
#include <cstdlib>

#include "conference.h"
#include "conf-cache.h"
#include "uconf-cache.h"
#include "set_conf_type_q.h"
#include "change_name_q.h"
#include "set_permitted_submitters_q.h"
#include "modify_conf_info_q.h"
#include "set_expire_q.h"
#include "set_garb_nice_q.h"
#include "set_keep_commented_q.h"

// ================================================================
// ================================================================
//                            Class Conference


// ================================================================ 
//                    Constructors and Destructors                  


Conference::Conference(Conf_no confno, Conf_cache * cc, UConf_cache * ucc)
    : cno(confno),
      cache(cc),
      ucache(ucc)
{
}

 
Conference::Conference(const Conference &conf)
    : Fetchable(conf),
      cno(conf.cno),
      cache(conf.cache),
      ucache(conf.ucache)
{
}


Conference::~Conference()
{
}


// ================================================================ 
//

const Conference & 
Conference::operator = (const Conference &c)
{
    cno = c.cno;
    cache = c.cache;
    ucache = c.ucache;
    return *this;
}


Status
Conference::prefetch() const
{
    return cache->prefetch(cno);
}

bool
Conference::prefetch_pending() const
{
    return cache->prefetch_pending(cno);
}

Status
Conference::prefetch_uconf() const
{
    if (bypass_uconf())
	return prefetch();
    else
	return ucache->prefetch(cno);
}

bool
Conference::prefetch_uconf_pending() const
{
    if (bypass_uconf())
	return prefetch_pending();
    else
	return ucache->prefetch_pending(cno);
}

Kom_err
Conference::uconf_kom_errno() const
{
    if (bypass_uconf())
	return kom_errno();
    else
	return ucache->get(cno).kom_errno();
}

Kom_err
Conference::kom_errno() const
{
    return cache->get(cno).kom_errno();
}

bool
Conference::present() const
{
    return cache->present(cno);
}

bool
Conference::uconf_present() const
{
    if (bypass_uconf())
	return present();
    else
	return ucache->present(cno);
}

bool
Conference::exists() const
{
    return kom_errno() == KOM_NO_ERROR;
}

bool
Conference::uconf_exists() const
{
    if (bypass_uconf())
	return exists();
    else
	return uconf_kom_errno() == KOM_NO_ERROR;
}

Conf_no
Conference::conf_no() const
{
    return cno;
}

Connection *
Conference::connection() const
{	
    return cache->connection();
}

bool
Conference::bypass_uconf() const
{
    return (cache->present(cno)
	    || (cache->prefetch_pending(cno)
		&& !ucache->present(cno)));
}

Pers_no 
Conference::creator() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).creator();
}

struct tm 
Conference::creation_time() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).creation_time();
}

Text_no 
Conference::presentation() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).presentation();
}

Conf_no 
Conference::supervisor() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).supervisor();
}

Conf_no 
Conference::permitted_submitters() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).permitted_submitters();
}

Conf_no 
Conference::super_conf() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).super_conf();
}

Conf_type 
Conference::type() const
{
    if (bypass_uconf())
    {
	assert(kom_errno() == KOM_NO_ERROR);
	return cache->get(cno).type();
    }
    else
    {
	assert(uconf_kom_errno() == KOM_NO_ERROR);
	return ucache->get(cno).type();
    }
}

struct std::tm 
Conference::last_written() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).last_written();
}

Text_no 
Conference::msg_of_day() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).msg_of_day();
}

Garb_nice 
Conference::nice() const
{
    if (bypass_uconf())
    {
	assert(kom_errno() == KOM_NO_ERROR);
	return cache->get(cno).nice();
    }
    else
    {
	assert(uconf_kom_errno() == KOM_NO_ERROR);
	return ucache->get(cno).nice();
    }
}

Garb_nice 
Conference::keep_commented() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).keep_commented();
}

LyStr 
Conference::name() const
{
    if (bypass_uconf())
    {
	assert(kom_errno() == KOM_NO_ERROR);
	return cache->get(cno).name();
    }
    else
    {
	assert(uconf_kom_errno() == KOM_NO_ERROR);
	return ucache->get(cno).name();
    }
}

unsigned short 
Conference::no_of_members() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).no_of_members();
}

Local_text_no 
Conference::first_local_no() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).first_local_no();
}

unsigned long 
Conference::no_of_texts() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).no_of_texts();
}

Garb_nice 
Conference::expire() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).expire();
}

int
Conference::num_aux() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).num_aux();
}

Aux_item
Conference::aux(int nr) const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return cache->get(cno).aux(nr);
}

Local_text_no 
Conference::highest_local_no() const
{
    if (bypass_uconf())
    {
	assert(kom_errno() == KOM_NO_ERROR);
	Conf_stat stat = cache->get(cno);
	return stat.first_local_no() + stat.no_of_texts() - 1;
    }
    else
    {
	assert(uconf_kom_errno() == KOM_NO_ERROR);
	return ucache->get(cno).highest_local_no();
    }
}

Kom_err
Conference::set_type(Conf_type conf_type)
{
    set_conf_type_question   sct_q(cache->connection(), cno, conf_type);
    if (sct_q.receive() == st_ok) 
    {
	if (present())
	    cache->get(cno).set_type(conf_type);
	if (uconf_present())
	    ucache->get(cno).set_type(conf_type);
    }
    return sct_q.error();
}

Kom_err
Conference::change_name(const LyStr &new_name)
{
    change_name_question  cn_q(cache->connection(), cno, new_name);
    if (cn_q.receive() == st_ok) 
    {
	if (present())
	    cache->get(cno).change_name(new_name);
	if (uconf_present())
	    ucache->get(cno).change_name(new_name);
    }
    return cn_q.error();
}

Kom_err
Conference::set_permitted_submitters(Conf_no permsub)
{
    set_permitted_submitters_question q(cache->connection(), cno, permsub);
    if (q.receive() == st_ok) 
    {
	if (present())
	    cache->get(cno).set_permitted_submitters(permsub);
    } 
    
    return q.error();
}


Kom_err
Conference::modify_info(const std::vector<Aux_no> &del,
			const std::vector<Aux_item> &add)
{
    modify_conf_info_question q(cache->connection(), cno, del, add);
    // No need to update the cache, as we get an
    // async-conf-aux-changed message.
    return q.error_blocking();
}


Kom_err
Conference::set_expire(Garb_nice expire)
{
    set_expire_question q(cache->connection(), cno, expire);
    if (q.receive() == st_ok) 
    {
	if (present())
	    cache->get(cno).set_expire(expire);
    } 
    
    return q.error();
}

Kom_err
Conference::set_garb_nice(Garb_nice garb_nice)
{
    set_garb_nice_question q(cache->connection(), cno, garb_nice);
    if (q.receive() == st_ok) 
    {
	if (present())
	    cache->get(cno).set_garb_nice(garb_nice);
    } 
    
    return q.error();
}

Kom_err
Conference::set_keep_commented(Garb_nice keep_commented)
{
    set_keep_commented_question q(cache->connection(), cno, keep_commented);
    if (q.receive() == st_ok) 
    {
	if (present())
	    cache->get(cno).set_keep_commented(keep_commented);
    }
    
    return q.error();
}


// ================================================================
// ================================================================
//                            Class Conferences


// ================================================================ 
//                    Constructors and Destructors                  


Conferences::Conferences(Connection *co, Async_dispatcher *async_disp)
  : conn(co),
    conf_cache(new Conf_cache(co, async_disp)),
    uconf_cache(new UConf_cache(co, async_disp))
{
}
 

Conferences::~Conferences()
{
    delete conf_cache;
    delete uconf_cache;
}


// ================================================================ 
//

Connection *
Conferences::connection() const
{
    return conn;
}

Conference
Conferences::operator [](Conf_no confno) const
{
    assert(conf_cache != NULL);
    return Conference(confno, conf_cache, uconf_cache);
}

Conf_cache * 
Conferences::read_conf_cache() const
{
    return conf_cache;
}

UConf_cache * 
Conferences::read_uconf_cache() const
{
    return uconf_cache;
}
