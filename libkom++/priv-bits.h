// Privilege bits.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_PRIV_BITS_H
#define KOMXX_PRIV_BITS_H

class LyStr;

/*
 * The privilige bits:  says what priviliges a person has.  E g
 *  if he is allowed to read texts he normally shouldn't be allowed
 *  to read. See file doc/security-levels.txt
 */
class Priv_bits {
  public:
    Priv_bits();
    Priv_bits(bool wheel,
	      bool admin,
	      bool statistic,
	      bool create_pers,
	      bool create_conf,
	      bool change_name,
	      bool flg7,
	      bool flg8,
	      bool flg9,
	      bool flg10,
	      bool flg11,
	      bool flg12,
	      bool flg13,
	      bool flg14,
	      bool flg15,
	      bool flg16);

    Priv_bits(const Priv_bits&);
    const Priv_bits &operator =(const Priv_bits &);

    friend Priv_bits parse_priv_bits(LyStr &prot_a_str);

    bool wheel() const;
    bool admin() const;
    bool statistic() const;
    bool create_pers() const;
    bool create_conf() const;
    bool change_name() const;
    bool flg7() const;
    bool flg8() const;
    bool flg9() const;
    bool flg10() const;
    bool flg11() const;
    bool flg12() const;
    bool flg13() const;
    bool flg14() const;
    bool flg15() const;
    bool flg16() const;
  private:
    bool pb_wheel       : 1;
    bool pb_admin       : 1;
    bool pb_statistic   : 1;
    bool pb_create_pers : 1;
    bool pb_create_conf : 1;
    bool pb_change_name : 1;
    bool pb_flg7 : 1;
    bool pb_flg8 : 1;
    bool pb_flg9 : 1;
    bool pb_flg10 : 1;
    bool pb_flg11 : 1;
    bool pb_flg12 : 1;
    bool pb_flg13 : 1;
    bool pb_flg14 : 1;
    bool pb_flg15 : 1;
    bool pb_flg16 : 1;
};

Priv_bits parse_priv_bits(LyStr &prot_a_str);

#endif
