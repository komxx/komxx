// Cache of memberships.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>

#include "membership-cache.h"
#include "question.h"
#include "misc.h"
#include "error-codes.h"
#include "get_membership_q.h"

static Membership bad_membership;
static Membership zero_membership(KOM_CONF_ZERO);

Membership_cache::Membership_cache(Connection *c)
  : conn(c)
{
}

Membership_cache::~Membership_cache()
{
}

Connection *
Membership_cache::connection() const
{
    return conn;
}

bool
Membership_cache::present(Pers_no pno, Conf_no cno) const
{
    return statusmap.find(std::make_pair(pno, cno)) != statusmap.end();
}

Status
Membership_cache::prefetch(Pers_no pno, Conf_no cno)
{
    if (pno <= 0 || cno <= 0)
	return st_error;

    std::pair<Pers_no, Conf_no>  index = std::make_pair(pno, cno);

    Immap::iterator stat_iter = statusmap.find(index);
    if (stat_iter != statusmap.end()
	&& (*stat_iter).second.kom_errno() != KOM_INVALIDATED)
    {
	if ((*stat_iter).second.kom_errno() == KOM_NO_ERROR)
	    return st_ok;
	else
	    return st_error;
    }
    else
    {
	Imreqmap::iterator req_iter = pendingmap.find(index);
	if (req_iter == pendingmap.end())
	{
	    pendingmap[index] = new get_membership_question(conn, pno, cno);
	    return st_pending;
	}
	else
	{
	    switch ((*req_iter).second->status())
	    {
	    case st_pending:
		return st_pending;
	    case st_ok:
		if (stat_iter != statusmap.end())
		    (*stat_iter).second = pendingmap[index]->result();
		else
		    statusmap[index] = pendingmap[index]->result();
		delete (*req_iter).second;
		pendingmap.erase(req_iter);
		return st_ok;
	    case st_error:
		if (stat_iter != statusmap.end())
		    (*stat_iter).second = 
			Membership(pendingmap[index]->error());
		else
		    statusmap[index] = Membership(pendingmap[index]->error());
		delete (*req_iter).second;
		pendingmap.erase(req_iter);
		return st_error;
	    default:
		std::abort();
	    }
	}
    }
    std::abort();
}

bool
Membership_cache::prefetch_pending(Pers_no pno, Conf_no cno)
{
    if (pno <= 0 || cno <= 0)
	return false;

    Imreqmap::iterator req_iter = pendingmap.find(std::make_pair(pno, cno));
    if (req_iter == pendingmap.end())
	return false;

    // Allow the prefetch code to receive the pending question.
    return prefetch(pno, cno) == st_pending;
}

bool
Membership_cache::present_or_pending(Pers_no pno, Conf_no cno)
{
    if (pno <= 0 || cno <= 0)
	return st_error;

    std::pair<Pers_no, Conf_no>  index = std::make_pair(pno, cno);

    return (statusmap.find(index) != statusmap.end()
	    || pendingmap.find(index) != pendingmap.end());
}

Membership
Membership_cache::get(Pers_no pno, Conf_no cno)
{
    if (pno <= 0 || cno <= 0)
	return zero_membership;

    std::pair<Pers_no, Conf_no>  index = std::make_pair(pno, cno);
    bool present = false;

    Immap::iterator status_iter = statusmap.find(index);
    if (status_iter != statusmap.end())
    {
	Membership retval = (*status_iter).second;
	if (retval.kom_errno() != KOM_INVALIDATED)
	    return retval;
	else
	    present = true;
    }

    Imreqmap::iterator req_iter = pendingmap.find(index);
    if (req_iter != pendingmap.end())
    {
	get_membership_question *gtr = (*req_iter).second;
	pendingmap.erase(req_iter);
	Membership result;
	if (gtr->receive() == st_ok)
	    result = gtr->result();
	else
	    result = Membership(gtr->error());
	if (present) 
	    (*status_iter).second = result;
	else
	    statusmap[index] = result;
	delete gtr;
	return result;
    }
    else
    {
	// Not present in cache, and no outstanding question.
	get_membership_question gt(conn, pno, cno);
	Membership result;
	if (gt.receive() == st_ok)
	    result = gt.result();
	else
	    result = Membership(gt.error());
	if (present) 
	    (*status_iter).second = result;
	else
	    statusmap[index] = result;
	return result;
    }
    std::abort();
}

// FIXME: implementing this is tricky, since several places in kom++
// may hold Membership objects.  It should probably never be done,
// since they share data.  This (the cache) is the one place where a
// mapping from (pno,cno) to Membership is done -- it should always
// return the same Membership!  It is probably OK to remove the
// membership if the reference count is 1 (meaning that the cache
// holds the only copy).
//void
//Membership_cache::remove(Pers_no pno, Conf_no cno)
//{
//    abort();
//}
