// A micro-conf object.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_MICRO_CONF_H
#define KOMXX_MICRO_CONF_H

#include "conf-type.h"
#include "kom-types.h"
#include "prot-a-LyStr.h"


class Micro_conf {
  public:
    Micro_conf();
    Micro_conf(const Micro_conf&);
    const Micro_conf &operator =(const Micro_conf &);

    void parse_conf_no(prot_a_LyStr &str); // +++Error handling??
    void parse_conf_type(prot_a_LyStr &str);

    Conf_no     conf_no() const;
    Conf_type   conf_type() const;
  private:
    Conf_no     no;
    Conf_type   type;
};
#endif
