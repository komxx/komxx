// Cache of conf-statuses.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_CONF_CACHE_H
#define KOMXX_CONF_CACHE_H

#include <map>

#include "kom-types.h"
#include "async_dispatcher.h"
#include "misc.h"
#include "conf-stat.h"

class get_conf_stat_question;

class Conf_cache {
  public:
    Conf_cache(Connection *c, Async_dispatcher *async_disp);
    Conf_cache(const Conf_cache&);
    Conf_cache& operator=(const Conf_cache&);
    ~Conf_cache();

    // Methods working on the entire cache:
    Connection *connection() const;
    bool        present(Conf_no cno) const;
    void        clear();

    // Methods working on individual conf-stats.
    Status      prefetch(Conf_no cno);
    bool        prefetch_pending(Conf_no cno);
    Conf_stat   get(Conf_no cno);
    void        remove(Conf_no cno);

    // The cache automatically registers this callback method with the
    // Async_dispatcher.  This method should not be used in any other
    // way.
    void        new_text(const Async_text_created &msg);

  private:
    typedef std::map<Conf_no, Conf_stat> Ccmap;
    typedef std::map<Conf_no, get_conf_stat_question *> Ccreqmap;

    Connection* conn;
    Async_dispatcher *async_disp;
    Async_handler_tag<Async_text_created> cr_tag;

    Ccmap       statusmap;
    Ccreqmap    pendingmap;
};

#endif
