// Information about a server.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_SERVER_INFO_H
#define KOMXX_SERVER_INFO_H

#include <vector>

#include "kom-types.h"
#include "prot-a-LyStr.h"

class Server_info_old {
  public:
    Server_info_old();
    Server_info_old(const Server_info_old&);
    Server_info_old(prot_a_LyStr &str);
    virtual ~Server_info_old();
    const Server_info_old &operator =(const Server_info_old &);

    long      version()        const;
    Conf_no   conf_pres_conf() const;
    Conf_no   pers_pres_conf() const;
    Conf_no   motd_conf()      const;
    Conf_no   kom_news_conf()  const;
    Text_no   motd_of_lyskom() const;
  private:
    long		si_version;
    Conf_no		si_conf_pres_conf; /* Presentation of new confs */
    Conf_no		si_pers_pres_conf; /* Presentation of new persons */
    Conf_no		si_motd_conf; 	   /* Conf that receive motds */
    Conf_no		si_kom_news_conf;  /* News about kom */
    Text_no		si_motd_of_lyskom; /* To be displayed after login */
};

class Server_info : public Server_info_old {
  public:
    Server_info();
    Server_info(const Server_info&);
    Server_info(prot_a_LyStr &str);
    ~Server_info();
    const Server_info &operator =(const Server_info &);
    
    int		num_aux()	 const;
    Aux_item    aux(int)	 const;
  private:
    std::vector<Aux_item> si_aux;
};

#endif
