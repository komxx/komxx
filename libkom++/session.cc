// Highlevel handling of a session to a LysKOM server.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstddef>
#include <cassert>

#include "session.h"
#include "container.h"

#include "login_q.h"
#include "logout_q.h"
#include "enable_q.h"
#include "get_time_q.h"
#include "get_last_text_q.h"
#include "set_motd_of_lyskom_q.h"
#include "set_info_q.h"
#include "set_presentation_q.h"
#include "change_what_i_am_doing_q.h"
#include "lookup_name_q.h"
#include "lookup_z_name_q.h"
#include "re_z_lookup_q.h"
#include "re_lookup_person_q.h"
#include "get_members_old_q.h"
#include "set_last_read_q.h"
#include "set_membership_type_q.h"
#include "set_unread_q.h"
#include "get_marks_q.h"
#include "get_collate_table_q.h"
#include "who_am_i_q.h"
#include "who_is_on_ident_q.h"
#include "who_is_on_dynamic_q.h"
#include "create_person_q.h"
#include "create_conf_q.h"
#include "add_member_q.h"
#include "sub_member_q.h"
#include "send_message_q.h"
#include "set_client_version_q.h"
#include "membership-cache.h"
#include "text-mapping.h"
#include "time-utils.h"
#include "user_active_q.h"
#include "query_async_q.h"
#include "static-session-info.h"
#include "modify_system_info_q.h"


template <class Q>
Session::Prefetchable_static_info<Q>::Prefetchable_static_info()
  : stored_result(),
    pending_question(NULL),
    result_fetched(false)
{
}

template <class Q>
Session::Prefetchable_static_info<Q>::~Prefetchable_static_info()
{
    delete pending_question;
}

template <class Q>
Status
Session::Prefetchable_static_info<Q>::receive()
{
    Status st = pending_question->receive();
    if (st == st_ok)
    {
	stored_result = pending_question->result();
	result_fetched = true;
	delete pending_question;
	pending_question = NULL;
    }
    return st;
}

template <class Q>
bool
Session::Prefetchable_static_info<Q>::fetched() const
{
    return result_fetched;
}

template <class Q>
typename Q::result_type
Session::Prefetchable_static_info<Q>::result() const
{
    return stored_result;
}

template <class Q>
Q*
Session::Prefetchable_static_info<Q>::question() const
{
    return pending_question;
}


template <class Q>
void
Session::Prefetchable_static_info<Q>::store_question(Q* q)
{
    pending_question = q;
}

template <class Q>
void
Session::Prefetchable_static_info<Q>::store_result(
    const typename Q::result_type &r)
{
    stored_result = r;
}


template <class Q>
void
Session::Prefetchable_static_info<Q>::finalize_error()
{
    delete pending_question;
    pending_question = NULL;
    result_fetched = true;
}


template <class Q, class Key>
Session::Prefetchable_static_info_map<Q, Key>::Prefetchable_static_info_map()
  : stored_results(),
    pending_questions()
{
}

template <class Q, class Key>
Session::Prefetchable_static_info_map<Q, Key>::~Prefetchable_static_info_map()
{
    delete_contents_map(pending_questions);
}

template <class Q, class Key>
Status
Session::Prefetchable_static_info_map<Q, Key>::receive(Key k)
{
    typename std::map<Key, Q*>::iterator iter(pending_questions.find(k));
    if (iter == pending_questions.end())
	std::abort();
    Status st = iter->second->receive();
    if (st == st_ok)
    {
	stored_results[k] = iter->second->result();
	delete iter->second;
	pending_questions.erase(iter);
    }
    return st;
}

template <class Q, class Key>
bool
Session::Prefetchable_static_info_map<Q, Key>::fetched(Key k) const
{
    return stored_results.find(k) != stored_results.end();
}

template <class Q, class Key>
Kom_res<typename Q::result_type>
Session::Prefetchable_static_info_map<Q, Key>::result(Key k) const
{
    typename res_map_type::const_iterator iter(stored_results.find(k));
    if (iter == stored_results.end())
	std::abort();
    else
	return iter->second;
}

template <class Q, class Key>
Q*
Session::Prefetchable_static_info_map<Q, Key>::question(Key k) const
{
    typename std::map<Key, Q*>::const_iterator iter(pending_questions.find(k));
    if (iter == pending_questions.end())
	return NULL;
    else
	return iter->second;
}


template <class Q, class Key>
void
Session::Prefetchable_static_info_map<Q, Key>::store_question(Key k, Q* q)
{
    pending_questions[k] = q;
}

template <class Q, class Key>
void
Session::Prefetchable_static_info_map<Q, Key>::finalize_error(
    Key k, 
    Kom_err err)
{
    typename std::map<Key, Q*>::iterator iter(pending_questions.find(k));
    if (iter == pending_questions.end())
	std::abort();
    delete iter->second;
    pending_questions.erase(iter);
    stored_results[k] = Kom_res<typename Q::result_type>(err);
}


template <class Q, class Key>
void
Session::Prefetchable_static_info_map<Q, Key>::clear_cache()
{
    stored_results.clear();
    delete_contents_map(pending_questions);
    pending_questions.clear();
}


template <class Q, class Key>
void
Session::Prefetchable_static_info_map<Q, Key>::clear_cache_entry(Key k)
{
    typename res_map_type::iterator iter(stored_results.find(k));
    if (iter != stored_results.end())
	stored_results.erase(iter);
}


// ================================================================ 
//                    Constructors and Destructors                  


Session::Session(Connection *connection,
		 const LyStr &client_name,
		 const LyStr &client_version) 
    : conn(connection),
      logged_in_person(0),
      session_number(0),
      all_confs(NULL),
      all_persons(NULL),
      all_maps(NULL),
      all_created_maps(NULL),
      all_texts(NULL),
      my_user_area(NULL),
      all_memberships(NULL),
      previous_user_active_time(0),
      pending_user_active(NULL),
      collate_table(),
      ignored_questions()
{
    assert(connection != NULL);

    if (conn->get_server_info()->version() >= 10400)
    {
	question *q;
	q = new set_client_version_question(conn, client_name, client_version);
	ignored_questions.push_back(q);
    }

    m_async_dispatcher = new Async_dispatcher(connection);
}
 

Session::~Session()
{
    if (my_user_area != NULL)
	delete my_user_area;
    delete_contents(ignored_questions);
    if (all_confs != NULL)
	delete all_confs;
    if (all_persons != NULL)
	delete all_persons;
    if (all_maps != NULL)
	delete all_maps;
    if (all_created_maps != NULL)
	delete all_created_maps;
    if (all_texts != NULL)
	delete all_texts;
    if (all_memberships != NULL)
	delete all_memberships;
    if (m_async_dispatcher != NULL)
	delete m_async_dispatcher;
}


// ================================================================ 
//

//void
//Session::set_connection(Connection *connection)
//{
//    conn = connection;
//    //+++ Reset confs, persons, maps and texts here.
//}


// ================================================================


Conferences & 
Session::conferences()
{
    assert(conn != NULL);

    if (all_confs == NULL) {
	all_confs = new Conferences(conn, m_async_dispatcher);
    }

    return *all_confs;
}


Persons &
Session::persons()
{
    if (all_persons == NULL) {
	all_persons = new Persons(conferences());
    }

    return *all_persons;
}


Maps<Text_mapping> &
Session::maps()
{
    if (all_maps == NULL) {
	all_maps = new Maps<Text_mapping>(conn, m_async_dispatcher);
    }

    return *all_maps;
}


Maps<Created_mapping> &
Session::created_maps()
{
    if (all_created_maps == NULL) {
	all_created_maps = new Maps<Created_mapping>(conn, m_async_dispatcher);
    }

    return *all_created_maps;
}


Texts &  
Session::texts()
{
    assert(conn != NULL);

    if (all_texts == NULL) {
	all_texts = new Texts(conn, m_async_dispatcher);
    }

    return *all_texts;

}


// ----------------------------------------------------------------

Kom_res<Pers_no>
Session::create_person(const LyStr &name,
		       const LyStr &password,
		       const Personal_flags &flags,
		       const std::vector<Aux_item> &aux_items)
{
    create_person_question q(conn, name, password, flags, aux_items);
    return q_result(&q);
}

Status
Session::login(Pers_no person, const LyStr &passwd, bool invisible)
{
    login_question in(conn, person, passwd, invisible);

    clear_caches();

    Status ret = in.receive();
    if (ret == st_ok)
	logged_in_person = person;
    else
	logged_in_person = 0;

    return ret;
}

void
Session::clear_caches()
{
    static_session_info.clear_cache();
    // collate_table need not be cleared.
    // version_info need not be cleared.

    if (my_user_area != NULL)
    {
	delete my_user_area;
	my_user_area = NULL;
    }

    // FIXME: Delete all other caches as well.
}


Status
Session::logout()
{
    if (logged_in_person != 0)
    {
	clear_caches();
	logout_question out(conn);
	logged_in_person = 0;
	return out.receive();
    }
    else
	return st_ok;
}
    
Status
Session::enable(unsigned char level)
{
    enable_question q(conn, level);
    // FIXME: prune caches if the level changes.  Remember the level
    // and do nothing unless it changes.
    return q.receive();
}



Status
Session::add_member(Conf_no cno, 
		    Pers_no reader,
		    unsigned char priority, 
		    unsigned short placement,
		    Membership_type type)
{
    add_member_question q(conn, cno, reader, priority, placement, type);

    if (all_memberships == NULL)
	all_memberships = new Membership_cache(conn);

    if (all_memberships->present_or_pending(reader, cno))
	all_memberships->get(reader, cno).invalidate();

    return q.receive();
}

Kom_err
Session::sub_member(Conf_no cno, 
		    Pers_no member)
{
    sub_member_question q(conn, cno, member);

    if (all_memberships != NULL
	&& all_memberships->present_or_pending(member, cno))
	all_memberships->get(member, cno).set(Membership(KOM_NOT_MEMBER));

    return q.error_blocking();
}

Status
Session::send_message(Pers_no recipient, 
		      const LyStr &message)
{
    // FIXME: Loses error code.
    send_message_question q(conn, recipient, message);
    return q.receive();
}

struct std::tm
Session::get_time()
{
    get_time_question   timeq(conn);
    Status              stat = timeq.receive();

    if (stat == st_ok)
	return timeq.result();
    else
	std::abort();		// +++ Error handling!!
}


Status
Session::change_what_i_am_doing(const LyStr &str)
{
    check_ignored();
    // FIXME: should the result maybe be ignored, so that no ping-pong
    // effect exists here?
    change_what_i_am_doing_question  doing(conn, str);

    return doing.receive();
}


std::vector<Who_info_ident>
Session::who_is_on()
{
    who_is_on_ident_question   who_q(conn);
    
    if (who_q.receive() == st_ok)
	return who_q.result();
    else
	std::abort();		// +++Error handling!
}

std::vector<Dynamic_session_info>
Session::who_is_on_dynamic(bool want_visible,
			   bool want_invisible,
			   long active_last)
{
    who_is_on_dynamic_question who_q(conn,
				     want_visible,
				     want_invisible,
				     active_last);
    
    if (who_q.receive() == st_ok)
	return who_q.result();
    else
	std::abort();		// +++Error handling!
}

Server_info
Session::get_server_info()
{
    return *conn->get_server_info();
}


Kom_err
Session::set_server_info(Conf_no conf_pres_conf,
			 Conf_no pers_pres_conf,
			 Conf_no motd_conf,
			 Conf_no kom_news_conf,
			 Text_no motd_of_lyskom)
{
    set_info_question q(conn,
			conf_pres_conf,
			pers_pres_conf,
			motd_conf,
			kom_news_conf,
			motd_of_lyskom);
    
    if (q.receive() == st_ok)
	conn->invalidate_server_info();
    
    return q.error();
}


Kom_err
Session::set_presentation(Conf_no cno, Text_no tno)
{
    set_presentation_question  q(conn, cno, tno);

    if (q.receive() == st_ok)
    {	// FIXME: Update the presentation field in the local cache.
    }

    return q.error();
}


Status
Session::set_motd_of_lyskom(Text_no motd)
{
    set_motd_of_lyskom_question  q(conn, motd);

    return q.receive();
}


Kom_res<Text_no>
Session::get_last_text(const struct std::tm &tme)
{
    get_last_text_question q(conn, tme);
    return q_result(&q);
}

bool
Session::is_supervisor(Pers_no supervisor, Conf_no cno)
{
    if (supervisor == 0)
	return false;

    if (supervisor == cno)
	return true;

    Conference conf = conferences()[cno];
    if (!conf.exists())
	return false;

    if (conf.supervisor() == 0)
	return false;

    if (conf.supervisor() == supervisor)
	return true;

    if (is_member(supervisor, conf.supervisor()))
	return true;

    return false;
}

bool
Session::is_member(Pers_no person, Conf_no conf)
{
    if (all_memberships == NULL)
	all_memberships = new Membership_cache(conn);

    Membership m(all_memberships->get(person, conf));
    return m.kom_errno() == KOM_NO_ERROR;
}

Status
Session::prefetch_is_member(Pers_no person, Conf_no conf)
{
    if (all_memberships == NULL)
	all_memberships = new Membership_cache(conn);

    return all_memberships->prefetch(person, conf);
}

std::vector<Pers_no>
Session::get_members(Conf_no cno, 
		     unsigned short first, 
		     unsigned short no_of_members)
{
    get_members_old_question   membersq(conn, cno, first, no_of_members);

    if (membersq.receive() == st_ok) 
	return membersq.result();
    else
	std::abort();		// +++Error handling!

}

Membership
Session::get_membership(Pers_no person, Conf_no conf)
{
    if (all_memberships == NULL)
	all_memberships = new Membership_cache(conn);

    Membership m(all_memberships->get(person, conf));
    return m;
}


Status
Session::prefetch_membership(Pers_no person, Conf_no conf)
{
    if (all_memberships == NULL)
	all_memberships = new Membership_cache(conn);

    return all_memberships->prefetch(person, conf);
}


bool
Session::prefetch_membership_pending(Pers_no person, Conf_no conf)
{
    if (all_memberships == NULL)
	all_memberships = new Membership_cache(conn);

    return all_memberships->prefetch_pending(person, conf);
}


bool
Session::membership_present(Pers_no person, Conf_no conf)
{
    if (all_memberships == NULL)
	all_memberships = new Membership_cache(conn);

    return all_memberships->present(person, conf);
}


Kom_err
Session::set_last_read(Conf_no cno,
		       Local_text_no last_text_read)
{
    if (is_true(server_supports_set_last_read)
	|| (!is_false(server_supports_set_last_read)
	    && conn->get_server_info()->version() >= 10800))
    {
	// Attempt to use this new call.
	set_last_read_question q(conn, cno, last_text_read);
	if (q.receive() != st_error || q.error() != KOM_NOT_IMPL)
	{
	    server_supports_set_last_read = true;
	    // The server apparently supported it.
	    return q.error();
	}
    }
    
    server_supports_set_last_read = false;
    // Fallback: use an older question that requries more preprocessing.
    Conference conf = conferences()[cno];
    if (conf.exists()) 
    {
	Local_text_no highest = conf.first_local_no() + conf.no_of_texts() - 1;
	int wanted = highest - last_text_read;
	if (wanted < 0)
	    wanted = 0;
	set_unread_question fallback(conn, cno, wanted);
	if (fallback.receive() == st_ok)
	    return KOM_NO_ERROR;
	else
	    return fallback.error();
    }
    else
    {
	return conf.kom_errno();
    }
}

Kom_err
Session::set_membership_type(Pers_no member,
			     Conf_no cno,
			     Membership_type type)
{
    set_membership_type_question q(conn, member, cno, type);

    if (q.receive() == st_ok
	&& all_memberships->present_or_pending(member, cno))
    {
	Membership m(all_memberships->get(member, cno));
	if (m.kom_errno() == KOM_NO_ERROR)
	    m.set_membership_type(type);
	else
	    m.invalidate();
    }

    return q.error();
}

std::vector<Micro_conf>
Session::lookup_name(const LyStr &pattern)
{
    lookup_name_question   lookupq(conn, pattern);

    if (lookupq.receive() == st_ok) 
	return lookupq.result();
    else
	std::abort();		// +++Error handling!
}

void
Session::user_active()
{
    // Receive any pending answer to a previous user-active question.
    if (pending_user_active != NULL
	&& pending_user_active->status() != st_pending)
    {
	if (pending_user_active->status() == st_error
	    && (pending_user_active->error() == KOM_NOT_IMPL
		|| pending_user_active->error() == KOM_OBSOLETE))
	{
	    server_supports_user_active = false;
	}

	delete pending_user_active;
	pending_user_active = NULL;
    }

    if (is_true(server_supports_user_active)
	|| (!is_false(server_supports_user_active)
	    && conn->get_server_info()->version() >= 10900))
    {
	if (pending_user_active == NULL
	    && !is_false(server_supports_user_active))
	{
	    time_t now = time(NULL);

	    if (previous_user_active_time == 0 ||
		now - previous_user_active_time > 30)
	    {
		server_supports_user_active = true;
		previous_user_active_time = now;
		pending_user_active = new user_active_question(conn);
	    }
	}
    }
}

LyStr
Session::get_collate_table()
{
    if (!collate_table.fetched())
    {
	if (collate_table.question() == NULL)
	    collate_table.store_question(new get_collate_table_question(conn));

	if (collate_table.receive() != st_ok)
	{
	    if (collate_table.question()->error() == KOM_NOT_IMPL)
	    {
		// Fallback.  Assume the server is using ASCII, and thus
		// use an empty collate table.  All modern servers support
		// this method.
		collate_table.finalize_error();
	    }
	    else
	    {
		// FIXME: better error handling.
		std::abort();
	    }
	}
    }
    return collate_table.result();
}

Status
Session::prefetch_collate_table()
{
    if (collate_table.fetched())
	return st_ok;

    if (collate_table.question() == NULL)
	collate_table.store_question(new get_collate_table_question(conn));

    return collate_table.question()->status();
}

Version_info
Session::get_version_info()
{
    if (!version_info.fetched())
    {
	if (version_info.question() == NULL)
	    version_info.store_question(new get_version_info_question(conn));

	if (version_info.receive() != st_ok)
	{
	    if (version_info.question()->error() == KOM_NOT_IMPL)
	    {
		version_info.store_result(
		    Version_info(0, LyStr("ancient"), LyStr("ancient")));
		version_info.finalize_error();
	    }
	    else 
	    {
		// FIXME: better error handling.
		std::abort();
	    }
	}
    }
    return version_info.result();
}

Status
Session::prefetch_version_info()
{
    if (version_info.fetched())
	return st_ok;

    if (version_info.question() == NULL)
	version_info.store_question(new get_version_info_question(conn));

    return version_info.question()->status();
}

std::vector<Aux_tag>
Session::query_predefined_aux_items()
{
    if (!predefined_aux_items.fetched())
    {
	if (predefined_aux_items.question() == NULL)
	    predefined_aux_items.store_question(
		new query_predefined_aux_items_question(conn));

	if (predefined_aux_items.receive() != st_ok)
	{
	    if (predefined_aux_items.question()->error() == KOM_NOT_IMPL)
	    {
		// No aux-item support => no predefined aux-items.
		predefined_aux_items.store_result(std::vector<Aux_tag>());
		predefined_aux_items.finalize_error();
	    }
	    else 
	    {
		// FIXME: better error handling.
		std::abort();
	    }
	}
    }
    return predefined_aux_items.result();
}

Status
Session::prefetch_predefined_aux_items()
{
    if (predefined_aux_items.fetched())
	return st_ok;

    if (predefined_aux_items.question() == NULL)
	predefined_aux_items.store_question(
	    new query_predefined_aux_items_question(conn));

    return predefined_aux_items.question()->status();
}

Kom_res<Static_session_info>
Session::get_static_session_info(Session_no s)
{
    if (!static_session_info.fetched(s))
    {
	if (static_session_info.question(s) == NULL)
	    static_session_info.store_question(
		s, new get_static_session_info_question(conn, s));

	if (static_session_info.receive(s) != st_ok)
	{
	    static_session_info.finalize_error(
		s, static_session_info.question(s)->error());
	}
    }
    return static_session_info.result(s);
}

Status
Session::prefetch_static_session_info(Session_no s)
{
    if (static_session_info.fetched(s))
	return st_ok;

    if (static_session_info.question(s) == NULL)
	static_session_info.store_question(
	    s, new get_static_session_info_question(conn, s));

    return static_session_info.question(s)->status();
}


Kom_res<LyStr>
Session::get_client_name(Session_no s)
{
    if (!client_name.fetched(s))
    {
	if (client_name.question(s) == NULL)
	    client_name.store_question(
		s, new get_client_name_question(conn, s));

	if (client_name.receive(s) != st_ok)
	{
	    client_name.finalize_error(
		s, client_name.question(s)->error());
	}
    }
    Kom_res<LyStr> result(client_name.result(s));

    // Don't remember the empty string in the cache, as it can be a
    // client that will store a value in the future.
    if (result.error == KOM_NO_ERROR && result.data.empty())
	client_name.clear_cache_entry(s);

    return result;
}


Status
Session::prefetch_client_name(Session_no s)
{
    if (client_name.fetched(s))
	return st_ok;

    if (client_name.question(s) == NULL)
	client_name.store_question(
	    s, new get_client_name_question(conn, s));

    return client_name.question(s)->status();
}


Kom_res<LyStr>
Session::get_client_version(Session_no s)
{
    if (!client_version.fetched(s))
    {
	if (client_version.question(s) == NULL)
	    client_version.store_question(
		s, new get_client_version_question(conn, s));

	if (client_version.receive(s) != st_ok)
	{
	    client_version.finalize_error(
		s, client_version.question(s)->error());
	}
    }
    Kom_res<LyStr> result(client_version.result(s));

    // Don't remember the empty string in the cache, as it can be a
    // client that will store a value in the future.
    if (result.error == KOM_NO_ERROR && result.data.empty())
	client_version.clear_cache_entry(s);

    return result;
}


Status
Session::prefetch_client_version(Session_no s)
{
    if (client_version.fetched(s))
	return st_ok;

    if (client_version.question(s) == NULL)
	client_version.store_question(
	    s, new get_client_version_question(conn, s));

    return client_version.question(s)->status();
}


std::vector<Conf_z_info>
Session::lookup_z_name(const LyStr &pattern,
		       bool want_persons,
		       bool want_confs)
{
    lookup_z_name_question   lookupq(conn, pattern, want_persons, want_confs);

    if (lookupq.receive() == st_ok) 
	return lookupq.result();
    else
	std::abort();		// +++Error handling!
}


std::vector<Conf_z_info>
Session::re_z_lookup(const LyStr &pattern,
		     bool want_persons,
		     bool want_confs)
{
    re_z_lookup_question   lookupq(conn, pattern, want_persons, want_confs);

    if (lookupq.receive() == st_ok) 
	return lookupq.result();
    else
	std::abort();		// +++Error handling!
}


std::vector<Pers_no>
Session::regexp_lookup_persons(const LyStr &pattern)
{
    re_lookup_person_question   lookupq(conn, pattern);

    if (lookupq.receive() == st_ok) 
	return lookupq.result();
    else
	std::abort();		// +++Error handling!
}


std::vector<Mark>
Session::get_marks()
{
    get_marks_question  markq(conn);

    if (markq.receive() == st_ok) 
	return markq.result();
    else
	std::abort();		// +++Error handling!
}

Kom_res<Local_text_no>
Session::last_local_before(Conf_no cno, const struct std::tm &wanted_time)
{
    Conference conf = conferences()[cno];
    if (conf.kom_errno() != KOM_NO_ERROR)
    {
	// return Kom_err<Local_text_no>(conf.kom_errno());
	return conf.kom_errno();
    }

    // FIXME: this could be rewritten more efficiently if Text_mapping
    // were a little smarter.  I could use a method that gives me the
    // next non-zero text, for instance.

    Text_mapping map = maps()[cno];

    Local_text_no lower = conf.first_local_no();
    Local_text_no higher = lower + conf.no_of_texts() - 1;
    Local_text_no diff = higher - lower;

    while (lower < higher)
    {
	Local_text_no middle = (lower + higher + 1)/2;
	Local_text_no attempt = middle;

	Text text = texts()[map[attempt]];
	while (!text.exists() && attempt < higher)
	    text = texts()[map[++attempt]];

	if (!text.exists())
	    higher = middle - 1; // No texts above middle exists.
	else
	{
	    struct std::tm entry_time = text.entry_time(cno, attempt);
	    if (struct_tm_newer(wanted_time, entry_time))
		lower = attempt;
	    else
		higher = middle - 1;
	}

	// Make sure the intervall was made smaller.
	assert(higher - lower < diff);
	diff = higher - lower;
    }

    while (lower >= conf.first_local_no())
    {
	Text found = texts()[map[lower]];
	if (found.exists() && struct_tm_newer(wanted_time,
					      found.entry_time(cno, lower)))
	    break;
	--lower;
    }

    return lower;
}

Kom_res<User_area*>
Session::get_user_area()
{
    if (my_user_area == NULL)
    {
	if (logged_in_person == 0)
	    return KOM_LOGIN;
	Texts &t = texts();
	my_user_area = new User_area(persons()[logged_in_person],
				     t.read_text_stat_cache(),
				     t.read_text_mass_cache());
    }

    return my_user_area;
}

Kom_res<Conf_no>
Session::create_conference(const LyStr &name,
			   const Conf_type &type,
			   const std::vector<Aux_item> &aux)
{
    create_conf_question q(conn, name, type, aux);
    return q_result(&q);
}


Kom_err
Session::modify_info(const std::vector<Aux_no> &del,
		     const std::vector<Aux_item> &add)
{
    modify_system_info_question q(conn, del, add);
    return q.error_blocking();
}

				     
Pers_no
Session::my_login() const
{
    return logged_in_person;
}

Session_no
Session::my_session() const
{
    if (session_number == 0)
    {
	who_am_i_question wq(conn);
	if (wq.receive() == st_ok)
	    ((Session*)this)->session_number = wq.result();
	else
	    std::abort();		// FIXME: Error handling
    }

    return session_number;
}


Async_dispatcher &
Session::async_dispatcher()
{
    return *m_async_dispatcher;
}


Status
Session::handle_async(int this_many)
{
    check_ignored();
    return m_async_dispatcher->handle_async(this_many);
}


std::vector<uint32_t>
Session::query_async()
{
    query_async_question q(conn);
    Status ret = q.receive();
    if (ret == st_ok)
	return q.result();
    else
	std::abort();		// FIXME: error handling
}


Connection *
Session::connection() const
{
    return conn;
}

struct got_reply : public std::unary_function<question*, bool> {

  bool operator () (question *q)
  { 
    if (q->status() != st_pending)
    {
	delete q;
	return true;
    }
    else
	return false;
  }
};

void
Session::check_ignored()
{
    std::vector<question*>::iterator iter = std::remove_if(
	ignored_questions.begin(), ignored_questions.end(), got_reply());
    
    ignored_questions.erase(iter, ignored_questions.end());
}
