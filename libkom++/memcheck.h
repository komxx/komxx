// Debugging object allocation and destruction
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_MEMCHECK_H
#define KOMXX_MEMCHECK_H

#include <config.h>

#if ENABLE_MEMCHECK
#  define MEMCHECK : private Memcheck
#else
#  define MEMCHECK
#endif

class Memcheck {

public:

#if ENABLE_MEMCHECK

    Memcheck();
    ~Memcheck();
    Memcheck(const Memcheck&);
    Memcheck &operator =(const Memcheck &);

#endif

    static long allocated_memchecks();
private:
    static long num_memchecks;

#if ENABLE_MEMCHECK

    enum memstat {
	INITIALIZED = 17,
	DESTRUCTED
	};
    memstat status;
    memstat *allocated_status;

#endif

};

#endif
