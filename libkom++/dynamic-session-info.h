// Dynamic session information.
//
// Copyright (C) 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_DYNAMIC_SESSION_INFO_H
#define KOMXX_DYNAMIC_SESSION_INFO_H

#include "kom-types.h"
#include "prot-a-LyStr.h"
#include "session-flags.h"

class Dynamic_session_info {
  public:
    Dynamic_session_info();
    Dynamic_session_info(const Dynamic_session_info&);
    Dynamic_session_info(prot_a_LyStr &prot_a_string);
    ~Dynamic_session_info();

    const Dynamic_session_info &operator =(const Dynamic_session_info &);

    Kom_err  parse(prot_a_LyStr &prot_a_string);

    Session_no    session()      const;
    Pers_no       pers_no()      const;
    Conf_no       working_conf() const;
    long          idle_time()    const;
    Session_flags flags()        const;
    LyStr         doing()        const;
  private:
    Session_no          session_number;
    Pers_no		person;
    Conf_no		working_conference;
    long                idle;
    Session_flags       flgs;
    LyStr		what_am_i_doing;
};
#endif
