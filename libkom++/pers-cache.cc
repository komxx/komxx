// Fetch-on-demand cache of persons.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>

#include "pers-cache.h"
#include "question.h"
#include "misc.h"
#include "error-codes.h"
#include "get_person_stat_q.h"

static Pers_stat zero_pers_stat(KOM_CONF_ZERO);

Pers_cache::Pers_cache(Connection *c)
  : conn(c)
{
}

Pers_cache::~Pers_cache()
{
}

bool
Pers_cache::present(Pers_no cno) const
{
    return statusmap.find(cno) != statusmap.end();
}

Status
Pers_cache::prefetch(Pers_no pno)
{
    if (pno <= 0)
	return st_error;

    Ppmap::iterator status_iter = statusmap.find(pno);
    if (status_iter != statusmap.end())
    {
	if ((*status_iter).second.kom_errno() == KOM_NO_ERROR)
	    return st_ok;
	else
	    return st_error;
    }
    else
    {
	Ppreqmap::iterator req_iter = pendingmap.find(pno);
	if (req_iter == pendingmap.end())
	{
	    pendingmap[pno] = new get_person_stat_question(conn, pno);
	    return st_pending;
	}
	else
	{
	    switch ((*req_iter).second->status())
	    {
	    case st_pending:
		return st_pending;
	    case st_ok:
		statusmap[pno] = (*req_iter).second->result();
		delete (*req_iter).second;
		pendingmap.erase(req_iter);
		return st_ok;
	    case st_error:
		statusmap[pno] = Pers_stat((*req_iter).second->error());
		delete (*req_iter).second;
		pendingmap.erase(req_iter);
		return st_error;
	    }
	}
    }
    std::abort();
}

bool
Pers_cache::prefetch_pending(Pers_no pno)
{
    if (pno <= 0)
	return false;

    if (pendingmap.find(pno) == pendingmap.end())
	return false;

    return prefetch(pno) == st_pending;
}


Pers_stat
Pers_cache::get(Pers_no pno)
{
    if (pno <= 0)
	return zero_pers_stat;

    Ppmap::iterator status_iter = statusmap.find(pno);
    if (status_iter != statusmap.end())
	return (*status_iter).second;
    else
    {
	Ppreqmap::iterator req_iter = pendingmap.find(pno);
	if (req_iter != pendingmap.end())
	{
	    Pers_stat res;
	    get_person_stat_question *gtr = (*req_iter).second;
	    if (gtr->receive() == st_ok)
		res = gtr->result();
	    else
		res = Pers_stat(gtr->error());
	    statusmap[pno] = res;
	    delete gtr;
	    pendingmap.erase(req_iter);
	    return res;
	}
	else		
	{
	    // Not present in cache, and no outstanding question.
	    Pers_stat res;
	    get_person_stat_question gt(conn, pno);
	    if (gt.receive() == st_ok)
		res = gt.result();
	    else
		res = Pers_stat(gt.error());
	    statusmap[pno] = res;
	    return res;
	}
    }
    std::abort();
}
