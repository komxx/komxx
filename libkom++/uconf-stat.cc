// A UConf-status class.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>
#include <cassert>

#include "error-codes.h"
#include "time-utils.h"
#include "kom-types.h"
#include "uconf-stat.h"
#include "prot-a-LyStr.h"


// ================================================================
//                    Conf_stat_internal methods


UConf_stat::UConf_stat_internal::UConf_stat_internal()
    : this_conf_no(0),
      kom_errno(KOM_NO_ERROR),
      type(),
      nice(77),
      conf_name(),
      highest_local_no(0)
{
}


UConf_stat::UConf_stat_internal::UConf_stat_internal(Conf_no conf_no,
						     prot_a_LyStr &prot_a_str)
    : this_conf_no(conf_no), kom_errno(KOM_NO_ERROR)
{
    LyStr_errno cname = prot_a_str.stos();
    kom_errno = cname.kom_errno;
    conf_name = cname.str;
    type = parse_conf_type(prot_a_str);
    highest_local_no = prot_a_str.stol();
    nice = prot_a_str.stol();
}


UConf_stat::UConf_stat_internal::UConf_stat_internal(const UConf_stat_internal &c)
    : this_conf_no(c.this_conf_no)
    , kom_errno(c.kom_errno)
    , type(c.type)
    , nice(c.nice)
    , conf_name(c.conf_name)
    , highest_local_no(c.highest_local_no)
{
}


const UConf_stat::UConf_stat_internal &
UConf_stat::UConf_stat_internal::operator =(const UConf_stat::UConf_stat_internal
					  &c)
{
    refcount = c.refcount;
    kom_errno = c.kom_errno;
    this_conf_no = c.this_conf_no;
    type = c.type;
    nice = c.nice;
    conf_name = c.conf_name;
    highest_local_no = c.highest_local_no;

    return *this;
}


// ================================================================
//                         UConf_stat methods

UConf_stat::UConf_stat()
{
    csi = NULL;
}


UConf_stat::UConf_stat(const UConf_stat &conf_stat)
{
    if (conf_stat.csi != NULL)
    {
	conf_stat.csi->refcount++;
    }
    csi = conf_stat.csi;
}


UConf_stat::UConf_stat(Conf_no conf_no, prot_a_LyStr &prot_a_str)
{
    csi = new UConf_stat_internal(conf_no, prot_a_str);
    csi->refcount = 1;
}

UConf_stat::UConf_stat(Kom_err errno)
{
    csi = new UConf_stat_internal;
    csi->refcount = 1;
    csi->kom_errno = errno;
}

UConf_stat::~UConf_stat()
{
    if (csi != NULL) 
    {
	csi->refcount--;
	if (csi->refcount == 0) 
	{
	    delete csi;
	}
    }
}


UConf_stat &
UConf_stat::operator=(const UConf_stat &conf_stat)
{
    if (&conf_stat != this) 
    {
	// Delete the old UConf_stat_internal if this was the last reference.
	if (csi != NULL)
	{
	    csi->refcount--;
	    if (csi->refcount == 0) 
	    {
		delete csi;
	    }
	}
	
	csi = conf_stat.csi;
	if (csi != NULL)
	    csi->refcount++;
    }

    return *this;
}


bool
UConf_stat::valid() const
{
    return (bool) (csi != NULL && csi->kom_errno == 0);
}

Kom_err
UConf_stat::kom_errno() const
{
    if (csi == NULL)
	return KOM_UNINITIALIZED;
    else
	return csi->kom_errno;
}



Conf_type	 	
UConf_stat::type() const
{
    assert(valid());
    return csi->type;
}

Garb_nice	        
UConf_stat::nice() const
{
    assert(valid());
    return csi->nice;
}

LyStr
UConf_stat::name() const
{
    assert(valid());
    return csi->conf_name;
}

Local_text_no	
UConf_stat::highest_local_no() const
{
    assert(valid());
    return csi->highest_local_no;
}

void
UConf_stat::set_type(Conf_type conf_type)
{
    assert(valid());
    csi->type = conf_type;
}


void
UConf_stat::change_name(const LyStr &new_name)
{
    assert(valid());
    csi->conf_name = new_name;
}

void
UConf_stat::inform_text_exists(Local_text_no lno)
{
    // FIXME: it should be possible to cache the "known" range of
    // lno:s even if the conf-stat is not yet fetched.  That may
    // not be very useful, though, and probably rather complicated.
    assert(valid());
    if (lno > csi->highest_local_no)
	csi->highest_local_no = lno;
}
