// Fetch-on-demand cache for texts.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_TEXT_CACHE_H
#define KOMXX_TEXT_CACHE_H

#include <map>

#include "kom-types.h"
#include "misc.h"
#include "async_dispatcher.h"
#include "text-stat.h"

class Connection;
class get_text_stat_question;

class Text_stat_cache {
  public:
    Text_stat_cache(Connection *c, Async_dispatcher *async_disp);
    Text_stat_cache(const Text_stat_cache&);
    Text_stat_cache& operator=(const Text_stat_cache&);
    ~Text_stat_cache();

    // Methods working on the entire cache:
    bool        present(Text_no tno) const;
    void        clear();
    Connection *connection() const;

    // Methods working on individual text-stats.
    Status      prefetch(Text_no tno);
    bool        prefetch_pending(Text_no tno);
    Text_stat   get(Text_no tno);
    void        remove(Text_no tno);

    // The cache automatically registers this callback method with the
    // Async_dispatcher.  This method should not be used in any other
    // way.
    void        new_text(const Async_text_created &msg);

  private:
    typedef std::map<Text_no, Text_stat> Tstmap;
    typedef std::map<Text_no, get_text_stat_question*> Tstreqmap;

    Connection* conn;
    Async_dispatcher *async_disp;
    Async_handler_tag<Async_text_created> cr_tag;
    
    Tstmap      statusmap;
    Tstreqmap   pendingmap;
};

#endif
