// For some reason, gcc-2.95.2 does not provide this template from <iterator>,
// so it is inlined here.

#ifndef NEED_STRUCT_ITERATOR
#  error why did you include me?
#endif

#ifndef KOMXX_STRUCT_ITERATOR_H
#define KOMXX_STRUCT_ITERATOR_H

template <class C, class T, class D, class P, class R>
struct iterator {
  typedef C iterator_category;
  typedef T value_type;
  typedef D difference_type;
  typedef P pointer;
  typedef R reference;
};

#endif
