// A mark object.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_MARK_H
#define KOMXX_MARK_H

#include "kom-types.h"

class Mark {
  public:
    Mark();
    Mark(const Mark&);
    const Mark &operator =(const Mark &);

    void  set_text_no(Text_no tno);
    void  set_type(unsigned char type);

    Text_no        text_no() const;
    unsigned char  type() const;
  private:
    Text_no        tno;
    unsigned char  mark_type;
};
#endif
