// Handle a membership in a conference.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_MEMBERSHIP_H
#define KOMXX_MEMBERSHIP_H

#include <ctime>
#include <vector>

#include "kom-types.h"
#include "error-codes.h"

class prot_a_LyStr;

class Membership_type {
  public:
    Membership_type();
    Membership_type(bool invitation,
		    bool passive,
		    bool secret,
		    bool passive_message_invert,
		    bool reserved2 = false,
		    bool reserved3 = false,
		    bool reserved4 = false,
		    bool reserved5 = false);
    explicit Membership_type(prot_a_LyStr &str);
    Membership_type(const Membership_type&);
    Membership_type &operator = (const Membership_type &);
    ~Membership_type();
    
    bool invitation() const;
    bool passive() const;
    bool secret() const;
    bool passive_message_invert() const;
    bool reserved2() const;
    bool reserved3() const;
    bool reserved4() const;
    bool reserved5() const;

    void set_invitation(bool);
    void set_passive(bool);
    void set_secret(bool);
    void set_passive_message_invert(bool);
    void set_reserved2(bool);
    void set_reserved3(bool);
    void set_reserved4(bool);
    void set_reserved5(bool);

  private:
    bool mt_invitation;
    bool mt_passive;
    bool mt_secret;
    bool mt_passive_message_invert;
    bool mt_reserved2;
    bool mt_reserved3;
    bool mt_reserved4;
    bool mt_reserved5;
};

//
// Information about a person's membership in a conference
//
class Membership {
 public:
    Membership();
    Membership(const Membership&);
    Membership(Pers_no pno, bool old, prot_a_LyStr &prot_a_str);
    Membership(Kom_err kom_errno);
    ~Membership();
    const Membership &operator =(const Membership &);

    bool   initialized() const;
    Kom_err kom_errno() const;

    Conf_no        conf_no()                        const;
    int            placement()                      const;
    unsigned char  priority()                       const;
    struct std::tm last_time_read()                 const;
    Membership_type type()                          const;

    // Retrieve information about unread texts.
    bool           is_read(Local_text_no)           const;
    Local_text_no  last_text_read()                 const;
    long	   num_read_after_last_text_read()  const;

    // Update the information about unread texts.
    void mark_as_read(Local_text_no);
    void set_last_text_read(Local_text_no);

    // Update other information.
    void set_membership_type(const Membership_type &t);

    // Force the cache to refetch this information from the server.
    void invalidate();

    // Set the internal membership to a specified state.  This will
    // affect all Membership objects that refer to the same
    // reference-counted internal structure.
    void set(Membership);
 private:
    struct Membership_internal {
	Membership_internal();
	Membership_internal(Pers_no pno, bool old, prot_a_LyStr &str);
	Membership_internal(const Membership_internal &);
	const Membership_internal & operator =(const Membership_internal &);

	int      refcount;
	Pers_no  this_pers_no;
	Kom_err  kom_errno;

	Conf_no		m_conf_no;
	unsigned char	m_priority; // Interrupt priority 
	struct std::tm	m_last_time_read;   // Updated every time a text in 
	                                    // this conf. is marked as read.
	Local_text_no	m_last_text_read;   // All texts before and
                                            // inclusive this are read.
	std::vector<Local_text_no> read_texts; // Texts after last_text_read.
				            // Sorted in ascending order. 
	Membership_type m_type;
    };

    Membership_internal * memb_int;
};
#endif
