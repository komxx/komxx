// A micro-conf object.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "micro-conf.h"

Micro_conf::Micro_conf()
    : no(0), type()
{
}

Micro_conf::Micro_conf(const Micro_conf &m)
    : no(m.no), type(m.type)
{
}

const Micro_conf &
Micro_conf::operator =(const Micro_conf &m)
{
    no = m.no;
    type = m.type;
    return *this;
}


void
Micro_conf::parse_conf_no(prot_a_LyStr &str)
{
    no = str.stol();
}

void
Micro_conf::parse_conf_type(prot_a_LyStr &str)
{
    type = ::parse_conf_type(str); // +++This should be a member function!
}


Conf_no
Micro_conf::conf_no() const
{
    return no;
}
 
Conf_type
Micro_conf::conf_type() const
{
    return type;
}

