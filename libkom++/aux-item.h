// A aux-item class.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_AUX_ITEM_H
#define KOMXX_AUX_ITEM_H

#include <ctime>
#include "kom-types.h"
#include "LyStr.h"

class prot_a_LyStr;

class Aux_item_flags {
  public:
    friend Aux_item_flags parse_aux_item_flags(prot_a_LyStr &);
    Aux_item_flags();
    Aux_item_flags(bool deleted,
                   bool inherit,
                   bool secret,
                   bool hide_creator,
                   bool dont_garb,
                   bool reserved2,
                   bool reserved3,
                   bool reserved4);
    // The compiler-generated copy constructor and assignment
    // operators work just fine for this class.

    bool deleted() const;
    bool inherit() const;
    bool secret() const;
    bool hide_creator() const;
    bool dont_garb() const;
    bool reserved2() const;
    bool reserved3() const;
    bool reserved4() const;

    void set_deleted(bool);
    void set_inherit(bool);
    void set_secret(bool);
    void set_hide_creator(bool);
    void set_dont_garb(bool);
    void set_reserved2(bool);
    void set_reserved3(bool);
    void set_reserved4(bool);
    
  private:
    bool aif_deleted;
    bool aif_inherit;
    bool aif_secret;
    bool aif_hide_creator;
    bool aif_dont_garb;
    bool aif_reserved2;
    bool aif_reserved3;
    bool aif_reserved4;
};
    

class Aux_item {
  public:
    friend Aux_item parse_aux_item(prot_a_LyStr &);

    Aux_item();
    Aux_item(const Aux_item&);
    Aux_item(Aux_tag tag,
	     Aux_item_flags flags,
	     uint32_t inherit_limit,
	     LyStr data);

    Aux_item &operator =(const Aux_item &);

    Aux_no aux_no() const;
    Aux_tag tag() const;
    Pers_no creator() const;
    struct std::tm created_at() const;
    Aux_item_flags flags() const;
    uint32_t inherit_limit() const;
    LyStr data() const;

    void set_tag(Aux_tag);
    void set_flags(const Aux_item_flags &);
    void set_inherit_limit(uint32_t );
    void set_data(const LyStr &);

 private:
    Aux_no           ax_aux_no;
    Aux_tag          ax_tag;
    Pers_no          ax_creator;
    struct std::tm   ax_created_at;
    Aux_item_flags   ax_flags;
    uint32_t         ax_inherit_limit;
    LyStr            ax_data;
};

Aux_item_flags parse_aux_item_flags(prot_a_LyStr &prot_a_str);
Aux_item       parse_aux_item(prot_a_LyStr &prot_a_str);

#endif
