// Parse a time according to protocol A spec.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_TIME_UTILS_H
#define KOMXX_TIME_UTILS_H

#include <ctime>

class prot_a_LyStr;

struct std::tm parse_time(prot_a_LyStr &prot_a_str);

// Return true if A is newer than B.
bool struct_tm_newer(const struct std::tm &a, const struct std::tm &b);

#endif
