// A conf-z-info object.
//
// Copyright (C) 1995  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>

#include "conf-z-info.h"


Conf_z_info
parse_conf_z_info(prot_a_LyStr &s)
{
    Conf_z_info res;
    LyStr_errno tmp = s.stos();
    assert(tmp.kom_errno == KOM_NO_ERROR);
    res.name = tmp.str;
    res.type = parse_conf_type(s);
    res.no = s.stol();
    return res;
}


Conf_z_info::Conf_z_info()
    : name(),
      type(),
      no(0)
{
}


Conf_z_info::Conf_z_info(const Conf_z_info &c)
    : name(c.name),
      type(c.type),
      no(c.no)
{
}

const Conf_z_info &
Conf_z_info::operator =(const Conf_z_info &c)
{
    name = c.name;
    type = c.type;
    no = c.no;

    return *this;
}

Conf_no     
Conf_z_info::conf_no() const
{
    return no;
}

Conf_type   
Conf_z_info::conf_type() const
{
    return type;
}

LyStr       
Conf_z_info::conf_name() const
{
    return name;
}

