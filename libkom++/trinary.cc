// Trinary logic.
//
// Copyright (C) 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "trinary.h"

trinary::trinary()
  : value(tri_unknown)
{
}


trinary::trinary(const trinary &t)
  : value(t.value)
{
}

trinary::trinary(bool b)
  : value(b ? tri_true : tri_false)
{
}

trinary::trinary(trinary::val v)
  : value(v)
{
}

trinary::~trinary()
{
}

trinary & 
trinary::operator =(const trinary &t)
{
    value = t.value;
    return *this;
}

trinary
trinary::operator &(const trinary &t)
{
    if (value == tri_false || t.value == tri_false)
	return tri_false;
    else if (value == tri_true && t.value == tri_true)
	return tri_true;
    else
	return tri_unknown;
}

trinary
trinary::operator |(const trinary &t)
{
    if (value == tri_true || t.value == tri_true)
	return tri_true;
    else if (value == tri_false && t.value == tri_false)
	return tri_false;
    else
	return tri_unknown;
}

trinary
trinary::operator ^(const trinary &t)
{
    if (value == tri_unknown || t.value == tri_unknown)
	return tri_unknown;
    else if (value == t.value)
	return tri_false;
    else
	return tri_true;
}

bool 
is_true(const trinary &t)
{
    return t.value == trinary::tri_true;
}

bool 
is_false(const trinary &t)
{
    return t.value == trinary::tri_false;
}

bool 
is_unknown(const trinary &t)
{
    return t.value == trinary::tri_unknown;
}
