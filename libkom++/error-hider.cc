// Template class for simple error management.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "error-hider.h"

template <class T>
Error_hider<T>::Error_hider(T result,
			 int errcode)
    : res(result), e_no(errcode)
{
}

template <class T>
Error_hider<T>::Error_hider(const Error_hider &e)
: res(e.res), e_no(e.e_no)
{
}


template <class T>
Error_hider<T>::operator =(const Error_hider &e) const
{
    res = e.res;
    e_no = e.e_no;
}

template <class T>
Error_hider<T>::operator T() const
{
    return T;
}

template <class T> int
Error_hider<T>::status() const
{
    return e_no;
}
