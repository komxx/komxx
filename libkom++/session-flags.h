// Session flags.
//
// Copyright (C) 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_SESSION_FLAGS_H
#define KOMXX_SESSION_FLAGS_H

class prot_a_LyStr;

/*
 * Flags in the Dynamic_session struct.
 */
class Session_flags {
  public:
    Session_flags();
    // Use compiler-generated copy constructor and copy assignment.

    friend Session_flags parse_session_flags(prot_a_LyStr &prot_a_str);

    bool invisible() const;
    bool user_active_used() const;
    bool user_absent() const;
    bool reserved3() const;
    bool reserved4() const;
    bool reserved5() const;
    bool reserved6() const;
    bool reserved7() const;
  private:
    bool df_invisible;
    bool df_user_active_used;
    bool df_user_absent;
    bool df_reserved3;
    bool df_reserved4;
    bool df_reserved5;
    bool df_reserved6;
    bool df_reserved7;
};

Session_flags parse_session_flags(prot_a_LyStr &prot_a_str);
#endif
