// Text status objects.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_TEXT_STAT_H
#define KOMXX_TEXT_STAT_H

#include <ctime>

#include "error-codes.h"
#include "kom-types.h"
#include "recipient.h"
#include "memcheck.h"

class Uplink;
class prot_a_LyStr;
class Aux_item;

/*  Struct for text status  */
class Text_stat MEMCHECK {
  public:
    Text_stat();
    Text_stat(const Text_stat&);
    Text_stat(Text_no tno, bool old, prot_a_LyStr &prot_a_string);
    Text_stat(Kom_err kom_errno);
    Text_stat &operator =(const Text_stat &);
    ~Text_stat();

    Kom_err     kom_errno()	 const;

    Text_no     text_no()        const;
    struct std::tm creation_time()  const;
    Pers_no     author()         const;
    long        num_lines()      const;
    long        num_chars()      const;
    int         num_marks()      const;
    int         num_recipients() const;
    const Recipient* recipient(int)   const;
    int		num_comment_in() const;
    Text_no	comment_in(int)	 const;
    int		num_footnote_in()const;
    Text_no	footnote_in(int) const;
    int		num_uplinks()	 const;
    const Uplink* uplink(int)	 const;
    int         num_aux()        const;
    Aux_item    aux(int)     const;

    void add_footnote_in(Text_no t);
    void add_comment_in(Text_no t);

    // Returns the time when this text was entered in the specified
    // conference with the specified local number.  This is most of
    // the time equal to creation_time(), but it may differ if this
    // text was added to the conference at a later time.
    struct std::tm   entry_time(Conf_no, Local_text_no);
  private:
    bool valid() const;

    struct Text_stat_internal;
    Text_stat_internal *tsi;
};
#endif
