// STL container templates.
//
// Copyright (C) 1999  Per Cederqvist
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_CONTAINER_H
#define KOMXX_CONTAINER_H

#include <functional>
#include <algorithm>

template <class T> struct delete_object : std::unary_function<T, void> {
    void operator()(T obj) { delete obj; }
};

template <class C>
void
delete_contents(C &container)
{
    std::for_each(container.begin(), container.end(), 
		  delete_object<typename C::value_type>());
    container.clear();
}

template <class T> struct delete_object_map : std::unary_function<T, void> {
    void operator()(T obj) { delete obj.second; }
};

template <class C>
void
delete_contents_map(C &container)
{
    std::for_each(container.begin(), container.end(), 
		  delete_object_map<typename C::value_type>());
    container.clear();
}

// FIXME: there should be a way to reconcile delete_contents and
// delete_contents_map.

#endif
