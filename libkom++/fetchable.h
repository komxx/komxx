// Fetchable objects inherit this virtual base class.
//
// Copyright (C) 1995  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_FETCHABLE_H
#define KOMXX_FETCHABLE_H

#include "memcheck.h"
#include "misc.h"
#include "error-codes.h"

class Fetchable MEMCHECK {
  public:
    Fetchable();
    Fetchable(const Fetchable &);
    virtual ~Fetchable();

    const Fetchable & operator = (const Fetchable &);

    // Check if the object is present in the local cache or not.
    // Don't start to fetch it.  This method will return immediately.
    virtual bool present() const =0;

    // Determine if the object exists or not.  May have to fetch the
    // object.  This method may block.
    virtual bool exists() const =0;

    // Send a request for the object (if needed) and return
    // immediately.
    virtual Status prefetch() const =0;

    // Return true if a prefetch is currently executing.  This method
    // will not start to prefetch anything.
    virtual bool prefetch_pending() const =0;

    // As exists, but return an error code explaining why the object
    // cannot be fetched.
    virtual Kom_err kom_errno() const =0;
};
#endif  // FETCHABLE_H
