// Priv_bits, Personal_flags, Who_info_old, Conf_type.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>

#include "time-utils.h"
#include "kom-types.h"
#include "priv-bits.h"
#include "personal-flags.h"
#include "who-info-old.h"
#include "conf-type.h"
#include "prot-a-LyStr.h"


static bool
parse_bin_digit(LyStr &prot_a_str)
{
    switch(prot_a_str[0])
    {
    case '0':
	prot_a_str.remove_first(1);
	return false;
    case '1':
	prot_a_str.remove_first(1);
	return true;
    default:
	std::abort();
    }
}

static trinary
parse_optional_bin_digit(LyStr &prot_a_str)
{
    switch(prot_a_str[0])
    {
    case '0':
	prot_a_str.remove_first(1);
	return false;
    case '1':
	prot_a_str.remove_first(1);
	return true;
    default:
	// Don't remove the character, which is typically a space or newline.
	return trinary();
    }
}

// The Priv_bits class

Priv_bits::Priv_bits()
  : pb_wheel(0), pb_admin(0), pb_statistic(0), pb_create_pers(1), 
    pb_create_conf(1), pb_change_name(1), 
    pb_flg7(0),
    pb_flg8(0),
    pb_flg9(0),
    pb_flg10(0),
    pb_flg11(0),
    pb_flg12(0),
    pb_flg13(0),
    pb_flg14(0),
    pb_flg15(0),
    pb_flg16(0)
{
}

Priv_bits::Priv_bits(bool wheel,
		     bool admin,
		     bool statistic,
		     bool create_pers,
		     bool create_conf,
		     bool change_name,
		     bool flg7,
		     bool flg8,
		     bool flg9,
		     bool flg10,
		     bool flg11,
		     bool flg12,
		     bool flg13,
		     bool flg14,
		     bool flg15,
		     bool flg16)
  : pb_wheel(wheel),
    pb_admin(admin),
    pb_statistic(statistic),
    pb_create_pers(create_pers),
    pb_create_conf(create_conf),
    pb_change_name(change_name),
    pb_flg7(flg7),
    pb_flg8(flg8),
    pb_flg9(flg9),
    pb_flg10(flg10),
    pb_flg11(flg11),
    pb_flg12(flg12),
    pb_flg13(flg13),
    pb_flg14(flg14),
    pb_flg15(flg15),
    pb_flg16(flg16)
{
}

Priv_bits::Priv_bits(const Priv_bits&s)
  : pb_wheel(s.pb_wheel),
    pb_admin(s.pb_admin),
    pb_statistic(s.pb_statistic),
    pb_create_pers(s.pb_create_pers),
    pb_create_conf(s.pb_create_conf),
    pb_change_name(s.pb_change_name),
    pb_flg7(s.pb_flg7),
    pb_flg8(s.pb_flg8),
    pb_flg9(s.pb_flg9),
    pb_flg10(s.pb_flg10),
    pb_flg11(s.pb_flg11),
    pb_flg12(s.pb_flg12),
    pb_flg13(s.pb_flg13),
    pb_flg14(s.pb_flg14),
    pb_flg15(s.pb_flg15),
    pb_flg16(s.pb_flg16)
{
}

const Priv_bits &Priv_bits::operator =(const Priv_bits &s)
{
    pb_wheel = s.pb_wheel;
    pb_admin = s.pb_admin;
    pb_statistic = s.pb_statistic;
    pb_create_pers = s.pb_create_pers;
    pb_create_conf = s.pb_create_conf;
    pb_change_name = s.pb_change_name;
    pb_flg7 = s.pb_flg7;
    pb_flg8 = s.pb_flg8;
    pb_flg9 = s.pb_flg9;
    pb_flg10 = s.pb_flg10;
    pb_flg11 = s.pb_flg11;
    pb_flg12 = s.pb_flg12;
    pb_flg13 = s.pb_flg13;
    pb_flg14 = s.pb_flg14;
    pb_flg15 = s.pb_flg15;
    pb_flg16 = s.pb_flg16;
    return *this;
}

Priv_bits
parse_priv_bits(LyStr &prot_a_str)
{
    Priv_bits res;

    while (prot_a_str.strlen() > 0 && prot_a_str[0] == ' ')
	prot_a_str.remove_first(1);

    res.pb_wheel = parse_bin_digit(prot_a_str);
    res.pb_admin = parse_bin_digit(prot_a_str);
    res.pb_statistic = parse_bin_digit(prot_a_str);
    res.pb_create_pers = parse_bin_digit(prot_a_str);
    res.pb_create_conf = parse_bin_digit(prot_a_str);
    res.pb_change_name = parse_bin_digit(prot_a_str);
    res.pb_flg7 = parse_bin_digit(prot_a_str);
    res.pb_flg8 = parse_bin_digit(prot_a_str);
    res.pb_flg9 = parse_bin_digit(prot_a_str);
    res.pb_flg10 = parse_bin_digit(prot_a_str);
    res.pb_flg11 = parse_bin_digit(prot_a_str);
    res.pb_flg12 = parse_bin_digit(prot_a_str);
    res.pb_flg13 = parse_bin_digit(prot_a_str);
    res.pb_flg14 = parse_bin_digit(prot_a_str);
    res.pb_flg15 = parse_bin_digit(prot_a_str);
    res.pb_flg16 = parse_bin_digit(prot_a_str);

    return res;
}

bool
Priv_bits::wheel() const
{
    return pb_wheel;
}

bool
Priv_bits::admin() const
{
    return pb_admin;
}

bool
Priv_bits::statistic() const
{
    return pb_statistic;
}

bool
Priv_bits::create_pers() const
{
    return pb_create_pers;
}

bool
Priv_bits::create_conf() const
{
    return pb_create_conf;
}

bool
Priv_bits::change_name() const
{
    return pb_change_name;
}

bool
Priv_bits::flg7() const
{
    return pb_flg7;
}

bool
Priv_bits::flg8() const
{
    return pb_flg8;
}

bool
Priv_bits::flg9() const
{
    return pb_flg9;
}

bool
Priv_bits::flg10() const
{
    return pb_flg10;
}

bool
Priv_bits::flg11() const
{
    return pb_flg11;
}

bool
Priv_bits::flg12() const
{
    return pb_flg12;
}

bool
Priv_bits::flg13() const
{
    return pb_flg13;
}

bool
Priv_bits::flg14() const
{
    return pb_flg14;
}

bool
Priv_bits::flg15() const
{
    return pb_flg15;
}

bool
Priv_bits::flg16() const
{
    return pb_flg16;
}

// The Personal_flags class

Personal_flags::Personal_flags()
  : pf_unread_is_secret(false),
    pf_flg2(false),
    pf_flg3(false),
    pf_flg4(false),
    pf_flg5(false),
    pf_flg6(false),
    pf_flg7(false),
    pf_flg8(false)
{
}

Personal_flags::Personal_flags(bool unread_is_secret)
  : pf_unread_is_secret(unread_is_secret),
    pf_flg2(false),
    pf_flg3(false),
    pf_flg4(false),
    pf_flg5(false),
    pf_flg6(false),
    pf_flg7(false),
    pf_flg8(false)
{
}

Personal_flags::Personal_flags(const Personal_flags&s)
  : pf_unread_is_secret(s.pf_unread_is_secret),
    pf_flg2(s.pf_flg2),
    pf_flg3(s.pf_flg3),
    pf_flg4(s.pf_flg4),
    pf_flg5(s.pf_flg5),
    pf_flg6(s.pf_flg6),
    pf_flg7(s.pf_flg7),
    pf_flg8(s.pf_flg8)
{
}

const Personal_flags &Personal_flags::operator =(const Personal_flags &s)
{
    pf_unread_is_secret = s.pf_unread_is_secret;
    pf_flg2 = s.pf_flg2;
    pf_flg3 = s.pf_flg3;
    pf_flg4 = s.pf_flg4;
    pf_flg5 = s.pf_flg5;
    pf_flg6 = s.pf_flg6;
    pf_flg7 = s.pf_flg7;
    pf_flg8 = s.pf_flg8;

    return *this;
}
bool
Personal_flags::unread_is_secret() const
{
    return pf_unread_is_secret;
}

bool
Personal_flags::flg2() const
{
    return pf_flg2;
}

bool
Personal_flags::flg3() const
{
    return pf_flg3;
}

bool
Personal_flags::flg4() const
{
    return pf_flg4;
}

bool
Personal_flags::flg5() const
{
    return pf_flg5;
}

bool
Personal_flags::flg6() const
{
    return pf_flg6;
}

bool
Personal_flags::flg7() const
{
    return pf_flg7;
}

bool
Personal_flags::flg8() const
{
    return pf_flg8;
}

Personal_flags
parse_personal_flags(LyStr &prot_a_str)
{
    Personal_flags res;

    while (prot_a_str.strlen() > 0 && prot_a_str[0] == ' ')
	prot_a_str.remove_first(1);

    res.pf_unread_is_secret = parse_bin_digit(prot_a_str);
    res.pf_flg2 = parse_bin_digit(prot_a_str);
    res.pf_flg3 = parse_bin_digit(prot_a_str);
    res.pf_flg4 = parse_bin_digit(prot_a_str);
    res.pf_flg5 = parse_bin_digit(prot_a_str);
    res.pf_flg6 = parse_bin_digit(prot_a_str);
    res.pf_flg7 = parse_bin_digit(prot_a_str);
    res.pf_flg8 = parse_bin_digit(prot_a_str);

    return res;
}

Who_info_old::Who_info_old()
    : person(0), working_conference(0)
{
}

Who_info_old::Who_info_old(const Who_info_old &w)
    : person(w.person), what_am_i_doing(w.what_am_i_doing),
      working_conference(w.working_conference)
{
}

const Who_info_old &Who_info_old::operator =(const Who_info_old &w)
{
    person = w.person;
    what_am_i_doing = w.what_am_i_doing;
    working_conference = w.working_conference;
    return *this;
}

// ================================================================


Conf_type::Conf_type()
    : rd_prot(false),
      original(false),
      secret(false),
      letter_box(false),
      allow_anonymous(true),
      forbid_secret(false),
      reserved2(false),
      reserved3(false)
{
}

Conf_type::Conf_type(const Conf_type&c)
    : rd_prot(c.rd_prot),
      original(c.original),
      secret(c.secret),
      letter_box(c.letter_box),
      allow_anonymous(c.allow_anonymous),
      forbid_secret(c.forbid_secret),
      reserved2(c.reserved2),
      reserved3(c.reserved3)
{
}

Conf_type::Conf_type(bool protect,
		     bool orig,
		     bool secr,
		     bool letterbox)
    : rd_prot(protect),
      original(orig),
      secret(secr),
      letter_box(letterbox),
      allow_anonymous(true),
      forbid_secret(false),
      reserved2(false),
      reserved3(false)
{
}


Conf_type::Conf_type(bool protect,
		     bool orig,
		     bool secr,
		     bool letterbox,
		     bool allowed_anonymous,
		     bool forbid_secr,
		     bool res2,
		     bool res3)
  : rd_prot(protect),
    original(orig),
    secret(secr),
    letter_box(letterbox),
    allow_anonymous(allowed_anonymous),
    forbid_secret(forbid_secr),
    reserved2(res2),
    reserved3(res3)
{
}


Conf_type &
Conf_type::operator =(const Conf_type &c)
{
    if (this != &c)
    {
	rd_prot = c.rd_prot;
	original = c.original;
	secret = c.secret;
	letter_box = c.letter_box;
	allow_anonymous = c.allow_anonymous;
	forbid_secret = c.forbid_secret;
	reserved2 = c.reserved2;
	reserved3 = c.reserved3;
    }

    return *this;
}

bool
Conf_type::is_protected() const
{
    return rd_prot;
}

bool
Conf_type::is_original()  const
{
    return original;
}

bool
Conf_type::is_secret()    const
{
    return secret;
}

bool
Conf_type::is_letterbox() const
{
    return letter_box;
}

trinary 
Conf_type::allows_anonymous() const
{
    return allow_anonymous;
}

trinary 
Conf_type::forbids_secret() const
{
    return forbid_secret;
}

trinary 
Conf_type::res2() const
{
    return reserved2;
}

trinary 
Conf_type::res3() const
{
    return reserved3;
}


Conf_type 
parse_conf_type(prot_a_LyStr &prot_a_str)
{
    Conf_type res;

    while (prot_a_str.strlen() > 0 && prot_a_str[0] == ' ')
	prot_a_str.remove_first(1);

    res.rd_prot = parse_bin_digit(prot_a_str);
    res.original = parse_bin_digit(prot_a_str);
    res.secret = parse_bin_digit(prot_a_str);
    res.letter_box = parse_bin_digit(prot_a_str);
    res.allow_anonymous = parse_optional_bin_digit(prot_a_str);
    res.forbid_secret = parse_optional_bin_digit(prot_a_str);
    res.reserved2 = parse_optional_bin_digit(prot_a_str);
    res.reserved3 = parse_optional_bin_digit(prot_a_str);

    return res;
}


// ================================================================


struct std::tm parse_time(prot_a_LyStr &prot_a_str)
{
    struct std::tm res;

    res.tm_sec = prot_a_str.stol();
    res.tm_min = prot_a_str.stol();
    res.tm_hour = prot_a_str.stol();
    res.tm_mday = prot_a_str.stol();
    res.tm_mon = prot_a_str.stol();
    res.tm_year = prot_a_str.stol();
    res.tm_wday = prot_a_str.stol();
    res.tm_yday = prot_a_str.stol();
    res.tm_isdst = prot_a_str.stol();

    return res;
}

bool
struct_tm_newer(const struct std::tm &a,
		const struct std::tm &b)
{
    // This code assumes that A and B are expressed in the same time zone.
    if (a.tm_year > b.tm_year)
	return true;
    if (a.tm_year < b.tm_year)
	return false;

    if (a.tm_mon > b.tm_mon)
	return true;
    if (a.tm_mon < b.tm_mon)
	return false;

    if (a.tm_mday > b.tm_mday)
	return true;
    if (a.tm_mday < b.tm_mday)
	return false;

    if (a.tm_hour > b.tm_hour)
	return true;
    if (a.tm_hour < b.tm_hour)
	return false;

    if (a.tm_min > b.tm_min)
	return true;
    if (a.tm_min < b.tm_min)
	return false;

    if (a.tm_sec > b.tm_sec)
	return true;
    if (a.tm_sec < b.tm_sec)
	return false;

    return false;
}
