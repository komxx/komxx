// Fetch-on-demand cache of persons.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_PERS_CACHE_H
#define KOMXX_PERS_CACHE_H

#include <map>

#include "kom-types.h"
#include "misc.h"
#include "pers-stat.h"

class Connection;
class get_person_stat_question;

class Pers_cache {
  public:
    Pers_cache(Connection *c);
    ~Pers_cache();

    // Methods working on the entire cache:
    bool        present(Pers_no pno) const;
    void        clear();

    // Methods working on individual pers-stats.
    Status      prefetch(Pers_no pno);
    bool        prefetch_pending(Pers_no pno);
    Pers_stat   get(Pers_no pno);
    void        remove(Pers_no pno);

  private:
    Pers_cache(const Pers_cache&);
    Pers_cache& operator=(const Pers_cache&);

    typedef std::map<Pers_no, Pers_stat> Ppmap;
    typedef std::map<Pers_no, get_person_stat_question*> Ppreqmap;

    Connection* conn;
    Ppmap       statusmap;
    Ppreqmap    pendingmap;
};

#endif
