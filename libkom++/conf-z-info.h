// A conf-z-info object.
//
// Copyright (C) 1995  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_CONF_Z_INFO_H
#define KOMXX_CONF_Z_INFO_H

#include "conf-type.h"
#include "kom-types.h"
#include "prot-a-LyStr.h"


class Conf_z_info {
  public:
    friend Conf_z_info parse_conf_z_info(prot_a_LyStr &);

    Conf_z_info();
    Conf_z_info(const Conf_z_info&);

    const Conf_z_info &operator =(const Conf_z_info &);

    LyStr       conf_name() const;
    Conf_type   conf_type() const;
    Conf_no     conf_no() const;
  private:
    LyStr     name;
    Conf_type type;
    Conf_no   no;
};

Conf_z_info parse_conf_z_info(prot_a_LyStr &s);

#endif
