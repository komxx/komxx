// Basic types used in the LysKOM system.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_KOM_TYPES_H
#define KOMXX_KOM_TYPES_H

#include <stdint.h>
#include <climits>

typedef unsigned short Pers_no;
typedef unsigned short Conf_no;
typedef unsigned long  Text_no;
typedef unsigned long  Local_text_no;
typedef unsigned long  Session_no;
typedef unsigned long  Garb_nice;
typedef uint32_t Aux_no;
typedef uint32_t Aux_tag;

#define	MAX_PERS_NO		((Pers_no) USHRT_MAX)
#define MAX_CONF_NO		((Conf_no) USHRT_MAX)
#define MAX_TEXT_NO		((Text_no) ULONG_MAX)
#define	MAX_LOCAL_TEXT_NO	((Local_text_no) ULONG_MAX)

#endif
