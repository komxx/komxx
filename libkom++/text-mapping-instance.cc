#include "local_to_global_q.h"
#include "text-mapping.cc"
#include "map-cache.cc"
#include "maps.cc"
template class Text_mapping_basic<local_to_global_question>;
template class Maps<Text_mapping>;
template class Map_cache<Text_mapping>;

template <>
void
Map_cache<Text_mapping>::new_text(const Async_text_created &msg)
{
    Text_stat t = msg.text_stat();
    for (int ix = 0; ix < t.num_recipients(); ix++)
    {
	const Recipient *recpt = t.recipient(ix);
	Text_mapping map = get(recpt->recipient());
	map.add(recpt->local_no(), t.text_no());
    }
}
