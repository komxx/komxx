// Store a mapping from local to global text-numbers.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>
#include <cstdlib>

#include "local-to-global-map.h"
#include "prot-a-LyStr.h"

local_to_global_map::local_to_global_map()
  : lg_range_begin(0),
    lg_range_end(0),
    lg_more_texts_exists(false),
    lg_mapping()
{
}

local_to_global_map::local_to_global_map(const local_to_global_map &o)
  : lg_range_begin(o.lg_range_begin),
    lg_range_end(o.lg_range_end),
    lg_more_texts_exists(o.lg_more_texts_exists),
    lg_mapping(o.lg_mapping)
{
}

local_to_global_map::local_to_global_map(prot_a_LyStr& prot_a_str)
  : lg_range_begin(prot_a_str.stol()),
    lg_range_end(prot_a_str.stol()),
    lg_more_texts_exists(false),
    lg_mapping()
{
    prot_a_str.skipspace();
    prot_a_str.parse_bit(lg_more_texts_exists);
    
    bool dense;
    prot_a_str.skipspace();
    prot_a_str.parse_bit(dense);
    if (dense)
    {
	Local_text_no lno = prot_a_str.stol();
	int arr_size = prot_a_str.parse_array_start();
	for (int i = 0; i < arr_size; ++i)
	{
	    Local_text_no gno = prot_a_str.stol();
	    lg_mapping[lno++] = gno;
	}
	prot_a_str.parse_array_end(arr_size);
    }
    else
    {
	int arr_size = prot_a_str.parse_array_start();

	for (int i = 0; i < arr_size; ++i)
	{
	    Local_text_no lno = prot_a_str.stol();
	    Local_text_no gno = prot_a_str.stol();
	    lg_mapping[lno] = gno;
	}
	prot_a_str.parse_array_end(arr_size);
    }
}

local_to_global_map::~local_to_global_map()
{
}

Local_text_no
local_to_global_map::range_begin() const
{
    return lg_range_begin;
}

Local_text_no
local_to_global_map::range_end() const
{
    return lg_range_end;
}

bool
local_to_global_map::more_texts_exists() const
{
    return lg_more_texts_exists;
}

local_to_global_map::const_iterator
local_to_global_map::begin() const
{
    return lg_mapping.begin();
}

local_to_global_map::const_iterator
local_to_global_map::end() const
{
    return lg_mapping.end();
}
