// Session flags.
//
// Copyright (C) 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "session-flags.h"
#include "prot-a-LyStr.h"

Session_flags::Session_flags()
  : df_invisible(false),
    df_user_active_used(false),
    df_user_absent(false),
    df_reserved3(false),
    df_reserved4(false),
    df_reserved5(false),
    df_reserved6(false),
    df_reserved7(false)
{
}

bool
Session_flags::invisible() const
{
    return df_invisible;
}

bool
Session_flags::user_active_used() const
{
    return df_user_active_used;
}

bool
Session_flags::user_absent() const
{
    return df_user_absent;
}

bool
Session_flags::reserved3() const
{
    return df_reserved3;
}

bool
Session_flags::reserved4() const
{
    return df_reserved4;
}

bool
Session_flags::reserved5() const
{
    return df_reserved5;
}

bool
Session_flags::reserved6() const
{
    return df_reserved6;
}

bool
Session_flags::reserved7() const
{
    return df_reserved7;
}

Session_flags
parse_session_flags(prot_a_LyStr &prot_a_str)
{
    Session_flags res;

    prot_a_str.skipspace();
    prot_a_str.parse_bit(res.df_invisible);
    prot_a_str.parse_bit(res.df_user_active_used);
    prot_a_str.parse_bit(res.df_user_absent);
    prot_a_str.parse_bit(res.df_reserved3);
    prot_a_str.parse_bit(res.df_reserved4);
    prot_a_str.parse_bit(res.df_reserved5);
    prot_a_str.parse_bit(res.df_reserved6);
    prot_a_str.parse_bit(res.df_reserved7);

    return res;
}
