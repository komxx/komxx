// A mark object.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "mark.h"

Mark::Mark()
    : tno(0), mark_type(0)
{
}

Mark::Mark(const Mark &m)
    : tno(m.tno), mark_type(m.mark_type)
{
}

const Mark &
Mark::operator =(const Mark &m)
{
    tno = m.tno;
    mark_type = m.mark_type;
    return *this;
}


void
Mark::set_text_no(Text_no text_no)
{
    tno = text_no;
}

void
Mark::set_type(unsigned char type)
{
    mark_type = type;
}


Text_no
Mark::text_no() const
{
    return tno;
}

unsigned char
Mark::type() const
{
    return mark_type;
}

