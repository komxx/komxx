// Command table handling.
//
// Copyright (C) 1994, 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#ifndef KOMXX_CMD_TABLE_ENTRY_H
#define KOMXX_CMD_TABLE_ENTRY_H

#include "LyStr.h"

class Cmd_table_entry {
public:
  Cmd_table_entry();
  Cmd_table_entry(const Cmd_table_entry &);
  Cmd_table_entry & operator = (const Cmd_table_entry &);
  Cmd_table_entry(const LyStr &cmd_template);
  ~Cmd_table_entry();

  // Returns number of penalty points, or -1 if not matching at all.
  int try_match(LyStr user_try) const;

  enum { MAXARGS = 4 };

  // The error codes are designed to be useful from inherited classes
  // who often contain an additional template, called the action
  // template (as opposed to the user template, which is given to the
  // Cmd_table_entry constructor).  The error codes which include
  // "ACTION" in their names are not actually used by the classes in
  // this file.
  enum errcode {
    NOERR = 0,
    EMPTY_TEMPLATE,
    EMPTY_CMD,
    BAD_WILDCARD,
    WILDCARD_MISMATCH,
    BAD_USER_TEMPLATE,
    UNINITIALIZED
  };
  
  errcode status();
  
private:
  enum errcode err;
  LyStr user_template;
  LyStr tcl_template;
  LyStr pretty;
};

#endif
