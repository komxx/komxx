// Fetch-on-demand cache for texts.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_TEXT_MASS_CACHE_H
#define KOMXX_TEXT_MASS_CACHE_H

#include "kom-types.h"
#include "misc.h"
#include "LyStr.h"
#include "LyStr-errno.h"
#include "error-codes.h"

class Connection;

// Text_mass_cache_internal contains STL templates.
// Since this file is included in several places, the
// templates are included only inside text-mass-cache.cc
// to shorten compile times.

class Text_mass_cache_internal;

class Text_mass_cache {
  public:
    Text_mass_cache(Connection *c, int sz = 250);
    ~Text_mass_cache();

    // Methods working on the entire cache:
    void        set_size(int sz);
    bool        present(Text_no tno);
    void        clear();
    // This method should be called when a user enables his
    // privileges, is added to a protected conference, or otherwise
    // gains more power.  It removes all cached errors, so that the
    // cache will attempt to fetch the text again.  (Normally, only
    // one attempt to fetch a given text is made).
    void	clear_errors();
    Connection *connection() const;

    // Methods working on individual texts.
    Status      prefetch(Text_no tno);
    bool        prefetch_pending(Text_no tno);

    void        remove(Text_no tno);

    // FIXME: Possibly use a get that looks like this:
    //   pair<LyStr, Kom_err>   get(Text_no tno);
    LyStr_errno get(Text_no tno);

    // Returns a formatted report in English on the cache performance.
    LyStr       stat_report() const;

  private:
    // Remove the least recently used entry in the cache.
    void lru_remove();

    // Make the selected text the most recently used.
    void mru_update(Text_no which);
    void set_mru_initial(Text_no which);

    // Make sure the cache is small enough.
    void        limit_size();

    // Check for internal data structure inconsistencies.
    void	assert_ok() const;

    Text_mass_cache_internal *stl_containers;
    Text_no     lru;
    Text_no     mru;
    Connection *conn;
    int         max_size;

    // Statistic gathering.
    long prefetch_miss;
    long prefetch_pending_cnt;
    long prefetch_hits_ok;
    long prefetch_hits_err;
    long miss_ok;
    long miss_err;
    long block_ok;
    long block_err;
    long hits_ok;
    long hits_err;

    // N/A.
    Text_mass_cache(const Text_mass_cache &); // N/A
    const Text_mass_cache & operator = (const Text_mass_cache &); // N/A
};
#endif
