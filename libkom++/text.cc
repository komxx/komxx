// Fetch-on-demand cache for texts.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <iostream>
#include <cassert>

#include "text.h"
#include "LyStr.h"
#include "person.h"
#include "conference.h"
#include "uplink.h"
#include "text-mass-cache.h"

#include "aux-item.h"
#include "get_text_q.h"
#include "mark_as_read_q.h"
#include "mark_as_unread_q.h"
#include "mark_text_q.h"
#include "unmark_text_q.h"
#include "add_recipient_q.h"
#include "sub_recipient_q.h"
#include "modify_text_info_q.h"


// ================================================================
// ================================================================
//                            Class Text


// ================================================================ 
//                    Constructors and Destructors                  


Text::Text()
{
}


Text::Text(Text_no t, 
	   Text_stat_cache *tstat_cache,
	   Text_mass_cache *tmass_cache)
    : tno(t), ts_cache(tstat_cache), tm_cache(tmass_cache)
{
}
 
Text::Text(const Text &t)
    : Fetchable(), tno(t.tno), ts_cache(t.ts_cache), tm_cache(t.tm_cache)
{
}


Text::~Text()
{
}


// ================================================================ 
//

const Text & 
Text::operator = (const Text &t)
{
    tno = t.tno;
    ts_cache = t.ts_cache;
    tm_cache = t.tm_cache;
    return *this;
}

Status
Text::prefetch() const
{
    return ts_cache->prefetch(tno);
}

bool
Text::prefetch_pending() const
{
    return ts_cache->prefetch_pending(tno);
}

Status
Text::prefetch_text() const
{
    return tm_cache->prefetch(tno);
}

Kom_err
Text::kom_errno() const
{
    return ts_cache->get(tno).kom_errno();
}

bool
Text::present() const
{
    return ts_cache->present(tno);
}

bool
Text::exists() const
{
    return kom_errno() == KOM_NO_ERROR;
}

Text_no
Text::text_no() const
{
    return tno;
}

struct std::tm
Text::creation_time() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).creation_time();
}

Pers_no
Text::author() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).author();
}

long       
Text::num_lines() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).num_lines();
}

long       
Text::num_chars() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).num_chars();
}

int         
Text::num_marks() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).num_marks();
}

int
Text::num_recipients() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).num_recipients();
}

const Recipient* 
Text::recipient(int i) const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).recipient(i);
}

int
Text::num_comment_in() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).num_comment_in();
}

Text_no	
Text::comment_in(int i) const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).comment_in(i);
}

int
Text::num_footnote_in() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).num_footnote_in();
}

Text_no
Text::footnote_in(int i) const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).footnote_in(i);
}

int		
Text::num_uplinks() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).num_uplinks();
}

const Uplink*
Text::uplink(int i) const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).uplink(i);
}

int		
Text::num_aux() const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).num_aux();
}

Aux_item
Text::aux(int i) const
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).aux(i);
}

LyStr_errno
Text::text_mass(long begin, long end) const
{
    // FIXME: should only ask the cache for as much as is requested.
    LyStr_errno res = tm_cache->get(tno);
    res.str = res.str.substr(begin, end);
    return res;
}

LyStr_errno
Text::subject() const
{
    // FIXME: should maybe only ask for the first 256 bytes or so.
    LyStr_errno res = text_mass();

    if (res.kom_errno == KOM_NO_ERROR)
    {
	long i = res.str.strchr('\n');
	if (i != -1)
	    res.str = res.str.substr(i);
    }

    return res;
}

LyStr_errno
Text::body(long begin, long end) const
{
    LyStr_errno res = text_mass();

    if (res.kom_errno == KOM_NO_ERROR)
    {
	long i = res.str.strchr('\n');
	if (i != -1)
	    res.str.remove_first(i + 1);
	res.str = res.str.substr(begin, end);
    }

    return res;
}

struct std::tm
Text::entry_time(Conf_no cno, Local_text_no lno)
{
    assert(kom_errno() == KOM_NO_ERROR);
    return ts_cache->get(tno).entry_time(cno, lno);
}


Status
Text::mark_as_read(Conf_no cno)
{
    Text_stat   textstat;
    int         i;

    assert(kom_errno() == KOM_NO_ERROR);
    textstat = ts_cache->get(tno);
    
    // If cno=0, then mark the text as read in all conferences.
    if (cno == 0) 
    {
	std::vector<mark_as_read_question *>  mark_q_arr(
	    textstat.num_recipients());

	for (i = 0; i < textstat.num_recipients(); ++i) {
	    mark_q_arr[i]
		= new mark_as_read_question(ts_cache->connection(),
					    textstat.recipient(i)->recipient(),
					    textstat.recipient(i)->local_no());
	}

	Status return_status = st_ok;
	for (i = 0; i < textstat.num_recipients(); ++i) {
	    if (return_status == st_ok)
		return_status = mark_q_arr[i]->receive();
	    else
		(void) mark_q_arr[i]->receive();

	    delete mark_q_arr[i];
	}
	return return_status;

    } 
    else
    {
	// Search the recipients for the one to mark as read.
	for (i = 0; i < textstat.num_recipients(); ++i) 
	{
	    const Recipient * recip = textstat.recipient(i);
	    if (recip->recipient() == cno) 
	    {
		mark_as_read_question mark_q(ts_cache->connection(),
					     recip->recipient(),
					     recip->local_no());
		return mark_q.receive();
	    }
	}
    }

    return st_ok;
}

Status
Text::mark_as_unread(Conf_no cno)
{
    Text_stat   textstat;
    int         i;

    assert(kom_errno() == KOM_NO_ERROR);
    textstat = ts_cache->get(tno);
    
    // If cno=0, then mark the text as unread in all conferences.
    if (cno == 0) 
    {
	std::vector<mark_as_unread_question *>  mark_q_arr(
	    textstat.num_recipients());

	for (i = 0; i < textstat.num_recipients(); ++i) {
	    mark_q_arr[i]
		= new mark_as_unread_question(
		    ts_cache->connection(),
		    textstat.recipient(i)->recipient(),
		    textstat.recipient(i)->local_no());
	}

	Status return_status = st_ok;
	for (i = 0; i < textstat.num_recipients(); ++i) {
	    if (return_status == st_ok)
		return_status = mark_q_arr[i]->receive();
	    else
		(void) mark_q_arr[i]->receive();

	    delete mark_q_arr[i];
	}
	return return_status;

    } 
    else
    {
	// Search the recipients for the one to unmark as read.
	for (i = 0; i < textstat.num_recipients(); ++i) 
	{
	    const Recipient * recip = textstat.recipient(i);
	    if (recip->recipient() == cno) 
	    {
		mark_as_unread_question mark_q(ts_cache->connection(),
					       recip->recipient(),
					       recip->local_no());
		return mark_q.receive();
	    }
	}
    }

    return st_ok;
}


Kom_err
Text::mark(unsigned char mark_type)
{
    mark_text_question   mark_q(ts_cache->connection(), tno, mark_type);
    return mark_q.error_blocking();
}


Kom_err
Text::unmark()
{
    unmark_text_question   unmark_q(ts_cache->connection(), tno);
    return unmark_q.error_blocking();
}

Kom_err
Text::add_recipient(Conf_no cno)
{
    add_recipient_question   addrec_q(ts_cache->connection(), tno, cno, false);
    // FIXME: Add this call when text_cache::remove() is implemented.
    //ts_cache->remove(tno);
    return addrec_q.error_blocking();
}


Kom_err
Text::sub_recipient(Conf_no cno)
{
    sub_recipient_question   subrec_q(ts_cache->connection(), tno, cno);
    // FIXME: Add this call when text_cache::remove() is implemented.
    //ts_cache->remove(tno);
    return subrec_q.error_blocking();
}


Kom_err
Text::modify_info(const std::vector<Aux_no> &del,
		  const std::vector<Aux_item> &add)
{
    modify_text_info_question q(ts_cache->connection(), tno, del, add);
    return q.error_blocking();
}
				     

Local_text_no
Text::local_no_recipient(Conf_no conf_no)
{
    for (int i=0; i < num_recipients(); i++)
	if (recipient(i)->recipient() == conf_no)
	    return recipient(i)->local_no();
	
    return 0;
}

// ================================================================
// ================================================================
//                            Class Texts


// ================================================================ 
//                    Constructors and Destructors                  


Texts::Texts(Connection *conn, Async_dispatcher *async_disp)
    : connection(conn), ts_cache(new Text_stat_cache(conn, async_disp)),
      tm_cache(new Text_mass_cache(conn))
{
}
 

Texts::~Texts()
{
    if (ts_cache)
	delete ts_cache;
    if (tm_cache)
	delete tm_cache;
}


// ================================================================ 
//

Text
Texts::operator [](Text_no textno)
{
    assert(ts_cache != NULL);
    return Text(textno, ts_cache, tm_cache);
}

Text_stat_cache *
Texts::read_text_stat_cache() const
{
    return ts_cache;
}

Text_mass_cache *
Texts::read_text_mass_cache() const
{
    return tm_cache;
}
