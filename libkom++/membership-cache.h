// Cache of memberships.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_MEMBERSHIP_CACHE_H
#define KOMXX_MEMBERSHIP_CACHE_H

#include <map>

#include "kom-types.h"
#include "misc.h"
#include "membership.h"

class Connection;
class get_membership_question;

class Membership_cache {
  public:
    Membership_cache(Connection *c);
    Membership_cache(const Membership_cache&);
    Membership_cache& operator=(const Membership_cache&);
    ~Membership_cache();

    // Methods working on the entire cache:
    Connection  * connection() const;
    bool          present(Pers_no pno, Conf_no cno) const;
    void          clear();

    // Methods working on individual conf-stats.
    Status        prefetch(Pers_no pno, Conf_no cno);
    bool          prefetch_pending(Pers_no pno, Conf_no cno);
    bool	  present_or_pending(Pers_no pno, Conf_no cno);
    Membership    get(Pers_no pno, Conf_no cno);
    void          remove(Pers_no pno, Conf_no cno);

  private:
    typedef std::map<std::pair<Pers_no, Conf_no>, Membership> Immap;

    typedef std::map<std::pair<Pers_no, Conf_no>, get_membership_question*> 
        Imreqmap;

    Connection* conn;
    Immap       statusmap;
    Imreqmap    pendingmap;
};

#endif
