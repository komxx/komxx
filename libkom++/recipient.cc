// A recipient of a text.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>
#include <cstdlib>

#include "prot-a-LyStr.h"
#include "time-utils.h"
#include "recipient.h"

Recipient::Recipient()
    : r_recipient(0),
      r_recipient_type(RECIPIENT),
      r_local_no(0),
      r_rec_time_valid(false),
      r_sender(0),
      r_sent_at_valid(false)
{
}

Recipient::Recipient(const Recipient &u)
    : r_recipient(u.r_recipient),
      r_recipient_type(u.r_recipient_type),
      r_local_no(u.r_local_no),
      r_rec_time_valid(u.r_rec_time_valid),
      r_rec_time(u.r_rec_time),
      r_sender(u.r_sender),
      r_sent_at_valid(u.r_sent_at_valid),
      r_sent_at(u.r_sent_at)
{
}

const Recipient &
Recipient::operator =(const Recipient &u)
{
    r_recipient = u.r_recipient;
    r_recipient_type = u.r_recipient_type;
    r_local_no = u.r_local_no;
    r_rec_time_valid = u.r_rec_time_valid;
    r_rec_time = u.r_rec_time;
    r_sender = u.r_sender;
    r_sent_at_valid = u.r_sent_at_valid;
    r_sent_at = u.r_sent_at;
    return *this;
}

Recipient::Recipient(Conf_no recpt, bool carbon_copy)
  : r_recipient(recpt),
    r_recipient_type(carbon_copy ? CC_RECIPIENT : RECIPIENT),
    r_local_no(0),
    r_rec_time_valid(false),
    r_sender(0),
    r_sent_at_valid(false)
{
}

int 
Recipient::parse(bool cc_recpt, 
		 prot_a_LyStr &prot_a_str)
{
    int consumed_miscs=0;

    if (cc_recpt)
	r_recipient_type = CC_RECIPIENT;
    else
	r_recipient_type = RECIPIENT;

    r_recipient = prot_a_str.stol();
    consumed_miscs++;

    // Get the local_no (which must be here!)
    r_local_no = prot_a_str.stol();
    assert(r_local_no == 6);	// loc_no
    r_local_no = prot_a_str.stol();
    consumed_miscs++;

    prot_a_str.skipspace();
    while (prot_a_str.strlen() > 0 && prot_a_str[0] != '}'
	   && prot_a_str[0] >= '6')
    {
	int misctype = prot_a_str.stol();

	switch(misctype)
	{
	case 7:			// rec_time
	    assert(r_rec_time_valid == false);
	    r_rec_time = parse_time(prot_a_str);
	    r_rec_time_valid = true;
	    break;

	case 8:			// sent_by
	    assert (r_sender == 0);
	    r_sender = prot_a_str.stol();
	    assert (r_sender != 0);
	    break;

	case 9:			// sent_at
	    assert(r_sent_at_valid == false);
	    r_sent_at = parse_time(prot_a_str);
	    r_sent_at_valid = true;
	    break;

	default:
	    abort();
	}

	consumed_miscs++;
	prot_a_str.skipspace();
    }

    return consumed_miscs;
}
    
	    
Conf_no
Recipient::recipient() const
{
    return r_recipient;
}

bool
Recipient::is_carbon_copy() const
{
    switch(r_recipient_type)
    {
    case CC_RECIPIENT:
	return true;
    case RECIPIENT:
	return false;
    default:
	std::abort();
    }
}

Local_text_no
Recipient::local_no() const
{
    return r_local_no;
}

bool
Recipient::has_rec_time() const
{
    return r_rec_time_valid;
}

struct tm
Recipient::rec_time() const
{
    assert(r_rec_time_valid);
    return r_rec_time;
}

Pers_no
Recipient::sender() const
{
    return r_sender;
}

bool
Recipient::has_sent_at() const
{
    return r_sent_at_valid;
}

struct tm
Recipient::sent_at() const
{
    assert(r_sent_at_valid);
    return r_sent_at;
}
