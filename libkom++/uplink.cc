// "Footn_to" and "comment_to" link objects.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>
#include <cstdlib>

#include "time-utils.h"
#include "uplink.h"
#include "prot-a-LyStr.h"
#include "misc.h"

Uplink::Uplink()
    : upl_link_type(COMMENT),
      upl_parent_text_no(0),
      upl_sender(0),
      upl_sent_later(false)
{
}

Uplink::Uplink(const Uplink &u)
    : upl_link_type(u.upl_link_type),
      upl_parent_text_no(u.upl_parent_text_no),
      upl_sender(u.upl_sender),
      upl_sent_later(u.upl_sent_later),
      upl_sent_at(u.upl_sent_at)
{
}

const Uplink &
Uplink::operator =(const Uplink &u)
{
    upl_link_type = u.upl_link_type;
    upl_parent_text_no = u.upl_parent_text_no;
    upl_sender = u.upl_sender;
    upl_sent_later = u.upl_sent_later;
    upl_sent_at = u.upl_sent_at;
    return *this;
}

int 
Uplink::parse(bool is_footnote, prot_a_LyStr &prot_a_str)
{
    int consumed_miscs=0;

    if (is_footnote)
	upl_link_type = FOOTNOTE;
    else
	upl_link_type = COMMENT;

    upl_parent_text_no = prot_a_str.stol();
    consumed_miscs++;

    prot_a_str.skipspace();
    while (prot_a_str.strlen() > 0 && prot_a_str[0] != '}'
	   && prot_a_str[0] >= '6')
    {
	int misctype = prot_a_str.stol();

	switch(misctype)
	{
	case 8:			// sent_by
	    assert (upl_link_type == COMMENT);
	    assert (upl_sender == 0);
	    upl_sender = prot_a_str.stol();
	    assert (upl_sender != 0);
	    break;

	case 9:			// sent_at
	    assert(upl_sent_later == false);
	    upl_sent_at = parse_time(prot_a_str);
	    upl_sent_later = true;
	    break;

	default:
	    abort();
	}

	consumed_miscs++;
	prot_a_str.skipspace();
    }

    return consumed_miscs;
}

bool
Uplink::is_footnote() const
{
    return upl_link_type == FOOTNOTE;
}

Text_no
Uplink::parent() const
{
    return upl_parent_text_no;
}

Pers_no
Uplink::sender() const
{
    return upl_sender;
}

bool
Uplink::sent_later() const
{
    return upl_sent_later;
}

struct std::tm
Uplink::sent_at() const
{
    return upl_sent_at;
}
