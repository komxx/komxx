// Highlevel handling of a session to a LysKOM server.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_SESSION_H
#define KOMXX_SESSION_H

#include <ctime>
#include <stdint.h>

// FIXME: are all these inclusions really necessary?
#include "LyStr.h"
#include "kom-types.h"
#include "who-info-ident.h"
#include "micro-conf.h"
#include "conf-z-info.h"
#include "connection.h"
#include "async_dispatcher.h"
#include "mark.h"
#include "trinary.h"
#include "error-aggregate.h"
#include "user-area.h"
#include "conference.h"
#include "person.h"
#include "maps.h"
#include "text.h"
#include "membership.h"
#include "get_collate_table_q.h"
#include "get_version_info_q.h"
#include "get_static_session_info_q.h"
#include "dynamic-session-info.h"
#include "get_client_name_q.h"
#include "get_client_version_q.h"
#include "query_predefined_aux_items_q.h"

class Membership_cache;
class user_active_question;
class question;

// +++FIXME: Hur skall felhanteringen g� till?  Vi vill kunna f� ut
//           en felkod om n�got g�r fel.  FIXME: use exceptions.  Soon.

class Session {
  private:
    
    // Helper class used for static information that can be fetched
    // once (using a single question) and cached.  The question
    // (inherited from class question) is the template parameter Q.

    template <class Q>
    class Prefetchable_static_info
    {
      public:
	Prefetchable_static_info();
	~Prefetchable_static_info();

	// Returns true if the info has been fetched.
	bool fetched() const;

	// Returns the currently pending question, or NULL if no
	// question is pending.
	Q *question() const;

	// Store a newly constructed question.  Ownership of the
	// question is moved to the Prefetchable_static_info object.
	void store_question(Q *q);

	// Return the stored result.  This should only be called if
	// fetched() returned true.
	typename Q::result_type result() const;

	// Block until the pending questions returns.  If the status
	// is st_ok, store the result from the question and deallocate
	// the question.  Otherwise, the status is st_error, and you
	// can get the error code from the question (using the
	// question() method) -- use finalize_error to clean up once
	// you no longer need access to the question.
	Status receive();

	// Store a result.  This can be used to store a default result
	// if the question returned an error.
	void store_result(const typename Q::result_type &result);

	// Delete the pending question.  This should be used when
	// receive() returns st_error.
	void finalize_error();

      private:
	typename Q::result_type stored_result;
	Q *pending_question;
	bool result_fetched;

	Prefetchable_static_info(const Prefetchable_static_info&); // N/A
	Prefetchable_static_info &operator = (
	    const Prefetchable_static_info&); // N/A
    };

    // Helper class used for static information that can be fetched
    // once (using a single question) and cached, but is indexed using
    // an integer type.  The question (inherited from class question)
    // is the template parameter Q.

    template <class Q, class Key = typename Q::key_type>
    class Prefetchable_static_info_map
    {
      public:
	Prefetchable_static_info_map();
	~Prefetchable_static_info_map();

	// Returns true if the info has been fetched.
	bool fetched(Key k) const;

	// Returns the currently pending question, or NULL if no
	// question is pending.
	Q *question(Key k) const;

	// Store a newly constructed question.  Ownership of the
	// question is moved to the Prefetchable_static_info object.
	void store_question(Key k, Q *q);

	// Return the stored result.  This should only be called if
	// fetched() returned true.
	Kom_res<typename Q::result_type> result(Key k) const;

	// Block until the pending questions returns.  If the status
	// is st_ok, store the result from the question and deallocate
	// the question.  Otherwise, the status is st_error, and you
	// can get the error code from the question (using the
	// question() method) -- use finalize_error to clean up once
	// you no longer need access to the question.
	Status receive(Key k);

	// Delete the pending question.  This should be used when
	// receive() returns st_error.  result is the result that
	// should be used instead.
	void finalize_error(Key k, Kom_err e);

	// Forget everything in the cache.
	void clear_cache();

	// Forget a single entry in the cache.
	void clear_cache_entry(Key k);

      private:
	typedef std::map<Key, Kom_res<typename Q::result_type> > res_map_type;
	res_map_type stored_results;
	std::map<Key, Q*> pending_questions;

	Prefetchable_static_info_map(const Prefetchable_static_info_map&); // N/A
	Prefetchable_static_info_map &operator = (
	    const Prefetchable_static_info_map&); // N/A
    };

  public:
    Session(Connection *,
	    const LyStr &client_name,
	    const LyStr &client_version);
    ~Session();

    Conferences &  conferences();
    Persons     &  persons();
    Maps<Text_mapping> &  maps();
    Maps<Created_mapping> &created_maps();
    Texts       &  texts();
    Pers_no        my_login() const;
    Session_no     my_session() const;

    // Create a new person.  This uses request 89, so no autologin will be
    // performed.
    Kom_res<Pers_no>    create_person(const LyStr &name, const LyStr &password,
				      const Personal_flags &flags,
				      const std::vector<Aux_item> &aux_items);

    Status              login(Pers_no, const LyStr &, bool invisible = false);
    Status              logout();

    Status		enable(unsigned char level);

    Status              add_member(Conf_no cno, Pers_no reader,
				   unsigned char priority, 
				   unsigned short placement,
				   Membership_type type);
    Kom_err		sub_member(Conf_no cno, Pers_no member);
    Status              send_message(Pers_no recipient, const LyStr &message);
    struct std::tm      get_time(); 
    Status              change_what_i_am_doing(const LyStr &str);
    std::vector<Who_info_ident> who_is_on();
    std::vector<Dynamic_session_info>
                        who_is_on_dynamic(bool want_visible = true,
					  bool want_invisible = false,
					  long active_last = 0);
    Server_info         get_server_info();
    Kom_err             set_server_info(Conf_no conf_pres_conf,
					Conf_no pers_pres_conf,
					Conf_no motd_conf,
					Conf_no kom_news_conf,
					Text_no motd_of_lyskom);
    Status              set_motd_of_lyskom(Text_no motd);
    Kom_err		set_presentation(Conf_no cno, Text_no tno);

    Kom_res<Text_no>	get_last_text(const struct std::tm &);
    bool		is_supervisor(Pers_no supervisor, Conf_no conf);

    // Membership handling.

    // Is person a member of conf?
    bool                is_member(Pers_no person, Conf_no conf);
    std::vector<Pers_no>     get_members(Conf_no cno, 
				    unsigned short first, 
				    unsigned short no_of_members);

    // Prefetch information so that is_member can be answered.
    Status              prefetch_is_member(Pers_no person, Conf_no conf);

    Membership          get_membership(Pers_no person, Conf_no conf);
    Status              prefetch_membership(Pers_no person, Conf_no conf);
    bool                prefetch_membership_pending(Pers_no, Conf_no);
    bool                membership_present(Pers_no, Conf_no);
    Kom_err		set_last_read(Conf_no cno,
				      Local_text_no last_text_read);
    Kom_err             set_membership_type(Pers_no, Conf_no, Membership_type);

    // Tell the server that the user was active.  You can call this
    // method as often as you like -- it will only send a user-active
    // question if at least 30 seconds have passed since the previous
    // user-active sent, and the reply to the previous user-active has
    // been received.  This method never blocks, except possibly the
    // first time it is called.
    void		user_active();

    LyStr get_collate_table();
    Status prefetch_collate_table();
    Version_info get_version_info();
    Status prefetch_version_info();
    std::vector<Aux_tag> query_predefined_aux_items();
    Status prefetch_predefined_aux_items();
    Kom_res<Static_session_info> get_static_session_info(Session_no s);
    Status prefetch_static_session_info(Session_no s);
    Kom_res<LyStr> get_client_name(Session_no s);
    Status prefetch_client_name(Session_no s);
    Kom_res<LyStr> get_client_version(Session_no s);
    Status prefetch_client_version(Session_no s);

    std::vector<Conf_z_info> lookup_z_name(const LyStr &pattern,
				      bool want_persons =true,
				      bool want_confs =true);
    std::vector<Conf_z_info> re_z_lookup(const LyStr &pattern,
					 bool want_persons =true,
					 bool want_confs =true);
    std::vector<Mark>        get_marks();
    Kom_res<Local_text_no> last_local_before(Conf_no, const struct std::tm &);

    // Get a user-area handler.  The handler class is owned by the
    // session, and deleted by it when the session is deleted or when
    // the user is logged out.  In other words: don't hold on to the
    // pointer returned by user_area too long!
    Kom_res<User_area*> get_user_area();

    Kom_res<Conf_no>    create_conference(const LyStr &name,
					  const Conf_type &type,
					  const std::vector<Aux_item> &aux);

    // Modify server aux-items.
    Kom_err             modify_info(const std::vector<Aux_no> &del,
				    const std::vector<Aux_item> &add);

    // Obsolete:
    std::vector<Micro_conf>  lookup_name(const LyStr &pattern);
    std::vector<Pers_no>     regexp_lookup_persons(const LyStr &pattern);

    // Async handling.
    Async_dispatcher  & async_dispatcher();
    Status              handle_async(int this_many = 1);
    std::vector<uint32_t> query_async();

    // Retrieve the connection.
    Connection         *connection() const;
  private:
    Connection  * conn;
    Pers_no       logged_in_person;
    Session_no    session_number; // 0 if not yet known

    Conferences * all_confs;
    Persons     * all_persons;
    Maps<Text_mapping> * all_maps;
    Maps<Created_mapping> *all_created_maps;
    Texts       * all_texts;
    User_area   * my_user_area;
    Membership_cache *all_memberships;

    Async_dispatcher  * m_async_dispatcher;

    trinary server_supports_set_last_read;
    trinary server_supports_user_active;

    std::time_t previous_user_active_time;
    user_active_question *pending_user_active;
    
    Prefetchable_static_info<get_collate_table_question>  collate_table;
    Prefetchable_static_info<get_version_info_question>   version_info;
    Prefetchable_static_info<query_predefined_aux_items_question>
      predefined_aux_items;
    Prefetchable_static_info_map<get_static_session_info_question>
      static_session_info;
    Prefetchable_static_info_map<get_client_name_question> client_name;
    Prefetchable_static_info_map<get_client_version_question> client_version;

    // From time to time, this class issues questions whose results it
    // doesn't care about.  They are pushed on this array, and once in
    // a while the session uses check_ignored() to remove those
    // questions whose result has returned.
    std::vector<question*> ignored_questions;
    void check_ignored();

    // Clear the contents of all caches that should be cleared when a
    // new login is performed.
    void clear_caches();

    // Not implemented, since it doesn't make sense to copy sessions.
    Session(const Session &);
    const Session & operator = (const Session &);
};


#endif
