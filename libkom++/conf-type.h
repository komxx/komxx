// A Conf-type class.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_CONF_TYPE_H
#define KOMXX_CONF_TYPE_H

#include "trinary.h"

class prot_a_LyStr;

class Conf_type {
 public:
    friend Conf_type parse_conf_type(prot_a_LyStr &);

    Conf_type();
    Conf_type(const Conf_type&);
    Conf_type(bool protect,
	      bool orig,
	      bool secr,
	      bool letterbox);
    Conf_type(bool protect,
	      bool orig,
	      bool secr,
	      bool letterbox,
	      bool allowed_anonymous,
	      bool forbid_secret,
	      bool reserved2,
	      bool reserved3);

    Conf_type &operator =(const Conf_type &);

    bool is_protected() const;
    bool is_original()  const;
    bool is_secret()    const;
    bool is_letterbox() const;
    trinary allows_anonymous() const;
    trinary forbids_secret() const;
    trinary res2() const;
    trinary res3() const;

 private:
    bool rd_prot;
    bool original;
    bool secret;
    bool letter_box;
    trinary allow_anonymous;
    trinary forbid_secret;
    trinary reserved2;
    trinary reserved3;
};

Conf_type parse_conf_type(prot_a_LyStr &prot_a_str);
#endif
