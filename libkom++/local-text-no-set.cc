// Set of Local_text_no.
//
// Copyright (C) 1999  Per Cederqvist
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>
#include <cstdlib>

#include "local-text-no-set.h"

static Local_text_no
plus_one(Local_text_no n)
{
    if (n == MAX_LOCAL_TEXT_NO)
	return n;
    else
	return n + 1;
}

Local_text_no_set &
Local_text_no_set::operator=(const Local_text_no_set &s)
{
    if (&s != this)
	included = s.included;
    return *this;
}

// Find the block containing LNO, or the previous block if no such
// block exists, or end() if no such block exists.
Local_text_no_set::Map::const_iterator
Local_text_no_set::find_containing(Local_text_no lno) const
{
    Map::const_iterator iter = included.lower_bound(lno);

    if (iter == included.end())
    {
	if (included.size() == 0)
	    return iter;
	iter = included.end();
	--iter;
    }
    else if ((*iter).first > lno)
    {
	if (iter == included.begin())
	    return included.end();
	else
	    --iter;
    }
    
    assert((*iter).first <= lno);
    return iter;
}

Local_text_no_set::Map::iterator
Local_text_no_set::find_containing(Local_text_no lno)
{
    Map::iterator iter = included.lower_bound(lno);

    if (iter == included.end())
    {
	if (included.size() == 0)
	    return iter;
	iter = included.end();
	--iter;
    }
    else if ((*iter).first > lno)
    {
	if (iter == included.begin())
	    return included.end();
	else
	    --iter;
    }
    
    assert((*iter).first <= lno);
    return iter;
}

void 
Local_text_no_set::add(Local_text_no first, 
		       Local_text_no last)
{
    assert(first <= last);

    // Find the block that contains the start of the range.
    Map::iterator i1 = find_containing(first);
    Map::iterator i2 = find_containing(plus_one(last));

    if (i1 == included.end())
    {
	// The container is empty, or we are inserting a block before
	// the first block.
	if (i2 != included.end())
	{
	    // Possible overlap detected.  Remove the overlap and
	    // extend the range we are inserting.
	    assert(first < i2->first);
	    assert(plus_one(last) >= i2->first);
	    last = std::max(i2->second, last);
	    included.erase(included.begin(), ++i2);
	}
	included[first] = last;
	return;
    }

    assert(i2 != included.end());
    assert(i1->first <= first);
    assert(i2->first <= last);

    if (first <= plus_one(i1->second))
    {
	// The start of the range is contained in, or adjacent to, i1.
	if (i1->second >= last)
	{
	    assert(i1 == i2);
	    return;		// Already completely contained
	}

	assert(i1->first <= first && first <= plus_one(i1->second));
	assert(i2->first <= plus_one(last));
	assert(i1 == i2 || i1->second < i2->second);
	i1->second = std::max(i2->second, last);
	included.erase(++i1, ++i2);
	return;
    }

    // Need to insert a new interval.
    assert(i1->first < first);
    assert(i1->second < first);
    assert(i2->first < last);
    if (i1 != i2)
    {
	assert(i2 != included.end());
	assert(i1 != included.end());
	last = std::max(last, i2->second);
	included.erase(++i1, ++i2);
    }
    included[first] = last;
}

void 
Local_text_no_set::add(Local_text_no lno)
{
    add(lno, lno);
}

bool 
Local_text_no_set::present(Local_text_no lno) const
{
    Map::const_iterator i1 = find_containing(lno);
    if (i1 == included.end())
	return false;
    return i1->first <= lno && lno <= i1->second;
}

Local_text_no
Local_text_no_set::front() const
{
    return (*included.begin()).first;
}

void
Local_text_no_set::erase(Local_text_no lno)
{
    Map::iterator iter = find_containing(lno);
    if (iter == included.end())
	return;
    if (lno > (*iter).second)
	return;
    if (lno == (*iter).second)
    {
	// Delete at end of interval.
	if (lno == (*iter).first)
	    included.erase(iter);
	else
	    (*iter).second = lno;
    }
    else if (lno == (*iter).first)
    {
	// Delete at beginning of interval.
	Local_text_no end = (*iter).second;
	included.erase(iter);
	assert(lno+1 <= end);
	included[lno+1] = end;
    }
    else
    {
	// Split the interval.
	Local_text_no end = (*iter).second;
	(*iter).second = lno;
	included[lno+1] = end;
    }
}

void
Local_text_no_set::erase_above(Local_text_no lno)
{
    while (!included.empty())
    {
	Map::iterator iter = included.end();
	--iter;
	if (iter->first > lno)
	    included.erase(iter);
	else
	{
	    if (iter->second > lno)
		iter->second = lno;
	    break;
	}
    }
}


void
Local_text_no_set::clear()
{
    included.clear();
}


bool
Local_text_no_set::get_range(Local_text_no key,
			     Local_text_no &first, 
			     Local_text_no &last)
{
    Map::iterator iter(find_containing(key));

    if (iter == included.end())
	return false;

    if (iter->second < key)
    {
	++iter;
	if (iter == included.end())
	    return false;
    }

    first = iter->first;
    last = iter->second;
    return true;
}
