dnl Local extensions macros to Autoconf.
dnl Copyright (C) 1994, 1995  Per Cederqvist and Inge Wallin
dnl
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2 of the License, or
dnl (at your option) any later version.
dnl 
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl 
dnl You should have received a copy of the GNU General Public License
dnl along with this program; if not, write to the Free Software
dnl Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
dnl
dnl
dnl
dnl Check if an option is acceptable to the C compiler in use
dnl
dnl Check if an option is acceptable to the C compiler in use.
dnl (This is taken verbatim from cmod 1.2.  Please don't make even a
dnl tiny change to it unless you change the name of all variables!
dnl See the cmod source code for more information.)
dnl
AC_DEFUN([CMOD_CHECK_CC_OPT],
[AC_MSG_CHECKING([whether ${CC} accepts $1])
AC_CACHE_VAL([cmod_cv_compiler_]$2,
[[cmod_oldflags=$CFLAGS
CFLAGS="$CFLAGS $1"]
AC_TRY_LINK(,,
	[cmod_cv_compiler_]$2[=yes],
	[cmod_cv_compiler_]$2[=no])
[CFLAGS=$cmod_oldflags]])dnl
AC_MSG_RESULT([$cmod_cv_compiler_]$2)
if test [$cmod_cv_compiler_]$2 = yes; then
  CFLAGS="$CFLAGS $1"
fi])dnl
dnl
dnl Another frozen defun.
dnl
AC_DEFUN([CMOD_C_WORKING_ATTRIBUTE_UNUSED],
[AC_CACHE_CHECK([[whether $CC understands __attribute__((unused))]],
    [[cmod_cv_c_working_attribute_unused]],
    [dnl gcc 2.6.3 understands the __attribute__((unused)) syntax
    dnl enough that it prints a warning and ignores it when the
    dnl variable "i" is declared inside the function body, but it
    dnl barfs on the construct when it is used in a
    dnl parameter-declaration.  That is why we have a function
    dnl definition in the prologue of AC_LANG_PROGRAM part.
    AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM([[int cmod_x(int y __attribute__((unused)))
			   { return 7; }]],
	    [[int i __attribute__((unused));]])],
	[cmod_cv_c_working_attribute_unused=yes],
	[cmod_cv_c_working_attribute_unused=no])])
[if test $cmod_cv_c_working_attribute_unused = yes ; then]
    AC_DEFINE([HAVE_ATTRIBUTE_UNUSED], [1], 
	      [Define if your compiler supports __attribute__ ((unused)).])
[fi]])dnl
dnl
dnl Check if an option is acceptable to the C++ compiler in use
dnl
AC_DEFUN([KOM_CHECK_CXX_OPT],
[AC_MSG_CHECKING([whether ${CXX} accepts $1])
AC_CACHE_VAL([kom_cv_compiler_cxx_]$2,
[[kom_oldflags=$CFLAGS
CFLAGS="$CFLAGS $1"]
AC_TRY_LINK(,,
	[kom_cv_compiler_cxx_]$2[=yes],
	[kom_cv_compiler_cxx_]$2[=no])
[CFLAGS=$kom_oldflags]])dnl
AC_MSG_RESULT([$kom_cv_compiler_cxx_]$2)
if test [$kom_cv_compiler_cxx_]$2 = yes; then
  CFLAGS="$CFLAGS $1"
fi])dnl
dnl
dnl
dnl Check that "struct std::iterator" is available in <iterator>.
dnl The STL supplied with gcc-2.95.2 does contain a proper struct
dnl iterator, but it doesn't make it available when compiled with 
dnl gcc...
dnl
AC_DEFUN([KOM_STL_STRUCT_ITERATOR],
[AC_MSG_CHECKING([whether struct std::iterator<> is available in <iterator>])
AC_CACHE_VAL([kom_cv_stl_struct_iterator],
	[AC_TRY_COMPILE([#include <iterator>],
		[class foo : public std::iterator<
			std::forward_iterator_tag, int, int, int*, int&>
		{
		public: foo();
		};
		foo bar;],
		[kom_cv_stl_struct_iterator=yes],
		[kom_cv_stl_struct_iterator=no])])
AC_MSG_RESULT([$kom_cv_stl_struct_iterator])
[if test $kom_cv_stl_struct_iterator = no; then]
  AC_DEFINE([NEED_STRUCT_ITERATOR], 1, 
	[Define if ``struct iterator<>'' isn't available.])
[fi]])
dnl
dnl
dnl Find stuff about the Python installation.
dnl This sets PYTHON_CONFIGDIR and PYTHON_INCLUDES.
dnl
AC_DEFUN([KOM_PYTHON_PATHS],
[[if test x$1 = x
then
    PYTHON_CONFIGDIR=
    PYTHON_INCLUDES=
else]
    AC_MSG_CHECKING([for Python config])
    AC_CACHE_VAL(kom_cv_prog_python_configdir,
    [[cat > conftestpy.py << \EOF
import sys
import string

try:
    v="%d.%d" % (sys.version_info[0], sys.version_info[1])
except AttributeError:
    v=sys.version[:3]

pfx = [sys.prefix]
if sys.exec_prefix != sys.prefix:
    pfx.append(sys.exec_prefix)

print "kom_cv_prog_python_configdir=%s/lib/python%s/%s" % (
	sys.exec_prefix, v, "config")
print "kom_cv_prog_python_includes=\"%s\"" % string.join(
    map(lambda p, v=v: "-I%s/include/python%s" % (p, v), pfx), " ")
EOF

    if $1 conftestpy.py > conftestpy.sh
    then
        . ./conftestpy.sh
    else]]
        AC_MSG_ERROR([cannot find python binary $1])
    [[fi]])
    AC_MSG_RESULT($kom_cv_prog_python_configdir)
    AC_MSG_CHECKING([for Python include files])
    AC_MSG_RESULT($kom_cv_prog_python_includes)
    [PYTHON_CONFIGDIR="$kom_cv_prog_python_configdir"
    PYTHON_INCLUDES="$kom_cv_prog_python_includes"
fi]
AC_SUBST(PYTHON_CONFIGDIR)
AC_SUBST(PYTHON_INCLUDES)
])dnl
dnl eof
