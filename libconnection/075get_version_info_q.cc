// Ask the server for the version.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "connection.h"
#include "time-utils.h"
#include "get_version_info_q.h"

get_version_info_question::get_version_info_question(Connection *conn)
    : question(conn)
{
    prot_a_LyStr b;
    b << ref_no() << ' ' << 75 << '\n';
    port->send_question(b);
}

get_version_info_question::~get_version_info_question()
{
}

void
get_version_info_question::parser(prot_a_LyStr& str)
{
    long protocol_version = str.stol();
    LyStr_errno server_software = str.stos();
    LyStr_errno software_version = str.stos();

    if (server_software.kom_errno != KOM_NO_ERROR)
	receive_err(server_software.kom_errno);
    else if (software_version.kom_errno != KOM_NO_ERROR)
	receive_err(software_version.kom_errno);
    else
    {
	version_info = Version_info(protocol_version,
				    server_software.str,
				    software_version.str);
	state = st_ok;
	deallocate_refno();
    }
}


Version_info
get_version_info_question::result()
{
    return version_info;
}
