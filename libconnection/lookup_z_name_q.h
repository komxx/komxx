// Look up a name.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_LOOKUP_Z_NAME_Q_H
#define KOMXX_LOOKUP_Z_NAME_Q_H

#include <vector>

#include "kom-types.h"
#include "question.h"
#include "conf-z-info.h"

class LyStr;

// 76

class lookup_z_name_question : public question {
  public:
    typedef std::vector<Conf_z_info> result_type;

    lookup_z_name_question(Connection *conn,
			   const LyStr &template_str,
			   bool want_persons =true,
			   bool want_confs =true);
    ~lookup_z_name_question();
    void parser(prot_a_LyStr &str);
    result_type result();
  private:
    std::vector<Conf_z_info> matches;
};

#endif
