// Set number of unread texts in a conference.
//
// Copyright (C) 1994, 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_SET_LAST_READ_Q_H
#define KOMXX_SET_LAST_READ_Q_H

#include "kom-types.h"
#include "question.h"
#include "conference.h"

// 77

class set_last_read_question : public question {
  public:
    typedef void result_type;

    set_last_read_question(Connection *conn, Conference conference,
			   Local_text_no last_read);
    set_last_read_question(Connection *conn, Conf_no conf_no,
			   Local_text_no last_read);
    ~set_last_read_question();
    void parser(prot_a_LyStr &str);
    void receive_err(int err);
    result_type result();
};

#endif
