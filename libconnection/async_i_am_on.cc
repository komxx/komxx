// Objects for handling the asynchronous message "I am on"
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "async_i_am_on.h"
#include "prot-a-LyStr.h"
#include "connection.h"


Async_i_am_on::Async_i_am_on()
: winfo()
{
}

Async_i_am_on::Async_i_am_on(const Async_i_am_on &i)
  : Async_message(), 
    winfo(i.winfo)
{
}

Async_i_am_on::~Async_i_am_on()
{
}

Async_i_am_on &
Async_i_am_on::operator =(const Async_i_am_on &i)
{
    winfo = i.winfo;

    return *this;
}

Async_name
Async_i_am_on::name()
{
    return ASYNC_I_AM_ON;
}

Who_info
Async_i_am_on::who_info() const
{
    return winfo;
}

Async_i_am_on::Async_i_am_on(prot_a_LyStr &p)
{
    winfo = Who_info(p);
}

template<>
const Async_message *
Async_administrator<Async_i_am_on>::parse(prot_a_LyStr &in)
{
    return new Async_i_am_on(in);
}
