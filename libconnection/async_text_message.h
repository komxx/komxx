// Objects for handling the asynchronous message "Text message"
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_ASYNC_TEXT_MESSAGE_H
#define KOMXX_ASYNC_TEXT_MESSAGE_H

#include "async.h"
#include "kom-types.h"
#include "LyStr.h"


class Async_text_message : public Async_message {
  public:
    Async_text_message();
    Async_text_message(const Async_text_message &);
    ~Async_text_message();
    Async_text_message &operator =(const Async_text_message &);

    static Async_name name();

    Conf_no         recipient() const;
    Pers_no         sender() const;
    const LyStr &   message() const;
  public:
    // Only intended to be used by class Async_administrator.
    Async_text_message(prot_a_LyStr &);

  private:
    Pers_no   recipient_m;
    Pers_no   sender_m;
    LyStr     message_m;
};

#endif
