// Get membership.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "get_membership_q.h"
#include "connection.h"
#include "membership.h"
    
get_membership_question::get_membership_question(Connection *conn, 
						 Pers_no pno, Conf_no cno)
    : question(conn),
      pers_no(pno),
      membership()
{
    prot_a_LyStr   s;

    s << ref_no() << " 9 " << pno << ' ' << cno << '\n';
    port->send_question(s);
}


get_membership_question::~get_membership_question()
{
}


void
get_membership_question::parser(prot_a_LyStr &str)
{
    membership = Membership(pers_no, true, str);

    state = st_ok;
    deallocate_refno();
}


Membership
get_membership_question::result()
{
    return membership;
}
