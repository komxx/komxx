// Create a conference.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "create_conf_q.h"
#include "conf-type.h"
#include "aux-item.h"
    
create_conf_question::create_conf_question(
    Connection *conn, 
    const LyStr &name,
    const Conf_type &type,
    const std::vector<Aux_item> &aux_items)
  : question(conn), cno(0)
{
    prot_a_LyStr   s;

    s << ref_no() << " 88 ";
    s.append_hollerith(name);
    s << ' ';
    s.append_bool(type.is_protected());
    s.append_bool(type.is_original());
    s.append_bool(type.is_secret());
    s.append_bool(type.is_letterbox());
    s.append_bool(!is_false(type.allows_anonymous()));
    s.append_bool(is_true(type.forbids_secret()));
    s.append_bool(is_true(type.res2()));
    s.append_bool(is_true(type.res3()));

    s << ' ';
    s.append_aux_list(aux_items);
    s << '\n';

    port->send_question(s);
}


create_conf_question::~create_conf_question()
{
}


void
create_conf_question::parser(prot_a_LyStr& str)
{
    cno = str.stol();

    state = st_ok;
    deallocate_refno();
}


Conf_no
create_conf_question::result()
{
    return cno;
}
