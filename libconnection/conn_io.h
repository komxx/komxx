// Abstract I/O to the LysKOM server.
//
// Copyright (C) 2002  Per Cederqvist
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_CONN_IO_H
#define KOMXX_CONN_IO_H

#include "prot-a-LyStr.h"
#include "misc.h"

struct Connection;

// Conn_io is a wrapper abstraction used by Connection when it talks
// to the LysKOm server.  It exists so that we can use ISC, QT, or
// some other means of talking to the server.  Most of the methods in
// this class are abstract.  Applications should use Conn_io_ISC or
// some other derived class.

class Conn_io {
  public:
    // Create a Conn_io object.
    Conn_io();

    // Destroy a Conn_io object.
    virtual ~Conn_io();

    // Call this after construction to check for errors.  (It is
    // enough to use Connection::connection_failed(), since that
    // method calls this method in turn.)
    virtual bool connection_failed() const =0;

    // Return the name of the local host.
    virtual LyStr localhost() const =0;


    // The got_data() methods are intended to be used by derived
    // classes.  When new data is available from the server, it should
    // be passed to any of the got_data() methods.

    void got_data(const prot_a_LyStr &buf);
    void got_data(const LyStr &buf);
    void got_data(const char *buf, long len);


    // All methods below are intended to be used only by class
    // Connection.


    // When a Connection is created, a Conn_io object must be passed
    // to its constructor.  That constructor uses this method to
    // inform the Conn_io object of what Connection it belongs to.
    void set_connection(Connection *);

    // Enqueue a block of data for transmission to the server.  Might
    // send some or all of it at once.  Should never block.
    virtual void queue_for_send(char *block, int len) =0;

    // Get data from the server, and pass it to one of the got_data()
    // methods.  Returns st_ok if any data was received, st_pending if
    // no data was received before the timeout limit was reached, and
    // st_error if an error occured.  The timeout is measured in
    // milliseconds.  If there is data enqueued for transmission to
    // the server, this function must attempt to send that data
    // (nonblockingly) until the function returns.
    virtual Status receive(int timeout=0) =0;

  private:
    Connection *parent;
};

#endif
