// Get text.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "get_text_q.h"
#include "connection.h"
#include "prot-a-LyStr.h"

    
get_text_question::get_text_question(Connection *conn, 
				     Text_no text_no,
				     long    start_char,
				     long    end_char)
    : question(conn)
{
    prot_a_LyStr   s;

    s << ref_no() << " 25 " << text_no << ' ' 
      << start_char << ' ' << end_char << '\n';
    port->send_question(s);
}


get_text_question::~get_text_question()
{
}


void
get_text_question::parser(prot_a_LyStr &str)
{
    LyStr_errno tmp = str.stos();
    if (tmp.kom_errno != KOM_NO_ERROR)
	receive_err(tmp.kom_errno);
    else
    {
	text = tmp.str;
	state = st_ok;
	deallocate_refno();
    }
}


LyStr
get_text_question::result()
{
    return text;
}
