// Handle a connection to a LysKOM server.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_CONNECTION_H
#define KOMXX_CONNECTION_H

#include <queue>

#include "server-info.h"

#include "prot-a-LyStr.h"
#include "misc.h"

class Conn_io;

// The class Connection adds low-level Protocol A understanding on top
// of a Conn_io stream. 

class Connection {
  public:
    Connection(Conn_io *io);
    bool connection_failed() const;
    ~Connection();

    // Enqueue a question for transmission to the server.  The
    // questions isn't necessarily sent right away.  All routines that
    // await input ensure that the question is sent.  Call drain() to
    // force pending questions to be sent to the server.
    void send_question(const prot_a_LyStr &str);

    // Parse any input that is available. Returns st_pending if no
    // input was available during TIMEOUT milliseconds. Returns st_ok
    // as soon as anything has been received. Returns st_error if
    // something is wrong with the connection - if that happens, you
    // should probably kill the connection and start a new one.
    Status drain(int timeout=0);

    // Register a function that will be called whenever an async
    // message has arrived. The first argument to register_async will
    // be passed as the first argument to the handler.  Only one handler
    // can be registered.  This is normally used by Async_dispatcher.
    // Application code should use the services provieded by
    // async_dispatcher.h instead of using this method.
    void register_async(void *, void (*new_sinc)(void *, prot_a_LyStr));

    // Get the server_info information for this server.  Once fetched,
    // the information is cached.
    Server_info  * get_server_info();

    // Invalidate the Server_info, so that it will be refetched the
    // next time it is used.
    void invalidate_server_info();

    // Retrieve statistics.
    long generated_questions() const;
    long received_asyncs() const;
    long ignored_asyncs() const;
    long received_replies() const;

    // Data arrived from the server.  This method is intended to be
    // called from the Call_io object given to the constructor.
    void got_data(const prot_a_LyStr &buf);
    void got_data(const LyStr &buf);
    void got_data(const char *buf, long len);

  private:
    // The class that does all the low-level communication.
    Conn_io    *conn_io;

    // Initialized to 7, to skip past "LysKOM\n" at start of protocol.
    int 	  LysKOM_gotten;

    // When a message is received, it is first collected in the string
    // named unparsed.  The 'end of message' token is a newline that
    // doesn't appear inside a string.  It is hunted for using
    // hunt_nl_index. Strings are skipped with str_skip_left.
    prot_a_LyStr  unparsed;
    long 	  hunt_nl_index;
    long	  str_skip_left;
    // Gather the len of the next string here.
    long	  strlen_gather;

    // Handle a single message.  On entry, the message is present in
    // unparsed, and it is hunt_nl_index characters long.
    void handle_package();

    // Parse the data in unparsed.  If any complete message is found,
    // call handle_package().  This is called from drain().
    Status parse();

    // This information is fetched from the server on a need-to-know
    // basis by various questions, so that they can talk to old
    // servers by emulating the newer questions (often by getting
    // the same information by issuing older questions).
    Server_info   server_info;
    bool          server_info_fetched;

    // Asynchronous messages are immediately sent to async_sink, if
    // that is initialized. Otherwise, they are discarded.
    void (*async_sink)(void *, prot_a_LyStr);
    void *sync_sink_data;

    // These are not, and should not currently be, implemented.
    Connection(const Connection&);
    Connection &operator =(const Connection&);

    // Statistics, statistics, statistics.
    long m_generated_questions;
    long m_received_asyncs;
    long m_ignored_asyncs;
    long m_received_replies;
};

#endif
