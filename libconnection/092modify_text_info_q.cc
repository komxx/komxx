// Modify aux-items of a text.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>

#include "prot-a-LyStr.h"
#include "modify_text_info_q.h"
#include "connection.h"

modify_text_info_question::modify_text_info_question(
    Connection *conn, 
    Text_no tno, 
    const std::vector<Aux_no> &del,
    const std::vector<Aux_item> &add)

  : question(conn)
{
    prot_a_LyStr   b;

    assert(tno != 0);
    b << ref_no() << " 92 " << tno << ' ';
    b.append_aux_no_list(del);
    b << ' ';
    b.append_aux_list(add);
    b << '\n';
    port->send_question(b);
}


modify_text_info_question::~modify_text_info_question()
{
}


void
modify_text_info_question::parser(prot_a_LyStr &str)
{
    state = st_ok;
    deallocate_refno();
}


void
modify_text_info_question::result()
{
}
