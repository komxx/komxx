// Objects for handling the asynchronous message "New text created"
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "async_text_created.h"
#include "prot-a-LyStr.h"
#include "connection.h"


Async_text_created::Async_text_created()
: tstat()
{
}

Async_text_created::Async_text_created(const Async_text_created &t)
  : Async_message(),
    tstat(t.tstat)
{
}

Async_text_created::~Async_text_created()
{
}

Async_text_created &
Async_text_created::operator =(const Async_text_created &t)
{
    tstat = t.tstat;
    return *this;
}

Async_name
Async_text_created::name()
{
    return ASYNC_NEW_TEXT;
}

Text_stat 
Async_text_created::text_stat() const
{
    return tstat;
}

Async_text_created::Async_text_created(prot_a_LyStr &p)
{
    Text_no tno = p.stol();
    Text_stat t(tno, false, p);
    tstat = t;
}

template<>
const Async_message *
Async_administrator<Async_text_created>::parse(prot_a_LyStr &in)
{
    return new Async_text_created(in);
}
