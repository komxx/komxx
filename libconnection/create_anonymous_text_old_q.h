// Create an anonymous text.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_CREATE_ANONYMOUS_TEXT_OLD_Q_H
#define KOMXX_CREATE_ANONYMOUS_TEXT_OLD_Q_H

#include <vector>

#include "kom-types.h"
#include "question.h"
#include "recipient.h"

class LyStr;

// 59

class create_anonymous_text_old_question : public question {
  public:
    typedef Text_no result_type;

    create_anonymous_text_old_question(
	Connection *conn, const LyStr &message, 
	const std::vector<Recipient> &recipients,
	const std::vector<Text_no>   &comment_to,
	const std::vector<Text_no>   &footnote_to);
    ~create_anonymous_text_old_question();
    void parser(prot_a_LyStr &str);
    result_type result();
  private:
    Text_no anon_text;
};

#endif
