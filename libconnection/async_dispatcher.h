// Higher-level handling of asynchronous messages.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_ASYNC_DISPATCHER_H
#define KOMXX_ASYNC_DISPATCHER_H

#include "async.h"


class Connection;

class Async_text_created;
class Async_text_message;

class Async_dispatcher {
  public:
    Async_dispatcher(Connection *);
    ~Async_dispatcher();
    Async_dispatcher &operator=(const Async_dispatcher &);

    Async_handler_tag<Async_text_created>
    register_text_created_handler(Kom_auto_ptr<Async_callback
				  <Async_text_created> > cb);

    Async_handler_tag<Async_text_message>
    register_text_message_handler(Kom_auto_ptr<Async_callback
				  <Async_text_message> > cb);

    void unregister_text_created_handler(Async_handler_tag<Async_text_created
					 >);
    void unregister_text_message_handler(Async_handler_tag<Async_text_message
					 >);

    // Once all handlers are registered, call this method so that the
    // server is informed about what you are looking for.
    void do_accept_async();

    Status handle_async(int this_many = 1);

  private:
    Connection           * port;
    Async_storage        * storage;
    Async_administrator<Async_text_created>  * text_created_admin;
    Async_administrator<Async_text_message>  * text_message_admin;
};


#endif
