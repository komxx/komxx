// Ask the server who is on.
//
// Copyright (C) 1994, 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>

#include "prot-a-LyStr.h"
#include "who_is_on_dynamic_q.h"
#include "connection.h"
    

who_is_on_dynamic_question::who_is_on_dynamic_question(
    Connection *conn,
    bool want_visible,
    bool want_invisible,
    long active_last)

  : question(conn), wholist()
{
    prot_a_LyStr   s;

    s << ref_no() << " 83 "
      << (want_visible ? '1' : '0') << ' '
      << (want_invisible ? '1' : '0') << ' '
      << active_last << '\n';

    port->send_question(s);
}


who_is_on_dynamic_question::~who_is_on_dynamic_question()
{
}


void
who_is_on_dynamic_question::parser(prot_a_LyStr &str)
{
    int   arrsize;
    int   i;

    arrsize = str.parse_array_start();
    if (arrsize < 0)
	std::abort();
    else
    {
	wholist.resize(arrsize);
	for (i = 0; i < arrsize; ++i)
	{
	    Kom_err err = wholist[i].parse(str);
	    if (err != KOM_NO_ERROR)
	    {
		receive_err(err);
		return;
	    }
	}
    }
    str.parse_array_end(arrsize);

    state = st_ok;
    deallocate_refno();
}


std::vector<Dynamic_session_info>
who_is_on_dynamic_question::result()
{
    return wholist;
}
