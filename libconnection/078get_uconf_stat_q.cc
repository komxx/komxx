// Get uconf_stat.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>

#include "prot-a-LyStr.h"
#include "uconf-stat.h"
#include "get_uconf_stat_q.h"
#include "connection.h"
    
get_uconf_stat_question::get_uconf_stat_question(Connection *conn, 
					       Conf_no conf_no)
    : question(conn), requested_conf_no(conf_no), the_conf(0)
{
    prot_a_LyStr   b;

    assert(conf_no != 0);
    b << ref_no() << " 78 " << conf_no << '\n';
    port->send_question(b);
}


get_uconf_stat_question::~get_uconf_stat_question()
{
    if (the_conf != NULL)
	delete the_conf;
}


void
get_uconf_stat_question::parser(prot_a_LyStr &str)
{
    the_conf = new UConf_stat(requested_conf_no, str);
    state = st_ok;
    deallocate_refno();
}


UConf_stat
get_uconf_stat_question::result()
{
    assert(the_conf != NULL);
    return *the_conf;
}
