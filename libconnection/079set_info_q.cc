// Set the server information.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "set_info_q.h"

    
set_info_question::set_info_question(Connection *conn,
				     Conf_no conf_pres_conf,
				     Conf_no pers_pres_conf,
				     Conf_no motd_conf,
				     Conf_no kom_news_conf,
				     Text_no motd_of_lyskom)
    : question(conn)
{
    prot_a_LyStr   s;

    s << ref_no() << " 79 10000 "
      << conf_pres_conf << ' '
      << pers_pres_conf << ' '
      << motd_conf << ' '
      << kom_news_conf << ' '
      << motd_of_lyskom << '\n';
    port->send_question(s);
}


set_info_question::~set_info_question()
{
}


void
set_info_question::parser(prot_a_LyStr &)
{
    state = st_ok;
    deallocate_refno();
}


void
set_info_question::result()
{
    return;
}
