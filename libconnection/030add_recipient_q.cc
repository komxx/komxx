// Add a recipient to an existing text
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "add_recipient_q.h"

    
add_recipient_question::add_recipient_question(Connection *conn,
					       Text_no     tno,
					       Conf_no     cno,
					       bool        cc_recipient)
    : question(conn)
{
    prot_a_LyStr   s;

    s << ref_no() << " 30 " << tno << ' ' << cno << ' '
      << (cc_recipient ? '1' : '0') << '\n';
    port->send_question(s);
}


add_recipient_question::~add_recipient_question()
{
}


void
add_recipient_question::parser(prot_a_LyStr &)
{
    state = st_ok;
    deallocate_refno();
}


void
add_recipient_question::result()
{
    return;
}
