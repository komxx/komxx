// Objects for handling the asynchronous message "Conf changed name"
//
// Copyright (C) 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>

#include "async_conf_changed_name.h"
#include "prot-a-LyStr.h"
#include "connection.h"


Async_conf_changed_name::Async_conf_changed_name()
: cno(0), oname(), nname()
{
}

Async_conf_changed_name::Async_conf_changed_name(const Async_conf_changed_name &c)
    : Async_message(), cno(c.cno), oname(c.oname), nname(c.nname)
{
}

Async_conf_changed_name::~Async_conf_changed_name()
{
}

Async_conf_changed_name &
Async_conf_changed_name::operator =(const Async_conf_changed_name &c)
{
    cno = c.cno;
    oname = c.oname;
    nname = c.nname;

    return *this;
}

Async_name
Async_conf_changed_name::name()
{
    return ASYNC_CONF_CHANGE_NAME;
}

Conf_no
Async_conf_changed_name::conf_no() const
{
    return cno;
}

LyStr
Async_conf_changed_name::old_name() const
{
    return oname;
}

LyStr
Async_conf_changed_name::new_name() const
{
    return nname;
}

Async_conf_changed_name::Async_conf_changed_name(prot_a_LyStr &p)
{
    cno = p.stol();
    LyStr_errno tmp = p.stos();
    assert(tmp.kom_errno == KOM_NO_ERROR);
    oname = tmp.str;

    tmp = p.stos();
    assert(tmp.kom_errno == KOM_NO_ERROR);
    nname = tmp.str;
}

template<>
const Async_message *
Async_administrator<Async_conf_changed_name>::parse(prot_a_LyStr &in)
{
    return new Async_conf_changed_name(in);
}
