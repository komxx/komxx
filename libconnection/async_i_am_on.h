// Objects for handling the asynchronous message "New text created"
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_ASYNC_I_AM_ON_H
#define KOMXX_ASYNC_I_AM_ON_H

#include "who-info.h"
#include "async.h"


class Async_i_am_on : public Async_message {
  public:
    Async_i_am_on();
    Async_i_am_on(const Async_i_am_on &);
    ~Async_i_am_on();
    Async_i_am_on &operator =(const Async_i_am_on &);
	
    static Async_name name();

    Who_info  who_info() const;
  public:
    // Only intended to be used by
    // class Async_administrator<Async_i_am_on>.
    Async_i_am_on(prot_a_LyStr &);
  private:
    Who_info  winfo;
};

#endif
