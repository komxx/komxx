// Perform a name lookup.
//
// Copyright (C) 1995  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "lookup_z_name_q.h"

lookup_z_name_question::lookup_z_name_question(Connection *conn,
					       const LyStr &template_str,
					       bool want_persons,
					       bool want_confs)
    : question(conn), matches()
{
    prot_a_LyStr   s;

    s << ref_no() << " 76 ";
    s.append_hollerith(template_str);
    s << ' ' << (want_persons ? '1' : '0')
      << ' ' << (want_confs ? '1' : '0')
      << '\n';
    port->send_question(s);
}


lookup_z_name_question::~lookup_z_name_question()
{
}


void
lookup_z_name_question::parser(prot_a_LyStr &str)
{
    int   arrsize;
    int   i;

    arrsize = str.parse_array_start();
    matches.resize(arrsize);

    for (i = 0; i < arrsize; ++i) 
	matches[i] = parse_conf_z_info(str);
    str.parse_array_end(arrsize);

    state = st_ok;
    deallocate_refno();
}


std::vector<Conf_z_info>
lookup_z_name_question::result()
{
    return matches;
}
