// Create an anonymous text.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "recipient.h"
#include "create_anonymous_text_old_q.h"
    
create_anonymous_text_old_question::create_anonymous_text_old_question(
    Connection *conn,
    const LyStr &message,
    const std::vector<Recipient> &recipients,
    const std::vector<Text_no>  &comment_to,
    const std::vector<Text_no>  &footnote_to)
  : question(conn), anon_text(0)
{
    typedef std::vector<Recipient>::const_iterator RI;
    typedef std::vector<Text_no>::const_iterator TI;

    prot_a_LyStr   s;
    int len = recipients.size() + comment_to.size() + footnote_to.size();

    s << ref_no() << " 59 ";
    s.append_hollerith(message);
    s << ' ' << len << " { ";
    for (TI iter = footnote_to.begin(); iter != footnote_to.end(); ++iter)
	s << "4 " << *iter << ' ';

    for (TI iter = comment_to.begin(); iter != comment_to.end(); ++iter)
	s << "2 " << *iter << ' ';

    for (RI iter = recipients.begin(); iter != recipients.end(); ++iter)
	s << ((*iter).is_carbon_copy() ? "1 " : "0 ")
	  << (*iter).recipient() << ' ';

    s << "}\n";

    port->send_question(s);
}


create_anonymous_text_old_question::~create_anonymous_text_old_question()
{
}


void
create_anonymous_text_old_question::parser(prot_a_LyStr& str)
{
    anon_text = str.stol();
    state = st_ok;
    deallocate_refno();
}


Text_no
create_anonymous_text_old_question::result()
{
    return anon_text;
}
