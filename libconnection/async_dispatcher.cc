// Higher-level handling of asynchronous messages.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#include <config.h>

#include <cstdlib>

#include "connection.h"
#include "async_dispatcher.h"
#include "async_text_created.h"
#include "async_text_message.h"


Async_dispatcher::Async_dispatcher(Connection *conn)
    : port(conn),
    text_created_admin(NULL), text_message_admin(NULL)
{
    storage = new Async_storage(port);
}


Async_dispatcher::~Async_dispatcher()
{
    delete storage;
    delete text_created_admin;
    delete text_message_admin;
}


// ================================================================


Async_handler_tag<Async_text_created>
Async_dispatcher::register_text_created_handler
(Kom_auto_ptr<Async_callback<Async_text_created> > cb)
{
    if (text_created_admin == NULL) {
	text_created_admin
	= new Async_administrator<Async_text_created>(storage);
    }

    return text_created_admin->register_handler(cb);
}


Async_handler_tag<Async_text_message>
Async_dispatcher::register_text_message_handler
(Kom_auto_ptr<Async_callback<Async_text_message> > cb)
{
    if (text_message_admin == NULL) {
	text_message_admin
	= new Async_administrator<Async_text_message>(storage);
    }

    return text_message_admin->register_handler(cb);
}


void
Async_dispatcher::unregister_text_created_handler(
    Async_handler_tag<Async_text_created> unwanted)
{
    text_created_admin->unregister_handler(unwanted);
}


void
Async_dispatcher::unregister_text_message_handler(
    Async_handler_tag<Async_text_message> unwanted)
{
    text_message_admin->unregister_handler(unwanted);
}


void
Async_dispatcher::do_accept_async()
{
    return storage->do_accept_async();
}


Status
Async_dispatcher::handle_async(int this_many)
{
    return storage->handle_async(this_many);
}
