// Get last text-number written before a specified time.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "get_last_text_q.h"


get_last_text_question::get_last_text_question(Connection *conn, 
					       const struct std::tm &time)
    : question(conn), last(0)
{
    prot_a_LyStr   s;

    s << ref_no() << " 58 "
      << time.tm_sec << ' ' << time.tm_min << ' '
      << time.tm_hour << ' ' << time.tm_mday << ' '
      << time.tm_mon << ' ' << time.tm_year << ' '
      << time.tm_wday << ' ' << time.tm_yday << ' ';
    
    // If the struct std::tm originates from strptime, the tm_isdst
    // field may be set to -1.  Silently use 0 instead, to avoid a
    // protocol error.  This field is anyhow not used by the server in
    // the comparison...
    if (time.tm_isdst < 0)
	s << '0';
    else
	s << time.tm_isdst;

    s << '\n';
    port->send_question(s);
}


get_last_text_question::~get_last_text_question()
{
}


void
get_last_text_question::parser(prot_a_LyStr &str)
{
    last = str.stol();
    state = st_ok;
    deallocate_refno();
}


Text_no
get_last_text_question::result()
{
    return last;
}
