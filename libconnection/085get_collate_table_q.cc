// Ask the server for the current collate table.
//
// Copyright (C) 2005  Per Cederqvist
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "connection.h"
#include "get_collate_table_q.h"

get_collate_table_question::get_collate_table_question(Connection *conn)
    : question(conn)
{
    prot_a_LyStr b;
    b << ref_no() << ' ' << 85 << '\n';
    port->send_question(b);
}

get_collate_table_question::~get_collate_table_question()
{
}

void
get_collate_table_question::parser(prot_a_LyStr& str)
{
    LyStr_errno tmp = str.stos();
    if (tmp.kom_errno != KOM_NO_ERROR)
	receive_err(tmp.kom_errno);
    else
    {
	collate_table = tmp.str;
	state = st_ok;
	deallocate_refno();
    }
}

LyStr
get_collate_table_question::result()
{
    return collate_table;
}
