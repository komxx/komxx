// Create a conference.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "create_conf_old_q.h"
#include "conf-type.h"
    
create_conf_old_question::create_conf_old_question(Connection *conn, 
						   const LyStr &name,
						   const Conf_type &type)
    : question(conn), cno(0)
{
    prot_a_LyStr   s;

    s << ref_no() << " 10 ";
    s.append_hollerith(name);
    s << ' ';
    s << (type.is_protected() ? '1' : '0');
    s << (type.is_original()  ? '1' : '0');
    s << (type.is_secret()    ? '1' : '0');
    s << (type.is_letterbox() ? '1' : '0');
    s << '\n';

    port->send_question(s);
}


create_conf_old_question::~create_conf_old_question()
{
}


void
create_conf_old_question::parser(prot_a_LyStr& str)
{
    cno = str.stol();

    state = st_ok;
    deallocate_refno();
}


Conf_no
create_conf_old_question::result()
{
    return cno;
}
