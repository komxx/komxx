// Remove a footnote-link.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "sub_footnote_q.h"

    
sub_footnote_question::sub_footnote_question(Connection *conn, 
					     Text_no     footnote,
					     Text_no     footnote_to)
    : question(conn)
{
    prot_a_LyStr   s;

    s << ref_no() << " 38 " << footnote << ' ' << footnote_to << '\n';
    port->send_question(s);
}


sub_footnote_question::~sub_footnote_question()
{
}


void
sub_footnote_question::parser(prot_a_LyStr &)
{
    state = st_ok;
    deallocate_refno();
}


void
sub_footnote_question::result()
{
    return;
}
