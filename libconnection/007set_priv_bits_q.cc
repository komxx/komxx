// Set privilige bits of a person.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "set_priv_bits_q.h"
#include "priv-bits.h"

set_priv_bits_question::set_priv_bits_question(Connection *conn, 
					       Pers_no pno, 
					       Priv_bits privs)
  : question(conn)
{
    prot_a_LyStr s;
    
    s << ref_no() << " 7 " << pno << ' '
      << (privs.wheel()       ? '1' : '0')
      << (privs.admin()       ? '1' : '0')
      << (privs.statistic()   ? '1' : '0')
      << (privs.create_pers() ? '1' : '0')
      << (privs.create_conf() ? '1' : '0')
      << (privs.change_name() ? '1' : '0')
      << (privs.flg7()        ? '1' : '0')
      << (privs.flg8()        ? '1' : '0')
      << (privs.flg9()        ? '1' : '0')
      << (privs.flg10()       ? '1' : '0')
      << (privs.flg11()       ? '1' : '0')
      << (privs.flg12()       ? '1' : '0')
      << (privs.flg13()       ? '1' : '0')
      << (privs.flg14()       ? '1' : '0')
      << (privs.flg15()       ? '1' : '0')
      << (privs.flg16()       ? '1' : '0')
      << '\n';

    port->send_question(s);
}

set_priv_bits_question::~set_priv_bits_question()
{
}

void 
set_priv_bits_question::parser(prot_a_LyStr &str)
{
    state = st_ok;
    deallocate_refno();
}

void 
set_priv_bits_question::result()
{
    return;
}
