// Get person_stat.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "get_person_stat_q.h"
#include "connection.h"
#include "pers-stat.h"
    
get_person_stat_question::get_person_stat_question(Connection *conn, 
						   Pers_no pno)
    : question(conn),
      pstat(NULL),
      pers_no(pno)
{
    prot_a_LyStr   s;

    s << ref_no() << " 49 " << pers_no << '\n';
    port->send_question(s);
}


get_person_stat_question::~get_person_stat_question()
{
    if (pstat != NULL)
	delete pstat;
}


void
get_person_stat_question::parser(prot_a_LyStr &str)
{
    pstat = new Pers_stat(pers_no, str);
    state = st_ok;
    deallocate_refno();
}


Pers_stat
get_person_stat_question::result()
{
    return *pstat;
}

