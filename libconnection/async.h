// Low-level handling of asynchronous messages.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_ASYNC_H
#define KOMXX_ASYNC_H

#include <queue>
#include <vector>

#include "misc.h"
#include "kom_memory.h"

const int MAX_ASYNC=255;

class prot_a_LyStr;
class Connection;
class accept_async_question;

// This description is up-to-date with LysKOM server 1.3.3.
enum Async_name {
    ASYNC_NEW_TEXT_OLD,		// 0
    ASYNC_LOGOUT_OBSOLETE,	// 1: OBSOLETE: don't use
    ASYNC_LOGIN_OBSOLETE,	// 2: OBSOLETE: don't use
    ASYNC_CONF_DELETED,		// 3: OBSOLETE: don't use
    ASYNC_CONF_CREATED,		// 4: OBSOLETE: don't use
    ASYNC_CONF_CHANGE_NAME,	// 5
    ASYNC_I_AM_ON,		// 6: Sent when a person logs in,
                                //    changes conference, or sends a 
                                //    "Change what I am doing".
    ASYNC_SYNC_DB,		// 7
    ASYNC_FORCED_LEAVE_CONF,	// 8
    ASYNC_LOGIN,		// 9
    ASYNC_BROADCAST_OBSOLETE,	// 10: OBSOLETE: don't use
    ASYNC_LYSKOM_IS_FULL,	// 11
    ASYNC_TEXT_MESSAGE,		// 12
    ASYNC_LOGOUT,		// 13
    ASYNC_DELETED_TEXT,		// 14
    ASYNC_NEW_TEXT,		// 15
    ASYNC_NEW_RECIPIENT,        // 16
    ASYNC_SUB_RECIPIENT,	// 17 A recipient has been removed from a text
    ASYNC_NEW_MEMBERSHIP,	// 18 A user has been added to a conference
    ASYNC_NEW_USER_AREA,	// 19 A user-area was changed
    ASYNC_NEW_PRESENTATION,	// 20 A presentation was changed
    ASYNC_NEW_MOTD,		// 21 A motd was changed
    ASYNC_TEXT_AUX_CHANGED,	// 22 The aux-item list of a text was changed
    ASYNC_TEXT_READ,		// 23 A text was read in another session
    ASYNC_INVALIDATE_TEXT_READ,	// 24 Recompute what is unread at a later time
};

class Async_message {
    // This is a very small class.
public:
    virtual ~Async_message();
};

class Async_administrator_base {
  public:
    virtual const Async_message *parse(prot_a_LyStr &) =0;
    virtual void handle_message(const Async_message *) =0;
    virtual ~Async_administrator_base();
};

class Async_storage {
  public:
    // Methods intended for use by the end user.
    Async_storage(Connection *);

    Status        handle_async(int this_many=1);
    Connection  * connection() const;

    // Methods intended for use by Async_administrator and its derived classes.
    void          register_administrator(Async_name, 
					 Async_administrator_base*);
    void          do_accept_async();

    // Function intended to be called from class Connection.
    static void   parse_and_store(void *async_storage, 
				  prot_a_LyStr async_msg);

  private:
    Connection           * port;
    bool                   is_registered_with_connection;
    bool                   need_accept_async;
    accept_async_question *pending_accept_q;
    Async_administrator_base *admin_table[MAX_ASYNC];
    struct Pending {
	Async_name             type;
	const Async_message  * object;
    };
    std::queue<Async_storage::Pending>  pending_async;
};

template <class MSG>
class Async_callback {
  public:
    virtual void operator() (const MSG &) =0;
    virtual ~Async_callback() {}
};

template <class MSG, class USER>
class Async_callback_function : public Async_callback<MSG> {
  public:
    Async_callback_function(void (*f)(const MSG &, USER &), USER user);
    ~Async_callback_function() {}
    void operator() (const MSG &);
  private:
    void (*fun)(const MSG &, USER &);
    USER userdata;
};

template<class MSG, class USER>
inline Kom_auto_ptr<Async_callback<MSG> >
async_callback_function(void (*cb)(const MSG &, USER &),
			USER user)
{
    Kom_auto_ptr<Async_callback<MSG> > res(
	new Async_callback_function<MSG, USER>(cb, user));
    return res;
}

template <class MSG, class T>
class Async_callback_method : public Async_callback<MSG>
{
  public:
    Async_callback_method(T *obj, void (T::*m)(const MSG &));
    void operator() (const MSG &);
  private:
    T *object;
    void (T::*member)(const MSG &);
};

template<class MSG, class T>
inline Kom_auto_ptr<Async_callback<MSG> >
async_callback_method(T *obj, void (T::*m)(const MSG &))
{
    Kom_auto_ptr<Async_callback<MSG> > res(
	new Async_callback_method<MSG, T>(obj, m));
    return res;
}

// MSG must be derived from Async_message.
template<class MSG>
class Async_handler_tag {
  public:
    // The end-user is allowed to copy handler-tags...
    Async_handler_tag(const Async_handler_tag<MSG>&);
    Async_handler_tag &operator=(const Async_handler_tag<MSG>&);
    ~Async_handler_tag();

    // ...and to compare them.
    bool operator==(const Async_handler_tag<MSG>&) const;
    bool operator!=(const Async_handler_tag<MSG>&) const;
    bool is_legal() const;

    // This is only intended to be used by Async_administrator<MSG>.
    void handle_message(const MSG *);
    Async_handler_tag(Kom_auto_ptr<Async_callback<MSG> > cb);

    // libg++ needs a default constructor, so give it one.
    // FIXME: Remove when it is no longer needed.
    Async_handler_tag();
  private:
    class Async_handler {
      public:
	void init(Kom_auto_ptr<Async_callback<MSG> > cb);
	void connect();
	void disconnect();

	Kom_auto_ptr<Async_callback<MSG> > handler;
      private:
	int ref_cnt;
#if 0
	// FIXME: This is needed, but let's wait a while with it.
	Set<Async_handler_tag<MSG> > prerequisites;
#endif	
    };
    Async_handler *rep;
};


// MSG must be derived from Async_message.
template <class MSG>
class Async_administrator : public Async_administrator_base {
  public:
    Async_administrator(Async_storage *);
    const Async_message* parse(prot_a_LyStr &); // No default implementation.

    // It is a runtime error if Async_message is not in fact of
    // type MSG, but since this method is called by Async_storage
    // the check cannot be done at compile time.  What a pity.
    void handle_message(const Async_message *);

    Async_handler_tag<MSG>
    register_handler(Kom_auto_ptr<Async_callback<MSG> > cb
#if 0
		     // This is needed, but let's wait a while with it.
		     , Set<Async_handler_tag<MSG> > prerequisites
#endif	
	);
    void unregister_handler(Async_handler_tag<MSG> tag);

    ~Async_administrator();
  private:
    
    std::vector<Async_handler_tag<MSG> > user_handlers;
    Async_storage *storage;
    bool is_registered;

    Async_administrator& operator=(const Async_administrator&); // Non-existing
    Async_administrator(const Async_administrator&);		// Non-existing
};    

#include "async.icc"
#endif
