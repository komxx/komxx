// Get server info.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "get_info_q.h"

    
get_info_question::get_info_question(Connection *conn)
    : question(conn)
{
    prot_a_LyStr   s;

    s << ref_no() << " 94\n";
    port->send_question(s);
}


get_info_question::~get_info_question()
{
    delete server_inf;
}


void
get_info_question::parser(prot_a_LyStr &str)
{
    server_inf = new Server_info(str);

    state = st_ok;
    deallocate_refno();
}


Server_info
get_info_question::result()
{
    return *server_inf;
}
