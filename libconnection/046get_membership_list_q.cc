// Get membership-list.
//
// Copyright (C) 1998  Per Cederqvist
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>

#include "prot-a-LyStr.h"
#include "get_membership_list_q.h"
#include "connection.h"

    
get_membership_list_question::get_membership_list_question(
    Connection *conn,
    Pers_no pno, 
    unsigned short first,
    unsigned short no_of_confs,
    bool want_read_texts)
    : question(conn), 
      pers_no(pno), 
      m_list()
{
    prot_a_LyStr   s;

    s << ref_no() << " 46 " << pno << ' ' << first << ' ' << no_of_confs 
      << (want_read_texts? " 1\n" : " 0\n");
    port->send_question(s);
}


get_membership_list_question::~get_membership_list_question()
{
}


void
get_membership_list_question::parser(prot_a_LyStr &str)
{
    int   arrsize;
    int   i;

    arrsize = str.parse_array_start();
    if (arrsize < 0)
	std::abort();		// +++Error handling;
    else {
	m_list.resize(arrsize);
	for (i = 0; i < arrsize; ++i) 
	{
	    m_list[i] = Membership(pers_no, true, str);
	}
    }
    str.parse_array_end(arrsize);

    state = st_ok;
    deallocate_refno();
}


std::vector<Membership>
get_membership_list_question::result()
{
    return m_list;
}
