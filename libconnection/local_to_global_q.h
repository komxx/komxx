// Get map from local text-number to global text-number.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_LOCAL_TO_GLOBAL_Q_H
#define KOMXX_LOCAL_TO_GLOBAL_Q_H

#include "kom-types.h"
#include "question.h"

class local_to_global_map;

// 103

class local_to_global_question : public question {
  public:
    typedef local_to_global_map result_type;

    local_to_global_question(Connection *conn, 
			     Conf_no cno, 
			     Local_text_no first_local_no, 
			     int no_of_existing_texts);
    ~local_to_global_question();
    void parser(prot_a_LyStr &str);
    result_type result();
  private:
    local_to_global_map *tlist;
};

#endif
