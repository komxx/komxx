// Handling and recycling question reference number.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_REFNO_H
#define KOMXX_REFNO_H

#include "misc.h"

// Ref_no, reused.
class Ref_no {
  public:
    Ref_no();			// Get an unused ref_no
    ~Ref_no();			// Return the no to the pool of unused ones.
    int ref_no() const;		// Query the ref_no
  private:
    int ref;
    Ref_no(const Ref_no &);
    Ref_no &operator =(const Ref_no &);
};

class question;

question *lookup_refno(int r);

#endif
