// Get map from local-text-no to global text-no.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "local_to_global_q.h"
#include "connection.h"
#include "local-to-global-map.h"
    
local_to_global_question::local_to_global_question(Connection *conn,
				   Conf_no cno, 
				   Local_text_no first_local_no, 
				   int no_of_texts)
    : question(conn), tlist(NULL)
{
    prot_a_LyStr out;

    out << ref_no() << " 103 " << cno << ' ' << first_local_no
	<< ' ' << no_of_texts << '\n';
    port->send_question(out);
}


local_to_global_question::~local_to_global_question()
{
    delete tlist;
}


void
local_to_global_question::parser(prot_a_LyStr &str)
{
    tlist = new local_to_global_map(str);
    state = st_ok;
    deallocate_refno();
}


local_to_global_map
local_to_global_question::result()
{
    return *tlist;
}
