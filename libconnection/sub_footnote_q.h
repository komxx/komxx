// Remove a footnote-link.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_SUB_FOOTNOTE_Q_H
#define KOMXX_SUB_FOOTNOTE_Q_H

#include "kom-types.h"
#include "question.h"

// 38

class sub_footnote_question : public question {
  public:
    typedef void result_type;

    sub_footnote_question(Connection *conn,
			  Text_no footnote, 
			  Text_no footnote_to);
    ~sub_footnote_question();
    void parser(prot_a_LyStr &str);
    result_type result();
};

#endif
