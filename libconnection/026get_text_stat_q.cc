// Get text-stat.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "get_text_stat_old_q.h"
#include "connection.h"
#include "text-stat.h"
    
get_text_stat_old_question::get_text_stat_old_question(Connection *conn, 
						       Text_no tno)
    : question(conn), text_no(tno), text_stat(NULL)
{
    prot_a_LyStr   s;

    s << ref_no() << " 26 " << text_no << '\n';
    port->send_question(s);
}


get_text_stat_old_question::~get_text_stat_old_question()
{
    if (text_stat != NULL)
	delete text_stat;
}


void
get_text_stat_old_question::parser(prot_a_LyStr &str)
{
    text_stat = new Text_stat(text_no, true, str);
    state = st_ok;
    deallocate_refno();
}


Text_stat
get_text_stat_old_question::result()
{
    return *text_stat;
}
