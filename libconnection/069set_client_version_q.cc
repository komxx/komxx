// Inform the server about the client version.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "set_client_version_q.h"

    
set_client_version_question::set_client_version_question(Connection *conn,
						   const LyStr &client_name,
						   const LyStr &client_version)
    : question(conn)
{
    if (conn->get_server_info()->version() >= 10400) 
    {
	prot_a_LyStr   s;

	s << ref_no() << " 69 ";
	s.append_hollerith(client_name);
	s << ' ';
	s.append_hollerith(client_version);
	s << '\n';
	port->send_question(s);
    }
    else 
    {
	// Pretend that everything went OK if we are connected to an
	// ancient server.
	state = st_ok;
	deallocate_refno();
    }
}


set_client_version_question::~set_client_version_question()
{
}


void
set_client_version_question::parser(prot_a_LyStr &)
{
    state = st_ok;
    deallocate_refno();
}


void
set_client_version_question::result()
{
}
