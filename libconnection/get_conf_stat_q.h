// Get conference status.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_GET_CONF_STAT_Q_H
#define KOMXX_GET_CONF_STAT_Q_H

#include "kom-types.h"
#include "question.h"

class Conf_stat;

// 50

class get_conf_stat_question : public question {
  public:
    typedef Conf_stat result_type;

    get_conf_stat_question(Connection *conn, Conf_no conf_no);
    ~get_conf_stat_question();
    void parser(prot_a_LyStr &str);
    result_type result();
  private:
    Conf_no requested_conf_no;
    Conf_stat *the_conf;
};

#endif
