// Ask the server about some possible unread conferences.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>

#include "prot-a-LyStr.h"
#include "get_unread_confs_q.h"
#include "connection.h"

    
get_unread_confs_question::get_unread_confs_question(Connection *conn, 
						     Pers_no pno)
    : question(conn), conf_no_list()
{
    prot_a_LyStr   s;

    s << ref_no() << " 52 " << pno << '\n';
    port->send_question(s);
}


get_unread_confs_question::~get_unread_confs_question()
{
}


void
get_unread_confs_question::parser(prot_a_LyStr &str)
{
    int   arrsize;
    int   i;

    arrsize = str.parse_array_start();
    if (arrsize < 0)
	std::abort();
    else {
	conf_no_list.resize(arrsize);
	for (i = 0; i < arrsize; ++i) {
	    conf_no_list[i] = Conf_no(str.stol());
	}
    }
    str.parse_array_end(arrsize);

    state = st_ok;
    deallocate_refno();
}


std::vector<Conf_no>
get_unread_confs_question::result()
{
    return conf_no_list;
}
