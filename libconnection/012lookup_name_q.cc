// Perform a name lookup.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdlib>
#include <iostream>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "lookup_name_q.h"

    
lookup_name_question::lookup_name_question(Connection *conn,
					   const LyStr &template_str)
    : question(conn), matches()
{
    prot_a_LyStr   s;

    s << ref_no() << " 12 ";
    s.append_hollerith(template_str);
    s << '\n';
    port->send_question(s);
}


lookup_name_question::~lookup_name_question()
{
}


void
lookup_name_question::parser(prot_a_LyStr &str)
{
    int   arrsize;
    int   i;

    arrsize = str.parse_array_start();
    matches.resize(arrsize);

    // Parse all Conf_nos first...
    for (i = 0; i < arrsize; ++i) 
	matches[i].parse_conf_no(str);
    str.parse_array_end(arrsize);

    // ...then parse all Conf_types.
    str.skipspace();
    if (str.strlen() == 0 || (arrsize > 0 && str[0] != '{')) {
	std::cerr << "lookup_name_question::parser(): protocol error\n";
	std::abort();
    } else
	str.remove_first(1);
    for (i = 0; i < arrsize; ++i) 
	matches[i].parse_conf_type(str);
    str.parse_array_end(arrsize);

    state = st_ok;
    deallocate_refno();
}


std::vector<Micro_conf>
lookup_name_question::result()
{
    return matches;
}
