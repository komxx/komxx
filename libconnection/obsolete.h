// Obsolete questions.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_OBSOLETE_H
#define KOMXX_OBSOLETE_H
// This file is used for all obsolete calls to the LysKOM server.
// These should not be used, and it's no use bothering to write the
// corresponding code.

#include <vector>

#include "kom-types.h"
#include "question.h"
#include "who-info-old.h"
#include "who-info.h"

// 0
class login_old_question : public question {
  public:
    typedef void result_type;

    login_old_question(Connection *conn, Pers_no person, const LyStr &passwd);
    ~login_old_question();
    void parser(prot_a_LyStr &str);
    result_type result();
};

// 6

//class get_person_stat_old_question : public question {
//  public:
//    get_person_stat_old_question(Connection *conn, Pers_no pno, int flags);
//    ~get_person_stat_old_question();
//    void parser(prot_a_LyStr &str);
//    Pers_stat result();
//  private:
//    Pers_stat *person;
//};

// 13

//class get_conf_stat_old_question : public question {
//  public:
//    get_conf_stat_old_question(Connection *conn, Conf_no cno, int flags);
//    ~get_conf_stat_old_question();
//    void parser(prot_a_LyStr &str);
//    Conf_stat result();
//  private:
//    Conf_stat *conf;
//};

// 24

class mark_text_old_question : public question {
  public:
    typedef void result_type;

    mark_text_old_question(Connection *conn, Text_no text,
			   unsigned char mark_type);
    ~mark_text_old_question();
    void parser(prot_a_LyStr &str);
    result_type result();
};

// 39

//class who_is_on_old_question : public question {
//  public:
//    who_is_on_old_question(Connection *conn);
//    ~who_is_on_old_question();
//    void parser(prot_a_LyStr &str);
//    SLList<Who_info_old> result();
//  private:
//    SLList<Who_info_old> wlist;
//};

// 45

//class broadcast_question : public question {
//  public:
//    broadcast_question(Connection *conn, const LyStr &message);
//    ~broadcast_question();
//    void parser(prot_a_LyStr &str);
//    void result();
//};

// 51

class who_is_on_question : public question {
  public:
    typedef std::vector<Who_info> result_type;

    who_is_on_question(Connection *conn);
    ~who_is_on_question();
    void parser(prot_a_LyStr &str);
    result_type result();
  private:
    std::vector<Who_info> who_info_list;
};

// 54

//class get_session_info_question : public question {
//  public:
//    get_session_info_question(Connection *conn, Session_no session);
//    ~get_session_info_question();
//    void parser(prot_a_LyStr &str);
//    Session_info result();
//  private:
//    Session_info *info;
//};

// 64

//class Session_info_ident {
//  public:
//    Session_info_ident();
//    Session_info_ident(const Session_info_ident&);
//    const Session_info_ident &operator =(const Session_info_ident &);
//  private:
//    Pers_no	person;
//    LyStr	what_am_i_doing;
//    LyStr	username; /* Userid and hostname,
//			     according to the client. */
//    LyStr	ident_user; /* According to Ident protocol. */
//    LyStr	hostname;   /* According to TCP/IP. */
//    Conf_no	working_conference;
//    Session_no	session;	/* Serial number of connection. */
//    struct tm	connection_time; /* Not logintime. */
//    unsigned long idle_time; /* Seconds. */
//};
//
//class get_session_info_ident_question : public question {
//  public:
//    get_session_info_ident_question(Connection *conn, Session_no session);
//    ~get_session_info_ident_question();
//    void parser(prot_a_LyStr &str);
//    Session_info_ident result();
//  private:
//    Session_info_ident *info;
//};

// 66

//class re_lookup_conf_question : public question {
//  public:
//    re_lookup_conf_question(Connection *conn, const LyStr &pattern);
//    ~re_lookup_conf_question();
//    void parser(prot_a_LyStr &str);
//    std::vector<Conf_no> result();
//  private:
//    std::vector<Conf_no> matches;
//};

// 67

//class lookup_person_question...

// 68

//class lookup_conf_question...

#endif
