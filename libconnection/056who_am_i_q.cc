// Ask about who I am.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "who_am_i_q.h"
#include "connection.h"

    
who_am_i_question::who_am_i_question(Connection *conn)
    : question(conn)
{
    prot_a_LyStr   s;

    s << ref_no() << " 56" << '\n';
    port->send_question(s);
}


who_am_i_question::~who_am_i_question()
{
}


void
who_am_i_question::parser(prot_a_LyStr &str)
{
    // +++ tty has a funny name!
    tty = str.stol();

    state = st_ok;
    deallocate_refno();
}


Session_no
who_am_i_question::result()
{
    return tty;
}
