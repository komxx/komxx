// Inline methods for asynchronous message handling.
// -*- C++ -*-
// Copyright (C) 1995  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

template <class MSG, class USER>
inline
Async_callback_function<MSG, USER>
::Async_callback_function(void (*f)(const MSG &, USER &), USER user)
  : fun(f),
    userdata(user)
{
}

template <class MSG, class USER>
inline void
Async_callback_function<MSG, USER>::operator() (const MSG &msg)
{
    fun(msg, userdata);
}

template <class MSG, class T>
inline
Async_callback_method<MSG, T>::Async_callback_method(T *obj,
						     void (T::*m)(const MSG &))
  : object(obj),
    member(m)
{
}

template <class MSG, class T>
inline void
Async_callback_method<MSG, T>::operator() (const MSG &msg)
{
    (object->*member)(msg);
}

