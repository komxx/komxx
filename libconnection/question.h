// Base clase for all question-sending classes.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_QUESTION_H
#define KOMXX_QUESTION_H

#include "misc.h"
#include "error-codes.h"
#include "error-aggregate.h"

class prot_a_LyStr;
class Ref_no;
class Connection;

// Send questions to LysKOM server

// Some things that are common to all different types of questions.
class question {
  public:
    question(Connection *conn);	// Allocates ref_no. The ref_no is
				// destructed when the reply comes.
    virtual ~question();	// Destructor.
    virtual  Status status();	// What is the status of this question?
    virtual  Status receive();	// Wait for reply.
    virtual  Kom_err error();	// The error code, or KOM_NO_ERROR. Only 
				// valid if status() != st_pending.
    Kom_err error_blocking();   // Short for q->receive(); return q->error();
    virtual void parser(prot_a_LyStr &str) =0;
    void receive_err(int err);	// Deallocates ref_no.
  protected:
    Status state;		// Updated by all parsers.
    void deallocate_refno();	// Should be called from all
				// parser()s.
    Connection *port;
    int ref_no() const;
  private:
    Kom_err errcode;
    Ref_no *refno;
};

template <class Q>
Kom_res<typename Q::result_type>
q_result(Q *q)
{
    if (q->receive() == st_ok)
	return q->result();
    else
	return q->error();
}

#endif
