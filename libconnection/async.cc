// Low-level handling of asynchronous messages.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>
#include <algorithm>
#include <stdint.h>

#include "async.h"
#include "prot-a-LyStr.h"
#include "connection.h"
#include "accept_async_q.h"

// ================================================================
//                  Async_message methods

Async_message::~Async_message()
{
}

// ================================================================
//                  Async_administrator_base methods

Async_administrator_base::~Async_administrator_base()
{
}

// ================================================================
//                  Async_storage methods

Async_storage::Async_storage(Connection *c)
  : port(c),
    is_registered_with_connection(false),
    need_accept_async(false),
    pending_accept_q(NULL),
    pending_async()
{
    for (int i=0; i < MAX_ASYNC; i++)
	admin_table[i] = NULL;
}

Status 
Async_storage::handle_async(int this_many)
{
    if (need_accept_async || pending_accept_q != NULL)
	do_accept_async();

    while (this_many-- > 0 && !pending_async.empty())
    {
	Pending next = pending_async.front();
	pending_async.pop();
	admin_table[next.type]->handle_message(next.object);
    }

    return pending_async.empty() ? st_ok : st_pending;
}


void
Async_storage::do_accept_async()
{
    if (need_accept_async && pending_accept_q == NULL)
    {
	std::vector<uint32_t> wanted;
	
	for (int i = 0; i < MAX_ASYNC; i++)
	    if (admin_table[i] != NULL)
		wanted.push_back(i);
	
	pending_accept_q = new accept_async_question(port, wanted);
	need_accept_async = false;
    }
	
    if (pending_accept_q != NULL && pending_accept_q->status() != st_pending)
    {
	if (pending_accept_q->receive() != st_ok)
	{
	    // FIXME: We should maybe warn about this, somehow.
	    // We are possibly attempting to use an async message that
	    // the server does not understand.
	}
	delete pending_accept_q;
	pending_accept_q = NULL;
    }
}


Connection *
Async_storage::connection() const
{
    return port;
}

void 
Async_storage::register_administrator(Async_name n, 
				      Async_administrator_base* adm)
{
    assert(0 <= n);
    assert(n < MAX_ASYNC);
    // We can only have one administrator per asynchronous message.
    // That is as it should be -- the user can register several
    // handlers with the administrator.
    assert(admin_table[n] == NULL);

    admin_table[n] = adm;
    need_accept_async = true;
    if (!is_registered_with_connection)
    {
	port->register_async((void*)this, Async_storage::parse_and_store);
	is_registered_with_connection = true;
    }
}

void 
Async_storage::parse_and_store(void *async_storage,
			       prot_a_LyStr async_msg)
{
    Async_storage *ast = (Async_storage *)async_storage;

    async_msg.stol();			// Skip 'number of items' field.
    long type = async_msg.stol();

    if (type >= 0 && type < MAX_ASYNC && ast->admin_table[type] != NULL)
    {
	Pending foo;   
	foo.object = ast->admin_table[type]->parse(async_msg);
	foo.type = Async_name(type);
	ast->pending_async.push(foo);
    }
}

// ================================================================
//                  Async_handler_tag methods

template<class MSG>
Async_handler_tag<MSG>::Async_handler_tag(const Async_handler_tag<MSG> &h)
    : rep(h.rep)
{
    if (rep != NULL)
	rep->connect();
}

template<class MSG>
Async_handler_tag<MSG> &
Async_handler_tag<MSG>::operator=(const Async_handler_tag<MSG> &h)
{
    if (this != &h)
    {
	if (rep != NULL)
	    rep->disconnect();
	rep = h.rep;
	if (rep != NULL)
	    rep->connect();
    }
    return *this;
}

template<class MSG>
Async_handler_tag<MSG>::~Async_handler_tag()
{
    if (rep != NULL)
	rep->disconnect();
}

template<class MSG>
bool
Async_handler_tag<MSG>::operator==(const Async_handler_tag<MSG> &h) const
{
    return rep == h.rep;
}

template<class MSG>
bool
Async_handler_tag<MSG>::operator!=(const Async_handler_tag<MSG> &h) const
{
    return rep != h.rep;
}

template<class MSG>
bool
Async_handler_tag<MSG>::is_legal() const
{
    return rep != NULL;
}

template<class MSG>
void
Async_handler_tag<MSG>::handle_message(const MSG *m)
{
    if (rep != NULL)
	(*rep->handler)(*m);
}

template<class MSG>
Async_handler_tag<MSG>::Async_handler_tag(Kom_auto_ptr<Async_callback<MSG> >
					  cb)
    : rep(new Async_handler)
{
    rep->init(cb);
}

template<class MSG>
Async_handler_tag<MSG>::Async_handler_tag()
    : rep(NULL)
{
}

template<class MSG>
void
Async_handler_tag<MSG>::Async_handler::init(Kom_auto_ptr<Async_callback<MSG> >
					    cb)
{
    handler = cb;
    ref_cnt = 1;
}

template<class MSG>
void
Async_handler_tag<MSG>::Async_handler::connect()
{
    ref_cnt++;
}

template<class MSG>
void
Async_handler_tag<MSG>::Async_handler::disconnect()
{
    if (--ref_cnt == 0)
    {
	// Commit suicide.  Suicide is painless, according to Q38
	// in the C++ FAQ posted 8 May 1995 to comp.lang.c++ from
	// Paradigm Shift, Inc.  I hope they know what they are
	// talking about.
	delete this;
    }
}

// ================================================================
//                  Async_administrator methods

template <class MSG>
Async_administrator<MSG>::Async_administrator(Async_storage *ast)
  : user_handlers(),
    storage(ast),
    is_registered(false)
{
}

// Note: there is no generic implementation for the parse method.
// Specialized versions are provided by async_new_text.cc and
// similar files.

template <class MSG>
void
Async_administrator<MSG>::handle_message(const Async_message *m)
{
    const MSG *msg = dynamic_cast<const MSG*>(m);
    assert(msg != NULL);

    for(typename std::vector<Async_handler_tag<MSG> >::iterator iter 
	    = user_handlers.begin();
	iter != user_handlers.end();
	++iter)
    {
	(*iter).handle_message(msg);
    }
}

template <class MSG>
Async_handler_tag<MSG>
Async_administrator<MSG>::register_handler(Kom_auto_ptr<Async_callback<MSG> >
					   cb)
{
    Async_handler_tag<MSG> tag(cb);
    user_handlers.push_back(tag);
    if (!is_registered)
    {
	storage->register_administrator(MSG::name(), this);
	is_registered = true;
    }
    return tag;
}

template <class MSG>
void
Async_administrator<MSG>::unregister_handler(Async_handler_tag<MSG> tag)
{
    typedef typename std::vector<Async_handler_tag<MSG> >::iterator ITER;

    ITER iter = std::remove(user_handlers.begin(), user_handlers.end(), tag);

    // Removing a handler twice is probably bad.
    assert(iter != user_handlers.end());

    user_handlers.erase(iter, user_handlers.end());
}


template <class MSG>
Async_administrator<MSG>::~Async_administrator()
{
}

// FIXME: instantiate in separate files!
#include "async_text_message.h"
template class Async_handler_tag<Async_text_message>;
template class Async_administrator<Async_text_message>;

#include "async_text_created.h"
template class Async_handler_tag<Async_text_created>;
template class Async_administrator<Async_text_created>;
