// Lookup a person name with a regexp search.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "re_lookup_person_q.h"

    
re_lookup_person_question::re_lookup_person_question(Connection *conn,
						     const LyStr &pattern)
    : question(conn), matches()
{
    prot_a_LyStr   s;

    s << ref_no() << " 65 ";
    s.append_hollerith(pattern);
    s << '\n';
    port->send_question(s);
}


re_lookup_person_question::~re_lookup_person_question()
{
}


void
re_lookup_person_question::parser(prot_a_LyStr &str)
{
    int   arrsize;
    int   i;

    arrsize = str.parse_array_start();
    matches.resize(arrsize);
    for (i = 0; i < arrsize; ++i) {
	matches[i] = str.stol();
    }
    str.parse_array_end(arrsize);

    state = st_ok;
    deallocate_refno();
}


std::vector<Pers_no>
re_lookup_person_question::result()
{
    return matches;
}
