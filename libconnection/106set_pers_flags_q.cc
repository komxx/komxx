// Set personal flags of a person.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "set_pers_flags_q.h"
#include "personal-flags.h"

set_pers_flags_question::set_pers_flags_question(Connection *conn, 
						 Pers_no pno, 
						 Personal_flags flags)
  : question(conn)
{
    prot_a_LyStr s;
    
    s << ref_no() << " 106 " << pno << ' '
      << (flags.unread_is_secret()       ? '1' : '0')
      << (flags.flg2()        ? '1' : '0')
      << (flags.flg3()        ? '1' : '0')
      << (flags.flg4()        ? '1' : '0')
      << (flags.flg5()        ? '1' : '0')
      << (flags.flg6()        ? '1' : '0')
      << (flags.flg7()        ? '1' : '0')
      << (flags.flg8()        ? '1' : '0')
      << '\n';

    port->send_question(s);
}

set_pers_flags_question::~set_pers_flags_question()
{
}

void 
set_pers_flags_question::parser(prot_a_LyStr &str)
{
    state = st_ok;
    deallocate_refno();
}

void 
set_pers_flags_question::result()
{
    return;
}
