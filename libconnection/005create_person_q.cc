// Create a person.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "create_person_old_q.h"

    
create_person_old_question::create_person_old_question(Connection   *conn, 
						       const LyStr  &name, 
						       const LyStr  &passwd)
    : question(conn), pno(0)
{
    prot_a_LyStr   s;

    s << ref_no() << " 5 ";
    s.append_hollerith(name);
    s << ' ';
    s.append_hollerith(passwd);
    s << '\n';
    port->send_question(s);
}


create_person_old_question::~create_person_old_question()
{
}


void
create_person_old_question::parser(prot_a_LyStr& str)
{
    pno = str.stol();

    state = st_ok;
    deallocate_refno();
}


Pers_no
create_person_old_question::result()
{
    return pno;
}
