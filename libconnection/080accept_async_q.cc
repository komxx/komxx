// Tell the server which async messages we want.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "accept_async_q.h"
    
accept_async_question::accept_async_question(
    Connection *conn,
    const std::vector<uint32_t>  &request_list)
  : question(conn)
{
    typedef std::vector<uint32_t>::const_iterator RI;

    prot_a_LyStr   s;
    int len = request_list.size();

    s << ref_no() << " 80 " << len << " { ";
    for (RI iter = request_list.begin(); iter != request_list.end(); ++iter)
	s << *iter << ' ';
    s << "}\n";

    port->send_question(s);
}


accept_async_question::~accept_async_question()
{
}


void
accept_async_question::parser(prot_a_LyStr& str)
{
    state = st_ok;
    deallocate_refno();
}


void
accept_async_question::result()
{
}
