// Handle a connection to a LysKOM server.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <pwd.h>
#include <sys/param.h>
#include <unistd.h>
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <cassert>
#include <cstdio>

#include "connection.h"
#include "refno.h"
#include "question.h"
#include "async.h"
#include "get_info_q.h"
#include "conn_io.h"

Connection::Connection(Conn_io *io)
  : conn_io(io),
    hunt_nl_index(0),
    str_skip_left(0),
    strlen_gather(0),
    server_info(), server_info_fetched(false),
    async_sink(NULL),
    sync_sink_data(NULL),
    m_generated_questions(-1),	// Account for the "A3Hfoo".
    m_received_asyncs(0),
    m_ignored_asyncs(0),
    m_received_replies(0)
{
    if (conn_io->connection_failed())
	return;

    conn_io->set_connection(this);

    // Do protocol.

    char *name = NULL;
    name = getlogin();
    if (name == NULL)
    {
	struct passwd *ps = getpwuid(getuid());
	if (ps == NULL || ps->pw_name == NULL)
	{
	    static char buf[16];
	    sprintf(buf, "uid%ld", (long)getuid());
	    name = buf;
	}
	else
	    name = ps->pw_name;
    }

    LyStr me;
    me << name << '%' << conn_io->localhost();
    prot_a_LyStr helo("A");
    helo.append_hollerith(me);
    helo << "\r\n";
    send_question(helo);	// Technically this isn't a question, but...
    LysKOM_gotten = 7;
}

Connection::~Connection()
{
}

bool
Connection::connection_failed() const
{
    if (conn_io == NULL || conn_io->connection_failed())
	return true;

    return false;
}


// Receive an event that is intended for this connection. This should
// never receive ISC_EVENT_TIMEOUT events, since they are handled in
// the callers of this method.
// Return values:
//   st_ok: A package was handled successfully
//   st_error: A unrecoverable error occured. Close this connection
//             and open a new one, or terminate the application.
//   st_pending: The event was a noop.
Status
Connection::parse()
{
    Status retval = st_pending;

    while (LysKOM_gotten && unparsed.strlen())
    {
	assert(LysKOM_gotten == 1
	       || unparsed[0] == "MOKsyL"[LysKOM_gotten-2]);
	LysKOM_gotten--;
	unparsed.remove_first(1);
    }
	
    // We collect input in UNPARSED until we have an entire
    // packet (either a reply or an async message). The
    // end-of-packet is detected by looking for a newline that
    // is not part of a string.
	
    while (hunt_nl_index < unparsed.strlen())
    {
	if (str_skip_left > 0)
	{
	    // We are currently in the middle of a string.
		
	    if (str_skip_left + hunt_nl_index <= unparsed.strlen())
	    {
		hunt_nl_index += str_skip_left;
		str_skip_left = 0;
	    }
	    else
	    {
		str_skip_left -= (unparsed.strlen() - hunt_nl_index);
		hunt_nl_index = unparsed.strlen();
	    }
	}
	else
	{
	    // We are not inside a string now.
		
	    switch (unparsed[hunt_nl_index++])
	    {
	    case ' ':
		// Skip whitespace.
		strlen_gather = 0;
		break;
	    case 'H':
		// We are entering a string.
		str_skip_left = strlen_gather;
		strlen_gather = 0;
		break;
	    case '0': case '1': case '2':
	    case '3': case '4': case '5':
	    case '6': case '7': case '8':
	    case '9':
		// We might be entering a string. Or, this may
		// be any number (or even a bit string).
		strlen_gather *= 10;
		strlen_gather += unparsed[hunt_nl_index-1] - '0';
		break;
		    
	    case '{':
	    case '}':
	    case ':':
	    case '=':
	    case '+':
	    case '%':
	    case '*':
		// Some special characters that need no
		// special attention.
		break;
		    
	    case '\n':
		// We got an entire package. Handle it.
		handle_package();
		retval = st_ok;
		break;
		    
	    default:
		// Unexpected character.
		std::cerr << "Protocol error!\n";
		std::cerr << "Bad character: "
			  << unparsed[hunt_nl_index-1]
			  << " (" << (int)unparsed[hunt_nl_index-1]
			  << " decimal).\nPackage size: "
			  << unparsed.strlen()
			  << "\nIndex of error: "
			  << hunt_nl_index-1
			  << "\nThe packet: >"
			  << unparsed << "<\n";
		assert(0);
	    }
	}
    }
    return retval;
}

Status
Connection::drain(int timeout)
{
    Status retval = conn_io->receive(timeout);
    
    parse();

    return retval;
}

void 
Connection::got_data(const prot_a_LyStr &buf)
{
    unparsed << buf;
    
}

void 
Connection::got_data(const LyStr &buf)
{
    unparsed << buf;
}

void 
Connection::got_data(const char *buf, long len)
{
    unparsed.append(buf, len);
}

void
Connection::handle_package()
{
    // Remember how much that should be left in unparsed when we have
    // handled this package.
    long rest = unparsed.strlen() - hunt_nl_index;
    
    if (unparsed[0] == ':')
    {
	unparsed.remove_first(1);
	hunt_nl_index--;
	++m_received_asyncs;
	if (async_sink != NULL)
	    (*async_sink)(sync_sink_data, unparsed.substr(hunt_nl_index));
	else
	    ++m_ignored_asyncs;
    }
    else
    {
	int ok=0;
	++m_received_replies;
	switch(unparsed[0])
	{
	case '=':
	    ok=1;
	    break;
	case '%':
	    ok=0;
	    break;
	default:
	    std::abort();
	}
	
	unparsed.remove_first(1);
	int r = unparsed.stol();
	
	if (ok)
	{
	    assert(lookup_refno(r) != NULL);
	    lookup_refno(r)->parser(unparsed);
	}
	else
	{
	    int error = unparsed.stol();
	    // unparsed.stol();	// Skip the err_info.
	    assert(lookup_refno(r) != NULL);
	    lookup_refno(r)->receive_err(error);
	}
    }
    
    unparsed.remove_first(unparsed.strlen() - rest);
    hunt_nl_index = 0;
}

void
Connection::send_question(const prot_a_LyStr &str)
{
    // FIXME: Should limit the number of outstanding questions
    // so that important questions can jump ahead in the queue.

    char *p = str.malloc_copy();
    conn_io->queue_for_send(p, str.strlen());
    std::free(p);
    ++m_generated_questions;
}

void
Connection::register_async(void *vp, void (*new_sinc)(void *, prot_a_LyStr))
{
    assert(async_sink == NULL);
    sync_sink_data = vp;
    async_sink = new_sinc;
}


Server_info *
Connection::get_server_info()
{
    if (server_info_fetched == false)
    {
	get_info_question   get_info_q(this);

	if (get_info_q.receive() == st_ok) 
	{
	    server_info = get_info_q.result();
	    server_info_fetched = true;
	} 
	else 
	{
	    std::abort();		// +++Error handling
	}
    }
    
    return &server_info;
}

void
Connection::invalidate_server_info()
{
    server_info_fetched = false;
}

long 
Connection::generated_questions() const
{
    return m_generated_questions;
}

long 
Connection::received_asyncs() const
{
    return m_received_asyncs;
}

long 
Connection::ignored_asyncs() const
{
    return m_ignored_asyncs;
}

long 
Connection::received_replies() const
{
    return m_received_replies;
}
