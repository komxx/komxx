// Ask the server about some possible unread conferences.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <vector>
#include <cstdlib>

#include "prot-a-LyStr.h"
#include "get_members_old_q.h"
#include "connection.h"

    
get_members_old_question::get_members_old_question(
    Connection    * conn, 
    Conf_no         cno,
    unsigned short  first,
    unsigned short  no_of_members)
  
  : question(conn), member_list()
{
    prot_a_LyStr   s;

    s << ref_no() << " 48 " << cno << " " << first << " " 
      << no_of_members <<'\n';
    port->send_question(s);
}


get_members_old_question::~get_members_old_question()
{
}


void
get_members_old_question::parser(prot_a_LyStr &str)
{
    str.parse_array(member_list);
    state = st_ok;
    deallocate_refno();
}


std::vector<Pers_no>
get_members_old_question::result()
{
    return member_list;
}
