// Concrete I/O to the LysKOM server using ISC.
//
// Copyright (C) 2002  Per Cederqvist
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <iostream>
#include <cassert>
#include <cstdlib>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "conn_io_isc.h"

IscMaster          *Conn_io_ISC::mcb = NULL;
Conn_io_ISC        *Conn_io_ISC::the_one_and_only_connection = NULL;
std::queue<IscEvent *> Conn_io_ISC::external_events;

Conn_io_ISC::Conn_io_ISC(const char *hostname,
			 const char *portnum, 
			 IscMaster *master)
  : Conn_io(),
    server(NULL)
{
    IscConfig config;

    // +++Can not currently have more than one connection.
    assert(mcb == NULL);
    assert(the_one_and_only_connection == NULL);

    the_one_and_only_connection = this;

    if (master != NULL)
	mcb = master;
    else
    {
	// Create a master.
	/*
	** Setup some parameters here
	*/
	config.version = 1006;
	config.master.version = 1001;
	config.master.memfn.alloc = NULL; // Use default allocators.
	config.master.memfn.realloc = NULL;
	config.master.memfn.free = NULL;
	config.master.abortfn = NULL; /* Use default abort function. */
	config.session.version = 1002;
	config.session.max.msgsize = -1; /* Use default sizes. */
	config.session.max.queuedsize = -1;
	config.session.max.dequeuelen = -1;
	config.session.max.openretries = -1;
	config.session.max.backlog = -1;
	config.session.fd_relocate = 0;

	mcb  = isc_initialize(&config);
	if ( mcb == NULL )
	{
	    std::cerr << "can't isc_initialize()\n";
	    return;
	}
    }

    server = isc_opentcp(mcb, hostname, portnum);
}

bool 
Conn_io_ISC::connection_failed() const
{
    if (server == NULL)
	return true;

    while (server->state == ISC_STATE_CONNECTING)
    {
	IscEvent *ev = isc_wait(mcb, 1000);
	// The server session is in state ISC_STATE_CONNECTING,
	// so an event of some kind should always be returned.
	assert (ev != NULL);
	if (ev->event == ISC_EVENT_ERROR)
	{
	    isc_dispose(ev);
	    return true;
	}
	isc_dispose(ev);
    }

    // FIXME: should some memory be freed here if the connection failed?
    if (server->state == ISC_STATE_RUNNING)
	return false;
    else
	return true;
}

Conn_io_ISC::~Conn_io_ISC()
{
    // Note: we do not delete these, since we do not own them.
    mcb = NULL;
    the_one_and_only_connection = NULL;
}

void 
Conn_io_ISC::queue_for_send(char *block, int len)
{
    isc_write(server, block, len);
}

Status 
Conn_io_ISC::receive(int timeout)
{
    if (mcb == NULL || the_one_and_only_connection->server == NULL)
	return st_error;

    IscEvent *ev;
    int errs = 0;

    while ((ev = isc_getnextevent(mcb, timeout)) != NULL)
    {
	if (ev->event == ISC_EVENT_ERROR)
	{
	    // FIXME: Improve error handling.
	    std::cerr << "isc_getnextevent error: " << ev->msg->buffer
		      << std::endl;
	    sleep(1);
	    if (errs++ > 10)
		return st_error;
	}
	else if (ev->event == ISC_EVENT_TIMEOUT)
	{
	    isc_dispose(ev);
	    return st_pending;
	}
	else if (ev->session == the_one_and_only_connection->server)
	{
	    // Need support for multiple servers here.
	    Status retval = the_one_and_only_connection->receive_event(ev);
	    isc_dispose(ev);
	    return retval;
	}
	else
	{
	    assert(ev->session != NULL);
	    timeout = 0;
	    external_events.push(ev);
	}
    }

    return st_error;
}

IscEvent Conn_io_ISC::error_event;
IscEvent Conn_io_ISC::timeout_event;

IscEvent *
Conn_io_ISC::get_event(int timeout)
{
    if (external_events.empty())
    {
	switch (the_one_and_only_connection->receive(timeout))
	{
	case st_ok:
	    break;
	case st_pending:
	    return &timeout_event;
	case st_error:
	    return &error_event;
	}
    }

    if (!external_events.empty())
    {
	IscEvent *result = external_events.front();
	external_events.pop();
	return result;
    }

    return NULL;
}


Status 
Conn_io_ISC::receive_event(IscEvent *ev)
{
    switch(ev->event)
    {
    case ISC_EVENT_CONNECTED:
	return st_pending;
    case ISC_EVENT_REJECTED:
	return st_error;
    case ISC_EVENT_ERROR:
	return st_error;
    case ISC_EVENT_TIMEOUT:
    case ISC_EVENT_LOGIN:
    case ISC_EVENT_LOGOUT:
	std::abort();
    case ISC_EVENT_MESSAGE:
	got_data(ev->msg->buffer, ev->msg->length);
	return st_ok;
    default:
	std::abort();
    }
}

LyStr
Conn_io_ISC::localhost() const
{
    if (IscAddress *ia = server->info.tcp.laddr)
    {
	struct in_addr *addr(
	    &reinterpret_cast<struct sockaddr_in*>(&ia->ip.saddr)->sin_addr);
 
	if (struct hostent *hp = gethostbyaddr(
	    reinterpret_cast<char*>(&addr->s_addr), 
	    sizeof(addr->s_addr), AF_INET))
	{
	    return hp->h_name;
	}
	else
	{
	    return inet_ntoa(*addr);
	}
    }
    else
    {
	// ISC failed to obtain the local address.  This should never happen.
	return "unknown";
    }
}
