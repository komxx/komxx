// Ask the server which async messages we are receiving.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "query_async_q.h"
    
query_async_question::query_async_question(Connection *conn)
  : question(conn)
{
    prot_a_LyStr   s;

    s << ref_no() << " 81\n";

    port->send_question(s);
}


query_async_question::~query_async_question()
{
}


void
query_async_question::parser(prot_a_LyStr& str)
{
    int   arrsize;
    int   i;

    arrsize = str.parse_array_start();
    res.resize(arrsize);

    for (i = 0; i < arrsize; ++i)
	res[i] = str.stol();
    str.parse_array_end(arrsize);
    
    state = st_ok;
    deallocate_refno();
}


query_async_question::result_type
query_async_question::result()
{
    return res;
}
