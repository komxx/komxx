// Look up the name of a person using a regular expression.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_RE_LOOKUP_PERSON_Q_H
#define KOMXX_RE_LOOKUP_PERSON_Q_H

#include <vector>

#include "kom-types.h"
#include "question.h"

class LyStr;

// 65

class re_lookup_person_question : public question {
  public:
    typedef std::vector<Pers_no> result_type;

    re_lookup_person_question(Connection *conn, const LyStr &pattern);
    ~re_lookup_person_question();
    void parser(prot_a_LyStr &str);
    result_type result();
  private:
    std::vector<Pers_no> matches;
};

#endif
