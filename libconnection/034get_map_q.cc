// Get map from local-text-no to global text-no.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "get_map_q.h"
#include "connection.h"
#include "Local2Global.h"
    
get_map_question::get_map_question(Connection *conn,
				   Conf_no cno, 
				   Local_text_no first_local_no, 
				   int no_of_texts)
    : question(conn), tlist(NULL)
{
    prot_a_LyStr out;

    out << ref_no() << " 34 " << cno << ' ' << first_local_no
	<< ' ' << no_of_texts << '\n';
    port->send_question(out);
}


get_map_question::~get_map_question()
{
    delete tlist;
}


void
get_map_question::parser(prot_a_LyStr &str)
{
    tlist = new Local2Global(str);
    state = st_ok;
    deallocate_refno();
}


Local2Global
get_map_question::result()
{
    return *tlist;
}
