// Objects for handling the asynchronous message "New text created"
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_ASYNC_CONF_CHANGED_NAME_H
#define KOMXX_ASYNC_CONF_CHANGED_NAME_H

#include "kom-types.h"
#include "prot-a-LyStr.h"
#include "async.h"


class Async_conf_changed_name : public Async_message {
  public:
    Async_conf_changed_name();
    Async_conf_changed_name(const Async_conf_changed_name &);
    ~Async_conf_changed_name();
    Async_conf_changed_name &operator =(const Async_conf_changed_name &);
	
    static Async_name name();

    Conf_no   conf_no()  const;
    LyStr     old_name() const;
    LyStr     new_name() const;

  public:
    // Only intended to be used by
    // class Async_administrator<Async_conf_changed_name>.
    Async_conf_changed_name(prot_a_LyStr &);
  private:
    Conf_no  cno;
    LyStr    oname;
    LyStr    nname;
};

#endif
