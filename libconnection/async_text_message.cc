// Objects for handling the asynchronous message "Text message"
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>

#include "async_text_message.h"
#include "prot-a-LyStr.h"
#include "connection.h"


Async_text_message::Async_text_message()
    : recipient_m(0), sender_m(0), message_m()
{
}

Async_text_message::Async_text_message(const Async_text_message &t)
  : Async_message(),
    recipient_m(t.recipient_m), 
    sender_m(t.sender_m), 
    message_m(t.message_m)
{
}

Async_text_message::~Async_text_message()
{
}

Async_text_message &
Async_text_message::operator =(const Async_text_message &t)
{
    recipient_m = t.recipient_m;
    sender_m = t.sender_m;
    message_m = t.message_m;

    return *this;
}

Async_name
Async_text_message::name()
{
    return ASYNC_TEXT_MESSAGE;
}

Conf_no
Async_text_message::recipient() const
{
    return recipient_m;
}

Pers_no
Async_text_message::sender() const
{
    return sender_m;
}

const LyStr &
Async_text_message::message() const
{
    return message_m;
}


Async_text_message::Async_text_message(prot_a_LyStr &p)
{
    recipient_m = p.stol();
    sender_m = p.stol();
    LyStr_errno tmp = p.stos();
    assert(tmp.kom_errno == KOM_NO_ERROR);
    message_m = tmp.str;
}

template<>
const Async_message *
Async_administrator<Async_text_message>::parse(prot_a_LyStr &in)
{
    return new Async_text_message(in);
}
