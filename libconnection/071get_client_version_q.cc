// Get client version for the client running on a specified connection.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "get_client_version_q.h"

    
get_client_version_question::get_client_version_question(Connection *conn, 
							 Session_no session)
    : question(conn)
{
    if (conn->get_server_info()->version() >= 10400) 
    {
	prot_a_LyStr   s;

	s << ref_no() << " 71 " << session << '\n';
	port->send_question(s);
    }
    else
    {
	receive_err(KOM_ANCIENT_SERVER);
    }
}


get_client_version_question::~get_client_version_question()
{
}


void
get_client_version_question::parser(prot_a_LyStr &str)
{
    LyStr_errno tmp = str.stos();
    if (tmp.kom_errno != KOM_NO_ERROR)
	receive_err(tmp.kom_errno);
    else
    {
	version = tmp.str;
	state = st_ok;
	deallocate_refno();
    }
}


LyStr
get_client_version_question::result()
{
    return version;
}
