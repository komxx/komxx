// Unmark a text.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "unmark_text_q.h"
#include "obsolete.h"		// for mark_text_old_question

    
unmark_text_question::unmark_text_question(Connection *conn,
					   Text_no text)
    : question(conn), mark_text_old(NULL)
{
    prot_a_LyStr   s;

    if (conn->get_server_info()->version() >= 10400) {
	s << ref_no() << " 73 " << text << '\n';
	port->send_question(s);
    } else {
	mark_text_old = new mark_text_old_question(conn, text, 0);
    }
}


unmark_text_question::~unmark_text_question()
{
    if (mark_text_old != NULL)
	delete mark_text_old;
}


Status 
unmark_text_question::status()
{
    if (mark_text_old == NULL)
	return question::status();
    else
	return mark_text_old->status();
}


Status 
unmark_text_question::receive()
{
    if (mark_text_old == NULL)
	return question::receive();
    else {
	deallocate_refno();
	return mark_text_old->receive();
    }
}


Kom_err
unmark_text_question::error()
{
    if (mark_text_old == NULL)
	return question::error();
    else
	return mark_text_old->error();
}


void
unmark_text_question::parser(prot_a_LyStr &)
{
    state = st_ok;
    deallocate_refno();
}


void
unmark_text_question::result()
{
    return;
}
