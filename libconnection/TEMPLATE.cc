// <PURPOSE OF THIS FILE>
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
//#include "THE-RIGHT-H-FILE.h"

    
TEMPLATE_question::TEMPLATE_question(Connection *conn, PARAMETERS...)
    : question(conn), MEMBERS(...)
{
    prot_a_LyStr   s;

    s << ref_no() << " <NUMBER> " << <PARAMETERS...> << '\n';
    port->send_question(s);
}


TEMPLATE_question::~TEMPLATE_question()
{
    DELETE MEMBERS HERE...
}


void
TEMPLATE_question::parser(prot_a_LyStr &str)
{
    PARSE RESULT IN str AND STORE IT IN SOME MEMBER...

    state = st_ok;
    deallocate_refno();
}


RESULT_TYPE
TEMPLATE_question::result()
{
    return *SOME_SUITABLE_MEMBER;
}
