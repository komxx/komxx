// Find previous used text-no.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "question.h"
#include "connection.h"
#include "find_previous_text_no_q.h"

    
find_previous_text_no_question::find_previous_text_no_question(
    Connection *conn,
    Text_no next)
    : question(conn), prev(0)
{
    prot_a_LyStr   s;

    s << ref_no() << " 61 " << next << '\n';
    port->send_question(s);
}


find_previous_text_no_question::~find_previous_text_no_question()
{
}


void
find_previous_text_no_question::parser(prot_a_LyStr &str)
{
    prev = str.stol();

    state = st_ok;
    deallocate_refno();
}


Text_no
find_previous_text_no_question::result()
{
    return prev;
}
