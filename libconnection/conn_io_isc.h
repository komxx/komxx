// Concrete I/O to the LysKOM server using ISC.
//
// Copyright (C) 2002  Per Cederqvist
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_CONN_IO_ISC_H
#define KOMXX_CONN_IO_ISC_H

#include <queue>

#include "conn_io.h"

extern "C" {

// Include a few headers that are required by isc.h.
#include <stddef.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>

#include "isc.h"

};


class Conn_io_ISC : public Conn_io {
  public:
    // If you don't need direct access to the IscMaster you can leave
    // out the IscMaster argument, and an IscMaster will be made for
    // you. There is, however, no way for you to get hold on that
    // IscMaster.  If your client uses other IscSessions as well, you
    // probably have to pass the IscMaster in the constructor.
    //
    // The hostname and portnum arguments are NUL-terminated strings.
    Conn_io_ISC(const char *hostname, 
		const char *portnum, 
		IscMaster *master=NULL);

    // See Conn_io for documentation about these.
    virtual ~Conn_io_ISC();
    virtual bool connection_failed() const;
    virtual LyStr localhost() const;
    virtual void queue_for_send(char *block, int len);
    virtual Status receive(int timeout=0);

    // ISC extensions to Conn_io:

    // If an application wants to have more than one IscSession, it
    // can use Conn_io_ISC::get_event() to poll for events.  Any event
    // that is handled by an Conn_io_ISC will be passed to the proper
    // object, and any other IscEvent objects will be returned to the
    // caller.
    //
    // Actually, there is a queue of pending IscEvent that don't
    // belong to a Conn_io_ISC.  If such IscEvents occur while
    // Conn_io_ISC::receive() is called, they will be queued there.
    // This method will return the events in order.
    //
    // Return NULL if an event was received and delivered to a
    // Conn_io_ISC.

    // The event that is returned should be disposed of with
    // isc_dispose(). You are in trouble if this returns an
    // ISC_EVENT_ERROR.  This might return the special value
    // &Conn_io_ISC::error_event if the connection was lost or
    // something similarily serious happens, or
    // Conn_io_ISC::timeout_event if a timeout occured.  Those events
    // should *not* be disposed of with isc_dispose()!
    //
    // If no events are available, this function will block for up to
    // TIMEOUT milliseconds.

    static IscEvent *get_event(int timeout=0);
    static IscEvent error_event;
    static IscEvent timeout_event;
  private:

    // FIXME: Support for multiple connections should be added.
    static Conn_io_ISC *the_one_and_only_connection;

    // A queue for any IscEvents that didn't belong to any
    // Conn_io_ISC.  They can be retrieved with get_event().
    static std::queue<IscEvent *> external_events;

    // The master control block. This is shared between all
    // Conn_io_ISC, and possibly used by other IscSessions as well.
    static IscMaster    *mcb;

    // The connection to the server.  (This is NULL if the
    // constructor failed to connect to a server).
    IscSession   *server;

    // Handle an IscEvent that belongs to this particular
    // Conn_io_ISC.
    Status receive_event(IscEvent *);
};

#endif
