// Get static session-info.
//
// Copyright (C) 1994, 1996  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_GET_STATIC_SESSION_INFO_Q_H
#define KOMXX_GET_STATIC_SESSION_INFO_Q_H

#include "kom-types.h"
#include "question.h"

class Static_session_info;

// 84

class get_static_session_info_question : public question {
  public:
    typedef Static_session_info result_type;
    typedef Session_no key_type;

    get_static_session_info_question(Connection *conn, key_type session);
    ~get_static_session_info_question();
    void parser(prot_a_LyStr &str);
    result_type result();
  private:
    Static_session_info *info;
};

#endif
