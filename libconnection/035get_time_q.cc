// Ask the server what time it is.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "connection.h"
#include "time-utils.h"
#include "get_time_q.h"

get_time_question::get_time_question(Connection *conn)
    : question(conn)
{
    prot_a_LyStr b;
    b << ref_no() << ' ' << 35 << '\n';
    port->send_question(b);
}

get_time_question::~get_time_question()
{
}

void
get_time_question::parser(prot_a_LyStr& str)
{
    time_date = parse_time(str);
    state = st_ok;
    deallocate_refno();
}

struct std::tm
get_time_question::result()
{
    return time_date;
}
