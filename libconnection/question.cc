// Base clase for all question-sending classes.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>
#include <iostream>

#include "question.h"
#include "refno.h"
#include "connection.h"

// refno stuff (se end of file for question stuff).

int limit=0;
const int CHUNKSIZ = 5;

static question **map = NULL;

Ref_no::Ref_no()
{
    if (map == NULL)
    {
	limit = CHUNKSIZ;
	map = new (question *[limit]);
	for (int i=0; i < limit; i++)
	  map[i] = NULL;
    }

    int i;
    for (i=0; i < limit; i++)
	if (map[i] == NULL)
	{
	    ref = i;
	    return;
	}

    question **newmap = new (question *[limit + CHUNKSIZ]);
    for (i=0; i < limit; i++)
	newmap[i] = map[i];
    ref = limit;
    limit += CHUNKSIZ;
    for (; i < limit; i++)
	newmap[i] = NULL;
    delete []map;
    map = newmap;
}

Ref_no::~Ref_no()
{
    map[ref] = NULL;
}

int Ref_no::ref_no() const
{
    return ref;
}
    
question *
lookup_refno(int r)
{
    assert(0 <= r && r < limit);
    return map[r];
}

// question stuff

question::question(Connection *conn)
  : state(st_pending),
    port(conn),
    errcode(KOM_NO_ERROR),
    refno(new Ref_no)
{
    map[refno->ref_no()] = this;
}

question::~question()
{
    if (refno)
    {
	std::cerr << "Warning: destructing unanswered question with ref_no=" 
		  << refno->ref_no() << std::endl;
	deallocate_refno();
    }
}

Status
question::status()
{
    if (state == st_pending)
    {
	if (port->drain(0) == st_error)
	{
	    errcode = KOM_NO_CONNECT;
	    state = st_error;
	}
    }

    return state;
}

Kom_err
question::error()
{
    return errcode;
}

void
question::receive_err(int err)
{
    state = st_error;
    errcode = int_to_kom_err(err);
    deallocate_refno();
}

Status question::receive()
{
    while (state == st_pending)
    {
	if (port->drain(100) == st_error)
	{
	    errcode = KOM_NO_CONNECT;
	    state = st_error;
	}
    }

    return state;
}

Kom_err
question::error_blocking()
{
    receive();
    return error();
}

void
question::deallocate_refno()
{
    delete refno;
    refno = NULL;
}

int
question::ref_no() const
{
    assert(refno != NULL);
    return refno->ref_no();
}
