// Abstract I/O to the LysKOM server.
//
// Copyright (C) 2002  Per Cederqvist
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>

#include "conn_io.h"
#include "connection.h"

Conn_io::Conn_io()
  : parent(NULL)
{
}


Conn_io::~Conn_io()
{
}

void
Conn_io::set_connection(Connection *p)
{
    assert(parent == NULL);
    parent = p;
}

void 
Conn_io::got_data(const prot_a_LyStr &buf)
{
    parent->got_data(buf);
}

void 
Conn_io::got_data(const LyStr &buf)
{
    parent->got_data(buf);
}

void 
Conn_io::got_data(const char *buf, long len)
{
    parent->got_data(buf, len);
}
