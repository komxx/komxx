// Get list of created texts for an author.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "prot-a-LyStr.h"
#include "map_created_texts_q.h"
#include "connection.h"
#include "local-to-global-map.h"
    
map_created_texts_question::map_created_texts_question(Connection *conn,
						       Pers_no pno, 
						       Local_text_no first, 
						       unsigned long len)
    : question(conn), tlist(NULL)
{
    prot_a_LyStr out;

    out << ref_no() << " 104 " << pno << ' ' << first
	<< ' ' << len << '\n';
    port->send_question(out);
}


map_created_texts_question::~map_created_texts_question()
{
    delete tlist;
}


void
map_created_texts_question::parser(prot_a_LyStr &str)
{
    tlist = new local_to_global_map(str);
    state = st_ok;
    deallocate_refno();
}


local_to_global_map
map_created_texts_question::result()
{
    return *tlist;
}
