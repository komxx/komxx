// Issue an old-style login request.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "obsolete.h"
#include "connection.h"

login_old_question::login_old_question(Connection *conn,
				       Pers_no pers_no,
				       const LyStr &passwd)
    : question(conn)
{
    prot_a_LyStr   b;

    b << ref_no() << ' ' << 0 << ' ' << pers_no << ' ';
    b.append_hollerith(passwd);
    b << '\n';
    port->send_question(b);
}


login_old_question::~login_old_question()
{
}


void
login_old_question::parser(prot_a_LyStr &)
{
    state = st_ok;
    deallocate_refno();
}

    
