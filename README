		       kom++ version 0.7.post.7
		    a LysKOM client library in C++
				 with
			   Python bindings
			     TCL bindings
			a line-oriented client
			and a web-based client
				  by
		Per Cederqvist <ceder@lysator.liu.se>
				 and
		  Inge Wallin <inge@lysator.liu.se>


What is kom++?
==============

The kom++ distribution is made up of several parts:

 - a C++ library that handles LysKOM protocol A, including caching of
   data, and much more.  [libconnection, libkom++, libisc]

 - a few demo clients written in C++.  [other-clients]

 - Python bindings so that the C++ library can be used from Python
   programs.  [py-clients]

 - A few very specialized LysKOM clients written in Python.
   [py-clients] 

 - A specialized web server written in Python that makes it possible
   to read LysKOM with an ordinary web browser.  [www]

 - TCL bindings to the C++ library.  [tcl-clients]

 - A simple line-oriented client written in TCL, called "nilkom".
   [tcl-clients]

 - The remains of an X-windows client written in TCL/Tk, called
   "tkom".  This has not been maintained for a very long time, and
   does currently not compiler.  [tcl-clients]

The C++ library is designed to be as fast as possible, caching data as
it reads them from the LysKOM server.  It can also send many questions
simultaneously without having to wait for the answer of the first
question to return.  Kom++ is certainly much faster than the older
library (written by ceder) which was written in C and used for earlier
clients (notably ttykom, originally written by Thomas Bellman, now
maintained by Linus Tolke).


Release Notes
=============

NOTE: This is a snapshot of work in progress.

NOTE: Don't expect the documentation to be up-to-date.

This release requires a modern C++ compiler, such as gcc-3.3.5.

The nilkom client should be linked against a TCL version 7
installation.  If used together with TCL version 8 it will send
UTF-8-encoded strings to the LysKOM server when characters outside the
US-ASCII range (0-127) are used.


License
=======

The stuff under in the libisc directory is distributed under GNU LGPL
version 2.  See libisc/COPYING.LIB.

The rest of the files (with a few exceptions that are only used to
build kom++) are distributed under GNU GPL version 2.  See COPYING.


Installation
============

Kom++ uses autoconf.  Generic installation instructions for programs
that use autoconf can be found in INSTALL.

Kom++ uses the following special configure options:

    --enable-tkom
	Attempt to build tkom.  Don't use this unless you are prepared
	to invest a fair amount of work.

    --disable-nilkom
	Don't build nilkom.  The default is to build and install
	nilkom.

    --enable-python
	Build and install python bindings.  This is automatically
	enabled if www-kom is built.

    --enable-www-kom
	Build the web server WWW-kom.  Please read www/README if you
	use this option.

    --enable-memcheck
	Add defensive debug code that tries to find a few memory
	allocation-related bugs.  The program will dump core if a
	problem is found, so that a debugger can be used to locate the
	bug.

    --enable-default-server=HOSTNAME
	Use HOSTNAME as default server.  This defaults to
	kom.lysator.liu.se.

    --enable-default-port=PORT
	Use PORT as the default LysKOM port.  This defaults to 4894.

    --with-purify
	Use Purify to try to find memory leaks.

    --with-python=FILE
	Select which Python installation the Python bindings
	should be built agains.  The default is to look for "python"
	in your path, but with this option you can specify either an
	alternate name (--with-python=python1.6) or a full path
	(--with-python=/opt/python/bin/python).

    --with-tcl=DIRECTORY
	Select which TCL installation the TCL bindings should be built
	against.  The default is to look for a TCL installation in a
	few standard places (see tcl-clients/tcl.m4 for the details).
	You can specify the directory where the appropriate
	"tclConfig.sh" is located if you want to select a special TCL
	installation, or if your TCL installation is located in a
	non-standard place.  Note: the PATH setting is not used when
	searching for a TCL installation.


Bug reports
===========

We use the Bugzilla installation at http://bugzilla.lysator.liu.se/ to
keep track of bug reports for the software in the kom++ distribution.
Use the "kom++" product.  Please write your bug report in English.

If for some reason you cannot use Bugzilla, you can send bug reports
to bug-lyskom@lysator.liu.se instead.  Please include the word
'kom++', 'WWW-kom', 'nilkom' or 'tkom' somewhere in the Subject: line.
Don't forget to include the version number.  But please use Bugzilla
instead, if possible.
