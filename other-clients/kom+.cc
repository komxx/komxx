// Small test program.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include <config.h>

#include <cstdlib>
#include <iostream>

#include "prot-a-LyStr.h"
#include "connection.h"
#include "login_q.h"
#include "get_text_q.h"
#include "prot-a-LyStr.h"
#include "async.h"
#include "conn_io_isc.h"

Connection *port = NULL;
Async_storage *async_store = NULL;

void async_login(prot_a_LyStr args)
{
    std::cout << "\nAsync login msg: " << args;
}


int 
main()
{
    Conn_io_ISC *conn_io = new Conn_io_ISC("kom.lysator.liu.se", "4894");
    port = new Connection(conn_io);
    async_store = new Async_storage(port);
#if 0
    port->install_async_parser(ASYNC_LOGIN, async_login);
#endif

    login_question in(port, 90, "gazonk");
    get_text_question te(port, 4711, 0, 10000);

    if (in.receive() != st_ok)
    {
	std::cerr << "Login error " << in.error() << "\n";
	std::exit(1);
    }

    if (te.receive() != st_ok)
    {
	std::cerr << "get-text error " << te.error() << "\n";
	std::exit(1);
    }

    std::cout << te.result() << std::endl;

    while (async_store->handle_async() == st_pending)
    {
	if (port->drain(1000) == st_error)
	{
	    std::cerr << "Connection lost." << std::endl;
	    std::exit(1);
	}
    }    

    delete port;
    std::exit(0);
}
