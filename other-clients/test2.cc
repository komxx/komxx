// Simple proof-of-concept LysKOM client using kom++.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

// Note: the error handling in this program is very poor.  There are
// several assertions that can easilly be triggered by this program.

#include <config.h>

#include <cassert>
#include <iterator>
#include <cstdlib>
#include <cstring>
#include <cstdio>		// FIXME: use C++-style I/O
#include <iostream>
#include <iomanip>

#include "session.h"		// High-level objects
#include "conference.h"
#include "text.h"
#include "person.h"
#include "maps.h"

#include "connection.h"
#include "conn_io_isc.h"
#include "obsolete.h"
#include "uplink.h"
#include "who-info.h"
#include "membership.h"
#include "read-order.h"
#include "unread-confs.h"
#include "text-mapping.h"
#include "server-info.h"

#include "get_membership_q.h"	// Questions (from libquestion.a)
#include "get_unread_confs_q.h"
#include "get_time_q.h"
#include "get_membership_list_q.h"


Connection   * port = NULL;
Session      * session;

Unread_confs * unread_confs;

Conf_no        current_conf = 0;
Pers_no        this_person;


class Noecho {
  public:
    Noecho();
    ~Noecho();
};

Noecho::Noecho()
{
    system("stty -echo");
}

Noecho::~Noecho()
{
    system("stty echo");
}

static void
find_password(char *res)
{
#if 1 // interactive
    Noecho noecho;
    std::cout << "Password: ";
    std::cin >> res;
    {
	int ch;
	while ((ch = std::cin.get()) != '\n' && !std::cin.eof())
	    ;
    }
    std::cout << std::endl;
    return;
#else // .kompwd
    char *home;
    if ((home = getenv("HOME")) == NULL)
    {
	std::cerr << "HOME is unset" << std::endl;
	exit(1);
    }
    char *buffer = new char[strlen(home) + 9];
    sprintf(buffer, "%s/.kompwd", home);
    FILE *fp = fopen(buffer, "r");
    if (fp == NULL)
    {
	perror(buffer);
	exit(1);
    }
    delete[] buffer;
    fgets(res, 20, fp);
    *strchr(res, '\n') = '\0';
#endif
}


// ================================================================
//                     Implementation of commands


void
map_command(prot_a_LyStr &str)
{
    Conf_no         cno;
    Local_text_no   lno;
    Text_no         tno;

    cno = str.stol();
    lno = str.stol();
    std::cout << "cno == " << cno << " och lno == " << lno << std::endl;
    if (cno == 0 || lno == 0)
	return;

    tno = session->maps()[cno][lno];
    std::cout << "Text nummer " << lno << " i m�te " << cno << " �r "
	      << tno << ".\n";
}


void
print_text(Text ts)
{
    int       i;

    Conferences &  conferences = session->conferences();
    Texts &        texts = session->texts();

    std::cout << ts.text_no() << " /" << ts.num_lines() << " rader/ " << ' '
	      << conferences[ts.author()].name() << '\n';

    for (i = 0; i < ts.num_uplinks(); ++i) {
	Text_no   parent;

	parent = ts.uplink(i)->parent();
	std::cout << "Kommentar till text " << parent << " av "
		  << conferences[texts[parent].author()].name() << '\n';
    }

    for (i = 0; i < ts.num_recipients(); ++i) {
	std::cout << "Mottagare: "
		  << conferences[ts.recipient(i)->recipient()].name() 
		  << " <" << ts.recipient(i)->local_no() << ">\n";
    }

    std::cout << "�rende: " << ts.subject().str << std::endl;
    std::cout << "---------------------------------------------------------\n";
    std::cout << ts.body().str << std::endl;
    std::cout << '(' << ts.text_no() << ") --------------------------------\n";
    for (i = 0; i < ts.num_footnote_in(); ++i) {
	Text_no   footnote;

	footnote = ts.footnote_in(i);
	std::cout << "Fotnot i text " << footnote << " av "
		  << conferences[texts[footnote].author()].name() 
		  << '\n';
    }

    for (i = 0; i < ts.num_comment_in(); ++i) {
	Text_no   comment;

	comment = ts.comment_in(i);
	std::cout << "Kommentar i text " << comment << " av "
		  << conferences[texts[comment].author()].name() 
		  << '\n';
    }
}


void
print_text_command(prot_a_LyStr &str)
{
    Texts &   texts = session->texts();
    Text      ts;
    Text_no   tn;


    tn = str.stol();
    if (tn != 0) {
	ts = texts[tn];
	print_text(ts);
    }
}



//
// List the number of unread texts in each conference.
// This code does not take into account deleted texts (yet).
//

void
list_unread_command(prot_a_LyStr &)
{
    Conferences &              conferences = session->conferences();
    get_unread_confs_question  ucq(port, this_person);

    if (ucq.receive() != st_ok) {
	std::cerr << "Unread confs error " << ucq.error() << "\n";
	std::exit(1);
    }

    std::cout << "Du har ungef�r s� h�r m�nga ol�sta inl�gg per m�te:\n";
    std::vector<Conf_no> unread_confs = ucq.result();

    for (std::vector<Conf_no>::iterator iter = unread_confs.begin();
	 iter < unread_confs.end(); ++iter) {
	get_membership_question  * gmq;

	gmq = new get_membership_question(port, this_person, *iter);
	if (gmq->receive() == st_ok) {
	    Conference      conf = conferences[*iter];
	    Membership      membership = gmq->result();
	    unsigned long   num_unread = (conf.no_of_texts() 
					  + conf.first_local_no()
					  - membership.last_text_read() - 1);

	    if (num_unread > 0) {
		std::cout << num_unread << '\t' << conf.name() << '\n';
	    }
	}
	delete gmq;
    }
}


// ----------------------------------------------------------------


void
lookup_name_command(prot_a_LyStr &str)
{
    Conferences &       conferences = session->conferences();
    std::vector<Micro_conf>  matches = session->lookup_name(str);
    
    if (matches.empty()) 
	std::cout << "Inga namn matchade " << str << std::endl;
    else {
	std::cout << "F�ljande namn matchar " << str << ":\n";
	for (std::vector<Micro_conf>::iterator iter = matches.begin();
	     iter != matches.end(); ++iter)
	    std::cout << iter->conf_no() << '\t' 
		      << conferences[iter->conf_no()].name() 
		      << std::endl;
    }
    std::cout << std::endl;
}


// ----------------------------------------------------------------


void
lookup_persons_command(prot_a_LyStr &str)
{
    Conferences &    conferences = session->conferences();
    std::vector<Pers_no>  persons;
    
    persons = session->regexp_lookup_persons(str);
    if (persons.empty() == 0) 
	std::cout << "Inga personer matchade " << str << std::endl;
    else {
	std::cout << "F�ljande personer matchar " << str << ":\n";
	for (std::vector<Pers_no>::iterator iter = persons.begin();
	     iter != persons.end(); ++iter) {
	    std::cout << *iter << '\t' << conferences[*iter].name() 
		      << std::endl;
	}
    }
    std::cout << std::endl;
}


// ----------------------------------------------------------------


void
next_text_command(prot_a_LyStr &)
{
    Texts &   texts = session->texts();
    Text      ts;
    Text_no   tn;

    if (unread_confs->next_prompt() == NO_MORE_TEXT) {
	std::cout << "Det finns ingen mer text\n";
    } else {
	tn = unread_confs->next_text(1);
	ts = texts[tn];
	print_text(ts);
    }
}


// ----------------------------------------------------------------


void
next_conf_command(prot_a_LyStr &)
{
    current_conf = unread_confs->goto_next_conf();

    if (current_conf == 0) {
	std::cout << "Inga fler ol�sta\n";
    } else {
	std::cout << session->conferences()[current_conf].name()
		  << " (" << unread_confs->min_unread_texts_in_current() 
		  << " ol�sta)\n";
    }
}


// ----------------------------------------------------------------


void
who_is_on_command(prot_a_LyStr &)
{
    Conferences &        conferences = session->conferences();
    who_is_on_question   wioq(port);
    
    if (wioq.receive() == st_error)
	std::cerr << "Kunde inte h�mta vilkalistan.  :-(\n";
    else {
	std::vector<Who_info> who_info_list(wioq.result());
//	who_info_list = session->who_is_on();
	for (std::vector<Who_info>::iterator iter = who_info_list.begin(); 
	     iter != who_info_list.end(); ++iter) {
	    Conf_no wc = iter->working_conf();
	    
	    if (wc != 0)
		conferences[wc].prefetch();
	    
	    conferences[iter->pers_no()].prefetch();
	}
	for (std::vector<Who_info>::iterator iter = who_info_list.begin(); 
	     iter != who_info_list.end(); ++iter) {
	    Conf_no wc = iter->working_conf();
	    
	    std::cout << iter->session() << '\t';
	    if (conferences[iter->pers_no()].exists())
		std::cout << conferences[iter->pers_no()].name();
	    else
		std::cout << "hemlig person " << iter->pers_no();
	    std::cout << "\n\t";
	    if (wc != 0)
		if (conferences[wc].exists())
		    std::cout << conferences[wc].name();
		else
		    std::cout << "hemligt m�te " << wc << std::endl;
	    else
		std::cout << "ej n�rvarande i n�got m�te";
	    std::cout << '\n';
	}
    }
}


// ----------------------------------------------------------------


void
list_members_command(prot_a_LyStr &str)
{
    Conferences &        conferences = session->conferences();
    long                 confno;
    bool                 error = 0;

    confno = str.extract_number(error);
    if (error)
	return;

    if (!conferences[confno].exists())
    {
	std::cout << "M�te nummer " << confno << " finns inte.\n";
	return;
    }

    std::vector<Pers_no> members = session->get_members(confno, 0, 9999);
    if (members.empty())
	std::cout << "Inga medlemmar i m�te " << confno << ".\n";
    else {
	std::cout << "Medlemmar i m�te " << confno << ":\n";
	for (std::vector<Pers_no>::iterator iter = members.begin(); 
	     iter < members.end(); ++iter) {
	    Conference conf = conferences[*iter];

	    std::cout << *iter << ":\t" << std::flush;
	    if (conf.exists())
		std::cout << conf.name() << std::endl;
	    else
		std::cout << "Person " << *iter << " (finns inte)." << std::endl;
	}
    }
}

// ----------------------------------------------------------------


void
list_membership_command(prot_a_LyStr &str)
{
    long                 persno;
    bool                 error = false;

    persno = str.extract_number(error);
    if (error)
	return;

    if (!session->persons()[persno].exists())
    {
	std::cout << "Person nummer " << persno << " finns inte.\n";
	return;
    }

    get_membership_list_question q(session->connection(), persno,
				   0, 1000);
    if (q.receive() != st_ok)
    {
	std::cout << "Kan inte h�mta medlemsskap: " << q.error() << std::endl;
	return;
    }
    
    std::vector<Membership> result = q.result();
    std::cout << "Person " << persno << " �r medlem i dessa m�ten.\n";
    for (std::vector<Membership>::iterator iter = result.begin();
	 iter < result.end(); ++iter)
    {
	std::cout << iter->conf_no() << ' '
		  << int(iter->priority()) << ' '
		  << iter->last_text_read() << ' '
		  << iter->num_read_after_last_text_read()
		  << std::endl;
    }
}


// ================================================================
//                       The command interpreter


void   list_commands_command(prot_a_LyStr &str);

struct command_struct {
    char    shortcommand[5];
    char  * description;
    void (* function)(prot_a_LyStr &);
} commands[] = {
    {"mpMP", "Map",                  map_command},
    {"lkLK", "lista kommandon",      list_commands_command},
    {"laLA", "lookup nAmn",          lookup_name_command},
    {"lnLN", "lista nyheter",        list_unread_command},
    {"lmLM", "lista medlemmar",      list_members_command},
    {"lsLS", "lista medlemsskap",    list_membership_command},
    {"lpLP", "lookup persons",       lookup_persons_command},
    {"niNI", "n�sta inl�gg",         next_text_command},
    {"nmNM", "n�sta m�te",           next_conf_command},
    {"slSL", "sluta",                NULL},
    {"viVI", "vilka",                who_is_on_command},
    {"}t]T", "�terse text <nummer>", print_text_command},
};

int num_commands = sizeof(commands) / sizeof(command_struct);


void
list_commands_command(prot_a_LyStr &)
{
    int   i;
    
    for (i = 0; i < num_commands; ++i) {
	std::cout << commands[i].shortcommand[0]
		  << commands[i].shortcommand[1] << '\t'
		  << commands[i].description << '\n';
    }
}


void
command_interpreter()
{
    char    ch;
    char    buf[2];
    prot_a_LyStr   command_line;
    int     i;

    while (1) {
	Prompt   prompt;
	char   * promptstr = NULL;

	command_line.clear();
	while ((prompt = unread_confs->next_prompt()) == TIMEOUT_PROMPT)
	{
	    printf(">");
	    fflush(stdout);
	}
	    
	if (current_conf == 0 && prompt == NEXT_TEXT)
	    prompt = NEXT_CONF;

	switch (prompt) {
	  case NEXT_CONF:
	    promptstr = "G� till n�sta m�te> "; 
	    break;
	  case NEXT_TEXT:
	    promptstr = "(L�sa) n�sta text> ";
	    break;
	  case NEXT_COMMENT:
	    promptstr = "(L�sa) n�sta kommentar> ";
	    break;
	  case NEXT_FOOTNOTE:
	    promptstr = "(L�sa) n�sta fotnot> ";
	    break;
	  case NO_MORE_TEXT:
	    promptstr = "(Se) tiden> ";
	    break;
	  case TIMEOUT_PROMPT:
	    std::abort();
	  case PROMPT_ERROR:
	    std::abort();
	}
	assert(promptstr != NULL);
	std::cout << promptstr;

	// Read the command from stdin
	while ((ch = std::cin.get()) != '\n')
	    command_line << ch;
	command_line.skipspace();

	if (command_line.strlen() == 0) {

	    // Default command.  Depends on what the prompt was.
	    switch (prompt) {
	      case NEXT_CONF:
		next_conf_command(command_line);
		break;
	      case NEXT_TEXT:
	      case NEXT_COMMENT:
	      case NEXT_FOOTNOTE:
		next_text_command(command_line);
		break;
	      case NO_MORE_TEXT:
		{
		    get_time_question q(port);
		    if (q.receive() == st_ok) {
			struct tm t = q.result();
			std::cout << "Klockan �r " << 1900+t.tm_year 
				  << '-' << std::setw(2) << std::setfill('0')
				  << 1+t.tm_mon
				  << '-' << std::setw(2) << std::setfill('0')
				  << t.tm_mday
				  << ' ' << std::setw(2) << std::setfill('0')
				  << t.tm_hour
				  << ':' << std::setw(2) << std::setfill('0')
				  << t.tm_min
				  << ':' << std::setw(2) << std::setfill('0')
				  << t.tm_sec
				  << " enligt servern." << std::endl;
		    } else {
			std::cout << "Klockan �r inte." << std::endl;
		    }
		}
		break;
	      case TIMEOUT_PROMPT:
		std::abort();
	      case PROMPT_ERROR:
		std::abort();
	    }
	    continue;
	    
	} else if (command_line.strlen() == 1) {
	    std::cout << "Ok�nt kommando\n";
	    continue;

	} else {
	    buf[0] = command_line[0];
	    buf[1] = command_line[1];
	    command_line.remove_first(2);
	    command_line.skipspace();
	}

	for (i = 0; i < num_commands; ++i) {
	    if ( ( (buf[0] == commands[i].shortcommand[0])
		   || (buf[0] == commands[i].shortcommand[2]))
		 && ( (buf[1] == commands[i].shortcommand[1])
		      || (buf[1] == commands[i].shortcommand[3])))
	    {
		if (commands[i].function == NULL)
		    return;
		else {
		    commands[i].function(command_line);
		    break;
		}
	    }
	}
	if (i == num_commands)
	    std::cout << "Ok�nt kommando!\n";
    }
}


// ================================================================


int
main(int argc,
     char **argv)
{
    char pwd[100];
    char *server_name = "kom.lysator.liu.se";

    this_person = argc >= 2 ? atol(argv[1]) : 19;
    find_password(&pwd[0]);

    if (argc >= 3)
	server_name = argv[2];

    Conn_io_ISC *conn_io = new Conn_io_ISC(server_name, "4894");
    port = new Connection(conn_io);
    session = new Session(port, "test2", "0.1");

    {
	Server_info  * sinfo;
	Text_no        motd;

	sinfo = port->get_server_info();
	motd = sinfo->motd_of_lyskom();
	std::cout << "motd_of_lyskom = " << motd << std::endl;

	if (motd != 0) {
	    std::cout << "Message of the day:\n";
	    print_text(session->texts()[motd]);
	}
    }

    if (session->login(this_person, &pwd[0]) != st_ok)
    {
	std::cerr << "Login error.\n";
	std::exit(1);
    }

    unread_confs = new Unread_confs(session, this_person);
    command_interpreter();
}
