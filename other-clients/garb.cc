// Demoprogram till GARB, augusti 1993.
//
// Programmet kopplar upp sig mot kom.lysator.liu.se och ser efter vad
// m�te nummer 5 har f�r namn. Det anv�nder sig av ett antal olika
// metoder f�r att demonstrera dem.
//
// Det �r troligt att vissa saker kommer att �ndra sig ganska snart
// (kanske redan innan du l�ser denna GARB). De saker vi r�knar med
// att �ndra har vi markerat med "Not 1". Det g�ller hur man hanterar
// fel som uppst�r n�r man f�rs�ker h�mta n�got ur cachen. Just nu ska
// man kontrollera att foo.errno() == 0. Vi inf�r kanske n�gon annan
// metod snart.
//
// Detta program �r skrivet av Per Cederqvist. Inge Wallin var med och
// best�mde vad det skulle g�ra.
//
// Detta program �r, till skillnad mot resten av kom++, Public Domain.
//

#include <config.h>

#include <cstdlib>
#include <iostream>

#include "connection.h"
#include "conference.h"
#include "conf-stat.h"
#include "conf-cache.h"
#include "get_conf_stat_q.h"
#include "conn_io_isc.h"

int main()
{
  // Denna variabel anv�nds i alla exempel f�r att mellanlagra aktuell
  // m�tesstatus.
  Conf_stat   conf_stat;
  
  // Koppla upp mot servern. Objektet ``conn'' st�r f�r f�rbindelsen.
  Conn_io_ISC isc("kom.lysator.liu.se", "4894");
  Connection conn(&isc);
  Async_dispatcher disp(&conn);

  // H�mta namn med hj�lp av question.
  get_conf_stat_question c(&conn, 5);
  if (c.receive() == st_error)
    abort();
  conf_stat = c.result();
  std::cout << "Resultat med question: " << conf_stat.name() << std::endl;

  // G�r samma sak med hj�lp av cachen. F�rst m�ste vi skapa en cache.
  // I det h�r fallet skapar vi bara en cache f�r Conf_stat:tar. Vi
  // kallar den f|r CC och hoppas att Pripps ska se detta och sponsra
  // oss. Det har g�tt �t m�nga liter Coca Cola medan vi skrev
  // LysKOM...
  Conf_cache   CC(&conn, &disp);

  conf_stat = CC.get(5);
  if (conf_stat.kom_errno() != 0)	// Not 1.
    abort();
  std::cout << "Resultat med cachen: " << conf_stat.name() << std::endl;

  // �n en g�ng g�r vi samma sak (det b�rjar n�stan bli tjatigt nu),
  // men denna g�ng anv�nder vi oss av en �nnu h�gre abstraktionsniv�.
  // Vi b�rjar med att skapa ett objekt som st�r f�r m�ngden av alla
  // Conf_stat:tar.
  Conferences   all_confs(&conn, &disp);

  if (all_confs[5].kom_errno() != 0) // Not 1.
    std::abort();

  std::cout << "Resultat med Conferences: " << all_confs[5].name() 
	    << std::endl;

  return 0;
}
