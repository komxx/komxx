// news - Log in to a LysKOM server and print messages when texts are created.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#include <config.h>

#include <cassert>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <iostream>

#include "connection.h"
#include "conn_io_isc.h"
#include "get_time_q.h"
#include "login_q.h"
#include "conference.h"
#include "text-stat.h"
#include "async_dispatcher.h"
#include "async_text_created.h"

static std::vector<Text_stat> newtexts;

static Connection *port = NULL;
static Conferences *confs;

static void
find_password(char *res)
{
    char *home;
    if ((home = getenv("HOME")) == NULL)
    {
	std::cerr << "HOME is unset" << std::endl;
	exit(1);
    }
    char *buffer = new char[strlen(home) + 9];
    sprintf(buffer, "%s/.kompwd", home);
    FILE *fp = fopen(buffer, "r");
    if (fp == NULL)
    {
	perror(buffer);
	exit(1);
    }
    delete[] buffer;
    fgets(res, 20, fp);
    *strchr(res, '\n') = '\0';
}

static void async_new_text(const Async_text_created &t, int&)
{
    newtexts.push_back(t.text_stat());
    (*confs)[t.text_stat().author()].prefetch();
}

// static void async_sync_db(prot_a_LyStr args)
// {
//     cout << "Sync at " << flush;
//     system("date");
//     get_time_question r(port);
//     assert (r.receive() == st_ok);
//     cout << "Awake at " << flush;
//     system("date");
// }

int main(int argc,
	 char **argv)
{
    char pwd[100];
    find_password(&pwd[0]);
    Conn_io_ISC *conn_io = new Conn_io_ISC("kom.lysator.liu.se", "4894");
    port = new Connection(conn_io);
    if (port == NULL || port->connection_failed())
    {
	fprintf(stderr, "Can't connect to server");
	exit(1);
    }
    Async_dispatcher *disp = new Async_dispatcher(port);
    confs = new Conferences(port, disp);

    Kom_auto_ptr<Async_callback<Async_text_created> > ptr;
    ptr.reset(async_callback_function<Async_text_created, int>(
		  async_new_text, 0).release());
    Async_handler_tag<Async_text_created> text_created_tag(
	disp->register_text_created_handler(ptr));
    
    login_question in(port, argc == 2 ? atol(argv[1]) : 19, &pwd[0]);
    if (in.receive() != st_ok)
    {
	std::cerr << "Login error " << in.error() << "\n";
	exit(1);
    }

    std::cerr << "Start OK" << std::endl;

    while (1)
    {
	if (port->drain(100) == st_error)
	{
	    std::cerr << "Connection lost." << std::endl;
	    exit(1);
	}

	while (disp->handle_async() == st_pending)
	    continue;
	while (newtexts.empty() == false
	       && (*confs)[newtexts.front().author()].prefetch() != st_pending)
	{
	    Text_stat front = newtexts.front();
	    int       num_recips = front.num_recipients();
	    Conference author = (*confs)[front.author()];
	    author.prefetch();
	    for (int i = 0; i < num_recips; ++i) 
	    {
		(*confs)[front.recipient(i)->recipient()].prefetch();
	    }

	    std::cout << "Text " << front.text_no() << " in ";

	    for (int i = 0; i < num_recips; ++i) 
	    {
		Conference rec = (*confs)[front.recipient(i)
					      ->recipient()];
		if (rec.exists())
		    std::cout << rec.name();
		else
		    std::cout << "(unknown)";
		if (num_recips > 1 && i < num_recips - 1)
		    std::cout << ',';
		if (num_recips > 1 && i == num_recips - 2)
		    std::cout << " and";
		std::cout <<' ';
	    }

	    std::cout << "by ";
	    if (author.exists())
		std::cout << author.name() << "\n";
	    else
		std::cout << "(unknown)\n";

	    newtexts.erase(newtexts.begin());
	}
    }
}
