# A transient window itcl class
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# ----------------------------------------------------------------------
#  PURPOSE:  Make a window transient
#
#   AUTHOR:  Inge Wallin
#            ingwa@signum.se
#
# ======================================================================

# ----------------------------------------------------------------------
#  This class can be inherited when a window should be transient.
# ----------------------------------------------------------------------
itcl_class transient_window {
    # ------------------------------------------------------------------
    #  CONSTRUCTOR - create new transient_window
    # ------------------------------------------------------------------
    constructor {{master .} config} {
	wm transient $this $master    

	#
	#  Explicitly handle config's that may have been ignored earlier
	#
	foreach attr $config {
	    configure -$attr [set $attr]
	}
    }

    # ------------------------------------------------------------------
    #  DESTRUCTOR - destroy window containing widget
    # ------------------------------------------------------------------
    destructor {}

    # ------------------------------------------------------------------
    #  METHOD:  configure - used to change public attributes
    # ------------------------------------------------------------------
    method configure {config} {}

    # ------------------------------------------------------------------
    #  METHOD:  center - Center the window in relation to the master.
    # ------------------------------------------------------------------
    method center {{offset_x 0} {offset_y 0}} {
	set master [wm transient $this]

	set master_geometry [split [wm geometry $master] "x+"]
	set client_geometry [split [wm geometry $this] "x+"]

	# Get the size of the master window
	if {[wm grid $master] == ""} {
	    set master_size_x [lindex $master_geometry 0]
	    set master_size_y [lindex $master_geometry 1]
	} else {
	    set master_size_x [expr [lindex [wm grid $master] 0] \
				   * [lindex [wm grid $master] 2]]
	    set master_size_y [expr [lindex [wm grid $master] 1] \
				   * [lindex [wm grid $master] 3]]
	}

	# Get the size of the transient window
	if {[wm grid $this] == ""} {
	    set client_size_x [lindex $client_geometry 0]
	    set client_size_y [lindex $client_geometry 1]
	} else {
	    set client_size_x [expr [lindex [wm grid $this] 0] \
				   * [lindex [wm grid $this] 2]]
	    set client_size_y [expr [lindex [wm grid $this] 1] \
				   * [lindex [wm grid $this] 3]]
	}

	# Calculate the offset.
	set offset2_x [expr ($master_size_x - $client_size_x) / 2]
	set offset2_y [expr ($master_size_y - $client_size_y) / 2]
	
	wm geometry $this \
	    [format "+%d+%d" \
		 [expr [lindex $master_geometry 2] + $offset_y + $offset2_y] \
		 [expr [lindex $master_geometry 3] + $offset_x + $offset2_x]]
    }
}
