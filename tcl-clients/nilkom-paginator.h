// nilkom_paginator.h - Medium-level *more* prompt handling
// 
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_NILKOM_PAGINATOR_H
#define KOMXX_NILKOM_PAGINATOR_H

#include <iostream>

#include "LyStr.h"
#include "misc.h"
#include "user-input.h"
#include "paginator.h"

// A specialized paginator for use with nilkom.  It knows how to
// process user input.

class Nilkom_paginator : public Paginator {
public:
    Nilkom_paginator(User_input *input,
		     std::ostream &screen=std::cout);
    ~Nilkom_paginator();
    
    // Register keys the user wants to handle.
    void register_key(const LyStr &);

    // Print.  If a more prompt is issued, check for user input.  The
    // user can scroll (using RET or SPC) or abort the output (using
    // q).  Use more_status() to see the more-status.
    Nilkom_paginator &operator << (const LyStr &);
    
    // Check the current status.  If KEY is returned and pressed_key
    // is non-NULL, the pressed key is stored in *pressed_key.
    enum Page_status {
	READY,			// Everything has been printed.
	PENDING,		// Some output is pending.
	KEY,			// A user-handled key has been pressed
				// (implies that output is pending).
	TERMINATE		// "q" has been pressed, and the user
				// doesn't handle it.
    };
    Page_status more_status(char *pressed_key =NULL) const;

    // more_status() and blocking_status() will continue to return KEY
    // untill acknowledge_key() is called.
    void acknowledge_key();

    // Like more_status, but never return PENDING.  That is,
    // read keys (and do more processing) if there is pending output.
    Page_status blocking_status(char *pressed_key =NULL);
    
private:
    // These are not, and should not currently be, implemented.
    Nilkom_paginator(const Nilkom_paginator&);
    Nilkom_paginator &operator =(const Nilkom_paginator&);

    // Registered keys.
    LyStr registered_keys;

    // A key that the user has pressed, which has not yet been
    // acknowledged.  -1 if no key pressed.
    int unacknowledged_key;

    // Check if user input is available, and act on it.
    void maybe_scroll();

    // Where to read input from.  Not owned by Nilkom_paginator.
    User_input *input_channel;
};

#endif
