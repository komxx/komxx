// nilkom TCL command tables.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include "command_table.h"

Command_table::Command_table()
    : entries()
{
}

Command_table::~Command_table()
{
}


Command_table_entry::errcode
Command_table::add_entry(const LyStr & cmd_template,
			 const LyStr & cmd_pretty,
			 const LyStr & tcl_cmd)
{
  Command_table_entry te(cmd_template, cmd_pretty, tcl_cmd);
  if (te.status() != Command_table_entry::NOERR)
    return te.status();
  else
    {
      entries.push_back(te);
      return Command_table_entry::NOERR;
    }
}

Command_table::matchres
Command_table::decode_input(const LyStr &user_input, 
			    LyStr &tcl_cmd, 
			    Tcl_LyStr &alternatives)
{
  int penalty;
  int best_penalty = -1;
  LyStr pretty;
  enum matchres res = NOMATCH;
  
  for (std::vector<Command_table_entry>::iterator iter = entries.begin(); 
       iter != entries.end(); ++iter)
    {
      iter->try_match(user_input, tcl_cmd, pretty, penalty);
      if (penalty != -1)
	{
	  if (best_penalty == -1 || penalty < best_penalty)
	    {
	      res = UNIQUE;
	      alternatives.clear();
	      alternatives.append_element(pretty);
	      best_penalty = penalty;
	    }
	  else if (penalty == best_penalty)
	    {
	      alternatives.append_element(pretty);
	      res = AMBIGUOUS;
	    }
	}
    }
  
  return res;
}
