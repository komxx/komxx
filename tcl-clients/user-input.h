// user-input.h - Input from the user
// 
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_USER_INPUT_H
#define KOMXX_USER_INPUT_H

#include "connection.h"
#include "LyStr.h"
#include "conn_io_isc.h"

class User_input {
public:
    User_input(IscMaster *master, int infd);
    ~User_input();

    // Set a new idle callback function
    void  idle_callback(void (*callback)());

    // Return number of pending characters.  This is only an estimate.
    // It is guaranteed that:
    //    + at least this many characters are available.
    //    + this function doesn't return 0 if any characters are
    //      available when it is called.
    // Retuns negative on error.
    int pending(int timeout=0);

    // Return a character, or negative on error.
    int get_key();
private:
    LyStr pending_input;
    IscSession *in_cb;
    IscMaster  *in_master;	// Not owned by User_input.

    void (*idle_function)();

    int fill_pending_input(int timeout);

    // Not implemented:
    User_input(const User_input&);
    User_input &operator =(const User_input&);
};

#endif
