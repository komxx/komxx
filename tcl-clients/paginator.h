// paginator.h - Low-level *more* prompt handling
// 
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_PAGINATOR_H
#define KOMXX_PAGINATOR_H

#include <iostream>

#include "LyStr.h"
#include "misc.h"

// The Paginator class is responsible for sending output to a screen.
// It knows the width and height of the screen, and makes sure that
// data never scrolls past the user before he has had a chance to see
// it.
//
// This class is a little bit tricky to use.  The interface could be
// simpler.  It is the way it is because it is intended to be used in
// conjunction with TCL the way it is used in nilkom.

class Paginator {
public:
    Paginator(std::ostream &screen=std::cout);
    ~Paginator();
    
    // Return current screen dimensions, as used by paginator.
    int columns() const;
    int rows() const;

    // Return current screen dimensions, as paginator believes they are.
    int real_columns() const;
    int real_rows() const;
    
    enum Translation {
	TRANSPARENT,		// Print what is asked for.
	SWASCII			// Translate Swedish ISO 8859/1 chars to ascii.
    };

    // Set the print mode.  Default (until set with translate) is TRANSPARENT.
    void translate(Translation);

    // Enqueue some text for printing, and print as much as fits on
    // the screen.
    Paginator & operator <<(const LyStr &);
    
    // Mark the end of output from a command.  The next line of output
    // will contain a prompt.  That prompt should always fit on a
    // single line.
    void sync();

    // Find out how much enqueued output there is before the next sync
    // mark.
    long pending() const;

    // Discard all pending output before the sync mark, and print
    // the following prompt (if there is one).  If no sync mark has
    // been set yet, this will also discard all ouput written in the
    // future untill a sync mark is set.
    void discard();

    // This method returns true while Paginator is discarding input.
    bool discarding() const;
    
    // Get all pending output before the sync point.  Don't discard
    // it.
    LyStr get_pending();

    // Enforce a given width/height.  0 is infinity, -1 is default
    // (which means determine automatically).
    void set_width(int width);
    void set_height(int height);

    // Set prompt to use for more prompting.
    void set_more_prompt(const LyStr &);

    // Print some more lines.  Paginator implicitly assumes that the
    // user acknowledges every prompt printed after a
    // Paginator::sync() before more lines are printed.
    // A good way to use this method is "p.more_lines(p.rows() -
    // context)", where context is 1 or 2.  It will scroll a
    // screenfull.
    void more_lines(int lines);

    // Beep, or flash the screen, or do something similar.
    void bell() const;

    // Remove some characters
    void remove(LyStr rm);
 private:
    // Should all input to the next sync mark be discarded?
    bool is_discarding;

    // Is the sync mark currently set?
    bool sync_is_set;

    // Clear the current line and move the cursor to the left-most
    // column.  This is used to get rid of the *more* prompt.
    void clearline();

    // Print c if there is still room for it on the screen.
    // Return number of characters written.
    enum Screenarea {
      PROMPT, USER
    };
    int maybe_print(char c, Screenarea area);

    // Print as much as possible to the screen, possibly including a prompt.
    void update_screen();

    // Print a *more* prompt and set moring.
    void start_moring();

    // Is a *more* prompt present on the screen?
    bool moring;

    // This is active when paginator has written an automatic newline.
    // If the next character is a newline it is discarded.
    bool auto_newline_active;

    // The screen.
    std::ostream &sout;

    // Current screen size.
    int n_rows;
    int n_cols;

    // Currently used screen size (overrides n_rows and n_cols)
    int use_rows;
    int use_cols;

    // Translation mode (TRANSPARENT or SWASCII).
    Translation  translation;

    // Current cursor position.  Note: ``cursor_row'' is updated as if
    // the screen was cleared everytime a *more* prompt is removed (or
    // a sync point printed) even though the normal mode is to use
    // scrolling.
    int cursor_row;
    int cursor_col;

    // Pending output before the sync point.
    LyStr pending_output;
    // Pending output after the sync point.
    LyStr next_prompt;

    // Prompt to use.
    LyStr more_prompt;

    // These are not, and should not currently be, implemented.
    Paginator(const Paginator&);
    Paginator &operator =(const Paginator&);
};

#endif
