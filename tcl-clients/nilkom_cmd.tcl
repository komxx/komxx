# The command interpreter for the nilkom client.
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# ----------------------------------------------------------------
# global variables (local to this file)
#
#  nilkom_prompt - the current prompt. Determines the next command to use.
#                  Values beginnig with NK_ belong entirely to the TCL layer,
#                  other values originate from [kom_unread prompt].

# global variables (shared)
#  review_list   - list of textnos to review

# The command table. Each entry consists of 
#    - a user command template
#    - a TCL command template
#    - a pretty-printing user command name (optional)
#
# The user command template is a list of words.  The input the user
# enters must match the words in the user command template.  The
# template can contain argument placeholders.  They take one of the
# following forms:
#    %I   - An integer.
#    %R   - The rest of the line.
# In the future, more forms will be supported.  Example: %CONFUNIQ
# will mean a unique conference name.
#
# If the match routine can find a best-match from the table, the TCL
# command template will be used to form a TCL command to execute.  In
# its simplest form, a TCL command template is a TCL command to
# execute.  If the User command template contained a placeholder, the
# corresponding user-supplied argument is inserted when a word
# consisting of %<digit> is encountered.  %0 is the leftmost argument,
# %1 the next, and so on.
#
# The pretty-printing user command name is used by the help commands.

# uttr�d (ur) m�te implementerat av Peter Antman, 22/1 1997. 
set cmdtab {
    {{vilka}
	{who_is_on_small}
	{vilka (�r inloggade)}}
    {{l�ng vilkalista}
	{who_is_on_verbose}
	{(visa) l�ng vilkalista}}
    {{se tiden} 
	{se tiden}}
    {{sluta}
	{quit_command}}
    {{markera}
	{mark $senaste 127}
	{markera (senast l�sta med markering 127)}}
    {{markera %I}
	{mark %0 127}
	{markera <inl�ggsnummer> (med markering 127)}}
    {{markera %I %I} 
	{mark %0 %1}
	{markera <inl�ggsnummer> (med markering) <markeringsnummer>}}
    {{avmarkera}
	{unmark $senaste}
	{avmarkera (senast l�sta)}}
    {{avmarkera %I}
	{unmark %0}
	{avmarkera <inl�ggsnummer>}}
    {{�terse senaste} 		
	{print_text $senaste}}
    {{�terse %I} 		
	{print_text %0}
	{�terse <inl�ggsnummer>}}
    {{�terse text %I} 
	{print_text %0}
	{�terse text <inl�ggsnummer>}}
    {{n�sta m�te} 	
	{next_conference}}
    {{g� %R}
	{goto_named_conference %0}
	{g� (till) <m�tesnamn>}}
    {{kommentera} 
	{k}}
    {{kommentera %I} 
	{k %0}
	{kommentera <inl�ggsnummer>}}
    {{kommentera kommenterade} 
	{kk}
	{kommentera (och �terse det) kommenterade}}
    {{personligt}
	{start_personal_answer $senaste}
	{personligt (svar p� senaste inl�gget)}}
    {{personligt %I}
	{start_personal_answer %0}
	{personligt (svar p� inl�gg) <inl�ggsnummer>}}
    {{inl�gg}
	{start_write_text}
	{(skriva) inl�gg}}
    {{brev %R}
	{start_write_letter %0}
	{(skriva) brev (till) <namn>}}
    {{mottagare: %R}
	{create_text_add_recipient %0}
	{(l�gg till) mottagare: <m�tesnamn> (till det inl�gg som editeras)}}
    {{extra kopia: %R}
	{create_text_add_cc_recipient %0}
	{(l�gg till) extra kopia: <m�tesnamn> (till det inl�gg som editeras)}}
    {{addera kommentar: %I}
	{create_text_add_comment %0}
	{addera kommentar: <inl�ggsnummer> (till det inl�gg som editeras)}}
    {{addera fotnot: %I}
	{create_text_add_footnote %0}
	{addera fotnot: <inl�ggsnummer> (till det inl�gg som editeras)}}
    {{subtrahera %R}
	{subtract_recipient %0}
	{subtrahera <m�tesnamn> (fungerar �nnu bara p� inl�gg som editeras)}}
    {{forts�tt}
	{create_text_resume_edit}
	{forts�tt (editera inl�gget)}}
    {{hela} 
	{create_text_redisplay; print "\n"}
	{(visa) hela (inl�gget som editeras)}}
    {{s�nd till %R}
	{send_message_command %0}
	{s�nd (meddelande) till <personnamn>}}
    {{till %R}
	{send_message_command %0}
	{(s�nd meddelande) till <personnamn>}}
    {{s�nd alarmmeddelande}
	{send_alert_message_command}
	{s�nd alarmmeddelande (till alla)}}
    {{�terse alla kommentarer}		
	{review_all_comments $senaste}}
    {{�terse alla kommentarer %I}
	{review_all_comments %0}
	{�terse alla kommentarer (till) <inl�ggsnumemr>}}
    {{�terse kommenterade} 		
	{review_commented $senaste}
	{�terse (det) kommenterade}}
    {{�terse kommenteras av %I}
	{review_commented %0}
	{�terse (det som) kommenteras av <inl�ggsnummer>}}
    {{�terse presentation %R}
	{review_presentation %0}
	{�terse presentation (f�r) <m�te/person>}}
    {{skicka} 
	{enter_text}
	{skicka (in texten)}}
    {{bort} 
	{throw_text}
	{(kasta) bort (inl�gget som editeras)}}
    {{l�smarkera inte}
	{reject}
	{l�smarkera inte (senast l�sta text)}}
    {{lista kommandon}
	{list_commands}
	{lista (alla) kommandon}}
    {{lista nyheter}
	{unread}
	{lista (alla) nyheter}}
    {{lista m�ten}
	{list_conferences}
	{lista (alla) m�ten}}
    {{lista personer}
	{list_persons}
	{lista (alla) personer}}
    {{medlem %R}
	{join_conference %0}
	{(bli) medlem (i) <m�tesnamn>}}
    {{uttr�da %R}
	{disjoin_conference %0}
	{uttr�da (ur) <m�tesnamn>}}
    {{s�tt sk�rmstorlek default}
	{nilkom_page pagesize -1 -1}
	{s�tt sk�rmstorlek (till) default}}
    {{s�tt sk�rmstorlek o�ndlig}
	{nilkom_page pagesize 0 0}
	{s�tt sk�rmstorlek (till) o�ndlig}}
    {{s�tt sk�rmstorlek %I %I}
	{nilkom_page pagesize %0 %1}
	{s�tt sk�rmstorlek (till) <rader> <kolumner>}}
    {{hj�lp}
	{help}}
    {{?}
	{help}
	{(Vad kan jag g�ra)?}}
    {{help}
	{help}}
}

set cmd_table [command_parser define $cmdtab]

proc list_commands {} {
    global cmdtab

    foreach cmd $cmdtab {
	if {[llength $cmd] > 2} {
	    set x [print "[lindex $cmd 2]\n"]
	} else {
	    set x [print "[lindex $cmd 0]\n"]
	}
	if {$x == "terminate"} {
	    return
	}
    }
}

proc calculate_prompt {} {
    global nilkom_prompt
    global review_list
    global last_message_reply_to
    global message_queue
    global prompt_state

    trim_review_list
    if {$last_message_reply_to != ""} {
	set nilkom_prompt NK_SEND_REPLY
    } elseif {[llength $message_queue] > 0} {
	set nilkom_prompt NK_RECEIVE
    } elseif {[llength $review_list] > 0} {
	set nilkom_prompt NK_REVIEW
    } else {
	set nilkom_prompt [kom_unread prompt]
    }

    set prompt_state ""
    case $nilkom_prompt in {
	NEXT_CONF     {return "G� till n�sta m�te - "}
	NEXT_TEXT     {return "L�sa n�sta text - "}
	NEXT_COMMENT  {return "L�sa n�sta kommentar - "}
	NEXT_FOOTNOTE {return "L�sa n�sta fotnot - "}
	NO_MORE_TEXT  {set prompt_state "/"; return "Se tiden - "}
	NK_REVIEW     {return "�terse n�sta - "}
	NK_RECEIVE    {return "Ta emot n�sta meddelande - "}
	NK_SEND_REPLY {return "S�nda svar p� meddelande - "}
    }
}

proc default_action {} {
    global nilkom_prompt
    global to_mark_as_read

    foreach tno $to_mark_as_read {
	kom_text mark_as_read $tno
    }
    set to_mark_as_read {}

    case $nilkom_prompt in {
      NEXT_CONF     {set new_conf [kom_unread goto_next_conf]; \
		     print [format "Du hamnade i %s (%d ol�sta)\n" \
				[kom_conference name $new_conf] \
				[lindex [kom_unread unread_texts_in_current] \
				        1]]}
      NEXT_TEXT     {read_text [kom_unread next_text remove]}
      NEXT_COMMENT  {read_text [kom_unread next_text remove]}
      NEXT_FOOTNOTE {read_text [kom_unread next_text remove]}
      NO_MORE_TEXT  {se tiden}
      NK_REVIEW     {review_next}
      NK_RECEIVE    {receive_message_command}
      NK_SEND_REPLY {reply_to_message_command}
    }
}

proc isoify {str} {
    regsub -all \{ $str � str
    regsub -all \} $str � str
    regsub -all \\\| $str � str
    regsub -all \\\[ $str � str
    regsub -all \\\] $str � str
    regsub -all \\\\ $str � str
    return $str
}

# do_command is called whenever the user has entered a command.
# The command is present in $cmd.  If the command is empty, a default
# action is performed.  If the first character is "=", the remainder
# of the line is executed as TCL code.  Otherwise, the command table
# in $cmd_table is used to construct a TCL command to execute.  If no
# match is found, an error message is printed.
proc do_command {cmd} {
    global cmd_table
    global errorCode

    kom_session user_active
    nilkom_page register "" "**MER** (SPC, RET, q)"
    if {[regexp {^ *$} $cmd]} {
	perform_command default_action
	return
    }

    if {[string range $cmd 0 0] == "="} {
	if {[catch {uplevel \#0 [string range $cmd 1 end]} res] == 0} {
	    if {[string length $res] > 0} {
	        print "$res\n"
	    }
	} else {
	    puts "Error: $res"
	    puts "errorCode: $errorCode"
	}
	return
    }

    # FIXME: This is bad for internationalization.
    set cmd [isoify $cmd]

    set match [command_parser match $cmd_table $cmd]

    if {[lindex $match 0]} {
	perform_command [lindex $match 1]
    } elseif {[lindex $match 1] == "NOMATCH"} {
	print "Jag f�rst�r inte vad du menar\n"
    } else {
	report_ambiguous [lindex $match 2]
    }
}

proc perform_command {cmd} {
    global senaste
    global errorCode

    # puts "DOING-->$cmd<--"
    if {[catch {eval $cmd} res] != 0} {
	print "Error: $res"
	print "errorCode: $errorCode"
    }
}

proc report_ambiguous {alts} {
    print "Du kan mena n�got av f�ljande:\n"
    foreach m $alts {
        if {[print "  $m\n"] == "terminate"} {
	    return
	}	
    }
}
