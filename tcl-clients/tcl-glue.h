// Declaration of the functions in tcl-glue.cc
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#ifndef KOMXX_TCL_GLUE_H
#define KOMXX_TCL_GLUE_H

#include "connection.h"
#include "conn_io_isc.h"

#include <tcl.h>

extern "C" {
void kom_define_commands(Tcl_Interp *interp);
}

extern IscMaster * mcb_for_isc;

#endif
