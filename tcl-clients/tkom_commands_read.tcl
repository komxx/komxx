# Commands found under the "Read" menu in tkom
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# ================================================================
#              Command: Show number of unread texts


proc show_unread_command {} {
    if {![start_of_command show_unread_command]} {
	return
    }

    #
    # Start by prefetching the information we need
    #
    set unread_confs [llength [kom_unread unread_texts]]
    set prefetch_calls 0
    while {[kom_unread prefetch 0 0 0 0 20 $unread_confs 40] > 0} {
        if {$prefetch_calls == 0} {
            show_message "Jag m�ste h�mta lite information fr�n servern\n"
            show_message "V�nta ett tag.."
        }
        show_message "."
	update
	incr prefetch_calls 1
    }
    if {$prefetch_calls > 0} {
        show_message "klart\n"
    }

    #
    # Ok, time to show how many unread texts we have.
    #
    set mintotal 0
    set maxtotal 0
    show_message "Du har ol�sta inl�gg i f�ljande m�ten:\n"
    foreach conf [kom_unread unread_texts] {
        set confno [lindex $conf 0]
        set min    [lindex $conf 1]
        set max    [lindex $conf 2]

	if {$max == $min} {
	    if {$max > 0} {
                show_message [format "     %6d %s\n" $min \
                                [kom_conference name $confno]]
            }
	} else {
                show_message [format "%4d-%6d %s\n" $min $max \
                                [kom_conference name $confno]]
	}
	incr mintotal $min
        incr maxtotal $max
    }
    if {$mintotal == $maxtotal} {
        show_message "\nTotalt $mintotal ol�sta\n\n"
    } else {
        show_message "\nTotalt mellan $mintotal och $maxtotal ol�sta\n"
    }

    end_of_command
}


# ================================================================
#                      Command: Goto conference


# This variable is 1 when there are alternatives to choose from
# in the listbox 'conflist'.
set conflist_contains_confs   0


proc goto_conf_window {w} {
    global romanfont
    global boldfont

    butwin $w -buttons {{"V�lj"} {"Avbryt"}}
    wm title $w "G� till m�te"

    # The selector frame
    frame $w.top 
    pack $w.top -side top -fill both
    listbox $w.top.conflist -relief raised -setgrid true \
	-yscrollcommand "$w.top.listscroll set"
    scrollbar $w.top.listscroll -relief flat -command "$w.top.conflist yview"
    pack $w.top.conflist -side left -expand TRUE -fill both
    pack $w.top.listscroll -side right -fill y

    lblentry $w.conf -lbltext "M�te" -lblwidth 6
    pack $w.conf -side top -fill x 

    bind $w.top.conflist <Double-1>  "goto_conf_click_on_conf $w %y"
}


proc goto_conf_window_destroy {w} {
    $w.conf delete
    $w delete
}

proc goto_conf_click_on_conf {w y} {
    global conflist_contains_confs

    if {$conflist_contains_confs} {
        $w.conf put [$w.top.conflist get [$w.top.conflist nearest $y]]
    }
}


proc goto_conf_command {{conf {}}} {
    global conflist_contains_confs
    global passwd_string

    if {![start_of_command goto_conf_command]} {
	return
    }

    goto_conf_window .goto_conf
    tkwait visibility .goto_conf
    if {$conf != ""} {
	$w.conf put $conf
    }
    set conflist_contains_confs 0

    while {1} {
	.goto_conf.conf focus
        if {[.goto_conf wait] == 0} {
	    set conf_to_go_to [.goto_conf.conf get]
            set matches [filter-confs [kom_session lookup_name $conf_to_go_to]]
            .goto_conf.top.conflist delete 0 end

            set conflist_contains_confs 0
            if {[llength $matches] == 0} {
                .goto_conf.top.conflist delete 0 end
                .goto_conf.top.conflist insert end "Ingen tr�ff p� namnet"
                set conflist_contains_confs 0

            } elseif {[llength $matches] == 1} {
                # Only 1 conf matched ==> Try to go there.
                set confno [microconf confno [lindex $matches 0]]

                if {[safe_eval {kom_unread goto_conf $confno; format TRUE} \
			       {{{KOM} {return FALSE}}}]} \
		{
		    show_current_conf $confno
                    set current_conf  $confno
                    goto_conf_window_destroy .goto_conf
                    end_of_command
                    return
                } else {
                    .goto_conf.top.conflist delete 0 end
                    .goto_conf.top.conflist insert end "Du fick inte g� dit."
                    set conflist_contains_confs 0
                }

            } elseif {[llength $matches] > 1} {
		foreach match $matches {
		    set confno [microconf confno $match]
		    .goto_conf.top.conflist insert end \
			[kom_conference name $confno]
                }
                set conflist_contains_confs 1
            }
        } else {
	    goto_conf_window_destroy .goto_conf
	    end_of_command
            return
        }
    }
}
