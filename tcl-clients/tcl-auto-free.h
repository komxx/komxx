// Utility class, useful when dealing with Tcl_SplitList
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#ifndef KOMXX_TCL_AUTO_FREE_H
#define KOMXX_TCL_AUTO_FREE_H

#include <cstdlib>

// This class is used to facilitate memory management in cooperation
// with Tcl_SplitList. The casts and the fact that p is a const
// char**, not a const char*, are not bugs. See the documentation for
// Tcl_SplitList.
class tcl_auto_free {
  public:
    tcl_auto_free()  { p = NULL; }
    ~tcl_auto_free() { ::free((char *)p); }
    void free()  { ::free((char *)p); p = NULL; }
    const char **p;
};

#endif
