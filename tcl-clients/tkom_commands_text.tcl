# Commands found under the "Text" menu in tkom
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# ================================================================
#                         Command: Mark


itcl_class mark_window {
    inherit butwin transient_window

    constructor {{textno ""} {mark ""} config} {
	butwin::constructor -buttons \
	    [list [list "Markera texten" "$this _mark_text"] \
		 [list "Avbryt" "show_message \"Markeringen avbruten!\n\"; $this delete"]]
	transient_window::constructor
#	transient_window $this . "Markera text"

	lblentry $this.textno -lbltext "Text nummer:" -lblwidth 18
	lblentry $this.markno -lbltext "Markering (1-255):" -lblwidth 18
	pack $this.textno $this.markno -side top -fill both

	$this.textno put $textno
	$this.markno put $mark
	$this.textno focus

	center
    }

    destructor {
	$this.textno delete
	$this.markno delete
    }

    method _mark_text {} {
	set textno [$this.textno get]
	set mark   [$this.markno get]

	if {[kom_text mark $textno $mark]} {
	    show_message "Text $textno �r nu markerad med markering $mark.\n"
	    $this delete
	} else {
	    show_message "Det gick inte att markera $textno.\n"
	}
    }
}


proc mark_command {{textno {}} {mark 255}} {
    if {![start_of_command mark_command]} {
	return
    }

    mark_window .mark $textno $mark
    tkwait window .mark

    end_of_command
}


# ================================================================
#                          Command: Unmark 


itcl_class unmark_window {
    inherit butwin transient_window

    constructor {{textno ""} config} {
	butwin::constructor -buttons \
	    [list [list "Avmarkera texten" "$this _unmark_text"] \
		 [list "Avbryt" "show_message \"Avmarkeringen avbruten!\n\"; $this delete"]]
	transient_window::constructor
#	transient_window $this . "Avmarkera text"

	lblentry $this.textno -lbltext "Text nummer:" -lblwidth 18
	pack $this.textno -side top -fill both

	$this.textno put $textno
	$this.textno focus

	center
    }

    destructor {
	$this.textno delete
    }

    method _unmark_text {} {
	set textno [$this.textno get]

	if {[kom_text unmark $textno]} {
	    show_message "Text $textno �r nu avmarkerad.\n"
	    $this delete
	} else {
	    show_message "Det gick inte att avmarkera $textno.\n"
	}
    }
}


proc unmark_command {{textno {}} } {
    if {![start_of_command mark_command]} {
	return
    }

    unmark_window .unmark $textno
    tkwait window .unmark

    end_of_command
}
