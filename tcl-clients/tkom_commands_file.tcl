# Commands found under the "File" menu in tkom
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# ================================================================
#                     Command: About tkom


itcl_class about_window {
    inherit butwin transient_window

    constructor {config} {
	butwin::constructor -buttons {{"St�ng f�nstret"}}
	transient_window::constructor .

	global tkom_version

	frame $this.top -relief raised -bd 2
	pack $this.top -side top -fill both

	message $this.top.msg -width 3i \
	    -font -adobe-times-medium-r-normal-*-180-* \
	    -text "Detta �r tkom version $tkom_version.\n\nTkom �r skrivet av Inge Wallin (inge@lysator.liu.se) och Per Cederqvist (ceder@lysator.liu.se)."
	pack $this.top.msg -side left -fill both -padx 5m -pady 5m

	center
    }

    destructor {}
}


proc show_about_command {} {
    if {![start_of_command show_about_command]} {
	return
    }

    about_window .about 
    .about wait
    .about delete

    end_of_command
}


# ================================================================
#                        Command: Quit

# NOTE: The yes-or-no window might be needed somewhere else later,
#       and then it should be moved.


itcl_class yes_or_no_window {
    inherit butwin transient_window

    constructor {text {okbutton Ok} {cancelbutton Avbryt} {default -1} config} {
	butwin::constructor \
	    -buttons [list [list $okbutton {}] [list $cancelbutton {}]] \
	    -default $default
	transient_window::constructor

	frame $this.top -relief raised -bd 2
	pack $this.top -side top -fill both

	message $this.top.msg -width 3i \
	    -font -adobe-times-medium-r-normal-*-180-* \
	    -text $text
	pack $this.top.msg -side left -fill both -padx 5m -pady 5m

	center
    }

    destructor {}
}


proc yes_or_no_p {text {yesbutton Ja} {nobutton Nej} {default -1}} {
    yes_or_no_window .dialog $text $yesbutton $nobutton $default

    set result [.dialog wait]
    .dialog delete

    return $result
}


proc tkom_quit_command {} {
    if {![start_of_command tkom_quit_command]} {
	return
    }

    set result [yes_or_no_p "Vill du verkligen avsluta tkom?" Ja Nej 1]

    if {$result == 0} {
        exit
    }

    end_of_command
}
