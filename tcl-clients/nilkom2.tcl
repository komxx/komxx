# Some miscellaneous code for the nilkom client.
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# global variables (shared)
#  review_list   - list of textnos to review

proc read_text {tno} {
    global to_mark_as_read

    nilkom_page register s "**MER** (SPC, RET, s, q): "

    print_text $tno
    switch -glob [nilkom_page flush] {
	ready  {
	    lappend to_mark_as_read $tno
	}
	key*   {
	    # The user pressed "s". 
	    nilkom_page discard
	    nilkom_page acknowledge_key
	    nilkom_page sync
	    lappend to_mark_as_read $tno
	    print "Texten kommer att l�smarkeras om du inte ger kommandot\n"
	    print "  l�smarkera inte\n"
	}
	terminate {
	    nilkom_page sync
	    print "Text $tno l�smarkeras inte\n"
	}
	* {
	    print "WNG: Internal error: Strange return value from sync.\n"
	}
    }
}

proc k {{parent senaste}} {
	global senaste

	if {[string compare $parent senaste] == 0} {
            start_comment $senaste
	} else {
            start_comment $parent
	}
}
proc kk {} {
	global senaste

	set s $senaste
    	review_commented $s
    	start_comment $s
	set senaste $s
}
	
proc next_conference {} {
    set new_conf [kom_unread goto_next_conf]
    print [format "Du hamnade i %s (%d ol�sta)\n" \
	       [kom_conference name $new_conf] \
	       [lindex [kom_unread unread_texts_in_current] 1]]
}

proc goto_named_conference {name} {
    set confno [select_unique_any "Vilket m�te vill du g� till? " $name]
    # FIXME: Kontrollera redan h�r om personen �r med i m�tet eller ej.
    #        Klienten vet ju vilka m�ten personen �r med i.
    # FIXME: Om man inte �r med i m�tet ska man f� ett erbjudande om
    #        att g� med.
    if {$confno == ""} {
	if {[kom_unread current_conf]} {
	    print [format "Du st�r kvar i %s (%d ol�sta)\n" \
		       [kom_conference name [kom_unread current_conf]] \
		       [lindex [kom_unread unread_texts_in_current] 1]]
	} else {
	    if {1} {
		print "Du k�nner dig fr�nvarande.\n"
	    } else {
		print "Du �r inte n�rvarande i n�got m�te.\n"
	    }
	}
    } else {
	set failed 0
	safe_eval {kom_unread goto_conf $confno} {
	    {
		{KOM FAILED-CALL}
		{
		    set failed 1
		}
	    }
	}
	if {$failed} {
	    # FIXME: Kontrollera returv�rdet ovan.  Varf�r
	    #        fick personen inte g� till m�tet?
	    print [format "Du kan inte g� till m�te %d (%s)\n" \
		       $confno [kom_conference name $confno]]
	    print "Antagligen �r du inte medlem i m�tet.\n"
	    print "Anv�nd kommandot \"medlem\" f�r att g� med i m�tet.\n"
	} else {
	    print [format "%s (%d ol�sta)\n" \
		       [kom_conference name $confno] \
		       [lindex [kom_unread unread_texts_in_current] 1]]
	}
    }
}

proc join_conference {name} {
    set confno [select_unique_any "Vilket m�te vill du bli medlem i? " $name]
    if {$confno == ""} {
	print "Inget.\n"
	return
    }
    # FIXME: Anv�ndaren vill kanske ha andra prioriteter...

    set failed 0
    safe_eval {kom_unread join_conf $confno 99 1} {
	{
	    {KOM FAILED-CALL}
	    {
		set failed 1
	    }
	}
    }
    if {$failed} {
	print "Du kunde inte bli medlem i m�tet.  Denna version av nilkom\n"
	print "�r inte smart nog att ber�tta varf�r; antagligen �r\n"
	print "[kom_conference name $confno] ett slutet m�te\n"
	print "eller en persons brevl�da.  Elisp-klienten �r b�ttre p�\n"
	print "att diagnosticera fel.\n"
    } else {
	print "Du �r nu medlem i [kom_conference name $confno].\n"
	print "nilkom har gett det prioritet 99, och det finns n�st f�rst\n"
	print "p� din m�teslista.  nilkom kommer inte att ge m�tet r�tt\n"
	print "prioritet och placering denna session.\n"
    }
}

# Implementerad av Peter Antman 22/1 1997.
proc disjoin_conference {name} {
 set confno [select_unique_any "Vilket m�te vill du uttr�da ur? " $name]
    if {$confno == ""} {
	print "Inget.\n"
	return
    }
     set failed 0
    safe_eval {kom_unread disjoin_conf $confno} {
	{
	    {KOM FAILED-CALL}
	    {
		set failed 1
	    }
	}
    }
    if {$failed} {
	print "Du kunde inte uttr�da ur m�tet.\n"  
	print "Denna version av nilkom �r inte smart nog att ber�tta varf�r.\n"
	print "Antingen var du inte medlem eller s� fanns inte m�tet.\n"
    } else {
	print "Du �r nu inte l�ngre medlem i [kom_conference name $confno].\n"
    }
}


proc select_unique_any {prompt name} {
    set confno ""
    while {$confno == ""} {
	# Don't ask for the name if a name was supplied to this function.
	if {$name == ""} {
	    set name [getstr $prompt]
	}
	if {$name == ""} {
	    return ""
	}
	if {[regexp {^p[ \t]+([0-9]+)$} $name dummy confno]} {
	    safe_eval {kom_person number $confno} {
		{
		    {KOM BADPERS}
		    {
			print "Person nummer $confno finns inte\n"
			set confno ""
		    }
		}
	    }
	} elseif {[regexp {^m[ \t]+([0-9]+)$} $name dummy confno]} {
	    safe_eval {kom_conference number $confno} {
		{
		    {KOM BADCONF}
		    {
			print "M�te nummer $confno finns inte\n"
			set confno ""
		    }
		}
	    }
	} else {
	    set matches [kom_session lookup_name $name]
	    if {[llength $matches] == 0} {
		# No match
		set confno ""
		print "Det finns inget m�te som matchar $name\n"
	    } elseif {[llength $matches] == 1} {
		set confno [microconf confno [lindex $matches 0]]
	    } else {
		set confno [select_a_match $matches]
	    }
	}
	# Make sure the second and subseqent loops ask the user for the name.
	set name ""
    }

    return $confno
}

proc reject {} {
    global to_mark_as_read

    if {[llength $to_mark_as_read]} {
	print "Text $to_mark_as_read l�smarkeras ej.\n"
	set to_mark_as_read {}
    } else {
	print "Inga texter var k�ade f�r l�smarkering.\n"
    }
}

proc quick_unread {} {
    set total 0

    print "Du har ol�sta inl�gg i f�ljande m�ten:\n\n"
    foreach conf [kom_unread unread_texts] {
        set confno [lindex $conf 0]
        set min [lindex $conf 1]
        set max [lindex $conf 2]

	if {$max == $min} {
	    print [format "     %6d %s\n" $min \
		[kom_conference name $confno]]
	} else {
	    print [format "%4d-%6d %s\n" $min $max \
		[kom_conference name $confno]]
	}
	incr total $min
    }
    print "\nTotalt minst $total ol�sta\n\n\n"
}

proc unread {} {
    set unread_confs [llength [kom_unread unread_texts]]
    set neednewline 0
    while {[kom_unread prefetch 0 0 0 0 20 $unread_confs 70] > 0} {
    }
    quick_unread
}

proc timeout {} {
    global prefetch_state
    global nilkom_prompt
    global prompt_state
    global idling_disabled

    kom_session handle_async
    switch $prefetch_state {
	notyet	
	{
	}
	0
	    {
    		if {[kom_unread prefetch 0 0 0 1000 1000 \
			 [llength [kom_unread unread_texts]] 2] == 0} {
		    set prefetch_state 1
		}
	    }
	1
	    {
		kom_unread prefetch 3 0 0 0 0 0 2
	    }
    }

    if {$prompt_state != "" && $idling_disabled == 0} {
	nilkom_page remove " "
	nilkom_page remove $prompt_state
	switch -exact -- $prompt_state {
	    - { set prompt_state "\\" }
	    \\ { set prompt_state "|" }
	    | { set prompt_state "/" }
	    / { set prompt_state "-" }
	}
	print "$prompt_state "
    }

    return 0
}

proc trim_review_list {} {
    global review_list

    set f 1
    while {$f==1} {
	set f 0
	if {[llength $review_list] > 0} {
	    safe_eval {kom_text author [lindex $review_list 0]} {
		{
		    {KOM BADTEXT}
		    {
			set review_list [lrange $review_list 1 end]
			set f 1
		    }
		}
	    }
	}
    }
}

proc review_next {} {
    global review_list

    if {[llength $review_list] == 0} {
	print "Det finns inget att se h�r.\n"
    } else {
	print_text [lindex $review_list 0]
	set review_list [lrange $review_list 1 end]
    }
}

proc review_commented {tno} {
    global review_list

    set l {}
    set i 0
    while {$i<[kom_text num_uplinks $tno]} {
	lappend l [uplink parent [kom_text uplink $tno $i]]
	incr i
    }

    set review_list "$l $review_list"
    trim_review_list
    review_next
}

proc review_all_comments {tno} {
    global review_list

    set l {}

    set i 0
    while {$i<[kom_text num_footnotes $tno]} {
	lappend l [kom_text footnote $tno $i]
	incr i
    }

    set i 0
    while {$i<[kom_text num_comments $tno]} {
	lappend l [kom_text comment $tno $i]
	incr i
    }

    set review_list "$l $review_list"
    trim_review_list
    review_next
}

proc review_presentation {name} {
    set confno [select_unique_any "Vad vill du se presentationen f�r? " $name]
    if {$confno == ""} {
	print "Du vill visst inte se n�gon presentation.\n"
	return
    }

    set pres [kom_conference presentation $confno]
    if {$pres == 0} {
	print "[kom_conference name $confno] har ingen presentation.\n"
    } else {
	print_text $pres
    }
}


proc mark {tno type} {
    safe_eval {
	kom_text mark $tno $type
	print "Text $tno markerad med markeringstyp $type.\n"
    } {
	{{KOM BADTEXT} {print "Det finns ingen s�dan text.\n"}}
    }
}

proc unmark {tno} {
    safe_eval {
	kom_text unmark $tno
	print "Text $tno avmarkerad.\n"
    } {
	{{KOM BADTEXT} {print "Det finns ingen s�dan text.\n"}}
    }
}

proc help {} {
    global nilkom_version

    print "V�lkommen till nilkom!\n"
    print "\n"
    print "Trycker RETURN f�r att utf�ra defaultkommandot.\n"
    print "Tryck Ctrl-C f�r att avsluta nilkom.\n"
    print {Skriv "lista kommandon" f�r att se vad du kan g�ra.}
    print "\nSkicka buggrapporter till ceder@lysator.liu.se.\n"
    print "Ha t�lamod med att m�nga finesser och kommandon saknas.\n"
    print "Detta �r nilkom $nilkom_version\n"
}

proc who_is_on_small {} {
    set people [kom_session who_is_on] 
    foreach session $people {
	set line [format "%4d %-38.38s %-35.35s\n" \
		      [lindex $session 2] \
		      [safe_eval {kom_conference name [lindex $session 0]} {
			  {{KOM BADCONF 0 8} {format "oinloggad person"}}
			  {{KOM BADCONF} {format "hemlig person"}}}] \
		      [safe_eval {kom_conference name [lindex $session 1]} {
			  {{KOM BADCONF 0 8}
			      {format "ej n�rvarande i n�got m�te"}}
			  {{KOM BADCONF} {format "hemligt m�te"}}}]]
	if {[print $line] == "terminate"} {
	    return
	}
    }
    print "Totalt [llength $people] personer.\n"
}

proc who_is_on_verbose {} {
    set people [kom_session who_is_on] 
    foreach session $people {
	set line [format "%4d %-38.38s %-35.35s\n" \
		      [lindex $session 2] \
		      [safe_eval {kom_conference name [lindex $session 0]} {
			  {{KOM BADCONF 0 8} {format "oinloggad person"}}
			  {{KOM BADCONF} {format "hemlig person"}}}] \
		      [safe_eval {kom_conference name [lindex $session 1]} {
			  {{KOM BADCONF 0 8}
			      {format "ej n�rvarande i n�got m�te"}}
			  {{KOM BADCONF} {format "hemligt m�te"}}}]]
	if {[print $line] == "terminate"} {
	    return
	}
	set line [format "     %-38.38s %-35.35s\n" \
		      [lindex $session 4] [lindex $session 3]]
	if {[print $line] == "terminate"} {
	    return
	}
    }
    print "Totalt [llength $people] personer.\n"
}    

proc list_conferences {} {
    set microconflist [filter-confs [kom_session lookup_name ""]]
    set num_confs 0
    foreach mc $microconflist {
	kom_conference prefetch_uconf [microconf confno $mc]
    }
    foreach mc $microconflist {
	set confno [microconf confno $mc]
	set line [format "%5d %s\n" $confno [kom_conference name $confno]]
	if {[print $line] == "terminate"} {
	    return
	}
	incr num_confs
    }
    print "\nTotalt $num_confs m�ten\n"
}


proc list_persons {} {
    set microconflist [filter-persons [kom_session lookup_name ""]]
    set num_confs 0
    foreach mc $microconflist {
	set confno [microconf confno $mc]
	set line [format "%5d %s\n" $confno [kom_conference name $confno]]
	if {[print $line] == "terminate"} {
	    return
	}
	incr num_confs
    }
    print "\nTotalt $num_confs personer\n"
}

proc send_message_command {name} {
    set persno [select_unique_any \
		    "Skicka direktmeddelande till: " $name]
    if {$persno == ""} {
	print "Inget meddelande skickat.\n"
	return
    }
    print "Skicka meddelande till [kom_conference name $persno].\n"
    set msg [getstr "Meddelande: "]
    if {$msg == ""} {
	print "Inget meddelande skickat.\n"
	return
    }
    safe_eval {
	kom_session send_message $persno $msg
	print "Meddelande s�nt till [kom_conference name $persno].\n"
    } {
	{{KOM FAILED-CALL} {
	    print "Du kunde inte skicka meddelandet.  "
	    print "Mottagare �r kanske oinloggad.\n"
	}}
    }
}

proc send_alert_message_command {} {
    print "Skicka alarmmeddelande till alla inloggade.\n"
    print "Somliga tycker inte om att f� alarmmedelanden.  Tryck RETURN\n"
    print "utan att skriva n�got f�r att avbryta.\n"
    set msg [getstr "Meddelande: "]
    if {$msg == ""} {
	print "Inget meddelande skickat.\n"
	return
    }
    safe_eval {
	kom_session send_message 0 $msg
	print "Alarmmeddelande s�nt till alla.\n"
    } {
	{{KOM FAILED-CALL} {
	    print "M�rkligt.  Du kunde inte skicka meddelandet.\n"
	}}
    }
}

proc receive_message_command {} {
    global last_message_reply_to
    global message_queue

    if {[llength $message_queue] == 0} {
	print "M�rkligt.  Det finns inget meddelande att ta emot.\n"
    } else {
	set msg [lindex $message_queue 0]
	set receiver [lindex $msg 0]
	set sender [lindex $msg 1]
	set message_queue [lrange $message_queue 1 end]
	set send_name [kom_conference name $sender]
	if {$receiver == 0} {
	    print "Alarmmeddelande fr�n $send_name:\n"
	} else {
	    set rcpt_name [kom_conference name $receiver]

	    print "Meddelande fr�n $send_name till $rcpt_name:\n\n"
	}
	print "[lindex $msg 2]\n\n"

	set me [lindex [kom_session who_am_i] 0]
	if {$me != $sender} {
	    if {$receiver == $me || $receiver == 0} {
		set last_message_reply_to $sender
		print "Svar till s�ndaren.\n"
	    } else {
		set last_message_reply_to $receiver
		print "Svar till alla mottagare.\n"
	    }
	}
    }
}

proc reply_to_message_command {} {
    global last_message_reply_to
 
    if {$last_message_reply_to == ""} {
	print "M�rkligt.  Det finns ingen att skicka svaret till.\n"
    } else {
	set rcpt $last_message_reply_to
	set last_message_reply_to ""
	print "Skicka meddelande till [kom_conference name $rcpt].\n"
	set msg [getstr "Meddelande: "]
	if {$msg == ""} {
	    print "Inget meddelande skickat.\n"
	    return
	}
	safe_eval {
	    kom_session send_message $rcpt $msg
	    print "Meddelande s�nt till [kom_conference name $rcpt].\n"
	} {
	    {
		{KOM FAILED-CALL} {
		    print "Du kunde inte skicka meddelandet.  "
		    print "Mottagare �r kanske oinloggad.\n"
		}
	    }
	}
    }
}

proc text_msg_handler {rec send msg} {
    global message_queue
    global prompt_state

    lappend message_queue [list $rec $send $msg]
    if {$prompt_state != ""} {
	print "\n[calculate_prompt]"
    }
}

proc quit_command {} {
    global quit_flag

    set quit_flag 1
    print "\nTack f�r den h�r g�ngen!\n"
}
