# NOTE: This file is not used.  It is preserved here, since it contains
# vital clues on what needs to be done to get tkom to run again.
#
# Makefile for the TCL clients.
#
# Copyright (C) 1994, 1996  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.]

# Process this file with python to generate Makefile.in.

from pmg import *

class tcldir_mixin(bindir_mixin):
    def destination(self):
	return "$(tcldir)"

class tcl_files(tcldir_mixin, tcl_mixin, collection_target):
    def __init__(self, name):
	self.name = "tcl-" + name
	collection_target.__init__(self)

class generated_tcl_file(no_add_dep_mixin, tcldir_mixin, collection_target):
    def __init__(self, name):
	self.name = name
	collection_target.__init__(self)
	collection_target.add_dep(self, generated_tcl_item(name))

    def emit_variables(self):
	pass

    def add_clean_info(self, makefile):
	makefile.mostlyclean(self.name)

m = komxxmakefile()

# We need to include libkom++.a twice, since libkom++ uses
# libconnection which uses libgenclass which uses libkom++.  Many
# linkers cannot resolve such circular references unless you do like
# this.
komlibs = ["../libkom++/libkom++.a",
	   "../libconnection/libconnection.a",
	   "../genclasses/libgenclass.a",
	   "../libkom++/libkom++.a",
	   "../libisc/src/libisc.a"]

m.add_include("../genclasses")
m.add_include("$(top_srcdir)/libconnection")
m.add_include("../libkom++")
m.add_include("$(top_srcdir)/libkom++")
m.add_include("$(top_srcdir)/libisc/src")
m.add_include("$(top_srcdir)/libcompat")
m.add_include("$(top_srcdir)/libtcl/generic")

generic_objs=["tcl-glue.o", "tcl-support.o", "Tcl_LyStr.o"]
nilkom_objs=["tcl-main.o",
	     "paginator.o",
	     "userio.o",
	     "cmd-decode.o",
	     "command_table.o",
	     "command_table_entry.o",
	     "user-input.o",
	     "nilkom-paginator.o"]

t = cxx_target("fixfd")

t.add("fixfd.o")
t.add_lib_nodep("-lg++")
t.add_lib_nodep("$(LIBS)")

m.add_target(t, "all", ["NILKOM"])

t = cxx_target("nilkom")

for o in generic_objs + nilkom_objs:
    t.add(o)

t.add_dep(c_item("insig.o"))
t.add_dep(generated_c_item("nilkominit.o"))
m.add_target(generated_source("nilkominit.c"), "source")
m.add_target(generated_source(".gdbinit"), "all")

for l in komlibs:
    t.add_lib(l)

t.add_lib_nodep("-lg++")
t.add_lib_nodep("$(TCL_LIB)")
t.add_lib_nodep("$(LIBS)")
t.add_lib_nodep("-lm")

m.add_target(t, "all", ["NILKOM"])

t = cxx_target("komiwish")

for o in generic_objs:
    t.add(o)

t.add_dep(c_item("tkom-appinit.o"))

for l in komlibs:
    t.add_lib(l)

t.add_lib_nodep("-lg++")
t.add_lib_nodep("$(TCL_LIB)")
t.add_lib_nodep("$(LIBS)")
t.add_lib_nodep("-ltk")
t.add_lib_nodep("-litcl")
t.add_lib_nodep("-ltcl")
t.add_lib_nodep("$(X_LIBS)")
t.add_lib_nodep("-lX11")
t.add_lib_nodep("-lm")

m.add_target(t, "all", ["TKOM"])

m.add_target(bin_shellscript("tkom"), "all", ["TKOM"])

t = tcl_files("common")
t.add("komlib.tcl")
m.add_target(t)

t = tcl_files("nilkom")

t.add("nilkom.tcl")
t.add("nilkom2.tcl")
t.add("nilkom_edit.tcl")
t.add("nilkom_cmd.tcl")
t.add("nilkom_print.tcl")

m.add_target(t, None, ["NILKOM"])
t = tcl_files("tkom")

t.add("nilkom_print.tcl")
t.add("butwin.tcl")
t.add("lblentry.tcl")
t.add("tkom_commands_conf.tcl")
t.add("tkom_commands_file.tcl")
t.add("tkom_commands_misc.tcl")
t.add("tkom_commands_read.tcl")
t.add("tkom_commands_session.tcl")
t.add("tkom_commands_text.tcl")
t.add("tkom_commands_write.tcl")
t.add("tkom_main_window.tcl")
t.add("tkom_write_window.tcl")
t.add("transient_window.tcl")

m.add_target(t, None, ["TKOM"])

m.add_target(generated_tcl_file("start_nilkom.tcl"), "all", ["NILKOM"])
m.add_target(generated_tcl_file("tclIndex"), "all", ["TKOM"])

m.emit()

print """\
tkom:	$(srcdir)/tkom.tcl Makefile
	echo "#!$(bindir)/komiwish -f" > tkom.tmp
	echo "# Do not edit - automatically generated" >> tkom.tmp
	echo "" >> tkom.tmp
	echo "lappend auto_path $(tcldir)" >> tkom.tmp
	echo "" >> tkom.tmp
	echo "set KOM_SERVER $(DEFAULT_SERVER)" >> tkom.tmp
	echo "set KOM_PORT $(DEFAULT_PORT)" >> tkom.tmp
	echo "set tkom_version $(VERSION)" >> tkom.tmp
	cat $(srcdir)/tkom.tcl >> tkom.tmp
	echo "# Do not edit - automatically generated" >> tkom.tmp
	chmod +x tkom.tmp
	@if $(AWK) "BEGIN { exit(length(\\""$(bindir)\\"") < 19) }" ; then \\
	  echo "-----------------------------------------------" >&2; \\
	  echo "Warning!  bindir=$(bindir) is so long that tkom" >&2; \\
	  echo "may have trouble starting $(bindir)/komiwish" >&2; \\
	  echo "If you cannot run tkom after installing it, please edit" >&2; \\
	  echo "the first line of $(bindir)/tkom, making sure it is at" >&2; \\
	  echo "most 32 characters long.  Then move $(bindir)/komiwish" >&2; \\
	  echo "to location you specified in tkom." >&2; \\
	  echo "" >&2; \\
	  echo "Please note that things may work on your system anyhow." >&2; \\
	  echo "-----------------------------------------------" >&2; \\
	fi
	$(RM) tkom
	mv tkom.tmp tkom

start_nilkom.tcl: Makefile
	echo '# Do not edit - this file was created by' > start_nilkom.tcl~
	echo '# $(srcdir)/Makefile' >> start_nilkom.tcl~
	echo 'set HOST $(DEFAULT_SERVER)' >> start_nilkom.tcl~
	echo 'set PORT $(DEFAULT_PORT)' >> start_nilkom.tcl~
	echo 'set nilkom_version $(VERSION)' >> start_nilkom.tcl~
	echo 'source $$kom_library/nilkom.tcl' >> start_nilkom.tcl~
	echo '# Do not edit - generated by make' >> start_nilkom.tcl~
	mv start_nilkom.tcl~ start_nilkom.tcl

nilkominit.c: stamp-nilkominit

stamp-nilkominit: Makefile
	$(RM) $@    
	echo '/* Do not edit -- automatically generated */' > $@.tmp
	echo 'char *nilkom_initscript =' >> $@.tmp
	echo '"if {[file exists start_nilkom.tcl]} {"' >> $@.tmp
	echo '    "set kom_library $(srcdir);"' >> $@.tmp
	echo '    "source start_nilkom.tcl"' >> $@.tmp
	echo '"} else {"' >> $@.tmp
	echo '    "set kom_library $(tcldir) ;"' >> $@.tmp
	echo '    "source $$kom_library/start_nilkom.tcl"' >> $@.tmp
	echo '"}";' >> $@.tmp
	cmp $@.tmp nilkominit.c || mv $@.tmp nilkominit.c
	touch $@

tclIndex: $(srcdir)/*.tcl
	echo 'source $(srcdir)/mkindex;auto_mkindex $(srcdir) *.tcl' \
		|tclsh > tclIndex

.gdbinit:
	$(RM) $@ $@.tmp
	echo 'dir .' > $@.tmp
	echo 'dir $(srcdir)' >> $@.tmp
	echo 'dir ../genclasses' >> $@.tmp
	echo 'dir $(top_srcdir)/libconnection' >> $@.tmp
	echo 'dir ../libkom++' >> $@.tmp
	echo 'dir $(top_srcdir)/libkom++' >> $@.tmp
	echo 'dir $(top_srcdir)/libisc/src' >> $@.tmp
	echo 'dir $(top_srcdir)/libcompat' >> $@.tmp
	echo 'dir $(top_srcdir)/libtcl/generic' >> $@.tmp
	mv $@.tmp $@

TCL_LIB = ../libtcl/unix/libtcl.a
X_CFLAGS = @X_CFLAGS@
X_LIBS = @X_LIBS@

tcldir = @tcldir@
"""
