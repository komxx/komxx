// Implementation of the TCL commands used in nilkom_print.tcl
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#include <config.h>

#include <sstream>
#include <cassert>
#include <cstdlib>
#include <tcl.h>

#include "tcl-support.h"
#include "connection.h"
#include "userio.h"
#include "paginator.h"
#include "LyStr.h"
#include "enomem.h"
#include "nilkom-paginator.h"
#include "user-input.h"

// FIXME: Use clientData instead of these static variables.
static Nilkom_paginator *pout = NULL;
static User_input *input_chan = NULL;
static Tcl_Interp *tcl_interp = NULL;

enum Kom_page_args {
    PAGE_PRINT=1, PAGE_REGISTER, PAGE_SYNC, PAGE_FLUSH, PAGE_DISCARD,
    PAGE_GETPENDING, PAGE_PAGESIZE, PAGE_TRANSLATE, PAGE_ACKNOWLEDGE,
    PAGE_BEEP, PAGE_REMOVE, PAGE_ACKNOWLEDGE_KEY
    };

static Tcl_Args kom_page_options[] = {
    {"print",           PAGE_PRINT,           1, 0, {TCLARG_STRING}},
    {"register",        PAGE_REGISTER, 2, 0, {TCLARG_STRING, TCLARG_STRING}},
    {"sync",            PAGE_SYNC,            0, 0, {}},
    {"flush",           PAGE_FLUSH,           0, 0, {}},
    {"discard",         PAGE_DISCARD,         0, 0, {}},
    {"get_pending",     PAGE_GETPENDING,      0, 0, {}},
    {"pagesize",        PAGE_PAGESIZE,        2, 0, {TCLARG_INT, TCLARG_INT}},
    {"translate",       PAGE_TRANSLATE,       1, 0, {TCLARG_STRING}},
    {"acknowledge",     PAGE_ACKNOWLEDGE,     0, 0, {}},
    {"acknowledge_key", PAGE_ACKNOWLEDGE_KEY, 0, 0, {}},
    {"beep",            PAGE_BEEP,            0, 0, {}},
    {"remove",          PAGE_REMOVE,          1, 0, {TCLARG_STRING}},
    
    {NULL, 0, 0, 0, {}}
};

// nilkom_print: send data to the screen.
//
// Command: 
//    nilkom_print print STR
// Return values: 
//    ready -- everything has been printed.
//    {pending SIZE} -- size characters is queued (in the first block)
//                 and not printed.
//    {key KEY} -- a registered character has been pressed by the user.
//    terminate -- the user don't want to see any more output from
//                 this command.
// Description:
//    Print STR.  Always flushes the output, so everything is written
//    if it returns 'ready'.  You can use 'nilkom_print print ""' to
//    find out if there is any pending output.
//
// Command:
//    nilkom_print sync
// Return values:
//    Same as "nilkom_print print", but "pending" and "terminate" are
//    currently never returned.
// Description:
//    Tell nilkom_print that the last text of a block was just
//    written.  This command should be issued immediately before a
//    prompt is written.  This command should only be called
//    immediately after a newline has been printed, or ceder will be
//    confused.  This code assumes that a prompt follows the sync
//    marker.
//
//    Currently, "nilkom_print sync" doesn't return until there is no
//    pending output left.  This may change in the future.
//
// Command:
//    nilkom_print flush
// Return values:
//    Same as "nilkom_print print", but "pending" is never returned.
// Description:
//    Write all pending output to the users screen.  Do *more*
//    processing.  Return when no pending output exists or when the
//    user has pressed "q" or a registered key.  This command does not
//    set a new sync point.
//
// Command:
//    nilkom_print acknowledge_key
// Return values:
//    none
// Description:
//    When "nilkom_print print" returns "key FOO" you must use this
//    command to acknowledge the key.  "nilkom_print sync"
//    automatically acknowledges any unacknowledged key.
//
// Command:
//    nilkom_print register KEYS MORE_PROMPT
// Return values:
//    unspecified
// Description:
//    Tell nilkom_print which keys the printing command wants to
//    accept (KEYS should be a string, such as "sS") and what prompt
//    nilkom_print should use if it has to do more prompting.
//    nilkom_print will always handle RET and SPC internally (meaning
//    scroll one line/screenfull).  The keys will be valid for the
//    current block and subsequent blocks (the block currently being
//    written to) only.  By default, nilkom_print handles SPC, RET and
//    q. q can be overridden by giving it in KEYS; SPC and RET
//    cannot.  The default prompt is "somebody please set the prompt>".
//
// Command:
//    nilkom_print discard
// Return values:
//    none
// Description:
//    Discard all output in the first block of output. Show a
//    screenful of output from the next block.  (Normally, the next
//    block will only contain a single line: the next prompt.  In
//    those cases, only one line will appear).
//
// Command:
//    nilkom_print get_pending
// Return values:
//    A string.
// Description:
//    Return the contents of the first block of pending output.  This
//    function does *not* discard the output.
//
// Command:
//    nilkom_print pagesize ROWS COLUMNS
// Return values:
//    {ROWS COLUMNS} -- The size that nilkom_print believes the tty
//    has.
// Description:
//    Set the size of the display.  Giving 0 as ROWS or COLUMNS means
//    infinite.  Giving -1 as ROWS or COLUMNS means "try to determine
//    automatically".  nilkom_print will normally determine the screen
//    size correctly by itself; this command can be used if the user
//    wants to override for some reason.  It always returns the
//    numbers the automatic code has figured out.
//
// Command:
//    nilkom_print translate MODE
// Return values:
//    MODE  -- The previous mode used.
// Description:
//    Set translation mode to MODE.  The only currently supported
//    modes are "transparent" (do no translations) and "swascii" (map
//    swedish characters written in ISO 8859-1 into "}{|" and so on).
//
// Command:
//    nilkom_print acknowledge
// Return values:
//    none
// Description:
//    Acknowledge that the user has seen everything on screen.
//    Typically called each time the user enters a key while entering
//    text, so that no *more* prompt will appear while he is typing.
//
// Command:
//    nilkom_print beep
// Return values:
//    none
// Description:
//    Beep or flash the screen or something similar.
//
// Command:
//    nilkom_print erase CHAR
// Return values:
//    none
// Description:
//    Remove a character.  This code needs to know which character
//    is written so that it can erase it even if it occupies more than
//    one display position.

static int
convert_more_status(Tcl_Interp *interp)
{
  char c;
  switch(pout->more_status(&c))
    {
    case Nilkom_paginator::READY:
      Tcl_SetResult(interp, "ready", TCL_STATIC);
      return TCL_OK;

    case Nilkom_paginator::PENDING:
      sprintf(interp->result, "pending %ld", pout->pending());
      return TCL_OK;

    case Nilkom_paginator::KEY:
      sprintf(interp->result, "key %c", c);
      return TCL_OK;

    case Nilkom_paginator::TERMINATE:
      Tcl_SetResult(interp, "terminate", TCL_STATIC);
      return TCL_OK;
    }
  /*NOTREACHED*/
  abort();
}

extern "C" int
tcl_page(ClientData /*clientData*/,
	 Tcl_Interp *interp, 
	 int argc, 
	 const char *argv[])
{
  Parse_tcl_args args(interp, kom_page_options, argc, argv);
  switch (args.option())
    {
    case PAGE_PRINT:
      *pout << args.stringarg(0);
      return convert_more_status(interp);
	      
    case PAGE_REGISTER:
      pout->register_key(args.stringarg(0));
      pout->set_more_prompt(args.stringarg(1));
      return TCL_OK;

    case PAGE_SYNC:
      pout->sync();
      pout->blocking_status();
      return convert_more_status(interp);

    case PAGE_FLUSH:
      pout->blocking_status();
      return convert_more_status(interp);

    case PAGE_ACKNOWLEDGE_KEY:
      pout->acknowledge_key();
      return TCL_OK;

    case PAGE_DISCARD:
      pout->discard();
      return TCL_OK;
      
    case PAGE_GETPENDING:
      {
	LyStr pend = pout->get_pending();
	const char *p = pend.view_block();
	ENOMEM_P(p);
	Tcl_SetResult(interp, (char*)p, TCL_VOLATILE); // No const in tcl.h.
	pend.view_done(p);
      }
      return TCL_OK;
      
    case PAGE_PAGESIZE:
      pout->set_width(args.intarg(1));
      pout->set_height(args.intarg(0));
      sprintf(interp->result, "%d %d", pout->real_rows(), pout->real_columns());
      return TCL_OK;
      
    case PAGE_TRANSLATE:
      if (!strcmp(args.stringarg(0), "transparent"))
	{
	  pout->translate(Paginator::TRANSPARENT);
	}
      else if (!strcmp(args.stringarg(0), "swascii")) 
	{
	  pout->translate(Paginator::SWASCII);
	}
      else
	{
	  Tcl_SetResult(interp,
"nilkom_print translate: argument must be either 'transparent' or 'swascii'.",
			TCL_STATIC);
	  Tcl_SetErrorCode(interp, "KOM", "BADARGS", (char *)NULL);
	  return TCL_ERROR;
	}
      return TCL_OK;

    case PAGE_ACKNOWLEDGE:
      pout->more_lines(pout->rows());
      return TCL_OK;

    case PAGE_BEEP:
      pout->bell();
      return TCL_OK;

    case PAGE_REMOVE:
      pout->remove(args.stringarg(0));
      return TCL_OK;

    default:
      assert(args.option() < 0);
      return TCL_ERROR;
    };
  /*NOTREACHED*/
}

enum Kom_input_args {
    INPUT_GET_KEY=1, INPUT_PENDING
    };

static Tcl_Args kom_input_options[] = {
    {"get_key", INPUT_GET_KEY, 0, 0, {}},
    {"pending", INPUT_PENDING, 0, 0, {}},

    {NULL, 0, 0, 0, {}}
};




extern "C" int
tcl_input(ClientData /*clientData*/,
	  Tcl_Interp *interp, 
	  int argc, 
	  const char *argv[])
{
  Parse_tcl_args args(interp, kom_input_options, argc, argv);

  switch (args.option())
    {
    case INPUT_GET_KEY:
      {
	int key = input_chan->get_key();
	std::ostringstream res;
	if (key == -1)
	  {
	    res << "Failed to read keyboard input";
	    set_tcl_result(interp, res);
	    return TCL_ERROR;
	  }
	res << key;
	set_tcl_result(interp, res);
      }
      return TCL_OK;

    case INPUT_PENDING:
      {
	std::ostringstream res;
	res << input_chan->pending();
	set_tcl_result(interp, res);
      }
      return TCL_OK;

    default:
      assert(args.option() < 0);
      return TCL_ERROR;
    }
  /*NOTREACHED*/
  abort();
}

enum Kom_idle {
    IDLE_SET=1, IDLE_REMOVE
    };

static Tcl_Args kom_idle_options[] = {
    {"set", IDLE_SET, 1, 0, {TCLARG_STRING}},
    {"remove", IDLE_REMOVE, 0, 0, {}},

    {NULL, 0, 0, 0, {}}
};

static LyStr *idle_script = NULL;

static void
idlecallback()
{
  assert(idle_script != NULL);
  char *s = idle_script->malloc_copy();
  if (s != NULL)		// Silently ignore errors here.
    {
      Tcl_Eval(tcl_interp, s);
      free(s);
    }
}

extern "C" int
tcl_idle(ClientData /*clientData*/,
	 Tcl_Interp *interp, 
	 int argc, 
	 const char *argv[])
{
  Parse_tcl_args args(interp, kom_idle_options, argc, argv);

  switch (args.option())
    {
    case IDLE_SET:
      if (idle_script != NULL)
	delete idle_script;

      idle_script = new LyStr(args.stringarg(0));

      input_chan->idle_callback(&idlecallback);
      return TCL_OK;
    case IDLE_REMOVE:
      input_chan->idle_callback(NULL);
      if (idle_script != NULL) 
	{
	  delete idle_script;
	  idle_script = NULL;
	}
      return TCL_OK;
    default:
      assert(args.option() < 0);
      return TCL_ERROR;
    }
  /*NOTREACHED*/
  abort();
}

void define_userio_commands(Tcl_Interp *interp,
			    User_input *inchan,
			    Nilkom_paginator *outchan)
{
  // FIXME make it possible to have more than one active interp.
  input_chan = inchan;
  pout = outchan;
  tcl_interp = interp;

  Tcl_CreateCommand(interp, "nilkom_page",  tcl_page,  NULL, NULL);
  Tcl_CreateCommand(interp, "nilkom_input", tcl_input, NULL, NULL);
  Tcl_CreateCommand(interp, "nilkom_idleproc", tcl_idle, NULL, NULL);
  // FIXME add a deletion callback and free resources.
}
