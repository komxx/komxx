// String class with knowledge on TCL strings.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#ifndef KOMXX_TCL_LYSTR_H
#define KOMXX_TCL_LYSTR_H

#include "LyStr.h"

class Tcl_LyStr : public LyStr {
  public:
    Tcl_LyStr();
    Tcl_LyStr(const char *c_str);
    Tcl_LyStr(void *block, int len);
    Tcl_LyStr(const Tcl_LyStr &);
    Tcl_LyStr(const LyStr &);

    // Append as list element, quoting as necessary.
    void append_element(const LyStr &str);
};
  
#endif
