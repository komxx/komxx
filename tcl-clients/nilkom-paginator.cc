// nilkom_paginator.h - Medium-level *more* prompt handling
// 
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cassert>

#include "nilkom-paginator.h"
#include "LyStr.h"

Nilkom_paginator::Nilkom_paginator(User_input *input,
				   std::ostream &screen)
   : Paginator(screen), registered_keys(), unacknowledged_key(-1),
     input_channel(input)
{
    assert(input != NULL);
}

Nilkom_paginator::~Nilkom_paginator()
{
}

void 
Nilkom_paginator::register_key(const LyStr &keys)
{
    registered_keys = keys;
}

Nilkom_paginator &
Nilkom_paginator::operator << (const LyStr &output)
{
    Paginator::operator <<(output);
    if (more_status() == PENDING)
	maybe_scroll();
    return *this;
}

Nilkom_paginator::Page_status 
Nilkom_paginator::more_status(char *pressed_key) const
{
    if (unacknowledged_key != -1)
    {
	if (pressed_key != NULL)
	    *pressed_key = unacknowledged_key;
	return KEY;
    }
    else if (discarding())
    {
	return TERMINATE;
    }
    else if (pending() > 0)
    {
	return PENDING;
    }
    else
    {
	return READY;
    }
}


void 
Nilkom_paginator::acknowledge_key()
{
    unacknowledged_key = -1;
}

Nilkom_paginator::Page_status 
Nilkom_paginator::blocking_status(char *pressed_key)
{
    Page_status res;

    while ((res = more_status(pressed_key)) == PENDING)
    {
	// To avoid a busy wait for the keypress of the user, we
	// have to have to block somewhere about here.  Calling
	// input_channel->pending() with a one-second timeout seems to
	// be the easiest way to do it.
	input_channel->pending(1000);
	maybe_scroll();
    }
    
    
    return res;
}

void
Nilkom_paginator::maybe_scroll()
{
    while (input_channel->pending() > 0)
    {
	int c = input_channel->get_key();
	if (c == -1)		// FIXME Error handling
	    return;
	if (c == ' ')
	    more_lines(rows() - 2); // FIXME: Hardcoded context
	else if (c == '\r' || c == '\n')
	    more_lines(1);
	else if (registered_keys.present(c))
	{
	    unacknowledged_key = (unsigned char)c;
	    return;
	} 
	else if (c == 'q')
	{
	    discard();
	    return;
	}
	else
	    bell();
    }
}

    
	

