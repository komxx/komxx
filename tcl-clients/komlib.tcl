# A small library of support functions for the TCL clients.
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


proc what {} {
	global errorInfo
	return [format "%s" $errorInfo]
}

proc safe_eval {script handled} {
    global errorCode errorInfo
    case [catch {uplevel 1 $script} res] in {
	0 {return $res}
	1 {
	    foreach hnd $handled {
		set anticipated [lindex $hnd 0]
		if {[llength $errorCode] >= [llength $anticipated]
		    && ([lrange $errorCode 0 [expr [llength $anticipated]-1]] \
		        == $anticipated)}\
		{
		    return [uplevel 1 [lindex $hnd 1]]
		}
	    }
	    error $res "$errorInfo\n    (unhandled error in safe_eval)" \
		$errorCode
	  }
	* {error $res "    (non-common exit code in safe_eval)"}
    }
}

# LysKOM data types

# Misc_info degenerates

proc recipient {what recip} {
	case $what in {
		{rec_no}	 {return [lindex $recip 0]}
		{is_cc}		 {return [lindex $recip 1]}
		{loc_no}	 {return [lindex $recip 2]}
		{rec_time_valid} {return [lindex $recip 3]}
		{rec_time}	 {return [lindex $recip 4]}
		{sender}	 {return [lindex $recip 5]}
		{sent_at_valid}	 {return [lindex $recip 6]}
		{sent_at}	 {return [lindex $recip 7]}
	}
}

proc uplink {what ulnk} {
	case $what in {
		{is_footnote}	{return [lindex $ulnk 0]}
		{parent}	{return [lindex $ulnk 1]}
		{sender}	{return [lindex $ulnk 2]}
		{sent_later }	{return [lindex $ulnk 3]}
		{sent_at}	{return [lindex $ulnk 4]}
		{*}		{error {uplink what}}
	}
}


# ================================================================
#                                 Micro-conf
#

proc microconf {option mc} {
    case $option in {
        confno       {return [lindex $mc 0]}
	is-protected {return [string index [lindex $mc 1] 0]}
	is-original  {return [string index [lindex $mc 1] 1]}
	is-secret    {return [string index [lindex $mc 1] 2]}
	is-letterbox {return [string index [lindex $mc 1] 3]}
    }
    error [format "bad option \"$option\" to microconf: %s%s" \
	       "should be confno, is-protected, is-original, " \
	       "is-secret, or is-letterbox"]
}

proc filter-persons {microconf_list} {
    set result {}
    foreach mc $microconf_list {
	if {[microconf is-letterbox $mc]} {
	    lappend result $mc
	}
    }
    return $result
}

proc filter-confs {microconf_list} {
    set result {}
    foreach mc $microconf_list {
	if {![microconf is-letterbox $mc]} {
	    lappend result $mc
	}
    }
    return $result
}

# ================================================================
#                             Printing a text
#


set delim_pre "------------------------------------------------------------"
set delim_post "------------------------------"
set senaste 0

proc print_text {text_no} {
    global delim_pre delim_post senaste

    print [format_header $text_no]

    print "�rende: "
    print "[kom_text subject $text_no]\n"
    print "$delim_pre\n"

    print "[kom_text body $text_no]\n"

    print [format "(%d)%s\n" $text_no $delim_post]
    print [format_footer $text_no]

    set senaste $text_no
}

# Printing a text: text before the $delim_pre

proc format_header {text_no} {
    set hline [format "(%d) %s /%d rader/ %s\n" \
			$text_no \
			[format_date [kom_text ctime $text_no]] \
			[kom_text num_lines $text_no] \
			[safe_eval {kom_conference name \
					 [kom_text author $text_no]} {
				{{KOM BADCONF} {format "Person %d (utpl�nad)" \
						[kom_text author $text_no]}}}]]
    for {set i 0} {$i<[kom_text num_recipients $text_no]} {incr i} {
	append hline [format_recipient [kom_text recipient $text_no $i]]
    }
    for {set i 0} {$i<[kom_text num_uplinks $text_no]} {incr i} {
        append hline [format_uplink [kom_text uplink $text_no $i]]
    }
    if [kom_text num_marks $text_no] {
	append hline [format "Markerad av %d personer\n" \
		[kom_text num_marks $text_no]]
    }
    return $hline
}

proc format_recipient {recip} {
	set result [format "%s: %s <%d>\n" \
		[expr {[recipient is_cc $recip] \
			? "Extra kopiemottagare" : "Mottagare"}] \
		[kom_conference name [recipient rec_no $recip]] \
		[recipient loc_no $recip]]
	if [recipient rec_time_valid $recip] {
	    append result [format "  Mottaget %s\n" \
		[format_date [recipient rec_time $recip]]]
	}
	# FIXME: fix line breaks and use some form of safe_name instead.
	if [recipient sender $recip] {
	    append result [safe_eval \
			       {format "  S�nt av %s\n" \
				    [kom_conference name [recipient sender $recip]]} {
					{{KOM BADCONF} {format "  Person %d (utpl�nad) s�nde\n" [recipient sender $recip]}}
				    }]
	}
	if [recipient sent_at_valid $recip] {
	    append result [format "  S�nt %s\n" \
		 [format_date [recipient sent_at $recip]]]
	}
	return $result
}

proc format_uplink {ulnk} {
    set line [format "%s till text %d %s\n" \
	[expr {[uplink is_footnote $ulnk] ? "Fotnot" : "Kommentar"}] \
	[uplink parent $ulnk] \
	[safe_eval \
	    {format "av %s" \
	       [kom_conference name [kom_text author [uplink parent $ulnk]]]} {
	    {{KOM BADCONF} {format " Person %d (utpl�nad)" \
				[kom_text author [uplink parent $ulnk]]}}
	    {{KOM BADTEXT} {format " (ej l�sbar av dig)"}}}]]

    if {[uplink sender $ulnk]} {
	append line [format " S�nt av %s\n" \
	    [kom_conference name [uplink sender $ulnk]]]
    }
    if {[uplink sent_later $ulnk]} {
	append line [format " S�nt %s\n" \
	    [format_date [uplink sent_at $ulnk]]]
    }
    return $line
}

# Printing a text: text after the $delim_post

proc format_footer {text_stat} {
    set fline {}
    for {set i 0} {$i<[kom_text num_footnotes $text_stat]} {incr i} {
	append fline [format_comment_in [kom_text footnote $text_stat $i] \
					 "Fotnot"]
    }
    for {set i 0} {$i<[kom_text num_comments $text_stat]} {incr i} {
	append fline [format_comment_in [kom_text comment $text_stat $i] \
					 "Kommentar"]
    }
    return $fline
}


proc format_comment_in {textno gurka} {
    return [format "%s i text %d %s\n" $gurka $textno \
	[safe_eval \
	    {format "av %s" [kom_conference name [kom_text author $textno]]} {
	    {{KOM BADCONF} {format {(Ok�nd f�rfattare)}}}
	    {{KOM BADTEXT} {format {(Ej l�sbar av dig)}}}}]]
}

# Formatting a date/time

proc format_date time {
	eval format_date_internal $time
}

proc format_date_internal {sec min hour mday mon year wday yday isdst} {
	format "%d-%02d-%02d %02d:%02d:%02d" [expr 1900+$year] \
		[expr $mon+1] $mday $hour $min $sec
}

#
# Commands
#

proc se {tiden} {
	if {$tiden == "tiden"} {
		print [format "%s (enligt servern)\n" \
			 [format_date [kom_session get_time]]]
	} { error [format "Kan inte se %s" $tiden] }
}

proc }terse {vad} {
	global senaste

	if {$vad == "senaste"} {
		print_text $senaste
	} else {
		print_text $vad
	}
	return ""
}

proc status {what whom} {
	case $what in {
		{person} {status_person $whom}
		{*} {error {status person nr}}
	}
}

proc status_person {p} {
	print [format "Nummer: %d\t\t\tNamn: %s\n" $p [kom_conference name $p]]
	print [format "Inloggad: %s\n" [person username $p]]
	print [format "Senast inloggning: %s\n"	\
		[format_date [person last_login $p]]]
	print [format "N�rvarotid: %.3f dygn\n"	\
		[expr [person total_time_present $p].0/3600/24]]
}

proc tid {nr} {
	print [format "%4d %8.3f %s\n" $nr	\
		[expr [person total_time_present $nr].0/3600/24] \
		[kom_conference name $nr]]
}

proc evaluate {nr} {
	print [format "%s %4d %5d %3d %3d %8.3f %s\n" \
		[format_date [person last_login $nr]] \
		$nr \
		[person no_of_created_texts $nr] \
		[person created_confs $nr] \
		[person created_persons $nr] \
		[expr [person total_time_present $nr].0/3600/24] \
		[kom_conference name $nr]]
}
