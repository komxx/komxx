# A simple kom client for www.
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

set SPIDER ""
set HOST kom.lysator.liu.se
set PORT 4894
set nilkom_version 0.3-www

source $kom_library/komlib.tcl

#set komfile [open "/usr/www/LysKOM/komrc"]
#set komfile [open "~/.komrc"]
#gets $komfile persno
#gets $komfile passwd

set persno 90
set passwd gazonk

proc htmlize {str} {
    regsub -all "&" $str "&amp;" str
    regsub -all "<" $str "&lt;" str
    regsub -all ">" $str "&gt;" str
    return $str
}

proc text_anchor_start {textno} {
    return "<a href=\"nilkom?text+$textno\">"
}
proc conf_anchor {persno} {
    return "<a href=\"nilkom?conf+$persno\">[htmlize [kom_conference name $persno]]</a>"
}

proc format_header {text_no} {
    set hline [format "(%d) %s /%d rader/ %s\n" \
			$text_no \
			[format_date [kom_text ctime $text_no]] \
			[kom_text num_lines $text_no] \
			[safe_eval {conf_anchor [kom_text author $text_no]} {
				{{KOM BADCONF} {format "Person %d (utpl�nad)" \
						[kom_text author $text_no]}}}]]
    for {set i 0} {$i<[kom_text num_recipients $text_no]} {incr i} {
	append hline [format_recipient [kom_text recipient $text_no $i]]
    }
    for {set i 0} {$i<[kom_text num_uplinks $text_no]} {incr i} {
        append hline [format_uplink [kom_text uplink $text_no $i]]
    }
    if [kom_text num_marks $text_no] {
	append hline [format "Markerad av %d personer\n" \
		[kom_text num_marks $text_no]]
    }
    return $hline
}

proc format_recipient {recip} {
	set result [format "%s: %s &lt;%d&gt;\n" \
		[expr {[recipient is_cc $recip] \
			? "Extra kopiemottagare" : "Mottagare"}] \
		[conf_anchor [recipient rec_no $recip]] \
		[recipient loc_no $recip]]
	if [recipient rec_time_valid $recip] {
	    append result [format "  Mottaget %s\n" \
		[format_date [recipient rec_time $recip]]]
	}
	if [recipient sender $recip] {
	    append result [format "  S�nt av %s\n" \
			       [conf_anchor [recipient sender $recip]]]
	}
	if [recipient sent_at_valid $recip] {
	    append result [format "  S�nt %s\n" \
		 [format_date [recipient sent_at $recip]]]
	}
	return $result
}

proc format_uplink {ulnk} {
    set line [text_anchor_start [uplink parent $ulnk]]
    append line [format "%s till text %d av %s\n" \
		     [expr {[uplink is_footnote $ulnk] ? "Fotnot" : "Kommentar"}] \
		     [uplink parent $ulnk] \
		     [conf_anchor [kom_text author [uplink parent $ulnk]]]]
    append line </a>
    if {[uplink sender $ulnk]} {
	append line [format " S�nt av %s\n" \
			 [conf_anchor [uplink sender $ulnk]]]
    }
    if {[uplink sent_later $ulnk]} {
	append line [format " S�nt %s\n" \
			 [format_date [uplink sent_at $ulnk]]]
    }
    return $line
}

proc format_comment_in {textno gurka} {
    format "%s%s i text %d</a> av %s\n" [text_anchor_start $textno] \
	$gurka $textno \
	[conf_anchor [kom_text author $textno]]
}

proc print_text {text_no} {
    global delim_pre delim_post senaste

    puts <pre>
    puts -nonewline [format_header $text_no]

    puts -nonewline "�rende: "
    puts [htmlize [kom_text subject $text_no]]
    puts $delim_pre
    
    puts [htmlize [kom_text body $text_no]]

    puts [format "(%d)%s" $text_no $delim_post]
    puts -nonewline [format_footer $text_no]
    puts </pre>
    
    set senaste $text_no
}

connect $HOST $PORT wwwtest 0.0
kom_session login $persno $passwd
set passwd ""

proc timeout {} {}

proc getstr {} {
    nilkom_page sync
    set res ""
    while {[set char [nilkom_input get_key]] != 13 && $char != 10} {
	append res [format "%c" $char]
    }
    return $res
}

set type [getstr]
set nr [getstr]

puts "Content-type: text/html"
puts ""
flush stdout
switch $type {
    text { 
	puts "<head>"
	puts "<title>"
	puts "Text $nr i LysKOM"
	puts "</title>"
	puts "<link rev=\"made\" href=\"mailto:ceder@lysator.liu.se\">"
	puts "</head>"
	puts "<body>"
	print_text $nr
	puts "<p><a href=\"http://www.lysator.liu.se:7500/\">Lysator</a> home."
	puts "</body>"
    }
    conf {
	puts "<head>"
	puts "<title>"
	puts "Presentation f�r [htmlize [kom_conference name $nr]] i LysKOM"
	puts "</title>"
	puts "<link rev=\"made\" href=\"mailto:ceder@lysator.liu.se\">"
	puts "</head>"
	puts "<body>"
	print_text [kom_conference presentation $nr]
	puts "<p><a href=\"http://www.lysator.liu.se:7500/\">Lysator</a> home."
	puts "</body>"
    }
    default { errmsg }
}
