#include <tk.h>
#include <itcl.h>


extern Tk_Window  Tk_MainWindow(Tcl_Interp *);
extern int        Tk_Init(Tcl_Interp *);
extern int        Tcl_Init(Tcl_Interp *);
extern void       kom_define_commands(Tcl_Interp *);

extern char *  tcl_RcFileName;

/*
 * This is a standard Tcl_AppInit procedure for tcl version 7.
 */

int
Tcl_AppInit(interp)
    Tcl_Interp *interp;
{
    Tk_Window main;

    main = Tk_MainWindow(interp);

    /*
     * Call the init procedures for included packages.  Each call should
     * look like this:
     *
     * if (Mod_Init(interp) == TCL_ERROR) {
     *     return TCL_ERROR;
     * }
     *
     * where "Mod" is the name of the module.
     */

    /*
     * Standard initializations (tcl & tk). 
     */
    if (Tcl_Init(interp) == TCL_ERROR) {
	return TCL_ERROR;
    }
    if (Tk_Init(interp) == TCL_ERROR) {
	return TCL_ERROR;
    }

    /*
     *  Add [incr Tcl] facilities...
     */
    if (Itcl_Init(interp) == TCL_ERROR) {
	return TCL_ERROR;
    }

    /*
     * tclkom commands.
     */
    kom_define_commands(interp);

    /*
     * Specify a user-specific startup file to invoke if the application
     * is run interactively.  Typically the startup file is "~/.apprc"
     * where "app" is the name of the application.  If this line is deleted
     * then no user-specific startup file will be run under any conditions.
     */

    tcl_RcFileName = "~/.tkomrc";
    return TCL_OK;
}
