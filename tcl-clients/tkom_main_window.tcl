# Code for handling the tkom main window
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# ================================================================
#                         Misc. small function.


proc beep {} {
    puts -nonewline "\a" 
    flush stdout
}


# ================================================================

proc main_menu {menuwin} {
    # File menu
    menubutton $menuwin.file -text "Arkiv   " -underline 0 \
	-menu $menuwin.file.m
    menu $menuwin.file.m
    $menuwin.file.m add command -label "Om tkom..." -underline 0 \
	-command {show_about_command}
    $menuwin.file.m add separator
    $menuwin.file.m add command -label "Sluta..." \
	-underline 0 -accelerator C-xC-c \
	-command {tkom_quit_command}
    pack $menuwin.file -side left

    # Reading menu
    menubutton $menuwin.reading -text "L�sning" -underline 0 \
	-menu $menuwin.reading.m
    menu $menuwin.reading.m
    $menuwin.reading.m add command -label "Lista nyheter" \
	-underline 0 -accelerator ln \
	-command {show_unread_command}
    $menuwin.reading.m add command -label "n�sta Text" \
	-underline 6 -accelerator nt -command {next_text_command}
    $menuwin.reading.m add command -label "Hoppa �ver alla kommentarer   " \
	-underline 0 -command {not_yet_implemented {Hoppa}} -state disabled
    $menuwin.reading.m add command -label "N�sta m�te" \
	-underline 0 -accelerator nm -command {next_conf_command}
    $menuwin.reading.m add command -label "G� till m�te..." \
	-underline 0 -accelerator g \
	-command {goto_conf_command}
    $menuwin.reading.m add command -label "Endast l�sa senaste" \
	-underline 0 -command {not_yet_implemented {Endast l�sa senaste}} \
	-state disabled
    $menuwin.reading.m add cascade -label "�terse" -underline 0 \
	-menu $menuwin.review_m -accelerator =>
    pack $menuwin.reading -side left

    # Writing menu
    menubutton $menuwin.writing -text "Skrivning" -underline 0 \
	-menu $menuwin.writing.m
    menu $menuwin.writing.m
    $menuwin.writing.m add command -label "Kommentera inl�gget    " \
	-underline 0 -accelerator k \
	-command {comment_text_command $current_text}
    $menuwin.writing.m add command -label "Fotnot till inl�gget" \
	-underline 0 -command {not_yet_implemented {Fotnot}} \
	-state disabled
    $menuwin.writing.m add command -label "Personligt svar" \
	-underline 0 -command {not_yet_implemented {Personligt svar}} \
	-state disabled
    $menuwin.writing.m add command -label "skriva Inl�gg" \
	-underline 7  -accelerator i \
 	-command {write_text_command}
    $menuwin.writing.m add command -label "skriva Brev" -underline 7 \
	-command {not_yet_implemented {Skriva brev}} -state disabled
    $menuwin.writing.m add command -label "Radera inl�gg" -underline 0 \
	-command {not_yet_implemented {Radera inl�gg}} -state disabled
    $menuwin.writing.m add command -label "Status inl�gg" -underline 0 \
	-command {not_yet_implemented {Status inl�gg}} -state disabled
    pack $menuwin.writing -side left

    # Review menu (submenu to the text menu)
    menu $menuwin.review_m
    $menuwin.review_m add command -label "Inl�gg" -underline 0 \
	-command {not_yet_implemented {�terse inl�gg}} \
	-state disabled
    $menuwin.review_m add command -label "det Kommenterade" -underline 4 \
	-command {not_yet_implemented {�terse det kommenterade}} \
	-state disabled
    $menuwin.review_m add command -label "kommentarsTr�det" -underline 10 \
	-command {not_yet_implemented {�terse Kommentarstr�det}} \
	-state disabled
    $menuwin.review_m add command -label "Alla kommentarer" -underline 0 \
	-command {not_yet_implemented {�terse alla kommentarer}} \
	-state disabled
    $menuwin.review_m add command -label "Markerade" -underline 5 \
	-command {not_yet_implemented {�terse markerade}} \
	-state disabled
    $menuwin.review_m add command -label "Presentation" -underline 0 \
	-command {not_yet_implemented {�terse presentation}} \
	-state disabled

    # Text menu
    menubutton $menuwin.text -text "Inl�gg  " -underline 0 \
	-menu $menuwin.text.m
    menu $menuwin.text.m
    $menuwin.text.m add command -label "Addera mottagare" -underline 0 \
	-command {not_yet_implemented {Addera mottagare}} -state disabled 
    $menuwin.text.m add command -label "Addera extra kopiemottagare" \
	-underline 0 -state disabled \
	-command {not_yet_implemented {Addera extra kopiemottagare}}
    $menuwin.text.m add command -label "Subtrahera mottagare" -underline 0 \
	-command {not_yet_implemented {Subtrahera mottagare}} -state disabled 
    $menuwin.text.m add command -label "Addera kommentar" -underline 0 \
	-command {not_yet_implemented {Addera kommentar}} -state disabled 
    $menuwin.text.m add command -label "Subtrahera kommentar" -underline 0 \
	-command {not_yet_implemented {Subtrahera kommentar}} -state disabled 
    $menuwin.text.m add command -label "Markera inl�gg" \
	-underline 0 -accelerator M \
        -command {mark_command $current_text 255}
    $menuwin.text.m add command -label "Avmarkera inl�gg" \
	-underline 0 -accelerator A \
        -command {unmark_command $current_text}
    pack $menuwin.text -side left

    # Person menu
    menubutton $menuwin.person -text "Personer" -underline 0 \
	-menu $menuwin.person.m
    menu $menuwin.person.m
    $menuwin.person.m add command -label "Lista personer" -underline 0 \
	-command {not_yet_implemented {Lista personer}} -state disabled
    $menuwin.person.m add command -label "Status person" -underline 0 \
	-command {not_yet_implemented {Status person}} -state disabled
    $menuwin.person.m add command -label "sKapa person" -underline 1 \
	-command {not_yet_implemented {Skapa person}} -state disabled
    $menuwin.person.m add command -label "Utpl�na person" -underline 0 \
	-command {not_yet_implemented {Utpl�na person}} -state disabled
    $menuwin.person.m add command -label "�ndra l�senord" -underline 0 \
	-command {not_yet_implemented {�ndra l�senord}} \
	-state disabled
    pack $menuwin.person -side left

    # Conference menu
    menubutton $menuwin.conf -text "M�ten   " -underline 0 \
	-menu $menuwin.conf.m
    menu $menuwin.conf.m
    $menuwin.conf.m add command -label "Lista m�ten" -underline 0 \
	-command {not_yet_implemented {Lista m�ten}} -state disabled
    $menuwin.conf.m add command -label "Bli medlem i m�te" \
	-command {not_yet_implemented {Bli medlem i m�te}} -state disabled
    $menuwin.conf.m add command -label "Uttr�da ur m�te" \
	-command {not_yet_implemented {Uttr�da ur m�te}} -state disabled
    $menuwin.conf.m add command -label "lista Medlemsskap" -underline 6 \
	-command {not_yet_implemented {Lista medlemsskap}} -state disabled
    $menuwin.conf.m add command -label "Status m�te" -underline 0 \
	-command {not_yet_implemented {Status m�te}} -state disabled
    $menuwin.conf.m add command -label "sKapa m�te" -underline 1 \
	-command {not_yet_implemented {Skapa m�te}} -state disabled
    $menuwin.conf.m add command -label "Utpl�na m�te" -underline 0 \
	-command {not_yet_implemented {Utpl�na m�te}} -state disabled
    $menuwin.conf.m add command -label "Addera medlem" -underline 0 \
	-command {not_yet_implemented {Addera medlem}} -state disabled 
    $menuwin.conf.m add command -label "Uteslut medlem" -underline 0 \
	-command {not_yet_implemented {Uteslut medlem}} -state disabled 
    $menuwin.conf.m add command -label "�ndra namn" -underline 0 \
	-command {not_yet_implemented {�ndra namn}} \
	-state disabled
    $menuwin.conf.m add command -label "�ndra organisat�r" -underline 0 \
	-command {not_yet_implemented {�ndra organisat�r}} \
	-state disabled
    $menuwin.conf.m add command -label "�ndra presentation" -underline 0 \
	-command {not_yet_implemented {�ndra presentation}} \
	-state disabled
    $menuwin.conf.m add command -label "�ndra livsl�ngd" -underline 0 \
	-command {not_yet_implemented {�ndra livsl�ngd}} \
	-state disabled
    $menuwin.conf.m add command -label "�ndra superm�te" -underline 0 \
	-command {not_yet_implemented {�ndra superm�te}} \
	-state disabled
    $menuwin.conf.m add command -label "�ndra till�tna f�rfattare" \
	-underline 0 \
	-command {not_yet_implemented {�ndra till�tna f�rfattare}} \
	-state disabled
    $menuwin.conf.m add command -label "Prioritera m�te" -underline 0 \
	-command {not_yet_implemented {Prioritera m�te}} -state disabled
    pack $menuwin.conf -side left

    # Session menu
    menubutton $menuwin.session -text "sEssion " -underline 1 \
	-menu $menuwin.session.m
    menu $menuwin.session.m
    $menuwin.session.m add command -label "B�rja med ny person..." \
	-underline 0 -command {login_command}
    $menuwin.session.m add command -label "�verg� till adminstrat�rsmod" \
	-underline 0 -command {not_yet_implemented {adm.mode}} -state disabled
    pack $menuwin.session -side left

    # Admin menu
#    menubutton $menuwin.admin -text "Administration" -underline 0 \
#	-menu $menuwin.admin.m
#    menu $menuwin.admin.m
#    $menuwin.admin.m add command -label "" \
#	-underline 0 -command {not_yet_implemented {}}
#    pack $menuwin.admin -side left

    # Misc menu
    menubutton $menuwin.misc -text "Diverse " -underline 0 \
	-menu $menuwin.misc.m
    menu $menuwin.misc.m
    $menuwin.misc.m add command -label "Vilka �r inloggade   " \
	-underline 0 -accelerator v \
	-command {show_logged_in_command}
    $menuwin.misc.m add command -label "Se tiden" \
	-underline 0 -accelerator t \
	-command {display_time_command}
    $menuwin.misc.m add command -label "F� uppmuntran" \
	-underline 0 -accelerator fu \
	-command {give_support_command}
    pack $menuwin.misc -side left

    # Window menu
    menubutton $menuwin.win -text "F�nster " -underline 0 \
	-menu $menuwin.win.m
    menu $menuwin.win.m
    $menuwin.win.m add command -label "Inloggade" -underline 0 \
	-command {not_yet_implemented {Inloggadef�nstret}} -state disabled
    $menuwin.win.m add command -label "Ol�sta inl�gg" -underline 0 \
	-command {not_yet_implemented {Ol�staf�nstret}} -state disabled
    pack $menuwin.win -side left

    tk_menuBar $menuwin $menuwin.file $menuwin.reading  $menuwin.writing \
	$menuwin.text $menuwin.person $menuwin.conf $menuwin.session \
	$menuwin.misc $menuwin.win
}


proc elisp-client-bind {w} {
    bind $w <space>                {default_command}
    bind $w <Control-x><Control-c> {tkom_quit_command}
    bind $w <l><n>                 {show_unread_command}
    bind $w <g>                    {goto_conf_command}
    bind $w <n><m>                 {next_conf_command}
    bind $w <n><t>                 {next_text_command}
    bind $w <i>                    {write_text_command}
    bind $w <k>                    {comment_text_command $current_text}
    bind $w <f><u>                 {give_support_command}
    bind $w <t>                    {display_time_command}
    bind $w <v>                    {show_logged_in_command}
    bind $w <KeyPress-M>	   {mark_command $current_text 255}
    bind $w <KeyPress-A>	   {unmark_command $current_text}
}

proc text_text {textwin} {
    global boldfont

    .text tag configure bold -font $boldfont
}

proc main_window {} {
    global romanfont
    global boldfont

    # The menu bar
    frame .menu -relief raised -bd 2
    pack .menu -side top -fill x
    main_menu .menu

    # The status frame
    frame .statf -relief raised -bd 2
    pack .statf -side top -fill x
    label .statf.cur_conf0 -font $boldfont -text "N�rvarande i: "
    label .statf.cur_conf -font $romanfont -text ""
    pack .statf.cur_conf0 .statf.cur_conf -side left

    # The default action frame
    frame .defaultf -relief raised -bd 2
    pack .defaultf -side top -fill x
    label .defaultf.default0 -font $boldfont -text "Kommando:     "
    button .defaultf.default -font $romanfont -text ""  -bd 2 -relief raised\
	-command {default_command} -anchor w
    pack .defaultf.default0  -side left 
    pack .defaultf.default -side left -fill x -expand TRUE

    # The message frame, text and scrollbar
    frame .mesgf -relief raised
    pack .mesgf -side bottom -fill x
    scrollbar .mesgf.messagescroll -relief flat -command ".mesgf.message yview"
    pack .mesgf.messagescroll -side right -fill y
    text .mesgf.message -relief raised -bd 2 \
	-height 15 -state disabled \
	-yscrollcommand ".mesgf.messagescroll set" -setgrid true
    pack .mesgf.message -side left -expand TRUE -fill x

    # The text window
    text .text -relief raised -bd 2 -yscrollcommand ".textscroll set" \
	-setgrid true -state disabled
    scrollbar .textscroll -relief flat -command ".text yview"
    pack .text -side left -expand TRUE -fill y
    pack .textscroll -side right -fill y
    text_text .text

    elisp-client-bind .
    wm title . "tkom"
    wm iconname . "tkom"
}


# ================================================================


proc next_text {{default 0}} {
    if {[kom_unread current_conf] == 0} { 
        if {$default} {
            kom_unread goto_next_conf
        } else {
            show_message "Du �r inte n�rvarande i n�got m�te.\n"
        }
    } else {
        read_text .text [kom_unread next_text remove]
    }
}

proc next_text_command {{default 0}} {
    if {![start_of_command next_text_command]} {
	return
    }
    next_text $default
    end_of_command
}



proc next_conf {{default 0}} {
    kom_unread goto_next_conf
}

proc next_conf_command {{default 0}} {
    if {![start_of_command next_conf_command]} {
	return
    }

    next_conf
    end_of_command
}


# ================================================================


set tkom_prompt ""

proc show_default_command {} {
    global tkom_prompt
    global current_conf

    if {$current_conf < 0} {
	.defaultf.default configure -text ""
	return
    }

    set tkom_prompt [kom_unread prompt]
    case $tkom_prompt in {
      NEXT_CONF     {.defaultf.default configure -text "G� till n�sta m�te"}
      NEXT_TEXT     {if {[kom_unread current_conf] == 0} { \
                         .defaultf.default configure -text "G� till n�sta m�te"
                     } else {
                         .defaultf.default configure -text "L�sa n�sta text"
		     } }
      NEXT_COMMENT  {.defaultf.default configure -text "L�sa n�sta kommentar"}
      NEXT_FOOTNOTE {.defaultf.default configure -text "L�sa n�sta fotnot"}
      NO_MORE_TEXT  {.defaultf.default configure -text "Se tiden"}
    }
}

proc default_command {} {
    global tkom_prompt
    global to_mark_as_read
    global current_conf

    if {$current_conf < 0} {
	.defaultf.default configure -text ""
	return
    }

    if {![start_of_command default_command]} {
	return
    }

    # Mark all texts as read that are on the to-mark-as-read list.
    foreach tno $to_mark_as_read {
	kom_text mark_as_read $tno
    }
    set to_mark_as_read {}

    case $tkom_prompt in {
      NEXT_CONF     {next_conf 1}
      NEXT_TEXT     {next_text 1}
      NEXT_COMMENT  {read_text .text [kom_unread next_text remove]}
      NEXT_FOOTNOTE {read_text .text [kom_unread next_text remove]}
      NO_MORE_TEXT  {display_time}
    }

    end_of_command
}


#
# Do necessary things before we wait for the user to do something.
#
proc do_things_before_waiting {} {
    show_current_conf [kom_unread current_conf]
    show_default_command
}


#
# This procedure must be called first in every command procedure in tkom.
# Return value is 1 if it is ok to continue and 0 otherwise.
#
set command_is_running ""

proc start_of_command {{command unknown}} {
    global tkom_priv
    global command_is_running

    if {$tkom_priv(commands_are_allowed) && ($command_is_running == "")} {
	set command_is_running $command
	return 1
    } else {
	return 0
    }
}


#
# This procedure must be called last in every command procedure in tkom.
#

proc end_of_command {} {
    global command_is_running

    set command_is_running ""
    do_things_before_waiting
}


# ================================================================


proc not_yet_implemented {msg} {
    show_message "$msg �r �nnu inte implementerat.\n"
}


# ================================================================


proc show_message {string} {
    .mesgf.message configure -state normal
    .mesgf.message insert end $string
    .mesgf.message yview -pickplace end
    .mesgf.message configure -state disabled
}


# +++ This is somewhat slow!
# There should be a "kom_unread total_unread_texts" giving the total
# number of unread texts.

proc show_current_conf {confno} {
    if {$confno == -2} then {
        .statf.cur_conf configure -text "Ej uppkopplad mot n�gon server"
        return
    } elseif {$confno == -1} then {
        .statf.cur_conf configure -text "Ej inloggad"
        return
    } else {
        if {$confno == 0} {
            set conf_name "Ej n�rvarande i n�got m�te"
        } else {
            set conf_name [kom_conference name $confno]
        }
        set local_unread 0
        set total_unread 0
        foreach conf [kom_unread unread_texts] {
            set unread [lindex $conf 1]
            if {[lindex $conf 0] == $confno} {
                set local_unread $unread
            }
            incr total_unread $unread
        }
    }
    set text "$conf_name <$local_unread/$total_unread>"
    .statf.cur_conf configure -text $text
}


# ================================================================

proc create_text_tag {textwin textno} {
    global $textwin-tags

    $textwin tag bind  hottext-$textno <Button-1> "show_text $textwin $textno"
    $textwin tag configure hottext-$textno -underline 1
    return hottext-$textno
}

proc delete_text_tags {textwin} {
    foreach tag [$textwin tag names] {
        if [regexp {^hottext-[0-9]+$} $tag] {
            $textwin tag delete $tag
        }
    }
}


proc show_uplink {textwin ulnk} {
    set parent [uplink parent $ulnk]
    set tagname [create_text_tag $textwin $parent]

    $textwin insert end [format "%s till text %d "\
		[expr {[uplink is_footnote $ulnk] ? "Fotnot" : "Kommentar"}]\
                $parent]
    $textwin tag add $tagname {end - 2 char wordstart} {end - 1 char}


    safe_eval {$textwin insert end [format "av %s\n" \
		    [kom_conference name [kom_text author $parent]]]} {
	    {{KOM BADCONF} {$textwin insert end "(Ok�nd f�rfattare)\n"}}
	    {{KOM BADTEXT} {$textwin insert end "(Ej l�sbar av dig)\n"}}
    }

    if {[uplink sender $ulnk]} {
	safe_eval {$textwin insert end [format " S\{nt av %s\n" \
	                [kom_conference name [uplink sender $ulnk]]]} {
	    {{KOM BADCONF} {$textwin insert end "(Ok�nd f�rfattare)\n"}}
	    {{KOM BADTEXT} {$textwin insert end "(Ej l�sbar av dig)\n"}}
	}
    }
    if {[uplink sent_later $ulnk]} {
	$textwin insert end [format " S\{nt %s\n" \
	    [format_date [uplink sent_at $ulnk]]]
    }
}


proc show_header {textwin textno} {
    $textwin insert end [format "(%d) %s /%d rader/ %s\n" \
			$textno \
			[format_date [kom_text ctime $textno]] \
			[kom_text num_lines $textno] \
			[safe_eval \
			    {kom_conference name [kom_text author $textno]} {
			      {{KOM BADCONF} {format "(f�rfattaren utpl�nad)"}}
			    }
			]]
    set i 0
    while {$i<[kom_text num_recipients $textno]} {
        $textwin insert end [format_recipient [kom_text recipient $textno $i]]
        incr i 1
    }
    set i 0
    while {$i<[kom_text num_uplinks $textno]} {
        show_uplink $textwin [kom_text uplink $textno $i]
	incr i 1
    }
    if [kom_text num_marks $textno] {
        $textwin insert end [format "Markerad av %d personer\n" \
			[kom_text num_marks $textno]]
    }
}


proc show_footer {textwin textno} {
    set i 0
    while {$i<[kom_text num_footnotes $textno]} {
        show_comment_in $textwin [kom_text footnote $textno $i] "Fotnot"
	incr i 1
    }

    set i 0
    while {$i<[kom_text num_comments $textno]} {
        show_comment_in $textwin [kom_text comment $textno $i] "Kommentar"
	incr i 1
    }
}

proc show_comment_in {textwin textno type} {

    set tagname [create_text_tag $textwin $textno]

    $textwin insert end [format "%s i text %d " $type $textno]
    $textwin tag add $tagname {end - 2 char wordstart} {end - 1 char}

    $textwin insert end \
	[safe_eval {format "av %s\n" \
			[kom_conference name [kom_text author $textno]]} {
	    {{KOM BADCONF} {$textwin insert end "(Ok�nd f�rfattare)\n"}}
	    {{KOM BADTEXT} {$textwin insert end "(Ej l�sbar av dig)\n"}}}]
    return
}

proc show_text {textwin textno} {
    global delim_pre
    global delim_post
    global current_text

    safe_eval {
	# Check if we are allowed to read the text before doing anything
	# to the window.
	kom_text author $textno 

	$textwin configure -state normal
	$textwin delete 1.0 end
	delete_text_tags $textwin

	show_header $textwin $textno
	#    $textwin insert end [format_header $textno]
	
	$textwin insert end {�rende: }
	$textwin tag add bold {end linestart} {end - 1 chars}
	$textwin insert end "[kom_text subject $textno]\n"
	$textwin insert end "$delim_pre\n"
	$textwin insert end "[kom_text body $textno]\n"
	$textwin insert end [format "(%d)%s\n" $textno $delim_post]
	
	show_footer $textwin $textno
	
	$textwin configure -state disabled
	update
	
	set current_text $textno

	# This is an ugly way to do return, since "return" is not allowed
	# within safe_eval.
	format TRUE
    } {
	{{KOM BADTEXT} {show_message "Text $textno �r inte l�sbar av dig.\n"
			beep
	                return FALSE}}
    }
}


proc read_text {textwin textno} {
    global to_mark_as_read
    
    if {[show_text $textwin $textno]} {
	lappend to_mark_as_read $textno
    } 
}
