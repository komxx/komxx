// Some functions for making it easier to write TCL commands in C++.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#include <config.h>

#include <cassert>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <sstream>

#include <tcl.h>

#include "tcl-support.h"


// ================================================================


int
Parse_tcl_args::parse_tcl_arg(Tcl_Interp *interp, 
			      Tclargtype argtype, 
			      const char *arg)
{
    int    result;

    switch (argtype)
    {
      case TCLARG_INT:
	result = Tcl_ExprLong(interp, arg, &tcl_intargs[int_index]);
	if (result == TCL_OK)
	{
	    int_index++;
	}
	else
	{
	    Tcl_AppendResult(interp, "Argument not a number: ", arg,
			     (char *) NULL);
	    return TCL_ERROR;
	}
	break;

      case TCLARG_FLOAT:
	Tcl_SetResult(interp, "Floating point argument not yet implemented\n",
		      TCL_STATIC);
	return TCL_ERROR;

      case TCLARG_STRING:
	tcl_stringargs[string_index++] = arg;
	break;

      case TCLARG_ARGS:
	break;

      default:
	fprintf(stderr, "Unknown argument type %d\n", argtype);
	assert(0);
	return TCL_ERROR;
    }

    return TCL_OK;
}


Parse_tcl_args::Parse_tcl_args(Tcl_Interp *interp,
			       Tcl_Args *args, 
			       int argc, 
			       const char *argv[])
  : int_index(0), float_index(0), string_index(0)
{
    int   option;
    int   num_args;

    // We must have at least one argument: the option
    if (argc == 1) 
    {
	Tcl_AppendResult(interp, "Arguments missing for tcl command ",
			 argv[0], (char *)NULL);
	tcl_option = -1;
	return;
    }

    for (option = 0; args[option].option != NULL; ++option) 
    {
	if (strcmp(args[option].option, argv[1]) == 0) 
	{
	    break;		// We found the option!
	}
    }

    // If we have searched all options, then the option doesn't exist.
    if (args[option].option == NULL) 
    {
	Tcl_AppendResult(interp, "unknown option to command ", argv[0],
			 ": ", argv[1], "\nMust be one of:", (char *) NULL);
	for (int i = 0; args[i].option != NULL; ++i)
	    Tcl_AppendResult(interp, args[i].option, " ", (char *)NULL);

	tcl_option = -1;
	return;
    }

    tcl_option = args[option].optionnum;

    // Check the number of arguments!
    num_args = args[option].num_args;
    if (argc - 2 < num_args)
    {
        // Too few arguments given.
	char  str1[20];
	char  str2[20];

	sprintf(str1, "%d", argc - 2);
	sprintf(str2, "%d", num_args);
	Tcl_AppendResult(interp, argv[0],
			 ": Too few arguments to option ",
			 args[option].option, ": ", str1,
			 "\nShould be at least ", str2, (char *)NULL);
	tcl_option = -1;
	return;
    }
    // We check for too many arguments later on.

    int ix;
    for (ix = 2; ix < num_args + 2; ix++)
    {
	if (parse_tcl_arg(interp, args[option].argtypes[ix-2],
			  argv[ix]) != TCL_OK)
	{	
	    Tcl_AppendResult(interp, "error in argument ", argv[ix],
			     (char *)NULL);
	    tcl_option = -1;
	    return;
	}
    }

    for (; ix < argc && ix < num_args + args[option].num_optional + 2; ix++)
    {
	if (parse_tcl_arg(interp, args[option].argtypes[ix-2],
			  argv[ix]) != TCL_OK)
	{	
	    Tcl_AppendResult(interp, "error in argument ", argv[ix],
			     (char *)NULL);
	    tcl_option = -1;
	    return;
	}
    }

    if (ix < argc && args[option].argtypes[ix-2-1] != TCLARG_ARGS)
    {
        // Too many arguments given.
	char  str1[20];
	char  str2[20];

	sprintf(str1, "%d", argc - 2);
	sprintf(str2, "%d", num_args + args[option].num_optional);
	Tcl_AppendResult(interp, argv[0],
			 ": Too many arguments to option ",
			 args[option].option, ": ", str1,
			 "\nShould be at most ", str2, (char *)NULL);
	tcl_option = -1;
	return;
    }
}

int
Parse_tcl_args::option() const
{
    return tcl_option;
}

long
Parse_tcl_args::intarg(int n) const
{
    assert(n < int_index);
    return tcl_intargs[n];
}

double
Parse_tcl_args::floatarg(int n) const
{
    assert(n < float_index);
    return tcl_floatargs[n];
}

const char *
Parse_tcl_args::stringarg(int n) const
{
    assert(n < string_index);
    return tcl_stringargs[n];
}

Parse_tcl_args::~Parse_tcl_args()
{
}

void
set_tcl_error(Tcl_Interp *interp,
	      const LyStr & errmsg,
	      const char *err_code1,
	      const char *err_code2,
	      const char *err_code3,
	      const char *err_code4,
	      const char *err_code5)
{
  char *buf = errmsg.malloc_copy();
  if (buf == NULL)
    {
      Tcl_SetResult(interp, "Out of memory", TCL_STATIC);
      Tcl_SetErrorCode(interp, "KOM", "ENOMEM", (char *)NULL);
    }
  else
    {
      Tcl_SetResult(interp, buf, TCL_DYNAMIC);
      if (err_code1 == NULL)
	{
	  // Let TCL default this to 'NONE'.
	}
      else if (err_code2 == NULL)
	Tcl_SetErrorCode(interp, err_code1, err_code2);
      else if (err_code3 == NULL)
	Tcl_SetErrorCode(interp, err_code1, err_code2, err_code3);
      else if (err_code4 == NULL)
	Tcl_SetErrorCode(interp, err_code1, err_code2, err_code3);
      else if (err_code4 == NULL)
	Tcl_SetErrorCode(interp, err_code1, err_code2, err_code3, err_code4);
      else if (err_code5 == NULL)
	Tcl_SetErrorCode(interp, err_code1, err_code2, err_code3, err_code4,
			 err_code5);
      else
	Tcl_SetErrorCode(interp, err_code1, err_code2, err_code3, err_code4,
			 err_code5, (char*)NULL);
    }
  return;
}

void
set_tcl_result(Tcl_Interp *interp,
	       const std::ostringstream &res)
{
    std::strncpy(interp->result, res.str().c_str(), TCL_RESULT_SIZE-1);
    interp->result[TCL_RESULT_SIZE-1] = '\0';
}
