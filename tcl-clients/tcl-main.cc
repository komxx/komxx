// Startup and main loop of nilkom.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#include <config.h>

#include <unistd.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <fcntl.h>

#include "insig.h"
#include "misc.h"
#include "tcl-glue.h"
#include "connection.h"
#include "userio.h"
#include "cmd-decode.h"
#include "conn_io_isc.h"

static User_input *input_channel = NULL;
static Nilkom_paginator *output_channel = NULL;

static IscMaster *    
init_isc()
{
    IscMaster *mcb;
    IscConfig config;
    
    config.version = 1006;
    config.master.version = 1001;
    config.master.memfn.alloc = NULL; // Use default allocators.
    config.master.memfn.realloc = NULL;
    config.master.memfn.free = NULL;
    config.master.abortfn = NULL; /* Use default abort function. */
    config.session.version = 1002;
    config.session.max.msgsize = -1; /* Use default sizes. */
    config.session.max.queuedsize = -1;
    config.session.max.dequeuelen = -1;
    config.session.max.openretries = -1;
    config.session.max.backlog = -1;
    config.session.fd_relocate = 0;
    
    mcb  = isc_initialize(&config);
    if ( mcb == NULL )
    {
	std::cerr << "can't isc_initialize()\n";
	exit(1);
    }

    return mcb;
}

int
main(int argc,
     char **argv)
{
    int result;

    Tcl_Interp *motor = Tcl_CreateInterp();
    mcb_for_isc = init_isc();

    setup_input_sig();

    input_channel = new User_input(mcb_for_isc, 0);
    output_channel = new Nilkom_paginator(input_channel, std::cout);

    kom_define_commands(motor);
    define_userio_commands(motor, input_channel, output_channel);
    define_cmd_decode_commands(motor);

    extern char *nilkom_initscript; // From nilkominit.c.
    
    char *initscript = nilkom_initscript;
    
#if 0
    if (argc >= 2 && !strcmp(argv[1], "--www"))
	initscript = "source $kom_library/www.tcl";
#endif

    // Set argc/argv for the TCL program
    char   *buf;
    buf = new char[strlen(initscript)+1];
    sprintf(buf, "%d", argc);
    Tcl_SetVar(motor, "argc", buf, TCL_GLOBAL_ONLY);
    if (argc == 0)
	Tcl_SetVar(motor, "argv", "", TCL_GLOBAL_ONLY);
    else 
    {
	for (int i = 0; i < argc; ++i) 
	    Tcl_SetVar(motor, "argv", argv[i],
		       TCL_GLOBAL_ONLY | TCL_APPEND_VALUE | TCL_LIST_ELEMENT);
    }

    strcpy(buf, initscript);	// TCL wants to write in this string!
    result = Tcl_Eval(motor, buf);
    delete[] buf;
    buf = NULL;
    restore_kbd();		// This line is essential.
    if (result != TCL_OK) 
	fprintf(stderr, "\nnilkom: Error in TCL code: %s\n", motor->result);

    // Clean up everything, so that Purify can do a better job.

    Tcl_DeleteInterp(motor);
    delete output_channel;
    delete input_channel;
    isc_shutdown(mcb_for_isc);

    // Setting these variables to NULL makes Purify find even more
    // memory leaks.

    motor = NULL;
    output_channel = NULL;
    input_channel = NULL;
    mcb_for_isc = NULL;
    return 0;
}
