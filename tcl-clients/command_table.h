// nilkom TCL command tables.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#ifndef KOMXX_COMMAND_TABLE_H
#define KOMXX_COMMAND_TABLE_H

#include <vector>

#include "command_table_entry.h"
#include "LyStr.h"
#include "Tcl_LyStr.h"

class Command_table {
public:
  Command_table();
  ~Command_table();

  Command_table_entry::errcode add_entry(const LyStr & cmd_template,
					 const LyStr & cmd_pretty,
					 const LyStr & tcl_cmd);
  enum matchres {
    NOMATCH,
    UNIQUE,
    AMBIGUOUS
    };
  
  matchres decode_input(const LyStr &user_input, 
			LyStr &tcl_cmd, 
			Tcl_LyStr &alternatives);

  Command_table(const Command_table &);
  Command_table & operator = (const Command_table &);
private:
  std::vector<Command_table_entry> entries;
};

#endif
