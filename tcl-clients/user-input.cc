// user-input.h - Input from the user
// 
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <cassert>
#include <tcl.h>
#include <cstdlib>

#include "user-input.h"
#include "LyStr.h"
#include "connection.h"

// FIXME

User_input::User_input(IscMaster *master, int infd)
    : pending_input(),
      in_master(master),
      idle_function(NULL)
{
  in_cb = isc_openfd(master, infd);
}

User_input::~User_input()
{
  isc_destroy(in_master, in_cb);
}

void
User_input::idle_callback(void (*callback)())
{
  idle_function = callback;
}

int
User_input::fill_pending_input(int timeout)
{
  IscEvent *ev;
  while ((ev = Conn_io_ISC::get_event(timeout)) == NULL)
      ;

  if (ev == &Conn_io_ISC::error_event)
    {
      return -1;
    }

  if (ev != &Conn_io_ISC::timeout_event)
    {
      assert (ev->session == in_cb);
      switch(ev->event)
	{
	case ISC_EVENT_ERROR:
	  abort();
	case ISC_EVENT_MESSAGE:
	  pending_input << LyStr(ev->msg->buffer,
					 ev->msg->length);
	  break;
	default:
	  abort();
	}
    }
  else
    {
      ev = NULL; // The timeout event should not be disposed of.
      if (idle_function != NULL)
      {
	  // Call the idle function, but make sure it is not called
	  // again until it returns.  Infinite recursion is bad.
	  void (*ifunc)() = idle_function;
	  idle_function = NULL;
	  (*ifunc)();
	  idle_function = ifunc;
      }
    }

  if (ev != NULL)
    isc_dispose(ev);

  return 0;
}

int
User_input::get_key()
{
  int timeout = 0;
    
  while (1)
    {
      if (pending_input.strlen() > 0)
	{
	  char res = pending_input[0];
	  pending_input.remove_first(1);
	  // FIXME: use new-style cast or maybe exceptions instead
	  return (unsigned char)res;
	}
      if (fill_pending_input(timeout) < 0)
	return -1;
      timeout = 1000;
    }
}

int
User_input::pending(int timeout)
{
  if (pending_input.strlen() == 0)
    fill_pending_input(timeout);
 
  return pending_input.strlen();
}


