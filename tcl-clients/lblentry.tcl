# A widget which combines a label and an entry.
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# ----------------------------------------------------------------------
#  PURPOSE:  A combination of a label and an entry widget
#
#   AUTHOR:  Inge Wallin
#            ingwa@signum.se
#
#   METHODS: configure	{config}
#	     get	{}
#	     put	{string}
#	     insert	{index string}
#	     bind	{sequence action}
#	     focus	{}
#
#   PUBLICS: lblwidth		- The width of the label
#	     lbltext		- The text in the label
#	     entrywidth 	- The width of the entry
#	     entrytext		- The text in the entry
#	     textvariable	- A variable associated with the entry
#	     orient		- The orientation of the lblentry
#
# ======================================================================

# ----------------------------------------------------------------------
#  A labeled entry
# ----------------------------------------------------------------------
itcl_class lblentry {
    # ------------------------------------------------------------------
    #  CONSTRUCTOR - create new lblentry
    # ------------------------------------------------------------------
    constructor {config} {
	#
	#  Create a window with the same name as this object
	#
	set class [$this info class]
	::rename $this $this-tmp-
	::frame $this -class $class
	::rename $this $this-win-
	::rename $this-tmp- $this

	label $this.lbl -anchor w -width $lblwidth -text $lbltext
	entry $this.ent -relief sunken -width $entrywidth
	if {$orient == "horizontal"} {
            pack $this.lbl $this.ent -side left
	} else {
            pack $this.lbl $this.ent -side top -anchor w
	}

	#
	#  Explicitly handle config's that may have been ignored earlier
	#
	foreach attr $config {
	    configure -$attr [set $attr]
	}
    }

    # ------------------------------------------------------------------
    #  DESTRUCTOR - destroy window containing widget
    # ------------------------------------------------------------------
    destructor {
	::rename $this-win- {}
	destroy $this
    }

    # ------------------------------------------------------------------
    #  METHOD:  configure - used to change public attributes
    # ------------------------------------------------------------------
    method configure {config} {}

    # ------------------------------------------------------------------
    #  METHOD:  get - Get the text in the entry
    # ------------------------------------------------------------------
    method get {} {
	$this.ent get
    }

    # ------------------------------------------------------------------
    #  METHOD:  put - Set the text in the entry
    # ------------------------------------------------------------------
    method put {string} {
	$this.ent delete 0 end
	$this.ent insert 0 $string
    }

    # ------------------------------------------------------------------
    #  METHOD:  insert - Insert text data into the entry
    # ------------------------------------------------------------------
    method insert {index string} {
	$this.ent insert $index $string
    }

    # ------------------------------------------------------------------
    #  METHOD:  bind - Bind actions to X windows events
    # ------------------------------------------------------------------
    method bind {sequence action} {
        ::bind $this.ent $sequence $action
    }

    # ------------------------------------------------------------------
    #  METHOD:  focus - Put the focus in the entry
    # ------------------------------------------------------------------
    method focus {} {
	::focus $this.ent
    }

    # ================================================================
    #  PUBLIC DATA
    # ================================================================

    # ------------------------------------------------------------------
    #  PUBLIC:  lblwidth - The width of the label
    # ------------------------------------------------------------------
    public lblwidth 10 {
	if {[winfo exists $this]} {
	    $this.lbl configure -width $lblwidth
	}
    }

    # ------------------------------------------------------------------
    #  PUBLIC:  lbltext - The text in the label
    # ------------------------------------------------------------------
    public lbltext "" {
	if {[winfo exists $this]} {
	    $this.lbl configure -text $lbltext
	}
    }

    # ------------------------------------------------------------------
    #  PUBLIC:  entrywidth - The width of the entry
    # ------------------------------------------------------------------
    public entrywidth 20 {
	if {[winfo exists $this]} {
	    $this.ent configure -width $entrywidth
	}
    }
    
    # ------------------------------------------------------------------
    #  PUBLIC:  textvariable - A variable associated with the entry
    # ------------------------------------------------------------------
    public textvariable {} {
	if {[winfo exists $this]} {
	    $this.ent configure -textvariable $textvariable
	}
    }
    
    # ------------------------------------------------------------------
    #  PUBLIC:  orient - The orientation of the lblentry
    # ------------------------------------------------------------------
    public orient horizontal {
	if {[winfo exists $this]} {
	    if {$orient == "horizontal"} {
		pack $this.lbl $this.ent -side left
	    } elseif {$orient == "vertical"} {
		pack $this.lbl $this.ent -side top
	    } else {
		set $orient horizontal
		error "Wrong orientation: $orient. Should be horizontal or vertical."
	    }
	}
    }
}
