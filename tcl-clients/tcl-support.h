// Declarations of the functions in tcl-support.cc
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#ifndef KOMXX_TCL_SUPPORT_H
#define KOMXX_TCL_SUPPORT_H

#include <tcl.h> 
#include "LyStr.h"

// ================================================================
//          Types for parsing arguments to tcl procedures


enum Tclargtype {
    TCLARG_INT,			// An integer argument
    TCLARG_FLOAT,		// A floating point argument
    TCLARG_STRING,		// A string argument
    TCLARG_ARGS,		// An unspecified number of arguments
				// (they are not checked or assigned)
};

const int TCL_ARGS_MAX = 8;

struct Tcl_Args {
    char        * option;
    int           optionnum;
    int           num_args;	// Number of arguments.
    int		  num_optional;	// Number of additional optional arguments.
    Tclargtype    argtypes[TCL_ARGS_MAX];
};


// ================================================================


class Parse_tcl_args {
  public:
    Parse_tcl_args(Tcl_Interp *, Tcl_Args *, int args, const char **argv);
    ~Parse_tcl_args();
    
    int option() const;

    long intarg(int) const;
    double floatarg(int) const;
    // Note: the pointers returned by these methods are equal to the
    // pointers that are given to the constructor in the argv
    // argument. That is, they are not allocated by this class.
    const char *stringarg(int) const;

    enum { TCL_MAX_INTS=7, TCL_MAX_FLOATS=4, TCL_MAX_STRINGS=4 };
  private:
    int    tcl_option;

    long     tcl_intargs[TCL_MAX_INTS];
    double   tcl_floatargs[TCL_MAX_FLOATS];
    const char *tcl_stringargs[TCL_MAX_STRINGS];

    int      int_index;
    int      float_index;
    int      string_index;
    int	     parse_tcl_arg(Tcl_Interp *interp, Tclargtype argtype, 
			   const char *arg);
};

// Calls Tcl_SetResult and Tcl_SetErrorCode.
extern void
set_tcl_error(Tcl_Interp *interp,
	      const LyStr & errmsg,
	      const char *err_code1 = NULL,
	      const char *err_code2 = NULL,
	      const char *err_code3 = NULL,
	      const char *err_code4 = NULL,
	      const char *err_code5 = NULL);

extern void
set_tcl_result(Tcl_Interp *interp,
	       const std::ostringstream &res);

#endif  // TCL_SUPPORT_H
