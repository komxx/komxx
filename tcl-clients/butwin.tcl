# A dialog window with some buttons.
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# ----------------------------------------------------------------------
#
#  PURPOSE:  A toplevel window with some buttons
#
#   AUTHOR:  Inge Wallin
#            ingwa@signum.se
#
#   METHODS: configure	       {config}
#            wait              {}
#            activate_button   {butno}
#
#   PUBLICS: buttons	- Array of button labels
#	     default	- Number of the default button if any
#
# ======================================================================

# ----------------------------------------------------------------------
#  A toplevel window with a number of buttons
# ----------------------------------------------------------------------
itcl_class butwin {
    # ------------------------------------------------------------------
    #  CONSTRUCTOR - create new butwin
    # ------------------------------------------------------------------
    constructor {config} {
	#
	#  Create a window with the same name as this object
	#
	set class [$this info class]
	::rename $this $this-tmp-
	::toplevel $this -class $class
	::rename $this $this-win-
	::rename $this-tmp- $this

	frame $this.f -relief raised -bd 1
	pack $this.f -side bottom -fill both

	#
	#  Explicitly handle config's that may have been ignored earlier
	#  This includes creating the buttons in the newly created frame.
	#
#	foreach attr $config {
#	    configure -$attr [set $attr]
#	}
	configure -buttons $buttons
   }

    # ------------------------------------------------------------------
    #  DESTRUCTOR - destroy window containing widget
    # ------------------------------------------------------------------
    destructor {
	catch {::rename $this-win- ""}
	destroy $this
    }

    # ------------------------------------------------------------------
    #  METHOD:  configure - used to change public attributes
    # ------------------------------------------------------------------
    method configure {config} {}


    # ------------------------------------------------------------------
    #  METHOD:  wait - Wait for a button to get pressed
    # ------------------------------------------------------------------
    method wait {} {
	global sgn_priv

	set oldFocus [focus]
	grab $this
	focus $this

	tkwait variable sgn_priv(butwin,$this)
	focus $oldFocus

	return $sgn_priv(butwin,$this)
    }


    # ------------------------------------------------------------------
    #  METHOD:  activate_button - Activate one of the buttons
    # ------------------------------------------------------------------
    method activate_button {butno} {
	$this.f.button$butno flash
	$this.f.button$butno invoke
    }


    # ================================================================
    #  PUBLIC DATA
    # ================================================================

    # ------------------------------------------------------------------
    #  PUBLIC:  buttons - A list of the button texts.
    # ------------------------------------------------------------------
    public buttons {Ok Cancel} {
	global sgn_priv

	if {[winfo exists $this]} {
	    # Destroy the old buttons if there are any.
	    foreach but [::info commands $this.f.*] {
		destroy $but
	    }

	    # Create new buttons instead of the old ones
	    set i 0
	    foreach but $buttons {
		set text [lindex $but 0]
		set action [lindex $but 1]
		button $this.f.button$i -text $text \
		    -command "set sgn_priv(butwin,$this) $i; $action"

		if {$i == $default} {
		    frame $this.f.default -relief sunken -bd 1
		    raise $this.f.button$i $this.f.default
		    pack $this.f.default -in $this.f -side left \
			-expand 1 -padx 3m -pady 2m
		    pack $this.f.button$i -in $this.f.default \
			-padx 2m -pady 2m -ipadx 2m -ipady 1m
		    bind $this <Return> \
			"$this activate_button $i"
		} else {
		    pack $this.f.button$i -in $this.f -side left \
			-expand 1 -padx 3m -pady 3m -ipadx 2m -ipady 1m
		}
		incr i
	    }
	}
    }

    # ------------------------------------------------------------------
    #  PUBLIC:  default - The number of the default button, if any
    #                     -1 ==> No default button
    # ------------------------------------------------------------------
    public default -1 {
	if {[winfo exists $this]} {
	    $this configure -buttons $buttons
	}
    }
}
