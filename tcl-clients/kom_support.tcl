# Debug functions which emulate the kom commands.
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


proc connect {server port} {
}


proc kom_session {operation {arg1 {}} {arg2 {}}} {
    case $operation {
        login 		   {return 1}
	set_client_version {return 1}
	lookup_name 	   {return {{5 {0000}} {7 {0001}} {19 {0001}}}}
    }

    puts "kom_session operation unknown: $operation\n"
    return 4711
}


proc kom_conference {operation confno} {
    case $operation {
        name {return "M�te $confno (finns inte)"}
    }

    puts "kom_conference operation: $operation\n"
    return 4711
}


proc kom_text {operation textno {arg1 0}} {
    case $operation {
        length         {return 9999}
	num_lines      {return 123}
        author         {return 17}
	num_marks      {return 3}
        num_recipients {return 1}
        recipient      {return {42 0 4711 0 1 0 0 0}}
	ctime          {return "5 10 20 21 11 1993 ons 324 a"}
	num_uplinks    {return 1}
        uplink         {if {$arg1!=0} \
			  {error {kom_text uplink textno uplink_no: uplink_no out of range}} \
			  {return [list 0 [expr $textno-1] 19 0 0]}}
	num_footnotes  {return 1}
	footnote       {if {$arg1<0||$arg1>0} \
			 {error {kom_text footnote textno footnote_no: footnote_no out of range}} \
			 {return [expr $textno+$arg1+1]}}
	num_comments   {return 2}
	comment        {if {$arg1<0||$arg1>1} \
			 {error {kom_text comment textno comment_no: comment_no out of range}} \
		       {return [expr $textno+$arg1+2]}}
	comments       {return [list [expr $textno+1] [expr $textno+2]]}
        subject        {return "�rende"}
        body	       {return "Den oerh�rt spirituella texten h�r!"}
	mark_as_read   {return 1}
    }

    puts "kom_text operation: $operation\n"
    return 4711
}


proc uplink {operation uplink} {
    case $operation {
        parent      {return [lindex $uplink 0]}
        is_footnote {return [lindex $uplink 1]}
	sender      {return 0}
	sent_later  {return 0}
    }
    puts "uplink operation: $operation\n"
    return 4711
}

set delim_pre "-------------------------------------------------------------\n"
set delim_post "--------------------------------\n"


# ================================================================
#         The kom_unread command and it's help commands


# Some examples to use while testing:
#
set current_unread  {5 {1 2 3 4 5 6 7 8 9 10}}
set unread_list    {{7 {15 16 17 18 19}} {10 {99 100 101 102}}}

proc unread_unread_texts {} {
    global current_unread
    global unread_list

    set result {}
    set start [linsert $unread_list 0 $current_unread]
    foreach conf $start {
        lappend result [list [lindex $conf 0] \
                             [llength [lindex $conf 1]] \
			     [expr [llength [lindex $conf 1]]+2]]
    }

    return $result
}

proc unread_prompt {} {
    global current_unread
    global unread_list

    if {$current_unread == ""} {
        return NO_MORE_TEXT
    } elseif {[llength [lindex $current_unread 1]] == 0} {
        if {[llength $unread_list] == 0} {
            return NO_MORE_TEXT
        } else {
            return NEXT_CONF
        }
    } else {
        return NEXT_TEXT
    }
}

proc unread_goto_next_conf {} {
    global current_unread unread_list

    if {[llength $unread_list] == 0} {
        if {$current_unread == ""} {
            return 0;
        }
    } else {
        set new_current [lindex $unread_list 0]
	set unread_list [lrange $unread_list 1 99999]
	if {[llength [lindex $current_unread 1]] > 0} {
            lappend unread_list $current_unread
        }
        set current_unread $new_current
    }

    return [lindex $current_unread 0]
}

proc unread_next_text {arg1} {
    global current_unread
    global unread_list

    if {$current_unread == ""} {
        return 0
    } elseif {[llength [lindex $current_unread 1]] == 0} {
	return 0
    } else {
        set next_text [lindex [lindex $current_unread 1] 0]
        if {$arg1 == "remove"} {
            set current_unread [list [lindex $current_unread 0] \
				  [lrange [lindex $current_unread 1] 1 99999]]
        }
        return $next_text
    }
}

proc kom_unread {option {arg1 {}} args} {
    global current_unread
    global unread_list

    case $option in {
        update                  {return}
        unread_texts            {return [unread_unread_texts] }
        unread_texts_in_current {return [llength [lindex $current_unread 1]]}
        prompt                  {return [unread_prompt]}
        goto_next_conf          {return [unread_goto_next_conf] }
        current_conf            {if {$current_unread == ""} \
				    {return 0} \
				 else \
				    {return [lindex $current_unread 0]}}
        next_text               {return [unread_next_text $arg1]}
        prefetch_map		{return 0}
    }
    puts "kom_unread: Unknown option $option"
    return 4711
}
