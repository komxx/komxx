# Commands found under the "Misc" menu in tkom
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# ================================================================
#                    Command: Display time


proc display_time {} {
    show_message \
      "\nKlockan �r [format_date [kom_session get_time]] (enligt servern)\n"
}


proc display_time_command {} {
    if {![start_of_command display_time_command]} {
	return
    }
    display_time
    end_of_command
}


# ================================================================
#                         Command: Give support


proc give_support_command {} {
    if {![start_of_command give_support_command]} {
	return
    }

    show_message \
"\nDu �r mycket vacker och mycket klok.  M�nga �lskar dig b�de \
till kropp\noch till sj�l.  Du kommer att �ver�sas med rikedom och \
f� stor lycka i ditt\nliv.  Var glad att just du �r du.  Det har du \
all anledning att vara.\n\n"

    end_of_command
}


# ================================================================
#                 Command: Show persons logged in 



proc show_logged_in_command {} {
    if {![start_of_command show_logged_in_command]} {
	return
    }

    show_message "F�ljande personer k�r just nu LysKOM:\n"
    set session_list [kom_session who_is_on]
    foreach session $session_list {
	show_message [format "%4d %-38.38s %-35.35s\n" \
		   [lindex $session 2] \
		   [safe_eval {kom_conference name [lindex $session 0]} {
		       {{KOM BADCONF 0 8} {format "oinloggad person"}}
		       {{KOM BADCONF}     {format "hemlig person"}}}] \
		   [safe_eval {kom_conference name [lindex $session 1]} {
		       {{KOM BADCONF 0 8}
			   {format "ej n�rvarande i n�got m�te"}}
		       {{KOM BADCONF} {format "hemligt m�te"}}}]]
	show_message [format "     %-38.38s %-35.35s\n" \
		   [lindex $session 4] [lindex $session 3]]
    }
    show_message "Totalt [llength $session_list] personer\n\n"

    end_of_command
}
