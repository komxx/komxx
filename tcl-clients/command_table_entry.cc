// nilkom TCL command table entries.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cctype>
#include <cassert>
#include <cstdlib>

#include "Tcl_LyStr.h"
#include "command_table_entry.h"

Command_table_entry::Command_table_entry()
    : err(UNINITIALIZED),
      user_template(),
      tcl_template(),
      pretty()
{
}

Command_table_entry::Command_table_entry(const Command_table_entry &te)
    : err(te.err),
      user_template(te.user_template),
      tcl_template(te.tcl_template),
      pretty(te.pretty)
{
}

Command_table_entry & 
Command_table_entry::operator = (const Command_table_entry &te)
{
  user_template = te.user_template;
  tcl_template = te.tcl_template;
  pretty = te.pretty;
  err = te.err;
  return *this;
}


Command_table_entry::Command_table_entry(const LyStr &cmd_template,
					 const LyStr &cmd_pretty,
					 const LyStr &tcl_cmd)
     : user_template(cmd_template), 
       tcl_template(tcl_cmd),
       pretty(cmd_pretty)
{
  int ix;
  int percents = 0;
  int maxpercent = -1;
  int r_found = 0;

  if (user_template.strlen() == 0)
    {
      err = EMPTY_USER_CMD;
      return;
    }
  if (tcl_template.strlen() == 0)
    {
      err = EMPTY_TCL_CMD;
      return;
    }
  if (user_template[0] == ' ')
    {
      err = BAD_USER_TEMPLATE;
      return;
    }

  // Check the user template.
  for (ix = 0; ix < user_template.strlen(); ix++)
    {
      switch(user_template[ix])
	{
	case ' ':
	  // Two spaces in a row is prohibited. Trailing space is prohibited.
	  if (ix == user_template.strlen() || user_template[ix+1] == ' ')
	    {
	      err = BAD_USER_TEMPLATE;
	      return;
	    }
	  break;
	case '%':
	  percents++;
	  if (ix == user_template.strlen())
	    {
	      err = BAD_WILDCARD;
	      return;
	    }

	  // Only a few %-modifiers exist.
	  switch (user_template[ix+1])
	    {
	    case 'I':
	      break;
	    case 'R':
	      // Can only have one 'R' modifier.
	      if (r_found++ > 0)
		{
		  err = BAD_WILDCARD;
		  return;
		}
	      break;
	    default:
	      err = BAD_WILDCARD;
	      return;
	    }

	  if (ix+2 < user_template.strlen()
	      && user_template[ix+2] != ' ')
	    {
	      err = BAD_WILDCARD;
	      return;
	    }
	  break;
	default:
	  // Ordinary characters are allowed.
	  break;
	}
    }

  // Check the tcl template.
  for (ix = 0; ix < tcl_template.strlen(); ix++)
    if (tcl_template[ix] == '%')
      {
	if (ix == tcl_template.strlen() || !isdigit(tcl_template[ix+1]))
	  {
	    err = BAD_TCL_WILDCARD;
	    return;
	  }
	if (maxpercent < tcl_template[ix+1] - '0')
	  maxpercent = tcl_template[ix+1] - '0';
      }
  
  if (maxpercent > percents)
    {
      err = WILDCARD_MISMATCH;
      return;
    }

  err = NOERR;
}

Command_table_entry::~Command_table_entry()
{
}

void
Command_table_entry::try_match(LyStr user_try,
			       LyStr &tcl_command,
			       LyStr &u_pretty,
			       int &penalty) const
{
  int ix = 0;
  Tcl_LyStr args[MAXARGS];
  int nargs = 0;

  assert(err == NOERR);

  penalty = 0;

  user_try.skipspace();

  // Match everything.
  while (ix < user_template.strlen() && user_try.strlen() > 0)
    {
      assert(user_try.strlen() > 0);
      assert(user_try[0] != ' ');
      assert(user_template.strlen() > ix);
      assert(user_template[ix] != ' ');

      // Try to match the next word.

      if (user_template[ix] == '%')
	{
	  assert(nargs < MAXARGS);
	  ix++;
	  assert(ix < user_template.strlen());

	  switch(user_template[ix])
	    {
	    case 'I':
	      // Expect an integer.
	      while (user_try.strlen() > 0 && isdigit(user_try[0]))
		{
		  args[nargs] << user_try[0];
		  user_try.remove_first(1);
		}
	      // Only space or end-of-string can terminate a number.
	      // Make sure there was at least one digit.
	      if ((user_try.strlen() > 0 && user_try[0] != ' ')
		  || args[nargs].strlen() == 0)
		{
		  // Bad integer; match failed.
		  penalty = -1;
		  return;
		}
	      nargs++;
	      break;
	    case 'R':
	      // Copy remainder of line verbatim.
	      args[nargs].append_element(user_try);
	      user_try.clear();
	      nargs++;
	      break;
	    default:
	      abort();
	    }
	  ix++;
	}
      else
	{
	  // The template did not contain a "%" wildcard, so it must
	  // be a word.  See if the current word matches it.

	  while (user_try.strlen() > 0 && user_try[0] != ' ' 
		 && user_template.strlen() > ix && user_template[ix] != ' '
		 && user_try[0] == user_template[ix])
	    {
	      user_try.remove_first(1);
	      ix++;
	    }
	  
	  if (user_try.strlen() > 0 && user_try[0] != ' ')
	    {
	      // Unmatched garbage - this cannot match.
	      penalty = -1;
	      return;
	    }

	  while (user_template.strlen() > ix && user_template[ix] != ' ')
	    {
	      // Abbreviated word.  Give one penalty point for each
	      // abbreviated character.
	      penalty++;
	      ix++;
	    }
	}

      // Skip trailing whitespace, if any.

      user_try.skipspace();
      if (user_template.strlen() > ix)
	{
	  assert(user_template[ix] == ' ');
	  ix++;
	}
    }

  if (user_try.strlen() > 0)
    {
      assert(ix >= user_template.strlen());
      // Trailing garbage in user input - cannot match.
      penalty = -1;
      return;
    }
  
  assert(user_try.strlen() == 0);

  // All input from the user has been used up.  Check that the rest of
  // the template is matched by the empty string.
  while (ix < user_template.strlen())
    {
      if (user_template[ix] == '%')
	{
	  // Special pattern.
	  assert(nargs < MAXARGS);
	  ix++;
	  assert(ix < user_template.strlen());

	  switch(user_template[ix])
	    {
	    case 'I':
	      // %I must always match some input.
	      penalty = -1;
	      return;
	    case 'R':
	      // %R can match a trailing empty string.
	      args[nargs].append_element("");
	      nargs++;
	      break;
	    default:
	      abort();
	    }
	  ix++;
	  assert(ix >= user_template.strlen() || user_template[ix] == ' ');
	}
      else
	{
	  // Normal word.  Give 100 penalty points for each
	  // abbreviated word.
	  penalty += 100;

	  // Hunt for next word.
	  while (ix < user_template.strlen() && user_template[ix] != ' ')
	    ix++;
	}
      
      if (ix < user_template.strlen())
	{
	  // Skip word separator
	  assert(user_template[ix] == ' ');
	  ix++;
	  // There was a separator, so there must be another word.
	  assert (ix < user_template.strlen());
	}
    }

  // We found a match.  Now build the tcl command string.
  tcl_command.clear();
  for (ix = 0; ix < tcl_template.strlen(); ix++)
    {
      if (tcl_template[ix] == '%')
	{
	  ix++;
	  assert(ix < tcl_template.strlen());
	  assert(MAXARGS < 10);
	  assert(tcl_template[ix] >= '0'
		 && tcl_template[ix] < '0' + MAXARGS);
	  tcl_command << args[tcl_template[ix] - '0'];
	}
      else      
	tcl_command << tcl_template[ix];
    }

  u_pretty = pretty;
  return;
}

Command_table_entry::errcode
Command_table_entry::status()
{
  return err;
}

