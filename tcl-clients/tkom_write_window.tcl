# Code for handling windows for writing messages in tkom
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# ================================================================
#                 A window for writing texts


itcl_class write_window {
    inherit butwin

    constructor {{title Textf�nster} config} {
        butwin::constructor \
	    -buttons [list [list "S�nd texten" "$this _send_text"] \
			  [list "Kasta bort texten" "$this _dont_send_text"]]
        wm title $this $title

        frame $this.menu -relief raised -bd 2
        pack $this.menu -side top -fill x

        # File menu
        menubutton $this.menu.file -text "Arkiv   " -underline 0 \
	    -menu $this.menu.file.m
        menu $this.menu.file.m
        $this.menu.file.m add command -label "L�s fr�n fil..." \
	    -underline 0 -state disabled
	$this.menu.file.m add command -label "Spara" -underline 0 \
	    -state disabled
	$this.menu.file.m add command -label "spara sOm..." -underline 7 \
	    -state disabled
	$this.menu.file.m add separator
	$this.menu.file.m add command -label "Avbryt" \
	    -underline 0 -accelerator C-xC-c \
	    -command "$this delete"
	pack $this.menu.file -side left

	# Text menu
	menubutton $this.menu.text -text "Text    " -underline 0 \
	    -menu $this.menu.text.m
	menu $this.menu.text.m
	$this.menu.text.m add command -label "Addera mottagare..." -underline 0 \
	    -state disabled
	$this.menu.text.m add command -label "Se det kommenterade" -underline 0 \
	    -state disabled
	pack $this.menu.text -side left

	tk_menuBar $this.menu $this.menu.file $this.menu.text

	# The text frame, text and scrollbar
	frame $this.textf -relief sunken
	pack $this.textf -side top -fill x
	scrollbar $this.textf.textscroll -relief flat -command "$this.textf.text yview"
	text $this.textf.text -relief raised -bd 2 -height 15 \
	    -yscrollcommand "$this.textf.textscroll set" -setgrid true
	pack $this.textf.textscroll -side right -fill y
	pack $this.textf.text -side left -expand TRUE -fill x

	tkwait visibility $this
	focus $this.textf.text

	#
	#  Explicitly handle config's that may have been ignored earlier
	#
	foreach attr $config {
	    configure -$attr [set $attr]
	}
    }

    destructor {
    }

    method configure {config} {}

    method _send_text {} {
	global to_mark_as_read

	set textmass     [$this.textf.text get 0.0 end]
	safe_eval {
	    set new_text [kom_session create_text $textmass \
				     $recipients $comment_to $footnote_to]
	    lappend to_mark_as_read $new_text
	    show_message "Text $new_text skapad.\n"} {
		{{KOM FAILED-CALL} {show_message "Texten kunde inte skapas\n"}}
	    }

	$this delete
    }

    method _dont_send_text {} {
        show_message "Inl�gget bortkastat\n"
        $this delete
    }


    public recipients {} {}
    public comment_to {} {}
    public footnote_to {} {}

    public subject {} {
	if {[winfo exists $this]} {
	    # FIXME+++: Delete the former subject, if any
	    $this.textf.text insert end $subject
	}
    }

#    public recipients {} {
#	if {[winfo exists $this]} {
#	    <Config code here...>
#	}
#    }
#
#    protected <VARNAME> <Init value>
#
#    common <VARNAME> <Init value>
}
