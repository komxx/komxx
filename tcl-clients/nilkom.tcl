# The main program for the nilkom client.
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Load all files we might use.
proc resource {} {
    uplevel #0 {source $kom_library/komlib.tcl}
    uplevel #0 {source $kom_library/nilkom2.tcl}
    uplevel #0 {source $kom_library/nilkom_edit.tcl}
    uplevel #0 {source $kom_library/nilkom_cmd.tcl}
    uplevel #0 {source $kom_library/nilkom_print.tcl}
}
resource

proc usage {} {
    global argv

    print "usage: [lindex $argv 0] \[argument\] \[host \[port\]\]\n"
    print "\n"
    print "...d�r argument �r n�got/n�gra av f�ljande:\n"
    print "    --hj�lp    - Skriv ut den h�r hj�lptexten.\n"
    print \
	"    --swascii  - �vers�tt 8-bitarstecken till l�mpliga 7-bitstecken\n"
    print "    --iswascii - Som swascii, men bara vid inmatning.\n"
    print "    --oswascii - Som swascii, men bara vid utmatning.\n"
    print "    --version  - Skriv ut versionsnumret.\n"
    clean_exit
}

# Parse the command line.
set got_host 0
set got_port 0
set nilkom_itranslate 0
for {set i 1} {$i < $argc} {incr i} {
    set arg [lindex $argv $i]
    switch -- $arg {
	--swascii {
	    nilkom_page translate swascii
	    set nilkom_itranslate 1
	}
	--iswascii {set nilkom_itranslate 1}
	--oswascii {nilkom_page translate swascii}
	--hj�lp   {usage}
	# For compatibility reasons:
	--help    {usage}
	--version {
	    print "nilkom version $nilkom_version\n"
	    clean_exit
	}
	default   
	    {
		if {$got_host && $got_port} {
		    usage
		} elseif {$got_host} {
		    set PORT $arg
		    set got_port 1
		} else {
		    set HOST $arg
		    set got_host 1
		}
	    }
    }
}

# No prefetching until after we have logged in
set prefetch_state notyet

if {[info exists env{LC_CTYPE}]} {
    if {[info exists env{IC_CTYPE}]} {
	if {$env{IC_CTYPE} != $env{LC_CTYPE}} {
	    puts stderr, "Error: conflicting CTYPE declarations in environment"
	    clean_exit
	}
    } else {
	puts stderr, "Error: IC_CTYPE not set"
	clean_exit
    }
} elseif {[info exists env{IC_CTYPE}]} {
    puts stderr, "Error: LC_CTYPE not set"
    clean_exit
}
    
print "nilkom version $nilkom_version\n"
print "\n"
print "Kopplar upp mot $HOST $PORT...\n"
connect $HOST $PORT nilkom $nilkom_version

kom_session register_async text_message text_msg_handler

# Read a string.  If prompt_notify should be a proc; it is called with
# "1" as argument when the prompt is printed and no other text is
# present, and "0" when the user has entered some text.
proc getstr {prompt {prompt_notify ""}} {
    nilkom_page sync
    print $prompt
    if {$prompt_notify != ""} {
	eval "$prompt_notify 0"
	set notify_state "0"
    } else {
	set notify_state "not"
    }
    set res ""
    while {[set char [nilkom_input get_key]] != 13 && $char != 10} {
	if {$notify_state == "0" && $char != 8 && $char != 127} {
	    eval "$prompt_notify 1"
	    set notify_state "1"
	}
	nilkom_page acknowledge
	switch $char {
	    4 -
	    3
	    {
		clean_exit
	    }
	    18 -
	    12
	    {
		print $prompt
		print $res
	    }
	    21
	    {
		print "\n"
		set res ""
	    }
	    8 -
	    127
	    {
		set len [string length $res]

		if {$len == 0} {
		    nilkom_page beep
		} else {
		    nilkom_page remove [string index $res [expr $len - 1]]
		    set res [string range $res 0 [expr $len - 2]]
		    if {$notify_state == "1" && [string length $res] == 0} {
			eval "$prompt_notify 0"
			set notify_state "0"
		    }
		}
	    }
	    default
	    {
		print [format "%c" $char]
		append res [format "%c" $char]
	    }
	}
    }
    print "\n"
    return $res
}

proc silentgetstr {prompt} {
    nilkom_page sync
    print $prompt
    set res ""
    while {[set char [nilkom_input get_key]] != 13 && $char != 10} {
	switch $char {
	    4 -
	    3
	    {
		clean_exit
	    }
	    18 -
	    12
	    {
		print $prompt
	    }
	    21
	    {
		set res ""
	    }
	    8 -
	    127
	    {
		set len [string length $res]

		if {$len == 0} {
		    nilkom_page beep
		} else {
		    set res [string range $res 0 [expr $len - 2]]
		}
	    }
	    default
	    {
		append res [format "%c" $char]
	    }
	}
    }
    print "\n"
    return $res
}

proc login {} {
    global env
    global prefetch_state

    set komrcname [format "%s/.nilkomrc" $env(HOME)]
    if {[file exists $komrcname]} {
	set komfile [open $komrcname r]
	gets $komfile persno
	gets $komfile passwd

	print "Loggar in som [kom_conference name $persno]...\n"
	if {! [kom_session login $persno $passwd]} {
	    error "Inloggningen misslyckades. Kontrollera $komrcname"
	}
    } else {
	set foo 0
	while {$foo == 0} {
	    set persno [select_unique_person "Ditt namn? " "" \
			    {set persno [maybe_create_new_person $name]}]
	    if {$persno == ""} {
		error "Hejd�."
	    }
	    if {[llength $persno] == 1} {
		print "[kom_conference name $persno]\n"
		set passwd [silentgetstr "Ditt l�senord? "]
		if {[kom_session login $persno $passwd]} {
		    set foo 1
		} else {
		    print "Fel l�sen.  F�rs�k igen.\n"
		}
	    } else {
		# A created person auto-logins.
		set foo 1
	    }
	}
    }
    set prefetch_state 0
}

# Return the number of a person of the users' choice.
# PROMPT is a prompt to use if more feedback from the user is needed.
# NAME is the users initial attempt, or the empty string.
# WHEN_NOTFOUND, if set, should be a TCL statement that will be
# executed when no match was found.   login uses this to offer
# creation of new persons.
proc select_unique_person {prompt name {when_notfound {}}} {
    set persno ""
    while {$persno == ""} {
	if {$name == ""} {
	    set name [getstr $prompt]
	}
	if {$name == ""} {
	    return ""
	}
	if {[regexp {^p[ \t]+([0-9]+)$} $name dummy persno]} {
	    safe_eval {kom_person number $persno} {
		{
		    {KOM BADPERS}
		    {
			print "Person nummer $persno finns inte\n"
			set persno ""
			set name ""
		    }
		}
	    }
	} else {
	    set matches [filter-persons [kom_session lookup_name $name]]
	    if {[llength $matches] == 0} {
		# No match.
		set persno ""
		if {$when_notfound == ""} {
		    print "Det finns ingen person som matchar $name\n"
		    return ""
		} else {
		    eval $when_notfound
		    set name ""
		}
	    } elseif {[llength $matches] == 1} {
		# A single match.
		set persno [microconf confno [lindex $matches 0]]
	    } else {
		# Many matches. Offer a menu.
		set persno [select_a_match $matches]
	    }
	}
    }
    return $persno
}

proc select_a_match {matches} {
    print "Du kan mena n�gon av f�ljande:\n"
    set x 1
    foreach match $matches {
	set res [print "$x: [kom_conference name [microconf confno $match]]\n"]
	if {$res == "terminate"} {
	    return ""
	}
	incr x
    }
    incr x -1
    set num [getstr "V�lj nummer 1-$x: "]
    if {[regexp {^[0-9]+$} $num] && $num > 0 && $num <= $x} {
	incr num -1
	return [microconf confno [lindex $matches $num]]
    } else {
	return ""
    }
}

proc maybe_create_new_person {name} {
    print "Det verkar vara f�rsta g�ngen du anv�nder LysKOM; V�lkommen!\n"
    print "Kontrollera att ditt namn, $name, �r r�ttstavat.\n"
    print "En anv�ndaridentitet kommer att skapas �t dig.\n"
    print "Ange ett personligt l�senord.  "
    print "Tryck RETUR f�r att avbryta skapandet.\n"
    set pass1 ""
    set pass2 "x"
    while {$pass1 != $pass2} {
	set pass1 [silentgetstr "L�sen? "]
	if {$pass1 == ""} {
	    return ""
	}    
	set pass2 [silentgetstr "Repetera ditt l�senord f�r kontroll: "]
	if {$pass2 == ""} {
	    return ""
	}
	if {$pass1 != $pass2} {
	    print "Du skrev inte in samma l�senord.  F�rs�k igen, "
	    print "eller tryck RETUR f�r att avbryta.\n"
	}
    }
    set pno [kom_session create_person $name $pass1]
    print "$name �r nu en registrerad anv�ndare.\n"
    print "Ditt personnummer �r $pno (fast det beh�ver du inte komma ih�g)\n"
    kom_session login $pno $pass1
    return [list new-person $pno]
}

set quit_flag 0
set to_mark_as_read {}
set review_list {}
set last_message_reply_to ""
set message_queue {}
# prompt_state is the empty string if no idling is requested, or one of
# the four characters "-/|\\" when idling is requested.  Idling is only
# activated if {$idling_disabled == 0 && $prompt_state != ""}.
set prompt_state ""
set idling_disabled 1

nilkom_idleproc set timeout
login
kom_session set_doing "K�r version ${nilkom_version} av nilkom"

proc nilkom_toploop {} {
    global quit_flag

    while {$quit_flag == 0} {
	do_command [getstr [calculate_prompt] \
		"global idling_disabled; set idling_disabled "]
    }
    return 0
}

nilkom_toploop

