# TCL code for editing messages in nilkom.
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# ----------------------------------------------------------------
# Global variables (local to this file):
#
#  textmass	 -  The text the user is entering
#  create_text_recips - Recipients of the text
#  create_text_comm_to - Lists of texts this text is a comment to
#  create_text_footn_to - Lists of texts this text is a footnote to
#

set textmass ""
set create_text_recips {}
set create_text_comm_to {}
set create_text_footn_to {}

set create_text_intr_cntr 0

proc start_comment {parent} {
    global create_text_recips
    global create_text_comm_to
    global create_text_footn_to
    global textmass

    set create_text_recips {}
    set create_text_comm_to $parent
    set create_text_footn_to {}
    set textmass [kom_text subject $parent]

    print [format "Kommentera text $parent av %s\n" \
		[kom_conference name [kom_text author $parent]]]
    set i 0
    while {$i<[kom_text num_recipients $parent]} {
	set r [kom_text recipient $parent $i]
	if {! [recipient is_cc $r]} {
	    append hline [format_recipient $r]
	    lappend create_text_recips $r
	}
        incr i 1
    }

    print "Skriv in din kommentar.  Avsluta med ESC eller EOF.\n"
    print "\n�rende: $textmass"
    text_editor
}

proc start_write_letter {name} {
    global create_text_recips
    global create_text_comm_to
    global create_text_footn_to
    global textmass

    set confno [select_unique_any "Vem vill du skriva brev till? " $name]
    if {$confno == ""} {
	print "Du vill visst inte skicka n�got brev.\n"
	return
    }

    set create_text_recips [list \
			     [list $confno \
				  0 0 0 0 0 0 0] \
			     [list [lindex [kom_session who_am_i] 0] \
				  0 0 0 0 0 0 0]]
    set create_text_comm_to {}
    set create_text_footn_to {}
    set textmass ""

    print [format "Skriva personligt brev till %s\n" \
	       [kom_conference name $confno]]

    print "Skriv in din kommentar.  Avsluta med ESC eller EOF.\n"
    print "\n�rende: "
    text_editor
}

proc start_personal_answer {parent} {
    global create_text_recips
    global create_text_comm_to
    global create_text_footn_to
    global textmass

    set create_text_recips [list \
			     [list [kom_text author $parent] \
				      0 0 0 0 0 0 0] \
			     [list [lindex [kom_session who_am_i] 0] \
				  0 0 0 0 0 0 0]]
    set create_text_comm_to $parent
    set create_text_footn_to {}
    set textmass [kom_text subject $parent]

    print [format "Svara p� text $parent av %s\n" \
		[kom_conference name [kom_text author $parent]]]

    print "Skriv in din kommentar.  Avsluta med ESC eller EOF.\n"
    print "\n�rende: $textmass"
    text_editor
}

proc start_write_text {} {
    global create_text_recips
    global create_text_comm_to
    global create_text_footn_to
    global textmass

    set create_text_recips [list [list [kom_unread current_conf] \
				      0 0 0 0 0 0 0]]
    set create_text_comm_to {}
    set create_text_footn_to {}
    set textmass {}

    print "Skriv in din text.  Avsluta med ESC eller EOF.\n"
    print "\n�rende: $textmass"
    text_editor
}

proc create_text_add_recipient {name} {
    global create_text_recips

    set recip [select_unique_any "Vart vill du skicka texten du skriver? " \
		   $name]
    if {$recip == ""} {
	print "Ingen ytterligare mottagare adderad.\n"
    } else {
	lappend create_text_recips [list $recip 0 0 0 0 0 0 0]
	print "Mottagare: [kom_conference name $recip]\n"
    }
}

proc create_text_add_cc_recipient {name} {
    global create_text_recips

    set recip [select_unique_any "Vart vill du skicka en extra kopia? " \
		   $name]
    if {$recip == ""} {
	print "Ingen extra kopie-mottagare adderad.\n"
    } else {
	lappend create_text_recips [list $recip 1 0 0 0 0 0 0]
	print "Extra kopiemottagare: [kom_conference name $recip]\n"
    }
}

# FIXME: Ska subtrahera senast l�sta inl�gg om man inte skriver
# n�got inl�gg.
proc subtract_recipient {name} {
    global create_text_recips

    set confno [select_unique_any \
		   "Vilket m�te skall inte l�ngre vara mottagare? " $name]
    if {$confno == ""} {
	print "Ingen mottagare subtraherad.\n"
    } else {
	set new {}
	set found 0
	foreach recip $create_text_recips {
	    if {[recipient rec_no $recip] == $confno} {
		set found 1
	    } else {
		lappend new $recip
	    }
	}
	if {$found} {
	    set create_text_recips $new
	    print "[kom_conference name $confno] subtraherad.\n"
	} else {
	    print "[kom_conference name $confno] var inte mottagare\n
	    print "till den text du editerar.\n"
	}
    }
}

proc create_text_add_comment {tno} {
    global create_text_comm_to

    lappend create_text_comm_to $tno
    print "Kommentar till text $tno\n"
}

proc create_text_add_footnote {tno} {
    global create_text_footn_to

    lappend create_text_footn_to $tno
    print "Fotnot till text $tno\n"
}

proc create_text_resume_edit {} {
    create_text_redisplay_line
    text_editor
}

proc create_text_redisplay {} {
    global create_text_recips
    global create_text_comm_to
    global create_text_footn_to
    global textmass

    print "\n"

    foreach recip $create_text_recips {
	print [format_recipient $recip]
    }
    foreach comment $create_text_comm_to {
	print [format "Kommentar till text $comment av %s\n" \
		[kom_conference name [kom_text author $comment]]]
    } 
    foreach footn $create_text_footn_to {
	print [format "Fotnot till text $footn av %s\n" \
		[kom_conference name [kom_text author $footn]]]
    } 
    print "�rende: $textmass"
}

proc create_text_redisplay_line {} {
    global textmass

    set bol [string last "\n" "$textmass"]
    if {$bol == -1} {
	print "\n�rende: "
	set bol 0
    }

    print [string range "$textmass" $bol end]
}

proc create_text_delete_line {} {
    global textmass

    set bol [string last "\n" $textmass]
    if {$bol == -1} {
	set bol 1
    }

    set textmass [string range $textmass 0 [expr $bol - 1]]
}

proc throw_text {} {
    global create_text_recips
    global create_text_comm_to
    global create_text_footn_to
    global textmass

    set create_text_recips {}
    set create_text_comm_to {}
    set create_text_footn_to {}
    set textmass {}
    print "Texten kastad.\n"
}

proc enter_text {} {
    global create_text_recips
    global create_text_comm_to
    global create_text_footn_to
    global textmass

    set rcpts {}
    foreach r $create_text_recips {
	lappend rcpts [recipient rec_no $r]
    }

    set tno [kom_session create_text $textmass \
		 $rcpts $create_text_comm_to $create_text_footn_to]
    print [format "Text %d skapad\n" $tno]
    kom_text mark_as_read $tno
}

proc text_editor {} {
    global textmass
    global create_text_inter_cntr
    global nilkom_itranslate

    set litnext 0

    while {1} {
	set n [nilkom_input get_key]
	nilkom_page acknowledge
	if {$n != 3} {
	    set create_text_inter_cntr 0
	}

	if {$n == 13} {
	    set n 10
	}

	if {$litnext == 1} {
	    set litnext 0
	    set char [format "%c" $n]
	    print $char
	    append textmass $char
	    continue
	}
	
	switch $n {
	    3
	    {
		if {$create_text_inter_cntr > 0} {
		    print "\n"
		    enter_text
		    return
		} else {
		    print "^C\n(Interrupt -- one more to enter text)\n"
		    incr create_text_inter_cntr
		}
	    }
	    4
	    {
		print "\n"
		enter_text
		return
	    }
	    12
	    {
		print "\n"
	    	create_text_redisplay
	    }
	    27
	    {
		print "\nGe kommandot  skicka  f�r att skicka in texten.\n"
		print "Ge kommandot  bort    f�r att kasta bort den.\n"
		print "(Du kan anv�nda ctrl-D f�r att skicka in texten\n"
		print "direkt medan du skriver den.)\n"
		return
	    }
	    8
	    -
	    127
	    {
		set len [string length $textmass]

		if {$len == 0} {
		    nilkom_page beep
		} else {
		    set lastchar [string index $textmass [expr $len - 1]]
		    set textmass [string range $textmass 0 [expr $len - 2]]
    		    if {[string compare $lastchar "\n"] == 0} {
			create_text_redisplay_line
		    } else {
		        nilkom_page remove $lastchar
		    }
		}
	    }
	    18
	    {create_text_redisplay_line}
	    21
	    {
		create_text_delete_line
		create_text_redisplay_line
	    }
	    22
	    {
		set litnext 1
	    }
	    default
	    {
		set char [format "%c" $n]
		if {$nilkom_itranslate == 1} {
		    set char [isoify $char]
		}
		print $char
		append textmass $char
	    }
	}
    }
}
