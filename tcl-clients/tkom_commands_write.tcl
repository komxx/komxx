# Commands found under the "Write" menu in tkom
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# ================================================================


proc get_window_name {name} {
    set number 1

    while {[info commands "$name-#$number"] != ""} {
        incr number
    }

    return "$name-#$number"
}


# ================================================================
#                     Command: Comment text


proc comment_text_command {textno} {
    if {![start_of_command comment_text_command]} {
	return
    }

    set recips {}
    for {set i 0} {$i < [kom_text num_recipients $textno]} {incr i} {
#	append hline [format_recipient [kom_text recipient $textno $i]]
	lappend recips [recipient rec_no [kom_text recipient $textno $i]]
    }

    write_window [get_window_name .write] "Kommentar till text $textno" \
	-recipients $recips \
	-comment_to $textno \
	-subject "[kom_text subject $textno]\n"

    end_of_command
}


# ================================================================
#                   Command: Write text


proc write_text_command {} {
    if {![start_of_command write_text_command]} {
	return
    }

    set recip [kom_unread current_conf]
    if {$recip < 1} {
	show_message "Du �r inte n�rvarande i n�got m�te.\n"
	beep
    } else {
	write_window [get_window_name .write] \
	    "Inl�gg i [kom_conference name $recip]"\
	    -recipients $recip
    }

    end_of_command
}


# ================================================================
