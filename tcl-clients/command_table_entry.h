// nilkom TCL command table entries.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#ifndef KOMXX_COMMAND_TABLE_ENTRY_H
#define KOMXX_COMMAND_TABLE_ENTRY_H

#include "LyStr.h"

class Command_table_entry {
public:
  Command_table_entry();
  Command_table_entry(const Command_table_entry &);
  Command_table_entry & operator = (const Command_table_entry &);
  Command_table_entry(const LyStr &cmd_template,
		      const LyStr &cmd_pretty,
		      const LyStr &tcl_cmd);
  void try_match(LyStr user_try, 
		 LyStr &tcl_command, LyStr &pretty, int &penalty) const;
  ~Command_table_entry();

  enum { MAXARGS = 4 };
  enum errcode {
    NOERR = 0,
    EMPTY_USER_CMD,
    EMPTY_TCL_CMD,
    BAD_WILDCARD,
    BAD_TCL_WILDCARD,
    WILDCARD_MISMATCH,
    BAD_USER_TEMPLATE,
    UNINITIALIZED
  };
  
  errcode status();
  
private:
  enum errcode err;
  LyStr user_template;
  LyStr tcl_template;
  LyStr pretty;
};

#endif
