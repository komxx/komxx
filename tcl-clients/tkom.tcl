# ================================================================


proc usage {{exitstatus 0}} {
    puts {Användning: tkom [server-adress [portnummer]]}
    exit $exitstatus
}


proc tkerror {dummy} {
    global errorInfo
    puts $errorInfo
    puts $dummy
}

proc stress {} {while 1 {default_action ;update}}

if {[lsearch [info vars] tk_library] == -1} {
    set tk_library /usr/local/lib/tk
    puts "tk_library reset to /usr/local/lib/tk"
}

# ================================================================

set romanfont {*-Courier-Medium-R-Normal-*-120-*}
set boldfont  {*-Courier-Bold-R-Normal-*-120-*}
set bigfont   {*-Courier-Bold-R-Normal-*-150-*}

# ================================================================


# Construct the main window and wait for it to show up on the screen.
set tkom_priv(commands_are_allowed) 0
main_window
focus default .
tkwait visibility .mesgf.message

# Init global variables and show current status.
set this_person 0
set to_mark_as_read {}
set current_text  0
set current_conf -2
show_current_conf $current_conf
update


# ================================================================
#              Now it's time to get things started...


#
# 1. Connect to the KOM server
#
if {$argc > 0} {set KOM_SERVER [lindex $argv 0]}
if {$argc > 1} {set KOM_PORT [lindex $argv 1]}
if {$argc > 2} {usage 1}

show_message "Kopplar upp mot $KOM_SERVER..."
update
connect $KOM_SERVER $KOM_PORT tkom $tkom_version
show_message "klart.\n"

set current_conf -1
show_current_conf $current_conf
update

#
# 2. Log in
#
set komrcname [format "%s/.komrc" $env(HOME)]
if {[file exists $komrcname]} {
    set komfile [open $komrcname r]
    gets $komfile persno
    gets $komfile passwd
    
    show_message "Loggar in som [kom_conference name $persno]..."
    update
    if {[kom_session login $persno $passwd]} {
        show_message "klart.\n"
        set this_person $persno
        set current_conf 0
        show_current_conf $current_conf
    } else {
        show_message "det gick inte.\n"
	set tkom_priv(commands_are_allowed) 1
	login_command
    }
} else {
    set tkom_priv(commands_are_allowed) 1
    login_command
}
set passwd ""

#
# 3. Do some things, such as show the default action, before we give 
#    the control to the user.
#
set tkom_priv(commands_are_allowed) 1
do_things_before_waiting
