// fixfd: Set stdin in blocking mode, in case nilkom forgets to restore it.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

#include <config.h>

#include <cstdio>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

int
main(int,
     char **argv)
{
  int res;
  
  /* Set non blocking write mode */
  if ((res = fcntl(0, F_GETFL, 0)) == -1)
  {
    fprintf(stderr, "%s: ", argv[0]);
    perror("fcntl(F_GETFL)");
    return 1;
  }
  
#ifdef FNDELAY
  if (fcntl(0, F_SETFL, res & ~FNDELAY) == -1)
#else
  if (fcntl(0, F_SETFL, res & ~O_NONBLOCK) == -1)
#endif   
  {
    fprintf(stderr, "%s: ", argv[0]);
    perror("fcntl(F_SETFL)");
    return 1;
  }
  return 0;
}
