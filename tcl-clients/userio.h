// Declarations of functions in userio.cc
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#ifndef KOMXX_USERIO_H
#define KOMXX_USERIO_H

extern "C" {
#include "tcl.h"
}
#include "user-input.h"
#include "nilkom-paginator.h"

void define_userio_commands(Tcl_Interp *interp,
			    User_input *inchan,
			    Nilkom_paginator *outchan);

#endif
