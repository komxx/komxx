# Commands found under the "Session" menu in tkom
#
# Copyright (C) 1994  Per Cederqvist and Inge Wallin
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


# ================================================================
#                         Command: Login


itcl_class login_window {
    inherit butwin

    constructor {name password config} {
	butwin::constructor -buttons \
	    [list [list "Logga in" "$this _login"] \
		 [list "Avbryt" "$this _cancel_login"]]

	wm title $this "B�rja med ny person"

	#
        # Build the contents of the login window
        #
        listbox $this.namelist -relief ridge -bd 2 -setgrid true
        lblentry $this.nameentry -lbltext "Namn:"
        lblentry $this.passwdentry -lbltext "L�sen:"
        pack $this.namelist $this.nameentry $this.passwdentry \
	    -side top -fill both

	$this.nameentry   bind <Return>   "$this _set_focus passwdentry"
        $this.nameentry   bind <Tab>      "$this _set_focus passwdentry"
        $this.passwdentry bind <Tab>      "$this _set_focus nameentry"
        $this.passwdentry bind <Control-KeyPress> "$this _handle_passwd %A"
        $this.passwdentry bind <KeyPress> "$this _handle_passwd %A"
        bind $this.namelist <Double-1>    "$this _click_on_name %y"

	$this.nameentry put $name
	$this.nameentry focus

	#
	#  Explicitly handle config's that may have been ignored earlier
	#
	foreach attr $config {
	    configure -$attr [set $attr]
	}
    }

    destructor {
	$this.nameentry delete
	$this.passwdentry delete
    }

    method configure {config} {}

    method _set_focus {w} {
	$this.$w focus
    }

    method _handle_passwd {key} {
        if {[string match "\r" $key]} {
            # <Return>
	    activate_button 0
            return

        } elseif {[string match "\010" $key] || [string match "\177" $key]} {
            # <Delete> or <Back space>
            set passwd_string [string range $passwd_string 0 \
                 [expr [string length $passwd_string]-2]]
            $this.passwdentry put ""
            return

        } elseif {[string match "\025" $key]} {
            # Control-u
            set passwd_string {}
            $this.passwdentry put ""
            return

        } elseif {[string match "\t" $key]} {
            # <Tab>
            #puts "You pressed <tab>"; return
        }
     
        if {[string compare $key " "] > 0} {
            set passwd_string "$passwd_string$key"
            $this.passwdentry insert end #
        } 
    }

    method _click_on_name {y} {
	set index [$this.namelist nearest $y]

        if {$namelist_contains_names} {
            $this.nameentry put [$this.namelist get $index]
        }
    }

    method _set_list {names micro_confs} {
	$this.namelist delete 0 end
	foreach name $names {
	    $this.namelist insert end $name
	}

	set micro_confs_list $micro_confs
	if {$micro_confs == {}} {
	    set namelist_contains_names 0
	} else {
	    set namelist_contains_names 1
	}
    }

    method _login {} {
        # Check to see how many names matched the string
        set matches [filter-persons \
			 [kom_session lookup_name [$this.nameentry get]]]
        _set_list {} {}

        if {[llength $matches] == 0} {
	    # No match
	    _set_list {"Ingen tr�ff p� namnet"} {}

        } elseif {[llength $matches] == 1} {
            # Only 1 name matched ==> Try to log in.
            set persno [microconf confno [lindex $matches 0]]
	    if {[do_login $persno $passwd_string]} {
                $this delete
            } else {
	        _set_list {"Felaktigt l�senord"} {}
            }

        } elseif {[llength $matches] > 1} {
	    # Many matches ==> Show them in the listbox
	    set person_names   {}
	    foreach match $matches {
		lappend person_names [kom_conference name \
					  [microconf confno $match]]
	    }
	    _set_list $person_names $matches
	}
    }

    method _cancel_login {} {
	show_message "Inloggningen avbruten!\n"
	$this delete
    }

    protected passwd_string           ""
    protected namelist_contains_names 0
    protected micro_confs_list
}


proc do_login {persno password} {
    global this_person
    global current_conf
    global tkom_version

    if {[kom_session login $persno $password]} {
	set this_person $persno
	show_message "Du �r nu inloggad som [kom_conference name $persno]\n"
	set current_conf 0
	kom_session set_doing "K�r version ${tkom_version} av tkom"
	return 1
    }

    return 0
}


proc login_command {{name {}} {password {}}} {
    if {![start_of_command login_command]} {
	return
    }

    login_window .login $name $password
    tkwait window .login

    end_of_command
}
