// Decoding user commands.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.


#include <config.h>

#include <map>

#include <sstream>
#include <cassert>
#include <cstdlib>
#include <cctype>

#include "tcl-support.h"
#include "cmd-decode.h"
#include "LyStr.h"
#include "Tcl_LyStr.h"
#include "tcl-auto-free.h"
#include "command_table.h"
#include "enomem.h"
#include "container.h"
 
  
enum Kom_cmd_parser {
    CMDP_DEFINE = 1, CMDP_MATCH
    };

static Tcl_Args kom_cmd_parser_options[] = {
    {"define",       CMDP_DEFINE, 1, 0, {TCLARG_STRING}},
    {"match",        CMDP_MATCH,  2, 0, {TCLARG_STRING, TCLARG_STRING}},

    {NULL, 0, 0, 0, {}}
};

static std::map<int, Command_table*> cmd_tables;

extern "C" int
tcl_cmd_parser(ClientData /*clientData*/,
	       Tcl_Interp *interp, 
	       int argc, 
	       const char *argv[])
{
  Parse_tcl_args args(interp, kom_cmd_parser_options, argc, argv);
  int ix;

  switch(args.option())
    {
    case CMDP_DEFINE:
      {
	int ac;
	tcl_auto_free av;
	int res;

	res = Tcl_SplitList(interp, args.stringarg(0), &ac, &av.p);
	if (res != TCL_OK)
	  {
	    return res;		// +++ errorCode? errorInfo?
	  }
	
	Command_table *cmd_tab = new Command_table;
	
	for (ix = 0; ix < ac; ix++)
	  {
	    int ac2;
	    tcl_auto_free av2;
	    
	    res = Tcl_SplitList(interp, av.p[ix], &ac2, &av2.p);
	    if (res != TCL_OK)
	      {
		return res;		// +++ errorCode? errorInfo?
	      }
	    
	    Command_table_entry::errcode code;
	    
	    switch (ac2)
	      {
	      case 2:
		// The pretty-printing command defaults to the 
		// command template.
		code = cmd_tab->add_entry(av2.p[0], av2.p[0], av2.p[1]);
		break;
	      case 3:
		code = cmd_tab->add_entry(av2.p[0], av2.p[2], av2.p[1]);
		break;
	      default:
		{
		  LyStr errmsg("\
malformed grammar to command 'command_parser define'.  Each entry in the\n\
grammar should be a list like {user_template tcl_template user_pretty},\n\
where user_pretty is optional.  Entry number ");
		  errmsg << ix << " contains " << ac2 << "\n\
list elements: " << av.p[ix];
		  set_tcl_error(interp, errmsg, "KOM", "BADARGS");
		}
		return TCL_ERROR;
	      }
	    
	    char *errmsg = NULL;
	    
	    switch (code)
	      {
	      case Command_table_entry::NOERR:
		break;
	      case Command_table_entry::EMPTY_USER_CMD:
		errmsg = "Empty user command";
		break;
	      case Command_table_entry::EMPTY_TCL_CMD:
		errmsg = "Empty tcl command";
		break;
	      case Command_table_entry::BAD_WILDCARD:
		errmsg = "Bad wildcard in user template";
		break;
	      case Command_table_entry::BAD_TCL_WILDCARD:
		errmsg = "Bad wildcard in TCL template";
		break;
	      case Command_table_entry::WILDCARD_MISMATCH:
		errmsg = "Too few wildcards in user template";
		break;
	      case Command_table_entry::BAD_USER_TEMPLATE:
		errmsg = "Mal-formed user template";
		break;
	      case Command_table_entry::UNINITIALIZED:
		abort();
	      default:

		return TCL_ERROR;
	      }
	    if (code != Command_table_entry::NOERR)
	      {
		assert (errmsg != NULL);
		LyStr  err("command_parser define: command table entry ");
		err << ix << "is bogus: " << errmsg << '\n' << av.p[ix];

		set_tcl_error(interp, err, "KOM", "BADARGS");
		return TCL_ERROR;
	      }
	    assert(errmsg == NULL);
	  }
	
	for (ix = 0; cmd_tables.find(ix) != cmd_tables.end(); ix++)
	  ;
	cmd_tables[ix] = cmd_tab;
	sprintf(interp->result, "cmdtab_%d", ix);
      }
      break;
      
    case CMDP_MATCH:
      {
	const char *s = args.stringarg(0);
	if (strncmp(s, "cmdtab_", 7) != 0 || !isdigit(s[7]))
	  {
	    LyStr errmsg;
	    errmsg << "bad command table handle " << s;
	    set_tcl_error(interp, errmsg, "KOM", "BADARGS");
	    return TCL_ERROR;
	  }

	s += 7;
	char  *end = NULL;
	ix = strtol(s, &end, 10);
	assert(end != NULL);
	assert(end > s);
	if (*end != '\0')
	  {
	    LyStr errmsg;
	    errmsg << "bad command table handle " << args.stringarg(0);
	    set_tcl_error(interp, errmsg, "KOM", "BADARGS");
	    return TCL_ERROR;
	  }
	if (cmd_tables.find(ix) == cmd_tables.end())
	  {
	    LyStr errmsg;
	    errmsg << "undefined command table handle " << args.stringarg(0);
	    set_tcl_error(interp, errmsg, "KOM", "BADARGS");
	    return TCL_ERROR;
	  }

	LyStr       tcl_cmd;
	Tcl_LyStr   alts;
	const char *res;

	// This variable could be deleted, but gcc 2.6.0 seems not to
	// reclaim the temporary if it is eliminated from the source code.
	LyStr FIXME(args.stringarg(1));
	switch(cmd_tables[ix]->decode_input(FIXME, 
					    tcl_cmd, alts))
	  {
	  case Command_table::NOMATCH:
	    sprintf(interp->result, "0 NOMATCH");
	    return TCL_OK;
	  case Command_table::UNIQUE:
	    sprintf(interp->result, "1");
	    res = tcl_cmd.view_block();
	    ENOMEM_P(res);
	    Tcl_AppendElement(interp, res);
	    tcl_cmd.view_done(res);
	    break;
	  case Command_table::AMBIGUOUS:
	    strcpy(interp->result, "0 AMBIGUOUS");
	    res = alts.view_block();
	    ENOMEM_P(res);
	    Tcl_AppendElement(interp, res);
	    alts.view_done(res);
	    return TCL_OK;
	  }
      }
      break;
      
    default:
      assert(args.option() < 0);
      return TCL_ERROR;
    }

  return TCL_OK;
}

extern "C" void
tcl_delete_command_parser(ClientData /*clientData*/)
{
    delete_contents_map(cmd_tables);
}

void define_cmd_decode_commands(Tcl_Interp *interp)
{
  Tcl_CreateCommand(interp, "command_parser",   tcl_cmd_parser,  
		    NULL, tcl_delete_command_parser);
}
