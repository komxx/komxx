// The implementation of the TCL kom commands.
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//


// Note: this has nothing at all to do with The Common Link, which
// is another KOM-clone.

// Note: the TCL interface is going away.  You are strongly encouraged
// to use the Python interface instead.

#include <config.h>

#include <cassert>
#include <cstdio>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <sstream>
#include <iostream>

#include "tcl-support.h"	// TCL support files
#include "tcl-glue.h"
#include "tcl-auto-free.h"

#include "session.h"		// High level KOM objects
#include "person.h"
#include "text.h"
#include "conference.h"
#include "maps.h"
#include "read-order.h"
#include "unread-confs.h"
#include "error-codes.h"

#include "text-mapping.h"	// Lower level KOM objects
#include "connection.h"
#include "conn_io_isc.h"
#include "uplink.h"
#include "micro-conf.h"
#include "map-cache.h"
#include "kom-error.h"
#include "mark.h"
#include "async_text_message.h"
#include "async_text_created.h"
#include "async.h"
#include "aux-item.h"

// Questions.  These will be deleted later, and we will use the 
// higher level interface instead.

#include "delete_conf_q.h"
#include "enable_q.h"		
#include "create_text_old_q.h"
#include "set_client_version_q.h"

#include "Tcl_LyStr.h"

// ================================================================
//                         Global variables


// isc_foo, conn_foo and session are set by tcl_connect() and deleted
// when that command is deleted.
static Conn_io_ISC * isc_foo = NULL;
static Connection  * conn_foo = NULL;
static Session     * session = NULL;

// unread_confs is set (and sometimes deleted) by [kom_session login].
// It is deleted when tcl_connect() is deleted.
static Unread_confs   * unread_confs = NULL;

// There should be a cleaner way... I do not like this global
// variable, but until we fix multiple sessions, it will remain.
// This variable is needed by nilkom, since it uses ISC to read the
// keyboard.  It is initialized and destroyed by tcl-main.cc and used by
// tcl_connect.
IscMaster          * mcb_for_isc = NULL;

// Handles returned by the async_dispatcher.
// FIXME: There is no cleanup for these.
// FIXME: These should be present in a struct, which should be passed
// as clientData.
static Async_handler_tag<Async_text_message> async_text_message_handle;
static Async_handler_tag<Async_text_created> async_text_created_handle;
static Tcl_Interp *the_interp = NULL;

// ================================================================


static std::ostream &
output_time(std::ostream &os, 
	    struct tm stm)
{
    os << stm.tm_sec << ' ' << stm.tm_min << ' ' << stm.tm_hour << ' ' 
       << stm.tm_mday << ' ' << stm.tm_mon << ' ' << stm.tm_year << ' '
       << stm.tm_wday << ' ' << stm.tm_yday << ' ' << stm.tm_isdst;
    return os;
}


// Format of a sender:
//  recipient is_carbon_copy local_no has_rec_time rec_time sender
//  has_sent_at sent_at
static std::ostream &
output_recipient(std::ostream &os,
		 const Recipient *r)
{
    os << r->recipient() << ' ' << (r->is_carbon_copy() ? '1' : '0') << ' ' 
       << r->local_no() << ' ' << (r->has_rec_time() ? '1' : '0') << ' ';
    if (r->has_rec_time())
    {
	os << '{';
	output_time(os, r->rec_time());
	os << "} ";
    }
    else
	os << "{} ";
    os << r->sender() << ' ' << (r->has_sent_at() ? '1' : '0');
    if (r->has_sent_at())
    {
	os << " {";
	output_time(os, r->sent_at());
	os << '}';
    }
    else
	os << " {}";
    return os;
}

static std::ostream &
output_uplink(std::ostream &os,
	      const Uplink *u)
{
    os << (u->is_footnote() ? '1' : '0') << ' ' << u->parent() << ' ' 
       << u->sender() << ' ' << (u->sent_later() ? '1' : '0');
    if (u->sent_later())
    {
	os << " {";
	output_time(os, u->sent_at());
	os << '}';
    }
    else
	os << " {}";
    return os;
}


static int
tcl_connect(ClientData /*clientData*/,
	    Tcl_Interp *interp, 
	    int argc, 
	    const char *argv[])
{
    if (argc != 5)
    {
	Tcl_SetResult(interp, "connect host port client_name client_version",
		      TCL_STATIC);
	Tcl_SetErrorCode(interp, "KOM", "BADARGS", (char *)NULL);
	return TCL_ERROR;
    }
    assert(isc_foo == NULL);
    assert(conn_foo == NULL);
    assert(session == NULL);
    
    isc_foo = new Conn_io_ISC(argv[1], argv[2], mcb_for_isc);
    conn_foo = new Connection(isc_foo);
    if (conn_foo->connection_failed())
    {
	Tcl_SetResult(interp, "cannot connect to the specified server",
		      TCL_STATIC);
	Tcl_SetErrorCode(interp, "KOM", "NOCONNECT", (char *)NULL);
	return TCL_ERROR;
    }
    session = new Session(conn_foo, argv[3], argv[4]);
    assert(isc_foo != NULL);
    assert(conn_foo != NULL);
    assert(session != NULL);

    return TCL_OK;
}

extern "C" void
tcl_delete_connection(ClientData /*clientData*/)
{
    delete unread_confs;
    unread_confs = NULL;
    delete session;
    session = NULL;
    delete conn_foo;
    conn_foo = NULL;
    delete isc_foo;
    isc_foo = NULL;
}


// ================================================================
//                   TCL command 'kom_session'

enum Kom_session_args {
    SESSION_LOGIN=1,     SESSION_GET_TIME, SESSION_SET_DOING,
    SESSION_LOOKUP_NAME, 
    SESSION_CREATE_TEXT, SESSION_GET_MARKS,
    SESSION_WHO_IS_ON,   SESSION_WHO_AM_I,  SESSION_CREATE_PERSON,
    SESSION_HANDLE_ASYNC, SESSION_SEND_MESSAGE,
    SESSION_REGISTER_ASYNC, SESSION_USER_ACTIVE
};

static Tcl_Args kom_session_options[] = {
    {"login",       SESSION_LOGIN,       2, 1, {TCLARG_INT, TCLARG_STRING, 
						TCLARG_STRING}},
    {"get_time",    SESSION_GET_TIME,    0, 0, {}},
    {"set_doing",   SESSION_SET_DOING,   1, 0, {TCLARG_STRING}},
    {"lookup_name", SESSION_LOOKUP_NAME, 1, 0, {TCLARG_STRING}},
    {"create_text", SESSION_CREATE_TEXT, 1, 3, 
	 {TCLARG_STRING, TCLARG_STRING, TCLARG_STRING, TCLARG_STRING}},
    {"get_marks",   SESSION_GET_MARKS,   0, 0, {}},
    {"who_is_on",   SESSION_WHO_IS_ON,   0, 0, {}},
    {"who_am_i",    SESSION_WHO_AM_I,    0, 0, {}},
    {"create_person", SESSION_CREATE_PERSON, 2, 0,
	 {TCLARG_STRING, TCLARG_STRING}},
    {"handle_async",SESSION_HANDLE_ASYNC, 0, 1, {TCLARG_INT}},
    {"send_message",SESSION_SEND_MESSAGE, 2, 0, {TCLARG_INT, TCLARG_STRING}},
    {"register_async", SESSION_REGISTER_ASYNC, 2, 0,
         {TCLARG_STRING, TCLARG_STRING}},
    {"user_active", SESSION_USER_ACTIVE,  0, 0, {}},
    {NULL, 0, 0, 0, {}}
};

static void
text_message_func(const Async_text_message &msg, LyStr &script)
{
    Tcl_LyStr s(script);
    s << " " << msg.recipient()
      << " " << msg.sender()
      << " ";
    s.append_element(msg.message());

    char *buf = s.malloc_copy();
    Tcl_Eval(the_interp, buf);	// Ignore retval.
    free(buf);
}

static void
text_created_func(const Async_text_created &msg, LyStr &script)
{
    Tcl_LyStr s(script);
    s << " " << msg.text_stat().text_no();

    char *buf = s.malloc_copy();
    Tcl_Eval(the_interp, buf);	// Ignore retval.
    free(buf);
}

extern "C" int
tcl_kom_session(ClientData  /*clientData*/,
		Tcl_Interp *interp, 
		int         argc, 
		const char *argv[])
{
    Status       status;
    std::ostringstream res;
    int tcl_rv;

    Parse_tcl_args args(interp, kom_session_options, argc, argv);
    switch (args.option()) 
    {
      case SESSION_LOGIN:
	if (argc == 5 && strcmp(args.stringarg(1), "invisible") != 0)
	{
	    Tcl_SetErrorCode(interp, "KOM", "BADARGS", (char *)NULL);
	    Tcl_SetResult(interp, "Fifth arg should be invisible.",
			  TCL_STATIC);
	    return TCL_ERROR;
	}

	status = session->login(args.intarg(0), args.stringarg(0), 
			       (argc==5) ? true : false);
	Tcl_SetResult(interp, 
		      const_cast<char*>((status == st_ok) ? "1" : "0"),
		      TCL_STATIC);

	// Initialize unread texts.
	if (unread_confs != NULL)
	    delete unread_confs;
	unread_confs = new Unread_confs(session, args.intarg(0));
	break;
	
    case SESSION_GET_TIME:
	// +++ Error handling???
	output_time(res, session->get_time());
	set_tcl_result(interp, res);
	return TCL_OK;

    case SESSION_SET_DOING:
	{
	    LyStr   doing(args.stringarg(0));
	    Status  status;

	    status = session->change_what_i_am_doing(doing);
	    Tcl_SetResult(interp,
			  const_cast<char*>((status == st_ok) ? "1" : "0"),
			  TCL_STATIC);
	}
        break;

    case SESSION_LOOKUP_NAME:
	{
	    std::vector<Micro_conf>  matches;

	    matches = session->lookup_name(args.stringarg(0));
	    for (std::vector<Micro_conf>::iterator iter = matches.begin();
		 iter < matches.end(); ++iter) {
		std::ostringstream   bufferstr;

		bufferstr << iter->conf_no() << ' ' 
		    << (iter->conf_type().is_protected() ? '1': '0')
		    << (iter->conf_type().is_original()  ? '1': '0')
		    << (iter->conf_type().is_secret()    ? '1': '0')
		    << (iter->conf_type().is_letterbox() ? '1': '0')
		    << '\0';

		Tcl_AppendElement(interp, bufferstr.str().c_str());
	    }
	}
	break;

    case SESSION_CREATE_TEXT:
	{
	    std::vector<Recipient> recipients;
	    std::vector<Text_no> comm_to;
	    std::vector<Text_no> footn_to;
	    int ac;
	    tcl_auto_free av;
	    int ix;
	    long ltmp;

	    switch(argc)
	    {
	    case 6:
		// Footnotes
		tcl_rv = Tcl_SplitList(interp, args.stringarg(3), &ac, &av.p);
		if (tcl_rv != TCL_OK)
		{
		    return tcl_rv;	// +++ errorCode? errorInfo?
		}

		for(ix = 0; ix < ac; ix++)
		{
		    tcl_rv = Tcl_ExprLong(interp, av.p[ix], &ltmp);
		    if (tcl_rv != TCL_OK)
			return tcl_rv;
		    footn_to.push_back(Text_no(ltmp));
		}
		// The pointers and the strings are allocated in a
		// single block by Tcl_SplitList, so a single free()
		// is enough.
		av.free();
		/*FALL THROUGH*/
	    case 5:
		// Comments
		tcl_rv = Tcl_SplitList(interp, args.stringarg(2), &ac, &av.p);
		if (tcl_rv != TCL_OK)
		{
		    return tcl_rv;	// +++ errorCode? errorInfo?
		}

		for(ix = 0; ix < ac; ix++)
		{
		    tcl_rv = Tcl_ExprLong(interp, av.p[ix], &ltmp);
		    if (tcl_rv != TCL_OK)
		    {
			return tcl_rv;
		    }
		    comm_to.push_back(Text_no(ltmp));
		}
		av.free();
		/*FALL THROUGH*/
	    case 4:
		// Recipients
		tcl_rv = Tcl_SplitList(interp, args.stringarg(1), &ac, &av.p);
		if (tcl_rv != TCL_OK)
		{
		    return tcl_rv;	// +++ errorCode? errorInfo?
		}

		for(ix = 0; ix < ac; ix++)
		{
		    int ac2;
		    tcl_auto_free av2;
		    bool carbon = false;

		    tcl_rv = Tcl_SplitList(interp, av.p[ix], &ac2, &av2.p);
		    if (tcl_rv != TCL_OK)
		    {
			return tcl_rv;	// +++ errorCode? errorInfo?
		    }

		    switch (ac2)
		    {
		    case 2:
			if (strcmp(av2.p[1], "carbon_copy")!=0)
			{
			    Tcl_SetErrorCode(interp, "KOM", "BADARGS", 
					     (char *)NULL);
			    Tcl_SetResult(interp, "expected carbon_copy",
					  TCL_STATIC);
			    return TCL_ERROR;
			}
			carbon = true;
			/*FALL THROUGH*/
		    case 1:
			{
			    tcl_rv = Tcl_ExprLong(interp, av2.p[0], &ltmp);
			    if (tcl_rv != TCL_OK)
			    {
				return tcl_rv;
			    }
			}

			recipients.push_back(Recipient(ltmp, carbon));
			break;
		    default:
			Tcl_SetErrorCode(interp, "KOM", "BADARGS", 
					 (char *)NULL);
			Tcl_SetResult(interp, "bad recipient list", 
				      TCL_STATIC);
			return TCL_ERROR;
		    }
		}
		av.free();
		/*FALL THROUGH*/
	    case 3:
		// The text
		{
		    create_text_old_question q(conn_foo, args.stringarg(0),
					       recipients, comm_to, footn_to);
		    if (q.receive() == st_error)
		    {
			Tcl_SetResult(interp, "couldn't create text", 
				      TCL_STATIC);
			char buf[20];
			sprintf(buf, "%d", q.error());
			Tcl_SetErrorCode(interp, "KOM", "FAILED-CALL", buf, 
					 kom_errmsg(q.error()), 
					 (char *)NULL);
			return TCL_ERROR;
		    }

		    res << (long) q.result();
		}
		break;
	    default:
		abort();
	    }
	}
	set_tcl_result(interp, res);
	return TCL_OK;

    case SESSION_GET_MARKS:
	{
	    std::vector<Mark>  marks;

	    marks = session->get_marks();
	    for (std::vector<Mark>::iterator iter = marks.begin();
		 iter < marks.end(); ++iter) {
		std::ostringstream   bufferstr;

		bufferstr << iter->text_no() << ' ' 
		    << static_cast<int>(iter->type()) << ' ';
		Tcl_AppendElement(interp, bufferstr.str().c_str());
	    }
	}
	break;

    case SESSION_WHO_IS_ON:
	{
	    Conferences &        conferences = session->conferences();
	    // FIXME: the code below only uses some fileds.  Some info
	    // is lost in the glue.
	    std::vector<Who_info_ident> who_info_list = session->who_is_on();

	    // Prefetch working conferences and persons.
	    // This is done because we will probably use them very
	    // soon anyhow.
	    for (std::vector<Who_info_ident>::iterator iter = who_info_list.begin();
		 iter < who_info_list.end(); ++iter) {
		Conf_no   wc = iter->working_conf();
		
		if (wc != 0)
		    conferences[wc].prefetch();
		
		conferences[iter->pers_no()].prefetch();
	    }

	    // Now, give the result to the TCL level.
	    bool first = true;
	    for (std::vector<Who_info_ident>::iterator iter = who_info_list.begin();
		 iter < who_info_list.end(); ++iter) {
		char      * copy1;
		char      * copy2;
		std::ostringstream  bufstream;

		// Add space between list elements.
		if (first == true)
		    first = false;
		else
		    Tcl_AppendResult(interp, " ", (char *)NULL);

		bufstream << '{'
		    << iter->pers_no()
		    << ' ' << iter->working_conf()
		    << ' ' << iter->session() << '\0';

		copy1 = iter->doing().malloc_copy();
		copy2 = iter->username().malloc_copy();
	        if (copy1 == NULL || copy2 == NULL)
	        {
		    Tcl_SetResult(interp, "out of memory", TCL_STATIC);
		    Tcl_SetErrorCode(interp, "KOM", "ENOMEM", (char *)NULL);
		    return TCL_ERROR;
		}
		Tcl_AppendResult(interp, bufstream.str().c_str(), 
				 (char *)NULL);
		Tcl_AppendElement(interp , copy1);
		Tcl_AppendElement(interp , copy2);
		Tcl_AppendResult(interp, "}", (char *) NULL);
		free(copy1);
		free(copy2);
	    }
	}
	break;

    case SESSION_WHO_AM_I:
	res << session->my_login() << ' ' << session->my_session();
	set_tcl_result(interp, res);
	return TCL_OK;

    case SESSION_CREATE_PERSON:
        {
	    Kom_res<Pers_no> created_person = session->create_person(
		args.stringarg(0),
		args.stringarg(1),
		Personal_flags(),
		std::vector<Aux_item>());
	    if (created_person.error ==  KOM_NO_ERROR)
	    {
		res << created_person.data;
		set_tcl_result(interp, res);
		return TCL_OK;
	    }
	    else
	    {
		char buf[20];
		sprintf(buf, "%d", created_person.error);
		set_tcl_error(interp, "can't create person",
			      "KOM", "FAILED-CALL", buf, 
			      kom_errmsg(created_person.error));
		return TCL_ERROR;
	    }
	}
	/*NOTREACHED*/
	abort();

    case SESSION_HANDLE_ASYNC:
	if (argc == 2)
	    session->handle_async();
	else
	    session->handle_async(args.intarg(0));
	return TCL_OK;
	break;

    case SESSION_SEND_MESSAGE:
        {
	    Status  stat = session->send_message(args.intarg(0),
						 args.stringarg(0));
	    if (stat !=  st_ok)
	    {
		// FIXME: Where is the error number?
		set_tcl_error(interp, "can't send message",
			      "KOM", "FAILED-CALL", "0", "unknown error");
		return TCL_ERROR;
	    }
	}
	return TCL_OK;

    case SESSION_REGISTER_ASYNC:
	// FIXME: This should return a handle, so that user code
	// can remove it (and have more than one active handler).
	if (strcmp(args.stringarg(0), "text_message") == 0)
	{	
	    if (async_text_message_handle.is_legal())
	    {
		set_tcl_error(interp, "can't register more than one "
			      "TCL handler for text_message async message",
			      "KOM", "BADARGS");
		return TCL_ERROR;
	    }

	    // FIXME: temporary restriction: use clientdata instead.
	    assert(the_interp == NULL || the_interp == interp);
	    the_interp = interp;

	    Async_dispatcher &d(session->async_dispatcher());
	    Kom_auto_ptr<Async_callback<Async_text_message> > ptr;
	    ptr.reset(async_callback_function(
			  &text_message_func,
			  LyStr(args.stringarg(1))).release());
	    async_text_message_handle = d.register_text_message_handler(ptr);
	}
	else if (strcmp(args.stringarg(0), "text_created") == 0)
	{	
	    if (async_text_created_handle.is_legal())
	    {
		set_tcl_error(interp, "can't register more than one "
			      "TCL handler for text_created async message",
			      "KOM", "BADARGS");
		return TCL_ERROR;
	    }

	    // FIXME: temporary restriction: use clientdata instead.
	    assert(the_interp == NULL || the_interp == interp);
	    the_interp = interp;

	    Async_dispatcher &d(session->async_dispatcher());
	    Kom_auto_ptr<Async_callback<Async_text_created> > ptr;
	    ptr.reset(async_callback_function(
			  &text_created_func,
			  LyStr(args.stringarg(1))).release());
	    async_text_created_handle = d.register_text_created_handler(ptr);
	}
	else 
	{
	    set_tcl_error(interp, "Unknown async type",
			  "KOM", "BADARGS");
	    return TCL_ERROR;
	}
	return TCL_OK;

    case SESSION_USER_ACTIVE:
	session->user_active();
	return TCL_OK;

    default:
	assert(args.option() < 0);
	return TCL_ERROR;
    }
    
    return TCL_OK;
}


// ================================================================
//                   TCL command 'conference'

enum Kom_conference_args {
    CONF_NAME=1, CONF_PREFETCH_UCONF, CONF_PREFETCH,
    CONF_NUMBER, CONF_PRESENTATION
};

static Tcl_Args kom_conf_options[] = {
    {"name",         CONF_NAME,         1, 0, {TCLARG_INT}},
    {"prefetch_uconf",CONF_PREFETCH_UCONF,1, 0, {TCLARG_INT}},
    {"prefetch",     CONF_PREFETCH,     1, 0, {TCLARG_INT}},
    {"number",       CONF_NUMBER,       1, 0, {TCLARG_INT}},
    {"presentation", CONF_PRESENTATION, 1, 0, {TCLARG_INT}},
    {NULL, 0, 0, 0, {}}
};

extern "C" int
tcl_kom_conference(ClientData  /*clientData*/,
		   Tcl_Interp *interp, 
		   int         argc, 
		   const char *argv[])
{
    Parse_tcl_args args(interp, kom_conf_options, argc, argv);
    if (args.option() < 0)
	return TCL_ERROR;
    std::ostringstream res;

    Conf_no      conf_no = args.intarg(0);
    Conference   conf = session->conferences()[conf_no];

    if (args.option() == CONF_PREFETCH_UCONF) {
        conf.prefetch_uconf();
        return TCL_OK;
    }
    if (args.option() == CONF_PREFETCH) {
        conf.prefetch();
        return TCL_OK;
    }

    if (!conf.exists())
    {
	char confnobuf[20];
	char buf[20];

	sprintf(confnobuf, "%d", conf_no);
	sprintf(buf, "%d", conf.kom_errno());
	Tcl_SetResult(interp, "can't get_conf_stat", TCL_STATIC);
	Tcl_SetErrorCode(interp, "KOM", "BADCONF", confnobuf, buf, 
			 kom_errmsg(conf.kom_errno()), (char *)NULL);
	return TCL_ERROR;
    }
    switch (args.option()) {
    case CONF_NAME:
	{
	    char *res = conf.name().malloc_copy();
	    if (res == NULL)
	    {
		Tcl_SetResult(interp, "out of memory", TCL_STATIC);
		Tcl_SetErrorCode(interp, "KOM", "ENOMEM", (char *)NULL);
		return TCL_ERROR;
	    }
	    Tcl_SetResult(interp, res, TCL_DYNAMIC);
	    return TCL_OK;
	}
	break;
    case CONF_NUMBER:
	{
	    // In this case we should not have to issue a get_conf_stat_question.
	    res << conf_no;
	    set_tcl_result(interp, res);
	    return TCL_OK;
	}
	break;
    case CONF_PRESENTATION:	
	{
	    res << conf.presentation();
	    set_tcl_result(interp, res);
	    return TCL_OK;	    
	}
	break;
    default:
	Tcl_SetResult(interp, "Internal error in kom_conference", TCL_STATIC);
	assert(0);
	return TCL_ERROR;
	break;
    }
}


// ================================================================
//                   TCL command 'person'


enum Kom_person_args {
    PERS_USERNAME=1,          PERS_NUMBER,        PERS_USER_AREA,          
    PERS_TOTAL_TIME_PRESENT,  PERS_SESSIONS,      PERS_CREATED_LINES,
    PERS_CREATED_BYTES,       PERS_READ_TEXTS,    PERS_NO_OF_TEXT_FETCHES,
    PERS_CREATED_PERSONS,     PERS_CREATED_CONFS, PERS_FIRST_CREATED_TEXT,
    PERS_NO_OF_CREATED_TEXTS, PERS_NO_OF_MARKS,   PERS_NO_OF_CONFS,
    PERS_UNREAD_IS_SECRET,    PERS_LAST_LOGIN
    };

static Tcl_Args kom_person_options[] = {
    {"user_name",           PERS_USERNAME,            1, 0, {TCLARG_INT}}, 
    {"number",              PERS_NUMBER,              1, 0, {TCLARG_INT}}, 
    {"pers_user_area",      PERS_USER_AREA,           1, 0, {TCLARG_INT}},
    {"total_time_present",  PERS_TOTAL_TIME_PRESENT,  1, 0, {TCLARG_INT}},
    {"sessions",            PERS_SESSIONS,            1, 0, {TCLARG_INT}},
    {"created_lines",       PERS_CREATED_LINES,       1, 0, {TCLARG_INT}},
    {"created_bytes",       PERS_CREATED_BYTES,       1, 0, {TCLARG_INT}},
    {"read_texts",          PERS_READ_TEXTS,          1, 0, {TCLARG_INT}},
    {"no_of_text_fetches",  PERS_NO_OF_TEXT_FETCHES,  1, 0, {TCLARG_INT}},
    {"created_persons",     PERS_CREATED_PERSONS,     1, 0, {TCLARG_INT}},
    {"created_confs",       PERS_CREATED_CONFS,       1, 0, {TCLARG_INT}},
    {"first_created_text",  PERS_FIRST_CREATED_TEXT,  1, 0, {TCLARG_INT}},
    {"no_of_created_texts", PERS_NO_OF_CREATED_TEXTS, 1, 0, {TCLARG_INT}},
    {"no_of_marks",         PERS_NO_OF_MARKS,         1, 0, {TCLARG_INT}},
    {"no_of_confs",         PERS_NO_OF_CONFS,         1, 0, {TCLARG_INT}},
    //    else if (!strcmp(argv[1], "privileges"))
    //	foo
    {"unread_is_secret",    PERS_UNREAD_IS_SECRET,    1, 0, {TCLARG_INT}},
    {"last_login",          PERS_LAST_LOGIN,          1, 0, {TCLARG_INT}},
    {NULL, 0, 0, 0, {}}
};


extern "C" int
tcl_kom_person(ClientData /*clientData*/,
	       Tcl_Interp *interp, 
	       int argc, 
	       const char *argv[])
{
    long   numeric_result = 0;
    
    Parse_tcl_args args(interp, kom_person_options, argc, argv);
    if (args.option() < 0)
	return TCL_ERROR;
    std::ostringstream res;

    Pers_no   pers_no = args.intarg(0);
    Person    p_stat = session->persons()[pers_no];
    if (!p_stat.exists())
    {
	char persnobuf[20];
	char buf[20];
	sprintf(persnobuf, "%d", pers_no);
	sprintf(buf, "%d", p_stat.kom_errno());
	Tcl_SetResult(interp, "can't get_person_stat", TCL_STATIC);
	Tcl_SetErrorCode(interp, "KOM", "BADPERS", persnobuf, buf,
			 kom_errmsg(p_stat.kom_errno()), (char *)NULL);
	return TCL_ERROR;
    }
    
    switch (args.option()) 
    {
    case PERS_USERNAME:
	{
	    char *res = p_stat.username().malloc_copy();
	    if (res == NULL)
	    {
		Tcl_SetResult(interp, "out of memory", TCL_STATIC);
		Tcl_SetErrorCode(interp, "KOM", "ENOMEM", (char *)NULL);
		return TCL_ERROR;
	    }
	    Tcl_SetResult(interp, res, TCL_DYNAMIC);
	}
	return TCL_OK;
    case PERS_NUMBER:
	// In this case we should not have to issue a get_person_stat_question.
	res << pers_no;
	set_tcl_result(interp, res);
	return TCL_OK;
    case PERS_USER_AREA:
	numeric_result = p_stat.user_area();
	break;
    case PERS_TOTAL_TIME_PRESENT:
	numeric_result = p_stat.total_time_present();
	break;
    case PERS_SESSIONS:
	numeric_result = p_stat.sessions();
	break;
    case PERS_CREATED_LINES:
	numeric_result = p_stat.created_lines();
	break;
    case PERS_CREATED_BYTES:
	numeric_result = p_stat.created_bytes();
	break;
    case PERS_READ_TEXTS:
	numeric_result = p_stat.read_texts();
	break;
    case PERS_NO_OF_TEXT_FETCHES:
	numeric_result = p_stat.no_of_text_fetches();
	break;
    case PERS_CREATED_PERSONS:
	numeric_result = p_stat.created_persons();
	break;
    case PERS_CREATED_CONFS:
	numeric_result = p_stat.created_confs();
	break;
    case PERS_FIRST_CREATED_TEXT:
	numeric_result = p_stat.first_created_text();
	break;
    case PERS_NO_OF_CREATED_TEXTS:
	numeric_result = p_stat.no_of_created_texts();
	break;
    case PERS_NO_OF_MARKS:
	numeric_result = p_stat.no_of_marks();
	break;
    case PERS_NO_OF_CONFS:
	numeric_result = p_stat.no_of_confs();
	break;
	//    else if (!strcmp(argv[1], "privileges"))
	//	foo
    case PERS_UNREAD_IS_SECRET:
	numeric_result = p_stat.flags().unread_is_secret();
	break;
    case PERS_LAST_LOGIN:
	{
	    std::ostringstream res;
	    
	    output_time(res, p_stat.last_login());
	    set_tcl_result(interp, res);
	    return TCL_OK;
	}
	break;
	
    default:
	Tcl_SetResult(interp, "Internal error in kom_person", TCL_STATIC);
	abort();
	return TCL_ERROR;
	break;
    }
    
    res << numeric_result;
    set_tcl_result(interp, res);
    return TCL_OK;
}


// ================================================================


extern "C" int
tcl_map(ClientData /*clientData*/,
	Tcl_Interp *interp, 
	int argc, 
	const char *argv[])
{
    if (argc != 3)
    {
	Tcl_SetResult(interp, "usage: [map conf_no local_no]", TCL_STATIC);
	Tcl_SetErrorCode(interp, "KOM", "BADARGS", (char *)NULL);
	return TCL_ERROR;
    }
    long ltmp;
    int result = Tcl_ExprLong(interp, argv[1], &ltmp);
    if (result != TCL_OK)
	return result;
    Conf_no conf_no = ltmp;
    result = Tcl_ExprLong(interp, argv[2], &ltmp);
    if (result != TCL_OK)
	return result;
    Local_text_no loc_no = ltmp;
    
    std::ostringstream res;
    res << session->maps()[conf_no][loc_no];
    set_tcl_result(interp, res);
    return TCL_OK;
}


// ================================================================
//                   TCL command 'kom_text'


enum Kom_text_args {
    TEXT_AUTHOR=1,      TEXT_NUM_LINES,     TEXT_NUM_CHARS,
    TEXT_CREATION_TIME, TEXT_NUM_MARKS,     TEXT_NUM_RECIPIENTS, 
    TEXT_RECIPIENT,     TEXT_NUM_FOOTNOTES, TEXT_FOOTNOTE,
    TEXT_NUM_COMMENTS,  TEXT_COMMENT,       TEXT_NUM_UPLINKS,
    TEXT_UPLINK,        TEXT_SUBJECT,       TEXT_BODY,
    TEXT_MARK_AS_READ,  TEXT_MARK,          TEXT_UNMARK
    };

static Tcl_Args kom_text_options[] = {
    {"author",        TEXT_AUTHOR,        1, 0, {TCLARG_INT}}, 
    {"num_lines",     TEXT_NUM_LINES,     1, 0, {TCLARG_INT}}, 
    {"num_chars",     TEXT_NUM_CHARS,     1, 0, {TCLARG_INT}}, 
    {"ctime",         TEXT_CREATION_TIME, 1, 0, {TCLARG_INT}}, 
    {"creation_time", TEXT_CREATION_TIME, 1, 0, {TCLARG_INT}}, 
    {"num_marks",     TEXT_NUM_MARKS,     1, 0, {TCLARG_INT}}, 
    {"num_recipients",TEXT_NUM_RECIPIENTS,1, 0, {TCLARG_INT}}, 
    {"recipient",     TEXT_RECIPIENT,     2, 0, {TCLARG_INT, TCLARG_INT}}, 
    {"num_footnotes", TEXT_NUM_FOOTNOTES, 1, 0, {TCLARG_INT}}, 
    {"footnote",      TEXT_FOOTNOTE,      2, 0, {TCLARG_INT, TCLARG_INT}},
    {"num_comments",  TEXT_NUM_COMMENTS,  1, 0, {TCLARG_INT}}, 
    {"comment",       TEXT_COMMENT,       2, 0, {TCLARG_INT, TCLARG_INT}},
    {"num_uplinks",   TEXT_NUM_UPLINKS,   1, 0, {TCLARG_INT}}, 
    {"uplink",        TEXT_UPLINK,        2, 0, {TCLARG_INT, TCLARG_INT}},
    {"subject",       TEXT_SUBJECT,       1, 0, {TCLARG_INT}}, 
    {"body",          TEXT_BODY,          1, 0, {TCLARG_INT}}, 
    {"mark_as_read",  TEXT_MARK_AS_READ,  1, 1, {TCLARG_INT, TCLARG_INT}}, 
    {"mark",          TEXT_MARK,          2, 0, {TCLARG_INT, TCLARG_INT}}, 
    {"unmark",        TEXT_UNMARK,        1, 0, {TCLARG_INT}}, 
    {NULL, 0, 0, 0, {}}
};


extern "C" int
tcl_kom_text(ClientData  /*clientData*/,
	     Tcl_Interp *interp, 
	     int         argc, 
	     const char *argv[])
{
    Parse_tcl_args args(interp, kom_text_options, argc, argv);
    if (args.option() < 0)
	return TCL_ERROR;
    std::ostringstream result;

    Text_no   text_no = args.intarg(0);
    Text      txt = session->texts()[text_no];
    if (!txt.exists())
    {
	char textnobuf[20];
	char buf[20];
	sprintf(textnobuf, "%ld", text_no);
	sprintf(buf, "%d", txt.kom_errno());
	Tcl_SetResult(interp, "Text nonexistent", TCL_STATIC);
	Tcl_SetErrorCode(interp, "KOM", "BADTEXT", textnobuf, buf,
			 kom_errmsg(txt.kom_errno()), "STATUS", (char *)NULL);
	return TCL_ERROR;
    }
    
    switch (args.option()) 
    {
    case TEXT_AUTHOR:
	result << txt.author();
	break;
    case TEXT_NUM_LINES:
	result << txt.num_lines();
	break;
    case TEXT_NUM_CHARS:
	result << txt.num_chars();
	break;
    case TEXT_CREATION_TIME:
	output_time(result, txt.creation_time());
	break;
    case TEXT_NUM_MARKS:
	result << txt.num_marks();
	break;
    case TEXT_NUM_RECIPIENTS:
	result << txt.num_recipients();
	break;
    case TEXT_RECIPIENT:
	output_recipient(result, txt.recipient(args.intarg(1)));
	break;
    case TEXT_NUM_FOOTNOTES:
	result << txt.num_footnote_in();
	break;
    case TEXT_FOOTNOTE:
	result << txt.footnote_in(args.intarg(1));
	break;
    case TEXT_NUM_COMMENTS:
	result << txt.num_comment_in();
	break;
    case TEXT_COMMENT:
	result << txt.comment_in(args.intarg(1));
	break;
    case TEXT_NUM_UPLINKS:
	result << txt.num_uplinks();
	break;
    case TEXT_UPLINK:
	output_uplink(result, txt.uplink(args.intarg(1)));
	break;
    case TEXT_SUBJECT:
	{
	    LyStr_errno subject = txt.subject();

	    if (subject.kom_errno != KOM_NO_ERROR)
	    {
		char textnobuf[20];
		char buf[20];
		sprintf(textnobuf, "%ld", text_no);
		sprintf(buf, "%d", txt.kom_errno());
		Tcl_SetResult(interp, "Text nonexistent", TCL_STATIC);
		Tcl_SetErrorCode(interp, "KOM", "BADTEXT", textnobuf, buf,
				 kom_errmsg(txt.kom_errno()), "TEXT-MASS",
				 (char *)NULL);
		return TCL_ERROR;
	    }
	    
	    char  * res = subject.str.malloc_copy();
	    // FIXME: Memory leak??????  where is res freed?
	    if (res == NULL)
	    {
		Tcl_SetResult(interp, "out of memory", TCL_STATIC);
		Tcl_SetErrorCode(interp, "KOM", "ENOMEM", (char *)NULL);
		return TCL_ERROR;
	    }
	    
	    Tcl_SetResult(interp, res, TCL_DYNAMIC);
	}
	return TCL_OK;
    case TEXT_BODY:
	{
	    LyStr_errno body = txt.body();

	    if (body.kom_errno != KOM_NO_ERROR)
	    {
		char textnobuf[20];
		char buf[20];
		sprintf(textnobuf, "%ld", text_no);
		sprintf(buf, "%d", txt.kom_errno());
		Tcl_SetResult(interp, "Text nonexistent", TCL_STATIC);
		Tcl_SetErrorCode(interp, "KOM", "BADTEXT", textnobuf, buf,
				 kom_errmsg(txt.kom_errno()), "TEXT-MASS",
				 (char *)NULL);
		return TCL_ERROR;
	    }

	    char  * res = body.str.malloc_copy();
	    if (res == NULL)
	    {
		Tcl_SetResult(interp, "out of memory", TCL_STATIC);
		Tcl_SetErrorCode(interp, "KOM", "ENOMEM", (char *)NULL);
		return TCL_ERROR;
	    }
	    
	    Tcl_SetResult(interp, res, TCL_DYNAMIC);
	}
	return TCL_OK;
    case TEXT_MARK_AS_READ:
	result 
	    << ((txt.mark_as_read((argc == 4) ? args.intarg(1) : 0) == st_ok)
		? "1" : "0");
	break;
    case TEXT_MARK:
	result << ((txt.mark(args.intarg(1)) == KOM_NO_ERROR) ? "1" :"0");
	break;
	
    case TEXT_UNMARK:
	// +++FIXME: Shouldn't this fail, and set errorCode, instead
	// of returning 0 if it fails? That way, kom_errno would be
	// accessible to the TCL layer. This note applies to several
	// other commands as well.
        result << ((txt.unmark() == KOM_NO_ERROR) ? "1" : "0");
	break;
	
    default:
	Tcl_SetResult(interp, "Internal error in kom_text", TCL_STATIC);
	assert(0);
	return TCL_ERROR;
    }
    set_tcl_result(interp, result);
    return TCL_OK;
}


// ================================================================
//                   TCL command 'kom_unread'

// +++Methods in class Unread_confs not yet implemented in TCL:
//    void                 add_conference(Conf_no);
//    void                 add_text(Text_no); // NYI
//
//    UNREAD_DISJOIN_CONF implementerad av Peter Antman, 23/1 1997.

enum Kom_unread_args {
    UNREAD_PREFETCH=1, UNREAD_UNREAD_TEXTS,
    UNREAD_UNREAD_TEXTS_IN_CURRENT,
    UNREAD_PROMPT, UNREAD_GOTO_NEXT_CONF, UNREAD_GOTO_CONF,
    UNREAD_CURRENT_CONF, UNREAD_NEXT_TEXT,
    UNREAD_JOIN_CONF, UNREAD_DISJOIN_CONF
	//    UNREAD_ADD_TEXT,
    };

static Tcl_Args kom_unread_options[] = {
    {"prefetch",                UNREAD_PREFETCH,                0, 7, 
     {TCLARG_INT, TCLARG_INT, TCLARG_INT, TCLARG_INT, TCLARG_INT,
      TCLARG_INT, TCLARG_INT}}, 
    {"unread_texts",            UNREAD_UNREAD_TEXTS,            0, 0, {}}, 
    {"unread_texts_in_current", UNREAD_UNREAD_TEXTS_IN_CURRENT, 0, 0, {}}, 
    {"prompt",                  UNREAD_PROMPT,                  0, 0, {}}, 
    {"goto_next_conf",          UNREAD_GOTO_NEXT_CONF,          0, 0, {}}, 
    {"goto_conf",               UNREAD_GOTO_CONF,	1, 0, {TCLARG_INT}},
    {"join_conf",               UNREAD_JOIN_CONF,	3, 0,
         {TCLARG_INT, TCLARG_INT, TCLARG_INT}},
    {"disjoin_conf",            UNREAD_DISJOIN_CONF,     1,0, {TCLARG_INT}},
    {"current_conf",            UNREAD_CURRENT_CONF,            0, 0, {}}, 
    {"next_text",               UNREAD_NEXT_TEXT,       0, 1, {TCLARG_STRING}},
    {NULL, 0, 0, 0, {}}
};


extern "C" int
tcl_kom_unread(ClientData  /*clientData*/,
	       Tcl_Interp *interp, 
	       int         argc, 
	       const char *argv[])
{
    Parse_tcl_args args(interp, kom_unread_options, argc, argv);
    std::ostringstream res;

    switch (args.option()) {
    case UNREAD_PREFETCH:
        {
	    Status st = st_error;
	    Work_quota w;
	    
	    switch(argc)
	    {
	    case 2:
		w = Work_quota();
		st = unread_confs->prefetch(&w);
		break;
	    case 5:
		w = Work_quota(args.intarg(0),
			       args.intarg(1),
			       args.intarg(2));
		st = unread_confs->prefetch(&w); 
		break;
	    case 9:
		w = Work_quota(args.intarg(0),
			       args.intarg(1),
			       args.intarg(2),
			       args.intarg(3),
			       args.intarg(4),
			       args.intarg(5),
			       args.intarg(6));
		st = unread_confs->prefetch(&w); 
		break;
	    default:
		// FIXME: is this bogus?
		Tcl_SetResult(interp, "Wrong number of arguments", TCL_STATIC);
		Tcl_SetErrorCode(interp, "KOM", "SYNTAX", (char *)NULL);
		return TCL_ERROR;
	    }
	    if (st == st_ok)
		res << '0';
	    else if (st == st_pending)
		res << '1';
	    else if (st == st_error)
		res << "-1";
	}
	break;
	
    case UNREAD_UNREAD_TEXTS:
	{
	    std::vector<Unread_info>  arr = unread_confs->confs_with_unread();
	    
	    for (std::vector<Unread_info>::iterator iter = arr.begin();
		 iter < arr.end(); ++iter) {
		std::ostringstream buffer;

		buffer << '{' << iter->conf
		    << ' ' << iter->min_unread
		    << ' ' << iter->max_unread << '}' << ' ';
		Tcl_AppendResult(interp, buffer.str().c_str(), (char *)NULL);
	    }
	}
	return TCL_OK;

    case UNREAD_UNREAD_TEXTS_IN_CURRENT:
	res << unread_confs->min_unread_texts_in_current()
	    << ' ' << unread_confs->max_unread_texts_in_current();
	break;

    case UNREAD_PROMPT:
	{
	    char  * promptstr = NULL;

	    switch (unread_confs->next_prompt()) {
	    case NEXT_CONF:
		promptstr = "NEXT_CONF"; 	    break;
	    case NEXT_TEXT:
		promptstr = "NEXT_TEXT";	    break;
	    case NEXT_COMMENT:
		promptstr = "NEXT_COMMENT";	    break;
	    case NEXT_FOOTNOTE:
		promptstr = "NEXT_FOOTNOTE";    break;
	    case NO_MORE_TEXT:
		promptstr = "NO_MORE_TEXT";	    break;
	    case TIMEOUT_PROMPT:
		promptstr = "TIMEOUT";	    break;
	    case PROMPT_ERROR:
		// +++FIXME error message!
		res << "Connection lost";
		set_tcl_result(interp, res);
		return TCL_ERROR;
	    default:
		abort();
	    }
	    assert(promptstr != NULL);
	    Tcl_SetResult(interp, promptstr, TCL_STATIC);
	}
	return TCL_OK;

      case UNREAD_GOTO_NEXT_CONF:
        res << unread_confs->goto_next_conf();
	break;

      case UNREAD_GOTO_CONF:
	switch(unread_confs->goto_conf(args.intarg(0)))
	{
	case st_ok:
	    break;
	case st_error:
	    {
		// +++ Report an error-code here, somehow.
		Tcl_SetResult(interp, "Can not go to that conference", 
			      TCL_STATIC);
		Tcl_SetErrorCode(interp, "KOM", "FAILED-CALL", "0",
				 kom_errmsg(0), (char *)NULL);
		return TCL_ERROR;
	    }
	    break;
	case st_pending:
	    assert(0);
	}
	break;

      case UNREAD_JOIN_CONF:
	switch(unread_confs->join_conference(args.intarg(0), args.intarg(1),
					     args.intarg(2), Membership_type()))
	{
	case st_ok:
	    break;
	case st_error:
	    {
		// +++ Report an error-code here, somehow.
		Tcl_SetResult(interp, "Can not join that conference", 
			      TCL_STATIC);
		Tcl_SetErrorCode(interp, "KOM", "FAILED-CALL", "0",
				 kom_errmsg(0), (char *)NULL);
		return TCL_ERROR;
	    }
	    break;
	case st_pending:
	    assert(0);
	}
	break;

	// Implemented by Peter Antman, 1997
    case UNREAD_DISJOIN_CONF:					
      switch(unread_confs->disjoin_conference(args.intarg(0)))
	{
	case KOM_NO_ERROR:
	    break;

	case KOM_UNDEF_CONF:
	    {
		// +++ Report an error-code here, somehow.
		Tcl_SetResult(interp, "Conference does not exist", 
			      TCL_STATIC);
		Tcl_SetErrorCode(interp, "KOM", "FAILED-CALL", "0",
				 kom_errmsg(0), (char *)NULL);
		return TCL_ERROR;
	    }
	    break;
	case KOM_NOT_MEMBER:
	     {
		// +++ Report an error-code here, somehow.
		Tcl_SetResult(interp, "Not member of that conference", 
			      TCL_STATIC);
		Tcl_SetErrorCode(interp, "KOM", "FAILED-CALL", "0",
				 kom_errmsg(0), (char *)NULL);
		return TCL_ERROR;
	    }
	    break;
	default:
	   {
		// +++ Report an error-code here, somehow.
		Tcl_SetResult(interp, "Some error ouccured", 
			      TCL_STATIC);
		Tcl_SetErrorCode(interp, "KOM", "FAILED-CALL", "0",
				 kom_errmsg(0), (char *)NULL);
		return TCL_ERROR;
	    }
	    break;
	}
	break;

      case UNREAD_CURRENT_CONF:
	// +++FIXME: This check should be all over this function.  
        // We fix it here for now so that tkom won't dump core when 
	// somebody aborts the login sequence.
	if (!unread_confs)
            res << "-1";
	else
            res << unread_confs->current_conf();
	break;

      case UNREAD_NEXT_TEXT:
	if (argc >= 3)
	{
	    if (strcmp(args.stringarg(0), "remove") == 0)
                res << unread_confs->next_text(true);
	    else 
	    {
		Tcl_AppendResult(interp,
				 "Unknown argument to kom_unread next_text. ",
				 "Should be 'remove' or nothing: ",
				 args.stringarg(0),
				 (char *)NULL);
		Tcl_SetErrorCode(interp, "KOM", "BADARGS", (char *)NULL);
		return TCL_ERROR;
	    }
	} 
	else
            res << unread_confs->next_text();
	break;

    default:
	assert(args.option() < 0);
	return TCL_ERROR;
    }
    set_tcl_result(interp, res);
    return TCL_OK;
}


// ================================================================


extern "C" int
tcl_delete_conf(ClientData /*clientData*/,
		Tcl_Interp *interp, 
		int argc, 
		const char *argv[])
{
    if (argc != 2)
    {
	Tcl_SetResult(interp, "One argument expected", TCL_STATIC);
	Tcl_SetErrorCode(interp, "KOM", "BADARGS", (char *)NULL);
	return TCL_ERROR;
    }
    long ltmp;
    int result = Tcl_ExprLong(interp, argv[1], &ltmp);
    if (result != TCL_OK)
	return result;
    delete_conf_question q(conn_foo, ltmp);
    if (q.receive() == st_ok)
	return TCL_OK;
    else
    {
	char buf[20];
	sprintf(buf, "%d", q.error());
	Tcl_SetErrorCode(interp, "KOM", "FAILED-CALL", buf, 
			 kom_errmsg(q.error()), (char *)NULL);
	return TCL_ERROR;
    }
}

extern "C" int
tcl_enable(ClientData /*clientData*/,
	   Tcl_Interp *interp, 
	   int argc, 
	   const char *argv[])
{
    if (argc != 2)
    {
	Tcl_SetResult(interp, "One argument expected", TCL_STATIC);
	Tcl_SetErrorCode(interp, "KOM", "BADARGS", (char *)NULL);
	return TCL_ERROR;
    }
    long ltmp;
    int result = Tcl_ExprLong(interp, argv[1], &ltmp);
    if (result != TCL_OK)
	return result;
    enable_question q(conn_foo, ltmp);
    if (q.receive() == st_ok)
	return TCL_OK;
    else
    {
	char buf[20];
	sprintf(buf, "%d", q.error());
	Tcl_SetErrorCode(interp, "KOM", "FAILED-CALL", buf, 
			 kom_errmsg(q.error()), (char *)NULL);
	return TCL_ERROR;
    }
}

void
kom_define_commands(Tcl_Interp *interp)
{
    Tcl_CreateCommand(interp, "kom_session",   tcl_kom_session,    NULL, NULL);
    Tcl_CreateCommand(interp, "kom_conference",tcl_kom_conference, NULL, NULL);
    Tcl_CreateCommand(interp, "kom_person",    tcl_kom_person,     NULL, NULL);
    Tcl_CreateCommand(interp, "map",           tcl_map,            NULL, NULL);
    Tcl_CreateCommand(interp, "kom_text",      tcl_kom_text,       NULL, NULL);
    Tcl_CreateCommand(interp, "kom_unread",    tcl_kom_unread,     NULL, NULL);
    Tcl_CreateCommand(interp, "connect",       tcl_connect,        
		      NULL, tcl_delete_connection);
    Tcl_CreateCommand(interp, "enable",        tcl_enable,         NULL, NULL);
    Tcl_CreateCommand(interp, "delete_conf",   tcl_delete_conf,    NULL, NULL);
}
