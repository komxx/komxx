// Low-level *more* prompt handling
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// paginator.cc

#include <config.h>

#include <cassert>
#include <sstream>
#include <cstdlib>

#include "paginator.h"

Paginator::Paginator(std::ostream &screen)
    : is_discarding(false),
      sync_is_set(false),
      moring(false),
      auto_newline_active(false),
      sout(screen),
      use_rows(-1),
      use_cols(-1),
      translation(TRANSPARENT),
      cursor_row(0),
      cursor_col(0),
      pending_output(),
      next_prompt(),
      more_prompt("somebody please set the prompt> ")
{
    // FIXME: Guess better values.
    n_rows = 24;
    n_cols = 80;
}

Paginator::~Paginator()
{
}


int 
Paginator::columns() const
{
    if (use_cols != -1)
	return use_cols;
    else
	return n_cols;
}

int 
Paginator::rows() const
{
    if (use_rows != -1)
	return use_rows;
    else
	return n_rows;
}

int
Paginator::real_columns() const
{
    return n_cols;
}

int
Paginator::real_rows() const
{
    return n_rows;
}

void
Paginator::translate(Translation mode)
{
    translation = mode;
}

Paginator &
Paginator::operator <<(const LyStr &o)
{
    if (sync_is_set)
	next_prompt << o;
    else if (!is_discarding)
	pending_output << o;
    update_screen();
    return *this;
}

void
Paginator::sync()
{
    sync_is_set = true;
}

long
Paginator::pending() const
{
    return pending_output.strlen();
}

void
Paginator::discard()
{
    pending_output.clear();
    is_discarding = true;
    update_screen();
}

bool
Paginator::discarding() const
{
    return is_discarding;
}

LyStr
Paginator::get_pending()
{
    return pending_output;
}

void
Paginator::set_width(int width)
{
    use_cols = width;
    // Make sure the cursor is not outside the screen.
    if (columns() != 0 && cursor_col >= columns())
	cursor_col = columns()-1;
}

void
Paginator::set_height(int height)
{
    use_rows = height;
    // Make sure the cursor is not outside the screen.
    if (rows() != 0 && cursor_row >= rows())
	cursor_row = rows() - 1;
}

void
Paginator::set_more_prompt(const LyStr &prompt)
{
    more_prompt = prompt;
}

void
Paginator::more_lines(int lines)
{
    cursor_row -= lines;
    if (cursor_row < 0)
	cursor_row = 0;
    update_screen();
}

void
Paginator::bell() const
{
    sout << '\a';
    sout.flush();
}

void
Paginator::remove(LyStr rm)
{
    assert(next_prompt.strlen() == 0);
    assert(sync_is_set == false); // this assertion has failed once.
    if (pending_output.strlen() >= rm.strlen())
	pending_output.remove_last(rm.strlen());
    else 
    {
	rm.remove_last(pending_output.strlen());
	pending_output.clear();
	// FIXME: Somehow make it possible to erase on the previous line.
	while (rm.strlen() > 0 && cursor_col > 0)
	{
	    int glyphsize = 1;
	    unsigned char uc = rm[0];
	    rm.remove_first(1);
	    switch (uc)
	    {
	    case '\r':
	    case '\n':
	    case '\t':
		// These are impossible to erase predictably,
		// so give it up.
		sout.flush();
		return;
	    }

	    if (uc < ' ' || (uc >= 0x80 && uc < ' ' + 0x80))
		glyphsize = 4;

	    while (glyphsize-- > 0 && cursor_col > 0)
	    {
		sout << "\b \b";
		cursor_col--;
	    }
	}
	sout.flush();
    }
}

void
Paginator::clearline()
{
    if (cursor_col != 0)
    {
	// FIXME: tputs something better.  Use the code below as a fallback.
	sout << '\r';
	for (int ix = 0; ix < cursor_col; ix++)
	    sout << ' ';
	sout << '\r';
	cursor_col = 0;
    }
}

int
Paginator::maybe_print(char c, Screenarea area)
{
  assert(cursor_col >= 0);
  assert(cursor_col < columns() || columns() == 0);
  assert(cursor_row >= 0);
  assert(cursor_row < rows() || rows() == 0);

  unsigned char uc = c;

  // Handle special characters.
  switch(uc)
    {
    case '\r':
      sout << uc;
      cursor_col = 0;
      auto_newline_active = false;
      return 1;
      
    case '\n':
      if (cursor_row == rows() - 1 && rows() != 0)
	return 0;

      if (auto_newline_active == false)
	{
	  sout << '\r' << '\n';
	  cursor_col = 0;
	  cursor_row++;
	}
      auto_newline_active = false;
      return 1;
      
    case '\t':
      {
	int ret;

	// FIXME: Should we support �rjan Berglund mode?
	while ((ret = maybe_print(' ', area)) && cursor_col % 8 != 0)
	  ;
	auto_newline_active = false;
	return ret;
      }
    }

  // FIXME: use locale stuff to determine if it is printable or not
  if (uc < ' ' || (uc >= 0x80 && uc < ' ' + 0x80))
    {
      // Control character.
      // FIXME: configurable printing would be nice.

      enum {GLYPH_SIZE = 4};
      std::ostringstream o;
      o << '\\';
      o.width(3);
      o.fill('0');
      o << (unsigned int)uc;
      const char *buffer = o.str().c_str();

      // Check if the screen is full, but only if it isn't infinite.
      // Do the check here, to avoid writing half a glyph before the
      // screen is full. 
      if (rows() != 0 && columns() != 0)
	if (area == PROMPT)
	  {
	    if (cursor_row == rows() - 1 
		&& cursor_col >= columns() - GLYPH_SIZE)
	      return 0;
	  } 
	else
	  {
	    if ((cursor_row == rows() - 2
		 && cursor_col > columns() - GLYPH_SIZE)
		|| cursor_row == rows() - 1)
	      return 0;
	  }

      for (int ix = 0; ix < GLYPH_SIZE; ix++)
	{
	  int ret = maybe_print(buffer[ix], area);
	  assert(ret > 0);
	}
      return 1;
    }

  // Print normal characters.

  if (cursor_row == rows() - 1 
      && (area == USER || cursor_col == columns() - 1)
      && rows() != 0 && columns() != 0)
    return 0;

  // Print the character in the mode given by the member 'printmode'.
  switch (translation)
    {
    case TRANSPARENT:
      break;
    case SWASCII:
      // Make a very simple and naive translation from iso-8859/1
      // to swedish ascii.
      if (uc >= 0200)
	{
	  switch (uc)
	    {
	    case 0304: uc = '[';  break;
	    case 0305: uc = ']';  break;
	    case 0311: uc = '@';  break;
	    case 0315: uc = 'I';  break;
	    case 0326: uc = '\\'; break;
	    case 0344: uc = '{';  break;
	    case 0345: uc = '}';  break;
	    case 0351: uc = '`';  break;
	    case 0355: uc = 'i';  break;
	    case 0366: uc = '|';  break;
	    
	    // A bit primitive, but ok for now.
	    default: 
	      uc = (~0200 & uc);	
	      break;
	    }
	}
      break;
    }
  sout << uc;
  auto_newline_active = false;
 
  if (++cursor_col >= columns() && columns() != 0)
    {
      sout << '\r' << '\n';
      auto_newline_active = true;
      cursor_col = 0;
      cursor_row++;
      
      assert(cursor_row < rows() || rows() == 0);
    }
  return 1;
}

void
Paginator::update_screen()
{
    if (cursor_row == rows() - 1 && rows() != 0 && pending_output.strlen() > 0)
    {
	if (!moring)
	{
	    start_moring();
	    sout.flush();
	}
	return;
    }
    
    if (moring)
    {
	clearline();
	moring = false;
    }

    while (pending_output.strlen() > 0
	   && maybe_print(pending_output[0], USER) > 0)
    {
	pending_output.remove_first(1);
    }
    
    if (pending_output.strlen() == 0)
    {
	if (sync_is_set)
	{
	    sync_is_set = false;
	    is_discarding = false;
	    cursor_row = 0;
	    pending_output = next_prompt;
	    next_prompt.clear();
	    while (pending_output.strlen() > 0
		   && maybe_print(pending_output[0], USER) > 0)
	    {
		pending_output.remove_first(1);
	    }
	    if (pending_output.strlen() > 0)
	    {
		// FIXME: Error handling.
		// This is an illegal state because the user is not
		// allowed to write more than a single prompt after a
		// sync point until the sync point has been output to
		// the screen.	    
		abort();
	    }
	}
    }
    else			// pending_output.strlen() > 0
    {
	start_moring();
    }

    sout.flush();
}

void
Paginator::start_moring()
{
    // The screen is now full.
    assert(rows() != 0);
    assert(cursor_row == rows() - 1);
    assert(moring == false);

    int foo;
    // Silently truncate the prompt if it is more than one line long.
    for (int ix = 0; 
	 ix < more_prompt.strlen() && (ix < columns()-1 || columns() == 0);
	 ix++)
    {
	foo = maybe_print(more_prompt[ix], PROMPT);
	assert(foo > 0);
    }
    moring = true;
}

// eof
