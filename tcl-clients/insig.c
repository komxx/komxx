/* Terminal initialization and signal handling under nilkom
//
// Copyright (C) 1994  Per Cederqvist and Inge Wallin
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <config.h>

#include <signal.h>
#include <fcntl.h>
#include <stdio.h>

#if defined(HAVE_TERMIOS_H)
#  include <termios.h>
#endif
#include <stdlib.h>

#include "unused.h"
#include "insig.h"

#ifdef HAVE_STRUCT_SIGACTION
static struct sigaction default_abort_act;
#endif

#ifdef HAVE_TERMIOS_H
struct termios origmode;
#endif
int oldflags;

/* This is 0 if the user is running interactively, that is, if
   tcgetattr succeeded. */
int no_tty = -1;

void
restore_kbd(void)
{
#ifdef HAVE_TERMIOS_H
    if (no_tty == 0)
	if (tcsetattr(0, TCSADRAIN, &origmode)!=0)
	{
	    perror("tcsetattr failed");
	}
#else
    system("stty cooked echo");
#endif

    if (fcntl(0, F_SETFL, oldflags) != 0)
    {
	perror("fcntl F_SETFL failed");
    }
}

static RETSIGTYPE
restore_kbd_and_exit(int UNUSED(sig))
{
    restore_kbd();
    exit(0);
}

static RETSIGTYPE
restore_kbd_and_abort(int sig)
{
    restore_kbd();

#ifdef HAVE_STRUCT_SIGACTION
    sigaction(SIGABRT, &default_abort_act, NULL);
#elif defined(SIGABRT)
    signal(sig, SIG_DFL);
    signal(SIGABRT, SIG_DFL);
#endif
    abort();
    fputs("I survived an abort!\n", stderr);
    exit(1);
}

void
setup_input_sig(void)
{
#ifdef HAVE_STRUCT_SIGACTION
    struct sigaction act;
#endif
#ifdef HAVE_TERMIOS_H
    struct termios newmode;
#endif

    /* Set up stdin in non-blocking mode. */
    if ((oldflags = fcntl(0, F_GETFL, 0)) == -1)
    {
	perror("fcntl F_GETFL failed");
	exit(1);
    }
#ifdef HAVE_TERMIOS_H
    no_tty = tcgetattr(0, &origmode);
#endif

    /* Trap all signals */
#ifdef HAVE_STRUCT_SIGACTION
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    act.sa_handler = restore_kbd_and_exit;
    sigaction(SIGHUP, &act, NULL);
    sigaction(SIGINT, &act, NULL);
    sigaction(SIGTERM, &act, NULL);
    act.sa_handler = restore_kbd_and_abort;
# ifdef SIGABRT
#  if defined(SA_RESETHAND)
    act.sa_flags = SA_RESETHAND;
#  elif defined(SA_ONESHOT)
    act.sa_flags = SA_ONESHOT;
#  else
    /* FIXME. */
    act.sa_flags = 0;
#  endif
    sigaction(SIGABRT, &act, &default_abort_act);
# endif
    sigaction(SIGQUIT, &act, NULL);
    sigaction(SIGBUS, &act, NULL);
    sigaction(SIGSEGV, &act, NULL);
    act.sa_handler = SIG_IGN;
# ifdef SIGWINCH
    sigaction(SIGWINCH, &act, NULL);
# endif

#else  /* !HAVE_STRUCT_SIGACTION */

    signal(SIGHUP, restore_kbd_and_exit);
    signal(SIGINT, restore_kbd_and_exit);
    signal(SIGTERM, restore_kbd_and_exit);
# ifdef SIGABRT
    signal(SIGABRT, restore_kbd_and_abort);
# endif
    signal(SIGQUIT, restore_kbd_and_abort);
    signal(SIGBUS, restore_kbd_and_abort);
    signal(SIGSEGV, restore_kbd_and_abort);
# ifdef SIGWINCH
    signal(SIGWINCH, SIG_IGN);
# endif
#endif /* !HAVE_STRUCT_SIGACTION */
    
#ifdef HAVE_TERMIOS_H    
    newmode = origmode;
    newmode.c_lflag &= ~(IEXTEN | ECHO | ISIG | ICANON);
    newmode.c_cc[VMIN] = 1;
    newmode.c_cc[VTIME] = 0;
    if (no_tty == 0)
      if (tcsetattr(0, TCSADRAIN, &newmode) != 0)
	{
	  perror("tcsetattr failed");
	  restore_kbd_and_exit(0);
	}
#else
    system("stty raw -echo");
#endif
}
/* eof */
