Filen isclib/README
Aronsson was here
8 mars 1991

			     Allt om ISC


				  av
			     Peter Norton

			   i samarbete med
			    Lars Aronsson



			   1. Den h{r filen

Den h{r filen skrevs av Lars Aronsson <aronsson@lysator.liu.se> efter
intervjuer med dem som konstruerade ISC. Det {r den mest detaljerade
beskrivningen av ISC som finns p} marknaden, n{st efter k{llkoden.
Manualsidan, som {nnu inte {r skriven, kommer att vara ett koncentrat
av kapitel 3, eftersom d{r st}r allt som en anv{ndare beh|ver veta.

Varning: den h{r dokumentationen {r deskriptiv. En manualsida eller
spec b|r naturligtvis vara normativ.

		--------------------------------------
		Avsnitt	Inneh}ll
		--------------------------------------
		1.	Den h{r filen
		2.	\verblick |ver filerna
		3.	Biblioteksinterface
		3.1	Arbetss{tt
		3.1.1	Minnesallokering
		3.1.2	Fatala fel
		3.2	Datatyper
		3.2.1	Sessioner
		3.2.2	Pools
		3.2.3	Events
		3.2.4	Meddelanden
		3.3	Biblioteksfunktioner
		3.3.1	Funktioner f|r pools
		3.3.2	Funktioner f|r sessioner
		3.3.3	Funktioner f|r events
		3.3.4	Funktioner f|r att skicka data
		3.3.5	\vriga funktioner
		4.	Intern uppbyggnad
		4.1	Teoretisk beskrivning
		4.2	Datatyper
		4.3	Funktioner
		--------------------------------------


		       2. \verblick |ver filerna

H{r hittar vi en massa filer som Peter Eriksson s{ger att han och Per
Cederqvist har skrivit. Snyggt programmerat i v{lkommenterad ANSI-C.
Kommentarerna {r visserligen p} engelska (you get what you pay for).
Filerna bildar ett funktionsbibliotek kallat ISC, vilket kanske
betyder International Syndicate of Computation, vem vet? Med ISC skall
det bli enklare att skriva multi session servers s{ger Peter.

H{r ges en lista |ver filerna med radantal och en kort kommentar. I
avsnitt 4.3 ges en lista |ver alla funktioner, b}de de som ing}r i
biblioteksinterfacet och de som bara anv{nds internt.

	------------------------------------------------------
	Rader	Filnamn		Kommentar
	------------------------------------------------------
	  128	EXAMPLE.c	Exempel p} anv{ndning av ISC
	  137	Makefile	Skapar libisc.a, nyttjar mkdep
	  626	README		Den h{r texten v{xer snabbast
	   91	TAGS		Den filen sk|ter Emacs om
	   32	TODO		Ideer, n}got f|r}ldrad
	   72	isc-internals.h	Interna funktioner, se kap 4
	  140	isc.3x		Mansida motsvarande kap 3
	  540	isc.c		Lejonparten av k{llkoden
	  249	isc.h		Biblioteksinterface, se kap 3
	   36	isc_abort.c	K{llkod, se avsnitt 4.3
	  107	isc_alloc.c	dito
	  103	isc_flush.c	dito
	   45	isc_log.c	dito
	   60	isc_msg.c	dito
	   39	isc_printf.c	dito
	   18	isc_putc.c	dito
	  127	isc_queue.c	dito
	   80	isc_session.c	dito
	   26	isc_shutdown.c	dito
	   51	isc_write.c	dito
	  684	printf.c	BSDs printf(), modifierad
	------------------------------------------------------


			3. Biblioteksinterface

Anv{ndare av ISC-biblioteket finner allt de beh|ver i filen isc.h och
i manualsidan isc(3). En del sockrade namn finns definierade i den
filen, h{r f|rs|ker jag ange de verkliga namnen.


			    3.1 Arbetss{tt

Under den h{r avdelningen finns massor av intressant kunskap i den
ordning den har dykt upp.


			3.1.1 Minnesallkering

ISC har hittills endast anv{nts i LysKOM-projektet. All allkoering av
minne sker d{r med egna rutiner som l{r vara smartare {n de vanliga
malloc(3) och free(). Funktionerna isc_malloc(), isc_realloc() och
isc_free() anropar de motsvarigheter till malloc(3), realloc(3) och
free(3) som definierats i ett tidigare anrop av isc_setallocfn(). Det
ges inga defaultv{rden h{r, ett anrop till isc_setallocfn() {r
tvunget.

Allokeringsfel, som skulle resultera i att motsvarigheten till
malloc(3) returnerar en nollpekare, anses s} ovanliga att de behandlas
som ett fatala fel, se annat avsnitt.


			   3.1.2 Fatala fel

N{r ett fatalt fel uppst}r anropas funktionen isc_abort() som anropar
den anv{ndarfunktion som definierats i ett tidigare anrop av
isc_setabortfn(). Anv{ndarfunktionen tar ett argument som {r en pekare
till en teckenstr{ng, l{mpligen inneh}llande information om felets
orsak.

Om isc_setabortfn() inte har anropats, skrivs ett meddelande p} stderr
och d{refter anropas abort(3). Observera att inga filer flushas d}.
(Meddelandet p} stderr avslutas med newline och vagnretur, s} den
borde flushas.)


			    3.2 Datatyper

De tv} viktigaste dataobjekten som ISC hanterar {r sessioner, som
representerar uppkopplingar (sessions), och pools, som {r samlingar
(pools) av sessioner som avs|ks (pollas) gemensamt. Meddelanden
Isc_msg {r en underordnad datatyp och events {r egentligen bara en
gruppering av n}gra returv{rden till en funktion.


			   3.2.1 Sessioner

Det kanske viktigaste dataobjektet i ISC {r sessionen. En session {r
en aktiv l{nk f|r data|verf|ring, det kan r|ra sig om en
TCP/IP-f|rbindelse eller en |ppnad fil. En session motsvaras av
Unixk{rnans file descriptors, som ju ocks} {r abstraktioner f|r m}nga
olika slags 
 En av tankarna med att skapa
en abstrakt datatyp som {r gemensam f|r all slags kommunikation {r att
underl{tta 
flera slag av
komunikation skall 
 Om det visar sig finnas behov
kunna erbjuda fler slag av kommunikation.

Typen Isc_session {r en struct Ett
sessionsblock inneh}ller inte mindre {n sexton element (members).
Bland de viktigare hittar vi fd som {r den file (eller socket)
descriptor som h|r till den h{r sessionen och hostname som pekar till
en textstr{ng med n{tverksnamnet f|r motpartens maskin.

Till varje session h|r tv} (pekare till) Isc_msgqueue k|er av
meddelanden: rd_msg_q f|r l{sning och wr_msg_q f|r skrivning.

Dessutom finns en s{ndbuffert f|r ett meddelande. Bufferten best}r av
en lagringsarea sendbuf och en r{knare sendindex.

 och en l{nk i
cirkul{rt l{nkad lista av sessioner (uppkopplingar).

			     3.2.2 Pools

Detta Master Control Block {r en struct som samlar en bunt med
sessioner. Den huvudsakliga anv{ndningen av ett ISCMCB masterblock {r
att man gemensamt kan polla alla uppkopplingar i blocket. Elementet
sessions {r en pekare till en cirkul{rt l{nkad lista av de ISCSCB
sessioner som lyder under masterblocket.

F|rutom att bevaka existerande uppkopplingar, lyssnar ISC {ven efter
nya uppkopplingar. ISC kan lyssna p} noll eller flera portar
samtidigt.  F|r detta finns i masterblocket ett element listenv
(lsiten vector) som {r en lista, d{r varje post i listan omfattar de
tv} element port (TCP/IP portnummer) och fd (file eller socket
descriptor) som representerar den port som ISC lyssnar (listen(2)) p}.
Se {ven eventet ISC_EVENT_LOGIN.

De |vriga elementen i masterblocket heter maxmsgsize, maxqueuesize och
maxdequeuesize. Vad kan de vara bra f|r?


			     3.2.3 Events

Detta Event Control Block {r en struct. En pekare till en s}dan struct
returneras av isc_getnextevent(). Ett ISCECB inneh}ller tre element
(members): event, session och msg. Elementet event av uppr{kningstypen
(enum) Isc_event_type avg|r vilket slag av event som har intr{ffat.
Elementet session {r en pekare till ett ISCSCB sessionsblock och talar
om i vilken session eventet intr{ffade. Elementet msg slutligen {r en
pekare till ett Isc_msg meddelande.

F|ljande slag av event kan intr{ffa:

	------------------------------------------------------
	Isc_event_type		Betydelse
	------------------------------------------------------
	ISC_EVENT_LOGIN		En ny session kopplades upp
	ISC_EVENT_LOGOUT	En session kopplades ner
	ISC_EVENT_MESSAGE	Ett meddelande anl{nde
	ISC_EVENT_TIMEOUT	Inget h{nde f|re timeout
	ISC_EVENT_ERROR		N}got annat h{nde
	------------------------------------------------------


			  3.2.4 Meddelanden

F|r "meddelanden" har grabbarna skapat en struct som heter Isc_msg.
Det {r en teckenbuffert (element buffer) med maximal och vutnyttjad
l{ngdangivelse (elementen size respektive length). Anv{ndaren verkar
komma i kontakt med datatypen endast efter anrop av isc_getnextevent()
eftersom ett meddelande ing}r som element msg i datatypen ISCECB
(se nedan).

Datatypen Isc_msg ing}r dock {ven i tv} k|er under varje ISCSCB
sessionsblock. K|erna har en egen datatyp Isc_msg_q_entry som {r en
l{nk i en linj{r dubbell{nkad (?) lista.


		       3.3 Biblioteksfunktioner

I avsnitt 4.3 finns en alfabetsordnad |versikt |ver samtliga ing}ende
funktioner.


		      3.3.1 Funktioner f|r pools


	ISCMCB *isc_initialize (void *cfg)

Funktionen allokerar ett masterblock och s{tter alla ing}ende element
till noll- eller defaultv{rden. Cfg {r reserverat f|r framtida bruk,
anropas f|r tillf{llet med NULL och ignoreras i funktionen. En pekare
till den allokerade structen returneras.


	void isc_shutdown (ISCMCB *mcb)

Funktionen st{nger file descriptorn f|r den port som masterblocket mcb
lyssnar p} efter inkommande uppkopplingar. F|r eventuella underlydande
sessioner anropas isc_close() innan masterblockets minnesutrymme
frias.


	int isc_listen (ISCMCB *mcb, int port)

Tala om f|r ISC p} vilken TCP/IP port ett visst masterblock skall
lyssna f|r inkommande uppkopplingar (sessioner). Om en port att lyssna
p} redan har definierats eller om argumentet port inte duger att
lyssna p}, returnerar funktionen -1. Om allt g}r bra, returneras 0.

Ett |nskem}l f|r framtiden vore att kunna ha mer {n en port att lyssna
p}. Min h|gst privata ide {r att man d} skulle ha ett ISCSCB
sessionsblock f|r varje port att lyssna p}, s} att det ISCECB
eventblock som isc_getnextevent() returnerar l{tt kan peka ut vilken
port (vilken session) som alstrade den nya sessionen.


	void isc_unlisten(ISCMCB *mcb, int port)

Stop to listen at a specified port


		    3.3.2 Funktioner f|r sessioner


	ISCSCB *isc_opentcp (ISCMCB *mcb, const char *host, int port)

Skapa ett nytt ISCSCB sessionsblock och koppla upp via TCP/IP till
angiven maskin och portnummer. Default maskinnamn {r "localhost". Den
nya sessionen underordnas angivet masterblock och en pekare till det
skapade ISCSCB sessionsblocket returneras.

Funktionen returnerar ett giltigt sessionsblock om allt gick bra. Om
ett fel intr{ffade, t ex att motparten inte kunde n}s, s} returneras
en nollpekare.


	void isc_close (ISCSCB *scb)

Funktionen st{nger den file descriptor som h|r till den angivna
sessionen. Om sessionen ing}r i en l{nkad lista, plockas den vackert
bort d{rifr}n. Eventuella meddelanden i sessionens s{nd- och
mottagningsk|er tas bort. Slutligen frias sessionsblockets
minnesutrymme.

Funktionen skall alltid anropas efter det att isc_getnextevent() har
returnerat ett ISCECB eventblock av typen ISC_EVENT_LOGOUT. Den kan
ocks} anv{ndas f|r att p} eget initiativ koppla ner en session.


	int isc_sessions (ISCMCB *sp)

Funktionen r{knar igenom hur m}nga sessioner som lyder (hur m}nga
ISCSCB sessionsblock som finns) under det angivna masterblocket.
Antalet returneras.


		   3.3.3 Funktioner f|r events


	ISCECB *isc_getnextevent(ISCMCB *mcb, long timeout)

Wait for an event on a specific server. Timeout is specified in
milliseconds BUGS: The pointer points to static data which is
overwritten on each call.

Tidigare returnerades en pekare till en statisk struct. Det meddelande
Isc_msg som utpekas av elementet msg i den statiska structen fick man
sj{lv fria eller ta hand om. Nu skall detta g|ras om till att
isc_getnextevent() allokerar en struct och att en ny funktion friar
den.


	void isc_dispose (ISCECB *event)

This should be used to discard an event when it has been processed.


		 3.3.4 Funktioner f|r att skicka data


	void isc_flush(ISCSCB *session)

Flush the transmit queue.


	int isc_write(ISCSCB *session,
		      const void *buffer, size_t length)

Put a buffer on the transmit queue


	int isc_putc(int chr, ISCSCB *session)

Put a single character on the transmit queue


	int isc_printf(ISCSCB *session, const char *format, ...)

Put a formatted string on the transmit queue


	void isc_setmaxmsgsize(ISCMCB *mcb, int size)

Set max size of messages for a server. If this function is not called a
default (currently 2048 bytes) is used.


		       3.3.5 \vriga funktioner

	void isc_setlogfn(void (*logfn)(const char *format, va_list AP))


	void isc_setallocfn(void * (*mallocfn)(size_t size),
			    void * (*reallocfn)(void *buf, size_t size),
			    void   (*freefn)(void *buf))


	void isc_setabortfn(void (*abortfn)(const char *msg))


			 4. Intern uppbyggnad
				   

		      4.1 Teoretisk beskrivning


			    4.2 Datatyper


			    4.3 Funktioner

H{r {r en snabb alfabetsordnad |versikt |ver samtliga ing}ende
funktioner. Funktioner som ing}r i biblioteksinterfacet beskrivs i
kapitel 3. De tas med i den h{r tabellen f|r fullst{ndighets skull.

	------------------------------------------------------
	Avsnitt	Funktion		K{llkodsfil
	------------------------------------------------------
	4.3.	isc_abort()		isc_abort.c
	4.3.	isc_addsession()	isc.c
	4.3.	isc_allocmsg()		isc_msg.c
	3.3.2	isc_close()		isc_session.c
	3.3.3	isc_dispose()		isc.c
	4.3.	isc_fill_in_session()	isc.c
	3.3.4	isc_flush()		isc_flush.c
	4.3.	isc_free()		isc_alloc.c
	4.3.	isc_freemsg()		isc_msg.c
	4.3.	isc_gethostname()	isc.c
	3.3.3	isc_getnextevent()	isc.c
	3.3.1	isc_initialize()	isc.c
	4.3.	isc_killqueue()		isc_queue.c
	3.3.1	isc_listen()		isc.c
	4.3.	isc_log()		isc_log.c
	4.3.	isc_malloc()		isc_alloc.c
	4.3.	isc_mkstrmsg()		isc_msg.c
	4.3.	isc_newqueue()		isc_queue.c
	4.3.	isc_openport()		isc.c
	3.3.2	isc_opentcp()		isc.c
	4.3.	isc_pollqueue()		isc_queue.c
	4.3.	isc_popqueue()		isc_queue.c
	3.3.4	isc_printf()		isc_printf.c
	4.3.	isc_pushqueue()		isc_queue.c
	3.3.4	isc_putc()		isc_putc.c
	4.3.	isc_readmsg()		isc.c
	4.3.	isc_realloc()		isc_alloc.c
	4.3.	isc_reallocmsg()	isc_msg.c
	3.3.2	isc_sessions()		isc_session.c
	3.3.5	isc_setabortfn()	isc_abort.c
	3.3.5	isc_setallocfn()	isc_alloc.c
	3.3.5	isc_setlogfn()		isc_log.c
	3.3.4	isc_setmaxmsgsize()	isc.c
	3.3.1	isc_shutdown()		isc_shutdown.c
	4.3.	isc_sizequeue()		isc_queue.c
	4.3.	isc_strdup()		isc_alloc.c
	4.3.	isc_topqueue()		isc_queue.c
	3.3.1	isc_unlisten()		isc.c
	3.3.4	isc_write()		isc_write.c
	------------------------------------------------------

	void isc_log (const char *format, ...)

	void isc_abort (const char *message)

	void *isc_malloc (size_t length)

	void *isc_realloc (void *buf, size_t length)

	void isc_free (void *buf)

	char *isc_strdup (const char *str)

	Isc_msgqueue *isc_newqueue (void)

	int isc_pollqueue (Isc_msgqueue *queue)

	void isc_killqueue (Isc_msgqueue *queue)

	void isc_pushqueue (Isc_msgqueue *queue, Isc_msg *msg)

	Isc_msg *isc_topqueue (Isc_msgqueue *queue)

	int isc_sizequeue (Isc_msgqueue *queue)

	Isc_msg *isc_popqueue (Isc_msgqueue *queue)

	Isc_msg *isc_allocmsg (size_t size)

	Isc_msg *isc_reallocmsg (Isc_msg *msg, size_t size)


	void isc_freemsg(Isc_msg *msg)

This should be used to discard the message when it is processed.

Funktionen l}g tidigare i kapitel 3, men flyttades hit n{r den under
isc_getnextevent() omtalade f|r{ndringen genomf|rdes.


	Isc_msg *isc_mkstrmsg (const char *str)

