/*
** isc_wait.c				wait for connects to succeed
**
** Copyright (C) 1996, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
**    960511 ceder     initial coding.
** (See ChangeLog for recent history)
*/

#ifdef TIME_WITH_SYS_TIME
#  include <time.h>
#endif
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <errno.h>
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif

#include "isc.h"
#include "intern.h"

/* This return NULL if there is no connecting session,
   or an event of type ISC_EVENT_ERROR, ISC_EVENT_TIMEOUT,
   ISC_EVENT_REJECTED or ISC_EVENT_CONNECTED.
   */
IscEvent *
isc_wait(IscMaster *mcb, long timeout)
{
  struct timeval    wait;
  fd_set            write_set;
  IscEvent        * event = NULL;
  IscSessionList  * isl;
  IscSession      * scb;
  IscAddress      * ia;
  int               nfds;
  
  /* Set up the list of file descriptors to wait on */
  FD_ZERO(&write_set);
  nfds = -1;

  /*
   * Go thru the list of sessions, add to read_set and handle LOGOUT
   */
  for (isl = mcb->sessions; isl != NULL; isl = isl->next)
  {
    scb = isl->scb;

    /*
     * Add to the write set if in state CONNECTING.
     */
    if (scb->state == ISC_STATE_CONNECTING)
    {
      FD_SET(scb->fd, &write_set);
      if (nfds <= scb->fd)
	nfds = scb->fd + 1;
    }

    /*
     * Terminate at end of circular ring
     */
    if (isl->next == mcb->sessions)
      break;
  }

  /*
   * Return immediately if there are no CONNECTING sessions.
   */
  if (nfds == -1)
  {
    return NULL;
  }

  /* Allocate an event */
  ISC_XNEW(event);
  event->msg     = NULL;
  event->session = NULL;

  /*
   * Set up the timeout structure.
   */
  wait.tv_sec  = timeout / 1000;
  wait.tv_usec = (timeout % 1000L) * 1000L;

  /* Wait for event */
  nfds = select(FD_SETSIZE, NULL, &write_set, NULL, &wait);

  /* Error in select? */
  if (nfds < 0)
  {
    event->event = ISC_EVENT_ERROR;
    event->msg   = isc_mkstrmsg("Error in select()");
    return event;
  }
  
  /* Timeout? */
  if (nfds == 0)
  {
    event->event = ISC_EVENT_TIMEOUT;
    return event;
  }
  
  for (isl = mcb->sessions; isl != NULL; isl = isl->next)
  {
    scb = isl->scb;

    
    if (FD_ISSET(scb->fd, &write_set))
      switch (scb->state)
      {
	case ISC_STATE_CONNECTING:
	  ia = isc_getraddress(scb);
	  if (ia == NULL)
	  {
	    /*
	     * Connect request failed
	     */
	    scb->state = ISC_STATE_CLOSING;
	    scb->isc_errno = errno;
	    event->event = ISC_EVENT_REJECTED;
	    event->session = scb;
	  }
	  else
	  {
	    /*
	     * Connect request acknowledged
	     */
	    isc_free(ia);
	    scb->state = ISC_STATE_RUNNING;
	    event->event = ISC_EVENT_CONNECTED;
	    event->session = scb;
	  }
	  
	  /* Move the session pointer to the next in the circular queue */
	  mcb->sessions = mcb->sessions->next;
      
	  return event;

	default:
	  /* Major fuck-up somewhere, this should not happen... */
	  event->event   = ISC_EVENT_ERROR;
	  event->session = scb;
	  event->msg     = isc_mkstrmsg(
		       "Should not happen; illegal state for write FD_SET");
	  
	  /* Move the session pointer to the next in the circular queue */
	  mcb->sessions = mcb->sessions->next;
	  
	  return event;
      }
    
    if (isl->next == mcb->sessions)
      break;
  }
  
  /* Major fuck-up somewhere, something else should have happened... */
  event->event   = ISC_EVENT_ERROR;
  event->session = NULL;
  event->msg     = isc_mkstrmsg(
	"Should not happen; nothing happend in isc_wait");
  return event;
}
