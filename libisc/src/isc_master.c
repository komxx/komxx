/*
** isc_master.c                               IscMaster control functions
**
** Copyright (C) 1991-1992, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved to separate file
** (See ChangeLog for recent history)
*/

#include <time.h>
#include <errno.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <sys/types.h>
#include <sys/file.h>
#include <sys/socket.h>
#ifndef NULL
#  include <stdio.h>
#endif

#include "isc.h"
#include "intern.h"



/*
** This routine will initialize the ISC subsystem
*/
IscMaster *
isc_initialize(IscConfig  * cfg)
{
  IscMaster         * mcb;
  IscMasterConfig   * mcfg;
  IscSessionConfig  * scfg;

  
  /* Handle user specified defaults */
  if (cfg)
    switch (cfg->version)
    {
      case 0:
        break;
	
      case 1006:
	mcfg = &cfg->master;
	
	switch (mcfg->version)
	{
	  case 0:
	    break;
	    
	  case 1001:
	    if (mcfg->memfn.alloc &&
		mcfg->memfn.realloc &&
		mcfg->memfn.free)
	      isc_setallocfn(mcfg->memfn.alloc,
			     mcfg->memfn.realloc,
			     mcfg->memfn.free);
	    if (mcfg->abortfn)
	      isc_setabortfn(mcfg->abortfn);
	    break;

	  default:
	    errno = EINVAL;
	    return NULL;
	}
	break;

      default:
	errno = EINVAL;
	return NULL;
    }
  
  ISC_XNEW(mcb);
  
  mcb->sessions = NULL;
  
  
  /* Setup default values */
  mcb->scfg.max.msgsize     = ISC_DEFAULT_MAX_MSG_SIZE;
  mcb->scfg.max.queuedsize  = ISC_DEFAULT_MAX_QUEUED_SIZE;
  mcb->scfg.max.dequeuelen  = ISC_DEFAULT_MAX_DEQUEUE_LEN;
  mcb->scfg.max.openretries = ISC_DEFAULT_MAX_OPEN_RETRIES;
  mcb->scfg.max.backlog     = ISC_DEFAULT_MAX_BACKLOG;
  mcb->scfg.fd_relocate     = 0;
  
  /* Handle user specified defaults */
  if (cfg)
    switch (cfg->version)
    {
      case 1006:
        scfg = &cfg->session;
	
	switch (scfg->version)
	{
	  case 0:
	    break;
	    
	  case 1002:
	    if (scfg->max.msgsize >= 0)
	      mcb->scfg.max.msgsize = scfg->max.msgsize;
	    if (scfg->max.queuedsize >= 0)
	      mcb->scfg.max.queuedsize = scfg->max.queuedsize;
	    if (scfg->max.dequeuelen >= 0)
	      mcb->scfg.max.dequeuelen = scfg->max.dequeuelen;
	    if (scfg->max.openretries >= 0)
	      mcb->scfg.max.openretries = scfg->max.openretries;
	    if (scfg->max.backlog >= 0)
	      mcb->scfg.max.backlog = scfg->max.backlog;
	    if (scfg->fd_relocate > 0)
	      mcb->scfg.fd_relocate = scfg->fd_relocate;
	    break;

	  default:
	    isc_free(mcb);
	    errno = EINVAL;
	    return NULL;
	}

	break;
	
      default:
	isc_free(mcb);
	errno = EINVAL;
	return NULL;
    }
  
  return mcb;
}



/*
** Close and destroy all sessions associated with a MCB, then
** destroy the MCB too.
*/
void
isc_shutdown(IscMaster  * mcb)
{
  while (mcb->sessions)
    isc_destroy(mcb, mcb->sessions->scb);

  isc_free(mcb);
}
