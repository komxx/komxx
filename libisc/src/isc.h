/*
** isc.h                        structures and defines used in a ISC server
**
** Copyright (C) 1991, 1996, 1998-1999, 2001 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910306 pen      major overhaul
** 910307 pen      type name changes, changes in structs..
** 920207 pen      updated
** (See ChangeLog for recent history)
*/

#ifndef __ISC_H__
#define __ISC_H__


/*
** Give the poor user a chance to change it
*/
#ifndef ISC_UDGTYPE
#  define ISC_UDGTYPE void
#endif



/*
** Some type names (typedef? What is that? :-)
*/
#define IscSessionConfig	struct isc_session_cfg
#define IscMasterConfig		struct isc_master_config
#define IscConfig		struct isc_config
#define IscAddress		union isc_address
#define IscMessage		struct isc_msg
#define IscMsgQE		struct isc_msg_q_entry
#define IscMsgQueue		struct isc_msgqueue
#define IscSessionInfo		union isc_session_info
#define IscSessionStats		struct isc_session_stats
#define IscHandler		struct isc_handler
#define IscHandlerList		struct isc_handler_list
#define IscHandlerCache		struct isc_handler_cache
#define IscSession		struct isc_scb
#define IscSessionList		struct isc_scb_entry
#define IscMaster		struct isc_mcb
#define IscEvent		struct isc_ecb



/*
** Some nice defaults
*/
#define ISC_DEFAULT_MAX_MSG_SIZE      2048
#define ISC_DEFAULT_MAX_QUEUED_SIZE   300
#define ISC_DEFAULT_MAX_DEQUEUE_LEN   10
#define ISC_DEFAULT_MAX_OPEN_RETRIES  10
#define ISC_DEFAULT_MAX_BACKLOG       5



/*
** Configuration structure for sessions
*/
IscSessionConfig
{
  int version;     /* This is version 1002 */
  struct
  {
    int msgsize;
    int queuedsize;
    int dequeuelen;
    int openretries;
    int backlog;
  } max;
  int fd_relocate;
};



/*
** Master configuration structure
*/
IscMasterConfig
{
  int version; /* This is version 1001 */
  struct
  {
    void * (*alloc)   (size_t size);
    void * (*realloc) (void *bufp, size_t size);
    void   (*free)    (void *bufp);
  } memfn;
  void (*abortfn)(const char *msg);
};



/*
** Configuration structure
*/
IscConfig
{
  int               version;  /* This is version 1006 */
  IscSessionConfig  session;
  IscMasterConfig   master;
};



/*
** Various types of events that may happen
*/
typedef enum 
{
  ISC_EVENT_ERROR,
  ISC_EVENT_TIMEOUT,
  ISC_EVENT_LOGIN,
  ISC_EVENT_LOGIN_UNRELOCATED,
  ISC_EVENT_LOGOUT,
  ISC_EVENT_MESSAGE,
  ISC_EVENT_CONNECTED,
  ISC_EVENT_REJECTED
} IscEventType;



/*
** Various types of sessions
*/
typedef enum
{
  ISC_TYPE_UNKNOWN,
  ISC_TYPE_TCP,
  ISC_TYPE_PIPE,
  ISC_TYPE_FILE,
  ISC_TYPE_UDP
} IscSessionType;



/*
** The different session states
*/
typedef enum
{
  ISC_STATE_UNKNOWN,
  ISC_STATE_CONNECTING,
  ISC_STATE_RUNNING,
  ISC_STATE_DISABLED,
  ISC_STATE_CLOSING,
  ISC_STATE_CLOSING2,		/* An ISC_EVENT_LOGOUT has been generated. */
  ISC_STATE_LISTENING
} IscSessionState;



/*
** Message information structure
*/
IscAddress
{
  struct
  {
    struct sockaddr saddr;
  } ip;
};



/*
** Generic message type
*/
IscMessage
{
  int               size;      	/* Maximum buffer size */
  int               length;    	/* Length of used buffer */
  char            * buffer;  	/* Pointer to buffer */
  IscAddress      * address;	/* Pointer to address */
};



/*
** Entry in a message queue
*/
IscMsgQE
{
  IscMsgQE    * prev;
  IscMsgQE    * next;
  IscMessage  * msg;
};



/*
** The generic message queue
*/
IscMsgQueue
{
  IscMsgQE  * head;
  IscMsgQE  * tail;
  int         entries;
};



/*
** Session information structures
*/
IscSessionInfo
{
  struct
  {
    IscAddress *raddr;
    IscAddress *laddr;
  } tcp;
  struct
  {
    IscAddress *raddr;
    IscAddress *laddr;
  } udp;
  struct
  {
    char  * pathname;
  } file;
  struct
  {
    char  * pathname;
  } pipe;
};



/*
** Statistical data about a session
*/
IscSessionStats
{
  struct
  {
    long bytes;
    long packets;
  } rx, tx;
};


IscHandlerCache
{
  IscHandlerList  * read;
  IscHandlerList  * write;
  IscHandlerList  * close;
  IscHandlerList  * poll;
  IscHandlerList  * accept;
  IscHandlerList  * destroy;
  IscHandlerList  * parse;
};



/*
** Session control structure
*/
IscSession
{
  int                 nlinks;
  
  IscSessionType      type;
  IscSessionState     state;
  int                 fd;
  int		      isc_errno;
  
  IscMsgQueue       * rd_msg_q;
  IscMsgQueue       * wr_msg_q;

  IscSessionConfig  * cfg;
  IscSessionInfo      info;
  
  /* Cheap transmit buffer */
  /* Should be dynamically allocated - into an IscMessage */
  char                sendbuf[2048];
  int                 sendindex;
  
  time_t              logintime;
  time_t	      idlesince;

  IscSessionStats     stats;

  IscHandlerList    * handlers;
  
  ISC_UDGTYPE       * udg;   /* User defined garbage :-) */
};




IscHandler
{
  IscMessage *  (*read)    (IscHandlerList *,
			    IscSession *scb);
  
  int		  (*write)   (IscHandlerList *hl,
			      IscSession *scb,
			      IscMessage *msg);
  
  int		  (*close)   (IscHandlerList *hl,
			      IscSession *scb);
  
  int		  (*poll)    (IscHandlerList *hl,
			      IscSession *scb);
  
  IscSession *  (*accept)  (IscHandlerList *hl,
			    IscSession *scb,
			    IscMessage *msg);
  
  void          (*destroy) (IscHandlerList *hl,
			    IscSession *scb);
  
  IscEvent *    (*parse)   (IscHandlerList *hl,
			    IscSession *scb,
			    IscMessage *msg);
};



IscHandlerList
{
  IscHandlerList  * next;
  IscHandler      * hcb;
  IscHandlerCache   current;
};



/*
** Session list entry
*/
IscSessionList
{
  IscSessionList  * prev;
  IscSessionList  * next;
  
  IscSession * scb;
};
  


/*
** The Master Control Block
*/
IscMaster
{
  IscSessionConfig    scfg;
  IscSessionList    * sessions;
};



/*
** Event control structure
*/
IscEvent
{
  IscEventType      event;
  IscSession      * session;
  IscMessage      * msg;
  IscSession	  * listen_session;
};



/*
** This routine will setup the ISC subsystem
*/
extern IscMaster *
isc_initialize(IscConfig  * cfg);



/*
** Shut down all sessions associated with an ISC Master Control Block
** and deallocate all storage used by the MCB.
*/
extern void
isc_shutdown(IscMaster  * mcb);



/*
** Establish a TCP port to listen for connections at
*/
extern IscSession *
isc_listentcp(IscMaster   * mcb,
	      const char  * address,
	      const char  * service);



/*
** Initiate a new session with a previously opened file
*/
extern IscSession *
isc_openfd(IscMaster  * mcb,
	   int          fd);



/*
** Initiate a new session with a specified file
*/
extern IscSession *
isc_openfile(IscMaster   * mcb,
	     const char  * pathname,
	     int           openmode);



/*
** Initiate a new session with a remote TCP service
*/
extern IscSession *
isc_opentcp(IscMaster   * mcb,
	    const char  * address,
	    const char  * service);



/*
** Initiate a new session with a remote UDP service
*/
extern IscSession *
isc_openudp(IscMaster   * mcb,
	    const char  * address,
	    const char  * service);



extern void
isc_close(IscSession  * scb);



extern int
isc_destroy(IscMaster  * mcb, IscSession  * scb);



/*
** This function returns the number of sessions
*/
extern int
isc_sessions(IscMaster  * mcb);



/*
** Wait for an event on a specific server. Timeout is specified
** in milliseconds
*/
extern IscEvent *
isc_getnextevent(IscMaster  * mcb,
		 long         timeout);



/*
** Wait for pending connections.
*/
IscEvent *
isc_wait(IscMaster *mcb, long timeout);
 
/*
** This should be used to discard an event when it has been processed.
*/
extern void
isc_dispose(IscEvent  * ecb);



/*
** Flush the transmit queue for a specific session
*/
extern void
isc_flush(IscSession  * scb);



/*
** Add a new message to the transmit queue
*/
extern int
isc_send(IscSession  * scb, IscMessage  * mcb);

extern int
isc_sendto(IscSession  * scb,
	   IscAddress  * ap,
	   IscMessage  * mcb);



/*
** Put a buffer on the transmit queue for a session
*/
extern int
isc_write(IscSession  * scb,
	  const void  * buffer,
	  size_t        length);



/*
** Put a single character on the transmit queue
*/
extern int
isc_putc(int           chr,
	 IscSession  * scb);

/*
** Put a NUL-terminated string on the transmit queue (the NUL is not sent).
*/
extern int
isc_puts(const char *str,
	 IscSession *scb);

/*
** Put a decimal representation of ``nr'' on the transmit queue.
*/
extern int isc_putul(unsigned long nr, IscSession *scb);



/*
** Allocate a new IscMessage of specified size
*/
extern IscMessage *
isc_allocmsg(size_t  size);



/*
** Reallocate an old IscMessage
*/
extern IscMessage *
isc_reallocmsg(IscMessage  * msg,
	       size_t        size);



/*
** Make a copy of an old IscMessage
*/
extern IscMessage *
isc_copymsg(IscMessage  *msg);



/*
** Free an allocated IscMessage
*/
extern void
isc_freemsg(IscMessage  * msg);



/*
** Make an IscMessage out of a string
*/
extern IscMessage *
isc_mkstrmsg(const char  * str);



/*
** Enable a previously disabled session
*/
extern int
isc_enable(IscSession *scb);



/*
** Disable a session. The session will not generate events.
*/
extern int
isc_disable(IscSession *scb);


extern int
isc_insert(IscMaster *mcb, IscSession *scb);

extern int
isc_remove(IscMaster *mcb, IscSession *scb);


extern IscSession *
isc_create(IscSessionConfig *cfg, IscHandler *fun);


extern IscSession *
isc_createtcp(IscSessionConfig *cfg, int fd);


extern IscSession *
isc_createudp(IscSessionConfig *cfg);


extern int
isc_bindtcp(IscSession *scb, const char *address, const char *port);

extern int
isc_bindudp(IscSession *scb, const char *address, const char *port);

extern int
isc_connecttcp(IscSession *scb, const char *address, const char *port);

extern int
isc_connectudp(IscSession *scb, const char *address, const char *port);


/*
** Establish an UDP port to listen for incoming messages at
*/
extern IscSession *
isc_listenudp(IscMaster   * mcb,
	      const char  * address,
	      const char  * service);



extern IscAddress *
isc_getladdress(IscSession *scb);


extern IscAddress *
isc_getraddress(IscSession *scb);


extern void
isc_freeaddress(IscAddress *addr);


extern IscAddress *
isc_copyaddress(IscAddress *addr);

extern char *
isc_getipnum(IscAddress *ia,
	     char *address,
	     int len);

extern char *
isc_gethostname(IscAddress *ia,
		char *address,
		int len);

extern int
isc_getportnum(IscAddress *ia);


extern char *
isc_getservice(IscAddress *sa,
	       char *buf,
	       int len,
	       IscSessionType type);


extern IscHandler *
isc_newhandler(void);

extern void
isc_freehandler(IscHandler *hcb);

extern IscHandler *
isc_copyhandler(IscHandler *hcb);

extern void
isc_pushhandler(IscSession *scb, IscHandler *hcb);

extern IscHandler *
isc_pophandler(IscSession *scb);



/*
** Handler call function macros
*/
#define ISC_HCALLFUN1(HLIST,NAME,ARG) \
  (*(HLIST)->current.NAME->hcb->NAME)((HLIST), (ARG))

#define ISC_HCALLFUN2(HLIST,NAME,A1,A2) \
  (*(HLIST)->current.NAME->hcb->NAME)((HLIST), (A1), (A2))

#define ISC_HCALLFUN3(HLIST,NAME,A1,A2,A3) \
  (*(HLIST)->current.NAME->hcb->NAME)((HLIST), (A1), (A2), (A3))
	


#endif /* __ISC_H__ */
