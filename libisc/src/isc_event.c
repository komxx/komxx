/*
** isc_event.c            		 definitions of ISC subsystem routines
**
** Copyright (C) 1990-1992, 1996, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
**    900403 pen       initial coding.
**    900612 pen       rewrote the message buffering.
**    901102 pen       fixed bug in isc_gethostname().
**    910303 ceder     clean up. Removed everything that is lyskom-specific.
**    910304 pen       really removed everything lyskom-specific.. :-)
**    920129 pen       added support for "lazy" connect
**    920805 pen       one unneccessary isc_pollqueue() removed.
** (See ChangeLog for recent history)
*/

#include <stdio.h>
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#ifdef TIME_WITH_SYS_TIME
#  include <time.h>
#endif
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <errno.h>
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_SYS_SELECT_H
#  include <sys/select.h>
#endif
#ifdef HAVE_UNISTD_H
#  include <unistd.h>	/* Needed on NetBSD1.1/SPARC due to select */
#endif
#ifdef HAVE_STRING_H
#  include <string.h>	/* Needed on NetBSD1.1/SPARC due to bzero */
#endif

#include "isc.h"
#include "intern.h"



IscEvent *
isc_getnextevent(IscMaster *mcb, long timeout)
{
  struct timeval    wait;
  fd_set            read_set;
  fd_set            write_set;
  fd_set            error_set;
  IscEvent        * event;
  IscEvent        * event2;
  IscSessionList  * isl;
  IscSession      * scb;
  IscSession      * new_scb;
  IscMessage      * msg;
  IscAddress      * ia;
  int               nfds;
  int		    numwaitmsgs;
  

  /* Allocate an event */
  ISC_XNEW(event);
  event->msg     = NULL;
  event->session = NULL;
  event->listen_session = NULL;

RETRY:
  /* Set up the list of file descriptors to wait on */
  FD_ZERO(&read_set);
  FD_ZERO(&write_set);
  FD_ZERO(&error_set);

  /* Number of sessions with queued messages */
  numwaitmsgs = 0;
  
  /*
   * Go thru the list of sessions, add to read_set and handle LOGOUT
   */
  for (isl = mcb->sessions; isl != NULL; isl = isl->next)
  {
    scb = isl->scb;

    if (scb->state == ISC_STATE_CLOSING)
    {
      /*
      ** If we are in state CLOSING, return all outstanding messages
      ** before returning event LOGOUT unless fd still open and outgoing
      ** messages exists.
      */
      event->session = scb;
      event->msg = isc_popqueue(scb->rd_msg_q);
      if (event->msg)
      {
	event->event = ISC_EVENT_MESSAGE;
	
	/* Move the session pointer to the next in the circular queue */
	mcb->sessions = mcb->sessions->next;
	
	return event;
      }
      else
	if (scb->fd == -1 || !isc_pollqueue(scb->wr_msg_q))
	{
	  event->event = ISC_EVENT_LOGOUT;
	  scb->state = ISC_STATE_CLOSING2;
	
	  /* Move the session pointer to the next in the circular queue */
	  mcb->sessions = mcb->sessions->next;
	  
	  return event;
	}
        else
	  event->session = NULL;
    }
      
    /*
     * Add to the write set if in state CONNECTING or RUNNING and there
     * are messages in the outgoing queue.
     */
    if (scb->state == ISC_STATE_CONNECTING ||
	((scb->state == ISC_STATE_RUNNING ||
	  scb->state == ISC_STATE_DISABLED) &&
	 (scb->sendindex > 0 || isc_pollqueue(scb->wr_msg_q))))
      FD_SET(scb->fd, &write_set);

    /*
     * Add to the read set if state RUNNING or LISTENING
     */
    if (scb->state == ISC_STATE_RUNNING ||
	scb->state == ISC_STATE_LISTENING)
      FD_SET(scb->fd, &read_set);

    /*
     * Increment the count of sessions with waiting messages
     */
    if (isc_pollqueue(scb->rd_msg_q))
      numwaitmsgs++;
    
    /*
     * Terminate at end of circular ring
     */
    if (isl->next == mcb->sessions)
      break;
  }

  /*
   * Set up the timeout structure: If there are queued messages,
   * do not wait in select, else use user supplied timeout limit
   */
  if (numwaitmsgs > 0)
  {
    wait.tv_sec  = 0;
    wait.tv_usec = 0;
  }
  else
  {
    wait.tv_sec  = timeout / 1000;
    wait.tv_usec = (timeout % 1000L) * 1000L;
  }

  /* Wait for event */
  nfds = select(FD_SETSIZE, &read_set, &write_set, &error_set, &wait);

  /* Error in select? */
  if (nfds < 0)
  {
    event->event = ISC_EVENT_ERROR;
    event->msg   = isc_mkstrmsg("Error in select()");
    return event;
  }
  
  /* Timeout? */
  if (nfds == 0 && numwaitmsgs == 0)
  {
    event->event = ISC_EVENT_TIMEOUT;
    return event;
  }
  
  for (isl = mcb->sessions; isl != NULL; isl = isl->next)
  {
    scb = isl->scb;

    
    if (FD_ISSET(scb->fd, &write_set))
      switch (scb->state)
      {
	case ISC_STATE_RUNNING:
        case ISC_STATE_DISABLED:
          isc_oflush(scb);
	  break;

	case ISC_STATE_CONNECTING:
	  ia = isc_getraddress(scb);
	  if (ia == NULL)
	  {
	    /*
	     * Connect request failed
	     */
	    scb->state = ISC_STATE_CLOSING;
	    scb->isc_errno = errno;
	    event->event = ISC_EVENT_REJECTED;
	    event->session = scb;
	  }
	  else
	  {
	    /*
	     * Connect request acknowledged
	     */
	    isc_free(ia);
	    scb->state = ISC_STATE_RUNNING;
	    event->event = ISC_EVENT_CONNECTED;
	    event->session = scb;
	  }
	  
	  /* Move the session pointer to the next in the circular queue */
	  mcb->sessions = mcb->sessions->next;
      
	  return event;

	default:
	  /* Major fuck-up somewhere, this should not happen... */
	  event->event   = ISC_EVENT_ERROR;
	  event->session = scb;
	  event->msg     = isc_mkstrmsg(
		       "Should not happen; illegal state for write FD_SET");
	  
	  /* Move the session pointer to the next in the circular queue */
	  mcb->sessions = mcb->sessions->next;
	  
	  return event;
      }
    
    if (isl->next == mcb->sessions)
      break;
  }
  
  /* Check for messages from sessions */
  for (isl = mcb->sessions; isl != NULL; isl = isl->next)
  {
    scb = isl->scb;

    if (FD_ISSET(scb->fd, &read_set) &&
	!(scb->type == ISC_TYPE_TCP && scb->state == ISC_STATE_LISTENING))
    {
      time(&scb->idlesince);
      
      msg = ISC_SCALLFUN1(scb, read, scb);
      if (msg)
      {
	scb->stats.rx.bytes += msg->length;
	scb->stats.rx.packets++;
	
	if (scb->handlers->current.parse)
	{
	  event2 = ISC_SCALLFUN2(scb, parse, scb, msg);
	  if (event2)
	  {
	    isc_free(event);
	    return event2;
	  }
	}
	
	isc_pushqueue(scb->rd_msg_q, msg);
      }
      else
      {
	scb->state = ISC_STATE_CLOSING;
	scb->isc_errno = errno;
      }
    }
    
    if (isl->next == mcb->sessions)
      break;
  }


  /* New connection? */
  for (isl = mcb->sessions; isl != NULL; isl = isl->next)
  {
    scb = isl->scb;

    if (scb->state == ISC_STATE_LISTENING &&
	FD_ISSET(scb->fd, &read_set) &&
	scb->handlers->current.accept != NULL)
    {
      new_scb = ISC_SCALLFUN2(scb, accept, scb, NULL);
      if (new_scb == NULL)
      {
	event->event   = ISC_EVENT_ERROR;
	event->session = NULL;
	event->msg     = isc_mkstrmsg("Failed isc_getnextevent() "
				      "in new session");
	return event;
      }
	    
      /* Fill in the event info structure */
      event->session = new_scb;
      event->event   = ISC_EVENT_LOGIN;

      if (new_scb->cfg->fd_relocate > 0
	  && new_scb->fd <= new_scb->cfg->fd_relocate)
	event->event = ISC_EVENT_LOGIN_UNRELOCATED;

      event->listen_session = scb;

      /* Move the session pointer to the next in the circular queue */
      mcb->sessions = mcb->sessions->next;

      isc_insert(mcb, new_scb);
      return event;
    }
    
    if (isl->next == mcb->sessions)
      break;
  }
  
  /* Return one of the received messages from the clients */
  for (isl = mcb->sessions; isl != NULL; isl = isl->next)
  {
    scb = isl->scb;

    
    if ((event->msg = isc_popqueue(scb->rd_msg_q)))
    {
      event->session = scb;
      event->event = ISC_EVENT_MESSAGE;
      
      /* Move the session pointer to the next in the circular queue */
      mcb->sessions = mcb->sessions->next;
      
      return event;
    }

    if (isl->next == mcb->sessions)
      break;
  }

  goto RETRY; /* This happens if/when only transmits were pending... */
}



/*
** Get rid of the specified event
*/
void
isc_dispose(IscEvent *ep)
{
  if (!ep)
    return;

  if (ep->msg)
    isc_freemsg(ep->msg);

  isc_free(ep);
}


