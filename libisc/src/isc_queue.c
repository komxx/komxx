/*
** isc_queue.c                           Handle queues for the ISC subsystem
**
** Copyright (C) 1991, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved to separate file
** (See ChangeLog for recent history)
*/

#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#ifndef NULL
#  include <stdio.h>
#endif
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "isc.h"
#include "intern.h"


IscMsgQueue *
isc_newqueue(void)
{
    IscMsgQueue  * msg_q;

    
    msg_q = (IscMsgQueue *) isc_malloc(sizeof(IscMsgQueue));
    msg_q->head = NULL;
    msg_q->tail = NULL;
    msg_q->entries = 0;
    
    return msg_q;
}


/* Return TRUE if the queue is not empty. */
#ifndef isc_pollqueue
int
isc_pollqueue(IscMsgQueue  * queue)
{
  return queue != NULL && queue->head != NULL;
}
#endif

void
isc_killqueue(IscMsgQueue  * queue)
{
    IscMsgQE  * mqe;
    IscMsgQE  * prev;


    if (queue == NULL)
      return;
    
    if ( queue->head != NULL )
    {
	mqe = queue->head;
	while ( mqe != NULL )
	{
	    prev = mqe;
	    mqe = mqe->next;
	    isc_freemsg(prev->msg);
	    isc_free(prev);
	}
    }

    isc_free(queue);
    return;
}



/*
** Push a message onto a queue. It is legal to push a message onto the
** the NULL queue and will be equal to freeing that message
*/
void
isc_pushqueue(IscMsgQueue  * queue,
	      IscMessage   * msg)
{
  IscMsgQE  * mqe;


  if (queue == NULL)
  {
    isc_freemsg(msg);
    return;
  }
  
  mqe = (IscMsgQE *) isc_malloc(sizeof(IscMsgQE));
  
  mqe->msg  = msg;
  mqe->prev = queue->tail;
  mqe->next = NULL;

  if (queue->head == NULL)
    queue->head = mqe;
  
  if (queue->tail)
    queue->tail->next = mqe;

  queue->tail = mqe;
  queue->entries++;
}


IscMessage *
isc_topqueue(IscMsgQueue  * queue)
{
    if (queue != NULL && queue->head != NULL)
        return queue->head->msg;
    else
        return NULL;
}


int
isc_sizequeue(IscMsgQueue  * queue)
{
  if (queue)
    return queue->entries;
  else
    return -1;
}
	      

IscMessage *
isc_popqueue(IscMsgQueue  * queue)
{
    IscMsgQE    * mqe;
    IscMessage  * msg;

    
    if (queue != NULL && queue->head != NULL)
    {
	mqe = queue->head;
	msg = mqe->msg;
	queue->head = mqe->next;

	if (queue->head)
	    queue->head->prev = NULL;
	else
	    queue->tail = NULL;

	queue->entries--;
	isc_free(mqe);
	return msg;
    }
    else
	return NULL;
}
