/*
** isc_handler.c                 Routines to handle IscSession handlers
**
** Copyright (C) 1992, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 920211 pen      initial coding
** (See ChangeLog for recent history)
*/

#include <stdio.h>
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>

#include "isc.h"
#include "intern.h"



IscHandler *
isc_copyhandler(IscHandler *hcb)
{
  IscHandler *nhcb;


  ISC_XNEW(nhcb);
  *nhcb = *hcb;

  return nhcb;
}



IscHandler *
isc_newhandler(void)
{
  IscHandler *hcb;


  ISC_XNEW(hcb);
  
  hcb->read = NULL;
  hcb->write = NULL;
  hcb->close = NULL;
  hcb->poll = NULL;
  hcb->accept = NULL;
  hcb->destroy = NULL;
  hcb->parse = NULL;
  
  return hcb;
}



void
isc_freehandler(IscHandler *hcb)
{
  isc_free(hcb);
}



void
isc_pushhandler(IscSession *scb, IscHandler *hcb)
{
  IscHandlerList *ihl;

  
  ISC_XNEW(ihl);
  ihl->hcb = hcb;
  ihl->next = scb->handlers;
  if (scb->handlers)
      ihl->current = scb->handlers->current;
  else
  {
      ihl->current.read = NULL;
      ihl->current.write = NULL;
      ihl->current.close = NULL;
      ihl->current.poll = NULL;
      ihl->current.accept = NULL;
      ihl->current.destroy = NULL;
      ihl->current.parse = NULL;
  }
  
  scb->handlers = ihl;

  if (hcb->read)
    ihl->current.read = ihl;
  if (hcb->write)
    ihl->current.write = ihl;
  if (hcb->close)
    ihl->current.close = ihl;
  if (hcb->poll)
    ihl->current.poll = ihl;
  if (hcb->accept)
    ihl->current.accept = ihl;
  if (hcb->destroy)
    ihl->current.destroy = ihl;
  if (hcb->parse)
    ihl->current.parse = ihl;
}


IscHandler *
isc_pophandler(IscSession *scb)
{
  IscHandlerList *ip;
  IscHandler *hcb;

  
  if (scb->handlers == NULL)
    return NULL;

  ip = scb->handlers;
  scb->handlers = ip->next;
  hcb = ip->hcb;
    
  isc_free(ip);
  
  return hcb;
}



