/*
** isc_alloc.c                               ISC storage allocation routines
**
** Copyright (C) 1991, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved into separate file
** (See ChangeLog for recent history)
*/

#include <sys/types.h>
#include <sys/socket.h>
#ifdef HAVE_STDLIB_H
#  include <stdlib.h>
#endif
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#ifdef HAVE_STRING_H
#  include <string.h>
#else
#  include <strings.h>
#endif
#ifndef NULL
#  include <stdio.h>
#endif
#include <time.h>

#include "isc.h"
#include "intern.h"

/*
** Some magic numbers
*/
#define ISC_MAGIC_ALLOC	0x12F54ACEu
#define ISC_MAGIC_FREE  0xEE47A37Fu


/*
** Pointers to functions to handle storage allocation
*/
static void * (*isc_mallocfn)(size_t size) = NULL;
static void * (*isc_reallocfn)(void *bufp, size_t size) = NULL;
static void (*isc_freefn)(void *bufp) = NULL;


/*
** Define functions to handle storage allocation
*/
void
isc_setallocfn(void * (*mallocfn)(size_t size),
             void * (*reallocfn)(void *buf, size_t size),
	     void   (*freefn)(void *buf))
{
  isc_mallocfn  = mallocfn;
  isc_reallocfn = reallocfn;
  isc_freefn    = freefn;
}



void *
isc_malloc(size_t size)
{
  unsigned long  * buf;


  if (isc_mallocfn)
    buf = (unsigned long *) (*isc_mallocfn)(size + sizeof(unsigned long));
  else
  {
    buf = (unsigned long *) malloc(size + sizeof(unsigned long));
    if (!buf)
      isc_abort("isc_malloc");
  }

  *buf++ = ISC_MAGIC_ALLOC;

  return (void *) buf; 
}


void *
isc_realloc(void *oldbuf, size_t size)
{
  unsigned long *buf;


  if (!oldbuf)
    return isc_malloc(size);

  buf = (unsigned long *) oldbuf;
  if (*--buf != ISC_MAGIC_ALLOC)
    isc_abort("isc_realloc");

  *buf = ISC_MAGIC_FREE;
  
  if (isc_reallocfn)
    buf = (unsigned long *) (*isc_reallocfn)((void *) buf,
					     size + sizeof(unsigned long));
  else
  {
    buf = (void *) realloc((void *) buf, size + sizeof(unsigned long));
    if (!buf)
      isc_abort("isc_realloc");
  }

  *buf++ = ISC_MAGIC_ALLOC;
  return (void *) buf;
}



void
isc_free(void *buf)
{
  unsigned long *ibuf;

  
  if (!buf)
    isc_abort("isc_free");
    
  ibuf = (unsigned long *) buf;
  if (*--ibuf != ISC_MAGIC_ALLOC)
    isc_abort("isc_free");

  *ibuf = ISC_MAGIC_FREE;
  
  if (isc_freefn)
  {
    (*isc_freefn)((void *) ibuf);
    return;
  }
  else
    free((void *) ibuf);
}



/*
** Safe (hopefully) string duplicator
*/
char *
isc_strdup(const char *str)
{
  char *newstr;

  
  newstr = isc_malloc(strlen(str)+1);
  strcpy(newstr, str);
  return newstr;
}

