/*
** isc_socket.c                               Socket specified code
**
** Copyright (C) 1992, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
**   920210 pen     initial coding
** (See ChangeLog for recent history)
*/

#include <stdio.h>
#ifdef TIME_WITH_SYS_TIME
#  include <time.h>
#endif
#include <sys/types.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/file.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <errno.h>
#include <netdb.h>
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STRING_H
#  include <string.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <fcntl.h>

#include "isc.h"
#include "intern.h"



#if defined(__GNUC__) && defined(__sparc__)
/*
** inet_ntoa() is buggy on SPARCs running SunOS 4.1.1, so
** we use our own
*/
#ifdef inet_ntoa
#undef inet_ntoa
#endif

char *inet_ntoa(ad)
    struct in_addr ad;
{
  u_long s_ad;
  int a, b, c, d;
  static char addr[20];
  
  s_ad = ad.s_addr;
  d = s_ad % 256;
  s_ad /= 256;
  c = s_ad % 256;
  s_ad /= 256;
  b = s_ad % 256;
  a = s_ad / 256;
  sprintf(addr, "%d.%d.%d.%d", a, b, c, d);
  
  return addr;
}
#endif



IscAddress *
isc_mkipaddress(struct sockaddr *addr)
{
  IscAddress *ia;


  ISC_XNEW(ia);
  memcpy(&ia->ip.saddr, addr, sizeof(ia->ip.saddr));

  return ia;
}




IscAddress *
isc_getladdress(IscSession *scb)
{
  struct sockaddr addr;
  socklen_t len;


  if (scb->type != ISC_TYPE_TCP &&
      scb->type != ISC_TYPE_UDP)
    return NULL;
  
  len = sizeof(addr);
  if (getsockname(scb->fd, &addr, &len) < 0)
    return NULL;

  return isc_mkipaddress(&addr);
}



IscAddress *
isc_getraddress(IscSession *scb)
{
  struct sockaddr addr;
  socklen_t len;


  if (scb->type != ISC_TYPE_TCP &&
      scb->type != ISC_TYPE_UDP)
    return NULL;
  
  len = sizeof(addr);
  if (getpeername(scb->fd, &addr, &len) < 0)
    return NULL;

  return isc_mkipaddress(&addr);
}


void
isc_freeaddress(IscAddress *addr)
{
  isc_free(addr);
}


IscAddress *
isc_copyaddress(IscAddress *addr)
{
  IscAddress *new_addr;

  ISC_XNEW(new_addr);
  *new_addr = *addr;

  return new_addr;
}



char *isc_getipnum(IscAddress *ia, char *buf, int len)
{
  static char hostname[256];
  struct sockaddr_in *addr;
  

  if (!buf)
  {
    buf = hostname;
    len = sizeof(hostname)-1;
  }
  
  addr = (struct sockaddr_in *) &ia->ip.saddr;
  
  strncpy(buf, inet_ntoa(addr->sin_addr), len);
  buf[len] = '\0';

  return buf;
}



char *isc_gethostname(IscAddress *ia, char *buf, int len)
{
  static char hostname[256];
  struct sockaddr_in *addr;
  struct hostent *hp;
  
  if (ia == NULL)
  {
    return NULL;
  }

  if (!buf)
  {
    buf = hostname;
    len = sizeof(hostname)-1;
  }
  
  addr = (struct sockaddr_in *) &ia->ip.saddr;
  
  hp = gethostbyaddr((char*)&addr->sin_addr, sizeof(struct in_addr), AF_INET);
  if (!hp)
    return NULL;
  
  strncpy(buf, hp->h_name, len);
  buf[len] = '\0';
  
  return buf;
}



int isc_getportnum(IscAddress *ia)
{
  struct sockaddr_in *addr;

  
  addr = (struct sockaddr_in *) &ia->ip.saddr;

  return ntohs(addr->sin_port);
}


char *isc_getservice(IscAddress *ia,
		     char *buf,
		     int len,
		     IscSessionType type)
{
  static char servbuf[256];
  struct sockaddr_in *addr;
  struct servent *sp;
  
  if (!buf)
  {
    buf = servbuf;
    len = sizeof(servbuf)-1;
  }
  
  addr = (struct sockaddr_in *) &ia->ip.saddr;
  
  switch (type)
  {
    case ISC_TYPE_TCP:
      sp = getservbyport(ntohs(addr->sin_port), "tcp");
      break;
      
    case ISC_TYPE_UDP:
      sp = getservbyport(ntohs(addr->sin_port), "udp");
      break;
    
    default:
      return NULL;
  }

  if (!sp)
    return NULL;
  
  strncpy(buf, sp->s_name, len);
  return buf;
}
