/*
** isc_stdout.c                 Some nice-to-have functions for output..
**
** Copyright (C) 1991, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved into separate file
** (See ChangeLog for recent history)
*/

#include <stdio.h>
#ifdef HAVE_STRING_H
#  include <string.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "isc.h"

int
isc_putc(int           chr,
	 IscSession  * scb)
{
  if (scb->state != ISC_STATE_CONNECTING &&
      scb->state != ISC_STATE_DISABLED &&
      scb->state != ISC_STATE_RUNNING)
    return EOF;
  
  while (scb->sendindex == sizeof(scb->sendbuf))
    isc_flush(scb);

  return scb->sendbuf[scb->sendindex++] = chr;
}



int
isc_write(IscSession  * scb,
	  const void  * buffer,
	  size_t        length)
{
  const char  * bp;
  int           len;
  int           blen;
  int           clen;


  if (scb->state != ISC_STATE_CONNECTING &&
      scb->state != ISC_STATE_DISABLED &&
      scb->state != ISC_STATE_RUNNING)
    return EOF;
  
  bp   = (const char *) buffer;
  clen = length;
  
  while (clen > 0)
  {
    blen = sizeof(scb->sendbuf) - scb->sendindex;

    /* Make room in sendbuf */
    while (blen == 0)
    {
      isc_flush(scb);
      blen = sizeof(scb->sendbuf) - scb->sendindex;
    }

    len  = (clen > blen ? blen : clen);
    
    memcpy(scb->sendbuf+scb->sendindex, bp, len);
    scb->sendindex += len;
    clen -= len;
    bp   += len;
  }

  return length;
}

int
isc_puts(const char *str,
	 IscSession *scb)
{
  return isc_write(scb, str, strlen(str));
}

int
isc_putul(unsigned long nr,
	  IscSession *scb)
{
  static char   buf[sizeof(unsigned long) * 3 + 1];
  char         *cp;

  if (nr < 10)
    return isc_putc("0123456789"[nr], scb);
  else
  {
    cp = buf + sizeof(buf);
    while (nr > 0)
    {
      *--cp = (nr % 10) + '0';
      nr /= 10;
    }
    return isc_write(scb, cp, buf + sizeof(buf) - cp);
  }
}
