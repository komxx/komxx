/*
** isc_session.c                              Routines to handle ISC sessions
**
** Copyright (C) 1991-1993, 1998-1999, 2001 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved into separate file
** 920129 pen      added support for "lazy" connect()
** 920209 pen      reworked some of the code
** 920209 pen      TCP and UDP specific code removed.
** 930117 pen      Two memory leak bugs fixed (found by Ceder)
** (See ChangeLog for recent history)
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <time.h>
#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <fcntl.h>
#ifndef NULL
#  include <stdio.h>
#endif
#ifdef HAVE_UNISTD_H
#  include <unistd.h>
#endif

#include "isc.h"
#include "intern.h"
#include "unused.h"



/*
** ISC-Internal default function to read data
*/
IscMessage *isc_default_read_fn(IscHandlerList *UNUSED(hl), IscSession *scb)
{
  int status;
  IscMessage *msg;
  
  
  msg = isc_allocmsg(scb->cfg->max.msgsize);
  
  status = read(scb->fd, msg->buffer, msg->size);
  if (status <= 0)
  {
    status = errno;
    isc_freemsg(msg);
    errno = status;
    return NULL;
  }
  
  msg->length = status;
  msg->buffer[msg->length] = '\0';
  
  return msg;
}



int isc_default_write_fn(IscHandlerList *UNUSED(hl),
			 IscSession *scb,
			 IscMessage *msg)
{
  return write(scb->fd, msg->buffer, msg->length);
}



int isc_default_close_fn(IscHandlerList *UNUSED(hl),
			 IscSession *scb)
{
  int code = 0;

  
  if (scb->fd != -1)
  {
    code = close(scb->fd);
    scb->fd = -1;
  }

  return code;
}



/*
** Insert a session into a master control structure
*/
int
isc_insert(IscMaster *mcb,
	   IscSession *scb)
{
  IscSessionList *isl;


  ISC_XNEW(isl);
  isl->scb = scb;

  /* Better check if this is the first session or not */
  if (mcb->sessions != NULL)
  {
    isl->next           = mcb->sessions;
    isl->prev           = mcb->sessions->prev;
    isl->prev->next     = isl;
    mcb->sessions->prev = isl;
  }
  else
  {
    isl->next = isl;
    isl->prev = isl;
  }
  
  mcb->sessions  = isl;

  return ++scb->nlinks;
}



/*
** Locate a session list entry in a MCB
*/
static IscSessionList *
isc_findsession(IscMaster *mcb,
		IscSession *scb)
{
  IscSessionList *isl;


  /* Locate the session in the MCB */
  for (isl = mcb->sessions; isl != NULL && isl->scb != scb; isl = isl->next)
    if (isl->next == mcb->sessions)
      break;

  /* Not found? Return -1 */
  if (isl == NULL || isl->scb != scb)
    return NULL;

  return isl;
}



/*
** Remove a session from a master control structure
*/
int
isc_remove(IscMaster *mcb,
	   IscSession *scb)
{
  IscSessionList *isl;


  isl = isc_findsession(mcb, scb);
  if (!isl)
  {
    errno = ENOENT;
    return -1;
  }
  
  /* Remove it from the list of sessions if it is still on it */
  if (isl->prev != NULL)
  {
    if (isl->next != isl)
    {
      /* Make sure noone references this session */
      if (mcb->sessions == isl)
	mcb->sessions = isl->next;

      isl->prev->next = isl->next;
      isl->next->prev = isl->prev;
    }
    else
    {
      /* This was the last session on the circular list. */
      mcb->sessions = NULL;
    }
    
    isl->prev = NULL;
    isl->next = NULL;
  }

  isc_free(isl);
  
  return --scb->nlinks;
}


static IscHandler isc_default_fun =
{
  &isc_default_read_fn,
  &isc_default_write_fn,
  &isc_default_close_fn,
  NULL,
  NULL,
  NULL,
  NULL
};



/*
** Create a new session structure. 
*/
IscSession *
isc_create(IscSessionConfig *cfg, IscHandler *fun)
{
  IscSession  * scb;
  
  
  ISC_XNEW(scb);

  scb->nlinks   = 0;
  scb->fd       = -1;
  scb->type     = ISC_TYPE_UNKNOWN;
  scb->state    = ISC_STATE_UNKNOWN;
  scb->isc_errno= 0;
  scb->rd_msg_q = NULL;
  scb->wr_msg_q = NULL; 
  scb->handlers = NULL;

  scb->cfg = cfg;
  
  scb->stats.rx.bytes   = 0;
  scb->stats.rx.packets = 0;
  scb->stats.tx.bytes   = 0;
  scb->stats.tx.packets = 0;
  
  /* Fill in the session structure */
  scb->rd_msg_q      = isc_newqueue();
  scb->wr_msg_q      = isc_newqueue();
  scb->sendindex     = 0;

  if (!fun)
    fun = &isc_default_fun;

  isc_pushhandler(scb, fun);
  
  return scb;
}



/*
** Destroy the allocated Session structure. Close the
** socket if open and if Master structure specified, remove
** the session from it.
*/
int
isc_destroy(IscMaster *mcb,
	    IscSession *scb)
{
  int code = 0;


  if (mcb)
  {
    code = isc_remove(mcb, scb);
    if (code < 0)
      return code;
  }


  /*
  ** Only close and deallocate storage if last reference has been
  ** removed. 
  */
  if (scb->nlinks <= 0)
  {
    if (scb->handlers->current.destroy)
      ISC_SCALLFUN1(scb, destroy, scb);

    while (isc_pophandler(scb))
      ;
    
    if (scb->fd != -1)
    {
      close(scb->fd);
      scb->fd = -1;
    }

    if (scb->rd_msg_q)
      isc_killqueue(scb->rd_msg_q);
    
    if (scb->wr_msg_q)
      isc_killqueue(scb->wr_msg_q);

    isc_free(scb);
  }

  return code;
}



/*
** Put the session into CLOSING state, this also kills the queue of incoming
** messages.
*/
void
isc_close(IscSession *scb)
{
  isc_flush(scb);
  scb->state = ISC_STATE_CLOSING;
  if (scb->rd_msg_q)
  {
    isc_killqueue(scb->rd_msg_q);
    scb->rd_msg_q = NULL;
  }
}



/*
** Return the number of active sessions
*/
int
isc_sessions(IscMaster  * mcb)
{
  IscSessionList  * isl;
  IscSessionList  * top_isl;
  int               cnt;
  

  top_isl = mcb->sessions;
  for (cnt = 0, isl = top_isl; isl; cnt++, isl = isl->next)
    if (isl->next == top_isl)
      break;

  return cnt;
}



/*
** Create a new session, insert it into a master structure
** and associate a file descriptor with it.
*/
IscSession *
isc_openfd(IscMaster *mcb,
	   int fd)
{
  IscSession  * scb;
  int           res;

  
  /* Set non blocking write mode */
  if ((res = fcntl(fd, F_GETFL, 0)) == -1)
  {
    close(fd);
    return NULL;
  }

  /* If compilation fails on the next line, please report it as a bug
     to ceder@lysator.liu.se.  I'd like to talk to you so that you can
     test an autoconf solution to this problem.  As a workaround, you
     can change "O_NONBLOCK" to "FNDELAY". */
  if (fcntl(fd, F_SETFL, res | O_NONBLOCK) == -1)
  {
    close(fd);
    return NULL;
  }
  
  scb = isc_create(&mcb->scfg, NULL);
  if (!scb)
      return NULL;

  time(&scb->logintime);

  scb->state = ISC_STATE_RUNNING;
  scb->fd = fd;
  
  (void) isc_insert(mcb, scb);
  return scb;
}



static void
isc_file_destroy_fn(IscHandlerList *UNUSED(hl),
		    IscSession *scb)
{
  if (scb->info.file.pathname)
  {
    isc_free(scb->info.file.pathname);
    scb->info.file.pathname = NULL;
  }
}



/*
** Create a new session, insert it into a master structure
** and associate a filesystem object with it.
*/
IscSession *
isc_openfile(IscMaster   * mcb,
	     const char  * pathname,
	     int           openmode)
{
  IscSession  * scb;
  IscHandler  * hcb;
  int           fd;
  

  fd = open(pathname, openmode);
  if (fd == -1)
      return NULL;

  scb = isc_openfd(mcb, fd);
  if (!scb)
      return NULL;

  scb->type               = ISC_TYPE_FILE;
  scb->info.file.pathname = isc_strdup(pathname);

  hcb = isc_newhandler();
  hcb->destroy = &isc_file_destroy_fn;
  isc_pushhandler(scb, hcb);
  

  return scb;
}



int isc_disable(IscSession *scb)
{
  if (scb->state != ISC_STATE_RUNNING)
    return -1;
  
  scb->state = ISC_STATE_DISABLED;
  return 0;
}



int isc_enable(IscSession *scb)
{
  if (scb->state != ISC_STATE_DISABLED)
    return -1;
  
  scb->state = ISC_STATE_RUNNING;
  return 0;
}


