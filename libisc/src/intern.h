/*
** intern.h                Definitions and prototypes used internally
**
** Copyright (C) 1991,1999 by Peter Eriksson and Per Cederqvist of the
**                         Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      initial coding
** (See ChangeLog for recent history)
*/

#ifndef __ISC_INTERNALS_H__
#define __ISC_INTERNALS_H__


/* isc_log.c */
extern void
isc_log(const char *format, ...);


/* isc_abort.c */
extern void
isc_abort(const char *message);


/* isc_alloc.c */
extern void *
isc_malloc(size_t length);

extern void *
isc_realloc(void *buf, size_t length);

extern void
isc_free(void *buf);

extern char *
isc_strdup(const char *str);


/* isc_queue.c */
extern IscMsgQueue *
isc_newqueue(void);

#if 0
extern int
isc_pollqueue(IscMsgQueue *queue);
#else
#define isc_pollqueue(qp)	((qp) != NULL && ((qp)->head != NULL))
#endif

extern void
isc_killqueue(IscMsgQueue *queue);

extern void
isc_pushqueue(IscMsgQueue *queue, IscMessage *msg);

extern IscMessage *
isc_topqueue(IscMsgQueue *queue);

extern int
isc_sizequeue(IscMsgQueue *queue);

extern IscMessage *
isc_popqueue(IscMsgQueue *queue);


extern IscMessage *
isc_default_read_fn(IscHandlerList *hl, IscSession *scb);

extern int
isc_default_write_fn(IscHandlerList *hl, IscSession *scb, IscMessage *msg);

extern int
isc_default_close_fn(IscHandlerList *hl, IscSession *scb);


extern IscAddress *
isc_mkipaddress(struct sockaddr *addr);


extern void
isc_oflush(IscSession  * scb);


/*
** Setup a set of functions to handle memory allocation
*/
extern void
isc_setallocfn(void  * (*mallocfn)(size_t size),
               void  * (*reallocfn)(void  * buf, size_t size),
	       void    (*freefn)(void  * buf));



/*
** Setup a function to handle fatal abort requests
*/
extern void
isc_setabortfn(void (*abortfn)(const char  * msg));


/*
 * Move a file descriptor FD the first unused file descriptor higher
 * than or equal to LIMIT.  Return the new file descriptor.  Close FD.
 *
 * On failure, the old FD will be returned, and errno will be set.
 *
 * Do nothing (and return FD) if LIMIT is 0.
 */
extern int
isc_relocate_fd(int fd, int limit);


#define ISC_XNEW(var)   (var = isc_malloc(sizeof(*var)))


#define ISC_SCALLFUN1(SCB, NAME, ARG) \
	ISC_HCALLFUN1(SCB->handlers, NAME, ARG)
	
#define ISC_SCALLFUN2(SCB, NAME, A1, A2) \
	ISC_HCALLFUN2(SCB->handlers, NAME, A1, A2)
	
#define ISC_SCALLFUN3(SCB, NAME, A1, A2, A3) \
	ISC_HCALLFUN3(SCB->handlers, NAME, A1, A2, A3)


#endif
