/*
** isc_output.c                                   Isc data output functions
**
** Copyright (C) 1991, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved into separate file
** 910310 pen      added isc_send()
** (See ChangeLog for recent history)
*/

#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#include <errno.h>
#ifdef HAVE_STRING_H
#  include <string.h>
#endif
#include <sys/file.h>
#include <errno.h>
#ifndef NULL
#  include <stdio.h>
#endif
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>

#include "isc.h"
#include "intern.h"



void isc_flush(IscSession *scb)
{
  switch (scb->state)
  {
    case ISC_STATE_CLOSING:
    case ISC_STATE_CLOSING2:
      if (scb->wr_msg_q)
      {
	isc_killqueue(scb->wr_msg_q);
	scb->wr_msg_q = NULL;
      }
      if (scb->rd_msg_q)
      {
	isc_killqueue(scb->rd_msg_q);
	scb->rd_msg_q = NULL;
      }
      scb->sendindex = 0;
      return;

    case ISC_STATE_RUNNING:
    case ISC_STATE_DISABLED:
    case ISC_STATE_CONNECTING:
      isc_oflush(scb);
      return;

    default:
      return;
  }
}


  
void isc_oflush(IscSession *scb)
{
  IscMessage  * msg;
  int           failed;
  int           wlen;
  int           loopcnt;
  

  /* Data in the block buffer? */
  if (scb->sendindex > 0)
  {
    /* Too many queued blocks? */
    if (isc_sizequeue(scb->wr_msg_q) >= scb->cfg->max.queuedsize)
    {
      scb->state = ISC_STATE_CLOSING;
      scb->isc_errno = E2BIG;
      scb->sendindex = 0; /* And lets fake a write */
      return;
    }
    
    msg = isc_allocmsg(scb->sendindex);
    memcpy(msg->buffer, scb->sendbuf, scb->sendindex);
    msg->length = scb->sendindex;
    isc_pushqueue(scb->wr_msg_q, msg);
    scb->sendindex = 0;
  }

  /* We only try to transmit messages for RUNNING or CLOSING sessions */
  if ((scb->state != ISC_STATE_RUNNING &&
       scb->state != ISC_STATE_DISABLED &&
       scb->state != ISC_STATE_CLOSING &&
       scb->state != ISC_STATE_CLOSING2) || scb->fd == -1)
    return;
  
  /* Queued entries? Send as much as possible */
  failed = 0;
  loopcnt = 0;
  while ((msg = isc_topqueue(scb->wr_msg_q)) != NULL &&
	 !failed && loopcnt < scb->cfg->max.dequeuelen)
  {
    wlen = ISC_SCALLFUN2(scb, write, scb, msg);

    if (wlen < 0)
    {
      switch ( errno )
      {
        case EWOULDBLOCK:
	  wlen = 0;
	  break;

        case EPIPE:
	  scb->state = ISC_STATE_CLOSING;
	  scb->isc_errno = errno;
	  return;

        default:
	  scb->state = ISC_STATE_CLOSING;
	  scb->isc_errno = errno;
          scb->sendindex = 0;
	  return;
      }
    }

    if (wlen > 0)
    {
      scb->stats.tx.bytes += wlen;
      scb->stats.tx.packets++;
    }
      
    msg->length -= wlen;
    if (msg->length > 0)
    {
      /* The write handler could not transmit the whole buffer */
      failed = 1;
      if (wlen > 0)
	memcpy(msg->buffer, msg->buffer + wlen, msg->length);
    }
    else
    {
      /* Drop the topmost entry */
      msg = isc_popqueue(scb->wr_msg_q);
      isc_freemsg(msg);
    }

    loopcnt++;
  }
}



/*
** Put an IscMessage onto an IscSession transmit queue
*/
int
isc_send(IscSession  *scb, IscMessage  *msg)
{
  /* Must flush if pending "stdout" output */
  if (scb->sendindex)
    isc_flush(scb);

  isc_pushqueue(scb->wr_msg_q, msg);
  return 0;
}



int
isc_sendto(IscSession *scb,
	   IscAddress *ia,
	   IscMessage *msg)
{
  if (scb->type != ISC_TYPE_UDP)
    return -1;

  msg->address = ia;

  return isc_send(scb, msg);
}

