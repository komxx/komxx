/*
** isc_message.c                      Generic "IscMessage" handling functions
**
** Copyright (C) 1991-1992, 1998-1999 by Peter Eriksson and
** Per Cederqvist of the Lysator Academic Computer Association.
**
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the Free
** Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
**
** history:
** 910305 pen      moved into separate file
** 920102 pen      added isc_copymsg()
** (See ChangeLog for recent history)
*/

#ifdef HAVE_STDDEF_H
#  include <stddef.h>
#endif
#ifdef HAVE_STDARG_H
#  include <stdarg.h>
#endif
#ifdef HAVE_STRING_H
#  include <string.h>
#endif
#ifndef NULL
#  include <stdio.h>
#endif
#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "isc.h"
#include "intern.h"


IscMessage *
isc_allocmsg(size_t size)
{
    IscMessage  * msg;

    
    ISC_XNEW(msg);
    msg->buffer = (char *) isc_malloc(size+1);
    msg->size   = size;
    msg->length = 0;
    msg->address = NULL;

    return msg;
}


IscMessage *
isc_reallocmsg(IscMessage  * msg,
	       size_t        newsize)
{
    msg->buffer = (char *) isc_realloc(msg->buffer, newsize+1);
    msg->size   = newsize;
    return msg;
}


IscMessage *
isc_copymsg(IscMessage  *msg)
{
  IscMessage  * newmsg;

  newmsg = isc_allocmsg(msg->size);
  if (msg->buffer && msg->length > 0)
    memcpy(newmsg->buffer, msg->buffer, msg->length);
  newmsg->length = msg->length;
  newmsg->address = msg->address;
  return newmsg;
}


void
isc_freemsg(IscMessage  * msg)
{
  if (msg->buffer)
    isc_free(msg->buffer);
  
  isc_free(msg);
}



IscMessage *
isc_mkstrmsg(const char *str)
{
  IscMessage  * msg;

  
  msg = isc_allocmsg(strlen(str)+1);
  strcpy(msg->buffer,str);
  msg->length = strlen(str);

  return msg;
}
