News in release 1.01 (release date: 2001-09-23):

* An unused attribute in IscSession was removed.

* This release was produced with automake 1.5 and autoconf 2.52.

News in release 1.00 (release date: 1999-05-01):

* All of ISC is now distributed under LGPL.

* isc_printf() and isc_vprintf() have been removed.  The
  implementation was unifficient, it used code that could not be
  distributed under GPL or LGPL, and the implementation was
  unportable.  It was also not needed by the core projects that use
  ISC.  If anybody supplies an efficient implementation that is
  distributable under LGPL it will be included in a future release.

* New functions: isc_puts() and isc_putul().

* New configure options --with-checker, --with-gcov and
  --without-optimization are useful during debugging.

* The distribution is now totally self-contained.

* Many implementations of stdio cannot use high file descriptors
  (typically the file descriptor must be less than 256).  You can now
  tell ISC to leave a few low-numbered file descriptors unused so that
  they are available for use with stdio.  Set the fd_relocate member
  of the IscSessionConfig structure to the lowest number that ISC is
  allowed to use.

* The new event ISC_EVENT_LOGIN_UNRELOCATED is generated if ISC failed
  to relocate the file descriptor when it was about to generate an
  ISC_EVENT_LOGIN session.  Applications could destroy the new
  session, or take care to not open new stdio files as long as the
  unrelocated session exists.  This event is never generated if
  fd_relocate is set to 0.

* The master config version is now 1006.  The session config version
  is now 1002.

* IscHandlerList has been simplified.

* Some other minor fixes and cleanup.

News in release 0.99 (release date: 1998-07-06):

* Uses autoconf and automake for easy integration in other packages.

* All improvements made to ISC in lyskom-server and kom++ have been
  merged back in the isc distribution.

* When isc_getnextevent returns a ISC_EVENT_LOGIN event, the event now
  contains a pointer to the listening session in the new field
  listen_session.

* Don't use the symbol "errno" as a struct tag.

* Some other bug fixes, style improvements and code conservation (to
  avoid bit decay).

* Ported to architectures where some data must be aligned on a "long"
  boundary (and "int" is shorter than "long").

* Previously, isc_getnextevent would return repeated ISC_EVENT_LOGOUT
  events until the corresponding session was destroyed.  Now, only a
  single ISC_EVENT_LOGOUT event will be returned.

* The library is normally compiled without the functions isc_printf
  and isc_vprintf.  They are not as portable as the rest of the
  library, and use of them may result in poor performance.  Use the
  --enable-isc-printf argument to configure to include those functions
  in the library.

* The new function isc_wait makes it possible to wait until an
  initiated connection attempt (to a remote server) either succeeds or
  fails.

* isc.h no longer includes any header files.

News in release 0.98.3 and earlier versions:

* Unknown.
