How to make a release of isc:

* Change the version number in the AM_INIT_AUTOMAKE macro in configure.in.
* Change the version number in the AC_INIT macro in configure.in.
* Update the year in all copyright statements.
* Update README.DEVO, especially the versions of the various tools used.
* Update NEWS if this is a full release.
* Ensure that all documentation is up to date, or that README says
  that it isn't.
* Write a note in ChangeLog.
* Commit all code.
* Run these commands to ensure that all generated files are up-to-date:

	./bootstrap.sh
	./configure
	make distcheck

* Test the resulting archive on several architectures.  If any
  problems are found, fix them, and start from the beginning.  This
  step may be skipped if this is a snapshot release, or if there is no
  test suite written yet.
* Set a CVS tag.  Full release are tagged like this:

	cvs tag isc_0_99_final

  Snapshot releases are tagged like this:

	cvs tag isc_0_99_post_1

* Move the tar file to 
	ftp://ftp.lysator.liu.se/pub/unix/isc/
	aka /lysator/ftp/pub/unix/isc/

* Announce the release in the following forums:

	- The LysKOM conference "ISC internals".
