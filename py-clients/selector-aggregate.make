selector-aggregate.incl: selector-aggregate.list selector-aggregate.template \
		selector-aggregate.make
	$(RM) selector-aggregate.incl selector-aggregate.incl.tmp
	> selector-aggregate.incl.tmp
	cat $(srcdir)/selector-aggregate.list  \
	| grep -v '^#' \
	| while read class method result; \
	do cat $(srcdir)/selector-aggregate.template \
		| sed -e s/CLASS/$$class/g \
		      -e s/METHOD/$$method/g \
		      -e s/RESULT/$$result/g \
		>> selector-aggregate.incl.tmp \
		|| exit 1; \
	done
	mv selector-aggregate.incl.tmp selector-aggregate.incl
