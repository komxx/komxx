import time
import string
import re
import os
import sys
import curses
import select
import komxx
import re


latin1map = {}
swasciimap = {}
for x in range(256):
    latin1map[x] = chr(x)
    swasciimap[x] = chr(x)

for x in range(32) + range(128, 128+32):
    latin1map[x] = "\\%03o" % x
    swasciimap[x] = "\\%03o" % x

swasciimap[0345] = "}"
swasciimap[0344] = "{"
swasciimap[0366] = "|"
swasciimap[0305] = "]"
swasciimap[0304] = "["
swasciimap[0306] = "\\"

latin1map[10] = None
latin1map[8] = None
swasciimap[10] = None
swasciimap[8] = None

for k in ["UP", "DOWN", "END", "HOME", "PPAGE", "NPAGE"]:
    if not curses.__dict__.has_key("KEY_" + k):
	curses.__dict__["KEY_" + k] = curses.__dict__["key_" + string.lower(k)]

def pretty_linesplit(s, cols, preline = ""):
    res = []
    lin = preline
    for c in s:
	c = ord(c)
	if latin1map[c] != None:
	    if len(lin) >= cols - 1:
		res.append(lin + "\\")
		lin = ""
	    lin = lin + chr(c)
	elif c == 10:
	    res.append(lin)
	    lin = ""
	elif c == 8:
	    lin = lin + " " * (8 - (len(lin) % 8))
	    if len(lin) >= cols - 1:
		res.append(lin[:cols-1] + "\\")
		lin = lin[cols-1:]
	else:
	    raise error, "Evil character %d" % c
    res.append(lin)
    return res

class window(object):
    def __init__(self):
	self.w = curses.initscr()
	self.w.keypad(1)
	curses.cbreak()
	curses.noecho()
	curses.nonl()
	curses.intrflush(0)
	self.w.nodelay(1)
	self.lines = []
	self.first_visible_line = None
	self.user_set_first_line = None
	self.acknowledged_line = None
	self.__modeline = ""
	self.__prompting = 0

    def modeline(self, line):
	self.__modeline = line
	self.refresh_modeline()

    def refresh_modeline(self):
	(r, k) = self.w.getmaxyx()
	self.w.hline(r-2, 0, ord("-") | curses.A_REVERSE, k)
	self.w.addstr(r-2, 4, self.__modeline, curses.A_REVERSE)

    def write(self, s):
	"""Append S to the end of the buffer.

	Nothing is changed on the display; see also the refresh method.
	"""

	self.__prompting = 0
	(r, k) = self.w.getmaxyx()
	if len(self.lines) == 0:
	    self.lines = pretty_linesplit(s, k)
	else:
	    self.lines[-1:] = pretty_linesplit(s, k, self.lines[-1])

    def prepare_refresh(self):
	"""Write the current screen contents to the curses buffers.

	Note that this does not actually refresh the curses window.
	"""
	self.w.move(0,0)
	self.w.erase()
	(r, k) = self.w.getmaxyx()
	start = self.user_set_first_line
	if start == None:
	    start = len(self.lines) - r + 2
	    if self.acknowledged_line != None \
	       and start > self.acknowledged_line:
		start = self.acknowledged_line
		self.user_set_first_line = start
		if self.user_set_first_line < 0:
		    self.user_set_first_line = 0
	if start < 0:
	    start = 0
	self.first_visible_line = start
	end = start + r - 2
	if end >= len(self.lines):
	    self.user_set_first_line = None
	    end = len(self.lines)
	pos = 0
	self.refresh_modeline()
	for line in self.lines[start:end]:
	    self.w.addstr(pos, 0, line)
	    pos = pos + 1

    def refresh(self):
	self.prepare_refresh()
	self.w.refresh()

    def prompt(self, p):
	self.write(p)
	self.write(" - ")
	self.__prompting = 1

    def ack_prompt(self):
	if self.__prompting:
	    self.__prompting = 0
	    self.lines[-1] = self.lines[-1][:-3] + "."
	    self.lines.append("")
	self.acknowledge()

    def acknowledge(self):
	self.acknowledged_line = len(self.lines) - 1

    def getstr(self, prompt, doecho = 1):
	(r, k) = self.w.getmaxyx()
	# FIXME: look out for bad chars/long prompts
	self.prepare_refresh()
	self.w.addstr(r-1, 0, prompt)
	self.w.clrtoeol()
	buf = ""
	if not doecho:
	    self.w.move(r-1, len(prompt))
	while 1:
	    if doecho:
		self.w.move(r-1, len(prompt) + len(buf))
		self.w.clrtoeol()
	    self.w.refresh()
	    key = self.w.getch()
	    if key == -1:
		pass
	    elif key == 8:
		buf = buf[:-1]
	    elif key == 10 or key == 13:
		self.w.move(r-1, 0)
		self.w.clrtoeol()
		self.acknowledge()
		return buf
	    else:
		k = chr(key)
		if len(k) == 1:
		    buf = buf + k
		    if doecho:
			self.w.addstr(k)

    def pagesize(self):
	(r, k) = self.w.getmaxyx()
	return r - 3


persno_regexp = re.compile("p[ \t]([1-9][0-9]*)$")

def select_unique_person(w, s, prompt):
    persno = None
    while persno == None:
	name = w.getstr(prompt)
	if name == "":
	    return None
	persno_match = persno_regexp.match(name)
	if persno_match is not None:
	    persno = int(persno_match.group(1))
	    if not s.pers(persno).exists():
		w.write("Person %d finns ej\n" % persno)
		persno = None
		name = ""
	else:
	    matches = s.lookup_z_name(name, 1, 0)
	    if len(matches) == 0:
		# FIXME: blink "no match"
		w.write("Ingen matchar %s\n" % name)
	    elif len(matches) == 1:
		persno = matches[0].conf_no()
	    else:
		# FIXME: present a choice
		w.write("%d matchar till %s\n" % (len(matches), name))
    return persno
	
def login_via_ask(w, s):
    while 1:
	persno = select_unique_person(w, s, "Vad heter du? ")
	if persno == None:
	    return
	w.write("Namn: " + s.conf(persno).name() + "\n")
	password = w.getstr("L�senord? ", 0)
	if s.login(persno, password) == komxx.st_ok:
	    return
	w.write("Felaktigt l�senord.\n")


def login_via_komrc(w, s):
    import os
    try:
	f = open(os.path.expanduser("~/.komrc"), "r")
    except IOError:
	return
    persno = string.atoi(f.readline()[:-1])
    password = f.readline()[:-1]
    f.close()
    if s.login(persno, password) == komxx.st_ok:
	w.write("V�lkommen " + s.conf(s.my_login()).name() + "\n")
    else:
	if s.pers(persno).exists():
	    w.write("Fel l�senord i ~/.komrc\n")
	else:
	    w.write("Felaktigt personnummer i ~/.komrc\n")

def toploop(w, s):
    intrace = open("intrace.kbd", "w")
    while 1:
	w.modeline("Computing prompt")
	w.refresh()
	prompt = None
	while prompt == None:
	    prompt = s.next_prompt()
	    if prompt == komxx.TIMEOUT_PROMPT:
		w.modeline("Computing prompt timeout")
		w.refresh()
		prompt = None
	w.prompt({komxx.NEXT_CONF: "N�sta m�te",
		  komxx.NEXT_TEXT: "N�sta text",
		  komxx.NEXT_COMMENT: "N�sta kommentar",
		  komxx.NEXT_FOOTNOTE: "N�sta fotnot",
		  komxx.NO_MORE_TEXT: "Se tiden",
		  komxx.PROMPT_ERROR: "TRAS!"}[prompt])
	w.modeline("Expecting input")
	w.refresh()
	cmd = None
	while cmd == None:
	    (rs, ws, es) = select.select([sys.stdin.fileno()], [], [], 0)
	    if sys.stdin.fileno() in rs:
		c = w.w.getch()
		intrace.write("Got key %s\n" % c)
		intrace.flush()
		w.modeline("Got key %s" % c)
		w.refresh()
		if c == curses.KEY_UP:
		    if w.user_set_first_line == None:
			w.user_set_first_line = w.first_visible_line
		    w.user_set_first_line = w.user_set_first_line - 1
		    if w.user_set_first_line < 0:
			w.user_set_first_line = 0
		    w.refresh()
		elif c == curses.KEY_DOWN:
		    if w.user_set_first_line != None:
			w.user_set_first_line = w.user_set_first_line + 1
		    w.refresh()
		elif w.user_set_first_line != None and c == 32:
		    w.user_set_first_line = (w.user_set_first_line
					     + w.pagesize())
		    w.refresh()
		elif c == curses.KEY_END:
		    w.user_set_first_line = None
		    w.acknowledge()
		    w.refresh()
		elif c == curses.KEY_HOME:
		    w.user_set_first_line = 0
		    w.refresh()
		elif c == curses.KEY_PPAGE:
		    if w.user_set_first_line == None:
			w.user_set_first_line = w.first_visible_line
		    w.user_set_first_line = (w.user_set_first_line
					     - w.pagesize())
		    if w.user_set_first_line < 0:
			w.user_set_first_line = 0
		    w.refresh()
		elif c == curses.KEY_NPAGE:
		    if w.user_set_first_line != None:
			w.user_set_first_line = (w.user_set_first_line
						 + w.pagesize())
		    w.refresh()
		elif c > 255:
		    w.write("Unhandled key %d\n" % c)
		    w.refresh()
		elif chr(c) == "q":
		    intrace.close()
		    sys.exit(0)
		elif chr(c) == " ":
		    cmd = prompt
		elif chr(c) == "V":
		    cmd = "vilka"
		else:
		    w.modeline("Bad key %d." % c)
		    w.refresh()
	    s.prefetch()

	w.ack_prompt()
	w.refresh()
	if cmd == komxx.NEXT_CONF:
	    w.write("G� till " + s.conf(s.goto_next_conf()).name())
	    w.write(" - %d ol�sta\n" % s.estimated_unread_texts_in_current())
	elif cmd == komxx.NEXT_TEXT:
	    w.write("L�sa text %d\n" % s.next_text(1))
	    w.write("H�r �r en massa\ntext som skrivits in.\n")
	elif cmd == komxx.NEXT_COMMENT:
	    w.write("L�sa kommentar %d\n" % s.next_text(1))
	elif cmd == komxx.NEXT_FOOTNOTE:
	    w.write("L�sa fotnot %d\n" % s.next_text(1))
	elif cmd == komxx.NO_MORE_TEXT:
	    w.write("Se tiden (ej implementerat)\n")
	elif cmd == "vilka":
	    for i in range(100):
		w.write("Rad %d.\n" % i)
	else:
	    w.write("Oups!  Oimplementerat kommando.\n")

def start_it(server_host, server_port):
    try:
	w = window()
	s = komxx.new_session(server_host, server_port, "t99699",
			      re.sub("[a-zA-Z$ :]*", "",
				     "$Revision: 1.7 $"))
	login_via_komrc(w, s)
	if s.my_login() == 0:
	    login_via_ask(w, s)
	if s.my_login() == 0:
	    w.modeline("Kunde inte logga in.")
	    sys.exit(2)

	w.write("V�lkommen " + s.conf(s.my_login()).name() + "\n")
	toploop(w, s)
    finally:
	curses.endwin()

if __name__ == "__main__":
    start_it("kom.lysator.liu.se", "4894")
