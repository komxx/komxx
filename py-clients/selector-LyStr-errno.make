selector-LyStr-errno.incl: selector-LyStr-errno.list \
		selector-LyStr-errno.template selector-LyStr-errno.make
	$(RM) selector-LyStr-errno.incl selector-LyStr-errno.incl.tmp
	cat $(srcdir)/selector-LyStr-errno.list \
	| grep -v '^#' \
	| while read class method; \
	do cat $(srcdir)/selector-LyStr-errno.template \
		| sed -e s/CLASS/$$class/g -e s/METHOD/$$method/g \
		>> selector-LyStr-errno.incl.tmp \
		|| exit 1; \
	done
	mv selector-LyStr-errno.incl.tmp selector-LyStr-errno.incl
