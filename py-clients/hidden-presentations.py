# Find presentations that aren't publically visible.

import os
import sys
import time

import komxx
import kompolicy
import prefetcher

BODY = """Det h�r brevet skickas ut automatiskt till ett femtiotal personer, som
har det gemensamt att era presentationer inte �r publika.  Jag har
inte l�st din hemliga presentation.

Eftersom din presentation inte har n�got publikt m�te som mottagare �r
den inte �r l�sbar av alla.  Om du anv�nder elispklienten kan du l�gga
till en �ppen mottagare med hj�lp av kommandot "a Addera mottagare".
M�tet "Presentation (av nya) medlemmar" eller "allm�nt l�sbart" kan
var l�mpliga mottagare.

Om det �r avsiktligt du gjort s� h�r, s� f�r du naturligtvis ha det
s�.  Men eftersom du har gjort dig besv�ret att skriva en presentation
antar jag att din avsikt faktiskt var att den skulle vara l�sbar f�r
allm�nheten.

Jag �r nyfiken p� varf�r s� m�nga presentationer inte �r allm�nt
l�sbara.  �r det en bugg i n�gon klient?  �r det en ny trend jag inte
begriper?  Stilla g�rna min nyfikenhet om du har lust."""


class applet(kompolicy.tty_io, kompolicy.login):
    def __init__(self, host, port):
        print "Connecting..."
        self.session = komxx.new_session(host, port, "hidden-presentations.py",
                                         "$Revision: 1.1 $")
        print "Connected."
        self.login()
        self.maybe_enable()
        targets = self.session.lookup_z_name("", 1, 0)
        targets = prefetcher.prefetch_confno_presentation(self.session,
                                                          targets)
        for target in targets:
            c = self.session.conf(target.conf_no())
            if c.presentation() != 0:
                t = self.session.text(c.presentation())
                if t.exists():
                    visible = 0
                    for r in t.recipients():
                        rc = self.session.conf(r.recipient())
                        if rc.exists():
                            ct = rc.type()
                            if not (ct.is_protected() or ct.is_secret()):
                                visible = 1
                                break
                    if not visible:
                        print "%5d %9d %s" % (target.conf_no(),
                                              c.presentation(),
                                              c.name())
                        if 0:
                            t = self.session.create_text("Din presentation �r osynlig",
                                                         BODY,
                                                         [target.conf_no(), 35],
                                                         [],
                                                         [c.presentation()],
                                                         [])
                            print "Skapat p�pekande", t
                else:
                    print "%5d %9s %s" % (target.conf_no(), "-", c.name())

if __name__ == "__main__":
    applet("kom.lysator.liu.se", "4894")
