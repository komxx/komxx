selector-assertion.incl: selector-assertion.list selector-assertion.template \
		selector-assertion.make
	$(RM) selector-assertion.incl selector-assertion.incl.tmp
	cat $(srcdir)/selector-assertion.list  \
	| grep -v '^#' \
	| while read class method assertion exception; \
	do cat $(srcdir)/selector-assertion.template \
		| sed -e s/CLASS/$$class/g \
		      -e s/METHOD/$$method/g \
		      -e s/ASSERTION/$$assertion/g \
		      -e s/EXCEPTION/$$exception/g \
		>> selector-assertion.incl.tmp \
		|| exit 1; \
	done
	mv selector-assertion.incl.tmp selector-assertion.incl
