/***********************************************************
Copyright 1991-1995 by Stichting Mathematisch Centrum, Amsterdam,
The Netherlands.
Copyright 1995 by Per Cederqvist, Signum Support, Linkoping, Sweden.

                        All Rights Reserved

Permission to use, copy, modify, and distribute this software and its 
documentation for any purpose and without fee is hereby granted, 
provided that the above copyright notice appear in all copies and that
both that copyright notice and this permission notice appear in 
supporting documentation, and that the names of Stichting Mathematisch
Centrum or CWI not be used in advertising or publicity pertaining to
distribution of the software without specific, written prior permission.

STICHTING MATHEMATISCH CENTRUM DISCLAIMS ALL WARRANTIES WITH REGARD TO
THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS, IN NO EVENT SHALL STICHTING MATHEMATISCH CENTRUM BE LIABLE
FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

******************************************************************/

#include <config.h>

#include <vector>

/* Komxx_session objects */
#include "session.h"
#include "uplink.h"
#include "unread-confs.h"
#include "create_text_old_q.h"
#include "create_text_q.h"
#include "create_anonymous_text_q.h"
#include "find_next_text_no_q.h"
#include "find_previous_text_no_q.h"
#include "get_created_texts_q.h"
#include "set_priv_bits_q.h"
#include "set_pers_flags_q.h"
#include "async.h"
#include "async_text_created.h"
#include "async_text_message.h"
#include "conn_io_isc.h"
#include "static-session-info.h"
#include "aux-item.h"
#include "get_members_q.h"
#include "delete_text_q.h"

// This entire file needs to be within extern "C".
// This hackery is here to avoid messing up emacs auto-indentation features.
#define BEGINEXTERNC extern "C" {
#define ENDEXTERNC }
BEGINEXTERNC

#include "Python.h"

// Objects-------------------------------------------------
static PyObject *error_class = NULL;
static PyObject *errno_base_class = NULL;
static PyObject *errno_classes[KOM_max_errno];
static PyObject *error_no_connection_class = NULL;
static PyObject *error_sequence_class = NULL;
static PyObject *error_no_time_class = NULL;
static PyObject *error_not_logged_in_class = NULL;
static PyObject *error_no_conf_selected_class = NULL;
static PyObject *error_bad_async_tag_class = NULL;

static PyObject *komxx_st_pending;
static PyObject *komxx_st_ok;
static PyObject *komxx_st_error;
static PyObject *prompt_NEXT_CONF;
static PyObject *prompt_NEXT_TEXT;
static PyObject *prompt_NEXT_COMMENT;
static PyObject *prompt_NEXT_FOOTNOTE;
static PyObject *prompt_NO_MORE_TEXT;
static PyObject *prompt_TIMEOUT_PROMPT;
static PyObject *prompt_PROMPT_ERROR;


#define SIZE(array) (sizeof(array)/sizeof((array)[0]))

// Error object support funcitons--------------------------

static PyObject *
create_class(PyObject *base,
	     PyObject *module_dict,
	     const char *name,
	     PyObject **class_dict = NULL)
{
    LyStr nm("komxx.");
    nm << LyStr(name);
    const char *buf = nm.view_block();
    PyObject *d = PyDict_New();
    PyObject *s = PyString_FromString((char*)buf);
    nm.view_done(buf);
    PyObject *tuple;
    if (base == NULL)
	tuple = NULL;
    else
    {
	tuple = PyTuple_New(1);
	Py_INCREF(base);
	PyTuple_SetItem(tuple, 0, base);
    }
    PyObject *new_class = PyClass_New(tuple, d, s);
    Py_XDECREF(tuple);
    if (class_dict != NULL)
    {
	assert(*class_dict == NULL);
	*class_dict = d;
    }
    else
    {
	Py_DECREF(d);
    }
    Py_DECREF(s);
    PyDict_SetItemString(module_dict, (char*)name, new_class);
    return new_class;
}

static void
create_errno(PyObject *base,
	     PyObject *module_dict,
	     Kom_err eno,
	     const char *name,
	     const char *explanation)
{
    PyObject *dict = NULL;
    errno_classes[eno] = create_class(base, module_dict, name, &dict);
    PyObject *number = PyInt_FromLong(eno);
    PyObject *str = PyString_FromString((char*)explanation);
    PyDict_SetItemString(dict, "errno", number);
    PyDict_SetItemString(dict, "errmsg", str);
    Py_DECREF(number);
    Py_DECREF(str);
    Py_DECREF(dict);
}

static void
throw_exception(PyObject *except)
{
    PyObject *exc = PyInstance_New(except, NULL, NULL);
    PyErr_SetObject(except, exc);
    Py_DECREF(exc);
}

static void
init_errors(PyObject *md)	// module dictionary
{
    int size = SIZE(errno_classes);
    for (int e = 0; e < size; e++)
	errno_classes[e] = NULL;

    error_class = create_class(NULL, md, "error");

    PyObject *base;
    
    base = error_class;
    error_sequence_class = create_class(base, md, "sequence_error");
    errno_base_class = create_class(base, md, "errno_error");
    error_no_connection_class = create_class(base, md, "no_connection");

    base = error_sequence_class;
    error_no_time_class = create_class(base, md, "no_time_error");
    error_not_logged_in_class = create_class(base, md, "not_logged_in_error");
    error_no_conf_selected_class = create_class(base, md,
						"no_conf_selected_error");
    error_bad_async_tag_class = create_class(base, md, "bad_async_tag_error");

    base = errno_base_class;
    create_errno(base, md, KOM_NOT_IMPL, "not_impl",
		 "Server call not implemented yet -- obsolete server");
    create_errno(base, md, KOM_OBSOLETE, "err_obsolete",
		 "Server call no longer implemented -- obsolete client");
    create_errno(base, md, KOM_PWD, "err_pwd",
		 "Password is wrong or contains illegal chars");
    create_errno(base, md, KOM_LONG_STR, "err_long_str",
		 "String too long");
    create_errno(base, md, KOM_LOGIN, "err_login",
		 "Not logged in.");
    create_errno(base, md, KOM_LOGIN_DISALLOWED, "err_login_disallowed",
		 "Logins are currently not allowed");
    create_errno(base, md, KOM_CONF_ZERO, "err_conf_zero",
		 "Internal kom++ error: attempt to use conference number 0.");
    create_errno(base, md, KOM_UNDEF_CONF, "err_undef_conf",
		 "Undefined or secret conference");
    create_errno(base, md, KOM_UNDEF_PERS, "err_undef_pers",
		 "Undefined or secret person");
    create_errno(base, md, KOM_ACCESS, "err_access",
		 "No 'read/write permission'");
    create_errno(base, md, KOM_PERM, "err_perm",
		 "No permission");
    create_errno(base, md, KOM_NOT_MEMBER, "err_not_member",
		 "Not member in conf");
    create_errno(base, md, KOM_NO_SUCH_TEXT, "err_no_such_text",
		 "Text does not exist or is secret");
    create_errno(base, md, KOM_TEXT_ZERO, "err_text_zero",
		 "Can't use text no 0");
    create_errno(base, md, KOM_NO_SUCH_LOCAL_TEXT, "err_no_such_local_text",
		 "No such local text number");
    create_errno(base, md, KOM_LOCAL_TEXT_ZERO, "err_local_text_zero",
		 "Can't use local text number 0");
    create_errno(base, md, KOM_BAD_NAME, "err_bad_name",
		 "Bad name: too long or contains illegal chars");
    create_errno(base, md, KOM_INDEX_OUT_OF_RANGE, "err_index_out_of_range",
		 "Index out of range");
    create_errno(base, md, KOM_CONF_EXISTS, "err_conf_exists",
		 "Conference already exists");
    create_errno(base, md, KOM_PERS_EXISTS, "err_pers_exists",
		 "Person already exists");
    create_errno(base, md, KOM_SECRET_PUBLIC, "err_secret_public",
		 "Cannot be secret and public");
    create_errno(base, md, KOM_LETTER_BOX, "err_letter_box",
		 "Cannot change letter_box flag");
    create_errno(base, md, KOM_LDB_ERR, "err_ldb_err",
		 "LysKOM database corrupt.");
    create_errno(base, md, KOM_ILL_MISC, "err_ill_misc",
		 "Illegal misc field.");
    create_errno(base, md, KOM_ILLEGAL_INFO_TYPE, "err_illegal_info_type",
		 "Internal error: info_type parameter was illegal.");
    create_errno(base, md, KOM_ALREADY_RECIPIENT, "err_already_recipient",
		 "Already recipient to this text.");
    create_errno(base, md, KOM_ALREADY_COMMENT, "err_already_comment",
		 "Already comment to this text.");
    create_errno(base, md, KOM_ALREADY_FOOTNOTE, "err_already_footnote",
		 "Already footnote to this text.");
    create_errno(base, md, KOM_NOT_RECIPIENT, "err_not_recipient",
		 "Not recipient");
    create_errno(base, md, KOM_NOT_COMMENT, "err_not_comment",
		 "Not comment to this text.");
    create_errno(base, md, KOM_NOT_FOOTNOTE, "err_not_footnote",
		 "Not footnote to this text.");
    create_errno(base, md, KOM_RECIPIENT_LIMIT, "err_recipient_limit",
		 "Too many recipients");
    create_errno(base, md, KOM_COMM_LIMIT, "err_comm_limit",
		 "Too many comments");
    create_errno(base, md, KOM_FOOT_LIMIT, "err_foot_limit",
		 "Too many footnotes");
    create_errno(base, md, KOM_MARK_LIMIT, "err_mark_limit",
		 "Too many marks.");
    create_errno(base, md, KOM_NOT_AUTHOR, "err_not_author",
		 "Only the author may add footnotes or delete texts.");
    create_errno(base, md, KOM_UNDEF_SESSION, "err_undef_session",
		 "No such session exists.");
    create_errno(base, md, KOM_REGEX_ERROR, "err_regex_error",
		 "Regexp compilation failed.");
    create_errno(base, md, KOM_NOT_MARKED, "err_not_marked",
		 "Cannot unmark an unmarked text.");
    create_errno(base, md, KOM_TEMPFAIL, "err_tempfail",
		 "Temporary failure -- please try again later.");
    create_errno(base, md, KOM_LONG_ARRAY, "err_long_array",
		 "Attempt to send too much data in an array to the server.");
    create_errno(base, md, KOM_ANON_REJECTED, "err_anon_rejected",
		 "Anonymous texts not allowed in that conference.");
    create_errno(base, md, KOM_ILLEGAL_AUX_ITEM, "err_illegal_aux_item",
		 "An illegal aux-item was found.");
    create_errno(base, md, KOM_AUX_ITEM_PERMISSION, "err_aux_item_permission",
		 "You lack permission to create this aux-item.");
    create_errno(base, md, KOM_UNKNOWN_ASYNC, "err_unknown_async",
		 "Unknown async seen.");
    create_errno(base, md, KOM_INTERNAL_ERROR, "err_internal_error",
		 "Internal server error detected.");
    create_errno(base, md, KOM_FEATURE_DISABLED, "err_feature_disabled",
		 "Feature disabled.");
    create_errno(base, md, KOM_MESSAGE_NOT_SENT, "err_message_not_sent",
		 "Async message not sent.");
    create_errno(base, md, KOM_INVALID_MEMBERSHIP_TYPE, 
		 "err_invalid_membership_type",
		 "Bad membership type.");
    create_errno(base, md, KOM_INVALID_RANGE, "err_invalid_range",
		 "Lower limit higher than upper limit.");
    create_errno(base, md, KOM_INVALID_RANGE_LIST, 
		 "err_invalid_range_list",
		 "Ranges not sorted.");
    create_errno(base, md, KOM_UNDEFINED_MEASUREMENT,
		 "err_undefined_measurement",
		 "That measurement is not made.");
    create_errno(base, md, KOM_PRIORITY_DENIED, "err_priority_denied",
		 "You lack privileges to do that.");
    create_errno(base, md, KOM_WEIGHT_DENIED, "err_weight_denied",
		 "You lack privileges to do that.");
    create_errno(base, md, KOM_WEIGHT_ZERO, "err_weight_zero",
		 "Weight must be non-zero.");
    create_errno(base, md, KOM_BAD_BOOL, "err_bad_bool",
		 "A bool must be sent as 0 or 1.");
    

    // Local error codes, created by kom++.
    create_errno(base, md, KOM_ANCIENT_SERVER, "err_ancient_server",
		 "The server is too old to use this client.");
    create_errno(base, md, KOM_UNINITIALIZED, "err_uninitialized",
		 "Internal client error: something was uninitialized.");
    create_errno(base, md, KOM_INDETERMINATE, "err_unknown",
		 "An error has occured, but kom++ has forgotten which.");
    create_errno(base, md, KOM_INVALIDATED, "err_invalidated",
		 "Attempt to use an invalidated cached object.");
    create_errno(base, md, KOM_NO_USER_AREA, "err_no_user_area",
		 "The user does not have a user area.");
    create_errno(base, md, KOM_KEY_NOT_FOUND, "err_key_not_found",
		 "The specified key does not exist.");
    create_errno(base, md, KOM_UNKNOWN_ERROR, "err_unknown_error",
		 "An error code that isn't known to kom++ was returned.");
    create_errno(base, md, KOM_BAD_HOLLERITH, "err_bad_hollerith",
		 "A malformed hollerith string was detected.");
    create_errno(base, md, KOM_BAD_USER_AREA, "err_bad_user_area",
		 "The user area is malformed.");
    
}

static void
set_errno_error(int err)
{
    PyObject *cls;		// exception class object
    PyObject *exc;		// exception instance object

    if (errno_classes[err] != NULL)
    {
	cls = errno_classes[err];
	exc = PyInstance_New(cls, NULL, NULL);
    }
    else
    {
	cls = errno_base_class;
	exc = PyInstance_New(cls, NULL, NULL);
	PyObject *number = PyInt_FromLong(err);
	LyStr s("unknown error number ");
	s << err;
	const char *buf = s.view_block();
	PyObject *str = PyString_FromString((char*)buf);
	s.view_done(buf);
	// FIXME: Is there a cleaner way to do this?
	// I don't feel comfortable accessing the cl_dict
	// member.
	PyDict_SetItemString(((PyClassObject*)exc)->cl_dict,
			     "errno", number);
	PyDict_SetItemString(((PyClassObject*)exc)->cl_dict,
			     "errmsg", str);
	Py_DECREF(number);
	Py_DECREF(str);
    }
    PyErr_SetObject(cls, exc);
    Py_DECREF(exc);
}

// Python Types--------------------------------------------

static int serial = 102;

ENDEXTERNC

template <class MSG>
struct komxx_async_tag {
    komxx_async_tag(Async_handler_tag<MSG> t, int s) : tag(t), serial(s) {}
    komxx_async_tag() : tag(), serial(0) {}
    Async_handler_tag<MSG> tag;
    int serial;
};

struct komxx_session_tags {
    std::vector<komxx_async_tag<Async_text_created>*> text_created_tags;
    std::vector<komxx_async_tag<Async_text_message>*> text_message_tags;
};

BEGINEXTERNC

typedef struct {
    PyObject_HEAD
    Connection *connection;
    Conn_io_ISC *conn_io;
    Session *session;
    Unread_confs *unread_confs;
    komxx_session_tags *tags;
} komxx_session_object;

typedef struct {
    PyObject_HEAD
    Text *data;
} komxx_text_object;

typedef struct {
    PyObject_HEAD
    Conference *data;
} komxx_conf_object;

typedef struct {
    PyObject_HEAD
    Person *data;
} komxx_pers_object;

typedef struct {
    PyObject_HEAD
    Uplink *data;
} komxx_uplink_object;

typedef struct {
    PyObject_HEAD
    Recipient *data;
} komxx_recipient_object;

typedef struct {
    PyObject_HEAD
    Conf_type *data;
} komxx_conf_type_object;

typedef struct {
    PyObject_HEAD
    Priv_bits *data;
} komxx_priv_bits_object;

typedef struct {
    PyObject_HEAD
    Membership_type *data;
} komxx_membership_type_object;

typedef struct {
    PyObject_HEAD
    Session_flags *data;
} komxx_session_flags_object;

typedef struct {
    PyObject_HEAD
    Aux_item *data;
} komxx_aux_item_object;

typedef struct {
    PyObject_HEAD
    Conf_z_info *data;
} komxx_conf_z_info_object;

typedef struct {
    PyObject_HEAD
    Text_mapping *data;
} komxx_text_mapping_object;

typedef struct {
    PyObject_HEAD
    Created_mapping *data;
} komxx_created_mapping_object;

typedef struct {
    PyObject_HEAD
    question *data;
} komxx_question_object;

// Support functions---------------------------------------

ENDEXTERNC

template<class C>
PyObject *
convert_aux_items(const C &data);

static PyObject *
convert_to_python(const Session_flags &t);

static PyObject *
convert_to_python(Status s)
{
    switch(s)
    {
    case st_pending:
	Py_INCREF(komxx_st_pending);
	return komxx_st_pending;
    case st_ok:
	Py_INCREF(komxx_st_ok);
	return komxx_st_ok;
    case st_error:
	Py_INCREF(komxx_st_error);
	return komxx_st_error;
    }
    abort();
}

static PyObject *
convert_to_python(bool b)
{
    if (b)
    {
	Py_INCREF(Py_True);
	return Py_True;
    }
    else
    {
	Py_INCREF(Py_False);
	return Py_False;
    }
}

static PyObject *
convert_to_python(trinary t)
{
    if (is_true(t))
    {
	Py_INCREF(Py_True);
	return Py_True;
    }
    else if (is_false(t))
    {
	Py_INCREF(Py_False);
	return Py_False;
    }
    else
    {
	Py_INCREF(Py_None);
	return Py_None;
    }
}

static PyObject *
convert_to_python(struct tm t)
{
    if (t.tm_wday == 0)
	t.tm_wday = 6;
    else
	t.tm_wday--;

    t.tm_year += 1900;
    t.tm_mon++;
    t.tm_yday++;

    return Py_BuildValue("(lllllllll)",
			 (long)t.tm_year, (long)t.tm_mon, (long)t.tm_mday,
			 (long)t.tm_hour, (long)t.tm_min, (long)t.tm_sec,
			 (long)t.tm_wday, (long)t.tm_yday, (long)t.tm_isdst);
}

static PyObject *
convert_to_python(unsigned long l)
{
    return PyInt_FromLong(l);
}

static PyObject *
convert_to_python(long l)
{
    return PyInt_FromLong(l);
}

static PyObject *
convert_to_python(int i)
{
    return PyInt_FromLong(i);
}

static PyObject *
convert_to_python(unsigned int i)
{
    return PyInt_FromLong(i);
}

static PyObject *
convert_to_python(unsigned short s)
{
    return PyInt_FromLong(s);
}

static PyObject *
convert_to_python(const LyStr &str)
{
    const char *buf = str.view_block();
    PyObject *rv = Py_BuildValue("s#", buf, str.strlen());
    str.view_done(buf);
    return rv;
}

static PyObject *
convert_to_python(const Unread_info &info)
{
    PyObject *tuple = PyTuple_New(3);
    if (tuple == NULL) 
	return NULL;

    PyObject *conf = convert_to_python(info.conf);
    PyObject *minu = convert_to_python(info.min_unread);
    PyObject *maxu = convert_to_python(info.max_unread);

    if (conf == NULL || minu == NULL || maxu == NULL)
    {
	Py_XDECREF(conf);
	Py_XDECREF(minu);
	Py_XDECREF(maxu);
	Py_DECREF(tuple);
	return NULL;
    }
    int r = 0;
    r |= PyTuple_SetItem(tuple, 0, conf);
    r |= PyTuple_SetItem(tuple, 1, minu);
    r |= PyTuple_SetItem(tuple, 2, maxu);
    if (r != 0)
    {
	Py_DECREF(tuple);
	return NULL;
    }
    return tuple;
}

static PyObject *
convert_to_python(const Unread_info_estimated &info)
{
    PyObject *tuple = PyTuple_New(2);
    if (tuple == NULL) 
	return NULL;

    PyObject *conf = convert_to_python(info.conf);
    PyObject *esti = convert_to_python(info.unread);

    if (conf == NULL || esti == NULL)
    {
	Py_XDECREF(conf);
	Py_XDECREF(esti);
	Py_DECREF(tuple);
	return NULL;
    }
    int r = 0;
    r |= PyTuple_SetItem(tuple, 0, conf);
    r |= PyTuple_SetItem(tuple, 1, esti);
    if (r != 0)
    {
	Py_DECREF(tuple);
	return NULL;
    }
    return tuple;
}

static PyObject *
convert_to_python(const Who_info_ident &info)
{
    PyObject *tuple = PyTuple_New(7);
    if (tuple == NULL) 
	return NULL;

    PyObject *pers = convert_to_python(info.pers_no());
    PyObject *work = convert_to_python(info.working_conf());
    PyObject *sess = convert_to_python(info.session());
    PyObject *doin = convert_to_python(info.doing());
    PyObject *unam = convert_to_python(info.username());
    PyObject *hnam = convert_to_python(info.hostname());
    PyObject *idus = convert_to_python(info.identuser());

    if (pers == NULL || work == NULL ||
	sess == NULL || doin == NULL || unam == NULL ||
	hnam == NULL || idus == NULL)
    {
	Py_XDECREF(pers);
	Py_XDECREF(work);
	Py_XDECREF(sess);
	Py_XDECREF(doin);
	Py_XDECREF(unam);
	Py_XDECREF(hnam);
	Py_XDECREF(idus);
	Py_DECREF(tuple);
	return NULL;
    }
    int r = 0;
    r |= PyTuple_SetItem(tuple, 0, pers);
    r |= PyTuple_SetItem(tuple, 1, work);
    r |= PyTuple_SetItem(tuple, 2, sess);
    r |= PyTuple_SetItem(tuple, 3, doin);
    r |= PyTuple_SetItem(tuple, 4, unam);
    r |= PyTuple_SetItem(tuple, 5, hnam);
    r |= PyTuple_SetItem(tuple, 6, idus);
    if (r != 0)
    {
	Py_DECREF(tuple);
	return NULL;
    }
    return tuple;
}

static PyObject *
convert_to_python(const Dynamic_session_info &info)
{
    PyObject *tuple = PyTuple_New(6);
    if (tuple == NULL) 
	return NULL;

    PyObject *sess = convert_to_python(info.session());
    PyObject *pers = convert_to_python(info.pers_no());
    PyObject *work = convert_to_python(info.working_conf());
    PyObject *idle = convert_to_python(info.idle_time());
    PyObject *flgs = convert_to_python(info.flags());
    PyObject *doin = convert_to_python(info.doing());

    if (sess == NULL || pers == NULL ||
	work == NULL || idle == NULL ||
	flgs == NULL || doin == NULL)
    {
	Py_XDECREF(sess);
	Py_XDECREF(pers);
	Py_XDECREF(work);
	Py_XDECREF(idle);
	Py_XDECREF(flgs);
	Py_XDECREF(doin);
	return NULL;
    }
    int r = 0;
    r |= PyTuple_SetItem(tuple, 0, sess);
    r |= PyTuple_SetItem(tuple, 1, pers);
    r |= PyTuple_SetItem(tuple, 2, work);
    r |= PyTuple_SetItem(tuple, 3, idle);
    r |= PyTuple_SetItem(tuple, 4, flgs);
    r |= PyTuple_SetItem(tuple, 5, doin);
    if (r != 0)
    {
	Py_DECREF(tuple);
	return NULL;
    }
    return tuple;
}

static PyObject *
convert_to_python(const Mark &mark)
{
    PyObject *tuple = PyTuple_New(2);
    if (tuple == NULL) 
	return NULL;

    PyObject *text = convert_to_python(mark.text_no());
    PyObject *type = convert_to_python(mark.type());

    if (text == NULL || type == NULL)
    {
	Py_XDECREF(text);
	Py_XDECREF(type);
	Py_DECREF(tuple);
	return NULL;
    }
    int r = 0;
    r |= PyTuple_SetItem(tuple, 0, text);
    r |= PyTuple_SetItem(tuple, 1, type);
    if (r != 0)
    {
	Py_DECREF(tuple);
	return NULL;
    }
    return tuple;
}

static PyObject *
convert_to_python(const Read_item &item)
{
    PyObject *tuple = PyTuple_New(2);
    if (tuple == NULL) 
	return NULL;

    PyObject *text = convert_to_python(item.text_no);
    PyObject *level = convert_to_python(item.nesting_level);

    if (text == NULL || level == NULL)
    {
	Py_XDECREF(text);
	Py_XDECREF(level);
	Py_DECREF(tuple);
	return NULL;
    }
    int r = 0;
    r |= PyTuple_SetItem(tuple, 0, text);
    r |= PyTuple_SetItem(tuple, 1, level);
    if (r != 0)
    {
	Py_DECREF(tuple);
	return NULL;
    }
    return tuple;
}

template <class T>
static PyObject *
convert_to_python(const std::vector<T> &arr)
{
    PyObject *res = PyList_New(arr.size());
    if (res == NULL)
	return NULL;

    for (unsigned int i = 0; i < arr.size(); i++)
    {
	PyObject *tuple = convert_to_python(arr[i]);
	
	if (PyList_SetItem(res, i, tuple))
	{
	    Py_DECREF(res);
	    return NULL;
	}
    }

    return res;
}

static PyObject *
convert_to_python(const Server_info &info)
{
    PyObject *tuple = PyTuple_New(7);
    if (tuple == NULL) 
	return NULL;

    PyObject *vers = convert_to_python(info.version());
    PyObject *conf = convert_to_python(info.conf_pres_conf());
    PyObject *pers = convert_to_python(info.pers_pres_conf());
    PyObject *motd = convert_to_python(info.motd_conf());
    PyObject *news = convert_to_python(info.kom_news_conf());
    PyObject *molk = convert_to_python(info.motd_of_lyskom());
    PyObject *aux  = convert_aux_items(info);

    if (vers == NULL || conf == NULL ||
	pers == NULL || motd == NULL ||
	news == NULL || molk == NULL ||
	aux == NULL)
    {
	Py_XDECREF(vers);
	Py_XDECREF(conf);
	Py_XDECREF(pers);
	Py_XDECREF(motd);
	Py_XDECREF(news);
	Py_XDECREF(molk);
	Py_XDECREF(aux);
	return NULL;
    }
    int r = 0;
    r |= PyTuple_SetItem(tuple, 0, vers);
    r |= PyTuple_SetItem(tuple, 1, conf);
    r |= PyTuple_SetItem(tuple, 2, pers);
    r |= PyTuple_SetItem(tuple, 3, motd);
    r |= PyTuple_SetItem(tuple, 4, news);
    r |= PyTuple_SetItem(tuple, 5, molk);
    r |= PyTuple_SetItem(tuple, 6, aux);
    if (r != 0)
    {
	Py_DECREF(tuple);
	return NULL;
    }
    return tuple;
}

static PyObject *
convert_to_python(const Kom_err &ecode)
{
    if (ecode == KOM_NO_ERROR)
    {
	Py_INCREF(Py_None);
	return Py_None;
    }
    else
    {
	set_errno_error(ecode);
	return NULL;
    }
}

template <class T>
static PyObject *
convert_to_python(const Kom_res<T> &r)
{
    if (r.error != KOM_NO_ERROR)
    {
	set_errno_error(r.error);
	return NULL;
    }
    else
	return convert_to_python(r.data);
}

static PyObject *
convert_to_python(const Local2Global &m)
{
    PyObject *res = PyList_New(m.size());
    if (res == NULL)
	return NULL;

    for (int i = 0; i < m.size(); i++)
    {
	PyObject *tuple = convert_to_python(m[m.first() + i]);
	
	if (PyList_SetItem(res, i, tuple))
	{
	    Py_DECREF(res);
	    return NULL;
	}
    }

    return res;
}

static PyObject *
convert_to_python(const Version_info &version_info)
{
    PyObject *tuple = PyTuple_New(3);
    if (tuple == NULL) 
	return NULL;

    PyObject *proto = convert_to_python(version_info.protocol_version());
    PyObject *software = convert_to_python(version_info.server_software());
    PyObject *version = convert_to_python(version_info.software_version());

    if (proto == NULL || software == NULL || version == NULL)
    {
	Py_XDECREF(proto);
	Py_XDECREF(software);
	Py_XDECREF(version);
	Py_DECREF(tuple);
	return NULL;
    }
    int r = 0;
    r |= PyTuple_SetItem(tuple, 0, proto);
    r |= PyTuple_SetItem(tuple, 1, software);
    r |= PyTuple_SetItem(tuple, 2, version);
    if (r != 0)
    {
	Py_DECREF(tuple);
	return NULL;
    }
    return tuple;
}

static PyObject *
convert_to_python(const Static_session_info &s)
{
    PyObject *tuple = PyTuple_New(4);
    if (tuple == NULL) 
	return NULL;

    PyObject *user = convert_to_python(s.user_name());
    PyObject *host = convert_to_python(s.hostname());
    PyObject *ident = convert_to_python(s.ident_user());
    PyObject *conn_time = convert_to_python(s.connection_time());

    if (user == NULL || host == NULL || ident == NULL || conn_time == NULL)
    {
	Py_XDECREF(user);
	Py_XDECREF(host);
	Py_XDECREF(ident);
	Py_DECREF(conn_time);
	return NULL;
    }
    int r = 0;
    r |= PyTuple_SetItem(tuple, 0, user);
    r |= PyTuple_SetItem(tuple, 1, host);
    r |= PyTuple_SetItem(tuple, 2, ident);
    r |= PyTuple_SetItem(tuple, 3, conn_time);
    if (r != 0)
    {
	Py_DECREF(tuple);
	return NULL;
    }
    return tuple;
}


static PyObject *
convert_to_python(const Conf_type &t);

static PyObject *
convert_to_python(const Priv_bits &t);

static PyObject *
convert_to_python(const Membership_type &t);

static PyObject *
convert_to_python(const Member &info)
{
    PyObject *tuple = PyTuple_New(4);
    if (tuple == NULL) 
	return NULL;

    PyObject *memb = convert_to_python(info.member());
    PyObject *adby = convert_to_python(info.added_by());
    PyObject *adat = convert_to_python(info.added_at());
    PyObject *type = convert_to_python(info.type());

    if (memb == NULL || adby == NULL
	|| adat == NULL || type == NULL)
    {
	Py_XDECREF(memb);
	Py_XDECREF(adby);
	Py_XDECREF(adat);
	Py_XDECREF(type);
	Py_DECREF(tuple);
	return NULL;
    }
    int r = 0;
    r |= PyTuple_SetItem(tuple, 0, memb);
    r |= PyTuple_SetItem(tuple, 1, adby);
    r |= PyTuple_SetItem(tuple, 2, adat);
    r |= PyTuple_SetItem(tuple, 3, type);
    if (r != 0)
    {
	Py_DECREF(tuple);
	return NULL;
    }
    return tuple;
}

template <class Q, class R = typename Q::result_type>
struct convert_result {
    static PyObject * to_python(Q*);
};

template <class Q>
struct convert_result<Q, void> {
    static PyObject * to_python(Q*);
};

template <class Q, class R>
PyObject *
convert_result<Q, R>::to_python(Q *q)
{ 
    return convert_to_python(q->result());
}

template <class Q>
PyObject *
convert_result<Q, void>::to_python(Q*)
{ 
    Py_INCREF(Py_None);
    return Py_None;
}

template <class Q>
static PyObject *
result_or_exception(Q *q)
{
    if (q->receive() == st_error)
    {
	set_errno_error(q->error());
	return NULL;
    }
    else
    {
	return convert_result<Q>::to_python(q);
    }
}

static Status
decode_aux_list(PyObject *aux_items,
		std::vector<Aux_item> *aux);

static Status
decode_aux_no_list(PyObject *aux_items,
		   std::vector<Aux_no> *aux);

BEGINEXTERNC

#include "selector.incl"
#include "selector-assertion.incl"
#include "selector-aggregate.incl"
#include "selector-aux-flag.incl"
#include "selector-aux-flag-set.incl"
#include "selector-errno.incl"
#include "selector-uconf-errno.incl"
#include "selector-LyStr-errno.incl"


// Komxx_uplink <-> Python glue----------------------------------

static void
komxx_uplink_dealloc(komxx_uplink_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyMethodDef komxx_uplink_methods[] = {
    {"is_footnote", komxx_uplink_is_footnote, 1, "No doc"},
    {"parent", komxx_uplink_parent, 1, "No doc"},
    {"sender", komxx_uplink_sender, 1, "No doc"},
    {"sent_later", komxx_uplink_sent_later, 1, "No doc"},
    {"sent_at", komxx_uplink_sent_at, 1, "No doc"},
    {NULL,		NULL, 1, "end"}		/* sentinel */
};

static PyObject *
komxx_uplink_getattr(komxx_uplink_object *self,
		     char *name)
{
    return Py_FindMethod(komxx_uplink_methods, (PyObject *) self, name);
}

static PyTypeObject Komxx_uplink_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"uplink",			/*tp_name*/
	sizeof(komxx_uplink_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_uplink_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_uplink_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM uplink object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};


// Komxx_recipient <-> Python glue----------------------------------

static void
komxx_recipient_dealloc(komxx_recipient_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyMethodDef komxx_recipient_methods[] = {
    {"recipient", komxx_recipient_recipient, 1, "No doc"},
    {"is_carbon_copy", komxx_recipient_is_carbon_copy, 1, "No doc"},
    {"local_no", komxx_recipient_local_no, 1, "No doc"},
    {"has_rec_time", komxx_recipient_has_rec_time, 1, "No doc"},
    {"rec_time", komxx_recipient_rec_time, 1, "No doc"},
    {"sender", komxx_recipient_sender, 1, "No doc"},
    {"has_sent_at", komxx_recipient_has_sent_at, 1, "No doc"},
    {"sent_at", komxx_recipient_sent_at, 1, "No doc"},
    {NULL,		NULL, 1, "end"}		/* sentinel */
};

static PyObject *
komxx_recipient_getattr(komxx_recipient_object *self,
			char *name)
{
    return Py_FindMethod(komxx_recipient_methods, (PyObject *) self, name);
}

static PyTypeObject Komxx_recipient_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"recipient",			/*tp_name*/
	sizeof(komxx_recipient_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_recipient_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_recipient_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM recipient object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};


// Komxx_text methods-----------------------------------
// Exported methods.


static PyObject *
komxx_text_entry_time(PyObject *s,
		      PyObject *args)
{
    komxx_text_object *self = (komxx_text_object*)s;

    int conf_no;
    int local_no;

    if (!PyArg_ParseTuple(args, "ii", &conf_no, &local_no))
	return NULL;

    if (self->data->kom_errno() != 0)
    {
	set_errno_error(self->data->kom_errno());
	return NULL;
    }

    return convert_to_python(self->data->entry_time(conf_no, local_no));
}


static PyObject *
komxx_text_recipients(PyObject *s,
		      PyObject *args)
{
    komxx_text_object *self = (komxx_text_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (self->data->kom_errno() != 0)
    {
	set_errno_error(self->data->kom_errno());
	return NULL;
    }

    PyObject *res = PyList_New(self->data->num_recipients());
    for (int i = 0; i < self->data->num_recipients(); i++)
    {
	komxx_recipient_object *r;
	r = PyObject_NEW(komxx_recipient_object, &Komxx_recipient_type);
	r->data = new Recipient(*self->data->recipient(i));
	if (PyList_SetItem(res, i, (PyObject *)r))
	{
	    Py_DECREF(res);
	    return NULL;
	}
    }
    
    return res;
}

static PyObject *
komxx_text_comments_in(PyObject *s,
		       PyObject *args)
{
    komxx_text_object *self = (komxx_text_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (self->data->kom_errno() != 0)
    {
	set_errno_error(self->data->kom_errno());
	return NULL;
    }

    PyObject *res = PyList_New(self->data->num_comment_in());
    for (int i = 0; i < self->data->num_comment_in(); i++)
    {
	PyObject *c = Py_BuildValue("i", self->data->comment_in(i));
	if (PyList_SetItem(res, i, c))
	{
	    Py_DECREF(res);
	    return NULL;
	}
    }
    
    return res;
}

static PyObject *
komxx_text_footnotes_in(PyObject *s,
		       PyObject *args)
{
    komxx_text_object *self = (komxx_text_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (self->data->kom_errno() != 0)
    {
	set_errno_error(self->data->kom_errno());
	return NULL;
    }

    PyObject *res = PyList_New(self->data->num_footnote_in());
    for (int i = 0; i < self->data->num_footnote_in(); i++)
    {
	PyObject *c = Py_BuildValue("i", self->data->footnote_in(i));
	if (PyList_SetItem(res, i, c))
	{
	    Py_DECREF(res);
	    return NULL;
	}
    }
    
    return res;
}

static PyObject *
komxx_text_uplinks(PyObject *s,
		      PyObject *args)
{
    komxx_text_object *self = (komxx_text_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (self->data->kom_errno() != 0)
    {
	set_errno_error(self->data->kom_errno());
	return NULL;
    }

    PyObject *res = PyList_New(self->data->num_uplinks());
    for (int i = 0; i < self->data->num_uplinks(); i++)
    {
	komxx_uplink_object *r;
	r = PyObject_NEW(komxx_uplink_object, &Komxx_uplink_type);
	r->data = new Uplink(*self->data->uplink(i));
	if (PyList_SetItem(res, i, (PyObject *)r))
	{
	    Py_DECREF(res);
	    return NULL;
	}
    }
    
    return res;
}


static PyObject *
komxx_text_mark(PyObject *s,
		PyObject *args)
{
    komxx_text_object *self = (komxx_text_object*)s;

    int mark_type;

    if (!PyArg_ParseTuple(args, "i", &mark_type))
	return NULL;

    return convert_to_python(self->data->mark(mark_type));
}


static PyObject *
komxx_text_add_recipient(PyObject *s,
			 PyObject *args)
{
    komxx_text_object *self = (komxx_text_object*)s;

    int recipient;

    if (!PyArg_ParseTuple(args, "i", &recipient))
	return NULL;

    return convert_to_python(self->data->add_recipient(recipient));
}


static PyObject *
komxx_text_sub_recipient(PyObject *s,
			 PyObject *args)
{
    komxx_text_object *self = (komxx_text_object*)s;

    int recipient;

    if (!PyArg_ParseTuple(args, "i", &recipient))
	return NULL;

    return convert_to_python(self->data->sub_recipient(recipient));
}


static PyObject *
komxx_text_modify_info(PyObject *s, PyObject *args)
{
    komxx_text_object *self = (komxx_text_object*)s;
    PyObject *aux_nos_del;
    PyObject *aux_items_add;

    if (!PyArg_ParseTuple(args, "O!O!",
			  &PyList_Type, &aux_nos_del,
			  &PyList_Type, &aux_items_add))
	return NULL;

    std::vector<Aux_no> del;
    std::vector<Aux_item> add;

    if (decode_aux_no_list(aux_nos_del, &del) != st_ok)
	return NULL;

    if (decode_aux_list(aux_items_add, &add) != st_ok)
	return NULL;

    return convert_to_python(self->data->modify_info(del, add));
}


static PyObject *
komxx_text_local_no_recipient(PyObject *s,
			      PyObject *args)
{
    komxx_text_object *self = (komxx_text_object*)s;

    int cno;

    if (!PyArg_ParseTuple(args, "i", &cno))
	return NULL;

    if (self->data->kom_errno() != 0)
    {
	set_errno_error(self->data->kom_errno());
	return NULL;
    }

    return convert_to_python(self->data->local_no_recipient(cno));
}



// Komxx_text <-> Python glue----------------------------------

static void
komxx_text_dealloc(komxx_text_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyObject *
komxx_text_aux_items(PyObject *s,
		     PyObject *args);

static PyMethodDef komxx_text_methods[] = {
	{"text_no",	  komxx_text_text_no,       1, "No doc"},
	{"prefetch",	  komxx_text_prefetch,      1, "No doc"},
	{"prefetch_text", komxx_text_prefetch_text, 1, "No doc"},
	{"present",	  komxx_text_present,       1, "No doc"},
	{"exists",	  komxx_text_exists,        1, "No doc"},
	{"errno",	  komxx_text_kom_errno,     1, "No doc"},
	{"creation_time", komxx_text_creation_time, 1, "No doc"},
	{"entry_time",    komxx_text_entry_time,    1, "No doc"},
	{"author",        komxx_text_author,        1, "No doc"},
	{"num_lines",	  komxx_text_num_lines,     1, "No doc"},
	{"num_chars",	  komxx_text_num_chars,     1, "No doc"},
	{"num_marks",	  komxx_text_num_marks,     1, "No doc"},
	{"recipients",    komxx_text_recipients,    1, "No doc"},
	{"comments_in",	  komxx_text_comments_in,   1, "No doc"},
	{"footnotes_in",  komxx_text_footnotes_in,  1, "No doc"},
	{"uplinks",	  komxx_text_uplinks,       1, "No doc"},
	{"aux_items",     komxx_text_aux_items,     1, "No doc"},
	{"subject",	  komxx_text_subject,       1, "No doc"},
	{"body",	  komxx_text_body,          1, "No doc"},
	{"text_mass",	  komxx_text_text_mass,     1, "No doc"},
	{"mark_as_read",  komxx_text_mark_as_read,  1, "No doc"},
	{"mark_as_unread", komxx_text_mark_as_unread, 1, "No doc"},
	{"mark",	  komxx_text_mark,	    1, "No doc"},
	{"unmark",   	  komxx_text_unmark,	    1, "No doc"},
	{"add_recipient", komxx_text_add_recipient, 1, "No doc"},
	{"sub_recipient", komxx_text_sub_recipient, 1, "No doc"},
	{"modify_info",   komxx_text_modify_info, 1, "No doc"},
	{"local_no_recipient", komxx_text_local_no_recipient, 1, "No doc"},
	{NULL,		NULL, 1, "end"}		/* sentinel */
};

static PyObject *
komxx_text_getattr(komxx_text_object *self,
		   char *name)
{
    return Py_FindMethod(komxx_text_methods, (PyObject *) self, name);
}

static PyTypeObject Komxx_text_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,				 /* ob_size*/
	"text",				 /* tp_name*/
	sizeof(komxx_text_object),	 /* tp_basicsize*/
	0,				 /* tp_itemsize*/
	/* methods */
	(destructor)komxx_text_dealloc,  /* tp_dealloc */
	0,				 /* tp_print */
	(getattrfunc)komxx_text_getattr, /* tp_getattr */
	0, 				 /* tp_setattr */
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
	0,			/*  tp_call */
	0, 			/*  tp_str */
	0, 			/*  tp_getattro */
	0, 			/*  tp_setattro */
	0, 			/*  tp_as_buffer */
	0, 			/*  tp_flags */
	"LysKOM text object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};


// Komxx_conf_type <-> Python glue----------------------------------

static void
komxx_conf_type_dealloc(komxx_conf_type_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyMethodDef komxx_conf_type_methods[] = {
    {"is_protected", komxx_conf_type_is_protected, 1, "No doc"},
    {"is_original",  komxx_conf_type_is_original,  1, "No doc"},
    {"is_secret",    komxx_conf_type_is_secret,    1, "No doc"},
    {"is_letterbox", komxx_conf_type_is_letterbox, 1, "No doc"},
    {"is_letterbox", komxx_conf_type_allows_anonymous, 1, "No doc"},
    {"is_letterbox", komxx_conf_type_forbids_secret, 1, "No doc"},
    {"is_letterbox", komxx_conf_type_res2, 1, "No doc"},
    {"is_letterbox", komxx_conf_type_res3, 1, "No doc"},
    {NULL,	     NULL,                         1, "end"}	/* sentinel */
};

static PyObject *
komxx_conf_type_getattr(komxx_conf_type_object *self,
			char *name)
{
    return Py_FindMethod(komxx_conf_type_methods, (PyObject *) self, name);
}

static PyTypeObject Komxx_conf_type_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"conf_type",			/*tp_name*/
	sizeof(komxx_conf_type_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_conf_type_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_conf_type_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM conf_type object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};


// Komxx_priv_bits <-> Python glue----------------------------------

static void
komxx_priv_bits_dealloc(komxx_priv_bits_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyMethodDef komxx_priv_bits_methods[] = {
    {"wheel", komxx_priv_bits_wheel, 1, "No doc"},
    {"admin", komxx_priv_bits_admin, 1, "No doc"},
    {"statistic", komxx_priv_bits_statistic, 1, "No doc"},
    {"create_pers", komxx_priv_bits_create_pers, 1, "No doc"},
    {"create_conf", komxx_priv_bits_create_conf, 1, "No doc"},
    {"change_name", komxx_priv_bits_change_name, 1, "No doc"},
    {"flg7", komxx_priv_bits_flg7, 1, "No doc"},
    {"flg8", komxx_priv_bits_flg8, 1, "No doc"},
    {"flg9", komxx_priv_bits_flg9, 1, "No doc"},
    {"flg10", komxx_priv_bits_flg10, 1, "No doc"},
    {"flg11", komxx_priv_bits_flg11, 1, "No doc"},
    {"flg12", komxx_priv_bits_flg12, 1, "No doc"},
    {"flg13", komxx_priv_bits_flg13, 1, "No doc"},
    {"flg14", komxx_priv_bits_flg14, 1, "No doc"},
    {"flg15", komxx_priv_bits_flg15, 1, "No doc"},
    {"flg16", komxx_priv_bits_flg16, 1, "No doc"},
    {NULL,	     NULL,                         1, "end"}	/* sentinel */
};

static PyObject *
komxx_priv_bits_getattr(komxx_priv_bits_object *self,
			char *name)
{
    return Py_FindMethod(komxx_priv_bits_methods, (PyObject *) self, name);
}

static PyTypeObject Komxx_priv_bits_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"priv_bits",			/*tp_name*/
	sizeof(komxx_priv_bits_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_priv_bits_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_priv_bits_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM priv_bits object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};

// Komxx_membership_type <-> Python glue----------------------------------

static void
komxx_membership_type_dealloc(komxx_membership_type_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyMethodDef komxx_membership_type_methods[] = {
    {"invitation", komxx_membership_type_invitation, 1, "No doc"},
    {"passive", komxx_membership_type_passive, 1, "No doc"},
    {"secret", komxx_membership_type_secret, 1, "No doc"},
    {"passive_message_invert", 
     komxx_membership_type_passive_message_invert, 1, "No doc"},
    {"reserved2", komxx_membership_type_reserved2, 1, "No doc"},
    {"reserved3", komxx_membership_type_reserved3, 1, "No doc"},
    {"reserved4", komxx_membership_type_reserved4, 1, "No doc"},
    {"reserved5", komxx_membership_type_reserved5, 1, "No doc"},
    {NULL,	     NULL,                         1, "end"}	/* sentinel */
};

static PyObject *
komxx_membership_type_getattr(komxx_membership_type_object *self,
			char *name)
{
    return Py_FindMethod(komxx_membership_type_methods, (PyObject *) self, name);
}

static PyTypeObject Komxx_membership_type_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"membership_type",			/*tp_name*/
	sizeof(komxx_membership_type_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_membership_type_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_membership_type_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM membership_type object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};

// komxx_session_flags <-> Python glue----------------------------------

static void
komxx_session_flags_dealloc(komxx_session_flags_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyMethodDef komxx_session_flags_methods[] = {
    {"invisible", komxx_session_flags_invisible, 1, "No doc"},
    {"user_active_used", komxx_session_flags_user_active_used, 1, "No doc"},
    {"user_absent", komxx_session_flags_user_absent, 1, "No doc"},
    {"reserved3", komxx_session_flags_reserved3, 1, "No doc"},
    {"reserved4", komxx_session_flags_reserved4, 1, "No doc"},
    {"reserved5", komxx_session_flags_reserved5, 1, "No doc"},
    {"reserved6", komxx_session_flags_reserved6, 1, "No doc"},
    {"reserved7", komxx_session_flags_reserved7, 1, "No doc"},
};

static PyObject *
komxx_session_flags_getattr(komxx_session_flags_object *self,
			char *name)
{
    return Py_FindMethod(komxx_session_flags_methods, (PyObject *) self, name);
}

static PyTypeObject Komxx_session_flags_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"session_flags",			/*tp_name*/
	sizeof(komxx_session_flags_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_session_flags_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_session_flags_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM session_flags object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};


// Komxx_aux_item <-> Python glue----------------------------------


static void
komxx_aux_item_dealloc(komxx_aux_item_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyObject *
komxx_aux_item_set_tag(PyObject *s, PyObject *args)
{
    komxx_aux_item_object *self = (komxx_aux_item_object*)s;

    int tag;

    if (!PyArg_ParseTuple(args, "i", &tag))
	return NULL;

    self->data->set_tag(tag);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
komxx_aux_item_set_inherit_limit(PyObject *s, PyObject *args)
{
    komxx_aux_item_object *self = (komxx_aux_item_object*)s;

    int limit;

    if (!PyArg_ParseTuple(args, "i", &limit))
	return NULL;

    self->data->set_inherit_limit(limit);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
komxx_aux_item_set_data(PyObject *s, PyObject *args)
{
    komxx_aux_item_object *self = (komxx_aux_item_object*)s;

    char *data;
    int data_len;

    if (!PyArg_ParseTuple(args, "s#", &data, &data_len))
	return NULL;

    self->data->set_data(LyStr(data, data_len));
    Py_INCREF(Py_None);
    return Py_None;
}


static PyMethodDef komxx_aux_item_methods[] = {
    {"aux_no", komxx_aux_item_aux_no, 1, "No doc"},
    {"tag", komxx_aux_item_tag, 1, "No doc"},
    {"creator", komxx_aux_item_creator, 1, "No doc"},
    {"created_at", komxx_aux_item_created_at, 1, "No doc"},
    {"inherit_limit", komxx_aux_item_inherit_limit, 1, "No doc"},
    {"data", komxx_aux_item_data, 1, "No doc"},
    {"deleted", komxx_aux_item_deleted, 1, "No doc"},
    {"inherit", komxx_aux_item_inherit, 1, "No doc"},
    {"secret", komxx_aux_item_secret, 1, "No doc"},
    {"hide_creator", komxx_aux_item_hide_creator, 1, "No doc"},
    {"dont_garb", komxx_aux_item_dont_garb, 1, "No doc"},
    {"reserved2", komxx_aux_item_reserved2, 1, "No doc"},
    {"reserved3", komxx_aux_item_reserved3, 1, "No doc"},
    {"reserved4", komxx_aux_item_reserved4, 1, "No doc"},

    {"set_tag", komxx_aux_item_set_tag, 1, "No doc"},
    {"set_inherit_limit", komxx_aux_item_set_inherit_limit, 1, "No doc"},
    {"set_data", komxx_aux_item_set_data, 1, "No doc"},
    {"set_deleted", komxx_aux_item_set_deleted, 1, "No doc"},
    {"set_inherit", komxx_aux_item_set_inherit, 1, "No doc"},
    {"set_secret", komxx_aux_item_set_secret, 1, "No doc"},
    {"set_hide_creator", komxx_aux_item_set_hide_creator, 1, "No doc"},
    {"set_dont_garb", komxx_aux_item_set_dont_garb, 1, "No doc"},
    {"set_reserved2", komxx_aux_item_set_reserved2, 1, "No doc"},
    {"set_reserved3", komxx_aux_item_set_reserved3, 1, "No doc"},
    {"set_reserved4", komxx_aux_item_set_reserved4, 1, "No doc"},
    {NULL,	     NULL,                         1, "end"}	/* sentinel */
};

static PyObject *
komxx_aux_item_getattr(komxx_aux_item_object *self,
			char *name)
{
    return Py_FindMethod(komxx_aux_item_methods, (PyObject *) self, name);
}

static PyTypeObject Komxx_aux_item_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"aux_item",			/*tp_name*/
	sizeof(komxx_aux_item_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_aux_item_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_aux_item_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM aux_item object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};


ENDEXTERNC

static Status
decode_aux_list(PyObject *aux_items,
		std::vector<Aux_item> *aux)
{
    if (aux_items != NULL)
	for (int i = 0; i < PyList_Size(aux_items); i++)
	{
	    PyObject *r = PyList_GetItem(aux_items, i);
	    if (PyErr_Occurred())
		return st_error;
	    if (r->ob_type != &Komxx_aux_item_type)
	    {
		throw_exception(PyExc_TypeError);
		return st_error;
	    }
	    aux->push_back(*reinterpret_cast<komxx_aux_item_object*>(r)->data);
	}
    return st_ok;
}


static Status
decode_aux_no_list(PyObject *aux_items,
		   std::vector<Aux_no> *aux)
{
    if (aux_items != NULL)
	for (int i = 0; i < PyList_Size(aux_items); i++)
	{
	    PyObject *r = PyList_GetItem(aux_items, i);
	    if (PyErr_Occurred())
		return st_error;
	    aux->push_back(PyInt_AsLong(r));
	    if (PyErr_Occurred())
		return st_error;
	}
    return st_ok;
}

BEGINEXTERNC


// Komxx_conf_z_info <-> Python glue----------------------------------

static void
komxx_conf_z_info_dealloc(komxx_conf_z_info_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyMethodDef komxx_conf_z_info_methods[] = {
    {"conf_no",   komxx_conf_z_info_conf_no,   1, "No doc"},
    {"conf_type", komxx_conf_z_info_conf_type, 1, "No doc"},
    {"name", komxx_conf_z_info_conf_name, 1, "No doc"},
    {NULL,		NULL, 1, "end"}		/* sentinel */
};

static PyObject *
komxx_conf_z_info_getattr(komxx_conf_z_info_object *self,
		   char *name)
{
    return Py_FindMethod(komxx_conf_z_info_methods, (PyObject *) self, name);
}

static PyTypeObject Komxx_conf_z_info_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"conf_z_info",			/*tp_name*/
	sizeof(komxx_conf_z_info_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_conf_z_info_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_conf_z_info_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM conf_z_info object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};

// Komxx_text_mapping <-> Python glue----------------------------------

static PyObject *
komxx_text_mapping_get_map(PyObject *s,
			   PyObject *args)
{
    komxx_text_mapping_object *self = (komxx_text_mapping_object*)s;
    int begin_local;
    int end_local;
	
    if (!PyArg_ParseTuple(args, "ii", &begin_local, &end_local))
	return NULL;

    int len = end_local - begin_local;
    if (len < 0)
	len = 0;

    // FIXME: we could prefetch better here.
    self->data->prefetch(begin_local);

    PyObject *res = PyList_New(len);
    for (int i = begin_local; i < begin_local + len; i++)
    {
	PyObject *number = PyInt_FromLong((*self->data)[i]);
	if (PyList_SetItem(res, i - begin_local, number))
	{
	    Py_DECREF(res);
	    return NULL;
	}
    }
    
    return res;
}

static PyObject *
komxx_text_mapping_next_existing(PyObject *s,
				 PyObject *args)
{
    komxx_text_mapping_object *self = (komxx_text_mapping_object*)s;
    int first;
	
    if (!PyArg_ParseTuple(args, "i", &first))
	return NULL;

    return convert_to_python(self->data->next_existing(first));
}

static PyObject *
komxx_text_mapping_prefetch(PyObject *s,
			    PyObject *args)
{
    komxx_text_mapping_object *self = (komxx_text_mapping_object*)s;
    int lno;
	
    if (!PyArg_ParseTuple(args, "i", &lno))
	return NULL;

    return convert_to_python(self->data->prefetch(lno));
}

static PyObject *
komxx_text_mapping_getitem(PyObject *s,
			   PyObject *subscript)
{
    komxx_text_mapping_object *self = (komxx_text_mapping_object*)s;
    long localno = PyInt_AsLong(subscript);

    return convert_to_python((*self->data)[localno]);
}

#if 0
static int
komxx_text_mapping_length(PyObject *s)
{
    komxx_text_mapping_object *self = (komxx_text_mapping_object*)s;

    return self->data->end_no() - self->data->begin_no();
}
#endif

static void
komxx_text_mapping_dealloc(komxx_text_mapping_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyMethodDef komxx_text_mapping_methods[] = {
    {"get_map", komxx_text_mapping_get_map, 1, "No doc"},
    {"next_existing", komxx_text_mapping_next_existing, 1, "No doc"},
    {"prefetch", komxx_text_mapping_prefetch, 1, "No doc"},
    {NULL,		NULL, 1, "end"}		/* sentinel */
};

static PyObject *
komxx_text_mapping_getattr(komxx_text_mapping_object *self,
		   char *name)
{
    return Py_FindMethod(komxx_text_mapping_methods, (PyObject *) self, name);
}


static PyMappingMethods komxx_text_mapping_as_mapping = {
#if 0
    komxx_text_mapping_length,	// mp_length
#else
    0,				// mp_length
#endif
    komxx_text_mapping_getitem,	// mp_subscript
    0				// ms_ass_subscript
};

static PyTypeObject Komxx_text_mapping_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"text_mapping",			/*tp_name*/
	sizeof(komxx_text_mapping_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_text_mapping_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_text_mapping_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	&komxx_text_mapping_as_mapping, /*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM text_mapping object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};


// Komxx_created_mapping <-> Python glue----------------------------------

static PyObject *
komxx_created_mapping_get_map(PyObject *s,
			      PyObject *args)
{
    komxx_created_mapping_object *self = (komxx_created_mapping_object*)s;
    int begin_local;
    int end_local;
	
    if (!PyArg_ParseTuple(args, "ii", &begin_local, &end_local))
	return NULL;

    int len = end_local - begin_local;
    if (len < 0)
	len = 0;

    // FIXME: we could prefetch better here.
    self->data->prefetch(begin_local);

    PyObject *res = PyList_New(len);
    for (int i = begin_local; i < begin_local + len; i++)
    {
	PyObject *number = PyInt_FromLong((*self->data)[i]);
	if (PyList_SetItem(res, i - begin_local, number))
	{
	    Py_DECREF(res);
	    return NULL;
	}
    }
    
    return res;
}

static PyObject *
komxx_created_mapping_next_existing(PyObject *s,
				    PyObject *args)
{
    komxx_created_mapping_object *self = (komxx_created_mapping_object*)s;
    int first;
	
    if (!PyArg_ParseTuple(args, "i", &first))
	return NULL;

    return convert_to_python(self->data->next_existing(first));
}

static PyObject *
komxx_created_mapping_getitem(PyObject *s,
			      PyObject *subscript)
{
    komxx_created_mapping_object *self = (komxx_created_mapping_object*)s;
    long localno = PyInt_AsLong(subscript);

    return convert_to_python((*self->data)[localno]);
}

static void
komxx_created_mapping_dealloc(komxx_created_mapping_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyMethodDef komxx_created_mapping_methods[] = {
    {"get_map", komxx_created_mapping_get_map, 1, "No doc"},
    {"next_existing", komxx_created_mapping_next_existing, 1, "No doc"},
    {NULL,		NULL, 1, "end"}		/* sentinel */
};

static PyObject *
komxx_created_mapping_getattr(komxx_created_mapping_object *self,
			      char *name)
{
    return Py_FindMethod(komxx_created_mapping_methods, (PyObject *) self, name);
}


static PyMappingMethods komxx_created_mapping_as_mapping = {
#if 0
    komxx_created_mapping_length,	// mp_length
#else
    0,				// mp_length
#endif
    komxx_created_mapping_getitem,	// mp_subscript
    0				// ms_ass_subscript
};

static PyTypeObject Komxx_created_mapping_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"created_mapping",			/*tp_name*/
	sizeof(komxx_created_mapping_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_created_mapping_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_created_mapping_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	&komxx_created_mapping_as_mapping, /*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM created texts_mapping object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};


// Komxx_question <-> Python glue----------------------------------

static void
komxx_question_dealloc(komxx_question_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyMethodDef komxx_question_methods[] = {
    {"status", komxx_question_status, 1, "No doc"},
    {"receive", komxx_question_receive, 1, "No doc"},
    {"error", komxx_question_error, 1, "No doc"},
    {"error_blocking", komxx_question_error_blocking, 1, "No doc"},
    {NULL,		NULL, 1, "end"}		/* sentinel */
};

static PyObject *
komxx_question_getattr(komxx_question_object *self,
		       char *name)
{
    return Py_FindMethod(komxx_question_methods, (PyObject *) self, name);
}


static PyTypeObject Komxx_question_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"question",			/*tp_name*/
	sizeof(komxx_question_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_question_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_question_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM created question object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};


// Komxx_conf <-> Python glue----------------------------------

static PyObject *
komxx_conf_set_type(PyObject *s, PyObject *args)
{
    komxx_conf_object *self = (komxx_conf_object*)s;
    int        is_protected;
    int        is_original;
    int        is_secret;
    int        is_letterbox;

    if (!PyArg_ParseTuple(args, "(iiii)", 
			  &is_protected,
			  &is_original,
			  &is_secret,
			  &is_letterbox))
	return NULL;

    // Argument parsing and conversion complete.  Now do the job.
    return convert_to_python(
	self->data->set_type(Conf_type((bool) is_protected,
				       (bool) is_original,
				       (bool) is_secret, 
				       (bool) is_letterbox)));
}


static PyObject *
komxx_conf_change_name(PyObject *s, PyObject *args)
{
    komxx_conf_object *self = (komxx_conf_object*)s;
    char  * new_name;
    int     new_name_len;


    if (!PyArg_ParseTuple(args, "s#", &new_name, &new_name_len)) 
	return NULL;

    // Argument parsing and conversion complete.  Now do the job.
    return convert_to_python(
	self->data->change_name(LyStr(new_name, new_name_len)));
}

static PyObject *
komxx_conf_set_permitted_submitters(PyObject *s, PyObject *args)
{
    komxx_conf_object *self = (komxx_conf_object*)s;
    int     permsub;


    if (!PyArg_ParseTuple(args, "i", &permsub)) 
	return NULL;

    // Argument parsing and conversion complete.  Now do the job.
    return convert_to_python(
	self->data->set_permitted_submitters(permsub));
}


static PyObject *
komxx_conf_modify_info(PyObject *s, PyObject *args)
{
    komxx_conf_object *self = (komxx_conf_object*)s;
    PyObject *aux_nos_del;
    PyObject *aux_items_add;

    if (!PyArg_ParseTuple(args, "O!O!",
			  &PyList_Type, &aux_nos_del,
			  &PyList_Type, &aux_items_add))
	return NULL;

    std::vector<Aux_no> del;
    std::vector<Aux_item> add;

    if (decode_aux_no_list(aux_nos_del, &del) != st_ok)
	return NULL;

    if (decode_aux_list(aux_items_add, &add) != st_ok)
	return NULL;

    return convert_to_python(self->data->modify_info(del, add));
}


static PyObject *
komxx_conf_set_expire(PyObject *s, PyObject *args)
{
    komxx_conf_object *self = (komxx_conf_object*)s;
    int     expire;


    if (!PyArg_ParseTuple(args, "i", &expire)) 
	return NULL;

    // Argument parsing and conversion complete.  Now do the job.
    return convert_to_python(
	self->data->set_expire(expire));
}

static PyObject *
komxx_conf_set_garb_nice(PyObject *s, PyObject *args)
{
    komxx_conf_object *self = (komxx_conf_object*)s;
    int     garb_nice;


    if (!PyArg_ParseTuple(args, "i", &garb_nice)) 
	return NULL;

    // Argument parsing and conversion complete.  Now do the job.
    return convert_to_python(
	self->data->set_garb_nice(garb_nice));
}

static PyObject *
komxx_conf_set_keep_commented(PyObject *s, PyObject *args)
{
    komxx_conf_object *self = (komxx_conf_object*)s;
    int     keep_commented;


    if (!PyArg_ParseTuple(args, "i", &keep_commented)) 
	return NULL;

    // Argument parsing and conversion complete.  Now do the job.
    return convert_to_python(
	self->data->set_keep_commented(keep_commented));
}

static PyObject *
komxx_conf_aux_items(PyObject *s,
		     PyObject *args)
{
    komxx_conf_object *self = (komxx_conf_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (self->data->kom_errno() != 0)
    {
	set_errno_error(self->data->kom_errno());
	return NULL;
    }

    return convert_aux_items(*self->data);
}


static void
komxx_conf_dealloc(komxx_conf_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyMethodDef komxx_conf_methods[] = {
    {"prefetch",             komxx_conf_prefetch,             1, "No doc"},
    {"prefetch_uconf",       komxx_conf_prefetch_uconf,       1, "No doc"},
    {"errno",                komxx_conf_kom_errno,            1, "No doc"},
    {"present",              komxx_conf_present,              1, "No doc"},
    {"exists",               komxx_conf_exists,               1, "No doc"},
    {"conf_no",              komxx_conf_conf_no,              1, "No doc"},
    {"creator",              komxx_conf_creator,              1, "No doc"},
    {"creation_time",        komxx_conf_creation_time,        1, "No doc"},
    {"presentation",         komxx_conf_presentation,         1, "No doc"},
    {"supervisor",           komxx_conf_supervisor,           1, "No doc"},
    {"permitted_submitters", komxx_conf_permitted_submitters, 1, "No doc"},
    {"super_conf",           komxx_conf_super_conf,           1, "No doc"},
    {"type",                 komxx_conf_type,                 1, "No doc"},
    {"last_written",         komxx_conf_last_written,         1, "No doc"},
    {"msg_of_day",           komxx_conf_msg_of_day,           1, "No doc"},
    {"nice",                 komxx_conf_nice,                 1, "No doc"},
    {"keep_commented",       komxx_conf_keep_commented,       1, "No doc"},
    {"name",                 komxx_conf_name,                 1, "No doc"},
    {"no_of_members",        komxx_conf_no_of_members,        1, "No doc"},
    {"first_local_no",       komxx_conf_first_local_no,       1, "No doc"},
    {"no_of_texts",          komxx_conf_no_of_texts,          1, "No doc"},
    {"highest_local_no",     komxx_conf_highest_local_no,     1, "No doc"},
    {"expire",               komxx_conf_expire,               1, "No doc"},
    {"aux_items",            komxx_conf_aux_items,            1, "No doc"},

    {"set_type",             komxx_conf_set_type,             1, "No doc"},
    {"change_name",          komxx_conf_change_name,          1, "No doc"},
    {"set_permitted_submitters", 
     komxx_conf_set_permitted_submitters,		      1, "No doc"},
    {"modify_info",          komxx_conf_modify_info,          1, "No doc"},
    {"set_expire",           komxx_conf_set_expire,           1, "No doc"},
    {"set_garb_nice",        komxx_conf_set_garb_nice,        1, "No doc"},
    {"set_keep_commented",   komxx_conf_set_keep_commented,   1, "No doc"},
    {NULL, NULL, 1, "end"}		/* sentinel */
};

static PyObject *
komxx_conf_getattr(komxx_conf_object *self,
		   char *name)
{
    return Py_FindMethod(komxx_conf_methods, (PyObject *) self, name);
}

static PyTypeObject Komxx_conf_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,				/*ob_size*/
	"conf",				/*tp_name*/
	sizeof(komxx_conf_object),	/*tp_basicsize*/
	0,				/*tp_itemsize*/
	/* methods */
	(destructor)komxx_conf_dealloc, /*tp_dealloc*/
	0,				/*tp_print*/
	(getattrfunc)komxx_conf_getattr, /*tp_getattr*/
	0,				/*tp_setattr*/
	0,				/*tp_compare*/
	0,				/*tp_repr*/
	0,				/*tp_as_number*/
	0,				/*tp_as_sequence*/
	0,				/*tp_as_mapping*/
	0,				/*tp_hash*/
	0,				/*  tp_call */
	0,				/*  tp_str */
	0,				/*  tp_getattro */
	0,				/*  tp_setattro */
	0,				/*  tp_as_buffer */
	0,				/*  tp_flags */
	"LysKOM conf object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};


// Komxx_pers <-> Python glue----------------------------------

static void
komxx_pers_dealloc(komxx_pers_object *self)
{
    delete self->data;
    PyMem_DEL(self);
}

static PyObject *
komxx_pers_created_texts(PyObject *s, PyObject *args)
{
    komxx_pers_object *self = (komxx_pers_object*)s;
    int first;
    int last;

    if (!PyArg_ParseTuple(args, "ii", &first, &last))
	return NULL;

    get_created_texts_question q(self->data->connection(),
				 self->data->conf_no(),
				 first,
				 last-first);
    
    return result_or_exception(&q);
}

static PyObject *
komxx_pers_set_privileges(PyObject *s, PyObject *args)
{
    komxx_pers_object *self = (komxx_pers_object*)s;

    PyObject *py_priv_bits = NULL;

    if (!PyArg_ParseTuple(args, "O!", &Komxx_priv_bits_type, &py_priv_bits))
	return NULL;

    komxx_priv_bits_object *priv_bits(
	reinterpret_cast<komxx_priv_bits_object*>(py_priv_bits));

    set_priv_bits_question q(self->data->connection(),
			     self->data->conf_no(),
			     *priv_bits->data);

    return result_or_exception(&q);
}

static PyObject *
komxx_pers_set_personal_flags(PyObject *s, PyObject *args)
{
    komxx_pers_object *self = (komxx_pers_object*)s;

    int unread_is_secret;

    if (!PyArg_ParseTuple(args, "i", &unread_is_secret))
	return NULL;

    set_pers_flags_question q(self->data->connection(),
			      self->data->conf_no(),
			      Personal_flags(unread_is_secret));

    return result_or_exception(&q);
}

static PyObject *
komxx_pers_unread_is_secret(PyObject *s, PyObject *args)
{
    komxx_pers_object *self = (komxx_pers_object*)s;

    if (self->data->kom_errno() != 0)
    {
	set_errno_error(self->data->kom_errno());
	return NULL;
    }

    return convert_to_python(self->data->flags().unread_is_secret());
}

static PyMethodDef komxx_pers_methods[] = {
    {"prefetch", komxx_pers_prefetch, 1, "No doc"},
    {"prefetch_conf_only", komxx_pers_prefetch_conf_only, 1, "No doc"},
    {"prefetch_uconf_only", komxx_pers_prefetch_uconf_only, 1, "No doc"},
    {"prefetch_pers_only", komxx_pers_prefetch_pers_only, 1, "No doc"},
    {"exists", komxx_pers_exists, 1, "No doc"},
    {"present", komxx_pers_present, 1, "No doc"},
    {"conf_present", komxx_pers_conf_present, 1, "No doc"},
    {"pers_present", komxx_pers_pers_present, 1, "No doc"},
    {"errno", komxx_pers_kom_errno, 1, "No doc"},
    {"pers_no", komxx_pers_pers_no, 1, "No doc"},
    {"user_area", komxx_pers_user_area, 1, "No doc"},
    {"privileges", komxx_pers_privileges, 1, "No doc"},
    {"set_privileges", komxx_pers_set_privileges, 1, "No doc"},
    {"set_personal_flags", komxx_pers_set_personal_flags, 1, "No doc"},
    {"unread_is_secret", komxx_pers_unread_is_secret, 1, "No doc"},
    {"last_login", komxx_pers_last_login, 1, "No doc"},
    {"total_time_present", komxx_pers_total_time_present, 1, "No doc"},
    {"sessions", komxx_pers_sessions, 1, "No doc"},
    {"created_lines", komxx_pers_created_lines, 1, "No doc"},
    {"created_bytes", komxx_pers_created_bytes, 1, "No doc"},
    {"read_texts", komxx_pers_read_texts, 1, "No doc"},
    {"no_of_text_fetches", komxx_pers_no_of_text_fetches, 1, "No doc"},
    {"no_of_marks", komxx_pers_no_of_marks, 1, "No doc"},
    {"no_of_confs", komxx_pers_no_of_confs, 1, "No doc"},
    {"creator", komxx_pers_creator, 1, "No doc"},
    {"creation_time", komxx_pers_creation_time, 1, "No doc"},
    {"presentation", komxx_pers_presentation, 1, "No doc"},
    {"supervisor", komxx_pers_supervisor, 1, "No doc"},
    {"permitted_submitters", komxx_pers_permitted_submitters, 1, "No doc"},
    {"super_conf", komxx_pers_super_conf, 1, "No doc"},
    {"last_written", komxx_pers_last_written, 1, "No doc"},
    {"msg_of_day", komxx_pers_msg_of_day, 1, "No doc"},
    {"nice", komxx_pers_nice, 1, "No doc"},
    {"name", komxx_pers_name, 1, "No doc"},
    {"no_of_members", komxx_pers_no_of_members, 1, "No doc"},
    {"first_local_no", komxx_pers_first_local_no, 1, "No doc"},
    {"no_of_texts", komxx_pers_no_of_texts, 1, "No doc"},
    {"highest_local_no", komxx_pers_highest_local_no, 1, "No doc"},
    {"first_created_text", komxx_pers_first_created_text, 1, "No doc"},
    {"no_of_created_texts", komxx_pers_no_of_created_texts, 1, "No doc"},
    {"created_texts", komxx_pers_created_texts, 1, "No doc"},
    {NULL, NULL, 1, "end"}		/* sentinel */
};

static PyObject *
komxx_pers_getattr(komxx_pers_object *self,
		   char *name)
{
    return Py_FindMethod(komxx_pers_methods, (PyObject *) self, name);
}

static PyTypeObject Komxx_pers_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"pers",			/*tp_name*/
	sizeof(komxx_pers_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_pers_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_pers_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM pers object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};


// Komxx_session methods-----------------------------------

// Helper function: make sure that unread_confs has been fetched.
static Status
require_unread_confs(komxx_session_object *self)
{
    if (self->unread_confs == NULL)
    {
	if (self->session->my_login() == 0)
	{
	    throw_exception(error_not_logged_in_class);
	    return st_error;
	}
	self->unread_confs = new Unread_confs(self->session,
					      self->session->my_login());
    }

    return st_ok;
}

// Helper function: make sure that unread_confs has been fetched.
static Status
require_selected_conf(komxx_session_object *self)
{
    if (require_unread_confs(self) != st_ok)
	return st_error;

    if (self->unread_confs->current_conf() == 0)
    {
	throw_exception(error_no_conf_selected_class);
	return st_error;
    }

    return st_ok;
}

// Exported methods.

ENDEXTERNC

struct Callback {
    PyObject *fnc;
    PyObject *py_data;
    Callback(PyObject *f, PyObject *d) : fnc(f), py_data(d) {
	Py_INCREF(fnc);
	Py_INCREF(py_data);
    }
    ~Callback() {
	Py_DECREF(fnc);
	Py_DECREF(py_data);
    }
    Callback(const Callback &c) : fnc(c.fnc), py_data(c.py_data) {
	Py_INCREF(fnc);
	Py_INCREF(py_data);
    }
  private:
    Callback &operator=(const Callback &);
};

BEGINEXTERNC

// Async message: text created.

static void
text_created_callback(const Async_text_created &msg,
		      Callback &callback)
{
    PyObject *res = PyObject_CallFunction(callback.fnc, "lO",
					  (long)msg.text_stat().text_no(),
					  callback.py_data);
    if (res == NULL)
    {
#if 0
	// FIXME: store the error for later.

	if (first_error == NULL)
	    first_error = current_backtrace;
	last_error = current_backtrace;
#endif
	PyErr_Clear();
    }
    else
    {
	// Silently ignore the result of the callback function.
	Py_DECREF(res);
    }
}



static PyObject *
komxx_session_register_async_text_created(PyObject *s,
					  PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    PyObject *cb;
    PyObject *extradata;

    if (!PyArg_ParseTuple(args, "OO", &cb, &extradata))
	return NULL;

    Async_dispatcher  &dispatcher = self->session->async_dispatcher();

    Kom_auto_ptr<Async_callback<Async_text_created> > ptr;
    ptr.reset(async_callback_function(text_created_callback, 
				      Callback(cb, extradata)).release());
    Async_handler_tag<Async_text_created> tag(
	dispatcher.register_text_created_handler(ptr));

    self->tags->text_created_tags.push_back(
	new komxx_async_tag<Async_text_created>(tag, ++serial));

    return convert_to_python(serial);
}



static PyObject *
komxx_session_unregister_async_text_created(PyObject *s,
					    PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int tagno;

    if (!PyArg_ParseTuple(args, "i", &tagno))
	return NULL;

    typedef std::vector<komxx_async_tag<Async_text_created>*> vec;

    vec &v(self->tags->text_created_tags);

    for (vec::iterator iter = v.begin(); iter != v.end(); ++iter)
    {
	if ((*iter)->serial == tagno)
	{
	    Async_dispatcher  &dispatcher = self->session->async_dispatcher();
	    dispatcher.unregister_text_created_handler((*iter)->tag);
	    delete *iter;
	    v.erase(iter);
	    Py_INCREF(Py_None);
	    return Py_None;
	}
    }

    throw_exception(error_bad_async_tag_class);
    return NULL;
}


// Async message: text message.

void
text_message_callback(const Async_text_message &msg,
		      Callback &callback)
{
    LyStr str = msg.message();
    const char *buf = str.view_block();

    PyObject *res = PyObject_CallFunction(
	callback.fnc, "lls#O", (long)msg.recipient(), (long)msg.sender(),
	buf, str.strlen(), callback.py_data);

    str.view_done(buf);

    if (res == NULL)
    {
#if 0
	// FIXME: store the error for later.

	if (first_error == NULL)
	    first_error = current_backtrace;
	last_error = current_backtrace;
#endif
	PyErr_Clear();
    }
    else
    {
	// Silently ignore the result of the callback function.
	Py_DECREF(res);
    }
}

static PyObject *
komxx_session_register_async_text_message(PyObject *s,
					  PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    PyObject *cb;
    PyObject *extradata;

    if (!PyArg_ParseTuple(args, "OO", &cb, &extradata))
	return NULL;

    Async_dispatcher  &dispatcher = self->session->async_dispatcher();

    Kom_auto_ptr<Async_callback<Async_text_message> > ptr;
    ptr.reset(async_callback_function(text_message_callback, 
				      Callback(cb, extradata)).release());
    Async_handler_tag<Async_text_message> tag(
	dispatcher.register_text_message_handler(ptr));

    self->tags->text_message_tags.push_back(
	new komxx_async_tag<Async_text_message>(tag, ++serial));

    return convert_to_python(serial);
}

static PyObject *
komxx_session_unregister_async_text_message(PyObject *s,
					    PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int tagno;

    if (!PyArg_ParseTuple(args, "i", &tagno))
	return NULL;

    typedef std::vector<komxx_async_tag<Async_text_message>*> vec;

    vec &v(self->tags->text_message_tags);

    for (vec::iterator iter = v.begin(); iter != v.end(); ++iter)
    {
	if ((*iter)->serial == tagno)
	{
	    Async_dispatcher  &dispatcher = self->session->async_dispatcher();
	    dispatcher.unregister_text_message_handler((*iter)->tag);
	    delete *iter;
	    v.erase(iter);
	    Py_INCREF(Py_None);
	    return Py_None;
	}
    }

    throw_exception(error_bad_async_tag_class);
    return NULL;
}

// Call this when all async callbacks are registered.

static PyObject *
komxx_session_do_accept_async(PyObject *s,
			      PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    self->session->async_dispatcher().do_accept_async();

    Py_INCREF(Py_None);
    return Py_None;
}

// End of async handling.

static PyObject *
komxx_session_create_person(PyObject *s,
			    PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    char *name;
    int name_len;
    char *pwd;
    int pwd_len;
    PyObject *aux_items = NULL;
    int unread_is_secret = 0;

    if (!PyArg_ParseTuple(args, "s#s#O!i", 
			  &name, &name_len, &pwd, &pwd_len,
			  &PyList_Type, &aux_items,
			  &unread_is_secret))
	return NULL;

    std::vector<Aux_item> aux;

    if (decode_aux_list(aux_items, &aux) != st_ok)
	return NULL;

    return convert_to_python(
	self->session->create_person(LyStr(name, name_len),
				     LyStr(pwd, pwd_len),
				     Personal_flags(unread_is_secret),
				     aux));
}

static PyObject *
komxx_session_login(PyObject *s,
		    PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int pno = 0;
    char *pwd = NULL;
    int pwd_len = 0;
    int invisible = 0;

    if (!PyArg_ParseTuple(args, "is#|i", &pno, &pwd, &pwd_len, &invisible))
	return NULL;

    switch (self->session->login(pno, LyStr(pwd, pwd_len), invisible))
    {
    case st_ok:
	return convert_to_python(st_ok);
    case st_error:
	return convert_to_python(st_error);
    default:
	abort();
    }
}

static PyObject *
komxx_session_logout(PyObject *s,
		    PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (self->unread_confs != NULL)
    {
	delete self->unread_confs;
	self->unread_confs = NULL;
    }
    
    return convert_to_python(self->session->logout());
}

static PyObject *
komxx_session_enable(PyObject *s,
		     PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int level;

    if (!PyArg_ParseTuple(args, "i", &level))
	return NULL;

    return convert_to_python(self->session->enable(level));
}

static PyObject *
komxx_session_set_presentation(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int cno;
    int tno;

    if (!PyArg_ParseTuple(args, "ii", &cno, &tno))
	return NULL;

    if (cno < 0 || tno < 0)
    {
	PyErr_SetString(PyExc_ValueError, "Integer out of range");
	return NULL;
    }

    return convert_to_python(
	self->session->set_presentation(cno, tno));
}

static PyObject *
komxx_session_add_member(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int cno;
    int reader;
    int prio;
    int place;

    PyObject *py_membership_type;

    if (!PyArg_ParseTuple(args, "iiiiO!", &cno, &reader, &prio, &place,
			  &Komxx_membership_type_type, &py_membership_type))
	return NULL;

    komxx_membership_type_object *type(
	reinterpret_cast<komxx_membership_type_object*>(py_membership_type));

    if (reader < 0 || cno < 0 || prio < 0 || place < 0
	|| prio > 255 || place > 255)
    {
	PyErr_SetString(PyExc_ValueError, "Integer out of range");
	return NULL;
    }

    if (reader == self->session->my_login())
    {
	// This code is almost verbatimly duplicated in
	// komxx_session_join_conference.
	if (require_unread_confs(self) != st_ok)
	    return NULL;

	return convert_to_python(
	    self->unread_confs->join_conference(cno, prio, place, 
						*type->data));
    }
    else 
    {
	return convert_to_python(self->session->add_member(cno, reader,
							   prio, place,
							   *type->data));
    }
}

static PyObject *
komxx_session_sub_member(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int cno;
    int reader;

    if (!PyArg_ParseTuple(args, "ii", &cno, &reader))
	return NULL;

    if (reader < 0 || cno < 0)
    {
	PyErr_SetString(PyExc_ValueError, "Integer out of range");
	return NULL;
    }

    if (reader == self->session->my_login())
    {
	// This code is almost verbatimly duplicated in
	// komxx_session_disjoin_conference.
	if (require_unread_confs(self) != st_ok)
	    return NULL;

	return convert_to_python(
	    self->unread_confs->disjoin_conference(cno));
    }
    else 
	return convert_to_python(self->session->sub_member(cno, reader));
}

static PyObject *
komxx_session_set_membership_type(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int member;
    int cno;

    PyObject *py_membership_type;

    if (!PyArg_ParseTuple(args, "iiO!", &member, &cno,
			  &Komxx_membership_type_type, &py_membership_type))
	return NULL;

    komxx_membership_type_object *type(
	reinterpret_cast<komxx_membership_type_object*>(py_membership_type));

    if (member < 0 || cno < 0)
    {
	PyErr_SetString(PyExc_ValueError, "Integer out of range");
	return NULL;
    }

    return convert_to_python(
	self->session->set_membership_type(member, cno, *type->data));
}

static PyObject *
komxx_session_send_message(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int rcpt;
    char *msg;
    int msg_len;

    if (!PyArg_ParseTuple(args, "is#", &rcpt, &msg, &msg_len))
	return NULL;

    if (rcpt < 0)
    {
	PyErr_SetString(PyExc_ValueError, "Integer out of range");
	return NULL;
    }

    return convert_to_python(self->session->send_message(rcpt,
							 LyStr(msg, msg_len)));
}

static PyObject *
komxx_session_change_what_i_am_doing(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    char *doing;
    int doing_len;

    if (!PyArg_ParseTuple(args, "s#", &doing, &doing_len))
	return NULL;

    return convert_to_python(
	self->session->change_what_i_am_doing(LyStr(doing, doing_len)));
}


static PyObject *
komxx_session_create_conference(PyObject *s,
				PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    char *name;
    int name_len;
    int protect;
    int orig;
    int secr;
    PyObject *aux_items = NULL;
    int allow_anonymous = 1;
    int forbid_secret = 0;
    int reserved2 = 0;
    int reserved3 = 0;

    if (!PyArg_ParseTuple(args, "s#iii|O!iiii", &name, &name_len,
			  &protect, &orig, &secr,
			  &PyList_Type, &aux_items,
			  &allow_anonymous, &forbid_secret, 
			  &reserved2, &reserved3))
	return NULL;

    std::vector<Aux_item> aux;

    if (decode_aux_list(aux_items, &aux) != st_ok)
	return NULL;

    return convert_to_python(
	self->session->create_conference(
	    LyStr(name, name_len),
	    Conf_type(protect, orig, secr, false,
		      allow_anonymous, forbid_secret, reserved2, reserved3),
	    aux));
}


static PyObject *
komxx_session_prefetch_is_member(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int pers;
    int conf;

    if (!PyArg_ParseTuple(args, "ii", &pers, &conf))
	return NULL;

    return convert_to_python(
	self->session->prefetch_is_member(pers, conf));
}

static PyObject *
komxx_session_is_member(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int pers;
    int conf;

    if (!PyArg_ParseTuple(args, "ii", &pers, &conf))
	return NULL;

    return convert_to_python(
	self->session->is_member(pers, conf));
}

static PyObject *
komxx_session_is_supervisor(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int pers;
    int conf;

    if (!PyArg_ParseTuple(args, "ii", &pers, &conf))
	return NULL;

    return convert_to_python(
	self->session->is_supervisor(pers, conf));
}


static PyObject *
komxx_session_create_text(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    char *subject;
    int subject_len;
    char *body;
    int body_len;
    PyObject *recips;
    PyObject *cc_recips;
    PyObject *comment_to;
    PyObject *footnote_to;
    PyObject *aux_items = NULL;
    int anonymous = 0;

    if (!PyArg_ParseTuple(args, "s#s#O!O!O!O!|O!i",
			  &subject, &subject_len, &body, &body_len,
			  &PyList_Type, &recips, 
			  &PyList_Type, &cc_recips, 
			  &PyList_Type, &comment_to, 
			  &PyList_Type, &footnote_to,
			  &PyList_Type, &aux_items,
			  &anonymous))
	return NULL;

    std::vector<Recipient> rec;
    std::vector<Text_no> com;
    std::vector<Text_no> foot;
    std::vector<Aux_item> aux;

    for (int i = 0; i < PyList_Size(recips); i++)
    {
	PyObject *r = PyList_GetItem(recips, i);
	long recip = PyInt_AsLong(r);
	if (PyErr_Occurred())
	    return NULL;
	rec.push_back(Recipient((Conf_no)recip, false));
    }
    for (int i = 0; i < PyList_Size(cc_recips); i++)
    {
	PyObject *r = PyList_GetItem(cc_recips, i);
	long recip = PyInt_AsLong(r);
	if (PyErr_Occurred())
	    return NULL;
	rec.push_back(Recipient((Conf_no)recip, true));
    }
    for (int i = 0; i < PyList_Size(comment_to); i++)
    {
	PyObject *r = PyList_GetItem(comment_to, i);
	long tno = PyInt_AsLong(r);
	if (PyErr_Occurred())
	    return NULL;
	com.push_back((Text_no)tno);
    }
    for (int i = 0; i < PyList_Size(footnote_to); i++)
    {
	PyObject *r = PyList_GetItem(footnote_to, i);
	long tno = PyInt_AsLong(r);
	if (PyErr_Occurred())
	    return NULL;
	foot.push_back((Text_no)tno);
    }
    LyStr text_mass(subject, subject_len);
    text_mass << '\n' << LyStr(body, body_len);

    if (decode_aux_list(aux_items, &aux) != st_ok)
	return NULL;

    if (anonymous)
    {
	create_anonymous_text_question q(self->connection, text_mass, 
					 rec, com, foot, aux);
	return result_or_exception(&q);
    }
    else if (aux.size() == 0)
    {
	create_text_old_question q(self->connection, text_mass, 
				   rec, com, foot);
	return result_or_exception(&q);
    }
    else
    {
	create_text_question q(self->connection, text_mass, 
			       rec, com, foot, aux);
	return result_or_exception(&q);
    }
}

static PyObject *
komxx_session_set_server_info(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int conf;
    int pers;
    int motd;
    int news;
    int molk;

    if (!PyArg_ParseTuple(args, "iiiii", &conf, &pers, &motd, &news, &molk))
	return NULL;

    if (conf < 0 || pers < 0 || motd < 0 || news < 0 || molk < 0)
    {
	PyErr_SetString(PyExc_ValueError, "Integer out of range");
	return NULL;
    }

    return convert_to_python(
	self->session->set_server_info(conf, pers, motd, news, molk));
}


static PyObject *
komxx_session_lookup_z_name(PyObject *s,
			    PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    char *templ = NULL;
    int templ_len = 0;
    int want_persons = 1;
    int want_confs = 1;

    if (!PyArg_ParseTuple(args, "s#|ii", &templ, &templ_len,
			  &want_persons, &want_confs))
	return NULL;

    // FIXME: error handling.    
    std::vector<Conf_z_info> matches(self->session
				->lookup_z_name(LyStr(templ, templ_len),
						want_persons, want_confs));

    PyObject *res = PyList_New(matches.size());
    for (unsigned int i = 0; i < matches.size(); i++)
    {
	komxx_conf_z_info_object *r;
	r = PyObject_NEW(komxx_conf_z_info_object, &Komxx_conf_z_info_type);
	r->data = new Conf_z_info(matches[i]);
	if (PyList_SetItem(res, i, (PyObject *)r))
	{
	    Py_DECREF(res);
	    return NULL;
	}
    }

    return res;
}

static PyObject *
komxx_session_re_z_lookup(PyObject *s,
			  PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    char *templ = NULL;
    int templ_len = 0;
    int want_persons = 1;
    int want_confs = 1;

    if (!PyArg_ParseTuple(args, "s#|ii", &templ, &templ_len,
			  &want_persons, &want_confs))
	return NULL;

    // FIXME: error handling.    
    std::vector<Conf_z_info> matches(self->session
				->re_z_lookup(LyStr(templ, templ_len),
					      want_persons, want_confs));

    PyObject *res = PyList_New(matches.size());
    for (unsigned int i = 0; i < matches.size(); i++)
    {
	komxx_conf_z_info_object *r;
	r = PyObject_NEW(komxx_conf_z_info_object, &Komxx_conf_z_info_type);
	r->data = new Conf_z_info(matches[i]);
	if (PyList_SetItem(res, i, (PyObject *)r))
	{
	    Py_DECREF(res);
	    return NULL;
	}
    }

    return res;
}

static PyObject *
komxx_session_text(PyObject *s,
		   PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int tno = 0;
    if (!PyArg_ParseTuple(args, "i", &tno))
	return NULL;

    komxx_text_object *rv = PyObject_NEW(komxx_text_object, &Komxx_text_type);
    if (rv == NULL)
	return NULL;

    rv->data = new Text(self->session->texts()[tno]);
    return (PyObject*)rv;
}

static PyObject *
komxx_session_conf(PyObject *s,
		   PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int cno = 0;
    if (!PyArg_ParseTuple(args, "i", &cno))
	return NULL;

    komxx_conf_object *rv = PyObject_NEW(komxx_conf_object, &Komxx_conf_type);
    if (rv == NULL)
	return NULL;

    rv->data = new Conference(self->session->conferences()[cno]);
    return (PyObject*)rv;
}

static PyObject *
komxx_session_pers(PyObject *s,
		   PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int pno = 0;
    if (!PyArg_ParseTuple(args, "i", &pno))
	return NULL;

    komxx_pers_object *rv = PyObject_NEW(komxx_pers_object, &Komxx_pers_type);
    if (rv == NULL)
	return NULL;

    rv->data = new Person(self->session->persons()[pno]);
    return (PyObject*)rv;
}

static PyObject *
komxx_session_text_mapping(PyObject *s,
			   PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int cno = 0;
    if (!PyArg_ParseTuple(args, "i", &cno))
	return NULL;

    if (self->session->my_login() == 0)
    {
	set_errno_error(KOM_LOGIN);
	return NULL;
    }

    komxx_text_mapping_object *rv = PyObject_NEW(komxx_text_mapping_object,
						 &Komxx_text_mapping_type);
    if (rv == NULL)
	return NULL;

    rv->data = new Text_mapping(self->session->maps()[cno]);
    return (PyObject*)rv;
}

static PyObject *
komxx_session_created_mapping(PyObject *s,
			      PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int pno = 0;
    if (!PyArg_ParseTuple(args, "i", &pno))
	return NULL;

    komxx_created_mapping_object *rv = PyObject_NEW(
	komxx_created_mapping_object,
	&Komxx_created_mapping_type);
    if (rv == NULL)
	return NULL;


    rv->data = new Created_mapping(self->session->created_maps()[pno]);
    return (PyObject*)rv;
}

static PyObject *
komxx_session_prefetch(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int texts;
    int text_in_confs;
    int confs;
    int maps;
    int maps_in_confs;
    int map_confs;
    int max_question;

    Work_quota w;

    if (PyArg_ParseTuple(args, ""))
	w = Work_quota();
    else
    {
	PyErr_Clear();
	if (PyArg_ParseTuple(args, "iii", &maps, &texts, &max_question))
	    w = Work_quota(maps, texts, max_question);
	else
	{
	    PyErr_Clear();
	    if (PyArg_ParseTuple(args, "iiiiiii", 
				 &texts, &text_in_confs, &confs,
				 &maps, &maps_in_confs, &map_confs,
				 &max_question))
		w = Work_quota(texts, text_in_confs, confs, maps, 
			       maps_in_confs, map_confs, max_question);
	    else
		return NULL;
	}
    }

    if (require_unread_confs(self) != st_ok)
	return NULL;

    return convert_to_python(self->unread_confs->prefetch(&w));
}

static PyObject *
komxx_session_goto_next_conf(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (require_unread_confs(self) != st_ok)
	return NULL;

    return convert_to_python(self->unread_confs->goto_next_conf());
}

static PyObject *
komxx_session_query_next_conf(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (require_unread_confs(self) != st_ok)
	return NULL;

    return convert_to_python(self->unread_confs->query_next_conf());
}

static PyObject *
komxx_session_goto_conf(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int conf_no;

    if (!PyArg_ParseTuple(args, "i", &conf_no))
	return NULL;

    if (require_unread_confs(self) != st_ok)
	return NULL;

    return convert_to_python(self->unread_confs->goto_conf(conf_no));
}

static PyObject *
komxx_session_next_prompt(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (require_unread_confs(self) != st_ok)
	return NULL;

    return convert_to_python(self->unread_confs->next_prompt());
}

static PyObject *
komxx_session_next_text(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int remove_it = 0;
    
    if (!PyArg_ParseTuple(args, "|i", &remove_it))
	return NULL;

    if (require_selected_conf(self) != st_ok)
	return NULL;

    return convert_to_python(self->unread_confs->next_text(remove_it));
}

static PyObject *
komxx_session_enqueue_children(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int parent = 0;
    
    if (!PyArg_ParseTuple(args, "i", &parent))
	return NULL;

    if (require_selected_conf(self) != st_ok)
	return NULL;

    self->unread_confs->enqueue_children(parent);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
komxx_session_remove_text(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int tno;

    if (!PyArg_ParseTuple(args, "i", &tno))
	return NULL;

    if (require_unread_confs(self) != st_ok)
	return NULL;

    self->unread_confs->remove_text(tno);

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
komxx_session_current_conf(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    
    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (require_unread_confs(self) != st_ok)
	return NULL;

    return convert_to_python(self->unread_confs->current_conf());
}

static PyObject *
komxx_session_confs_with_unread(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    
    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (require_unread_confs(self) != st_ok)
	return NULL;

    return convert_to_python(self->unread_confs->confs_with_unread());
}

static PyObject *
komxx_session_confs_with_estimated_unread(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int max_limit = 0;
    
    if (!PyArg_ParseTuple(args, "|i", &max_limit))
	return NULL;

    if (require_unread_confs(self) != st_ok)
	return NULL;

    return convert_to_python(
	self->unread_confs->confs_with_estimated_unread(max_limit));
}

static PyObject *
komxx_session_min_unread_texts_in_current(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    
    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (require_selected_conf(self) != st_ok)
	return NULL;

    return convert_to_python(
	self->unread_confs->min_unread_texts_in_current());
}

static PyObject *
komxx_session_max_unread_texts_in_current(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    
    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (require_selected_conf(self) != st_ok)
	return NULL;

    return convert_to_python(
	self->unread_confs->max_unread_texts_in_current());
}

static PyObject *
komxx_session_estimated_unread_texts_in_current(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    
    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (require_selected_conf(self) != st_ok)
	return NULL;

    return convert_to_python(
	self->unread_confs->estimated_unread_texts_in_current());
}

static PyObject *
komxx_session_estimated_unread_texts_in_conf(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int conf_no;
    
    if (!PyArg_ParseTuple(args, "i", &conf_no))
	return NULL;

    if (require_unread_confs(self) != st_ok)
	return NULL;

    return convert_to_python(
	self->unread_confs->estimated_unread_texts_in_conf(conf_no));
}

static PyObject *
komxx_session_list_unread_texts_in_current(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int wanted_texts = 100;
    
    if (!PyArg_ParseTuple(args, "|i", &wanted_texts))
	return NULL;

    if (require_selected_conf(self) != st_ok)
	return NULL;

    Read_order_iterator begin = self->unread_confs->current_conf_begin(wanted_texts);
    Read_order_iterator end = self->unread_confs->current_conf_end();
    Read_order_iterator iter;
    int size = 0;
    for (iter = begin; iter != end; ++iter)
	++size;

    std::vector<Read_item> arr(size);
    int n;
    for (iter = begin, n = 0; iter != end; ++iter, ++n)
	arr[n] = *iter;

    return convert_to_python(arr);
}

static PyObject *
komxx_session_get_pending_footnotes(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (require_selected_conf(self) != st_ok)
	return NULL;

    return convert_to_python(self->unread_confs->query_pending_footnotes());
}

static PyObject *
komxx_session_get_pending_comments(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (require_selected_conf(self) != st_ok)
	return NULL;

    return convert_to_python(self->unread_confs->query_pending_comments());
}

static PyObject *
komxx_session_set_pending_footnotes(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    PyObject *footnotes;

    if (!PyArg_ParseTuple(args, "O!", &PyList_Type, &footnotes))
	return NULL;

    std::vector<Text_no> foot(PyList_Size(footnotes));

    for (int i = 0; i < PyList_Size(footnotes); i++)
    {
	PyObject *r = PyList_GetItem(footnotes, i);
	foot[i] = PyInt_AsLong(r);
	if (PyErr_Occurred())
	    return NULL;
    }

    if (require_selected_conf(self) != st_ok)
	return NULL;

    self->unread_confs->set_pending_footnotes(foot);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
komxx_session_set_pending_comments(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    PyObject *comments;

    if (!PyArg_ParseTuple(args, "O!", &PyList_Type, &comments))
	return NULL;

    std::vector<Text_no> comm(PyList_Size(comments));

    for (int i = 0; i < PyList_Size(comments); i++)
    {
	PyObject *r = PyList_GetItem(comments, i);
	comm[i] = PyInt_AsLong(r);
	if (PyErr_Occurred())
	    return NULL;
    }

    if (require_selected_conf(self) != st_ok)
	return NULL;

    self->unread_confs->set_pending_comments(comm);
    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
komxx_session_join_conference(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int cno;
    int prio;
    int place;
    PyObject *py_membership_type;

    if (!PyArg_ParseTuple(args, "iiiO!", &cno, &prio, &place,
			  &Komxx_membership_type_type, &py_membership_type))
	return NULL;

    komxx_membership_type_object *type(
	reinterpret_cast<komxx_membership_type_object*>(py_membership_type));

    if (require_unread_confs(self) != st_ok)
	return NULL;

    // The following code is almost verbatimly duplicated in
    // komxx_session_add_member.
    if (cno < 0 || prio < 0 || place < 0 || prio > 255 || place > 255)
    {
	PyErr_SetString(PyExc_ValueError, "Integer out of range");
	return NULL;
    }

    return convert_to_python(
	self->unread_confs->join_conference(cno, prio, place, *type->data));
}

static PyObject *
komxx_session_disjoin_conference(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int cno;

    if (!PyArg_ParseTuple(args, "i", &cno))
	return NULL;

    if (require_unread_confs(self) != st_ok)
	return NULL;

    // The following code is almost verbatimly duplicated in
    // komxx_session_sub_member.
    if (cno < 0)
    {
	PyErr_SetString(PyExc_ValueError, "Integer out of range");
	return NULL;
    }

    return convert_to_python(self->unread_confs->disjoin_conference(cno));
}

static PyObject *
komxx_session_set_last_read(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int cno;
    int last_read;

    if (!PyArg_ParseTuple(args, "ii", &cno, &last_read))
	return NULL;

    if (require_unread_confs(self) != st_ok)
	return NULL;

    return convert_to_python(
	self->unread_confs->set_last_read(cno, last_read));
}

static PyObject *
komxx_session_user_active(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    self->session->user_active();

    Py_INCREF(Py_None);
    return Py_None;
}

static PyObject *
komxx_session_get_members(PyObject *s,
			  PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int           confno = 0;
    unsigned int  first = 0;
    unsigned int  no_of_members = 0;

    if (!PyArg_ParseTuple(args, "iii", &confno, &first, &no_of_members))
	return NULL;

    // FIXME: error handling.    
    std::vector<Pers_no> members = self->session
	->get_members((Conf_no) confno, 
		      (unsigned short) first, 
		      (unsigned short) no_of_members);

    return convert_to_python(members);
}

static PyObject *
komxx_session_get_members_2(PyObject *s,
			    PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int           confno = 0;
    unsigned int  first = 0;
    unsigned int  no_of_members = 0;

    if (!PyArg_ParseTuple(args, "iii", &confno, &first, &no_of_members))
	return NULL;

    get_members_question q(self->connection, confno, first, no_of_members);
    return result_or_exception(&q);
}

static PyObject *
komxx_session_block(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int timeout;

    if (!PyArg_ParseTuple(args, "i", &timeout))
	return NULL;

    return convert_to_python(self->connection->drain(timeout));
}

static PyObject *
komxx_session_handle_async(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int asyncs = 1000;

    if (!PyArg_ParseTuple(args, "|i", &asyncs))
	return NULL;

    return convert_to_python(self->session->handle_async(asyncs));
}

static PyObject *
komxx_session_find_next_text_no(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int start;
    
    if (!PyArg_ParseTuple(args, "i", &start))
	return NULL;

    find_next_text_no_question q(self->connection, start);
    
    return result_or_exception(&q);
}

static PyObject *
komxx_session_find_previous_text_no(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int start;
    
    if (!PyArg_ParseTuple(args, "i", &start))
	return NULL;

    find_previous_text_no_question q(self->connection, start);
    
    return result_or_exception(&q);
}

static PyObject *
komxx_session_last_local_before(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int cno;
    int year;
    int mon;
    int mday;
    int hour;
    int minute;
    int sec;
    int wday;
    int yday;
    int isdst;

    if (!PyArg_ParseTuple(args, "i(iiiiiiiii)", &cno,
			  &year, &mon, &mday,
			  &hour, &minute, &sec,
			  &wday, &yday, &isdst))
	return NULL;

    // Convert from Python conventions to UNIX conventions.
    if (wday == 6)
	wday = 0;
    else
	++wday;

    year -= 1900;
    --mon;
    --yday;

    struct tm t;
    t.tm_year = year;
    t.tm_mon = mon;
    t.tm_mday = mday;
    t.tm_hour = hour;
    t.tm_min = minute;
    t.tm_sec = sec;
    t.tm_wday = wday;
    t.tm_yday = yday;
    t.tm_isdst = isdst;
    
    // Argument parsing and conversion complete.  Now do the job.
    return convert_to_python(self->session->last_local_before(cno, t));
}

static PyObject *
komxx_session_get_last_text(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    int year;
    int mon;
    int mday;
    int hour;
    int minute;
    int sec;
    int wday;
    int yday;
    int isdst;

    if (!PyArg_ParseTuple(args, "(iiiiiiiii)",
			  &year, &mon, &mday,
			  &hour, &minute, &sec,
			  &wday, &yday, &isdst))
	return NULL;

    // Convert from Python conventions to UNIX conventions.
    if (wday == 6)
	wday = 0;
    else
	++wday;

    year -= 1900;
    --mon;
    --yday;

    struct tm t;
    t.tm_year = year;
    t.tm_mon = mon;
    t.tm_mday = mday;
    t.tm_hour = hour;
    t.tm_min = minute;
    t.tm_sec = sec;
    t.tm_wday = wday;
    t.tm_yday = yday;
    t.tm_isdst = isdst;
    
    // Argument parsing and conversion complete.  Now do the job.
    return convert_to_python(self->session->get_last_text(t));
}


static PyObject *
komxx_session_get_static_session_info(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int session = 1;

    if (!PyArg_ParseTuple(args, "i", &session))
	return NULL;

    return convert_to_python(self->session->get_static_session_info(session));
}


static PyObject *
komxx_session_prefetch_static_session_info(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int session = 1;

    if (!PyArg_ParseTuple(args, "i", &session))
	return NULL;

    return convert_to_python(
	self->session->prefetch_static_session_info(session));
}


static PyObject *
komxx_session_get_client_name(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int session = 1;

    if (!PyArg_ParseTuple(args, "i", &session))
	return NULL;

    return convert_to_python(self->session->get_client_name(session));
}


static PyObject *
komxx_session_prefetch_client_name(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int session = 1;

    if (!PyArg_ParseTuple(args, "i", &session))
	return NULL;

    return convert_to_python(
	self->session->prefetch_client_name(session));
}


static PyObject *
komxx_session_get_client_version(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int session = 1;

    if (!PyArg_ParseTuple(args, "i", &session))
	return NULL;

    return convert_to_python(self->session->get_client_version(session));
}


static PyObject *
komxx_session_prefetch_client_version(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int session = 1;

    if (!PyArg_ParseTuple(args, "i", &session))
	return NULL;

    return convert_to_python(
	self->session->prefetch_client_version(session));
}


static PyObject *
komxx_session_user_area_get(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    char *area;
    int area_len;
    char *key;
    int key_len;

    if (!PyArg_ParseTuple(args, "s#s#", &area, &area_len,
			  &key, &key_len))
	return NULL;

    LyStr area_str(area, area_len);
    LyStr key_str(key, key_len);

    Kom_res<User_area*> ua = self->session->get_user_area();
    User_area *ptr;
    if (ua.error != KOM_NO_ERROR)
	return convert_to_python(ua.error);
    else
    {
	ptr = ua.data;
	Kom_res<LyStr> result = ptr->lookup(area_str, key_str);
	return convert_to_python(result);
    }
}


static PyObject *
komxx_session_user_area_set(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    char *area;
    int area_len;
    char *key;
    int key_len;
    char *val;
    int val_len;

    if (!PyArg_ParseTuple(args, "s#s#s#", &area, &area_len,
			  &key, &key_len, &val, &val_len))
	return NULL;

    Kom_res<User_area*> ua = self->session->get_user_area();
    if (ua.error != KOM_NO_ERROR)
	return convert_to_python(ua.error);
    else
	return convert_to_python(ua.data->set(LyStr(area, area_len),
					      LyStr(key, key_len),
					      LyStr(val, val_len)));
}


static PyObject *
komxx_session_user_area_store(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    Kom_res<User_area*> ua = self->session->get_user_area();
    if (ua.error != KOM_NO_ERROR)
	return convert_to_python(ua.error);
    else
	return convert_to_python(ua.data->store_in_server());
}


static PyObject *
komxx_session_modify_info(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    PyObject *aux_nos_del;
    PyObject *aux_items_add;

    if (!PyArg_ParseTuple(args, "O!O!",
			  &PyList_Type, &aux_nos_del,
			  &PyList_Type, &aux_items_add))
	return NULL;

    std::vector<Aux_no> del;
    std::vector<Aux_item> add;

    if (decode_aux_no_list(aux_nos_del, &del) != st_ok)
	return NULL;

    if (decode_aux_list(aux_items_add, &add) != st_ok)
	return NULL;

    return convert_to_python(self->session->modify_info(del, add));
}


static PyObject *
komxx_session_generated_questions(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    
    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    return convert_to_python(self->connection->generated_questions());
}

static PyObject *
komxx_session_received_asyncs(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    
    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    return convert_to_python(self->connection->received_asyncs());
}

static PyObject *
komxx_session_ignored_asyncs(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    
    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    return convert_to_python(self->connection->ignored_asyncs());
}

static PyObject *
komxx_session_received_replies(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;
    
    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    return convert_to_python(self->connection->received_replies());
}


static PyObject *
komxx_session_who_is_on_dynamic(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    int want_visible = 1;
    int want_invisible = 0;
    long active_last = 0;

    if (!PyArg_ParseTuple(args, "|iil", &want_visible, &want_invisible, 
			  &active_last))
	return NULL;

    return convert_to_python(self->session->who_is_on_dynamic(want_visible, 
							      want_invisible, 
							      active_last));
}


static PyObject *
komxx_session_unsafe_delete_text(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    long text_no = 0;

    if (!PyArg_ParseTuple(args, "l", &text_no))
	return NULL;

    delete_text_question q(self->connection, text_no);
    
    return result_or_exception(&q);
}

static PyObject *
komxx_session_unsafe_initiate_delete_text(PyObject *s, PyObject *args)
{
    komxx_session_object *self = (komxx_session_object*)s;

    long text_no = 0;

    if (!PyArg_ParseTuple(args, "l", &text_no))
	return NULL;

    komxx_question_object *rv = PyObject_NEW(
	komxx_question_object,
	&Komxx_question_type);
    if (rv == NULL)
	return NULL;

    rv->data = new delete_text_question(self->connection, text_no);
    return (PyObject*)rv;
}


// Komxx_session <-> Python glue--------------------------------

static void
komxx_session_dealloc(komxx_session_object *self)
{
    // FIXME: unregister async callbacks found in self->tags.
    delete self->unread_confs;
    delete self->session;
    delete self->connection;
    delete self->conn_io;
    delete self->tags;
    PyMem_DEL(self);
}

static PyMethodDef komxx_session_methods[] = {
    // Get prefetch-on-demand shadow objects.
    {"text",	komxx_session_text, 1, "No doc"},
    {"conf",	komxx_session_conf, 1, "No doc"},
    {"pers",	komxx_session_pers, 1, "No doc"},

    {"text_mapping", komxx_session_text_mapping, 1, "No doc"},
    {"created_mapping", komxx_session_created_mapping, 1, "No doc"},

    // State retrieval
    {"my_login", komxx_session_my_login, 1, "No doc"},
    {"my_session", komxx_session_my_session, 1, "No doc"},

    // Unread_confs handling
    {"prefetch",	komxx_session_prefetch, 1, "No doc"},
    {"goto_next_conf",	komxx_session_goto_next_conf, 1, "No doc"},
    {"query_next_conf",	komxx_session_query_next_conf, 1, "No doc"},
    {"goto_conf",	komxx_session_goto_conf, 1, "No doc"},
    {"next_prompt",	komxx_session_next_prompt, 1, "No doc"},
    {"next_text",	komxx_session_next_text, 1, "No doc"},
    {"remove_text",	komxx_session_remove_text, 1, "No doc"},
    {"enqueue_children",komxx_session_enqueue_children, 1, "No doc"},
    {"current_conf",	komxx_session_current_conf, 1, "No doc"},
    {"confs_with_unread",	komxx_session_confs_with_unread, 1, "No doc"},
    {"confs_with_estimated_unread",
     komxx_session_confs_with_estimated_unread, 1, "No doc"},
    {"min_unread_texts_in_current",
     komxx_session_min_unread_texts_in_current, 1, "No doc"},
    {"max_unread_texts_in_current",
     komxx_session_max_unread_texts_in_current, 1, "No doc"},
    {"estimated_unread_texts_in_current",
     komxx_session_estimated_unread_texts_in_current, 1, "No doc"},
    {"estimated_unread_texts_in_conf",
     komxx_session_estimated_unread_texts_in_conf, 1, "No doc"},
    {"list_unread_texts_in_current",
     komxx_session_list_unread_texts_in_current, 1, "No doc"},
    {"get_pending_footnotes",
     komxx_session_get_pending_footnotes, 1, "No doc"},
    {"get_pending_comments",
     komxx_session_get_pending_comments, 1, "No doc"},
    {"set_pending_footnotes",
     komxx_session_set_pending_footnotes, 1, "No doc"},
    {"set_pending_comments",
     komxx_session_set_pending_comments, 1, "No doc"},
    {"join_conference",	komxx_session_join_conference, 1, "No doc"},
    {"disjoin_conference", komxx_session_disjoin_conference, 1, "No doc"},
    {"set_last_read",	komxx_session_set_last_read, 1, "No doc"},

    // Handling of asynchronous messages
    {"do_accept_async", komxx_session_do_accept_async, 1, "No doc"},
    {"handle_async", komxx_session_handle_async, 1, "No doc"},
    {"register_async_text_created", komxx_session_register_async_text_created,
     1, "No doc"},
    {"unregister_async_text_created",
     komxx_session_unregister_async_text_created,
     1, "No doc"},
    {"register_async_text_message", komxx_session_register_async_text_message,
     1, "No doc"},
    {"unregister_async_text_message",
     komxx_session_unregister_async_text_message,
     1, "No doc"},
    {"query_async", komxx_session_query_async, 1, "No doc"},

    // Perform utility functions.
    {"block", komxx_session_block, 1, "No doc"},
    {"create_person", komxx_session_create_person, 1, "No doc"},
    {"set_presentation", komxx_session_set_presentation, 1, "No doc"},
    {"add_member", komxx_session_add_member, 1, "No doc"},
    {"sub_member", komxx_session_sub_member, 1, "No doc"},
    {"set_membership_type", komxx_session_set_membership_type, 1, "No doc"},
    {"send_message", komxx_session_send_message, 1, "No doc"},
    {"get_server_info", komxx_session_get_server_info, 1, "No doc"},
    {"set_server_info", komxx_session_set_server_info, 1, "No doc"},
    {"get_time", komxx_session_get_time, 1, "No doc"},
    {"change_what_i_am_doing",
     komxx_session_change_what_i_am_doing, 1, "No doc"},
    {"who_is_on", komxx_session_who_is_on, 1, "No doc"},
    {"who_is_on_dynamic", komxx_session_who_is_on_dynamic, 1, "No doc"},
    {"get_marks", komxx_session_get_marks, 1, "No doc"},
    {"create_conference", komxx_session_create_conference, 1, "No doc"},
    {"prefetch_is_member", komxx_session_prefetch_is_member, 1, "No doc"},
    {"is_member", komxx_session_is_member, 1, "No doc"},
    {"is_supervisor", komxx_session_is_supervisor, 1, "No doc"},
    {"create_text", komxx_session_create_text, 1, "No doc"},
    {"user_active", komxx_session_user_active, 1, "No doc"},
    {"get_members", komxx_session_get_members, 1, "No doc"},
    {"get_members_2", komxx_session_get_members_2, 1, "No doc"},
    {"get_collate_table", komxx_session_get_collate_table, 1, "No doc"},
    {"prefetch_collate_table", 
     komxx_session_prefetch_collate_table, 1, "No doc"},
    {"get_version_info", komxx_session_get_version_info, 1, "No doc"},
    {"prefetch_version_info", 
     komxx_session_prefetch_version_info, 1, "No doc"},
    {"query_predefined_aux_items", 
     komxx_session_query_predefined_aux_items, 1, "No doc"},
    {"prefetch_predefined_aux_items", 
     komxx_session_prefetch_predefined_aux_items, 1, "No doc"},
    {"get_static_session_info", 
     komxx_session_get_static_session_info, 1, "No doc"},
    {"prefetch_static_session_info", 
     komxx_session_prefetch_static_session_info, 1, "No doc"},
    {"get_client_name", komxx_session_get_client_name, 1, "No doc"},
    {"prefetch_client_name", 
     komxx_session_prefetch_client_name, 1, "No doc"},
    {"get_client_version", komxx_session_get_client_version, 1, "No doc"},
    {"prefetch_client_version", 
     komxx_session_prefetch_client_version, 1, "No doc"},
    {"modify_info", 
     komxx_session_modify_info, 1, "No doc"},

    // Searching.
    {"find_next_text_no", komxx_session_find_next_text_no, 1, "No doc"},
    {"find_previous_text_no", komxx_session_find_previous_text_no,1, "No doc"},
    {"last_local_before", komxx_session_last_local_before,1, "No doc"},
    {"get_last_text", komxx_session_get_last_text, 1, "No doc"},

    // Login/logout
    {"login",	komxx_session_login, 1, "No doc"},
    {"logout",	komxx_session_logout, 1, "No doc"},
    {"enable",	komxx_session_enable, 1, "No doc"},
    {"lookup_z_name", komxx_session_lookup_z_name, 1, "No doc"},
    {"re_z_lookup", komxx_session_re_z_lookup, 1, "No doc"},

    // User area
    {"user_area_get", komxx_session_user_area_get, 1, "No doc"}, 
    {"user_area_set", komxx_session_user_area_set, 1, "No doc"}, 
    {"user_area_store", komxx_session_user_area_store, 1, "No doc"},

    // Statistics
    {"generated_questions", komxx_session_generated_questions, 1, "No doc"},
    {"received_asyncs", komxx_session_received_asyncs, 1, "No doc"},
    {"ignored_asyncs", komxx_session_ignored_asyncs, 1, "No doc"},
    {"received_replies", komxx_session_received_replies, 1, "No doc"},

    // Delete objects.  Does not updated the local cache!
    {"unsafe_delete_text", komxx_session_unsafe_delete_text, 1, "No doc"},
    {"unsafe_initiate_delete_text", komxx_session_unsafe_initiate_delete_text, 1, "No doc"},

    {NULL,		NULL, 1, "end"}		/* sentinel */
};

static PyObject *
komxx_session_getattr(komxx_session_object *self,
		      char *name)
{
    return Py_FindMethod(komxx_session_methods, (PyObject *) self, name);
}

static PyTypeObject Komxx_session_type = {
	PyObject_HEAD_INIT(&PyType_Type)
	0,			/*ob_size*/
	"session",			/*tp_name*/
	sizeof(komxx_session_object),	/*tp_basicsize*/
	0,			/*tp_itemsize*/
	/* methods */
	(destructor)komxx_session_dealloc, /*tp_dealloc*/
	0,			/*tp_print*/
	(getattrfunc)komxx_session_getattr, /*tp_getattr*/
	0, /*tp_setattr*/
	0,			/*tp_compare*/
	0,			/*tp_repr*/
	0,			/*tp_as_number*/
	0,			/*tp_as_sequence*/
	0,			/*tp_as_mapping*/
	0,			/*tp_hash*/
	0, /*  tp_call */
	0, /*  tp_str */
	0, /*  tp_getattro */
	0, /*  tp_setattro */
	0, /*  tp_as_buffer */
	0, /*  tp_flags */
	"LysKOM session object.\n\nDoc not yet available.", /*  *tp_doc */
	0, /*  tp_xxx5 */
	0, /*  tp_xxx6 */
	0, /*  tp_xxx7 */
	0, /*  tp_xxx8 */
};


// Global functions----------------------------------------------------

static PyObject *
komxx_session_new(PyObject *,
		  PyObject *args)
{
    char *server = NULL;
    char *port = NULL;
    char *client_name = NULL;
    char *client_version = NULL;

    if (!PyArg_ParseTuple(args, "ssss", &server, &port,
			  &client_name, &client_version))
	return NULL;

    komxx_session_object *rv = PyObject_NEW(komxx_session_object,
					    &Komxx_session_type);
    if (rv == NULL)
	return NULL;

    rv->conn_io = new Conn_io_ISC(server, port);
    rv->connection = new Connection(rv->conn_io);
    if (rv->connection->connection_failed())
    {
	throw_exception(error_no_connection_class);
	return NULL;
    }

    rv->session = new Session(rv->connection, client_name, client_version);
    rv->unread_confs = NULL;
    rv->tags = new komxx_session_tags;
    return (PyObject *)rv;
}

static PyObject *
komxx_priv_bits(PyObject *,
		PyObject *args)
{
    int wheel = false;
    int admin = false;
    int statistic = false;
    int create_pers = false;
    int create_conf = false;
    int change_name = false;
    int flg7 = false;
    int flg8 = false;
    int flg9 = false;
    int flg10 = false;
    int flg11 = false;
    int flg12 = false;
    int flg13 = false;
    int flg14 = false;
    int flg15 = false;
    int flg16 = false;

    if (!PyArg_ParseTuple(args, "|iiiiiiiiiiiiiiii", 
			  &wheel,
			  &admin,
			  &statistic,
			  &create_pers,
			  &create_conf,
			  &change_name,
			  &flg7,
			  &flg8,
			  &flg9,
			  &flg10,
			  &flg11,
			  &flg12,
			  &flg13,
			  &flg14,
			  &flg15,
			  &flg16))
	return NULL;

    komxx_priv_bits_object *rv = PyObject_NEW(komxx_priv_bits_object,
					      &Komxx_priv_bits_type);
    if (rv == NULL)
	return NULL;

    rv->data = new Priv_bits(wheel,
			     admin,
			     statistic,
			     create_pers,
			     create_conf,
			     change_name,
			     flg7,
			     flg8,
			     flg9,
			     flg10,
			     flg11,
			     flg12,
			     flg13,
			     flg14,
			     flg15,
			     flg16);
    return (PyObject *)rv;
}

static PyObject *
komxx_membership_type(PyObject *,
		      PyObject *args)
{
    int invitation = false;
    int passive = false;
    int secret = false;
    int passive_message_invert = false;
    int reserved2 = false;
    int reserved3 = false;
    int reserved4 = false;
    int reserved5 = false;

    if (!PyArg_ParseTuple(args, "|iiiiiiii", 
			  &invitation,
			  &passive,
			  &secret,
			  &passive_message_invert,
			  &reserved2,
			  &reserved3,
			  &reserved4,
			  &reserved5))
	return NULL;

    komxx_membership_type_object *rv = PyObject_NEW(
	komxx_membership_type_object,
	&Komxx_membership_type_type);
    if (rv == NULL)
	return NULL;

    rv->data = new Membership_type(invitation,
				   passive,
				   secret,
				   passive_message_invert,
				   reserved2,
				   reserved3,
				   reserved4,
				   reserved5);
    return (PyObject *)rv;
}

static PyObject *
komxx_aux_item(PyObject *,
	       PyObject *args)
{
    int tag;
    char *data;
    int data_len;
    int inherit_limit = 0;
    int inherit = 0;
    int secret = 0;
    int hide_creator = 0;
    int dont_garb = 0;
    int reserved2 = 0;
    int reserved3 = 0;
    int reserved4 = 0;

    if (!PyArg_ParseTuple(args, "is#|iiiiiiii", 
			  &tag,
			  &data, &data_len,
			  &inherit_limit,
			  &inherit,
			  &secret,
			  &hide_creator,
			  &dont_garb,
			  &reserved2,
			  &reserved3,
			  &reserved4))
	return NULL;

    komxx_aux_item_object *rv = PyObject_NEW(komxx_aux_item_object,
					     &Komxx_aux_item_type);
    if (rv == NULL)
	return NULL;

    rv->data = new Aux_item(tag,
			    Aux_item_flags(false, inherit, secret, 
					   hide_creator, dont_garb,
					   reserved2, reserved3, reserved4),
			    inherit_limit,
			    LyStr(data, data_len));
    return (PyObject *)rv;
}

// Global function glue-------------------------------------------------
static PyMethodDef komxx_functions[] = {
	{"new_session",		komxx_session_new, 1, "No doc available"},
	{"priv_bits",		komxx_priv_bits, 1, "No doc available"},
	{"membership_type",     komxx_membership_type, 1, "No doc available"},
	{"aux_item",		komxx_aux_item, 1, "No doc available"},
	{NULL,		NULL, 1, "End"}		/* sentinel */
};

void
initkomxx()
{
    PyObject *m = Py_InitModule("komxx", komxx_functions);
    PyObject *d = PyModule_GetDict(m);
    init_errors(d);

    // st_ok, st_pending and st_error.
    komxx_st_pending = PyString_FromString("komxx.st_pending");
    PyDict_SetItemString(d, "st_pending", komxx_st_pending);
    komxx_st_ok = PyString_FromString("komxx.st_ok");
    PyDict_SetItemString(d, "st_ok", komxx_st_ok);
    komxx_st_error = PyString_FromString("komxx.st_error");
    PyDict_SetItemString(d, "st_error", komxx_st_error);

    // The different prompts
    prompt_NEXT_CONF = PyInt_FromLong(NEXT_CONF);
    PyDict_SetItemString(d, "NEXT_CONF", prompt_NEXT_CONF);
    prompt_NEXT_TEXT = PyInt_FromLong(NEXT_TEXT);
    PyDict_SetItemString(d, "NEXT_TEXT", prompt_NEXT_TEXT);
    prompt_NEXT_COMMENT = PyInt_FromLong(NEXT_COMMENT);
    PyDict_SetItemString(d, "NEXT_COMMENT", prompt_NEXT_COMMENT);
    prompt_NEXT_FOOTNOTE = PyInt_FromLong(NEXT_FOOTNOTE);
    PyDict_SetItemString(d, "NEXT_FOOTNOTE", prompt_NEXT_FOOTNOTE);
    prompt_NO_MORE_TEXT = PyInt_FromLong(NO_MORE_TEXT);
    PyDict_SetItemString(d, "NO_MORE_TEXT", prompt_NO_MORE_TEXT);
    prompt_TIMEOUT_PROMPT = PyInt_FromLong(TIMEOUT_PROMPT);
    PyDict_SetItemString(d, "TIMEOUT_PROMPT", prompt_TIMEOUT_PROMPT);
    prompt_PROMPT_ERROR = PyInt_FromLong(PROMPT_ERROR);
    PyDict_SetItemString(d, "PROMPT_ERROR", prompt_PROMPT_ERROR);

    // Check for errors.
    if (PyErr_Occurred())
	Py_FatalError("can't initialize module komxx");
}

ENDEXTERNC

template<class C>
PyObject *
convert_aux_items(const C &data)
{
    PyObject *res = PyList_New(data.num_aux());
    for (int i = 0; i < data.num_aux(); i++)
    {
	komxx_aux_item_object *r;
	r = PyObject_NEW(komxx_aux_item_object, &Komxx_aux_item_type);
	r->data = new Aux_item(data.aux(i));
	if (PyList_SetItem(res, i, (PyObject *)r))
	{
	    Py_DECREF(res);
	    return NULL;
	}
    }
    
    return res;
}

static PyObject *
convert_to_python(const Conf_type &t)
{
    komxx_conf_type_object *r;
    r = PyObject_NEW(komxx_conf_type_object, &Komxx_conf_type_type);
    r->data = new Conf_type(t);
    return (PyObject*)r;
}

static PyObject *
convert_to_python(const Priv_bits &pb)
{
    komxx_priv_bits_object *r;
    r = PyObject_NEW(komxx_priv_bits_object, &Komxx_priv_bits_type);
    r->data = new Priv_bits(pb);
    return (PyObject*)r;
}

static PyObject *
convert_to_python(const Membership_type &mt)
{
    komxx_membership_type_object *r;
    r = PyObject_NEW(komxx_membership_type_object, 
		     &Komxx_membership_type_type);
    r->data = new Membership_type(mt);
    return (PyObject*)r;
}

static PyObject *
convert_to_python(const Session_flags &sf)
{
    komxx_session_flags_object *r;
    r = PyObject_NEW(komxx_session_flags_object, &Komxx_session_flags_type);
    r->data = new Session_flags(sf);
    return (PyObject*)r;
}

BEGINEXTERNC

static PyObject *
komxx_text_aux_items(PyObject *s,
		     PyObject *args)
{
    komxx_text_object *self = (komxx_text_object*)s;

    if (!PyArg_ParseTuple(args, ""))
	return NULL;

    if (self->data->kom_errno() != 0)
    {
	set_errno_error(self->data->kom_errno());
	return NULL;
    }

    return convert_aux_items(*self->data);
}

ENDEXTERNC
