# -*- coding: iso-8859-1 -*-

import os
import sys
import string

import komxx

class tty_io(object):
    def prompt(self, prompt):
        return raw_input(prompt)

    def msg(self, msg):
        print msg

    def echo_off(self):
	os.system("stty -echo")

    def echo_on(self):
	print
	os.system("stty echo")

    def getpassword(self, prompt):
	self.echo_off()
	try:
	    passwd = self.prompt(prompt)
	finally:
	    self.echo_on()
	return passwd

class login(object):
    def nomatch(self):
        self.msg("Det finns ingen som heter s�.")
        return 0

    def get_unique(self, want_pers, want_conf, prompt):
        """Get a conference number.

        WANT_PERS: 1 if persons are acceptable, 0 if not.
        WANT_CONF: 1 if conferences are acceptable, 0 if not.
        PROMPT: prompt to print.

        Returns:
          None if the user entered the empty string.
          A string if the user entered something that matches nothing.
          A conference number otherwise.
        """

        name = self.prompt(prompt)
        if name == "":
            return None
        matches = self.session.lookup_z_name(name, want_pers, want_conf)
        if len(matches) == 0:
            return name
        elif len(matches)== 1:
            self.msg(matches[0].name())
            return matches[0].conf_no()
        else:
            self.msg("Du kan mena n�gon av dessa:")
            nr = 1
            for match in matches:
                self.msg(" %3d %s" % (nr, matches[nr-1].name()))
                nr = nr + 1
            number = self.prompt("Ange nummer f�r �nskad person: ")
            return matches[string.atoi(number)-1].conf_no()

    def login(self):
	while 1:
            pno = self.get_unique(1, 0, "Vad heter du? ")
            if pno == None:
                self.msg("Hejd�.")
                sys.exit(0)
            elif type(pno) == type(""):
                if self.no_match(pno):
                    return
            else:
                if self.trylogin(pno):
                    return

        

    def trylogin(self, cno):
        password = self.getpassword("L�senord: ")
        if password == "":
            self.msg("Inget l�sen angivet")
            return 0
        elif self.session.login(cno, password) == komxx.st_ok:
            self.msg("V�lkommen!")
            return 1
        else:
            self.msg("Felaktigt l�senord")
            return 0

    def maybe_enable(self):
        ans = None
        while ans not in ["j", "n"]:
            ans = self.prompt("Vill du anv�nda dina privbittar? (j/n) ")
        if ans == "j":
            self.session.enable(255)
