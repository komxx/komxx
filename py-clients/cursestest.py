import sys
import curses
import select

def foo():
    w = curses.initscr()
    w.move(10,5)
    w.addstr("hello, world")
    l = 5
    for x in [("A_NORMAL", curses.A_NORMAL),
	      ("A_STANDOUT", curses.A_STANDOUT),
	      ("A_UNDERLINE", curses.A_UNDERLINE),
	      ("A_REVERSE", curses.A_REVERSE),
	      ("A_BLINK", curses.A_BLINK),
	      ("A_DIM", curses.A_DIM),
	      ("A_BOLD", curses.A_BOLD),
	      ("A_ALTCHARSET", curses.A_ALTCHARSET)]:
	w.addstr(l, 50, x[0], x[1])
	l = l + 1

    w.keypad(1)
    curses.cbreak()
    curses.noecho()
    curses.nonl()
    curses.intrflush(0)
    w.nodelay(1)

    clock = "|/-\\"
    i = 0
    while 1:
	w.move(0, 30)
	w.refresh()
	select.select([sys.stdin.fileno()], [], [], 2)
	key = w.getch()

	if key == 113:
	    break
	elif key == 114:
	    curses.endwin()
	    w.addstr(12, 12, "New size: (%d, %d), (%d, %d)" % (r, k, rm, km))
	    w.refresh()
	elif key == -1:
	    w.addstr(0, 35, clock[i])
	    i = (i + 1) % 4
	else:
	    w.addstr(1, 0, str(key))
	    w.clrtoeol()
	    (r, k) = w.getbegyx()
	    (rm, km) = w.getmaxyx()
	    w.addstr(12, 12, "(%d, %d), (%d, %d)" % (r, k, rm, km))
	    w.clrtoeol()

try:
    foo()
finally:
    curses.endwin()
