#! @PYKOM@
# -*- coding: latin-1 -*-
# Find the word of the day for a certain conference and day.
# Words already seen are stored in komwords.<confno>.db; they are not
# considered.

import re
import os
import sys
import time
import string
import random
import cPickle

sys.path.insert(1, '@PYKOMLIB@')

import komxx
import kompolicy

def fmt_time(t):
    return time.strftime("%Y-%m-%d %H:%M:%S", t)

def print_list(lst):
    prev = None
    for txt in lst:
        if txt == None:
            print
        else:
            now = time.mktime(txt.creation_time())
            if prev:
                print "%12d s" % (now - prev)
            prev = now
            pretty = fmt_time(txt.creation_time())
            print "%10d %s" % (txt.text_no(), pretty)

class komwordofday(object):
    def __init__(self, host, port):
	(pno, passwd) = self.read_password()
        self.session = komxx.new_session(host, port, "komwordofday.py",
                                         "$Revision: 1.2 $")
        if self.session.login(pno, passwd, 1) != komxx.st_ok:
            sys.stderr.write("Login failed\n")
            sys.exit(1)
        while self.session.goto_next_conf() != 0:
            if self.session.next_prompt() in [komxx.NO_MORE_TEXT,
                                              komxx.TIMEOUT_PROMPT,
                                              komxx.PROMPT_ERROR]:
                sys.exit(0)
            print "Entering process_one_day"
            self.process_one_day()

    def process_one_day(self):
        t0 = None
        processed_texts = []
        cc = self.session.current_conf()
        print "Loading stopwords...",
        sys.stdout.flush()
        stop = load_stopwords(cc)
        print "Done."
        words = {}
        while 1:
            tno = self.session.next_text()
            if tno == 0:
                print
                return # Don't process this chunk yet; we want a complete day!
            t = self.session.text(tno)
            t.prefetch_text()
            self.session.prefetch()
            t1 = t.entry_time(cc, t.local_no_recipient(cc))
            if t0 == None:
                t0 = tc = t1
            elif not sameday(tc, t1):
                print
                winner = self.find_winner(words, stop)
                if winner != None:
                    break
                tc = t1
            self.session.remove_text(tno)
            sys.stdout.write("%d %s %-50.50s\r" % (
                tno, fmt_time(t1), t.subject()))
            sys.stdout.flush()
            add_words(words, t.body(), tno)
            processed_texts.append(tno)
        print

        self.create_winner(t0, tc, winner, words[winner][0], words[winner][1],
                           processed_texts[-1])
        print "Saving stopwords...",
        sys.stdout.flush()
        save_stopwords(cc, stop)
        print "Done."

        self.mark_as_read(processed_texts)

        print "Press Enter for next day"
        sys.stdin.readline()

    def find_winner(self, words, stop):
        candidates = []
        for w in words.keys():
            if not stop.has_key(w):
                candidates.append((len(words[w][0]), len(words[w][1]), w))
        if len(candidates) == 0:
            print "No candidates!"
            return None
        print len(candidates), "unique new candidates."

        # We sort on:
        #  0: number of texts that contain the word
        #  1: total number of occurences of the word
        #  2: the word itself
        candidates.sort()
        candidates.reverse()

        global all_words
        all_words = ["", "Nedan f�ljer listan �ver *alla* nya ord.", ""]
        for (all_texts, all_occurences, all_word) in candidates:
            all_words.append("* %3d %3d %s" % (
                all_texts, all_occurences, all_word))

        # If there is a tie, consider all the winners.
        nr = 0
        mtexts = candidates[0][0]
        mtotal = candidates[0][1]
        while (nr < len(candidates) and
               candidates[nr][0] == mtexts and candidates[nr][1] == mtotal):
            nr = nr + 1
        candidates = candidates[:nr]

        print "CANDIDATES"
        print candidates

        # Less than 5 hits are boring.
        if candidates[0][0] < 5:
            print "Too few occurences."
            return None

        # We want a single winner.
        winner = random.choice(candidates)

        print "Periodens ord �r \"%s\" med %d f�rekomster i %d texter." % (
            winner[2], winner[1], winner[0])

        for w in words.keys():
            stop[w] = None

        return winner[2]


    def read_password(self):
        f = open(".komwordrc", "r")
        nr = string.atoi(f.readline())
        pwd = string.strip(f.readline())
        f.close()
        return (nr, pwd)

    def create_winner(self, t0, t1, word, texts, positions, commentee):
        if t0 == t1:
            s = "Dagens ord f�r %04d-%02d-%02d" % (t0[0], t0[1], t0[2])
        else:
            s = "Periodens ord f�r %04d-%02d-%02d--%04d-%02d-%02d" % (
                t0[0], t0[1], t0[2], t1[0], t1[1], t1[2])
        s = s + " �r \"%s\" med %d f�rekomster i %d inl�gg." % (
            word, len(positions), len(texts))

        if len(s) < 70:
            n = [s, ""]
        else:
            pos = string.rfind(s, " ", 0, 70)
            n = [s[:pos], s[pos+1:], ""]

        lines = []
        tnos = texts.keys()
        tnos.sort()
        for tno in tnos:
            self.append_occurences(word, lines, tno)

        lines2 = []
        lineno = 0
        for lin in lines:
            lines2.append((lineno, lin))
            lineno = lineno + 1

        # More than 10 hits is boring.
        random.shuffle(lines2)
        lines2 = lines2[:10]

        lines.sort(line_comparator)

        lines2.sort()
        lines = []
        for lin in lines2:
            lines.append(lin[1].rstrip())
        n = n + lines
        # n = n + all_words

        print
        print string.join(n, "\n")
        print
        raw_input("confirm")
        new = self.session.create_text(self.session.text(commentee).subject(),
                                       string.join(n, "\n"),
                                       [self.session.current_conf()], [],
                                       [tno], [])
        print "Created", new
        self.session.text(new).mark_as_read()

    def append_occurences(self, word, lines, tno):
        t = self.session.text(tno)
        raw = string.replace(t.body(), "\n", " ")
        raw = string.replace(raw, "\t", " ")
        for token in lexer(t.body(), True):
            if token.is_word(word):
                pos = token.startpos()
                lines.append("* %9d:  %27.27s%-36.36s" % (
                    tno, raw[max(0, pos-27):pos], raw[pos:]))

    def mark_as_read(self, textlist):
        for tno in textlist:
            self.session.text(tno).mark_as_read()

def line_comparator(a, b):
    a = lowerstring(a)
    b = lowerstring(b)
    if a[41:] > b[41:]:
        return 1
    if a[41:] < b[41:]:
        return -1
    if a > b:
        return 1
    if a < b:
        return -1
    return 0

def load_stopwords(cno):
    try:
        f = open(".komword-%d" % cno, "r")
    except IOError:
        return {}
    res = cPickle.load(f)
    f.close()
    return res

def save_stopwords(cno, words):
    fn = ".komword-%d" % cno
    tmp = fn + ".tmp"
    f = open(tmp, "w")
    res = cPickle.dump(words, f)
    f.close()
    os.rename(tmp, fn)


def sameday(t0, t1):
    return t0[0] == t1[0] and t0[1] == t1[1] and t0[2] == t1[2]

def add_words(words, body, tno):
    wordno = 0
    for w in words_in_body(body):
        if not words.has_key(w):
            words[w] = ({}, [])
        words[w][0][tno] = None
        words[w][1].append((tno, wordno))
        wordno = wordno + 1

class Terminal(object):
    __value = None
    __startpos = None

    def __init__(self, preserve_case, startpos):
        if preserve_case:
            self.__value = ""
            self.__startpos = startpos
        self.__downcase = ""

    def add(self, c, lower_c):
        if self.__value:
            self.__value += c
        self.__downcase += lower_c

    def orig_value(self):
        assert self.__value is not None
        return self.__value

    def downcase_value(self):
        return self.__downcase

    def is_word(self, word):
        return False

    def startpos(self):
        return self.__startpos

class Word(Terminal):
    def accept(self, c):
        if c >= 'a' and c <= 'z':
            return True
        if c == '�' or c == '�' or c == '�':
            return True
        return False

    def is_word(self, lower_word):
        return self.downcase_value() == lower_word

class Space(Terminal):
    def accept(self, c):
        return c == ' ' or c == '\t' or c == '\n'

class Begin(Terminal):
    def accept(self, c):
        return c == "(" or c == "-"

class End(Terminal):
    def accept(self, c):
        return c in ").!?"

class Junk(Terminal):
    def accept(self, c):
        return True

def words_in_body(s):
    return Parser(lexer(s, False)).words

def lexer(s, preserve_case):
    lo = lowerstring(s)
    current = None
    tokens = []
    for ix, c in enumerate(s):
        for cls in [Word, Space, Begin, End, Junk]:
            if current and isinstance(current, cls):
                if current.accept(c):
                    current.add(c, lo[ix])
                    break
            else:
                next = cls(preserve_case, ix)
                if next.accept(c):
                    next.add(c, lo[ix])
                    current = next
                    tokens.append(current)
                    break
    return tokens

class Parser(object):
    def __init__(self, tokens):
        self.words = []
        self.tokens = [None] + tokens + [None]
        self.parse()

    PATTERNS = [
        ([(None, Space), Word], [Space]),
        ([(None, Space), Word, None], []),
        ([(None, Space), Word, End], [Space]),
        ([(None, Space), Word, End, None], []),
        ([(None, Space), Begin, Word], [Space]),
        ([(None, Space), Begin, Word, None], []),
        ([(None, Space), Begin, Word, End], [Space]),
        ([(None, Space), Begin, Word, End, None], []),
        ]

    def parse(self):
        while len(self.tokens):
            for pattern, lookahead in self.PATTERNS:
                if self.match(pattern, lookahead):
                    break
            else:
                while len(self.tokens) and not isinstance(self.tokens[0], Word):
                    self.tokens = self.tokens[1:]
                if len(self.tokens):
                    self.tokens = self.tokens[1:]

    def match(self, pattern, lookahead):
        if len(self.tokens) < len(pattern) + len(lookahead):
            return False
        w = []
        for pos, clstuple in enumerate(pattern + lookahead):
            token = self.tokens[pos]
            if not self.matchpattern(token, clstuple):
                return False
            if isinstance(token, Word):
                w.append(token.downcase_value())

        self.words.extend(w)
        self.tokens = self.tokens[len(pattern):]
        return True

    def matchpattern(self, token, clstuple):
        if not isinstance(clstuple, tuple):
            clstuple = (clstuple, )
        for cls in clstuple:
            if cls == None and token == None:
                return True
            if cls != None and token != None and isinstance(token, cls):
                return True
        return False

def lowerstring(w):
    w = string.lower(w)
    w = string.replace(w, "�", "�")
    w = string.replace(w, "�", "�")
    w = string.replace(w, "�", "�")
    w = string.replace(w, "�", "�")
    w = string.replace(w, "�", "�")
    return w

if __name__ == "__main__":
    komwordofday("kom.lysator.liu.se", "4894")
