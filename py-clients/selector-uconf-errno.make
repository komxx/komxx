selector-uconf-errno.incl: selector-uconf-errno.list \
		selector-uconf-errno.template \
		selector-uconf-errno.make
	$(RM) selector-uconf-errno.incl selector-uconf-errno.incl.tmp
	cat $(srcdir)/selector-uconf-errno.list \
	| grep -v '^#' \
	| while read class method; \
	do cat $(srcdir)/selector-uconf-errno.template \
		| sed -e s/CLASS/$$class/g -e s/METHOD/$$method/g \
		>> selector-uconf-errno.incl.tmp \
		|| exit 1; \
	done
	mv selector-uconf-errno.incl.tmp selector-uconf-errno.incl
