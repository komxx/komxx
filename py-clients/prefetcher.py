import komxx

import UserList

class prefetcher_base(UserList.UserList):
    def __init__(self, session, list):
        UserList.UserList.__init__(self, list)
        self._session = session
        self[0]

    def __getitem__(self, i):
        for p in range(i, min(i+10, len(self.data))):
            self._prefetch(self.data[p])
        return UserList.UserList.__getitem__(self, i)

class prefetch_confno_conf(prefetcher_base):
    def _prefetch(self, item):
        self._session.conf(item.conf_no()).prefetch()

class prefetch_confno_pers(prefetcher_base):
    def _prefetch(self, item):
        self._session.pers(item.conf_no()).prefetch()

class prefetch_confno_presentation(prefetcher_base):
    def _prefetch(self, item):
        c = self._session.conf(item.conf_no())
        if c.prefetch() == komxx.st_ok and c.presentation() != 0:
            self._session.text(c.presentation()).prefetch()
