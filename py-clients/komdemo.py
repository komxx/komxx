#! @PYKOM@
# A simple demo client for komxxmodule

import string
import sys
import os
import time

sys.path.insert(1, '@PYKOMLIB@')

import komxx

def format_time(time):
    return "%04d-%02d-%02d %02d:%02d:%02d" % time[0:6]

class demo_kom(object):
    def __init__(self, host, port):
	self.session = komxx.new_session(host, port, "komdemo.py",
					 "$Revision: 1.8 $")
	self.login()
	self.toploop()

    def login(self):
	while 1:
	    name = raw_input("Vad heter du? ")
	    if name == "":
		print "Jag talar inte med folk som inte ber�ttar vad de heter."
		sys.exit(0)
	    matches = self.session.lookup_z_name(name, 1, 0)
	    if len(matches) == 0:
		print "Det finns ingen som heter s�."
	    elif len(matches)== 1:
		print matches[0].name()
		cno = matches[0].conf_no()
		password = self.getpassword()
		if password == "":
		    print "Inget l�sen angivet"
		elif self.session.login(cno, password) == komxx.st_ok:
		    print "V�lkommen!"
		    return
		else:
		    print "Felaktigt l�senord"
	    else:
		print "Du kan mena n�gon av dessa:"
		nr = 1
		for match in matches:
		    print " %3d" % nr, matches[nr-1].name()
		    nr = nr + 1
		number = raw_input("Ange nummer f�r �nskad person: ")
		cno = matches[eval(number)-1].conf_no()
		password = self.getpassword()
		if password == "":
		    print "Inget l�sen angivet"
		elif self.session.login(cno, password) == komxx.st_ok:
		    print "V�lkommen!"
		    return
		else:
		    print "Felaktigt l�senord"
   
    def echo_off(self):
	os.system("stty -echo")

    def echo_on(self):
	print
	os.system("stty echo")

    def getpassword(self):
	self.echo_off()
	try:
	    passwd = raw_input("L�senord: ")
	finally:
	    self.echo_on()
	return passwd

    def toploop(self):
	print "Du �r inne.  Du kan ha ol�sta i f�ljande m�ten:"
	while self.session.prefetch(0, 0, 0, 0, 20, 100, 20) == komxx.st_pending:
	    self.session.block(100)
	for m in self.session.confs_with_unread():
	    self.session.conf(m[0]).prefetch_uconf()
	for m in self.session.confs_with_unread():
	    if m[1] == m[2]:
		i=`m[1]`
	    else:
		i=`m[1]`+'-'+`m[2]`
	    print " %9s" % i, self.session.conf(m[0]).name()
	print "Tyv�rr kan du inte anv�nda den h�r klienten f�r att l�sa dem."
	raw_input("[TRYCK RETURN]")

	try:
	    t = self.session.text(4711)
	    print "Text 4711 ser ut s� h�r:", t.subject()
	    print t.body()
	except komxx.err_no_such_text:
	    print "Text 4711 finns visst inte."
	raw_input("[TRYCK RETURN]")

	self.list_times()
	raw_input("[TRYCK RETURN]")

	self.list_first(50)
	sys.exit(0)

    def list_times(self):
	t = 0
	x = -1
	prev_time = None
	while 1:
	    text = self.session.text(t)
	    if text.exists():
		x = t
	    else:
		try:
		    l = self.session.find_previous_text_no(t)
		except komxx.err_no_such_text:
		    l = None
		try:
		    m = self.session.find_next_text_no(t)
		except komxx.err_no_such_text:
		    m = None
		if l == x:
		    break
		if l == None:
		    x = m
		elif m == None:
		    x = l
		elif t - l < m - t:
		    x = l
		else:
		    x = m
		if x == None:
		    break
		text = self.session.text(x)
	    print format_time(text.creation_time()),
	    print "%8d" % x,
	    now = time.mktime(text.creation_time())
	    if prev_time != None:
		print (now - prev_time) / (3600 * 24), "dagar"
	    else:
		print
	    prev_time = now
	    t = t + 100000

    def list_first(self, n):
	tn = 0
	try:
	    for i in range(n):
		tn = self.session.find_next_text_no(tn)
		text = self.session.text(tn)
		now = text.creation_time()
		prev = time.localtime(time.mktime(now) - 1)
		next = time.localtime(time.mktime(now) + 1)
		print format_time(prev), self.session.get_last_text(prev)
		print format_time(now), self.session.get_last_text(now)
		print format_time(next), self.session.get_last_text(next)
		print
	except komxx.err_no_such_text:
	    pass


if __name__ == "__main__":
    if len(sys.argv) > 1:
	host = sys.argv[1]
    else:
	host = "kom.lysator.liu.se"
    if len(sys.argv) > 2:
	port = sys.argv[2]
    else:
	port = "4894"
    s=demo_kom(host, port)
