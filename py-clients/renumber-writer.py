#! @PYKOM@
# Move the permission to write in certain conferences from one person
# to another.  This script was used when "Brevb�raren (som �r
# implementerad i) Python" took over the responsibilities of
# "Postmaster".  Don't use this script casually.

import os
import sys

import lyspython.lyslog
from lyspython.lyslog import log

sys.path.insert(1, '@PYKOMLIB@')

import komxx
import kompolicy
import prefetcher

def set_el():
    global el

    p = os.popen("tput el", "r")
    el = p.read()
    p.close()


set_el()

class renumber(kompolicy.tty_io, kompolicy.login):
    def __init__(self, host, port):
        print "Connecting..."
        self.session = komxx.new_session(host, port, "renumber-writer.py",
                                         "$Revision: 1.1 $")
        print "Connected."
        self.login()
        self.maybe_enable()
        old = None
        while type(old) != type(1):
            old = self.get_unique(1, 1, "Gamla skrivaren: ")
        new = None
        while type(new) != type(1):
            new = self.get_unique(1, 1, "Nya skrivaren: ")
        self.msg("%d -> %d" % (old, new))
        targets = self.session.lookup_z_name("", 1, 1)
        targets = prefetcher.prefetch_confno_conf(self.session, targets)
        for target in targets:
            self.convert(target, old, new)
        sys.stderr.write(el)

    def convert(self, target, old, new):
        sys.stderr.write("%sConsidering %d %s\r" % (
            el, target.conf_no(), target.name()))
        t = self.session.conf(target.conf_no())
        submitters = t.permitted_submitters()
        if submitters == 0:
            return
        if submitters == old:
            sys.stderr.write("\n")
            log("Change permitted submitters for"
                " %d (%s) from %d (%s) to %d (%s)" % (
                t.conf_no(), t.name(),
                old, self.session.conf(old).name(),
                new, self.session.conf(new).name()))
            t.set_permitted_submitters(new)
        elif self.session.is_member(old, submitters) \
             and not self.session.is_member(new, submitters):
            sys.stderr.write("\n")
            log("Adding %d (%s) to %d (%s) because of %d (%s)" % (
                new, self.session.conf(new).name(),
                submitters, self.session.conf(submitters).name(),
                t.conf_no(), t.name()))
            self.session.add_member(submitters, new, 1, 1, 
				    komxx.membership_type())

    def maybe_enable(self):
        ans = None
        while ans not in ["j", "n"]:
            ans = self.prompt("Vill du anv�nda dina privbittar? (j/n) ")
        if ans == "j":
            self.session.enable(255)
        
if __name__ == "__main__":
    lyspython.lyslog.openlog("renumber-writer.log")
    renumber("kom.lysator.liu.se", "4894")
