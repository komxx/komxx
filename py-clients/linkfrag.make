pykom: Modules/config.c Makefile
	$(LINKCC) $(LDFLAGS) $(LINKFORSHARED) -o $@ \
	    $(PYTHON_INCLUDES) \
	    $(PYTHON_CONFIGDIR)/$(MAINOBJ) Modules/config.c \
	    $(PYTHON_CONFIGDIR)/$(LIBRARY) \
	$(LIBS) $(MODLIBS) $(SYSLIBS) $(LDLAST)
