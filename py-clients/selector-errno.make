selector-errno.incl: selector-errno.list selector-errno.template \
		selector-errno.make
	$(RM) selector-errno.incl selector-errno.incl.tmp
	cat $(srcdir)/selector-errno.list \
	| grep -v '^#' \
	| while read class method; \
	do cat $(srcdir)/selector-errno.template \
		| sed -e s/CLASS/$$class/g -e s/METHOD/$$method/g \
		>> selector-errno.incl.tmp \
		|| exit 1; \
	done
	mv selector-errno.incl.tmp selector-errno.incl
