#! @PYKOM@
# Print all conference numbers that are visible.
# Todo: exclude those with an aux-item that prohibits all import.

import os
import sys
import time
import string

sys.path.insert(1, '@PYKOMLIB@')

import komxx

def visible_confs(host, port, userid, passwd_file):
    pwd = open(passwd_file).read().strip()
    session = komxx.new_session(host, port, "visible_confs.py", "1.0")
    if session.login(userid, pwd, 1) != komxx.st_ok:
        sys.stderr.write("login failed.\n")
        sys.exit(1)
    for conf_z_info in session.lookup_z_name(''):
        print conf_z_info.conf_no()

def main(argv):
    server = "kom.lysator.liu.se"
    port = "4894"
    userid = 7871
    pwd_file = "/home/kent/letterman.kompwd"
    
    if len(argv) == 1:
        pwd_file = argv[0]
    elif len(argv) == 2:
        userid = int(argv[0])
        pwd_file = argv[1]
    elif len(argv) == 4:
        userid = int(argv[0])
        pwd_file = argv[1]
        server = argv[2]
        port = argv[3]
    elif len(argv) != 0:
        sys.stderr.write("usage: visible_confs "
                         "[ [ userid ] passwordfile [ server port ]\n")
        sys.stderr.write("userid must be given if server is given\n")
        sys.exit(1)

    visible_confs(server, port, userid, pwd_file)


if __name__ == "__main__":
    main(sys.argv[1:])
