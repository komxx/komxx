# Makefile template for py-clients testsuite
# Copyright (C) 1997  Per Cederqvist
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

# Process this file with python to generate Makefile.in.

from pmg import *

m = komxxmakefile()

m.mostlyclean('*.log')
m.mostlyclean('*.sum')
m.emit()

print """
check: runtest

runtest: runtest.sh
	sed s+\@srcdir\@+$(srcdir)+ $(srcdir)/runtest.sh > runtest.tmp
	chmod +x runtest.tmp
	mv -f runtest.tmp runtest

.gdbinit:
	$(RM) $@ $@.tmp
	echo 'dir $(top_srcdir)/py-clients' >> $@.tmp
	echo 'dir ../../genclasses' >> $@.tmp
	echo 'dir $(top_srcdir)/libconnection' >> $@.tmp
	echo 'dir ../../libkom++' >> $@.tmp
	echo 'dir $(top_srcdir)/libkom++' >> $@.tmp
	echo 'dir $(top_srcdir)/libisc/src' >> $@.tmp
	echo 'dir $(top_srcdir)/libcompat' >> $@.tmp
	mv $@.tmp $@

"""
