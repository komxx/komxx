#!/bin/sh

srcdir=@srcdir@

if [ $# != 1 ]
then
    echo $0: usage: $0 testcase >&2
    exit 1
fi

case "$1" in
    *.py)
	PYTHONPATH=$srcdir/..
	export PYTHONPATH
	interpreter=../../python/python ;;
    *.sh)
	interpreter=sh ;;
    *)
	echo $0: dont know what to do with $1 >&2
	exit 1;;
esac

b=`basename $1`
rm -f $b.out
$interpreter $srcdir/$b > $b.out
e=$?
if [ $e != 0 ]
then
    echo $0: $interpreter gave exit status $e >&2
    exit 1
fi

if [ -f $srcdir/$b.ok ]
then
    if diff -u $srcdir/$b.ok $b.out
    then
	exit 0
    else
	exit 1
    fi
else
    mv $b.out $srcdir/$b.ok
    echo $0: created $srcdir/$b.ok: check it manually
    exit 2
fi
