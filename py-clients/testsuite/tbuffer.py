import curmgr
b = curmgr.buffer()

testdata = [
    ["insert", "",
     [0,0], None, ""],
    ["insert", "asdf",
     [0,4], None, "asdf"],
    ["insert", "",
     [0,4], None, "asdf"],
    ["goto-char", [0,0],
     [0,0], None, "asdf"],
    ["insert", "q",
     [0,1], None, "qasdf"],
    ["insert", "\n",
     [1,0], None, "q\nasdf"],
    ["delete-char", 1,
     [1,0], None, "q\nsdf"],
    ["delete-char", 0,
     [1,0], None, "q\nsdf"],
    ["delete-char", 2,
     [1,0], None, "q\nf"],
    ["delete-backward-char", 2,
     [0,0], None, "f"],
    ["delete-backward-char", 2,
     [0,0], None, "f"],
    ["delete-char", 2,
     [0,0], None, ""],
    ["insert", "A",
     [0,1], None, "A"],
    ["insert", "hejsan",
     [0,7], None, "Ahejsan"],
    ["insert", "hoppsan\ntesting\n",
     [2,0], None, "Ahejsanhoppsan\ntesting\n"],
    ["delete-backward-char", 3,
     [1,5], None, "Ahejsanhoppsan\ntesti"],
    ["delete-backward-char", 7,
     [0,13], None, "Ahejsanhoppsa"],
    ["goto-char", [0,0],
     [0,0], None, "Ahejsanhoppsa"],
    ["delete-char", 2,
     [0,0], None, "ejsanhoppsa"],
    ["goto-char", [0,4],
     [0,4], None, "ejsanhoppsa"],
    ["insert", "qwe\nfxy\nbdzo\n",
     [3,0], None, "ejsaqwe\nfxy\nbdzo\nnhoppsa"],
    ["delete-backward-char", 13,
     [0,4], None, "ejsanhoppsa"],
    ["insert", "qwe\nfxy\nbdzo\njklq",
     [3,4], None, "ejsaqwe\nfxy\nbdzo\njklqnhoppsa"],
    ["delete-char", 2,
     [3,4], None, "ejsaqwe\nfxy\nbdzo\njklqoppsa"],
    ["push-mark", None,
     [3,4], [3,4], "ejsaqwe\nfxy\nbdzo\njklqoppsa"],
    ["goto-char", [0,0],
     [0,0], [3,4], "ejsaqwe\nfxy\nbdzo\njklqoppsa"],
    ["delete-char", 3,
     [0,0], [3,4], "aqwe\nfxy\nbdzo\njklqoppsa"],
    ["delete-char", 6,
     [0,0], [2,4], "xy\nbdzo\njklqoppsa"],
    ["delete-char", 9,
     [0,0], [0,3], "klqoppsa"],
    ["delete-char", 7,
     [0,0], [0,0], "a"],
    ["delete-char", 1,
     [0,0], [0,0], ""],
    ]

for [cmd, data, pt, mark, buf] in testdata:
    print cmd, `data`
    if cmd == "insert":
	b.insert(data)
    elif cmd == "goto-char":
	b.goto_char(data)
    elif cmd == "delete-char":
	b.delete_char(data)
    elif cmd == "delete-backward-char":
	b.delete_backward_char(data)
    elif cmd == "push-mark":
	b.push_mark()
    else:
	print " Bad command", cmd
    if pt != b.point():
	print " Expected point %s got %s" % (pt, b.point())
    if mark != b.mark():
	print " Expected mark %s got %s" % (mark, b.mark())
    if buf != b.string():
	print " Expected buffer '%s' got '%s'" % (`buf`, `b.string()`)
    print " %s" % b.point_max()
