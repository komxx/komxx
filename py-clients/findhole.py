#! @PYKOM@
# Print summary info about the texts created around a specified time.

import os
import sys
import time
import string

sys.path.insert(1, '@PYKOMLIB@')

import komxx
import kompolicy

def fmt_time(t):
    return time.strftime("%Y-%m-%d %H:%M:%S", t)

def print_list(lst):
    prev = None
    for txt in lst:
        if txt == None:
            print
        else:
            now = time.mktime(txt.creation_time())
            if prev:
                print "%12d s" % (now - prev)
            prev = now
            pretty = fmt_time(txt.creation_time())
            print "%10d %s" % (txt.text_no(), pretty)

class findhole(kompolicy.tty_io, kompolicy.login):
    def __init__(self, host, port, wanted_time):
        print "Connecting..."
        self.session = komxx.new_session(host, port, "findhole.py",
                                         "$Revision: 1.2 $")
        print "Connected."
        self.login()
        self.maybe_enable()
        before = self.session.get_last_text(wanted_time)
        lst = self.iterate(before + 1, self.session.find_previous_text_no)
        lst.reverse()
        lst = lst + [None] + self.iterate(before,
                                          self.session.find_next_text_no)
        print_list(lst)

    def iterate(self, tno, iterator):
        res = []
        try:
            for i in range(5):
                tno = iterator(tno)
                txt = self.session.text(tno)
                txt.prefetch()
                res.append(txt)
        except komxx.err_no_such_text:
            pass
        return res


if __name__ == "__main__":
    t = time.strptime(string.join(sys.argv[1:], " "),
                      "%Y-%m-%d %H:%M:%S")
    print "Searching for", fmt_time(t)
    findhole("kom.lysator.liu.se", "4894", t)
