#include <config.h>

#include <vector>

#include "LyStr.h"

// This entire file, almost, needs to be within extern "C".
// This hackery is here to avoid messing up emacs auto-indentation features.
#define BEGINEXTERNC extern "C" {
#define ENDEXTERNC }
BEGINEXTERNC

#include "Python.h"

ENDEXTERNC

template <class T>
static PyObject *
convert_to_python(const std::vector<T> &arr)
{
    PyObject *res = PyList_New(arr.size());
    if (res == NULL)
	return NULL;

    for (unsigned int i = 0; i < arr.size(); i++)
    {
	PyObject *tuple = convert_to_python(arr[i]);
	
	if (PyList_SetItem(res, i, tuple))
	{
	    Py_DECREF(res);
	    return NULL;
	}
    }

    return res;
}

static PyObject *
convert_to_python(const LyStr &str)
{
    const char *buf = str.view_block();
    PyObject *rv = Py_BuildValue("s#", buf, str.strlen());
    str.view_done(buf);
    return rv;
}

static PyObject *
convert_to_python(int i)
{
    return PyInt_FromLong(i);
}

BEGINEXTERNC

static PyObject *
curemacs_render_line(PyObject *,
		     PyObject *args)
{
    int c;
    int col = 0;
    int done = 0;
    int cursor_phys_row = -1;
    int cursor_phys_col = -1;
    std::vector<LyStr> rendered_lines;
    LyStr current_line = "";

    // Arguments;
    char *line;			// The string to render.
    int line_len;		// Length of string to render.
    int col_limit;		// Maximum number of chars in each row.
    int row_limit;		// Maximum number of rows to return.
    int point_col;		// Logical place where cursor is expected.
    int ctl_arrow;		// True if "^A" is wanted instead of "\001".
    
    if (!PyArg_ParseTuple(args, "s#iiii",
			  &line, &line_len,
			  &col_limit, &row_limit, &point_col, &ctl_arrow))
	return NULL;

    if (col_limit < 2)
    {
	PyErr_SetString(PyExc_ValueError, "Too low column limit");
	return NULL;
    }

    while (done == 0)
    {
	if (point_col == col)
	{
	    cursor_phys_row = rendered_lines.size();
	    cursor_phys_col = current_line.strlen();
	}

	if (col >= line_len)
	{
	    rendered_lines.push_back(current_line);
	    break;
	}
	
	c = (unsigned char)line[col];

	// Find the printable representation of c.

	if (c < ' ' && ctl_arrow)
	    current_line << '^' << (char)(c+64);
	else if (c < ' ' || (c >= 0x7f && c <= 0xa0))
	{
	    static char buf[4];
	    sprintf(buf, "\\%03o", c);
	    current_line.append(buf, 4);
	}
	else
	    current_line << (char)c;

	while (done == 0 && current_line.strlen() >= col_limit)
	{
	    LyStr overflow = current_line.substr(col_limit,
						 current_line.strlen());
	    current_line.remove_last(current_line.strlen() - col_limit);
	    current_line << '\\';
	    rendered_lines.push_back(current_line);
	    if (rendered_lines.size() >= row_limit)
		done = 1;
	    else
		current_line = overflow;
	}
	if (done == 0)
	    ++col;
    }

    // Build the return value.
    PyObject *tuple = PyTuple_New(4);
    if (tuple == NULL)
	return NULL;

    PyObject *line_array = convert_to_python(rendered_lines);
    PyObject *phys_row = convert_to_python(cursor_phys_row);
    PyObject *phys_col = convert_to_python(cursor_phys_col);
    PyObject *used_cols = convert_to_python(col);

    if (line_array == NULL || phys_row == NULL || phys_col == NULL
	|| used_cols == NULL)
    {
	Py_XDECREF(line_array);
	Py_XDECREF(phys_row);
	Py_XDECREF(phys_col);
	Py_XDECREF(used_cols);
	return NULL;
    }
    int r = 0;
    r |= PyTuple_SetItem(tuple, 0, line_array);
    r |= PyTuple_SetItem(tuple, 1, used_cols);
    r |= PyTuple_SetItem(tuple, 2, phys_row);
    r |= PyTuple_SetItem(tuple, 3, phys_col);
    if (r != 0)
    {
	Py_DECREF(tuple);
	return NULL;
    }
    return tuple;
}

static PyMethodDef curemacs_functions[] = {
	{"render_line", curemacs_render_line, 1, "No doc available"},
	{NULL,		NULL, 1, "End"}		/* sentinel */
};

void
initcuremacs()
{
    Py_InitModule("curemacs", curemacs_functions);

    // Check for errors.
    if (PyErr_Occurred())
	Py_FatalError("can't initialize module curemacs");
}

ENDEXTERNC
