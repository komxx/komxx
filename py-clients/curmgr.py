import string
import curses
import select
import sys

error = 'error'
CurMgrExit = 'CurMgrExit'

kill_ring = []

need_cleanup = 0

def assert(val):
    if not val:
	raise error, "Assertion failed"

logfile = None

def log(msg):
    global logfile

    if logfile == None:
	logfile = open("curmgr.trace", "w")
    logfile.write(msg + "\n")
    logfile.flush()

def marker_position(pos):
    if pos == None:
	return None
    elif type(pos) == type([]):
	return pos
    else:
	return pos.marker_position()

def copy_marker(pos):
    if pos == None:
	return None
    elif type(pos) == type([]):
	return pos
    else:
	return pos.copy_marker()

class marker(object):
    def __init__(self, pos, buf):
	self.__buffer = buf
	# "[0, 0]" means just before the first character; "[1, 2]"
	# means between the second and third characters on the second
	# line.
	[self.__r, self.__c] = marker_position(pos)
	self.__insertion_type = 0
	buf.markers.append(self)
	# log("ALLOC %s" % self)

    def __del__(self):
	self.__buffer.markers.remove(self)
	# log("FREE %s" % self)

    def insertion_type(self):
	return self.__insertion_type

    def set_insertion_type(self, type):
	self.__insertion_type = not not type

    def marker_buffer(self):
	return self.__buffer

    def marker_position(self):
	return [self.__r, self.__c]

    def set_marker(self, pos, buf):
	if self.__buffer != buf:
	    self.__buffer.markers.remove(self)
	    buf.markers.append(self)
	    self.__buffer = buf

	pos = marker_position(pos)

	buf.validate_pos(pos)
	[self.__r, self.__c] = pos

    def copy_marker(self):
	return marker([self.__r, self.__c], self.__buffer)

    def move_marker_forward(self, r, c, r1, c1):
	if self.__r == r:
	    if self.__c + self.__insertion_type > c:
		self.__r = self.__r + r1
		self.__c = self.__c + c1
	elif self.__r > r:
	    self.__r = self.__r + r1

all_buffers = {}

class buffer(object):
    """A buffer contains text.
    """

    def __init__(self, name):
	global all_buffers

	# List of markers that point into this buffer.
	self.markers = []

	self.__name = name
	all_buffers[name] = self

	# Modification counter.
	self.__modiff = 0

	# Modification counter value when the buffer was last saved.
	self.__save_modiff = 0

	# The text in the buffer.  This is a list of strings, one for
	# each logical line in the buffer.  The newlines in the buffer
	# are not stored -- they are represented by beginning a new
	# string.  [""] represents an empty buffer.
	self.__text = [""]

	# Point.  Insertions are performed at this point.
	self.__pt = marker([0, 0], self)
	self.__pt.set_insertion_type(1)

	# The narrowing region.
	#self.__begv = None
	#self.__zv = None

	# Modification time when last saved or read in.
	#self.__modtime = None

	#self.__auto_save_modified = None
	#self.__auto_save_failure_time = None
	self.__last_window_start = None

	# Buffer name
	#self.__name = None

	# File name
	#self.__filename = None
	#self.__directory = None

	#self.__backed_up = None
	#self.__auto_save_file_name = None

	# True if buffer is readonly.
	#self.__read_only = 0

	# The mark.  None means not set.
	self.__mark = None

	#self.__major_mode = None
	#self.__mode_name = None
	#self.__mode_line_format = None
	#self.__keymap = None
	#self.__fill_column = 70
        #self.__auto_fill_function = None
	#self.__truncate_lines = 0
	self.ctl_arrow = 0
	self.__minor_modes = []
	#self.__display_table = None
	#self.__mark_active = 0
	#self.__point_before_scroll = None
	self.__mark_stack = []

    def validate_pos(self, pos):
	if pos[0] >= len(self.__text):
	    raise error, "Pos %s out of range; too large row" % pos
	elif pos[1] > len(self.__text[pos[0]]):
	    raise error, "Pos %s out of range; too large col" % pos

    def eobp(self):
	[r, c] = self.__pt.marker_position()
	return r == len(self.__text) -1 and c == len(self.__text[-1])

    def push_mark(self):
	# Allocates one marker.
	if self.__mark != None:
	    self.__mark_stack.append(self.__mark)
	self.__mark = self.__pt.copy_marker()

    def insert(self, text):
	lines = string.split(text, "\n")
	if self.eobp():
	    [r, c] = self.point_max()
	    if self.__text == [""]:
		self.__text = lines
	    else:
		self.__text[-1] = self.__text[-1] + lines[0]
		self.__text[len(self.__text):] = lines[1:]
	    [r1, c1] = self.point_max()
	    self.__move_markers_forward(r, c, r1-r, c1-c)
	else:
	    [r, c] = self.__pt.marker_position()
	    r1 = 0
	    c1 = 0
	    if len(lines) == 1:
		self.__text[r] = (self.__text[r][:c]
				  + lines[0] + self.__text[r][c:])
		c1 = len(lines[0])
	    elif len(lines) > 1:
		self.__text[r:r+1] = ([self.__text[r][:c] + lines[0]]
				      + lines[1:-1] +
				      [lines[-1] + self.__text[r][c:]])
		r1 = len(lines) - 1
		c1 = len(lines[-1]) - c
	    self.__move_markers_forward(r, c, r1, c1)

    def __move_markers_forward(self, r, c, r1, c1):
	for m in self.markers:
	    m.move_marker_forward(r, c, r1, c1)

    def __move_markers_backward(self, b, e):
	[br, bc] = marker_position(b)
	[er, ec] = marker_position(e)
	for m in self.markers:
	    [mr, mc] = m.marker_position()
	    if mr < br or (mr == br and mc <= bc):
		# pos is before the deleted block
		pass
	    elif mr < er or (mr == er and mc <= ec):
		# pos is within the deleted block
		m.set_marker(b, self)
	    elif mr == er:
		# pos is on the line where the deletion ends
		m.set_marker([br, bc + mc - ec], self)
	    else:
		# pos is beyond the deleted block
		m.set_marker([mr - er + br, mc], self)

    def delete_region(self, b, e):
	[br, bc] = marker_position(b)
	[er, ec] = marker_position(e)
	if br == er:
	    if bc > ec:
		raise error, "bad interval %s:%s" % (b, e)
	    res = [self.__text[br][bc:ec]]
	    self.__text[br] = self.__text[br][:bc] + self.__text[br][ec:]
	elif br == er - 1:
	    res = [self.__text[br][bc:], self.__text[er][:ec]]
	    self.__text[br:er+1] = [self.__text[br][:bc] +
				    self.__text[er][ec:]]
	elif br < er - 1:
	    res = ([self.__text[br][bc:]]
		   + self.__text[br+1:er] +
		   [self.__text[er][:ec]])
	    self.__text[br:er+1] = [self.__text[br][:bc] +
				    self.__text[er][ec:]]
	else:
	    raise error, "bad interval %s:%s" % (b, e)

	self.__move_markers_backward(b, e)
	return res
		   
    def kill_region(self, b, e):
	global kill_ring

	kill_ring.append(self.delete_region(b, e))

    def offset(self, chars, anchor=None):
	if anchor == None:
	    [r, c] = self.__pt.marker_position()
	else:
	    [r, c] = marker_position(anchor)
	if chars == 0:
	    return [r, c]
	elif chars > 0:
	    while c + chars > len(self.__text[r]):
		chars = chars - (len(self.__text[r]) - c) - 1
		c = 0
		r = r + 1
		if r >= len(self.__text):
		    return self.point_max()
	    if r >= len(self.__text) - 1 and c + chars > len(self.__text[-1]):
		return self.point_max()
	    else:
		return [r, c + chars]
	else:
	    while c + chars < 0:
		chars = chars + c + 1
		r = r - 1
		if r < 0:
		    return [0, 0]
		c = len(self.__text[r])
	    return [r, c + chars]
		
    def delete_backward_char(self, arg=1, kill_flag=0):
	if kill_flag:
	    self.kill_region(self.offset(-arg), self.__pt)
	else:
	    self.delete_region(self.offset(-arg), self.__pt)

    def delete_char(self, arg=1, kill_flag=0):
	if kill_flag:
	    self.kill_region(self.__pt, self.offset(arg))
	else:
	    self.delete_region(self.__pt, self.offset(arg))

    def point_min(self):
	return [0, 0]

    def point_max(self):
	return [len(self.__text)-1, len(self.__text[-1])]

    def point(self):
	return self.__pt.marker_position()

    def point_marker(self):
	# Allocates one marker.
	return self.__pt.copy_marker()

    def mark_marker(self):
	# Allocates one marker, unless the mark is unset.
	if self.__mark == None:
	    return None
	else:
	    return self.__mark.copy_marker()

    def mark(self):
	if self.__mark == None:
	    return None
	else:
	    return self.__mark.marker_position()

    def string(self):
	return string.joinfields(self.__text, "\n")

    def substring_eol(self, pos):
	"""Return the substring from POS to end of line.
	"""
	[r, c] = marker_position(pos)
	return self.__text[r][c:]

    def __repr__(self):
	return "#buffer: pt=%s mark=%s text=%s." %(
	    self.__pt.marker_position(), self.__mark.marker_position(),
	    self.__text)

    def validate_buffer(self, pos):
	if type(pos) == type([]):
	    return
	elif pos.marker_buffer() == self:
	    return
	else:
	    raise error, "marker points to wrong buffer"

    def rows(self):
	return len(self.__text)

    def goto_char(self, pos):
	self.validate_buffer(pos)
	pos = marker_position(pos)
	self.validate_pos(pos)
	self.__pt.set_marker(pos, self)

    def set_last_window_start(self, start):
	if self.__last_window_start == None:
	    self.__last_window_start = marker(start, self)
	else:
	    self.__last_window_start.set_marker(start, self)

    def last_window_start(self):
	return marker_position(self.__last_window_start)

    def active_maps(self):
	return [{ord("a"):buffer.insert_a,
		 ord("q"):buffer.exit}]

    def insert_a(self):
	self.insert("a")

    def exit(self):
	raise CurMgrExit

class window(object):
    def __init__(self, buf, parent):
	"""Create a window showing BUF in PARENT.

	PARENT must be a virtual window, or None if this is the root
	window.  BUF must be a buffer object, or None if this is
	a virtual window.
	"""

	self.__mini_p = 0
	self.__vchildren = None
	# Splitting the screen horizontally is not yet supported.
	#self.__hchildren = None
	self.__parent = parent

	# The upper left corner coordinates of this window,
	# as integers relative to upper left corner of frame = 0, 0.
	self.__left = 0
	self.__top = 0

	# The size of the window.  The mode line (if any)
	# is not included.
	self.__height = None
	self.__width = None

	# The buffer of this window, or None if this is an internal window.
	self.__buffer = None

	# A marker pointing to where in the text to start displaying.
	self.__start = None
	# A marker pointing to where in the text point is in this window,
	# used only when the window is not selected.
	# This exists so that when multiple windows show one buffer
	# each one can have its own value of point.  It is None for
	# the selected window.
	self.__pointm = None
	# Non-nil means next redisplay must use the value of start
	# set up for it in advance.  Set by scrolling commands.
	self.__force_start = 0

	# A list of strings -- the source lines that are present on
	# the screen.  The first string corresponds to the substring
	# that starts at self.__start and ends at that line end.  The
	# strings do not contain any newline.  The value None means
	# that the line is known to be dirty.
	self.__visible_content = []

	# A list of lists of strings.  Each list of strings is the
	# physical lines that are created by a single string from the
	# __visible_content string.
	self.__on_screen = []

	if buf != None:
	    if parent == None:
		raise error, "leaf buffer must have parent"
	    parent.__vchildren.append(self)
	    self.__connect(buf, 0)
	else:
	    if parent == None:
		self.__vchildren = []
	    else:
		raise error, "horizontal splitting not yet supported"
	
    def __assert_leaf(self):
	if self.__buffer == None:
	    raise "not a leaf window"

    def set_mini_buf(self):
	self.__mini_p = 1
	self.__height = 1

    def switch_to_buffer(self, buf):
	self.__assert_leaf()
	selected = self.__pointm == None
	self.__buffer.set_last_window_start(marker_position(self.__start))
	self.__connect(buf, selected)

    def window_buffer(self):
	return self.__buffer

    def __connect(self, buf, selected):
	self.__buffer = buf
	self.__on_screen = []
	self.__visible_content = []
	lws = self.__buffer.last_window_start()
	if lws == None:
	    lws = [0, 0]
	if self.__start == None:
	    self.__start = marker(lws, self.__buffer)
	else:
	    self.__start.set_marker(lws, self.__buffer)
	self.__force_start = 0
	if not selected:
	    if self.__pointm == None:
		self.__pointm = self.__buffer.point_marker()
	    else:
		self.__pointm.set_marker(self.__buffer.point(), self.__buffer)
	else:
	    if self.__pointm != None:
		del self.__pointm
		self.__pointm = None

    def is_leaf(self):
	if self.__buffer != None:
	    assert(self.__vchildren == None)
	    return 1
	else:
	    assert(type(self.__vchildren) == type([]))
	    return 0

    def pack(self, height, width, left, top):
	"""Assign size and position so that this window occupies R�C.
	"""
	
	if self.is_leaf():
	    self.__height = height
	    self.__width = width
	    self.__left = left
	    self.__top = top
	else:
	    # Initialize h_sum to the number of modelines.
	    h_sum = len(self.__vchildren) - 1

	    # We expect at least a minbuffer and another buffer, so we
	    # expecte at least one mode-line.
	    assert(h_sum > 0)

	    # Add requested sizes of child windows.
	    unasigned = 0
	    for win in self.__vchildren:
		if win.__height != None:
		    h_sum = h_sum + win.__height
		else:
		    unasigned = unasigned + 1

	    # Distribute the difference, if any, to the children.
	    if unasigned > 0:
		for win in self.__vchildren:
		    if win.__height == None:
			# FIXME: this is ugly!  We should not assign
			# win.__height.
			win.__height = (height - h_sum) / unasigned
			# This assertion may fail if the frame shrinks.
			assert(win.__height > 0)
			h_sum = h_sum + win.__height
			unasigned = unasigned - 1
		assert(unasigned == 0)
		assert(h_sum == height)
	    else:
		# This code will only be reached if the frame shrinks.
		assert(0)
	    
	    assert(unasigned == 0)
	    assert(h_sum == height)
	    # Assign positions and set the width.
	    r = top
	    for win in self.__vchildren:
		win.pack(win.__height, width, left, r)
		r = r + win.__height + 1 # 1 for the mode line.

    def __start_pos(self, selected_window):
	# Compute the first visible position to display.
	return [0,0] # FIXME

    def __setup_start(self, selected_window):
	# Set up self.__start and make sure that
	# self.__visible_content and self.__on_screen is adjusted to
	# it.
	new_start = self.__start_pos(selected_window)
	if self.__start != None:
	    assert(len(self.__visible_content) == len(self.__on_screen))
	    # Set up self.__visible_content so that it matches the
	    # new starting position.
	    [oldr, oldc] = self.__start.marker_position()
	    [newr, newc] = new_start
	    if oldr > newr:
		del self.__visible_content[:newr-oldr+1]
		del self.__on_screen[:newr-oldr+1]
		if newc != 0 and len(self.__visible_content) > 0:
		    self.__visible_content[0] = None
		    self.__on_screen[0] = None
	    elif oldr < newr:
		if oldc != 0 and len(self.__visible_content) > 0:
		    self.__visible_content[0] = None
		    self.__on_screen[0] = None
		self.__visible_content[:0] = [None] * (newr-oldr)
		self.__on_screen[:0] = [None] * (newr-oldr)
	    elif oldc != newc and len(self.__visible_content) > 0:
		self.__visible_content[0] = None
		self.__on_screen[0] = None
	else:
	    assert(self.__visible_content == None)
	    assert(self.__on_screen == None)
	    self.__visible_content = []
	    self.__on_screen = []
	self.__start.set_marker(new_start, self.__buffer)

    def __render_line(self, line, rows_limit, point_col = None):
	col = 0
	cursor = []
	if point_col != None:
	    cursor = [None, None]
	res = []
	pl = ""
	assert(point_col == None or point_col <= len(line))
	assert(rows_limit > 0)
	linelen = len(line)
	while 1:
	    if point_col == col:
		cursor = [len(res), len(pl)]
	    if col >= linelen:
		col = None
		break
	    c = ord(line[col])
	    # Find the printable representation of c.
	    if c < 32 and self.__buffer.ctl_arrow:
		c = "^%c" % (c+64)
	    elif c < 32 or (c >= 127 and c < 161):
		c = "\\%03o" % c
	    else:
		c = chr(c)
	    # Append c to the buffer, taking care of line wrapping.
	    pl = pl + c
	    if len(pl) >= self.__width - 1:
		overflow = pl[self.__width-1:]
		pl = pl[:self.__width-1] + "\\"
		res.append(pl)
		if len(res) >= rows_limit:
		    return [res, col] + cursor
		pl = overflow
	    col = col + 1
	res.append(pl)
	return [res, col] + cursor

    def __active_point(self, selected_window):
	if self == selected_window:
	    assert(self.__pointm == None)
	    return self.__buffer.point()
	else:
	    assert(self.__pointm != None)
	    return self.__pointm.marker_position()

    def __repaint_win(self, w, selected_window, want_cursor):
	cursor_y = None
	cursor_x = None
	phys_row = self.__top
	phys_limit = self.__top + self.__height
	assert(self.__left == 0) # Vertical splitting not supported yet.
	[r0, c0] = self.__start.marker_position()
	r = r0
	c = c0
	[point_row, point_col] = self.__active_point(selected_window)

	while phys_row < phys_limit and r < self.__buffer.rows():
	    line = self.__buffer.substring_eol([r, c])
	    if len(self.__visible_content) <= r - r0:
		self.__visible_content.append(None)
		self.__on_screen.append(None)
	    if self.__visible_content[r - r0] == line \
	       and (want_cursor == 0 or point_row != r):
		phys = self.__on_screen[r - r0]
	    else:
		if point_row == r and want_cursor:
		    [phys, clim, cursor_y, cursor_x] = self.__render_line(
			line, phys_limit - phys_row, point_col)
		else:
		    [phys, clim] = self.__render_line(
			line, phys_limit - phys_row)
		if clim == None:
		    self.__visible_content[r - r0] = line
		else:
		    self.__visible_content[r - r0] = line[:clim]
		self.__on_screen[r - r0] = phys
	    for p in phys:
		w.addstr(phys_row, 0, p)
		phys_row = phys_row + 1
	    r = r + 1
	if want_cursor:
	    return (cursor_y, cursor_x)
	else:
	    assert(cursor_y == None)
	    assert(cursor_x == None)
	    return None

    def __place_cursor(self, w):
	w.move(self.__pt_phys_row, self.__pt_phys_col)

    def refresh(self, w, selected_window):
	if self.__vchildren != None:
	    cursor = None
	    for child in self.__vchildren:
		nc = child.refresh(w, selected_window)
		if nc != None:
		    assert(cursor == None)
		    cursor = nc
		if child.__mini_p == 0:
		    # Produce a mode line
		    child.refresh_modeline(w)
	    assert(cursor != None)
	    return cursor
	else:
	    assert(self.__buffer != None)
	    self.__setup_start(selected_window)
	    if self == selected_window:
		return self.__repaint_win(w, selected_window, 1)
	    else:
		self.__repaint_win(w, selected_window, 0)
	    	

    def refresh_modeline(self, w):
	assert(self.__buffer != None)
	assert(self.__mini_p == 0)
	w.addstr(self.__top + self.__height, 0, "-" * self.__width,
		 curses.A_REVERSE)

    def select(self):
	if self.__pointm != None:
	    self.__buffer.goto_char(self.__pointm)
	    del self.__pointm
	    self.__pointm = None

    def active_maps(self):
	return self.__buffer.active_maps()

class frame(object):
    def __init__(self):
	global need_cleanup

	# Cursor position measured in characters from the upper left corner
	# within the frame.
	self.__cursor_x = 0
	self.__cursor_y = 0

	# Size of frame, in characters.
	self.__height = None
	self.__width = None

	self.__message_buf = None

	# The root window.  This is always a virtual vertical window.
	self.__root_window = window(None, None)
	self.__selected_window = window(buffer("*scratch*"),
					self.__root_window)
	self.__selected_window.select()
	self.__minibuffer_window = window(buffer(" *Minibuf*"),
					  self.__root_window)
	self.__minibuffer_window.set_mini_buf()

	self.unread_command_events = []
	self.w = curses.initscr()
	need_cleanup = 1
	self.w.keypad(1)
	curses.cbreak()
	curses.noecho()
	curses.nonl()
	curses.intrflush(0)
	self.w.nodelay(1)
	(self.__height, self.__width) = self.w.getmaxyx()
	self.__root_window.pack(self.__height, self.__width, 0, 0)

    def cleanup(self):
	curses.endwin()

    def refresh(self):
	(self.__cursor_y, self.__cursor_x) = self.__root_window.refresh(
	    self.w, self.__selected_window)
	if self.__cursor_y == None or self.__cursor_x == None:
	    self.w.move(self.__height-1, 0)
	else:
	    self.w.move(self.__cursor_y, self.__cursor_x)
	self.w.refresh()

    def read_fd(self):
	return sys.stdin.fileno()

    def read_input(self):
	ch = self.w.getch()
	if ch == -1:
	    return
	self.unread_command_events.append(ch)
	self.parse()

    def parse(self):
	while 1:
	    bnd = self.find_binding(self.unread_command_events)
	    if bnd == None:
		# Incomplete key
		return
	    else:
		(binding, klen) = bnd
		self.current_key = self.unread_command_events[:klen]
		self.unread_command_events = self.unread_command_events[klen:]
		if binding == None:
		    curses.beep()
		else:
		    binding(self.selected_buffer())

		
    def find_binding(self, key):
	maxlen = 0
	for keymap in self.active_maps():
	    for ix in range(len(key)):
		k = key[ix]
		if ix >= maxlen:
		    maxlen = ix + 1
		if keymap.has_key(k):
		    if keymap[k] == None:
			# Explicitly unbound.
			return (None, maxlen)
		    elif type(keymap[k]) == type({}):
			keymap = keymap[k]
		    else:
			# Complete key found
			return (keymap[k], maxlen)
		else:
		    # Unbound in this map.
		    break
	    else:
		# Incomplete key (as opposed to unbound key).
		return None
	# Unbound
	return (None, maxlen)


    def active_maps(self):
	return self.__selected_window.active_maps() + [self.global_map]

    global_map = {}

    def selected_buffer(self):
	return self.__selected_window.window_buffer()

def init():
    global the_only_frame
    global selected_frame

    the_only_frame = frame()
    selected_frame = the_only_frame
    #all_buffers["*scratch*"].insert("This is the *scratch* buffer.")
    try:
	while 1:
	    selected_frame.refresh()
	    fd = selected_frame.read_fd()
	    (r, w, e) = select.select([fd], [], [])
	    if fd in r:
		selected_frame.read_input()
    except CurMgrExit:
	pass
    the_only_frame.cleanup()
    print "Bye, bye."

def test():
    global need_cleanup

    the_only_frame = None
    try:
	init()
    finally:
	if need_cleanup:
	    curses.endwin()

if __name__ == "__main__":
    test()

