#! @PYKOM@
#
# This program erases all texts whose recipients are all among a list
# of forbidden recipients.  This can be used to permanently erase
# texts that nobody are allowed to see when a LysKOM database changes
# ownership.
#
# It can also create a list of all conferences and persons.  This list
# can be manually pruned in a text editor, and then used as input to
# this program.
#
# Usage:
#
#   Get a list of all conferences and persons:
#
#      bulk-eraser.py -o listfile.txt --list server port
#
#   Check what would be done:
#
#      bulk-eraser.py --dry-run server port listfile.txt
#
#   Actually erase texts:
#
#      bulk-eraser.py --erase server port listfile.txt
#
# WARNING: This program should be run as a person with full
# privileges.  Otherwise, it may erase texts that have an additional
# secret recipient.
#
# This program was written when Cendio and Ingate stopped sharing the
# same LysKOM system.  Two copies were made of the database.  A list
# of conferences that Ingate was not allowed to read was made, and
# this program was run on the Ingate database using that list.  A
# similar run was made on the Cendio database.  The end result is that
# no Ingate secrets are present in the Cendio database, and vice
# versa.  This means that (once all backup copies have expired) there
# is no way for Ingate to access the secrets of Cendio, and vice
# versa, even if the builtin LysKOM security is bypassed.

import os
import sys
import time
import string
import getopt

sys.path.insert(1, '@PYKOMLIB@')

import komxx
import kompolicy
import prefetcher

PROGRESS = 0
WOULD_BLOCK = 1
DONE = 2

class confno_db(dict):
    """Read a file of conference numbers.

    Blank lines and lines starting with "#" are ignored.
    Only the first field of each line is used.  It must be a number.

    Returns a dictionary from conference number to the rest of the line.
    """

    def __init__(self, fn):
        dict.__init__(self)
        fp = open(fn, "r")
        for line in fp:
            if len(line) == 0:
                continue
            if line[0] == "#":
                continue
            confno, rest = line.split(None, 1)
            confno = int(confno)
            self[confno] = rest
        fp.close()

class text_nos_of_conf(object):
    def __init__(self, session, cno, consumer):
        self.session = session
        self.conf = self.session.conf(cno)
        self.tm = self.session.text_mapping(cno)
        self.lno = 0
        self.consumer = consumer

    def next(self):
        rv = WOULD_BLOCK
        while True:
            while self.tm.prefetch(self.lno + 1) == komxx.st_pending:
                return rv
            self.lno = self.tm.next_existing(self.lno)
            if self.lno == 0:
                return DONE
            self.tm.prefetch(self.lno + 1)
            tno = self.tm[self.lno]
            if tno != 0:
                self.consumer.add(tno)
                rv = PROGRESS

class eraser(object):
    def __init__(self, session, safety):
        self.__session = session
        self.__safety = safety
        self.__all_added = False
        self.__pending = []

    def add(self, text_no):
        if not self.__safety:
            self.__pending.append(
                self.__session.unsafe_initiate_delete_text(text_no))

    def next(self):
        rv = WOULD_BLOCK
        while len(self.__pending):
            q = self.__pending[0]
            if q.status() == komxx.st_pending:
                return rv
            if q.status() == komxx.st_error:
                sys.stderr.write("\nRemoval failed: %d\n" % q.error())
                sys.exit(1)
            del self.__pending[0]
            rv = PROGRESS
        if self.__all_added:
            return DONE
        else:
            return rv

    def all_added(self):
        self.__all_added = True

class erase_texts(object):
    def __init__(self, session, forbidden, eraser):
        self.__session = session
        self.__forbidden = forbidden
        self.__eraser = eraser
        self.__inqueue = []
        self.__seen = set()
        self.__all_added = False
        self.__erased = 0
        self.__spared = 0

    def add(self, tno):
        if tno in self.__seen:
            return
        self.__seen.add(tno)
        text = self.__session.text(tno)
        text.prefetch()
        self.__inqueue.append(text)

    def all_added(self):
        self.__all_added = True

    def next(self):
        rv = WOULD_BLOCK
        while len(self.__inqueue) > 0:
            text = self.__inqueue[0]
            if text.prefetch() == komxx.st_pending:
                t1 = time.time()
                sys.stdout.write(
                    ("\033[1K\r"
                     "Examined: %d Erased: %d Spared: %d"
                     " Requests: %d (%.3f req/s)") % (
                    self.__erased + self.__spared,
                    self.__erased,
                    self.__spared,
                    self.__session.generated_questions(), 
                    float(self.__session.generated_questions()) / (t1 - t0)))
                sys.stdout.flush()
                return rv

            rv = PROGRESS

            saved = False
            for r in text.recipients():
                if r.recipient() not in self.__forbidden:
                    saved = True
                    break
            if not saved:
                self.__eraser.add(text.text_no())
                self.__erased += 1
            else:
                self.__spared += 1

            del self.__inqueue[0]

        if self.__all_added:
            return DONE
        else:
            return rv

    def report(self):
        print "Total of %s erased and %d spared texts" % (self.__erased,
                                                          self.__spared)

    
class bulk_eraser(kompolicy.tty_io, kompolicy.login):
    def __init__(self, host, port):
        print "Connecting..."
        self.session = komxx.new_session(host, port, "bulk-eraser.py",
                                         "1.0")
        print "Connected."
        self.login()
        self.session.enable(255)
        p = self.session.pers(self.session.my_login())
        privs = p.privileges()
        if not (privs.wheel() and privs.admin() and privs.statistic()):
            print "This person is not an administrator"
            sys.exit(1)
        self.__safety = True

    def remove_safety(self):
        self.__safety = False

    def get_all_conf(self):
        return prefetcher.prefetch_confno_conf(
            self.session,
            self.session.lookup_z_name("", False, True))

    def get_all_pers(self):
        return prefetcher.prefetch_confno_pers(
            self.session,
            self.session.lookup_z_name("", True, False))

    def idle(self, then):
        then = time.mktime(then)
        now = time.time()
        return int(float(now - then) / (24 * 3600))

    def conf_idle(self, z):
        return self.idle(self.session.conf(z.conf_no()).last_written())

    def pers_idle(self, z):
        return self.idle(self.session.pers(z.conf_no()).last_login())

    def list(self, outfile):
        ofp = open(outfile, "w")
        ofp.write("#\n# Conferences\n#\n")
        for z in self.get_all_conf():

            ofp.write("%5d %4d %s\n" % (z.conf_no(),
                                        self.conf_idle(z),
                                        z.name().replace("\n", "-")))

        ofp.write("#\n# Persons\n#\n")
        for z in self.get_all_pers():

            ofp.write("%5d %4d %s\n" % (z.conf_no(),
                                        self.pers_idle(z),
                                        z.name().replace("\n", "-")))

        ofp.write("#\n# End of list.\n#\n")
        ofp.close()

    def examine(self, forbidden):
        global t0

        t0 = time.time()
        erase_worker = eraser(self.session, self.__safety)
        erase_filter = erase_texts(self.session, forbidden, erase_worker)
        workers = [erase_filter, erase_worker]
        for cno in forbidden.iterkeys():
            workers.append(text_nos_of_conf(self.session, cno, erase_filter))

        while len(workers) > 0:
            need_block = True
            restart = False
            for ix, worker in enumerate(workers):
                rv = worker.next()
                if rv == PROGRESS:
                    need_block = False
                elif rv == WOULD_BLOCK:
                    pass
                elif rv == DONE:
                    del workers[ix]
                    restart = True
                    if len(workers) == 2:
                        erase_filter.all_added()
                    if len(workers) == 1:
                        erase_worker.all_added()
                    break
                else:
                    raise ValueError("bad return value from next() method")
            if need_block and not restart:
                self.session.block(1000000)
        t1 = time.time()
        print
        erase_filter.report()
        print "Generated %d requests in %.3f seconds.  %.3f req/s" % (
            self.session.generated_questions(), t1 - t0,
            float(self.session.generated_questions()) / (t1 - t0))
        

def usage():
    sys.stderr.write(
        "usage: bulk-erase.py { --dry-run | --erase } server port listfile\n")
    sys.stderr.write(
        "or:    bulk-erase.py -o listfile --list server port\n")
    sys.exit(1)

def main():
    list = 0
    dry_run = 0
    erase = 0
    outfile = None
    infile = None
    opts, args = getopt.getopt(sys.argv[1:], "o:",
                               ["list", "dry-run", "erase"])
    for opt, val in opts:
        if opt == "-o":
            outfile = val
        elif opt == "--list":
            list += 1
        elif opt == "--dry-run":
            dry_run += 1
        elif opt == "--erase":
            erase += 1
        else:
            sys.stderr.write("unhandled option '%s'.\n" % repr(opt))
            sys.exit(1)

    if list + dry_run + erase != 1:
        usage()

    if list:
        if len(args) != 2 or outfile is None:
            usage()
    else:
        if len(args) != 3 or outfile is not None:
            usage()
        infile = args[2]

    server = args[0]
    port = args[1]

    obj = bulk_eraser(server, port)

    if list:
        obj.list(outfile)

    if erase:
        obj.remove_safety()

    if erase or dry_run:
        obj.examine(confno_db(infile))

if __name__ == "__main__":
    main()
