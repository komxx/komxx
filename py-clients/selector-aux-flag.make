selector-aux-flag.incl: selector-aux-flag.list \
		selector-aux-flag.template \
		selector-aux-flag.make
	$(RM) selector-aux-flag.incl selector-aux-flag.incl.tmp
	cat $(srcdir)/selector-aux-flag.list  \
	| grep -v '^#' \
	| while read class method data; \
	  do \
		if [ "$$data" = "" ]; then data=data; fi; \
		cat $(srcdir)/selector-aux-flag.template \
		| sed \
			-e s/CLASS/$$class/g \
			-e s/DATA/$$data/g \
			-e s/METHOD/$$method/g \
		>> selector-aux-flag.incl.tmp \
		|| exit 1; \
	  done
	mv selector-aux-flag.incl.tmp selector-aux-flag.incl
