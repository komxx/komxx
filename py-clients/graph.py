#! @PYKOM@
# A simple demo client for komxxmodule.
# This will use xvcg to draw a graph of the comment trees in a
# conference of your choice.

import string
import sys
import os
import time

sys.path.insert(1, '@PYKOMLIB@')

import komxx

class demo_kom(object):
    def __init__(self, host, port, conf_no, texts):
	self.session = komxx.new_session(host, port, "komdemo.py",
					 "$Revision: 1.4 $")
	self.login()
	self.toploop(conf_no, texts)

    def login(self):
	while 1:
	    name = raw_input("Vad heter du? ")
	    if name == "":
		print "Jag talar inte med folk som inte ber�ttar vad de heter."
		sys.exit(0)
	    matches = self.session.lookup_z_name(name, 1, 0)
	    if len(matches) == 0:
		print "Det finns ingen som heter s�."
	    elif len(matches)== 1:
		print matches[0].name()
		cno = matches[0].conf_no()
		password = self.getpassword()
		if password == "":
		    print "Inget l�sen angivet"
		elif self.session.login(cno, password) == komxx.st_ok:
		    print "V�lkommen!"
		    return
		else:
		    print "Felaktigt l�senord"
	    else:
		print "Du kan mena n�gon av dessa:"
		nr = 1
		for match in matches:
		    print " %3d" % nr, matches[nr-1].name()
		    nr = nr + 1
		number = raw_input("Ange nummer f�r �nskad person: ")
		cno = matches[eval(number)-1].conf_no()
		password = self.getpassword()
		if password == "":
		    print "Inget l�sen angivet"
		elif self.session.login(cno, password) == komxx.st_ok:
		    print "V�lkommen!"
		    return
		else:
		    print "Felaktigt l�senord"
   
    def echo_off(self):
	os.system("stty -echo")

    def echo_on(self):
	print
	os.system("stty echo")

    def getpassword(self):
	self.echo_off()
	try:
	    passwd = raw_input("L�senord: ")
	finally:
	    self.echo_on()
	return passwd

    def toploop(self, conf_no, texts):
	c = self.session.conf(conf_no)
	tm = self.session.text_mapping(conf_no)
	vcg = open("sample.vcg", "w")
	vcg.write('graph: { title: "KOM"\n')
	loc = c.first_local_no() + c.no_of_texts() - 1
	active_texts = {}
	text_list = []

	# Prefetch the part of the map that we definitely know we need.
	#start = c.first_local_no()
	#if start < loc - texts + 1:
	#    start = loc - texts + 1
	#tm.get_map(start, loc)

	report = 0
	# Find the correct number of global text numbers.
	while loc >= c.first_local_no() and texts > 0:
	    if report % 100 == 0:
		print "\rMap... got %d at %d  " % (len(text_list), loc),
	    report = report + 1
	    glob = tm[loc]
	    if glob != 0:
		text_list.append(glob)
		texts = texts - 1
	    loc = loc - 1
	print "\rMap... got %d at %d  " % (len(text_list), loc)

	pipeline = [self.session.text(text_list[0])] * 20
	report = 0
	# Prefetch text statuses.  Remember the order in wich they are
	# prefetched.  Emit all nodes to vcg.
	for tno in text_list:
	    t = self.session.text(tno)
	    active_texts[tno] = t
	    t.prefetch()

	    # Don't prefetch too much at a time.
	    pipeline.append(t)
	    pipeline[0].exists()
	    del pipeline[0]
	    report = report + 1
	    if report % 100 == 0:
		print "\rPrefetch... %d of %d" % (report, len(text_list)),

	print "\rPrefetch... %d of %d" % (report, len(text_list))

	# Retrieve all texts and emit edges.
	report = 0
	for tno in text_list:
	    t = active_texts[tno]
	    if t.exists():
	        vcg.write('node: { title: "%d" label: "%d" }\n' % (tno, tno))
		for child in t.comments_in():
		    if active_texts.has_key(child) \
		       and active_texts[child].exists():

			vcg.write(
			    'edge: { sourcename: "%d" targetname: "%d" }\n' %
			    (tno, child))
		for child in t.footnotes_in():
		    if active_texts.has_key(child) \
		       and active_texts[child].exists():

			vcg.write(('edge: { sourcename: "%d" ' +
				   'targetname: "%d" thickness: 4 }\n') %
				  (tno, child))
	    report = report + 1
	    if report % 100 == 0:
		print "\rProcess... %d of %d" % (report, len(text_list)),

	print "\rProcess... %d of %d" % (report, len(text_list))
	# Finish up.
	vcg.write("}\n")
	vcg.close()
	sys.exit(0)

if __name__ == "__main__":
    if len(sys.argv) > 1:
	host = sys.argv[1]
    else:
	host = "kom.lysator.liu.se"
    if len(sys.argv) > 2:
	port = sys.argv[2]
    else:
	port = "4894"
    if len(sys.argv) > 3:
	conf_no = string.atoi(sys.argv[3])
    else:
	conf_no = 6
    if len(sys.argv) > 4:
	texts = string.atoi(sys.argv[4])
    else:
	texts = 50
    s=demo_kom(host, port, conf_no, texts)
