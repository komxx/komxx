selector.incl: selector.list selector.template selector.make
	$(RM) selector.incl selector.incl.tmp
	cat $(srcdir)/selector.list  \
	| grep -v '^#' \
	| while read class method data; \
	  do \
		if [ "$$data" = "" ]; then data=data; fi; \
		cat $(srcdir)/selector.template \
		| sed \
			-e s/CLASS/$$class/g \
			-e s/DATA/$$data/g \
			-e s/METHOD/$$method/g \
		>> selector.incl.tmp \
		|| exit 1; \
	  done
	mv selector.incl.tmp selector.incl
