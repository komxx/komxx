selector-aux-flag-set.incl: \
		selector-aux-flag-set.list \
		selector-aux-flag-set.template \
		selector-aux-flag-set.make
	$(RM) selector-aux-flag-set.incl \
		selector-aux-flag-set.incl.tmp
	cat $(srcdir)/selector-aux-flag-set.list  \
	| grep -v '^#' \
	| while read class method data; \
	  do \
		if [ "$$data" = "" ]; then data=data; fi; \
		cat $(srcdir)/selector-aux-flag-set.template \
		| sed \
			-e s/CLASS/$$class/g \
			-e s/DATA/$$data/g \
			-e s/METHOD/$$method/g \
		>> selector-aux-flag-set.incl.tmp \
		|| exit 1; \
	  done
	mv selector-aux-flag-set.incl.tmp selector-aux-flag-set.incl
