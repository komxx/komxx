import komxx
import sys

def pretty_name(confno):
   return "M�te/Person " + session.conf(confno).name()

def pretty_author(textno):
   t=session.text(textno)
   return pretty_name(t.author())

def pretty_date(t):
   return `t[0]` + "-" + `t[1]` + "-" + `t[2]` \
	  + " " + `t[3]` + ":" + `t[4]` + ":" + `t[5]`

def printtext(text):
   print text.text_no(), pretty_date(text.creation_time()),
   print "/" + `text.num_lines()`,
   if (text.num_lines() == 1):
      print "rad/",
   else:
      print "rader/",
   print pretty_name(text.author())

   for rcpt in text.recipients():
      if (rcpt.is_carbon_copy()):
	 print "Extra kopiemottagare",
      else:
	 print "Mottagare",
      print pretty_name(rcpt.recipient()), "<" + `rcpt.local_no()` + ">"
      if (rcpt.has_rec_time()):
	 print "    Mottaget:", pretty_date(rcpt.rec_time())
      if (rcpt.has_sent_at()):
	 print "    S�nt:", pretty_date(rcpt.sent_at())
      if (rcpt.sender() != 0):
	 print "    S�nt av:", pretty_name(rcpt.sender())

   for uplink in text.uplinks():
      if (uplink.is_footnote()):
	 print "Fotnot",
      else:
	 print "Kommentar",
      print "till text", uplink.parent();
      if (uplink.sent_later()):
	 print "    S�nt:", pretty_date(uplink.sent_at())
      if (uplink.sender() != 0):
	 print "    S�nt av:", pretty_name(uplink.sender())
   if (text.num_marks() == 1):
      print "Markerad av 1 person"
   elif (text.num_marks() > 1):
      print "Markerad av", text.num_marks(), "personer"
   print "�rende:", text.subject()
   print "-" * 70
   print text.body()
   print "(" + `text.text_no()` + ")" + 35*"-"
   for footnote in text.footnotes_in():
      print "Fotnot i text", footnote, "av", pretty_author(footnote)
   for comment in text.comments_in():
      print "Kommentar i text", comment, "av", pretty_author(comment)

def find_empty_conf():
    cno = 1
    while 1:
	c = session.conf(cno)
	if c.exists():
	    if (c.no_of_texts() == 0 and c.first_local_no() > 1
		and not c.type().is_protected()
		and not c.type().is_letterbox()
		and not c.type().is_secret()):
		print "Found", cno, ":", c.name()
		return
	    else:
		print cno
	cno = cno + 1

def find_empty_pers():
    cno = 1
    while 1:
	c = session.pers(cno)
	if c.exists():
	    if c.no_of_created_texts() == 0:
		print "Found", cno, ":", c.name()
		return
	    else:
		print cno
	cno = cno + 1

def test(cno):
   global session
   session = komxx.new_session("kom.lysator.liu.se", "4894", "test.py",
			       "$Revision")
   print sys.argv
   if session.login(19, sys.argv[1]) != komxx.st_ok:
      print "Kunde inte logga in.  Fel l�sen?"
   print "Inloggad som person 19"
   #find_empty_pers()
   #find_empty_conf()
   print session.get_members(cno, 0, 9999)
   return

   tno = 4711
   text = session.text(tno)
   try:
      printtext(text)
   except komxx.err_pwd:
      print "Bad password"
   except komxx.err_obsolete:
      print "Obsolete command"
   except komxx.err_long_str:
      print "Very long string"
   except komxx.err_no_such_text:
      print "That text does not exist"   
   except komxx.errno_error:
      print "Unknown KOM error: " + sys.exc_value.errmsg

if __name__ == "__main__":
	test(eval(sys.argv[2]))
